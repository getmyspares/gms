<?php
require_once '/var/www/html/app/bootstrap.php';
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
?>
<script>
	window.location.href="<?php echo $site_url; ?>";
</script>