<?php
error_reporting(-1);
ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);

include('app/bootstrap.php');
include 'barcode/vendor/autoload.php';
use Magento\Framework\App\Bootstrap;
setlocale(LC_MONETARY,"en_IN.UTF-8");
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$customerSession = $objectManager->get('Magento\Customer\Model\Session');
$site_url=$storeManager->getStore()->getBaseUrl(); 
/*
if($customerSession->isLoggedIn()) 
{
	$customerArray=$customerSession->getData();
	$group_id=$customerArray['customer_group_id'];
	$user_idd=$customerArray['customer_id'];
	
	
}
else
{ 
?>
<script>
	window.location.href="<?php echo $site_url; ?>";
</script> 
<?php	
}	 
*/



$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');

$webkul_helper = $objectManager->create('Webkul\Marketplace\Helper\Data');
$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');








$order_inc_id='000000863'; 
$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
$order_id=$order->getId();

/* echo "<pre>";
	print_r($order->getData()); 
echo "</pre>"; */




$awb=$tax_helpers->tax_get_order_awb($order_id);  

if($awb==0)
{
	$awb='';	
}	
$currencySymbol = '<img src="'.$site_url.'Customimages/Indian_Rupee_symbol.png" style="width:12px;">';   


$orderArray=$order->getData();
$shipping_amount=$orderArray['shipping_amount'];
$grand_total=$orderArray['grand_total']; 
$shipping_amount=number_format($shipping_amount, 2, '.', '');


$cgst_amount=$orderArray['cgst_amount']; // base price + tax
$sgst_amount=$orderArray['sgst_amount']; // base price + tax
$igst_amount=$orderArray['igst_amount']; // base price + tax
//$utgst_amount=$orderArray['utgst_amount']; // base price + tax
$customer_id=$orderArray['customer_id']; 



$cgst_amount=number_format($cgst_amount, 2, '.', '');  
$sgst_amount=number_format($sgst_amount, 2, '.', '');  
$igst_amount=number_format($igst_amount, 2, '.', '');  
$utgst_amount=0;
$utgst_amount=number_format($utgst_amount, 2, '.', '');  

$tax_amount=$igst_amount + $cgst_amount + $sgst_amount;	


$order_subtotal=$orderArray['subtotal'];
$order_subtotals=$order_subtotal-$tax_amount;


$grand_total=number_format($grand_total, 2, '.', ''); 
$order_subtotals=number_format($order_subtotals, 2, '.', ''); 

$invoice_id=$data_helpers->get_order_invoice_id($order_inc_id); 


$order_cr_date=$order->getCreatedAt();
$order_cr_date=date('M d, Y', strtotime($order_cr_date));
 
$billing_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
$billing_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
$billing_name=$billing_firstname.' '.$billing_lastname; 
$billing_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
$billing_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
$billing_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
$billing_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
$billing_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','billing');
  
$street = ''; 
foreach($order->getShippingAddress()->getStreet() as $value){
	$street .= $value.', ' ;
}
$billing_street_address = $street; 	



$shipping_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','shipping');
$shipping_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','shipping');
$shipping_name=$shipping_firstname.' '.$shipping_lastname; 
$shipping_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
$shipping_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
$shipping_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
$shipping_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
$shipping_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
  
$shipping_street = ''; 
foreach($order->getShippingAddress()->getStreet() as $value){
	$shipping_street .= $value.', ' ;
}
$shipping_street_address = $shipping_street; 

$payment = $order->getPayment();
$method = $payment->getMethodInstance();
$methodTitle = $method->getTitle();


foreach ($order->getAllItems() as $item)
{
	
	$iteamArray=$item->getData();
	$item_product_id=$item->getId(); 
	
	
	$sellerArray[]=$custom_helpers->get_product_seller($order_id,$item_product_id); 
	 
}

$sellerArrays=array_unique($sellerArray);
$seller_other_info='';
$seller_shop='';
$seller_logo='';
$seller_description='';
$companyLocality='';
 $cin_number='';
$seller_array=array();

if(!empty($sellerArrays)) {
	foreach($sellerArrays as $sellerId)
	{
		$sellerId;
		$sellerInfo = $webkul_helper->getSellerInfo($sellerId);
		$sellerData = $webkul_helper->getSellerDataBySellerId($sellerId);
		

		$shopTitle = $sellerInfo['shop_title'];
		$shopUrl = $sellerInfo['shop_url'];
		
		
		//$companyLocality = $block->escapeHtml($companyLocality);
		if (!$shopTitle) {
			$shopTitle = $shopUrl;
		}
		// print_r($sellerInfo); 
		//print_r($sellerData->getData());
		$seller_array = $sellerData->getData();
		if(!empty($seller_array))
		{		
			$seller_description = $seller_array[0]['company_description'];
		}
		if(!empty($seller_array[0]['others_info']))
		{
			$seller_other_info=$seller_array[0]['others_info'];	
			
			$seller_new_addresss=explode(PHP_EOL,$seller_other_info);
			foreach($seller_new_addresss as $row)
			{
				$seller_shop .=$row."<br>";	
			}	
			
		}	
		
		if(isset($sellerInfo['logo_pic']))
		{
			$logo = $sellerInfo['logo_pic'];
			$seller_logo=$webkul_helper->getMediaUrl().'avatar/'.$logo;
			$seller_logo_s=$site_url.'image.php?image='.$seller_logo; 
		}
		$productCount = $sellerInfo['product_count'];
		
		
		if(isset($sellerInfo['company_locality']))
		{
			$companyLocality = trim($sellerInfo['company_locality']);
		}	
		
		
		
		
		
	}	
}
$logo_img='';	


$seller_logo_s='https://getmyspares.com/pub/media/logo/stores/1/logo.png';
if($seller_logo!='')
{	 
	$logo_img='<img src="'.$seller_logo_s.'" style="width:300px"/>';	
}	





$sellerArray=$custom_helpers->get_seller_details($sellerId);
/*  echo "<pre>";
	print_r($sellerArray);
echo "</pre>";  */

$pincodeee=$sellerArray['zipcode'];
$city_state=$api_helpers->get_pincode_details($pincodeee);

$seller_city='';
$seller_state='';


if($city_state!='0')
{
	$city_state=json_decode($city_state);	
	$seller_city=$city_state->city;
	$seller_state=$city_state->state;
}	

	
$customer_id;	
$customerArray=$api_helpers->get_user_details($customer_id);	

$customer_gst='';
$cust_company_address='';
 
if(!empty($customerArray[1]['gst']))
{
	$customer_gst=$customerArray[1]['gst'];
}
if(!empty($customerArray[1]['company_name']))
{
	 $cust_company_address=$customerArray[1]['company_name'];
}	
  
/*  echo "<pre>";
	print_r($customerArray);
echo "</pre>"; */  



$select = $connection->select()
	  ->from('marketplace_userdata')  
	  ->where('seller_id = ?', $user_idd);
	 

$result = $connection->fetchAll($select);
if(empty($result))		
{
	$sub_heading="Original for Recipient";	
}
else
{
	$sub_heading="Triplicate for Supplier";	
}	
 

 
//$tax_helpers->success_page($order_id);
 
$select = $connection->select()
	  ->from('order_tax_list')  
	  ->where('order_id = ?', $order_inc_id);
	 

$result = $connection->fetchAll($select);
if(!empty($result))
{
	$tax_list=$result[0]['tax_list'];
	$tax_list=json_decode($tax_list);
	
	$shipping_gst_rate=$tax_list->shipping_gst;
	
}

$select = $connection->select()
->from('customer_entity_varchar')  
->where('entity_id = ?', $sellerId)
->where('attribute_id = ?', '265');
$result = $connection->fetchAll($select);
$signtureImg='0';
if(!empty($result))
{
	$signtureImg = $site_url.'/pub/media/signature/saller/'.$result[0]['value'];	   
}
$customer_gst_sec='';
$cust_company_address_sec='';

if($customer_gst!='')
{
	$customer_gst_sec='<p>GSTIN: '.$customer_gst.'</p>'; 
}

if($cust_company_address!='')
{
	 $cust_company_address_sec='<p>'.$cust_company_address.'</p>';
}	

$html = ' 
<html>
<head>  
<style>
table{
	width:100%;
font-size:10px;
border-collapse: collapse;
margin:0px auto;
background:#fff;
}
@page {margin:30px;}
table img{width:250px}

table tr th{
vertical-align:middle;
font-size:24px;
}
table tr th:first-child{
	text-align:left;
}
table tr th:last-child{
	text-align:right;
}
table tr th p{
	margin:0px;
	font-size:20px;
	width:100%;
}
table tr td p{
	margin:0px;
}

table tr th, table tr td{
    padding:10px;
    font-size:14px;
}
.border{border:1px solid #9c9c9c}
.price-table tr th{
	background:#edeaea
}
.price-table tr td{
	font-weight:600;
}
</style>
</head>
<body style="font-family: Open Sans, sans-serif;font-size:10px;" >';
$html .='</div> 
	<table>
	<tbody>
		<tr>			
			<td colspan="4" align="center" valign="top">
				<strong style="font-size:25px;border-bottom:2px solid #000">TAX INVOICE</strong>				
			</td>   
		</tr>
		<tr>
			<td colspan="2" align="left" valign="top">
				<img src="https://getmyspares.com/pub/media/email/logo/stores/1/logo.jpg" style="width:150px" >
			</td>
			<td colspan="2" align="right"><strong style="font-size:14px;">'.$sub_heading.'</strong></td>			
		</tr>
		<tr style="background:#c2c2c2; color:#fff"> 
			<td colspan="2">
				<p style="color:#000">'.$sellerArray['seller_comp_nam'].'<br>
				'.$sellerArray['seller_comp_address'].'<br>
				CITY : '.$seller_city.'<br>
				STATE : '.$seller_state.'<br>
				PINCODE : '.$pincodeee.'<br>  
				GSTIN : '.$sellerArray['seller_gst'].'<br>
				PAN: '.$sellerArray['seller_comp_pan'].'<br>
				CIN: '.$sellerArray['cin_number'].'<br></p> 
			</td>
			<td colspan="2">
				<p style="color:#000">Invoice Num : '.$invoice_id.'</p>
				<p style="color:#000">Invoice Date : '.$order_cr_date.'</p> 
				<p style="color:#000">Order Num  : '.$order_inc_id.'</p>
				
			</td> 
		</tr> 
		<tr>
			<td class="border" style="background:#edeaea" colspan="2"><strong>BILL TO:</strong></td>
			<td class="border" style="background:#edeaea" colspan="2"><strong>SHIP TO:</strong></td>
		</tr> 
		<tr class="border">
			<td colspan="2">
				<p>'.$billing_name.'</p>
				'.$cust_company_address_sec.'
				<p>'.$billing_street_address.'</p>
				<p>'.$billing_city.', '.$billing_state.', '.$billing_pincode.'</p>
				<p>INDIA. M: '.$billing_telephone.'</p> 
				'.$customer_gst_sec.'
			</td>
			<td colspan="2">
				<p>'.$shipping_name.'</p>
				'.$cust_company_address_sec.'
				<p>'.$shipping_street_address.'</p>
				<p>'.$shipping_city.', '.$shipping_state.', '.$shipping_pincode.'</p>
				<p>INDIA. M:'.$shipping_telephone.'</p> 
				'.$customer_gst_sec.'
			</td>   
		</tr> 
		<tr>
			<td colspan="4" style="height:20px;"></td>
		</tr>
		<tr >
			<td colspan="2" class="border" valign="top">
			<p style="margin-bottom:10px"><strong>Place of Supply</strong> : '.$shipping_city.'</p>
			<p style="margin-bottom:10px"><strong>Tax payable on Reverse Charge</strong> : No</p>
			<p><strong>Payment Method</strong> : '.$methodTitle.'</p>
			</td>
			<td colspan="2" class="border" valign="top">
			<p style="margin-bottom:10px"><strong>Shipping Details</strong> : Through E-com Express</p>
			<p><strong>AWB</strong> : '.$awb.'</p>
			</td>
		</tr>
		<!--<tr class="border">
			<td colspan="2">
				<p>'.$shipping_city.'</p> 
			</td>
			<td colspan="2">
				<p>No</p>  
			</td> 
		</tr> 
		<tr>
			<td colspan="4" style="height:20px;"></td>
		</tr>
		<tr>
			<td class="border" style="background:#edeaea" colspan="2"><strong>Payment Method</strong></td>
			<td class="border" style="background:#edeaea" colspan="2"><strong>Shipping Details:</strong></td>
		</tr>
		<tr class="border">
			<td colspan="2">
				<p>'.$methodTitle.'</p> 
			</td>
			<td colspan="2">
				<p><strong>Through</strong> : E-com Express</p>  
				<p><strong>AWB</strong> : '.$awb.'</p>
				<p style="display:none;">(Total Shipping Charges '.$currencySymbol.$shipping_amount.')</p>
			</td> 
		</tr> -->
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		
	</tbody>
	</table>';
	
	$html .='<table class="price-table">
		<thead> 
			<tr class="border">
				<th align="left">Sr. No</th>
 				<th align="center">Products</th>
				<th align="center">GST Info</th>
				<th align="center">Unit Price</th>
				<th align="center">Qty</th>
				<th align="center">Invoice Value</th>
				<th align="center">Discount</th>
				<th align="center">Taxable Value</th>
				<th align="center">CGST</th>
				<th align="center">SGST/UTGST</th>
				<th align="center">IGST</th>
				<th align="center">Total GST</th>
				<th align="center">Cess/Other Taxes</th>
				<th align="center">Total Invoice Value</th>    
			</tr>  
		</thead>
		<tbody>';
		
		
			$ProductorderArray =   $tax_helpers->order_item_details($order_inc_id); 
			$ProductorderArray =   json_decode($ProductorderArray); 
			/* echo "<pre>";
				print_r($ProductorderArray);
			echo "</pre>";  */
			$a=0;
			$i=1;
			$net_taxable_value_total=0;
			
			$order_unit_price_total=0;
			$order_qty_total=0;
			$order_invoice_value_total=0;
			$order_discount_value_total=0;
			
			
			$order_prod_cgst_total=0;
			$order_prod_sgst_total=0;
			$order_prod_igst_total=0;
			$order_product_gst_total=0;
			$order_other_tax_total=0;
			$order_net_taxable_value_total=0; 
			foreach ($order->getAllItems() as $item)
			{
				
				
				$product_idd=$item->getProductId();
				/* echo "<pre>";  
				/* echo "<pre>";  
				print_r($item->getData());
				echo "</pre>";   */
				$product = $productRepository->getById($product_idd); 
				$other_tax=0;
				$prod_cgst=0;
				$prod_sgst=0;
				$prod_igst=0;   
				
				
				$gst_rate=$product->getGstRate(); 
				$hsn=$product->getHsn(); 
				
				$productArray='';
				$productArray=$item->getData();
				
				
				
				
				$productgstArray=$tax_helpers->order_item_details_product($order_inc_id,$product_idd);
				/*  echo "<pre>";
					print_r($productgstArray);
				echo "</pre>";     */ 
				
				$prod_cgst=0;
				$prod_sgst=0;
				$prod_igst=0;
				$prod_utgst=0;
				
				
				$prod_cgst=$productgstArray[0]['cgst_amount'];
				$prod_sgst=$productgstArray[0]['sgst_amount'];
				$prod_igst=$productgstArray[0]['igst_amount'];
				$prod_utgst=$productgstArray[0]['utgst_amount'];
				
				
				if($prod_cgst!='0.00' && $prod_sgst!='0.00')
				{
					$prod_sgst=$productgstArray[0]['sgst_amount'];	
				}	
				if($prod_cgst!='0.00' && $prod_utgst!='0.00')
				{
					$prod_sgst=$productgstArray[0]['utgst_amount'];	
				} 
				
				/*
				if(!empty($productArray['utgst_amount']))
				{	
					$prod_sgst=$productArray['utgst_amount']; 
				}
				else
				{
					$prod_sgst=$productArray['sgst_amount'];
				}
				*/
				
				$prod_base_price_incl_tax=$productArray['base_price_incl_tax'];
				
				
				$product_gst=$prod_cgst+$prod_sgst+$prod_igst; 
				 
				$product_base_price=$prod_base_price_incl_tax-$product_gst;
				
				$qty=$item->getQtyOrdered();
				$qty=number_format($qty, 0, '.', ''); 
				$prod_base_price_incl_tax=number_format($prod_base_price_incl_tax, 2, '.', '');  
				$product_base_price=number_format($product_base_price, 2, '.', '');  
				//$product_gst=number_format($product_gst, 2, '.', '');  
				$product_gst=$api_helpers->custom_number_format($product_gst);     
				$product_name=$item->getName();
				//$product_name=preg_replace('/[^A-Za-z0-9]/', '', $product_name); 
				 
				//$invoice_value=$productArray['row_total'];
				$invoice_value=$product_base_price*$qty;
				$invoice_value=number_format($invoice_value, 2, '.', '');  
				
				$discount_value=0;
				$discount_value=number_format($discount_value, 2, '.', '');  
				
				
				
				$prod_cgst=number_format($prod_cgst, 2, '.', '');  
				$prod_sgst=number_format($prod_sgst, 2, '.', '');  
				$prod_igst=number_format($prod_igst, 2, '.', '');  
				
				$other_tax=number_format($other_tax, 2, '.', '');  
				
				
				
				
				$gst_rate;
				
				if($prod_igst!=0)
				{
					$gst_info='
						<p>IGST : '.$gst_rate.'%</p>
					';	
				}
				else
				{
					
					$gst_rate=$gst_rate/2;
					$gst_rate=number_format($gst_rate, 2, '.', '');  
					$gst_info='
						
						<p>CGST : '.$gst_rate.'%</p>
						<p>SGST/UTGST : '.$gst_rate.'%</p>  
					';	
				}		
			

			$product_base_price=$ProductorderArray[$a]->base_amount;
			$product_qty=$ProductorderArray[$a]->product_qty;
			$invoice_value=$product_base_price;
			
			$product_unit_price=$product_base_price/$product_qty;
			//$product_unit_price=number_format($product_unit_price, 2, '.', ''); 
			$product_unit_price=$api_helpers->custom_number_format($product_unit_price);   
			
			$product_base_price=number_format($product_base_price, 2, '.', '');  
			 
			
			//$taxable_value=$invoice_value-$discount_value;
			$taxable_value=$ProductorderArray[$a]->total_price;
			
			$taxable_value=number_format($taxable_value, 2, '.', '');  
			
			
			$net_taxable_value=$taxable_value;
			$net_taxable_value=number_format($net_taxable_value, 2, '.', '');  
				
			$net_taxable_value_total=$net_taxable_value_total+$net_taxable_value;
			
			$order_unit_price_total=$order_unit_price_total+$product_unit_price;
			$order_qty_total=$order_qty_total+$qty;
			$order_invoice_value_total=$order_invoice_value_total+$invoice_value;
			$order_discount_value_total=$order_discount_value_total+$discount_value;
			
			$order_prod_cgst_total=$order_prod_cgst_total+$prod_cgst;
			$order_prod_sgst_total=$order_prod_sgst_total+$prod_sgst;
			$order_prod_igst_total=$order_prod_igst_total+$prod_igst;
			$order_product_gst_total=$order_product_gst_total+$product_gst;
			$order_other_tax_total=$order_other_tax_total+$other_tax;
			$order_net_taxable_value_total=$order_net_taxable_value_total+$net_taxable_value;
			
			
			
			
			$order_discount_value_total=$api_helpers->custom_number_format($order_discount_value_total);  
			
			$order_prod_cgst_total=$api_helpers->custom_number_format($order_prod_cgst_total); 
			$order_prod_sgst_total=$api_helpers->custom_number_format($order_prod_sgst_total); 
			$order_prod_igst_total=$api_helpers->custom_number_format($order_prod_igst_total); 
			$order_product_gst_total=$api_helpers->custom_number_format($order_product_gst_total); 
			$order_other_tax_total=$api_helpers->custom_number_format($order_other_tax_total); 
			$order_net_taxable_value_total=$api_helpers->custom_number_format($order_net_taxable_value_total); 
 
			$taxable_value=$taxable_value-$discount_value;  
			$taxable_value=$api_helpers->custom_number_format($taxable_value); 	 
			
			$product_qtyy=$qty;
			$product_qtyy=$api_helpers->custom_number_format($product_qtyy); 	
			
			$html .='
			<tr>
				<td align="left">'.$i.'</td>  
				<td align="center">'.$product_name.'</td>
				<td align="center">
					<p>HSN: '.$hsn.'</p>
					'.$gst_info.'  
				</td>
				<td align="center">'.$product_unit_price.'</td> 
				<td align="center">'.$product_qtyy.'</td> 
				<td align="center">'.$invoice_value.'</td>
				<td align="center">'.$discount_value.'</td>
				<td align="center">'.$invoice_value.'</td>
				<td align="center">'.$prod_cgst.'</td>
				<td align="center">'.$prod_sgst.'</td>
				<td align="center">'.$prod_igst.'</td>
				<td align="center">'.$product_gst.'</td> 
				<td align="center">'.$other_tax.'</td>
				<td align="center">'.$net_taxable_value.'</td> 
			</tr>';
			$i++;
			$a++;
			}
			
			
			$shipping_gst_amount=($shipping_amount*$shipping_gst_rate)/100;
			$shipping_gst_amount=number_format($shipping_gst_amount, 2, '.', '');  
			$shipping_gst_amount_12=number_format($shipping_gst_amount, 2, '.', '');  
			
			$shipping_gst_ex_gst=$shipping_amount-$shipping_gst_amount;
			
			$shipping_gst_ex_gst=number_format($shipping_gst_ex_gst, 2, '.', '');  
			
			
			$shipping_igst_amount=0;
			$shipping_cgst_amount=0;
			$shipping_sgst_amount=0; 
			
			
			
			$net_taxable_value_total=number_format($net_taxable_value_total, 2, '.', '');  
			$order_total=$net_taxable_value_total+$shipping_amount;
			$order_total=number_format($order_total, 2, '.', '');  
			
			
			
			
			
			
			
			$adminArray = $tax_helpers->order_amount_details($order_inc_id); 
			$adminArray =  json_decode($adminArray); 
			
			$shipping_gst=$adminArray->shipping_gst;
			$shipping_base=$adminArray->shipping_base;
			
			
			if($prod_igst!=0)
			{
				$gst_info='
					<p>HSN : 996812</p>
					<p>IGST : '.$shipping_gst_rate.'%</p>
				';	
				
				$shipping_igst_amount=$shipping_gst;
				
			}
			else
			{
				
				$gst_rate=$shipping_gst_rate/2;
				$gst_rate=number_format($gst_rate, 2, '.', '');  
				$gst_info='
					<p>HSN : 996812</p>
					<p>CGST : '.$gst_rate.'%</p>
					<p>SGST/UTGST : '.$gst_rate.'%</p>  
				';	
				
				$shipping_gst_amount_1=$shipping_gst/2;
				
				$shipping_cgst_amount=$shipping_gst_amount_1;
				$shipping_sgst_amount=$shipping_gst_amount_1; 
				
				 
			}	 
			
			$shipping_cgst_amount=number_format($shipping_cgst_amount, 2, '.', '');  
			$shipping_igst_amount=number_format($shipping_igst_amount, 2, '.', '');  
			$shipping_sgst_amount=number_format($shipping_sgst_amount, 2, '.', '');  
			$shipping_base=number_format($shipping_base, 2, '.', '');   
			 
			
			$total_shipping_gst=$shipping_gst;
			
			
			
			/* echo "<pre>";
				print_r($adminArray);
			echo "</pre>";  */ 
			
			
			$html .='
			<tr>
				<td align="left">'.$i.'</td>   
				<td align="center">Shipping & Handling</td>
				<td align="center">
					'.$gst_info.'
				</td> 
				<td align="center">'.$shipping_base.'</td>
				<td align="center">1.00</td> 
				<td align="center">'.$shipping_base.'</td>
				<td align="center">0.00</td> 
				<td align="center">'.$shipping_base.'</td>
				<td align="center">'.$shipping_cgst_amount.'</td>
				<td align="center">'.$shipping_sgst_amount.'</td>
				<td align="center">'.$shipping_igst_amount.'</td>
				<td align="center">'.$total_shipping_gst.'</td>
				<td align="center">0.00</td>
				<td align="center">'.$shipping_amount.'</td>
				
			</tr>';
			$i++;
			 
			
			$order_unit_price_total=$order_unit_price_total+$shipping_base;
			$order_qty_total=$order_qty_total+1; 
			$order_invoice_value_total=$order_invoice_value_total+$shipping_base;
			$order_discount_value_total=$order_discount_value_total+0;
			
			$order_prod_cgst_total=$order_prod_cgst_total+$shipping_cgst_amount;
			$order_prod_sgst_total=$order_prod_sgst_total+$shipping_sgst_amount;
			$order_prod_igst_total=$order_prod_igst_total+$shipping_igst_amount;
			$order_product_gst_total=$order_product_gst_total+$total_shipping_gst;
			$order_other_tax_total=$order_other_tax_total+0;
			$order_net_taxable_value_total=$order_net_taxable_value_total+$shipping_amount;
			
			 
			
			
			$order_discount_value_total=$api_helpers->custom_number_format($order_discount_value_total);  
			
			$order_prod_cgst_total=$api_helpers->custom_number_format($order_prod_cgst_total); 
			$order_prod_sgst_total=$api_helpers->custom_number_format($order_prod_sgst_total); 
			$order_prod_igst_total=$api_helpers->custom_number_format($order_prod_igst_total); 
			$order_product_gst_total=$api_helpers->custom_number_format($order_product_gst_total); 
			$order_other_tax_total=$api_helpers->custom_number_format($order_other_tax_total); 
			$order_net_taxable_value_total=$api_helpers->custom_number_format($order_net_taxable_value_total); 
 
			$taxable_value=$taxable_value-$discount_value;  
			$taxable_value=$api_helpers->custom_number_format($taxable_value); 	 
			
			
			
			$order_qty_total=$api_helpers->custom_number_format($order_qty_total); 
			$order_invoice_value_total=$api_helpers->custom_number_format($order_invoice_value_total); 
			
			$order_unit_price_total=$api_helpers->custom_number_format($order_unit_price_total); 
			$order_qty_total=$api_helpers->custom_number_format($order_qty_total);  
			 
			
			$html .='
			<tr>
				<td align="left" style="border-top:2px solid #000"></td>   
				<td align="center" style="border-top:2px solid #000">Total</td> 
				<td align="center" style="border-top:2px solid #000">
					&nbsp;
				</td> 
				<td align="center" style="border-top:2px solid #000">'.$order_unit_price_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_qty_total.'</td> 
				<td align="center" style="border-top:2px solid #000">'.$order_invoice_value_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_discount_value_total.'</td> 
				<td align="center" style="border-top:2px solid #000">'.$order_invoice_value_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_prod_cgst_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_prod_sgst_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_prod_igst_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_product_gst_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_other_tax_total.'</td>
				<td align="center" style="border-top:2px solid #000">'.$order_net_taxable_value_total.'</td>
				
			</tr>';
			
			
			$html .='
			
			<tr>
				<td colspan="13" align="right" style="border-top:2px solid #000">Net Invoice Value:</td> 
				<td align="right" style="border-top:2px solid #000">'.$order_total.'</td>
			</tr>';
			
			
			$html .='<tr><td colspan="13" align="right" valign="top">
				<p>For '.$sellerArray['seller_comp_nam'].'</p>
				
			</td></tr>';  
			$html .='
			
			<tr>';
			if($signtureImg!='0')
			{	
			$html .='<td colspan="13" align="right" valign="top">
				
				<img src="'.$signtureImg.'" style="width:150px" >
			</td>';  
			}	
			$html .='</tr>
			';  
			
			$html .='
			<tr>
			<td colspan="13" align="right" valign="top">
				<p>(Authorised Signatory)</p>
			</td>';
			
			
		$html .='</tbody>
		</table> 
		';
		 
	
	
                       

$html .='</body></html>';

echo $html; 
die;     

 
//==============================================================
//==============================================================
//==============================================================



$file='invoice_'.$order_inc_id;

include("pdf/mpdf.php");

$mpdf=new mPDF('c','A4-P','','',32,25,27,25,16,13);  

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
//$stylesheet = file_get_contents('mpdfstyletables.css');
//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);
$price_name='PackingSlip-'.date('Y-m-d'); 
$mpdf->Output($file.'.pdf','D');   
//$mpdf->Output($price_name.'.pdf','I');
exit;


?>
 
