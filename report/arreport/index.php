	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
	<script>
		$(document).ready( function () {
			$("#example").DataTable( {
				dom: "Bfrtip",
				buttons: [
					{ extend: "excel", text: "Export In Excel" }
				],
				"pageLength": 100,
				"aaSorting": [ [2,'desc'] ]
			} );
		} );
</script>

<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include('../../app/bootstrap.php');
	
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	// From Rma extension
    $return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
	
	try
	{
		
		$orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
		$orderDatamodel->setOrder('entity_id', 'DESC');
		//$orderDatamodel->addAttributeToSort('entity_id', 'ASC');
		
		
		?>
<table border="1" style="width:100%" id="example"> 
	<thead>
		<tr>
			<th>Buyer name</th>
			<th>Buyer Address</th>		
			<th>Invoice Num</th>	
			<th>Invoice date</th>	
			<th>Invoice Amt </th>
			<th>Ageing</th>	<!-- Ageing --> 

		</tr>
	</thead>
	<tbody>
<?php
	
		foreach($orderDatamodel as $orderDatamodel1){
			$orderId = $orderDatamodel1->getEntityId();
			$transaction = $objectManager->create('\Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory')->create()->addOrderIdFilter($orderId)->getFirstItem();

			$transactionId = $transaction->getData('txn_id');
			$payment = $orderDatamodel1->getPayment();
			$amount_paid = $payment->getAmountPaid();
			
			
			$coupon_code = $orderDatamodel1->getCouponCode();
			$order_status = $orderDatamodel1->getStatus();
			
			$orderId = $orderDatamodel1->getEntityId();
			$order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($orderId);

			$order_id = $orderDatamodel1->getIncrementId();
			$order_GrandTotal = $orderDatamodel1->getGrandTotal();
			
			$order_increment_id = $orderDatamodel1->getIncrementId();
				
			$invoice_details = $order->getInvoiceCollection();
			// echo '<pre>';
			// print_r($invoice_details->getData());				
			// echo '</pre>';
			foreach ($invoice_details as $_invoice) {
					$invoice_id =  '#'.$_invoice['increment_id'];
					$date = $_invoice['created_at'];
			}
			
			$buyer_name = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
			$buyer_city = $order->getBillingAddress()->getCity();
			$buyer_state = $order->getBillingAddress()->getRegion();
			$buyer_pincode = $order->getBillingAddress()->getPostcode();
			$buyer_country_id = $order->getBillingAddress()->getCountryId();
			$buyer_street = '';
			foreach($order->getBillingAddress()->getStreet() as $value){
				$buyer_street .= $value.', ' ;
			}
			$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode.".";


			?>
			<tr>
			
				<!--Buyer name -->
				<td>
				<?php
				if($buyer_name!=" "){
					echo $buyer_name;
				}else{
					echo "Guest";
				}
				?>
				</td>
			
				<td><?php echo $buyer_address; ?></td>	 				<!-- Buyer address -->
				
				
				<!-- Invoice Ref Number -->
				<td>
				<?php
					if(!empty($invoice_id)){
						echo $invoice_id;
					}else{
						echo 'n/a';
					}
				?>
				</td>
				
				<!-- Invoice Date -->
				<td>
					<?php 
					if(!empty($date)){
						echo $date;
					}else{
						echo 'n/a';
					}
					?>
				</td>
				<!-- Month -->				
				<td><?php echo $order_GrandTotal; ?></td>
				
				<!-- Buyer details -->
						
				<!-- aging-->
				<!-- (invoice date + return date) - Current date --> 
				<?php
					//$return_date = 30 ;
					$today = date("Y-m-d",strtotime($date)); 
					$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
					$today = date("Y-m-d");

					$date1=date_create($RealizationDays);
					$date2=date_create($today);
					$diff=date_diff($date1,$date2);
							
				?>
				<td>
				
				<?php
				// echo $RealizationDays;
				// echo $today;
					$left_days = $diff->format("%R%a");
					if($left_days < 0){
					echo $diff->format("%a days"); 
					}else{
						echo 'Completed';
					}
					?>
				</td>	 				<!-- transaction id -->

			</tr>	
	<?php
		}
	?>
		</tbody>
	</table>
	<?php	
	}catch(Exception $e){		
		echo $msg = 'Error : '.$e->getMessage();
	}
?>