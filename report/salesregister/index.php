	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
	<script>
			$(document).ready( function () {
				$("#example").DataTable( {
					dom: "Bfrtip",
					buttons: [
						{ extend: "excel", text: "Export In Excel" }
					],
					"pageLength": 100,
					"aaSorting": [ [0,'desc'] ]
				} );
			} );
	</script>
<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include('../../app/bootstrap.php');
	
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	try{
		$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');		
		$orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
		$orderDatamodel->setOrder('entity_id', 'DESC');
	?>
	<table border="1" id ="example">
		<thead>
			<tr>
				<th>Invoice Ref Number</th>	
				<th>Invoice date</th>	
				<th>Month</th>
				<th>Buyer name (Billing name)</th>	
				<th>Buyer address (Billing address)</th>	 
				<th>Buyer City (Billing city)</th>	
				<th>Buyer State (Billing state)</th>	
				<th>Buyer pincode (Billing pincode)</th>	
				<th>Delivery name (Shipping name)</th>	
				<th>Delivery Address (Shipping address)</th>	 
				<th>Delivery City (Shipping city)</th>	
				<th>Delivery State (Shipping state)</th>	
				<th>Delivery pincode (Shipping pincode)</th>	
				<th>Item</th>	
				<th>Quantity</th>	
				<th>order no</th>	
				<th>Product category</th>	
				<th>Gross Price Amt(INR)</th>	
				<th>Seller code</th>	
				<th>Seller name</th>	
				<th>seller address</th>	
				<th>Seller City</th>	
				<th>Seller State</th>	
				<th>Seller pincode</th>	
				<th>Mat. Descrip</th>	
				<th>GST</th>	
				<th>shipping</th>	
				<th>shipping tax</th>	
				<th>Total</th>
				<th>Commision</th>	
				<th>Commision tax</th>	
				<th>Comission TDS</th>	
				<th>Total commission</th>	
				<!--<th>Collection TDS</th>	-->
				<th>Credit card charges</th>	
				<th>credit card tax</th>	
				<th>Total credit card expenses</th>	
				<th>COD charges</th>	
				<th>COD tax</th>	
				<th>Total COD expenses</th>	
				<th>Status of order</th>	
				<th>Order date</th>	
				<th>Shipping date</th>	
				<th>Delivery date</th>	
				<th>Realization date</th>	
				<th>Length</th>	
				<th>Breadth</th>	
				<th>Height</th>	
				<th>Absolute wt</th>	
				<th>Logistic Wt</th>	
				<th>Discount %</th>	
				<th>Reason of discount</th>	
				<th>Discount cost</th>	
				<th>Coupon used</th>	
				<th>counpon amount</th>
			</tr>
		</thead>
		
		<tbody>

		<?php
			// $i =0 ;
			foreach($orderDatamodel as $orderDatamodel1){
			
			// if($i==6){
				// die();
			// }
			// order details
			// echo '<pre>';
			// print_r($orderDatamodel1->getData());
			// echo '</pre>';
			
			
			$order_basesubtotal = $orderDatamodel1->getBaseSubtotal();
			$base_shipping_amount = $orderDatamodel1->getBaseShippingAmount();
			$coupon_code = $orderDatamodel1->getCouponCode();
			$order_status = $orderDatamodel1->getStatus();
			
			$orderId = $orderDatamodel1->getEntityId();
			//$order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($orderId);

			$order_id = $orderDatamodel1->getIncrementId();
			
			$order_increment_id = $orderDatamodel1->getIncrementId();
			foreach ($orderDatamodel1->getAllItems() as $item){
				
				
				
				
				$invoice_details = $orderDatamodel1->getInvoiceCollection();
				// echo '<pre>';
				// print_r($invoice_details->getData());				
				// echo '</pre>';
				foreach ($invoice_details as $_invoice) {
						$invoice_id =  '#'.$_invoice['increment_id'];
						$date = $_invoice['created_at'];
				}
				
				$buyer_name = $orderDatamodel1->getCustomerFirstname().' '.$orderDatamodel1->getCustomerLastname();
				$buyer_city = $orderDatamodel1->getBillingAddress()->getCity();
				$buyer_state = $orderDatamodel1->getBillingAddress()->getRegion();
				$buyer_pincode = $orderDatamodel1->getBillingAddress()->getPostcode();
				$buyer_country_id = $orderDatamodel1->getBillingAddress()->getCountryId();
				$buyer_street = '';
				foreach($orderDatamodel1->getBillingAddress()->getStreet() as $value){
					$buyer_street .= $value.', ' ;
				}
				$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode.".";
				
				
				$delivery_name = $orderDatamodel1->getShippingAddress()->getFirstname().' '.$orderDatamodel1->getShippingAddress()->getLastname();
				$delivery_city = $orderDatamodel1->getShippingAddress()->getCity();
				$delivery_state = $orderDatamodel1->getShippingAddress()->getRegion();
				$delivery_pincode = $orderDatamodel1->getShippingAddress()->getPostcode();
				$delivery_country_id = $orderDatamodel1->getShippingAddress()->getCountryId();
				
				$street = '';
				foreach($orderDatamodel1->getShippingAddress()->getStreet() as $value){
					$street .= $value.', ' ;
				}
				$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode.".";
				
				
				$product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getProductId());
				$product_desc = $product->getDescription();

			?>
			<tr>
				<!-- Invoice Ref Number -->
				<td>
				<?php
					if(!empty($invoice_id)){
						echo $invoice_id;
					}else{
						echo 'n/a';
					}
				?>
				</td>
				
				<!-- Invoice Date -->
				<td>
					<?php 
					if(!empty($date)){
						echo $date;
					}else{
						echo 'n/a';
					}
					?>
				</td>
				<!-- Month -->				
				<td><?php echo date("F",strtotime($date)); ?></td>
				
				<!-- Buyer details -->
				<!--Buyer name -->
				<td>
				<?php
				if($buyer_name!=" "){
					echo $buyer_name;
				}else{
					echo "Guest";
				}
				?>
				</td>		
				<td><?php echo $buyer_address; ?></td>	 				<!-- Buyer address -->
				<td><?php echo $buyer_city; ?></td>		<!-- Buyer City -->
				<td><?php echo $buyer_state; ?></td>	<!-- Buyer State -->
				<td><?php echo $buyer_pincode; ?></td>	<!-- Buyer pincode -->
				
				<!-- Shipping details -->
				
								
				<td><?php echo $delivery_name; ?></td>		<!-- Delivery name -->					
				<td><?php echo $delivery_adress; ?></td>	<!-- Delivery name -->				
				<td><?php echo $delivery_city; ?> </td>		<!-- Delivery City -->							
				<td><?php echo $delivery_state; ?> </td>	<!-- Delivery State -->					
				<td><?php echo $delivery_pincode; ?> </td>	<!-- Delivery pincode -->
				
				<!-- Item -->
				<td><?php echo $item->getName();?></td>	
				<td><?php echo $item->getQtyOrdered(); ?> <!-- item Quantity --> </td>	
				<td><?php echo '#'.$order_increment_id ; ?> <!--order no--></td>
				
				
				<!-- Product categories -->
				<td>
				<?php 
					$categoryIds = $product->getCategoryIds();
					$cat_count = count($categoryIds);
					$y = 1;
					foreach ($categoryIds as $categoryid){
						//echo $categoryid;
						$category = $objectManager->create('Magento\Catalog\Model\Category')->load($categoryid);
						echo $category->getName();
						if($cat_count==$y){
							echo '.';
						}else{
							echo ',';
						}
						$y++;
					}
				?>
				
				</td>
				
				<!-- Gross Price Amt(INR) -->
				<td>
				<?php echo $item_price = $item->getPrice(); ?>
				<?php
					// echo '<pre>';
					// print_r($item->getData());
					// echo '</pre>';
				?>
				
				</td>
				
				
				<?php
					$returnArray = array();
					$productid = $item->getItemId();
					$sellerOrder = $objectManager->create('Webkul\Marketplace\Model\Orders')->getCollection()
									->addFieldToFilter('order_id', $orderId);
									
					if(!empty($sellerOrder)){
						//echo 'in loop';
						foreach ($sellerOrder as $info) {
							//print_r($info->getData());
							$sellerid = $info->getSellerId();
							if ($sellerid > 0) {
								$customer = $objectManager->get('Magento\Customer\Model\Customer'
								)->load($sellerid);
								if ($customer) {
														
									$returnArray = [];
									$returnArray['seller_name'] = $customer->getName();
									$returnArray['seller_gst'] = $customer->getGstNumber();
									$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
									$returnArray['id'] = $customer->getId();
								}
								
							}
						}
					}
				?>
				
				<!-- Seller code -->
				
					<td> <?php	echo $sellerid;	?> </td>
					<!-- Seller name -->				
					<td>
						<?php 
						if($sellerid != 0){ 
						 echo $returnArray['seller_name'];
						}
						?> 
					
					</td>

					<!-- Seller address -->
					<td><?php if($sellerid != 0){ echo $customer->getCompanyAdd();  } ?> </td>
					
					<?php //echo '<pre>'; print_r($customer->getData()); echo '</pre>';?>
					
					<?php 
						$seller_default_billing = $customer->getDefaultBilling();
						$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory'); 
						$sell_customer=$customerFactory->create();
						$sell_customer->load($sellerid);// load customer by using ID
						//$data= $sell_customer->getAddresses();
						$addresses = $sell_customer->getAddresses();
						if(count($addresses)) {
						  foreach($addresses as $addresse) {
							$address_id = $addresse->getId();
							if($seller_default_billing == $address_id ){
								$seller_country = $addresse->getCountryId();
								$seller_city = $addresse->getCity();
								$seller_state =  $addresse->getRegion();
								$seller_pincode =  $addresse->getPostcode();
								
							}
						  }
						}
					?>
					<td>
					
					<?php if($sellerid != 0){  echo $seller_city; } ?>
					</td>	<!-- Seller City -->
					<td><?php  if($sellerid != 0){ echo $seller_state; } ?></td>	<!-- Seller State -->
					<td><?php  if($sellerid != 0){ echo $seller_pincode; } ?></td>	<!-- Seller pincode -->
				

				<?php
					if (strlen($product_desc) > 25) {
						$trimstring = substr($product_desc, 0, 25). '.... ';
					} else {
						$trimstring = $product_desc;
					}
					// echo $trimstring;
					//Output : Lorem Ipsum is simply dum [readmore...][1]
				?>
				
				<td><?php echo $trimstring; ?> </td> <!-- Mat. Descrip -->
				<!--Tax-->				
				<td><?php echo $tax_amount = $item->getTaxAmount();?></td>
				<!-- shipping -->				
				<td><?php echo $base_shipping_amount; ?></td>
				<?php
					$shipping_tax = ($base_shipping_amount*5)/100;
				?>				
				<td><?php echo $shipping_tax; ?></td> <!-- shipping tax -->
				<!-- Total -->				
				<td><?php echo ($item_price +$tax_amount); ?></td>
				
				<!-- Commission -->
				<?php $custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); ?>
				<?php $admin_commission = $custom_helpers->custom_admin_commission($order_increment_id); ?>
				
				<td><?php echo $admin_commission; ?></td>
				<?php
					// tax rate on commission 18%
					$comission_percentage = 18;
					$comission_tax = ($admin_commission*$comission_percentage)/100;
				?>
				<td><?php echo $comission_tax ?></td>
				<?php
					// tds on commission 10%
					$tds_percentage = 10;
					$comission_tds = ($admin_commission*$tds_percentage)/100;
				?>
				
				<td><?php echo $comission_tds; ?></td>
				
				<?php $commission_total = $admin_commission +$comission_tax +$comission_tds; ?>				
				<td><?php echo $commission_total; ?></td>	
				<!--<td>Collection TDS</td>-->
				<?php $method=$orderDatamodel1->getPayment()->getMethod();  ?>
				<!-- Credit card charges 
				cashondelivery
				razorpay-->
				<?php
					$razorpay_id = "";
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					$sql = "SELECT * FROM `custom_order_razorpay` where order_id=$order_id";
					$result = $connection->fetchAll($sql);
					
					foreach($result as $row)
					{
					 $razorpay_id = $row['razorpay'];
					 $response = $row['response_details'];
					}

					$result=json_decode($response);
					if(!empty($result)){
						$fee=$result->fee;
						$tax=$result->tax;
						$razor_amount=$fee+$tax;
					}
				?>
				<!-- credit card charges -->
				<td>
				<?php if($method == 'razorpay'){ echo $fee;  }else { echo 'n/a'; }?></td>
				<!-- Credit card tax -->
				<td><?php if($method == 'razorpay'){echo $tax;}else { echo 'n/a'; }?></td>
				<!-- Total credit card expenses -->
				<td><?php if($method == 'razorpay'){echo $razor_amount;}else { echo 'n/a'; }?></td>	
				
				<?php
				// Base subtotal of order's 2%
				$cod_charge_percentage = 2;
				$cod_charge = ($order_basesubtotal*$cod_charge_percentage)/100;
				// Cod tax_amount
				$cod_tax_percentage = 18;
				$cod_tax = ($cod_charge*$cod_tax_percentage)/100;
				// total cod expenses
				$cod_expenses = $cod_charge+$cod_tax;
				?>
				
				
				<!-- COD charges -->
				<td><?php if($method == 'cashondelivery'){echo $cod_charge;}else { echo 'n/a'; }?></td>	
				<!-- COD tax -->
				<td><?php if($method == 'cashondelivery'){echo $cod_tax;}else { echo 'n/a'; }?> </td>	
				<!-- Total COD expenses -->
				<td><?php if($method == 'cashondelivery'){echo $cod_expenses;}else { echo 'n/a'; }?></td>	
				
				<td><?php echo $order_status; ?></td>	<!-- Status of order -->
				<td><?php echo $orderDatamodel1->getCreatedAt(); ?></td>	<!-- Order date -->
				<?php 
				foreach($orderDatamodel1->getShipmentsCollection() as $shipment){
					/** @var $shipment Mage_Sales_Model_Order_Shipment */
					$shipping_created =  $shipment->getCreatedAt();
				}
				
				?>
				<!-- Shipping date -->
				<td>
					<?php 
						if( !empty($shipping_created)){
							echo $shipping_created;
						}else{
							echo 'n/a';
						}
							
					?>
				</td>
				<!-- Delivery date   6days plus Invoice -->			
				<td>
				<?php
					$today = date("Y-m-d",strtotime($date)); 
					$sixDays = date ("Y-m-d", strtotime ($today ."+6 days"));
					echo $sixDays; 
				?>	</td> 
				<?php
					//$return_date = 30 ;
					$today = date("Y-m-d",strtotime($date)); 
					$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
							
				?>
				
				<td><?php echo $RealizationDays ?> </td>	<!-- Realization date -->
				<td><?php echo $product->getData('ecom_length'); ?> </td>	<!-- Length -->
				<td><?php echo $product->getData('ecom_breadth'); ?></td>	<!-- Breadth -->	
				<td><?php echo $product->getData('ecom_height'); ?></td>	<!-- Height -->
				<td><?php echo $item->getWeight(); ?></td>	<!-- Absolute wt -->
				<?php 
				 $length = $product->getData('ecom_length');
				 $breadth = $product->getData('ecom_breadth');
				 $height = $product->getData('ecom_height');
				// value are in cm 
				 $logistic_weight = $length*$breadth*$height;
				 if(!empty($logistic_weight)){
					 if($logistic_weight>1){
						 $logistic_weight = $logistic_weight/5000;
					 }else{
						 $logistic_weight = 0;
					 }
				 }
				?>
				<!-- Logistic Wt  -->
				<td><?php echo $logistic_weight; ?></td>	
				<td><?php echo round($item->getDiscountPercent()).'%'; ?></td>	<!-- Discount % -->
				<!-- Reason of discount -->
				<td><?php echo 'n/a'; ?></td>	
				<td><?php echo round($item->getDiscountAmount()); ?></td>	<!-- Discount cost -->
				<td><?php if(!empty($coupon_code)){ echo $coupon_code; }else{ echo 'n/a';} ?></td>	<!-- Coupon used -->
				<td><?php if(!empty($coupon_code)){ echo $coupon_code; }else{ echo 'n/a';} ?></td> <!-- counpon amount -->
			</tr>

	<?php
			}	
	//$i++;
		}
		?>
		</tbody>

</table>
	<?php
		
	}catch(Exception $e){		
		echo $msg = 'Error : '.$e->getMessage();
	}
	

?>
