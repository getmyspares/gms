<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

use Magento\Framework\App\Bootstrap;

try{

    require __DIR__ . '/app/bootstrap.php';

    $params = $_SERVER;
    $bootstrap = Bootstrap::create(BP, $params);
    $obj = $bootstrap->getObjectManager();
    $obj->get('Magento\Framework\Registry')->register('isSecureArea', true);
    $appState = $obj->get('\Magento\Framework\App\State');
    $appState->setAreaCode('frontend');

    $products = $obj->create('\Magento\Catalog\Model\Product')->getCollection();
    $products->addAttributeToSelect(array('name'))
        ->addFieldTofilter('type_id','simple')
        ->addFieldToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
        ->addFieldToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
        ->load();

    echo "Total products count: " . $products->count() . "<hr>";
    echo "<pre>";
    if ($products->getSize() > 0) {
        foreach ($products as $product) {
            print_r($product->getData());
        }
    }

} catch (Exception $e) {
    echo $e->getMessage();
}