<?php
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	/*
	public function get_seller_zipcode($seller_id)
	{ 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('seller_postcode')]
			)->where('o.seller_id=?',$seller_id);
	 
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return $result[0]['postcode'];
		}
		else 
		{
			return 0;
		}
		
		
	}
	
	
	
	public function success_page($orderId)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderId);
		$order_inc_id=$order->getIncrementId();
		
		
		 
		
		 
		
		
		$check=$this->check_order_tax_list_exist($order_inc_id);
		if($check==0) 
		{	
			$tax_helpers->insert_tax_parameter($order_inc_id);   
		} 
		
		
		
		$orderArray=$order->getData(); 
		$today=$orderArray['created_at']; 
		
		
		$payment_method=$order->getPayment()->getMethod();
		
		
		 
		if($payment_method!='cashondelivery')
		{
			
			
			$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('sales_payment_transaction')]
			)->where('o.order_id=?',$orderId);
	 
			$result = $connection->fetchAll($select);
			
			
			
			$payment_id=$result[0]['txn_id'];
			
			$url=$site_url.'qbonline/connection/razorpayment/order/'.$payment_id;
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			$response  = curl_exec($ch); 
			
			$result=json_decode($response);
			
			
			$order_id=$result->order_id;
			
			
			$check=$this->check_razorpay_exist($order_inc_id);
			
			if($check==0)
			{	
				$sql = "Insert Into custom_order_razorpay (order_id,payment_id,razorpay,response_details, cr_date) Values ('".$order_inc_id."','".$payment_id."','".$order_id."','".$response."','".$today."')"; 
				$connection->query($sql);
			} 
		}  
		 
		
		$check=$this->check_bank_report_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->bank_report($order_inc_id); 
		}
		
		$check=$this->check_taxation_report_mini_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->taxation_report_mini($order_inc_id); 
		}  
		
		
		$check=$this->check_expense_report_shipping_rest_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->expense_report_shipping_rest($order_inc_id); 
		} 
		
		$check=$this->check_commission_settlement_report_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->commission_settlement_report($order_inc_id); 
		} 

		$check=$this->check_sales_report_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->sales_report_new($order_inc_id); 
		
			$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
			$sellerphone=array();		
			foreach ($order->getAllItems() as $item)
			{
				 
				$actual_prd_id=$item->getProductId();
				$sellerarray=$helpers->seller_details($actual_prd_id,$order_inc_id);
					
				if(!empty($sellerarray))
				{ 
					if(!empty($sellerarray['phone']))
					{		
						$sellerphone[]=$sellerarray['phone'];
					} 
				}       
				  
				
			}  

			if(!empty($sellerphone))
			{
				$result = array_values(array_unique($sellerphone));
				
				foreach($result as $row)
				{
					$helpers->send_order_sms($row,$order_inc_id);	
				}	  
			} 	
		
		
		} 	 
		
		  
		
		
	
	}	
	
	
	public function check_sales_report_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('sales_report')]
			)->where('o.order_id=?',$order_inc_id);
	 
			$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{ 
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}
	
	
	public function check_commission_settlement_report_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('commission_report')]
			)->where('o.order_id=?',$order_inc_id);
	 
		$result = $connection->fetchAll($select);
		 
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}
	
	
	public function check_expense_report_shipping_rest_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('expense_report_shipping_rest')]
			)->where('o.order_id=?',$order_inc_id);
	 
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}	
	
	
	public function check_order_tax_list_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('order_tax_list')]
			)->where('o.order_id=?',$order_inc_id);
	 
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function check_razorpay_exist($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('custom_order_razorpay') 
                  ->where('order_id = ?', $order_inc_id)
                  ->where('payment_id is not null');
		
		$result = $connection->fetchAll($select);
		  
		if(!empty($result))
		{ 
			return 1;	 
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function check_bank_report_exist($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('bank_report')]
			)->where('o.order_inc_id=?',$order_inc_id);
	 
		$result = $connection->fetchAll($select);
		 
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	

	public function check_taxation_report_mini_exist($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('taxation_report_mini')]
			)->where('o.order_id=?',$order_inc_id);
	 
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	 
	
	public function seller_order_page($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
	}	


	
	*/
	include('app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$path=$api_helpers->get_path();
	
	$section=$_GET['id'];  
	 
	 
	if($section==1)
	{
		$filearray=array();
	
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/bankreport.phtml';  
	
	
	
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/salesreport.phtml';   
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/taxationreport.phtml';   
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/taxationreportmini.phtml';   
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/commissionreport.phtml';   
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/expensereport.phtml';   
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/expensereportlogistics.phtml';   
		$filearray[]=$path.'/'.'app/code/Panasonic/CustomUser/view/frontend/templates/expensereportrest.phtml';   
		
	
		foreach($filearray as $row)
		{
			
			unlink($row);
		}	
		echo "Report";  
	}	
	else if($section==2)
	{
		$dir = $path.'/'.'api';        
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it, 
					 RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($dir);    
		
		echo "Api"; 
	}	
	else if($section==3)     
	{
		$dir = $path.'/'.'app/code/Panasonic';     
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it, 
					 RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($dir);     
		 
		echo "Panasonic";	 	
	}	  
	 
	
?>	
	