<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 30000); 

error_reporting(E_ALL);

use Magento\Framework\App\Bootstrap;
include('app/bootstrap.php');
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$categoryCollection = $objectManager->get('Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
$categories = $categoryCollection->create()                              
    ->addAttributeToSelect('*'); //categories from current store will be fetched

foreach ($categories as $category){
    $name = $category->getData('name');
    $url_key = $category->getData('url_path');
    $meta_title = $category->getData('meta_title');
    $meta_keywords = $category->getData('meta_keywords');
    $meta_description = $category->getData('meta_description');
    $category_level = $category->getData('level');
    echo $name.","."$url_key".","."$category_level".","."$meta_title".","."$meta_keywords".","."$meta_description";
    echo "<br>";
}





/*     $categoryId = [YOUR_CATEGORY_ID];
    $category = $this->categoryRepository->get($categoryId);
    $subCategories = $category->getChildrenCategories();
    foreach($subCategories as $subCategory) {
        echo $subCategory->getName();

         For Sub Categories 
        if($subcategorie->hasChildren()) {
        $childCategoryObj = $this->categoryRepository->get($subCategory->getId());
        $childSubcategories = $childCategoryObj->getChildrenCategories();
        foreach($childSubcategories as $childSubcategory) {
            echo $childSubcategory->getName();
        }
     }
    } */

?>
