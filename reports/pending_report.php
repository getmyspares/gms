<?php require_once('../reports/pending_report_data.php'); ?>
<?php 
	
	$columns = getColumns(); 
	if( isset($_GET['export_data']) ){
		exportData($_GET);
	}
	//print_r( $_GET );	
	$resultsDatas = getResultData($_GET); 
	
	//print_r($resultsDatas ); exit;
	$results = $resultsDatas['datas'];
	$paging  = $resultsDatas['paging'];
?>
<style>
.exportbu 
{
	width:21% !important;
}
</style>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
  <link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
  <link rel="stylesheet" href="/reports/assets/datepicker.css">
  <link rel="stylesheet" href="/reports/assets/skin.css">
  <link rel="stylesheet" href="/reports/assets/style.css">
</head>

<body class="skin-blue">
<div class="wrapper">
	
  <div class="content-wrapper">
    <section class="content">
			
    <div class="box box-default">
		<div class="box-body" style="padding:4px;">
		  <div class="row">
			 
			<div class="col-md-12">
				<form action="/reports/pending_report.php?k=<?php echo time(); ?>" id="filter_user" method="get" autocomplete="off" novalidate="novalidate">
					
					<div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID FROM</label>
							<input type="text" name="order_id" placeholder="000000000" value="<?php echo @$_GET['order_id']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID TO</label>
							<input type="text" name="order_id_to" placeholder="000000000" value="<?php echo @$_GET['order_id_to']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>From Date</label>
							<input name="from_date" type="text" value="<?php echo @$_GET['from_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>End Date</label>
							<input name="to_date" type="text" value="<?php echo @$_GET['to_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
						<div style="clear:both">&nbsp;</div>
					<div class="col-md-2  exportbu" >
					  <label style="width:100%;">&nbsp;</label>
					  <input type="hidden" value="<?php echo time(); ?>" name="key" />
					  <input type="hidden" value="<?php echo isset($_GET['areatype']) ? $_GET['areatype']:''; ?>" name="areatype" />
					  <input type="hidden" value="<?php echo isset($_GET['seller_id']) ? $_GET['seller_id']:''; ?>" name="seller_id" />
					  
					  <button name="search" type="submit" class="btn bg-navy">Search</button>
					  <button name="reset" type="button" onclick="location.href='/reports/pending_report.php<?php echo $append; ?>'" class="btn bg-navy">Reset</button>
					  <button name="export_data" type="submit" class="btn bg-navy">Export</button>				  
					</div>
					
					<div style="clear:both">&nbsp;</div>
					<div class="col-sm-4">
						Per Page :
						<select name="perpage" onchange="this.form.submit()">
							<option <?php echo $_GET['perpage']==15 ? 'selected':""; ?> value="15">15</option> 
							<option <?php echo $_GET['perpage']==30 ? 'selected':""; ?> value="30">30</option> 
							<option <?php echo $_GET['perpage']==50 ? 'selected':""; ?> value="50">50</option>
							<option <?php echo $_GET['perpage']==100 ? 'selected':""; ?> value="100">100</option>
							<option <?php echo $_GET['perpage']==200 ? 'selected':""; ?> value="200">200</option>
						</select>
					</div>
					<div class="col-sm-8">
						<ul class="pagination pull-right" style="margin: 10px 0;">
							<?php echo $paging; ?>
						</ul>
					</div>
			
				</form>
			</div>
		  </div>
		</div>
      
	    <div class="box">
            <div class="box-body">
				
			
			
              <table class="table table-bordered table-striped" style="font-size:13px;">
                <thead>
					<tr>				  
					  <?php foreach($columns as $column){ ?>
					  <th><?php echo $column; ?></th>
					  <?php } ?>
					</tr>
                </thead>  
                <tbody>
					<?php foreach($results as $result){ ?>
						<tr>				  					  
						  <?php foreach($columns as $key => $column){ ?>
							<?php if($key=="order_date" || $key=='created_at'):?>
								<td title="" ><?php echo date("Y-m-d", (strtotime($result[$key])+19800)); ?></td>
							<?php elseif($key=="mobile_number"):?>
								<?php
									$order_increment_id = $result['increment_id'];
									$query1="SELECT `entity_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
									$entity_id = $connection->fetchOne($query1);
									$query="SELECT `telephone` FROM `sales_order_address` where address_type='shipping' and parent_id='$entity_id'";
									$telephone = $connection->fetchOne($query);
								?>
								<td><?php echo $telephone; ?></td>
							<?php else:?>
								<td title="<?php echo $result[$key]; ?>" ><?php echo $result[$key]; ?></td>
							<?php endif;?>
							<?php } ?>
					</tr>
					<?php } ?>
                </tbody>              
              </table>
            </div>
            
        </div>
        
        
	</div>	
    </section>
    
  </div>
</div>


<script src="/reports/assets/jquery.min.js"></script>
<script src="/reports/assets/bootstrap.min.js"></script>
<script src="/reports/assets/datepicker.js"></script>
<script>
$(function () {
	$('.datepicker').datepicker({
	  autoclose: true
	})
})
</script>
</body>
</html>
