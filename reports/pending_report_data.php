<?php
include('../app/bootstrap.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
error_reporting(0);
ini_set( "display_errors", 0);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$areaType = isset($_GET['areatype']) ? $_GET['areatype'] : 'backend';

$lUserId = 0;
	
function getResultData($request){
	global $connection;
	
	$query = createQueryString($request);
	$perpage    = isset($request['perpage']) ? $request['perpage'] : 15;
	$page       = isset($request['page']) ? $request['page'] : 1;
	$countTotal = $connection->query($query)->rowCount();
	$offset     = ($page*$perpage)-$perpage;
	$total_page = ceil($countTotal/$perpage);
	
	$getU = $request;
	unset($getU['page']);
	
	$pagingData = '';
	if($total_page > 1){
			
		if($page>1){
			$getU['page'] = 1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/pending_report.php?'.$str.'">First</a></li>';
			
			$getU['page'] = $page-1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/pending_report.php?'.$str.'">Previous</a></li>';
	    } 		
		
		if($total_page > 10){
		$end = ($page+10) <= $total_page ? ($page+10) : $total_page;
		$start =  $end - 10;
		}else{
			$end = $total_page;
			$start =  1;
		}
		
		for($i=$start;$i<=$end;$i++){ 
			$getU['page'] = $i;
			$str = http_build_query($getU, '', '&');
			$active = ($page==$i) ? ' active':'';
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/pending_report.php?'.$str.'">'.$i.'</a></li>';
		}
		
		if($page < $total_page){
			$getU['page'] = $page+1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/pending_report.php?'.$str.'">Next</a></li>';
			
			$getU['page'] = $total_page;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/pending_report.php?'.$str.'">Last</a></li>';
		}
	
    }
	$query." ORDER BY increment_id DESC LIMIT $offset,$perpage";
    $datas = $connection->fetchAll($query." ORDER BY increment_id DESC LIMIT $offset,$perpage");
	
	$return = [];
	$return['datas']  = $datas;
	$return['paging'] = $pagingData;
	return $return;	
}

function exportData($request){
	global $connection;
	$query = createQueryString($request);
	$datas = $connection->fetchAll($query.' ORDER BY increment_id DESC');
	$columns = getColumns();
	$csvHeader = array_values($columns);
	$fp = fopen('php://memory', 'w');
	fputcsv( $fp, $csvHeader,",");
	foreach ($datas as $data) {
		$insertArr = array();
		foreach($columns as $key => $column){
			if($key=='increment_id'){
				$insertArr[] = "\t".$data[$key];
			} elseif($key=='created_at') {
				$insertArr[] = date("Y-m-d", (strtotime($data[$key])+19800) ); 
			} elseif($key=='mobile_number') {
				$order_increment_id = $data['increment_id'];
				$query1="SELECT `entity_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
				$entity_id = $connection->fetchOne($query1);
				$query="SELECT `telephone` FROM `sales_order_address` where address_type='shipping' and parent_id='$entity_id'";
				$telephone = $connection->fetchOne($query);
				$insertArr[] = $telephone;
			} else {
				$insertArr[] = $data[$key];
			}
			
		}
		fputcsv($fp, $insertArr, ",");
	}
	$filename = 'Sales Pending Report-'.date('Y-m-d-H-i-s').'.csv';
	fseek($fp, 0);
	header('Content-Type: text/csv');
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header('Content-Disposition: attachment; filename="'.$filename.'";');
	fpassthru($fp);
	exit;  	
}


function createQueryString($request){
	global $areaType,$lUserId;
	$query = "SELECT *,
	(select GROUP_CONCAT(sku separator ',') from sales_order_item oi where `oi`.order_id=`og`.entity_id) as skus ,
	(select GROUP_CONCAT(name separator ',') from sales_order_item oi where `oi`.order_id=`og`.entity_id) as names ,
	(select GROUP_CONCAT(qty_ordered separator ',') from sales_order_item oi where `oi`.order_id=`og`.entity_id) as qtys
	FROM `sales_order_grid` og where status in ('pending_payment','canceled') ";
	$where = [];
	$whereStr = '';
	
	if( isset($request['search']) || isset($request['export_data']) ){
		
		if( isset($request['order_id']) && $request['order_id']!='' && isset($request['order_id_to']) && $request['order_id_to']!='' ){
			$where[] = "increment_id BETWEEN '".$request['order_id']."' AND '".$request['order_id_to']."'";
		}else{
			if( isset($request['order_id']) && $request['order_id']!='' ){
				$where[] = "increment_id LIKE '%".$request['order_id']."%'";
			}
			
			if( isset($request['order_id_to']) && $request['order_id_to']!='' ){
				$where[] = "increment_id LIKE '%".$request['order_id_to']."%'";
			}	
		}
		
		if( isset($request['from_date']) && $request['from_date']!='' ){
			$date = date('Y-m-d H:i:s',strtotime($_GET['from_date'])-19800);
			$where[] = "created_at > '$date'";
		}
		if( isset($request['to_date']) && $request['to_date']!='' ){
			$date = date('Y-m-d H:i:s',strtotime($_GET['to_date'])-19800);
			$where[] = "created_at < '$date'";
		}

	}
	
	if(!empty($where)){
		$whereStr = ' AND '.implode(' AND ', $where);
	}
	//echo $query.$whereStr; exit;
	return $query.$whereStr;
}

function getColumns(){
	$columns = array(
		'increment_id' => 'Order Num',
		'status' => 'Order Status',
		'base_grand_total' => 'Grand Total',
		'created_at' => 'Order Date',
		'customer_email' => 'Email',
		'shipping_name' => 'Shipping Name',
		'mobile_number' => 'Mobile',
		'skus' => 'Sku',
		'names' => 'Names',
		'qtys' => 'Qty'
	);
	return $columns;
}
