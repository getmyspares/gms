<?php
include('../app/bootstrap.php');
error_reporting(0);
ini_set( "display_errors", 0);
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();

?>
<?php

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
        $tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction'); 
        $api_helpers = $objectManager->create('Customm\Apii\Helper\Data');    
        $sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$sql = "Select * FROM om_bussiness_survey order by id desc"; 
		// $sql = "Select * FROM sales_report_new where order_id='000000896'"; 
		$results = $connection->fetchAll($sql);
		 

		echo '<style>
		@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap");
		.custom_wrap {
			font-family: "Open Sans", sans-serif;
		    width: 100%;
		    overflow: auto;
		    padding: 10px;
		    box-sizing: border-box;
		    border: 1px solid #cacaca;
		    border-radius: 4px;
		}
		.custom_wrap table {
		    border: 0px !important;
		    border-collapse: collapse !important;
		}
		.custom_wrap table th{
			border: 1px solid #7d7d7d !important;
			background:#8a8a8a;
			color:#fff;
			white-space:nowrap
		}
		.custom_wrap table td{
		    border-bottom: 1px solid #ccc !important;
		}
		</style>
		
		<table border="1"  id="example"> 
		<thead> 
		<tr>
			<th style="display:none;">ID</th> 
			<th>Name</th>
			<th>Email ID</th>
			<th>Mobile number</th>
			<th>Spare parts dealer of Products (Ref, MWO, TV, AC etc.)</th>
			<th>Brands of spare parts dealing in</th>
			<th>Where do you usually procure your spares from</th>
			<th>Difficult spares to get</th>
			<th>Would you buy spare parts online at the right price?</th>
			<th>reason</th>
			<th>Would you be interested in selling spare parts online?</th>
		</tr>      
		</thead>
		<tbody>
		';

		foreach($results as $row)
		{
			echo '
			<tr>
				<td style="display:none;">'.$row['id'].'</td>  
				<td>'.$row['name'].'</td> 
				<td>'.$row['email_id'].'</td> 
				<td>'.$row['mobile_number'].'</td> 
				<td>'.$row['dealer_of_product'].'</td> 
				<td>'.$row['dealing_in'].'</td> 
				<td>'.$row['procure_from'].'</td> 
				<td>'.$row['difficult_spares_to_get'].'</td> 
				<td>'.$row['like_to_buy_online'].'</td> 
				<td>'.$row['not_buy_online_reason'].'</td> 
				<td>'.$row['like_to_sell'].'</td> 
			</tr>';	 
						
		}  
		echo '
		</tbody>
		</table>
		
		';
		
		echo '
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>';
		
		echo '
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
		';
		echo '<script>
		$(document).ready( function () {
			$("#example").DataTable( {
				dom: "Bfrtip",
				buttons: [
					{ extend: "excel", text: "Export In Excel" }
				],
				"pageLength": 6,
				order:[[0,"desc"]]
			} );  
			$("table").wrap("<div class=custom_wrap></div>"); 
		} );
		</script>
		';	
		
?>