<?php
include('../app/bootstrap.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
error_reporting(1);
ini_set( "display_errors", 1);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url     = $storeManager->getStore()->getBaseUrl();
$resource     = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection   = $resource->getConnection();
$cusomterRepo = $objectManager->get('Magento\Customer\Model\Customer');


function corectDatewithouttime($date){
	if(empty($date)){return $date;}
	$date=date_create($date);
	return date_format($date,'d-m-Y');
}

	
function getResultData($request){
	global $connection;
	
	$query = createQueryString($request);
	$perpage    = isset($request['perpage']) ? $request['perpage'] : 15;
	$page       = isset($request['page']) ? $request['page'] : 1;
	$countTotal = $connection->query($query)->rowCount();
	$offset     = ($page*$perpage)-$perpage;
	$total_page = ceil($countTotal/$perpage);
	$getU = $request;
	unset($getU['page']);
	
	$pagingData = '';
	if($total_page > 1){
			
		if($page>1){
			$getU['page'] = 1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_gsttcs_data.php?'.$str.'">First</a></li>';
			
			$getU['page'] = $page-1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/om_gsttcs_data.php?'.$str.'">Previous</a></li>';
	    } 		
		
		if($total_page > 10){
		$end = ($page+10) <= $total_page ? ($page+10) : $total_page;
		$start =  $end - 10;
		}else{
			$end = $total_page;
			$start =  1;
		}
		
		for($i=$start;$i<=$end;$i++){ 
			$getU['page'] = $i;
			$str = http_build_query($getU, '', '&');
			$active = ($page==$i) ? ' active':'';
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_gsttcs_data.php?'.$str.'">'.$i.'</a></li>';
		}
		
		if($page < $total_page){
			$getU['page'] = $page+1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/om_gsttcs_data.php?'.$str.'">Next</a></li>';
			
			$getU['page'] = $total_page;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_gsttcs_data.php?'.$str.'">Last</a></li>';
		}
	
		}
	$today=date('Y-m-d');
	
	$query = "SELECT a.id,a.order_id,a.invoice_id,a.sales_details,b.transaction_completion_date FROM `gst_tcs_report` as a left join `om_settlement_report` as b on a.order_id=b.order_num where date(b.transaction_completion_date)<='$today' and report_type!='refund' GROUP BY  a.id,a.order_id,a.invoice_id,a.sales_details,b.transaction_completion_date";	
	$query =  $query." LIMIT $offset,$perpage";
	$datas = $connection->fetchAll($query);
	$report_data=[];
	foreach ($datas as $key => $value) {
		$resultarray=json_decode($value['sales_details']);
		$resultarray->orignal_invoice_date = corectDatewithouttime($resultarray->orignal_invoice_date);
		$resultarray->document_date = corectDatewithouttime($resultarray->document_date);
		if(!empty($resultarray->customer_gstin_number))
		{
			$is_customer_registered="Registered";		
		} else  
		{
			$is_customer_registered="";		
		}
		$product_qty=$resultarray->product_qty;	
		if($resultarray->product_name == "Shipping & Handling")
		{
			$product_qty=1;
		}
		if(!empty($value->order_id))
		{
			$order_id = $value->order_id;
			$sql = "SELECT * FROM `wk_rma` where increment_id = '$order_inc_id' and final_status in ('1','0') and settled_without_return='0'";
			$results = $connection->fetchAll($sql);
			if(!empty($results))
			{
				continue;
			}

		}
		$posting_date=0;
		$transaction_completion_date = $value['transaction_completion_date']; 
		$posting_date = date('Y-m-t',strtotime($transaction_completion_date));	
		$report_data[] = array(
			'invoice_no'=>$resultarray->invoice_id,
			'suppliers_gstin'=>$resultarray->seller_gst,
			'register_unregister_supplier'=>$resultarray->seller_register,
			'state_of_supplier'=>ucfirst($resultarray->seller_state),
			'gms_ecommerce_gstin'=>$resultarray->gms_ecom_gst,
			'state_of_e_commerce_gstin'=>$resultarray->ecom_state,
			'type_of_transaction'=>$resultarray->transection_type,
			'type_of_document'=>$resultarray->document_type,
			'state_of_customer'=>$resultarray->buyer_state,
			'customer_gstin'=>$resultarray->customer_gstin_number,
			'is_customer_registered'=>$is_customer_registered,
			'original_gstin_of_the_supplier'=>$resultarray->orignal_gst,
			'original_invoice_no'=>$resultarray->orignal_invoice,
			'original_invoice_date'=>$resultarray->orignal_invoice_date,
			'document_number_internal_invoice_no'=>$resultarray->document_no,
			'document_date'=>$resultarray->document_date,
			'posting_date'=>$posting_date,
			'place_of_supply'=>$resultarray->buyer_state,
			'type_of_supply'=>$resultarray->supply_type,
			'description_of_goods_services_supplied'=>$resultarray->product_name,
			'hsn_sac_code'=>$resultarray->product_hsn,
			'unit_of_measurement'=>$resultarray->unit_measurement,
			'number_of_units'=>$product_qty,
			'gross_value_of_supplies_made'=>$resultarray->gross_price_seller,
			'value_of_supplies_returned'=>$resultarray->value_supplier_return,
			'net_amount_liable_for_tcs'=>$resultarray->net_amount_lible_tcs,
			'igst_tcs_rate'=>$resultarray->igst_rate,
			'igst_amount_tcs'=>$resultarray->igst_gst_amount_tcs,
			'cgst_tcs_rate'=>$resultarray->cgst_rate,
			'cgst_amount_tcs'=>$resultarray->cgst_gst_amount_tcs,
			'sgst_tcs_rate'=>$resultarray->sgst_rate,
			'sgst_amount_tcs'=>$resultarray->sgst_gst_amount_tcs,
			'utgst_tcs_rate'=>$resultarray->utgst_rate,
			'utgst_amount_tcs'=>$resultarray->utgst_gst_amount_tcs,
			'total_gst_tcs_amount'=>$resultarray->total_gst_tcs,
			);
	}
	$return = [];
	$return['datas']  = $report_data;
	$return['paging'] = $pagingData;
	return $return;	
}

function exportData($request){
	global $connection;
	$query = createQueryString($request);
	$datas = $connection->fetchAll($query);
	$columns = getColumns();
	$csvHeader = array_values($columns);
	$fp = fopen('php://memory', 'w');
	fputcsv( $fp, $csvHeader,",");
	foreach ($datas as $data) {
		$resultarray=json_decode($data['sales_details']);
		$resultarray->orignal_invoice_date = corectDatewithouttime($resultarray->orignal_invoice_date);
		$resultarray->document_date = corectDatewithouttime($resultarray->document_date);
		if(!empty($resultarray->customer_gstin_number))
		{
			$is_customer_registered="Registered";		
		} else  
		{
			$is_customer_registered="";		
		}
		$product_qty=$resultarray->product_qty;	
		if($resultarray->product_name == "Shipping & Handling")
		{
			$product_qty=1;
		}
		if(!empty($resultarray->order_id))
		{
			$order_id = $resultarray->order_id;
			$sql = "SELECT * FROM `wk_rma` where increment_id = '$order_inc_id' and final_status in ('1','0') and settled_without_return='0'";
			$results = $connection->fetchAll($sql);
			if(!empty($results))
			{
				continue;
			}

		}
		$posting_date=0;
		$transaction_completion_date = $data['transaction_completion_date']; 
		$posting_date = date('Y-m-t',strtotime($transaction_completion_date));
		$insertArr = array(
			'invoice_no'=>"\t".$resultarray->invoice_id,
			'suppliers_gstin'=>$resultarray->seller_gst,
			'register_unregister_supplier'=>$resultarray->seller_register,
			'state_of_supplier'=>ucfirst($resultarray->seller_state),
			'gms_ecommerce_gstin'=>$resultarray->gms_ecom_gst,
			'state_of_e_commerce_gstin'=>$resultarray->ecom_state,
			'type_of_transaction'=>$resultarray->transection_type,
			'type_of_document'=>$resultarray->document_type,
			'state_of_customer'=>$resultarray->buyer_state,
			'customer_gstin'=>$resultarray->customer_gstin_number,
			'is_customer_registered'=>$is_customer_registered,
			'original_gstin_of_the_supplier'=>$resultarray->orignal_gst,
			'original_invoice_no'=>$resultarray->orignal_invoice,
			'original_invoice_date'=>$resultarray->orignal_invoice_date,
			'document_number_internal_invoice_no'=>$resultarray->document_no,
			'document_date'=>$resultarray->document_date,
			'posting_date'=>$posting_date,
			'place_of_supply'=>$resultarray->buyer_state,
			'type_of_supply'=>$resultarray->supply_type,
			'description_of_goods_services_supplied'=>$resultarray->product_name,
			'hsn_sac_code'=>$resultarray->product_hsn,
			'unit_of_measurement'=>$resultarray->unit_measurement,
			'number_of_units'=>$product_qty,
			'gross_value_of_supplies_made'=>$resultarray->gross_price_seller,
			'value_of_supplies_returned'=>$resultarray->value_supplier_return,
			'net_amount_liable_for_tcs'=>$resultarray->net_amount_lible_tcs,
			'igst_tcs_rate'=>$resultarray->igst_rate,
			'igst_amount_tcs'=>$resultarray->igst_gst_amount_tcs,
			'cgst_tcs_rate'=>$resultarray->cgst_rate,
			'cgst_amount_tcs'=>$resultarray->cgst_gst_amount_tcs,
			'sgst_tcs_rate'=>$resultarray->sgst_rate,
			'sgst_amount_tcs'=>$resultarray->sgst_gst_amount_tcs,
			'utgst_tcs_rate'=>$resultarray->utgst_rate,
			'utgst_amount_tcs'=>$resultarray->utgst_gst_amount_tcs,
			'total_gst_tcs_amount'=>$resultarray->total_gst_tcs,
			);	
		fputcsv($fp, $insertArr, ",");
	}
	$filename = 'Gst_Tcs_Report-'.date('Y-m-d-H-i-s').'.csv';
	fseek($fp, 0);
	header('Content-Type: text/csv');
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header('Content-Disposition: attachment; filename="'.$filename.'";');
	fpassthru($fp);
	exit;    	
}

function createQueryString($request){
	global $areaType,$lUserId;
	$today=date('Y-m-d');
	$query = "SELECT a.id,a.order_id,a.invoice_id,a.sales_details,b.transaction_completion_date FROM `gst_tcs_report` as a left join `om_settlement_report` as b on a.order_id=b.order_num where date(b.transaction_completion_date)<='$today' and report_type!='refund' GROUP BY  a.id,a.order_id,a.invoice_id,a.sales_details,b.transaction_completion_date";
	$whereStr="";
	return $query.$whereStr;
}

function getColumns(){
	$columns = array(
	'invoice_no'=>'Invoice No',
	'suppliers_gstin'=>"Supplier's GSTIN",
	'register_unregister_supplier'=>'Register/Unregister Supplier',
	'state_of_supplier'=>'State of supplier',
	'gms_ecommerce_gstin'=>'GMS E-Commerce GSTIN',
	'state_of_e_commerce_gstin'=>'State of E-Commerce GSTIN',
	'type_of_transaction'=>'Type of transaction',
	'type_of_document'=>'Type of Document',
	'state_of_customer'=>'State Of Customer',
	'customer_gstin'=>'Customer  Gstin',
	'is_customer_registered'=>'Is Customer  Registered',
	'original_gstin_of_the_supplier'=>'Original GSTIN of the Supplier',
	'original_invoice_no'=>'Original Invoice No',
	'original_invoice_date'=>'Original Invoice Date',
	'document_number_internal_invoice_no'=>'Document Number(Internal Invoice No.)',
	'document_date'=>'Document Date',
	'posting_date'=>'Posting Date',
	'place_of_supply'=>'Place of Supply',
	'type_of_supply'=>'Type of Supply',
	'description_of_goods_services_supplied'=>'Description of goods / services supplied',
	'hsn_sac_code'=>'HSN / SAC code',
	'unit_of_measurement'=>'Unit of measurement',
	'number_of_units'=>'Number of units',
	'gross_value_of_supplies_made'=>'Gross Value of Supplies Made',
	'value_of_supplies_returned'=>'Value of Supplies Returned',
	'net_amount_liable_for_tcs'=>'Net Amount liable for TCS',
	'igst_tcs_rate'=>'IGST TCS Rate',
	'igst_amount_tcs'=>'IGST Amount(TCS)',
	'cgst_tcs_rate'=>'CGST TCS Rate',
	'cgst_amount_tcs'=>'CGST Amount(TCS)',
	'sgst_tcs_rate'=>'SGST TCS Rate',
	'sgst_amount_tcs'=>'SGST Amount(TCS)',
	'utgst_tcs_rate'=>'UTGST TCS Rate',
	'utgst_amount_tcs'=>'UTGST Amount(TCS)',
	'total_gst_tcs_amount'=>'Total GST (TCS) Amount'
	); 
	return $columns;
}
?>

<?php 
	
	$columns = getColumns(); 
	if( isset($_GET['export_data']) ){
		exportData($_GET);
	}
	//print_r( $_GET );	
	
	$resultsDatas = getResultData($_GET); 

	$results = $resultsDatas['datas'];
	$paging  = $resultsDatas['paging'];
?>
<style>
.exportbu 
{
	width:21% !important;
}
</style>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
  <link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
  <link rel="stylesheet" href="/reports/assets/datepicker.css">
  <link rel="stylesheet" href="/reports/assets/skin.css">
  <link rel="stylesheet" href="/reports/assets/style.css">
</head>

<body class="skin-blue">
<div class="wrapper">
	
  <div class="content-wrapper">
    <section class="content">
			
    <div class="box box-default">
		<div class="box-body" style="padding:4px;">
		  <div class="row">
			 
			<div class="col-md-12">
				<form action="/reports/om_gsttcs_data.php?k=<?php echo time(); ?>" id="filter_user" method="get" autocomplete="off" novalidate="novalidate">
					 
					<div style="clear:both">&nbsp;</div>
					  <button name="export_data" type="submit" class="btn bg-navy">Export</button>				  
					</div>
					
					<div style="clear:both">&nbsp;</div>
					<div class="col-sm-4">
						Per Page :
						<select name="perpage" onchange="this.form.submit()">
							<option <?php echo $_GET['perpage']==15 ? 'selected':""; ?> value="15">15</option> 
							<option <?php echo $_GET['perpage']==30 ? 'selected':""; ?> value="30">30</option> 
							<option <?php echo $_GET['perpage']==50 ? 'selected':""; ?> value="50">50</option>
							<option <?php echo $_GET['perpage']==100 ? 'selected':""; ?> value="100">100</option>
							<option <?php echo $_GET['perpage']==200 ? 'selected':""; ?> value="200">200</option>
						</select>
					</div>
					<div class="col-sm-8">
						<ul class="pagination pull-right" style="margin: 10px 0;">
							<?php echo $paging; ?>
						</ul>
					</div>
			
				</form>
			</div>
		  </div>
		</div>
      
	    <div class="box">
            <div class="box-body">
              <table class="table table-bordered table-striped" style="font-size:13px;">
                <thead>
					<tr>				  
					  <?php foreach($columns as $column){ ?>
					  <th><?php echo $column; ?></th>
					  <?php } ?>
					</tr>
                </thead>  
                <tbody>
					<?php foreach($results as $result){ ?>
						<tr>				  					  
						  <?php foreach($columns as $key => $column){ ?>
								<td title="<?php echo $result[$key]; ?>" ><?php echo substr($result[$key],0,40); ?></td>
							<?php } ?>
					</tr>
					<?php } ?>
                </tbody>              
              </table>
            </div>
            
        </div>
        
        
	</div>	
    </section>
    
  </div>
</div>


<script src="/reports/assets/jquery.min.js"></script>
<script src="/reports/assets/bootstrap.min.js"></script>
<script src="/reports/assets/datepicker.js"></script>
<script>
$(function () {
	$('.datepicker').datepicker({
	  autoclose: true
	})
})
</script>
</body>
</html>



