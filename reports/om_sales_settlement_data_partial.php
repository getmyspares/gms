<?php
/******************************
TODO : Move all of the written function to helper file in OM modules

Please do not write new functions here use app/code/OM/GenerateReport/Helper/Reports/SalesReport instead @ritesh8nov2021

***************** */

include('../app/bootstrap.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
error_reporting(1);
ini_set( "display_errors", 1);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url     = $storeManager->getStore()->getBaseUrl();
$resource     = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection   = $resource->getConnection();
$cusomterRepo = $objectManager->get('Magento\Customer\Model\Customer');
$areaType = isset($_GET['areatype']) ? $_GET['areatype'] : 'backend';
$lUserId = 0;
$customerSession = $objectManager->get('Magento\Customer\Model\Session');
if($customerSession->isLoggedIn()) {
	$lUserId = $customerSession->getCustomer()->getId();
}

if(isset($_GET['seller'])){
	$lUserId = $_GET['seller'];
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */
function downloadInvoicesByOrderIds($request){
    global $objectManager;
		global $connection;
    $order_id_array=[];
		if(isset($request['from_date']) || isset($request['transaction_from_date']))
		{
			if(!empty($request['from_date']) || !empty($request['transaction_from_date']))
			{
				$query = createQueryString($request);
				$datas = $connection->fetchAll($query.' ORDER BY order_num DESC');
				foreach($datas as $result) {
						$order_inc_id = $result['order_num'];
						$order_idd = $result['order_id'];
						if ($result['report_type'] == "normal") {
								$rmastatus = hasRmadetail($order_inc_id);
								$hascreditmemo = getCreditNoteCreatedDate($order_idd);
								if (empty($rmastatus) && empty($hascreditmemo)) {
										$order_id_array[] = $order_idd;
								}
						} else
						{
										$order_id_array[] = $order_idd;
						}
				}
				$export_invoice = $objectManager->create('OM\Rewrite\Helper\ExportInvoice');
				$export_invoice->downloadInvoicesByOrderIds($order_id_array);

			} else 
			{
				echo "date filters are neccasary to download invoices";
				die();
			}
		} else 
		{
			echo "date filters are neccasary to download invoices";
			die();
		}
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */
function getResultData($request){
	global $connection;
	$query = createQueryString($request);
	$perpage    = isset($request['perpage']) ? $request['perpage'] : 15;
	$page       = isset($request['page']) ? $request['page'] : 1;
	$countTotal = $connection->query($query)->rowCount();
	$offset     = ($page*$perpage)-$perpage;
	$total_page = ceil($countTotal/$perpage);
	
	$getU = $request;
	unset($getU['page']);
	
	$pagingData = '';
	if($total_page > 1){
			
		if($page>1){
			$getU['page'] = 1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sales_settlement_partial.php?'.$str.'">First</a></li>';
			
			$getU['page'] = $page-1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/om_sales_settlement_partial.php?'.$str.'">Previous</a></li>';
	    } 		
		
		if($total_page > 10){
		$end = ($page+10) <= $total_page ? ($page+10) : $total_page;
		$start =  $end - 10;
		}else{
			$end = $total_page;
			$start =  1;
		}
		
		for($i=$start;$i<=$end;$i++){ 
			$getU['page'] = $i;
			$str = http_build_query($getU, '', '&');
			$active = ($page==$i) ? ' active':'';
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sales_settlement_partial.php?'.$str.'">'.$i.'</a></li>';
		}
		
		if($page < $total_page){
			$getU['page'] = $page+1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/om_sales_settlement_partial.php?'.$str.'">Next</a></li>';
			
			$getU['page'] = $total_page;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sales_settlement_partial.php?'.$str.'">Last</a></li>';
		}
	
    }
	$query." ORDER BY order_num DESC LIMIT $offset,$perpage";
    $datas = $connection->fetchAll($query." ORDER BY order_num DESC LIMIT $offset,$perpage");
	
	$return = [];
	$return['datas']  = $datas;
	$return['paging'] = $pagingData;
	return $return;	
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */
function exportData($request){
	global $connection;
	global $objectManager;
	
	$query = createQueryString($request);
	$datas = $connection->fetchAll($query.' ORDER BY order_num DESC');
	
	$report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');
	$columns = $report_send_helper->getSettlementColumns();
	$csvHeader = array_values($columns);
	$fp = fopen('php://memory', 'w');
	fputcsv( $fp, $csvHeader,",");
	foreach ($datas as $data) 
	{
		if($data['report_type'] == "normal")
		{
			$order_inc_id=$data['order_num'];
			$order_idd=$data['order_id'];
			$rmastatus = hasRmadetail($order_inc_id);			
			if(!empty($rmastatus))
			{	
				continue;
			}
			$hascreditmemo = getCreditNoteCreatedDate($order_idd);
			if(!empty($hascreditmemo))
			{
				continue;	
			}
		}
		$insertArr = array();
		$insertArr=$report_send_helper->generateSettlementReportRow($data);
		fputcsv($fp, $insertArr, ",");
	}
	$filename = 'Sales Settlement Report Partial-'.date('Y-m-d-H-i-s').'.csv';
	fseek($fp, 0);
	header('Content-Type: text/csv');
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header('Content-Disposition: attachment; filename="'.$filename.'";');
	fpassthru($fp);
	exit;    	
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */
function createQueryString($request){
	global $areaType,$lUserId;
	$query = 'SELECT * FROM om_settlement_report_partial';
	$where = [];
	$whereStr = '';
	
	if( isset($request['search']) || isset($request['export_data']) || isset($request['download_invoices']) ){
		
		if( isset($request['order_id']) && $request['order_id']!='' && isset($request['order_id_to']) && $request['order_id_to']!='' ){
			$where[] = "order_num BETWEEN '".$request['order_id']."' AND '".$request['order_id_to']."'";
		}else{
			if( isset($request['order_id']) && $request['order_id']!='' ){
				$where[] = "order_num LIKE '%".$request['order_id']."%'";
			}
			
			if( isset($request['order_id_to']) && $request['order_id_to']!='' ){
				$where[] = "order_num LIKE '%".$request['order_id_to']."%'";
			}	
		}
		
		if( isset($request['from_date']) && $request['from_date']!='' ){
			$date = date('Y-m-d H:i:s',strtotime($_GET['from_date'])-19800);
			$where[] = "order_date > '$date'";
		}
		
		if( isset($request['seller']) && $request['seller']!='' ){
			$where[] = "seller_code LIKE '".$request['seller']."'";
		}
		
		if( isset($request['to_date']) && $request['to_date']!='' ){
			$date = date('Y-m-d H:i:s',strtotime($_GET['to_date'])-19800);
			$where[] = "order_date < '$date'";
		}

		if( isset($request['transaction_from_date']) && $request['transaction_from_date']!='' ){
			$date = date('Y-m-d',strtotime($_GET['transaction_from_date']));
			$where[] = "transaction_completion_date >= '$date'";
		}

		if( isset($request['transaction_to_date']) && $request['transaction_to_date']!='' ){
			$date = date('Y-m-d',strtotime($_GET['transaction_to_date']));
			$where[] = "transaction_completion_date <= '$date'";
		}

	}
	
	if($areaType=='frontend' && $lUserId > 0 ){
		$where[] = "seller_code LIKE '".$lUserId."'";
	}
		
	if(!empty($where)){
		$whereStr = ' WHERE '.implode(' AND ', $where) ."";
	}
	//  echo $query.$whereStr;
	return $query.$whereStr;
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */

function allSellerList(){
	global $connection,$cusomterRepo;
	$sql = "SELECT seller_id  FROM `marketplace_userdata` WHERE `is_seller` = 1";
	$results = $connection->fetchAll($sql);
	$retArr = array();	
	foreach($results as $result){
		$custid   = $result['seller_id'];
		$sql    = "Select value FROM customer_entity_varchar where entity_id='".$custid."' and attribute_id='173'";
		$companyName = $connection->fetchOne($sql);  
		if($companyName !=''){
			$retArr[$custid] = $companyName;
		}
	}
	return $retArr;
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */

function getCreditNoteCreatedDate($order_idd)
{
	global $connection;
	$sqleee = "SELECT `created_at` FROM `sales_creditmemo` where order_id='$order_idd'";  
	$created_at = $connection->fetchOne($sqleee);
	if(!empty($created_at))
	{
		return $created_at;
	}
	return 0;
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/Settlement instead
 */

function hasRmadetail($order_inc_id)
{
	global $connection;
	$status=false;
	$sql = "SELECT * FROM `wk_rma` where increment_id = '$order_inc_id' and final_status='3'";
	$results = $connection->fetchAll($sql);
	if(!empty($results))
	{
		$status = true;
	}
	return $status;

}
		
