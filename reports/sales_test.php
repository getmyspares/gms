<?php
ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


include('../app/bootstrap.php');
//include('config.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();

$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');


$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
?>
<?php

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
       
        $tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction'); 
        $api_helpers = $objectManager->create('Customm\Apii\Helper\Data');    
        $sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		$sql = "Select * FROM sales_report_new order by id desc"; 
		$sql = "Select * FROM sales_report_new where order_id='000000800'"; 
		//$sql = "Select * FROM sales_report_new order by DATE(created_at) desc"; 
		//$sql = "Select * FROM sales_report_new where id in (35,71)";  
        $results = $connection->fetchAll($sql);             
		 
		
		
		echo '<style>
		@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap");
		.custom_wrap {
			font-family: "Open Sans", sans-serif;
		    width: 100%;
		    overflow: auto;
		    padding: 10px;
		    box-sizing: border-box;
		    border: 1px solid #cacaca;
		    border-radius: 4px;
		}
		.custom_wrap table {
		    border: 0px !important;
		    border-collapse: collapse !important;
		}
		.custom_wrap table th{
			border: 1px solid #7d7d7d !important;
			background:#8a8a8a;
			color:#fff;
			white-space:nowrap
		}
		.custom_wrap table td{
		    border-bottom: 1px solid #ccc !important;
		}
		</style>
		
		<table border="1"  id="example"> 
		<thead> 
		<tr>
			<th style="display:none;">ID</th> 
			<th>Order Num</th>
			<th>Order Status</th>
			<th>Movement type</th>
			<th>Order Date</th>
			<th>Order Type</th>
			<th>Payment Ref Num</th>
			<th>Invoice Ref Num</th>
			<th>Invoice Date</th> 
			<th>Return request  Date</th> 
			<th>Return request reason</th> 
			<th>AWB</th> 
			<th>Pickup Date</th> 
			<th>Seller Code</th>
			<th>Seller Name</th>
			<th>Seller address</th>
			<th>Seller city</th>
			<th>Seller state</th>
			<th>Seller pincode</th>
			<th>Buyer ID</th>
			<th>Billing name</th>
			<th>Billing address</th>
			<th>Billing city</th>
			<th>Billing state</th>
			<th>Billing pincode</th>
			<th>Shipping name</th>
			<th>Shipping address</th>
			<th>Shipping city</th>
			<th>Shipping state</th>
			<th>Shipping pincode</th>
			<th>Product Code</th>
			<th>Product Category </th>
			<th>BP Category </th>
			
			
			<th>Item</th>
			<th>Item Description</th>
			<th>HSN code</th>
			<th>Product GST rate</th>
			<th>Product Comission Category</th> 
			<th>Product Comission(%)</th>
			<th>Quantity</th> 
			<th>Unit of measurement</th> 
			<th>Currency</th>
			<th>Gross Price Amt(INR)</th>
			<th>Discount%</th>
			<th>Type of Discount</th>
			<th>Coupon Number</th>
			<th>Coupon Amount</th>
			<th>Total Discount</th>
			<th>Net Sales value</th>
			<th>GST</th> 
			<th>Product Invoice value</th> 
			
			<th>Shipping GST rate</th> 
			<th>Shipping</th>
			<th>Shipping GST</th>
			<th>Shipping Invoice Total</th>
			<th>Total Invoice Value</th>
			<th>Comission</th>
			<th>Shipping Date</th>
			<th>Delivery Date</th>
			<th>Cancellation Date</th>
			
			
			 
		</tr>      
		</thead>
		<tbody>
		
		';
		
		foreach($results as $row)
		{
			
			$order_id = $row['order_id'];
			$order_inc_id = $row['order_id'];
			
			$sales_array=json_decode($row['sales_details']);
			/* echo "<pre>";
				print_r($sales_array);   
			echo "</pre>";  */
			//die();   
			
			$gstArray=$tax_helpers->get_order_gst_details($row['order_id']);
			
			$total_gst=$gstArray['total_gst'];
			
			/* 
			$today = strtotime(date('Y-m-d')); 
			$return_date = strtotime($sales_array->realization_days);
			
			$recognition='Due';		
			if($today > $return_date)
			{
				$recognition='Confirmed';		
			} 	 */
			
			/*  
			  */
			/* 
			$delivery_firstname=$custom_helpers->get_customer_order_adress($order_idd,'firstname','billing');
			$delivery_lastname=$custom_helpers->get_customer_order_adress($order_idd,'lastname','billing');
			$delivery_name=$delivery_firstname.' '.$delivery_lastname;
			$delivery_city = $custom_helpers->get_customer_order_adress($order_idd,'city','shipping');
			$delivery_state = $custom_helpers->get_customer_order_adress($order_idd,'region','shipping');
			$delivery_pincode = $custom_helpers->get_customer_order_adress($order_idd,'postcode','shipping');
			$delivery_country_id = $custom_helpers->get_customer_order_adress($order_idd,'country_id','shipping'); */
			 
			$street = '';   
				
			/* foreach($order->getShippingAddress()->getStreet() as $value){
				$street .= $value.', ' ;
			} */
			/* 
			$delivery_adress = $delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode.".";  */	
			$order_inc_id=$row['order_id'];
			
			
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id);
			
			$orderArray=$order->getData();
			$order_idd=$order->getId();
			/* echo "<pre>";
			print_r($orderArray);
			echo "</pre>";  */
			
			
			 
			if(!empty($orderArray['status']))
			{
				$status=$orderArray['status'];  
			}	 
			   
			$pickup_date=0;
			$delivered_date=0; 
			$movement_type='Forward';
			/*
			$select = $connection->select()
				->from('wk_rma') 
				->where('order_id = ?', $order_idd); 
							
			$result_return = $connection->fetchAll($select);
			$return_req_date='';
			$additional_info='';
			if(!empty($result_return))
			{
				$return_req_date=$result_return[0]['created_at'];
				$additional_info=$result_return[0]['additional_info'];
				$movement_type='Return';
				$returnarray=$sales_helpers->get_return_order_tracking($order_idd);
				//print_r($returnarray);
			}	
			*/
			$item_id=0;
			$product_id=$sales_array->product_code;
			foreach ($order->getAllItems() as $item)
			{
				$product_idd=$item->getProductId();
				
				
				if($product_idd==$product_id)
				{
					$item_id=$item->getId();
					break;
				}	
				
			} 
			
			$select = $connection->select()
				->from('wk_rma_items') 
				->where('item_id = ?', $item_id); 
			$rma_id=0;				
			$result_return_item = $connection->fetchAll($select);
			if(!empty($result_return_item))
			{
				$rma_id=$result_return_item[0]['rma_id'];
				
			}	
			
			$return_req_date='';
			$additional_info='';
			
			if($rma_id!=0)
			{
				$select = $connection->select()
				->from('wk_rma') 
				->where('rma_id = ?', $rma_id); 
							
				$result_return = $connection->fetchAll($select);
				
				if(!empty($result_return))   
				{
					$return_req_date=$result_return[0]['created_at'];
					$additional_info=$result_return[0]['additional_info'];
					$movement_type='Return'; 
					$array=$sales_helpers->get_return_order_tracking($order_idd,$product_id);
					
					//print_r($array);
					
					$pickup_date=$array['pickup_date'];
					$delivered_date=$array['delivered_date'];
				} 	
			}	  
			else
			{
				$array=$sales_helpers->get_forward_order_tracking($order_idd);
				$pickup_date=$array['pickup_date'];
				$delivered_date=$array['delivered_date'];
			}
				
			
				 	
			
			//$sales_array->pickup_date;
			$status=$api_helpers->api_get_order_tracking($sales_array->order_inc_id);  
			//$array=$sales_helpers->get_forward_order_tracking($order_id);
			
			
			$bp_category=$sales_array->bp_category;
			
			$bp_category_array=explode(",", $bp_category);
			/* echo "<pre>"; 
				print_r($bp_category_array);  
			echo "</pre>";  */
			
			/*
			$functionalArray=array(3,5,6,8);
			$toolArray=array(90);
			$accessoriesArray=array(4);
			$universalArray=array(7);
			
			$product_category_s='';
			
			foreach($bp_category_array as $row_name)
			{
				$cat_name=$row_name;
				
				$sql_cat_id = "SELECT * FROM `catalog_category_entity_varchar` where  value='".$cat_name."' and  store_id='0'"; 
				$results_cat_id = $connection->fetchAll($sql_cat_id);     
				if(!empty($results_cat_id))  
				{
					$cat_id=$results_cat_id[0]['entity_id'];
					
					
					
					if (in_array($cat_id, $functionalArray)) 
					{
						$product_category_s='Functional Parts'; 
					}
					else if (in_array($cat_id, $toolArray)) 
					{
						$product_category_s='Tools and Equipments';
					}
					else if (in_array($cat_id, $accessoriesArray)) 
					{
						$product_category_s='Accessories';
					}
					else if (in_array($cat_id, $universalArray)) 
					{
						$product_category_s='Universal Parts';
					}
					
					
					
				}		
				
			}	
			*/
			if($row['type']==1)
			{	
				$shipping_rate=$sales_array->shipping_gst_rate;
				$shipping_base=$sales_array->shipping;
				//$shipping_invoice_total=$sales_array->shipping_invoice_total;
				

				$shipping_base=$sales_array->shipping;
				$shipping_gst=$sales_array->shipping_gst;
				$shipping_invoice_total=$sales_array->shipping_invoice_total;
				$total_invoice_value=$sales_array->total_invoice_value;
				
				$array=$sales_helpers->get_forward_order_tracking($order_idd);
				
					
				//print_r($array);
				
				$pickup_date=$array['pickup_date'];
				$delivered_date=$array['delivered_date'];
				$movement_type='Forward'; 
				$return_req_date='';
				$additional_info='';
				
				$awb=$tax_helpers->tax_get_order_awb($order_idd); 
				
			}
			else
			{
				
				$shipping_rate=$sales_array->shipping_gst_rate;
				$shipping_base=$sales_array->shipping;
				$shipping_invoice_total=$sales_array->shipping_invoice_total;
				
				$shipping_array=$custom_helpers->add_shipping_return_price($shipping_rate,$shipping_base);	
				//print_r($shipping_array);
				
				
				
				$shipping_base=$shipping_array['shipping_base'];
				$shipping_gst=$shipping_array['shipping_gst_amount'];
				$shipping_invoice_total=$shipping_array['total_shipping'];
				$total_invoice_value='';
				$array=$sales_helpers->get_return_order_tracking($order_idd,$product_id);
				$pickup_date=$array['pickup_date'];
				$delivered_date=$array['delivered_date'];
				$movement_type='Return';  
				
				$return_req_date='';
				$additional_info='';
				$select = $connection->select()
				->from('wk_rma') 
				->where('rma_id = ?', $rma_id); 
							
				$result_return = $connection->fetchAll($select);
				
				if(!empty($result_return))   
				{
					$return_req_date=$result_return[0]['created_at'];
					$additional_info=$result_return[0]['additional_info'];
				}
				
				$awb=$tax_helpers->tax_get_order_return_awbno($order_inc_id); 
				 
				 
			}		
			
			
			$sellerdetails=$tax_helpers->tax_get_seller_array($sales_array->seller_id);
			$seller_details=json_decode($sellerdetails);
			
			
			$seller_comp_nam=$seller_details->seller_comp_nam;
			$seller_comp_nam=str_replace(array('\'', '"'), '', $seller_comp_nam); 
			
			
			$product_category_s='';
			$sql_part = "Select * FROM catalog_product_entity_int where entity_id='".$sales_array->product_code."' and attribute_id='261'";
			$results_part = $connection->fetchAll($sql_part);  
			if(!empty($results_part))
			{	
				$product_type=$results_part[0]['value'];
				
				$product_category_s='';	
				if($product_type==234)
				{
					$product_category_s='Functional Parts';	
				}	
				else if($product_type==235)
				{
						
					$product_category_s='Universal Parts';	 
				}	
				else if($product_type==236)
				{
						
					$product_category_s='Accessories';	
				}	
				else if($product_type==1061)
				{
					$product_category_s='Tools and Equipments';		 
				}	  
			}
					
					
			if($sales_array->product_code=='17916')		
			{

				$product_category_s='Accessories';		
			}		
					
			echo '
			<tr>
				<td style="display:none;">'.$row['id'].'</td>  
				<td>'.$sales_array->order_inc_id.'</td> 
				<td>'.$status.'</td> 
				<td>'.$movement_type.'</td> 
				<td>'.$sales_array->order_date.'</td>
				<td>'.$sales_array->order_type.'</td>
				<td>'.$sales_array->payment_ref.'</td>
				<td>'.$sales_array->invoice_id.'</td>
				<td>'.$sales_array->invoice_date.'</td>
				<td>'.$return_req_date.'</td>
				<td>'.$additional_info.'</td> 
				<td>'.$awb.'</td>
				<td>'.$pickup_date.'</td>  
				<td>'.$sales_array->seller_id.'</td>
				<td>'.$seller_comp_nam.'</td>
				<td>'.$sales_array->seller_address.'</td>
				<td>'.$sales_array->seller_city.'</td>
				<td>'.$sales_array->seller_state.'</td>
				<td>'.$sales_array->seller_pincode.'</td>
				<td>'.$sales_array->buyer_id.'</td>
				<td>'.$sales_array->billing_name.'</td>
				<td>'.$sales_array->billing_address.'</td>
				<td>'.$sales_array->billing_city.'</td>
				<td>'.$sales_array->billing_state.'</td>
				<td>'.$sales_array->billing_pincode.'</td>
				<td>'.$sales_array->shipping_name.'</td>
				<td>'.$sales_array->shipping_address.'</td>
				<td>'.$sales_array->shipping_city.'</td>
				<td>'.$sales_array->shipping_state.'</td>
				<td>'.$sales_array->shipping_pincode.'</td>
				<td>'.$sales_array->product_code.'</td>
				<td>'.$sales_array->product_category.'</td>
				<td>'.$sales_array->bp_category.'</td> 
				
				
				<td>'.$sales_array->item_name.'</td>
				<td>'.$sales_array->item_description.'</td>
				<td>'.$sales_array->product_hsn.'</td>
				<td>'.$sales_array->product_gst_rate.'</td>
				<td>'.$product_category_s.'</td> 
				<td>'.$sales_array->product_admin_comm.'</td>
				<td>'.$sales_array->product_qty.'</td>
				<td>'.$sales_array->unit_measurement.'</td>
				<td>'.$sales_array->currency.'</td>
				<td>'.$sales_array->gross_price.'</td>
				<td>'.$sales_array->discount_per.'</td>
				<td>'.$sales_array->discount_type.'</td>
				<td>'.$sales_array->discount_amount.'</td>
				<td>'.$sales_array->coupon_number.'</td>
				<td>'.$sales_array->total_discount.'</td>
				<td>'.$sales_array->net_sale_value.'</td>
				<td>'.$sales_array->total_gst.'</td>
				<td>'.$sales_array->product_invoice_value.'</td>
				
				<td>'.$sales_array->shipping_gst_rate.'</td>
				<td>'.$shipping_base.'</td>
				<td>'.$shipping_gst.'</td>
				<td>'.$shipping_invoice_total.'</td>
				<td>'.$total_invoice_value.'</td>
				<td>'.$sales_array->comission.'</td>
				<td>'.$pickup_date.'</td>
				<td>'.$delivered_date.'</td>
				<td>'.$sales_array->cancel_date.'</td>
				
				
			</tr>';	 
						
		}  
		
		
				
		
		
		echo '
		</tbody>
		</table>
		
		';
		
		echo '
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>';
		
		echo '
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
		
		';
		
		echo '<script>
		$(document).ready( function () {
			$("#example").DataTable( {
				dom: "Bfrtip",
				buttons: [
					{ extend: "excel", text: "Export In Excel" }
				],
				"pageLength": 6,
				order:[[0,"desc"]]
			} );  
		
			$("table").wrap("<div class=custom_wrap></div>"); 
		 
		
		} );
		</script>
		
		 
		';	
		
?>