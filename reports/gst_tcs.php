<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include('../app/bootstrap.php');
//include('config.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();

$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');


$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();

function corectDate($objectManager,$date){
	$timezone = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface'); 
	$corrected_date = $timezone->date($date)->format('d-m-Y h:i:s a');
	return $corrected_date;
}

function corectDatewithouttime($objectManager,$date){
	if(empty($date)){return $date;}
	$date=date_create($date);
	return date_format($date,'d-m-Y');
}

?>
<style>
@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap");
.custom_wrap {
	font-family: "Open Sans", sans-serif;
	width: 100%;
	overflow: auto;
	padding: 10px;
	box-sizing: border-box;
	border: 1px solid #cacaca;
	border-radius: 4px;
}
.custom_wrap table {
	border: 0px !important;
	border-collapse: collapse !important;
}
.custom_wrap table th{
	border: 1px solid #7d7d7d !important;
	background:#8a8a8a;
	color:#fff;
	white-space:nowrap
}
.custom_wrap table td{
	border-bottom: 1px solid #ccc !important;
}
</style>
<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
$sql = "SELECT * FROM `gst_tcs_report` order by id asc";
$results = $connection->fetchAll($sql);          
?>
<table border="1"  id="example"> 
<thead>
<tr>
	<th>Invoice No</th>
	<th>Supplier's GSTIN</th>
	<th>Register/Unregister Supplier</th>
	<th>State of supplier</th>
	<th>GMS E-Commerce GSTIN</th>
	<th>State of E-Commerce GSTIN</th>
	<th>Type of transaction</th>
	<th>Type of Document</th>
	<th>State Of Customer</th>
	<th>Customer  Gstin</th>
	<th>Is Customer  Registered</th>
	<th>Original GSTIN of the Supplier</th>
	<th>Original Invoice No</th>
	<th>Original Invoice Date</th>
	<th>Document Number(Internal Invoice No.)</th>
	<th>Document Date</th>
	<th>Posting Date</th>
	<th>Place of Supply</th>
	<th>Type of Supply</th>
	<th>Description of goods / services supplied</th>
	<th>HSN / SAC code</th>
	<th>Unit of measurement</th>
	<th>Number of units</th>
	<th>Gross Value of Supplies Made</th>
	<th>Value of Supplies Returned</th>
	<th>Net Amount liable for TCS</th>
	<th>IGST TCS Rate</th>
	<th>IGST Amount(TCS)</th>
	<th>CGST TCS Rate</th>
	<th>CGST Amount(TCS)</th>
	<th>SGST TCS Rate</th>
	<th>SGST Amount(TCS)</th>
	<th>UTGST TCS Rate</th>
	<th>UTGST Amount(TCS)</th>
	<th>Total GST (TCS) Amount</th>
</tr>
</thead>
<tbody>
<?php
foreach($results as $row)
{
	$resultarray=json_decode($row['sales_details']);
	$type=$row['type'];
	$product_qty=$resultarray->product_qty;	
	$resultarray->orignal_invoice_date = corectDatewithouttime($objectManager,$resultarray->orignal_invoice_date);
	$resultarray->document_date = corectDatewithouttime($objectManager,$resultarray->document_date);
	if($resultarray->product_name == "Shipping & Handling")
	{
		$product_qty=1;
	}
	if(!empty($resultarray->customer_gstin_number))
	{
		$is_customer_registered="Registered";		
	} else  
	{
		$is_customer_registered="";		
	}
?>

<tr>
	<td><?php echo $resultarray->invoice_id; ?></td>
	<td><?php echo $resultarray->seller_gst; ?></td>
	<td><?php echo $resultarray->seller_register; ?></td>
	<td><?php echo ucfirst($resultarray->seller_state); ?></td>
	<td><?php echo $resultarray->gms_ecom_gst; ?></td>
	<td><?php echo $resultarray->ecom_state; ?></td>
	<td><?php echo $resultarray->transection_type; ?></td>
	<td><?php echo $resultarray->document_type; ?></td>
	<td><?php echo $resultarray->buyer_state; ?></td>
	<td><?php echo $resultarray->customer_gstin_number; ?></td>
	<td><?php echo $is_customer_registered; ?></td>
	<td><?php echo $resultarray->orignal_gst; ?></td>
	<td><?php echo $resultarray->orignal_invoice; ?></td>
	<td><?php echo $resultarray->orignal_invoice_date; ?></td>
	<td><?php echo $resultarray->document_no; ?></td>
	<td><?php echo $resultarray->document_date; ?></td>
	<td><?php echo $resultarray->posting_date; ?></td> 
 	
	 <td><?php echo $resultarray->buyer_state; ?></td>
	<td><?php echo $resultarray->supply_type; ?></td>
	<td><?php echo $resultarray->product_name; ?></td>
	<td><?php echo $resultarray->product_hsn; ?></td>
	<td><?php echo $resultarray->unit_measurement; ?></td>
	<td><?php echo $product_qty; ?></td> 
	<td><?php echo $resultarray->gross_price_seller; ?></td>
	<td><?php echo $resultarray->value_supplier_return; ?></td>
	<td><?php echo $resultarray->net_amount_lible_tcs; ?></td>
	
	<td><?php echo $resultarray->igst_rate; ?></td>
	<td><?php echo $resultarray->igst_gst_amount_tcs; ?></td>
	<td><?php echo $resultarray->cgst_rate; ?></td>
	<td><?php echo $resultarray->cgst_gst_amount_tcs; ?></td>
	<td><?php echo $resultarray->sgst_rate; ?></td>
	<td><?php echo $resultarray->sgst_gst_amount_tcs; ?></td>	
	<td><?php echo $resultarray->utgst_rate; ?></td>
	<td><?php echo $resultarray->utgst_gst_amount_tcs; ?></td>
	<td><?php echo $resultarray->total_gst_tcs; ?></td>
	
	
</tr>

<?php
}
?>
</tbody> 
</table>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<?php  $d =  date("Y-m-d");  ?>
<script>
	$(document).ready( function () {
		$("#example").DataTable( {
			dom: "Bfrtip",
			buttons: [
				{ extend: "excelHtml5",text: "Export In Excel", filename: function () {
									var date= "<?php echo $d; ?>";

								return  "GST TCS Reports_" + date ;
				} }, 
				
			],
			"pageLength": 6,
			order:[[0,"desc"]]  
		} ); 
		
		$("table").wrap("<div class=custom_wrap></div>"); 
		
	});
</script>


		
