
<?php

/******************************
TODO : Move all of the written function to helper file in OM modules : last Code clean up 3january2021

Please do not write new functions here use app/code/OM/GenerateReport/Helper/Reports/SalesReport instead .
 ***************** */

include('../app/bootstrap.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
error_reporting(0);
ini_set( "display_errors", 0);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$sales_report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');
$connection = $resource->getConnection();

$areaType = isset($_GET['areatype']) ? $_GET['areatype'] : 'backend';

$lUserId = 0;
$logged_in_seller = false;
$customerSession = $objectManager->get('Magento\Customer\Model\Session');

if($areaType=="frontend")
{
    $logged_in_seller = true;
}
if($customerSession->isLoggedIn()) {
	$lUserId = $customerSession->getCustomer()->getId();
	$Seller_name_session = $customerSession->getCustomer()->getName();
}
if(isset($_GET['seller_id'])){
	$lUserId = $_GET['seller_id'];
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/SalesReport instead
 */
function getResultData($request){
	global $objectManager,$areaType,$lUserId;
	$SalesReport = $objectManager->create('OM\GenerateReport\Helper\Reports\SalesReport');
	return $SalesReport->getResultData($request,$lUserId);
}

function downloadInvoice($to_date, $from_date){
    global $connection;
    global $objectManager;

    $export_invoice = $objectManager->create('OM\Rewrite\Helper\ExportInvoice');
    $export_invoice->downloadInvoices($to_date,$from_date);
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/SalesReport instead
 */
function getSalesReportSeller()
{
    global $objectManager;
    $SalesReport = $objectManager->create('OM\GenerateReport\Helper\Reports\SalesReport');
    return $SalesReport->getSalesReportSeller();
}

/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/SalesReport instead
 */
function exportData($request){
	global $objectManager,$areaType,$lUserId;
	$SalesReport = $objectManager->create('OM\GenerateReport\Helper\Reports\SalesReport');
	$SalesReport->exportData($request,$lUserId);
}


/**
 * @deprecated  use  OM/GenerateReport/Helper/Reports/SalesReport instead
 */
function createQueryString($request){
	global $objectManager,$areaType,$lUserId;
	$SalesReport = $objectManager->create('OM\GenerateReport\Helper\Reports\SalesReport');
	return $SalesReport->getResultData($request,$lUserId);
}

/* do not write new functions here this is not the right place , this file will be deleted and replaced with OM\GenerateReport\Helper\Reports\SalesReport */