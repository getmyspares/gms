<?php require_once('../reports/om_sales_settlement_data.php'); ?>
<?php 
	$report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');
	
	$columns = $report_send_helper->getSettlementColumns(); 
	if( isset($_GET['export_data']) ){
		exportData($_GET);
	}
	//print_r( $_GET );	
	
	$resultsDatas = getResultData($_GET); 

	$results = $resultsDatas['datas'];
	$paging  = $resultsDatas['paging'];
	$sellers = allSellerList();
?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
  <link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
  <link rel="stylesheet" href="/reports/assets/datepicker.css">
  <link rel="stylesheet" href="/reports/assets/skin.css">
  <link rel="stylesheet" href="/reports/assets/style.css">
  <style>
	 @media only screen and (max-width:1200px) and (min-width:600px){
         form#filter_user {
            display: block;
            flex-wrap: wrap;
            align-items: start;
            justify-content: space-between;
        }

        .col-md-6.exportbu.col-sm-6 {
            display: flex;
            width: 50% !important;
        }

        div[style="clear:both"] { 
            display: none;
        }

        form#filter_user .col-md-2 {width: 25%;float: left;}

        form#filter_user .col-md-2:nth-child(3) {
            float: right;
        }

        form#filter_user .col-md-2:nth-child(3) select.form-control.file-filter.required {
            height: 110px;
        }



        div[style="clear:both"] + .col-md-6.col-sm-6 > label {
            display: none;
        }

         div[style="clear:both"] + .col-sm-4 {
            margin-top: 15px;
        }
		form#filter_user .col-md-12.col-sm-12.exportbu {
			width: 100% !important;
		}
		table.table th {
			white-space: nowrap;
		}
    }
	.box.box-default .box-body {
            overflow-x: auto;
        }
	@media only screen and (max-width:600px){
		
		.box-body {
			width: 100%;
			overflow-x: auto;
		}

		form#filter_user > div {
			padding: 0;
			width: 100%;
		}

		table.table th {
			white-space: nowrap;
		}

		form#filter_user > div button {
			margin-bottom: 10px;
			margin-right: 10px;
		}

		form#filter_user > div.col-md-6.exportbu.col-sm-6, form#filter_user > div.col-md-12.col-sm-12.exportbu {
			width: 100% !important;
		}
	}
  </style>
</head>

<body class="skin-blue">
<div class="wrapper">
	
  <div class="content-wrapper">
    <section class="content">
			
    <div class="box box-default">
		<div class="box-body" style="padding:4px;">
		  <div class="row">
			 
			<div class="col-md-12">
				<form action="/reports/om_sales_settlement.php?k=<?php echo time(); ?>" id="filter_user" method="get" autocomplete="off" novalidate="novalidate">
					
					<div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID FROM</label>
							<input type="text" name="order_id" placeholder="000000000" value="<?php echo @$_GET['order_id']; ?>" class="form-control" />
						  </div>
						</div>
					</div>
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID TO</label>
							<input type="text" name="order_id_to" placeholder="000000000" value="<?php echo @$_GET['order_id_to']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2" id="s_area_div">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Seller</label>
							<select name="seller" class="form-control file-filter required" style="width: 100%;" tabindex="-1" aria-hidden="true">
							<option value=''>-- select Seller --</option> 
							<?php foreach($sellers as $sid => $seller){ ?>
								<option <?php echo ($sid==$_GET['seller']) ? 'selected':''; ?> value='<?php echo $sid; ?>'><?php echo $seller; ?></option>
							<?php } ?>
							</select>			
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>From Date</label>
							<input name="from_date" type="text" value="<?php echo @$_GET['from_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>End Date</label>
							<input name="to_date" type="text" value="<?php echo @$_GET['to_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
						<div style="clear:both">&nbsp;</div>
					<div class="col-md-12  col-sm-12  exportbu" >
					  <label style="width:100%;">&nbsp;</label>
					  <input type="hidden" value="<?php echo time(); ?>" name="key" />
					  <input type="hidden" value="<?php echo isset($_GET['areatype']) ? $_GET['areatype']:''; ?>" name="areatype" />
					  <?php /*<input type="hidden" value="<?php echo isset($_GET['seller']) ? $_GET['seller']:''; ?>" name="seller" />*/ ?>
					  <?php 
					  $append = '';
					  if(isset($_GET['areatype']) && $_GET['areatype']!=''){
						$append = '?areatype='.$_GET['areatype'].'&seller='.$_GET['seller'];
						echo "<style>#s_area_div{display:none;}</style>"; 
					  } 
					  ?>
					  
					  <button name="search" type="submit" class="btn bg-navy">Search</button>
					  <button name="reset" type="button" onclick="location.href='/reports/om_sales_settlement.php<?php echo $append; ?>'" class="btn bg-navy">Reset</button>
					  <button name="export_data" type="submit" class="btn bg-navy">Export</button>				  
					</div>
					
					<div style="clear:both">&nbsp;</div>
					<div class="col-sm-4">
						Per Page :
						<select name="perpage" onchange="this.form.submit()">
							<option <?php echo $_GET['perpage']==15 ? 'selected':""; ?> value="15">15</option> 
							<option <?php echo $_GET['perpage']==30 ? 'selected':""; ?> value="30">30</option> 
							<option <?php echo $_GET['perpage']==50 ? 'selected':""; ?> value="50">50</option>
							<option <?php echo $_GET['perpage']==100 ? 'selected':""; ?> value="100">100</option>
							<option <?php echo $_GET['perpage']==200 ? 'selected':""; ?> value="200">200</option>
						</select>
					</div>
					<div class="col-sm-8">
						<ul class="pagination pull-right" style="margin: 10px 0;">
							<?php echo $paging; ?>
						</ul>
					</div>
			
				</form>
			</div>
		  </div>
		</div>
      
	    <div class="box">
            <div class="box-body">
				
			
			
              <table class="table table-bordered table-striped" style="font-size:13px;">
                <thead>
						<tr>				  
						  <?php foreach($columns as $column){ ?>
						<th><?php echo $column; ?></th>
					  <?php } ?>
					</tr>
					</thead>  
				<tbody>
					<?php foreach($results as $result){ 
							if($result['report_type'] == "normal")
							{
								$order_inc_id=$result['order_num'];
								$order_idd=$result['order_id'];
								$rmastatus = hasRmadetail($order_inc_id);			
								if(!empty($rmastatus))
								{
									continue;
								}
								$hascreditmemo = getCreditNoteCreatedDate($order_idd);
								if(!empty($hascreditmemo))
								{
									continue;	
								}
							}
						?>
						<tr>				  					  
						  <?php foreach($columns as $key => $column){ ?>
							<?php if($key=="order_date" || $key=='invoice_date'):?>
								<td title="" ><?php echo date("Y-m-d", (strtotime($result[$key])+19800)); ?></td>
							<?php else:?>
								<td title="<?php echo $result[$key]; ?>" ><?php echo substr($result[$key],0,10); ?></td>
							<?php endif;?>
							<?php } ?>
					</tr>
					<?php } ?>
                </tbody>              
              </table>
            </div>
            
        </div>
        
        
	</div>	
    </section>
    
  </div>
</div>


<script src="/reports/assets/jquery.min.js"></script>
<script src="/reports/assets/bootstrap.min.js"></script>
<script src="/reports/assets/datepicker.js"></script>
<script>
$(function () {
	$('.datepicker').datepicker({
	  autoclose: true
	})
})
</script>
</body>
</html>
