<?php require_once('../reports/om_sales_data_generated.php'); ?>
<?php 
	
	$columns = getColumns(); 
	if( isset($_GET['export_data']) ){
		exportData($_GET);
	}	
	
	$resultsDatas = getResultData($_GET); 
	$results = $resultsDatas['datas'];

	$paging  = $resultsDatas['paging'];
?>
<style>
.exportbu 
{
	width:21% !important;
}
</style>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
  <link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
  <link rel="stylesheet" href="/reports/assets/datepicker.css">
  <link rel="stylesheet" href="/reports/assets/skin.css">
  <link rel="stylesheet" href="/reports/assets/style.css">
</head>

<body class="skin-blue">
<div class="wrapper">
	
  <div class="content-wrapper">
    <section class="content">
			
    <div class="box box-default">
		<div class="box-body" style="padding:4px;">
		  <div class="row">
			 
			<div class="col-md-12">
				<form action="/reports/om_sales_generated.php?k=<?php echo time(); ?>" id="filter_user" method="get" autocomplete="off" novalidate="novalidate">
					
					<div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID FROM</label>
							<input type="text" name="order_id" placeholder="000000000" value="<?php echo @$_GET['order_id']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID TO</label>
							<input type="text" name="order_id_to" placeholder="000000000" value="<?php echo @$_GET['order_id_to']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
								<label>Status</label>
								<select name="status" class="form-control file-filter required" style="width: 100%;" tabindex="-1" aria-hidden="true">
									<option value=''>-- select Status --</option> 
									<option <?php echo @$_GET['status']=='New' ? 'selected':''; ?> value='New'>New</option>
									<option <?php echo @$_GET['status']=='processing' ? 'selected':''; ?> value='processing'>Processing</option>
									<option <?php echo @$_GET['status']=='Shipped' ? 'selected':''; ?> value='shipped'>Shipped</option>
									<option <?php echo @$_GET['status']=='Delivered' ? 'selected':''; ?> value='delivered'>Delivered</option>		
									<option <?php echo @$_GET['status']=='closed' ? 'selected':''; ?> value='Refunded'>Refunded</option>
									<option <?php echo @$_GET['status']=='refudnandrefunded' ? 'selected':''; ?> value='refudnandrefunded'>Refund/Refunded</option>
								</select>
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>From Date</label>
							<input name="from_date" type="text" value="<?php echo @$_GET['from_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>End Date</label>
							<input name="to_date" type="text" value="<?php echo @$_GET['to_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
						
					<div class="col-md-2  exportbu" >
					  <label style="width:100%;">&nbsp;</label>
					  <input type="hidden" value="<?php echo time(); ?>" name="key" />
					  <button name="search" type="submit" class="btn bg-navy">Search</button>
					  <button name="reset" type="button" onclick="location.href='/reports/om_sales_generated.php'" class="btn bg-navy">Reset</button>
					  <button name="export_data" type="submit" class="btn bg-navy">Export</button>
					  
					</div>
					
				</form>
			</div>
		  </div>
		</div>
      
	    <div class="box">
            <div class="box-body">
				
			<div class="col-sm-12">
				<ul class="pagination pull-right" style="margin: 10px 0;">
					<?php echo $paging; ?>
				</ul>
			</div>
			
              <table class="table table-bordered table-striped" style="font-size:13px;">
                <thead>
					<tr>				  
					  <?php foreach($columns as $column){ 




					  	?>
					  <th><?php echo $column; ?></th>
					  <?php } ?>
					</tr>
                </thead>  
                <tbody>
					<?php foreach($results as $result){ 
						$result['gross_price_amt_inr']   =  round($result['gross_price_amt_inr'], 2,PHP_ROUND_HALF_DOWN); 						
						$result['net_sales_value']   =  round($result['net_sales_value'], 2,PHP_ROUND_HALF_DOWN); 						
						$result['gst']   =  round($result['gst'], 2);
						?>
					<tr>				  					  
						  <?php foreach($columns as $key => $column){ ?>
							<?php if($key=="part_code"):?>
								<?php
									$item_id = $result['item_id'];
									$query1="SELECT `product_id`  FROM `sales_order_item` WHERE `item_id` = '$item_id'";
									$entity_id = $connection->fetchOne($query1);
									$query="SELECT `value` FROM `catalog_product_entity_varchar` where attribute_id='197' and entity_id='$entity_id'";
									$item_model_number = $connection->fetchOne($query); 
								?>
							<td title="" ><?php echo $item_model_number; ?></td>
							<?php else:?>
								<td title="<?php echo $result[$key]; ?>" ><?php echo substr($result[$key],0,10); ?></td>
							<?php endif;?>
						  
							<?php } ?>

						
					</tr>
					<?php } ?>
                </tbody>              
              </table>
            </div>
            
        </div>
        
        
	</div>	
    </section>
    
  </div>
</div>


<script src="/reports/assets/jquery.min.js"></script>
<script src="/reports/assets/bootstrap.min.js"></script>
<script src="/reports/assets/datepicker.js"></script>
<script>
$(function () {
	$('.datepicker').datepicker({
	  autoclose: true
	})
})
</script>
</body>
</html>
