<?php 
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php require_once('../reports/om_sales_sellersettlement_data.php'); ?>
<?php 
	$report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');
	$columns = $report_send_helper->getSellerSettlementColumns();
	if( isset($_GET['export_data']) ){
		exportData($_GET);
	}
	//print_r( $_GET );	
	
	$resultsDatas = getResultData($_GET); 
	
	$tinfo = $resultsDatas['tdatas'][0];
	$results = $resultsDatas['datas'];

	$paging  = $resultsDatas['paging'];
?>
<style>
.exportbu 
{
	width:21% !important;
}
</style>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
  <link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
  <link rel="stylesheet" href="/reports/assets/datepicker.css">
  <link rel="stylesheet" href="/reports/assets/skin.css">
  <link rel="stylesheet" href="/reports/assets/style.css">
</head>

<body class="skin-blue">
<div class="wrapper">
	
  <div class="content-wrapper">
    <section class="content">
			
    <div class="box box-default">
		<div class="box-body" style="padding:4px;">
		  <div class="row">
			 
			<div class="col-md-12">
				<form action="/reports/om_sellersettle_sales.php?k=<?php echo time(); ?>" id="filter_user" method="get" autocomplete="off" novalidate="novalidate">
					
					<div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID FROM</label>
							<input type="text" name="order_id" placeholder="000000000" value="<?php echo @$_GET['order_id']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID TO</label>
							<input type="text" name="order_id_to" placeholder="000000000" value="<?php echo @$_GET['order_id_to']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
								<label>Status</label>
								<select name="status[]" multiple class="form-control file-filter required" style="width: 100%;" tabindex="-1" aria-hidden="true">
									<option value=''>-- select Status --</option> 
									<option <?php echo @in_array('New',$_GET['status']) ? 'selected':''; ?> value='New'>New</option>
									<option <?php echo @in_array('processing',$_GET['status']) ? 'selected':''; ?> value='processing'>Processing</option>
									<option <?php echo @in_array('shipped',$_GET['status']) ? 'selected':''; ?> value='shipped'>Shipped</option>
									<option <?php echo @in_array('delivered',$_GET['status']) ? 'selected':''; ?> value='delivered'>Delivered</option>		
									<option <?php echo @in_array('closed',$_GET['status']) ? 'selected':''; ?> value='Refunded'>Refunded</option>
									<option <?php echo @in_array('refudnandrefunded',$_GET['status']) ? 'selected':''; ?> value='refudnandrefunded'>Refund/Refunded</option>
								</select>
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>From Date</label>
							<input name="from_date" type="text" value="<?php echo @$_GET['from_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
					 
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>End Date</label>
							<input name="to_date" type="text" value="<?php echo @$_GET['to_date']; ?>" class="form-control datepicker" />
						  </div>
						</div>
					 </div>
						<div style="clear:both">&nbsp;</div>
					<div class="col-md-2  exportbu" >
					  <label style="width:100%;">&nbsp;</label>
					  <input type="hidden" value="<?php echo time(); ?>" name="key" />
					  <input type="hidden" value="<?php echo isset($_GET['areatype']) ? $_GET['areatype']:''; ?>" name="areatype" />
					  <input type="hidden" value="<?php echo isset($_GET['seller_id']) ? $_GET['seller_id']:''; ?>" name="seller_id" />
					  <?php 
					  $append = '';
					  if(isset($_GET['areatype'])){
						$append = '?areatype='.$_GET['areatype'].'&seller_id='.$_GET['seller_id'];
					  } 
					  ?>
					  
					  <button name="search" type="submit" class="btn bg-navy">Search</button>
					  <button name="reset" type="button" onclick="location.href='/reports/om_sellersettle_sales.php<?php echo $append; ?>'" class="btn bg-navy">Reset</button>
					  <button name="export_data" type="submit" class="btn bg-navy">Export</button>				  
					</div>
					
					<div class="col-md-4  exportbu" >
						<label>Total Order - <?php echo $tinfo['total_orders'];?></label><br />
						<label>Total Invoiced - <?php echo $tinfo['total_invoiced'];?></label><br />
						<label>Total Refunded - <?php echo $tinfo['total_refunded']?$tinfo['total_refunded']:0;?></label>
					</div>
					<div style="clear:both">&nbsp;</div>
					<div class="col-sm-4">
						Per Page :
						<select name="perpage" onchange="this.form.submit()">
							<option <?php echo $_GET['perpage']==15 ? 'selected':""; ?> value="15">15</option> 
							<option <?php echo $_GET['perpage']==30 ? 'selected':""; ?> value="30">30</option> 
							<option <?php echo $_GET['perpage']==50 ? 'selected':""; ?> value="50">50</option>
							<option <?php echo $_GET['perpage']==100 ? 'selected':""; ?> value="100">100</option>
							<option <?php echo $_GET['perpage']==200 ? 'selected':""; ?> value="200">200</option>
						</select>
					</div>
					<div class="col-sm-8">
						<ul class="pagination pull-right" style="margin: 10px 0;">
							<?php echo $paging; ?>
						</ul>
					</div>
			
				</form>
			</div>
		  </div>
		</div>
      
	    <div class="box">
            <div class="box-body">
				
			
			
              <table class="table table-bordered table-striped" style="font-size:13px;">
                <thead>
					<tr>				  
					  <?php foreach($columns as $column){ ?>
					  <th><?php echo $column; ?></th>
					  <?php } ?>
					</tr>
                </thead>  
                <tbody>

					<?php foreach($results as $result){ ?>
						<tr>				  					  
						  <?php foreach($columns as $key => $column){ ?>
							<?php if($key=="part_code"):?>
								<?php
									$item_id = $result['item_id'];
									$query1="SELECT `product_id`  FROM `sales_order_item` WHERE `item_id` = '$item_id'";
									$entity_id = $connection->fetchOne($query1);
									$query="SELECT `value` FROM `catalog_product_entity_varchar` where attribute_id='197' and entity_id='$entity_id'";
									$item_model_number = $connection->fetchOne($query); 
								?>
							<td title="" ><?php echo $item_model_number; ?></td>

							<?php elseif($key=="sub_order_num"):?>
							<?php 
								
								$order_increment_id = $result['order_increment_id'];
								$query1="SELECT `increment_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
								$entity_id = $connection->fetchAll($query1);
								if($orderIncrementId == $entity_id[0]['increment_id']){
									$count++;
								}
								else{
									$orderIncrementId = $entity_id[0]['increment_id'];
									$count = 1;
								}
							?>	

							<td title="" ><?php echo $order_increment_id.'-'.$count; ?></td>

							<?php elseif($key=="sales_return_confirmation_date"):?>
							<?php 
								$dateVal ='';
								$creditmemo_increment_id = $result['creditmemo_increment_id'];
								$order_increment_id = $result['order_increment_id'];
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
								$orderCollection = $objectManager->create('Magento\Sales\Model\Order'); 
								$order = $orderCollection->loadByIncrementId($order_increment_id);
								$orderId = $order->getId();
								if($creditmemo_increment_id){
									$query1="SELECT `created_at`  FROM `sales_creditmemo` WHERE `order_id` = '$orderId'";
									$dateData = $connection->fetchAll($query1);
									if($dateData){
									$dateVal = $dateData[0]['created_at']; 
									$createDate = new DateTime($dateVal);
									$strip = $createDate->format('Y-m-d');
								}	
							}
								else{
								$query1="SELECT `created_at`  FROM `sales_invoice` WHERE `order_id` = '$orderId'";
								$dateData = $connection->fetchAll($query1);	
								if($dateData){
									$dateVal = $dateData[0]['created_at']; 
									$createDate = new DateTime($dateVal);
									$strip = $createDate->format('Y-m-d');
								}
								}
							?>	
							<td title="" ><?php echo $strip; ?></td>

							<?php elseif($key=="transaction_completion_date"):?>
							<?php 
								$transactionDate = '';
								$order_increment_id = $result['order_increment_id'];
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
								$orderCollection = $objectManager->create('Magento\Sales\Model\Order'); 
								$order = $orderCollection->loadByIncrementId($order_increment_id);
								$orderId = $order->getId();
								$query="SELECT `transaction_completion_date`  FROM `om_settlement_report` WHERE `order_id` = '$orderId'";
								$trasactionDateData = $connection->fetchAll($query);
								if($trasactionDateData){
									$transactionDate = $trasactionDateData[0]['transaction_completion_date'];
								}
							?>	
							<td title="" ><?php echo $transactionDate; ?></td>

							<?php elseif($key=="expected_payment_date"):?>
							<?php 
								$paymentDate = '';
								$order_increment_id = $result['order_increment_id'];
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
								$orderCollection = $objectManager->create('Magento\Sales\Model\Order'); 
								$order = $orderCollection->loadByIncrementId($order_increment_id);
								$orderId = $order->getId();
								$query="SELECT `transaction_completion_date`  FROM `om_settlement_report` WHERE `order_id` = '$orderId'";
								$trasactionDateData = $connection->fetchAll($query);
								if($trasactionDateData){
									$transactionDate = $trasactionDateData[0]['transaction_completion_date'];
									$paymentDateData = new DateTime($transactionDate);
									$paymentDate = $paymentDateData->format('Y-m-d');
									$paymentDate = date('Y-m-d', strtotime($paymentDate. ' + 3 days'));
								}
							?>	
							<td title="" ><?php echo $paymentDate; ?></td>

							<?php elseif($key=="gst_on_comm_18_percent"):?>
							<?php 
								$comission = $result['comission'];
								$comissionGst = $comission*0.18;
							?>	
							<td title="" ><?php echo $comissionGst; ?></td>

							<?php elseif($key=="net_commission"):?>
							<?php 
								$comission = $result['comission'];
								$comissionGst = $comission*0.18;
								$netCommsion = $comission + $comissionGst;
							?>	
							<td title="" ><?php echo $netCommsion; ?></td>

							<?php elseif($key=="tds_on_pi_comission"):?>
							<?php 
								$comission = $result['comission'];
								$tdsComission = $comission*0.0375;
							?>	
							<td title="" ><?php echo $tdsComission; ?></td>

							<?php elseif($key=="gst_tcs_1_on_basic_sale_amt"):?>
							<?php 
								$netSalevalue = $result['net_sales_value'];
								$netValue = $netSalevalue*0.01;
							?>	
							<td title="" ><?php echo $netValue; ?></td>

							<?php elseif($key=="due_from_seller_to_pi"):?>
							<?php 
								$comission = $result['comission'];
								$comissionGst = $comission*0.18;
								$netCommsion = $comission + $comissionGst;
								$netSalevalue = $result['net_sales_value'];
								$netValue = $netSalevalue*0.01;
								$tdsComission = $comission*0.0375;
								$due = $netCommsion + $netValue - $tdsComission;

							?>	
							<td title="" ><?php echo $due; ?></td>

							<?php elseif($key=="amount_due_to_seller"):?>
								<?php 
								$comission = $result['comission'];
								$comissionGst = $comission*0.18;
								$netCommsion = $comission + $comissionGst;
								$netSalevalue = $result['net_sales_value'];
								$netValue = $netSalevalue*0.01;
								$tdsComission = $comission*0.0375;
								$gst = $result['gst'];
								$itemval = $netSalevalue + $gst;
								$amountDue = $itemval - $netCommsion - $netValue + $tdsComission;

							?>	
							
							<td title="" ><?php echo $amountDue;?></td>


							<?php elseif($key=="refunded_amount"):?>
							<?php 
								$itemRefunded = '';
								$creditId = '';
								$creditmemo_increment_id = $result['creditmemo_increment_id'];
								if($creditmemo_increment_id){
								$query1="SELECT `entity_id`  FROM `sales_creditmemo` WHERE `increment_id` = '$creditmemo_increment_id'";
								$entity_id = $connection->fetchAll($query1);

								$creditId = $entity_id[0]['entity_id'];
								if($creditId){
								$creditQuery = "SELECT `base_price` FROM `sales_creditmemo_item` WHERE `parent_id` = $creditId";
								$creditQueryResults = $connection->fetchAll($creditQuery);
								$itemRefunded = $creditQueryResults[$counter]['base_price'];
								$counter++;
							}
							}else{
								$counter = 0;
								$orderCreditId = $creditmemo_increment_id;
							}
							?>	
							<td title="" ><?php echo $itemRefunded; ?></td>


							<?php elseif($key=="order_date" || $key=='invoice_date' || $key=='shipping_date'):?>
							<td title="" ><?php echo date("Y-m-d", (strtotime($result[$key])+19800)); ?></td>
							<?php elseif($key=="mobile_number"):?>
								<?php
									$order_increment_id = $result['order_increment_id'];
									$query1="SELECT `entity_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
									$entity_id = $connection->fetchOne($query1);
									$query="SELECT `telephone` FROM `sales_order_address` where address_type='shipping' and parent_id='$entity_id'";
									$telephone = $connection->fetchOne($query);
								?>
								<td><?php echo $telephone; ?></td>

							<?php elseif($key=="order_comment"):
								/* get order commnets */
								$order_increment_id = $result['order_increment_id'];
								$query1="SELECT `entity_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
								$entity_id = $connection->fetchOne($query1);
								$query =  "SELECT `comment` FROM `sales_order_status_history` where parent_id='$entity_id'  and comment is not  null order by `created_at` desc limit 2";
								$comments = $connection->fetchAll($query);
								$comment_array = array();
								if(!empty($comments))
								{
									foreach ($comments as $comment) {
										$comment_array[] = $comment['comment'];
									}

								}
								$comments_string = implode(" | | ",$comment_array);

							?>
							<td><?php echo $comments_string; ?></td>
							<?php else:?>
								<td title="<?php echo $result[$key]; ?>" ><?php echo substr($result[$key],0,10); ?></td>
							<?php endif;?>
							<?php } ?>
					</tr>
					<?php } ?>
                </tbody>              
              </table>
            </div>
            
        </div>
        
        
	</div>	
    </section>
    
  </div>
</div>


<script src="/reports/assets/jquery.min.js"></script>
<script src="/reports/assets/bootstrap.min.js"></script>
<script src="/reports/assets/datepicker.js"></script>
<script>
$(function () {
	$('.datepicker').datepicker({
	  autoclose: true
	})
})
</script>
</body>
</html>
