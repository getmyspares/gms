<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include('../app/bootstrap.php');
//include('config.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();

$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
$environment="STAGING";
if($site_url="https://getmyspares.com" || $site_url="https://getmyspares.com/")
{
	$environment="LIVE";
}
?>
<?php

		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction'); 
        $api_helpers = $objectManager->create('Customm\Apii\Helper\Data');    
        $sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
        $sql = "Select * FROM settlement_report order by id desc";
        //$sql = "Select * FROM settlement_report where order_id='000001015'";
		$results = $connection->fetchAll($sql);    

		function checkifshippingbypassed($connection,$incorder_idd) 
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
			$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
			$sqleee = "Select entity_id FROM sales_order where increment_id='$incorder_idd'";  
			$order_id = $connection->fetchOne($sqleee);
			$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
			$couponCode = $order->getCouponCode();
			$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
			$rule = $saleRule->load($ruleId);
			$freeShippingCoupon = $rule->getSimpleFreeShipping();
			if($freeShippingCoupon){
				$sql = "Select * FROM sales_order_grid where increment_id='$incorder_idd' and ecom_order='No'";  
				$results = $connection->fetchAll($sql);
				$status = !empty($results) ? $results[0]['delivery_date']:0; 
				return $status;
			}
		}

		function getCreditNoteCreatedDate($order_idd)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$sqleee = "SELECT `created_at` FROM `sales_creditmemo` where order_id=$order_idd";  
			$created_at = $connection->fetchOne($sqleee);
			if(!empty($created_at))
			{
				return $created_at;
			}
			return 0;
		}

		
		function getOrderCreatedDate($order_idd)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$sqleee = "SELECT `created_at` FROM `sales_order` where entity_id=$order_idd";  
			$created_at = $connection->fetchOne($sqleee);
			if(!empty($created_at))
			{
				return $created_at;
			}
			return 0;
		}

		?>
		<style>
		@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap");
		.custom_wrap {
			font-family: "Open Sans", sans-serif;
		    width: 100%;
		    overflow: auto;
		    padding: 10px;
		    box-sizing: border-box;
		    border: 1px solid #cacaca;
		    border-radius: 4px;
		}
		.custom_wrap table {
		    border: 0px !important;
		    border-collapse: collapse !important;
		}
		.custom_wrap table th{
			border: 1px solid #7d7d7d !important;
			background:#8a8a8a;
			color:#fff;
			white-space:nowrap
		}
		.custom_wrap table td{
		    border-bottom: 1px solid #ccc !important;
		}
		</style>
		 
		<table border="1"  id="example"> 
		<thead>
		<tr>
			<th>Order Num</th>
			<th>Order Date</th>
			<th>Buyer Id</th>
			<th>Order Type</th>
			<th>Payment Id</th>
			<th>Invoice Ref Num</th>
			<th>Invoice Date</th>
			<th>Seller Code</th>
			<th>Seller Name</th>
			<th>Delivery Date</th>
			<th>Sales/Return confirmation Date</th>
			<th>Transaction Completion Date</th>
			<th>Basic Sale Amount(ex-GST)</th>
			<th>Nature of GST</th>
			<th>IGST/SGST/ UGST Amount</th>
			<th>Listed price</th>
			<th>Shipping charges(basic)</th>
			<th>Shipping charges(GST)</th>
			<th>Rate%</th>
			<th>IGST</th>
			<th>CGST</th>
			<th>SGST</th>
			<th>UTGST</th>
		    <th>Shipping charges(Total)</th>
			<th>Total sale (Invoice value)</th>
			<th>Amount received in Payment Gateway</th>
			<th>Payment Method</th>
			<th>Card Type</th>
			<th>Card Network</th>
			<th>Payment Gateway PG charges</th>
			<th>GST on PG charges</th>
			<th>Rate%</th>
			<th>IGST</th>
			<th>CGST</th>
			<th>SGST</th>
			<th>UTGST</th>
			<th>Payment Gateway charges invoice value</th>
			<th>TDS</th>
			<th>Payment Gateway Charges net of TDS</th>
			<th>Net Amount received in nodal account</th>
			<th>Nodal charges(monthly)</th>
			<th>Balance in nodal</th>
			
			<th>Logistic TDS</th>
			<th>GST TCS @1% on Supplies <br>by Logistic Co and <br>collected & to be <br>deposited by PI</th> 
			<th>Amount due to logistics</th>  
			
			<th>Balance in nodal</th>
			
			<th>Gross comm due</th>
			<th>GST on comm @ 18%</th>
			<!--<th>IGST</th>
			<th>CGST</th>
			<th>SGST</th>
			<th>UTGST</th>-->
			<th>Net commission</th>
			<th>GST TCS @1% on basic sale amt</th>
			<th>TDS on PI Comission</th>
			<th>Due from Seller to PI</th>
			<th>Amount due to PI</th>
			<th>Balance to be in nodal if paid to PI</th>
			<th>Amount due to seller</th>
			<th>Refunded Amount</th>
			<th>Refund Settlement Balance in Nodal </th>
		</tr> 
		</thead>
		<tbody>
		<?php
		foreach($results as $row)
		{
			$sales_confirmation_date="";
			$transaction_complete_modified="";
		
			$report_details=json_decode($row['details']);
			$order_inc_id=$row['order_id'];
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id);
			$orderArray=$order->getData();
			$order_idd=$order->getId();
			$awb=$tax_helpers->tax_get_order_awb($order_idd);
			 
			if(!empty($orderArray['status']))
			{
				$status=$orderArray['status'];  
			}	 
			$delivery_date=0; 

			if($row['type']=="normal")
			{
				$sql = "SELECT * FROM `wk_rma` where increment_id = '$order_inc_id' and final_status='3'";
				$results = $connection->fetchAll($sql);
				if(!empty($results))
				{
					continue;
				}
				$hascreditmemo = getCreditNoteCreatedDate($order_idd);
				if(!empty($hascreditmemo))
				{
					continue;	
				}
			}
			
			$array=$sales_helpers->getOrderTrackingOM($order_idd,$row['type']);
			
			if ($order->hasShipments()) 
			{
                $shipArray=$order->getShipmentsCollection()->getData();
				$shipping_date=$shipArray[0]['created_at'];
			}

			$pickup_date=$array['pickup_date'];   
			$delivery_date=$array['delivered_date'];
			$raz_cgst=0;
			$raz_sgst=0;
			$raz_igst=0;
			$raz_utgst=0;
			$raz_igst=$report_details->razor_pay_gst_igst+$report_details->razor_pay_gst_cgst+$report_details->razor_pay_gst_sgst+$report_details->razor_pay_gst_ugst;  
			
			$recognition_status='Non-recognised';
			$RealizationDays=0; 
			$settlement_due_date=0; 

			$bypassedshippingdeldate = checkifshippingbypassed($connection,$order_inc_id);
			if($bypassedshippingdeldate)
			{
				$delivery_date = $bypassedshippingdeldate;
			}; 
			if($delivery_date!=0) 
			{	
				$return_days = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
				$today = date("Y-m-d",strtotime($delivery_date)); 
				$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
				
				
				$settlement_due_date=date('Y-m-d', strtotime('next monday', strtotime($RealizationDays)));
				
				$todayy=date('Y-m-d'); 
				$today_time = strtotime($todayy);
				$expire_time = strtotime($RealizationDays);	
				
				if ($expire_time < $today_time)
				{
					$recognition_status='Recognised';
				}		
				
			 
			}
			
			/*
			$return_date=7;
			$settlement_due_date = date ("Y-m-d", strtotime ($delivery_date ."+".$return_date." days"));
			if($delivery_date==0)
			{
				
				$recognition_status='0';
				$settlement_due_date='0';	
			}	
			*/

	
		/*   echo "<pre>";
		print_r($report_details);
		echo "</pre>";    */
		//$payment_amount=number_format($report_details->payment_amount, 2, '.', '');
		$razorpay_gstt = $report_details->razor_pay_fee_amount;
		$razor_pay_gst_amountt = $report_details->razor_pay_gst_amount;
		
		if($razorpay_gstt==0)
		{
			$razorpay_gstrate=0;
		}
		else
		{		
			$razorpay_gstrate=($razor_pay_gst_amountt/$razorpay_gstt)*100; 
		}
		$razorpay_gstrate=number_format($razorpay_gstrate, 2, '.', '');
		
		
		$shipping_igst=round($report_details->shipping_igst);
		$shipping_cgst=round($report_details->shipping_cgst);
		$shipping_sgst=round($report_details->shipping_sgst);
		$shipping_ugst=round($report_details->shipping_ugst);
		
		$gst_comm_due_igst=0;
		$gst_comm_due_cgst=0;
		$gst_comm_due_sgst=0;
		$gst_comm_due_utgst=0;
		
		
		if($shipping_igst!=0)
		{
			$gst_comm_due_igst=$report_details->gst_gross_comm_due;
		}	
		else if($shipping_cgst!=0 && $shipping_sgst!=0)
		{
			$gst_gross_comm_due=$report_details->gst_gross_comm_due;
			$gst_gross_comm_due=$gst_gross_comm_due/2;
			$gst_comm_due_cgst=$gst_gross_comm_due;
			$gst_comm_due_sgst=$gst_gross_comm_due;
			
		}
		else if($shipping_cgst!=0 && $shipping_ugst!=0)
		{
			$gst_gross_comm_due=$report_details->gst_gross_comm_due;
			$gst_gross_comm_due=$gst_gross_comm_due/2;
			$gst_comm_due_cgst=$gst_gross_comm_due;
			$gst_comm_due_utgst=$gst_gross_comm_due;
			
		}
		$razorpay_charges_net_tds=$report_details->razor_pay_total_charge_amount-$report_details->razorpay_tds;
		$return=0;
		
		// $select = $connection->select()
		// 		->from('wk_rma') 
		// 		->where('order_id = ?', $order_idd); 
							
		// $result_return = $connection->fetchAll($select);
		
		$settlement_due_date=0;
		if($return==0)
		{
			if($delivery_date!=0)
			{	
				$return_days = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
				$today = date("Y-m-d",strtotime($delivery_date)); 
				$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
				$settlement_due_date=date('Y-m-d', strtotime('next monday', strtotime($RealizationDays)));	
			}
		}	
		
		$today_m = date('m');
		$today_d = date('m');
		
		
		$sellerdetails=$tax_helpers->tax_get_seller_array($report_details->seller_id);
		$seller_details=json_decode($sellerdetails);
		
		
		$seller_comp_nam=$seller_details->seller_comp_nam;
		$seller_comp_nam=str_replace(array('\'', '"'), '', $seller_comp_nam); 
		
		
		
		$cgst=$order->getCgstAmount();
		$sgst=$order->getSgstAmount();
		$igst=$order->getIgstAmount();
		$utgst=$order->getUtgstAmount();
		
		$razor_pay_gst_gst_rate=$report_details->razor_pay_gst_gst_rate;
		
		if($razor_pay_gst_gst_rate!=0)
		{
			$razor_pay_gst_gst_rate=18;	
		}	
		if($igst!='0.0000')
		{
			$gst_nature='IGST';	
		}
		else if($cgst!='0.0000' && $sgst!='0.0000')
		{
			$gst_nature='CGST/SGST';	
		}	
		else if($cgst!='0.0000' && $utgst!='0.0000')
		{
			$gst_nature='CGST/UTGST';	
		}
		
		$date1 = date('Y-m-d');

		// $refudncheck = $row['type']=="refund" ? true:false;
		$refudncheck = $row['type']=="refund" ? true:true;

		if($environment!="LIVE")
		{
			$refudncheck=true;
		}
		$not_to_show_old_format=true;
		$datecheck = "2020-09-01";
		$datecheck = strtotime($datecheck);
		$order_Created_date = getOrderCreatedDate($order_idd);
		$order_Created_date = strtotime($order_Created_date);
		if($RealizationDays!=0)
		{
			$transaction_complete_date=date('Y-m-d',strtotime($RealizationDays));
			$sales_confirmation_date =$transaction_complete_date; 
			$todayStamp = strtotime($transaction_complete_date);
			$numOfDays = date('t', $todayStamp);
			$base = strtotime('+'.$numOfDays.' days', strtotime(date('Y-m-01', $todayStamp)));
			$transaction_complete_modified = date('Y-m-25', $base);
			
			$datetocheck = strtotime("2020-10-01");
			
			if(strtotime($RealizationDays) >= $datetocheck)
			{
				$not_to_show_old_format=false;
				$day = date("d", strtotime($sales_confirmation_date));
				if($day<=15)
				{
					$transaction_complete_modified = date('Y-m-30',strtotime($sales_confirmation_date));
				}
				if($day>=16)
				{
					$day = date("d", strtotime($sales_confirmation_date));
					$transaction_complete_modified = date('Y-m-15', strtotime("+1 months", strtotime($sales_confirmation_date)));	
				}

			}

		} 
		if($row['type']=='refund')
		{
			$credir_note_date = getCreditNoteCreatedDate($order_idd);
			$return_days = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
			$today = date("Y-m-d",strtotime($credir_note_date));	
			$transaction_complete_modified = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
			$sales_confirmation_date = $credir_note_date;
		}

		if($datecheck>$order_Created_date && $row['type']!='refund' && $not_to_show_old_format)
		{
			$order_date_type="old_date_format";
			$transaction_complete_modified = $settlement_due_date;
		}


		$isnotrmainbetween=true;
		
		if($delivery_date!=0 || $refudncheck)
		{	
			if($RealizationDays!=0 || $refudncheck)
			{
				$date1 = date('Y-m-d');
				$date2 = date("Y-m-d", strtotime($RealizationDays));
				
				$dateTimestamp1 = strtotime($date1); 
				$dateTimestamp2 = strtotime($date2); 
				  
				$ret_m = date("m", strtotime($RealizationDays));
				$ret_d = date("m", strtotime($RealizationDays));
				  
				
				$dateTimestamp1 = strtotime($date1); 
				$dateTimestamp2 = strtotime($date2); 
				  
				// Compare the timestamp date  
				//if ($dateTimestamp1 <= $dateTimestamp2) 
				//if($today_m>=$ret_m && $today_d>=$ret_d)
				if($date1>=$date2 || $refudncheck)
				{    
					
					if($row['type']=='normal')
					{
						$sql = "SELECT * FROM `wk_rma` where increment_id = '$order_inc_id' and final_status='1'";
						$results = $connection->fetchAll($sql);
						if(!empty($results))
						{
							$isnotrmainbetween=false;
						}
					}
					if($isnotrmainbetween){ 
		?>
		<tr>
				<td><?php echo $row['order_id']; ?></td>
				<td><?php echo $report_details->order_date; ?></td>
				<td><?php echo $report_details->buyer_id; ?></td>
				<td><?php echo $report_details->order_type; ?></td>
				<td><?php echo $report_details->payment_id; ?></td>
				<td><?php echo $report_details->invoice_id; ?></td>
				<td><?php echo $report_details->invoice_date; ?></td>
				<td><?php echo $report_details->seller_id; ?></td>
				<td><?php echo $seller_comp_nam; ?></td>
				<td><?php echo $delivery_date; ?></td>
				<td><?php echo $sales_confirmation_date; ?></td>
				<td><?php echo $transaction_complete_modified; ?></td>
				<td><?php echo $report_details->basic_sale_amount; ?></td>
				<td><?php echo $gst_nature; ?></td> 
				<td><?php echo $report_details->total_gst; ?></td>
				<td><?php echo $report_details->listed_price; ?></td>
				<td><?php echo $report_details->shipping_basic; ?></td>
				<td><?php echo $report_details->shipping_gst; ?></td>
				<td><?php echo $report_details->shipping_gst_rate; ?></td>
				<td><?php echo $report_details->shipping_igst; ?></td>
				<td><?php echo $report_details->shipping_cgst; ?></td>
				<td><?php echo $report_details->shipping_sgst; ?></td>
				<td><?php echo $report_details->shipping_ugst; ?></td>
				<td><?php echo $report_details->total_shipping; ?></td>
				<td><?php echo $report_details->total_sales; ?></td>
				<td><?php echo $report_details->payment_gateway; ?></td>
				<td><?php echo $report_details->razor_method; ?></td>
				<td><?php echo $report_details->razor_card_type; ?></td>
				<td><?php echo $report_details->razor_network; ?></td>
				<td><?php echo $report_details->razor_pay_fee_amount; ?></td>
				<td><?php echo $report_details->razor_pay_gst_amount; ?></td>
			    <td><?php echo $razor_pay_gst_gst_rate; ?></td>
				<td><?php echo $raz_igst; ?></td>
				<td><?php echo $raz_cgst; ?></td>
				<td><?php echo $raz_sgst; ?></td>
				<td><?php echo $raz_utgst; ?></td> 
				<td><?php echo $report_details->razor_pay_total_charge_amount; ?></td>
				<td><?php echo $report_details->razorpay_tds; ?></td> 
				<td><?php echo $razorpay_charges_net_tds; ?></td> 
				<td><?php echo $report_details->net_amount_rec_nodal; ?></td> 
				<td><?php echo $report_details->nodal_charges; ?></td> 
			   <td><?php echo $report_details->nodal_balance; ?></td> 
				<td><?php echo $report_details->logistic_tds; ?></td> 
				<td><?php echo $report_details->shipping_gst_tcs; ?></td> 
				<td><?php echo $report_details->amount_due_to_logistic; ?></td> 
				
				<td><?php echo $report_details->balance_in_nodal; ?></td> 
				
				<td><?php echo $report_details->gross_comm_due; ?></td> 
				<td><?php echo $report_details->gst_gross_comm_due; ?></td> 
				
				<?php /*<td><?php echo $gst_comm_due_igst; ?></td> 
				<td><?php echo $gst_comm_due_cgst; ?></td> 
				<td><?php echo $gst_comm_due_sgst; ?></td> 
				<td><?php echo $gst_comm_due_utgst; ?></td> */?> 
				
				<td><?php echo $report_details->net_comm; ?></td> 
			   
				<td><?php echo $report_details->gst_tcs; ?></td>   
				<td><?php echo $report_details->tds_pi_comm; ?></td> 
				<td><?php echo $report_details->gross_remittance_pi; ?></td> 
				<td><?php echo $report_details->net_remittance_pi; ?></td> 
				<td><?php echo $report_details->balance_in_nodal_pi; ?></td> 
				<td><?php echo $report_details->remittance_to_seller; ?></td>
				<td><?php 
				if(isset($report_details->refunded_amount))
				{
					echo $report_details->refunded_amount; 
				}
				?></td> 
				<td><?php
				if(isset($report_details->refund_settlement_balance))
				{
					echo $report_details->refund_settlement_balance; 
				} 
				?></td> 
			</tr>    
	 	 
			
				  
		<?php			
			
			} 
		  }
		} 
		
		} 
	}	
		?>
		</body>
		</html> 
		
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

		
		
		<script>
		$(document).ready( function () {
			$("#example").DataTable( {
				dom: "Bfrtip",
				buttons: [
					{ extend: "excel", text: "Export In Excel" }
				],
				"pageLength": 100
			} );
		} );
		</script>
		
		
 
 
	
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		
		
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
		
		<?php  $d =  date("Y-m-d"); ?>
		 
		<script>
		$(document).ready( function () {
		

			$("#example").DataTable( {
				dom: "Bfrtip",
				buttons: [ 
					{ extend: "excelHtml5",text: "Export In Excel", filename: function () {
                 var date= "<?php echo $d; ?>";

               return   "Settlement_Reports_" + date ;
      } }, 
				],
				"pageLength": 6,
				order:[[0,"desc"]] 
			} );   

			$("table").wrap("<div class=custom_wrap></div>"); 
			
		} );
		</script>
		
		