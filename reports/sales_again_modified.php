<?php
include('../app/bootstrap.php');
error_reporting(0);
ini_set( "display_errors", 0);
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();

?>

<style>
		@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap");
		.custom_wrap {
			font-family: "Open Sans", sans-serif;
		    width: 100%;
		    overflow: auto;
		    padding: 10px;
		    box-sizing: border-box;
		    border: 1px solid #cacaca;
		    border-radius: 4px;
		}
		.custom_wrap table {
		    border: 0px !important;
		    border-collapse: collapse !important;
		}
		.custom_wrap table th{
			border: 1px solid #7d7d7d !important;
			background:#8a8a8a;
			color:#fff;
			white-space:nowrap
		}
		.custom_wrap table td{
		    border-bottom: 1px solid #ccc !important;
		}
		</style>
		
		<table border="1"  id="example"> 
		<thead> 
		<tr>
			<th style="display:none;">ID</th> 
			<th>Order Num</th>
			<th>Order Status</th>
			<th>Movement type</th>
			<th>Order Date</th>
			<th>Order Type</th>
			<th>Payment Ref Num</th>
			<th>Invoice Ref Num</th>
			<th>Invoice Date</th> 
			<th>Return request  Date</th> 
			<th>Return request reason</th> 
			<th>AWB</th> 
			<th>Pickup Date</th> 
			<th>Seller Code</th>
			<th>Seller Name</th>
			<th>Seller address</th>
			<th>Seller city</th>
			<th>Seller state</th>
			<th>Seller pincode</th>
			<th>Buyer ID</th>
			<th>Billing name</th>
			<th>Billing address</th>
			<th>Billing city</th>
			<th>Billing state</th>
			<th>Billing pincode</th>
			<th>Shipping name</th>
			<th>Shipping address</th>
			<th>Shipping city</th>
			<th>Shipping state</th>
			<th>Shipping pincode</th>
			<th>Product Code</th>
			<th>Product Category </th>
			<th>BP Category </th>
			<th>Item</th>
			<th>Item Description</th>
			<th>HSN code</th>
			<th>Product GST rate</th>
			<th>Product Comission Category</th> 
			<th>Product Comission(%)</th>
			<th>Quantity</th> 
			<th>Unit of measurement</th> 
			<th>Currency</th>
			<th>Gross Price Amt(INR)</th>
			<th>Discount%</th>
			<th>Type of Discount</th>
			<th>Coupon Number</th>
			<th>Coupon Amount</th>
			<th>Total Discount</th>
			<th>Net Sales value</th>
			<th>GST</th> 
			<th>Product Invoice value</th> 
			
			<th>Shipping GST rate</th> 
			<th>Shipping</th>
			<th>Shipping GST</th>
			<th>Shipping Invoice Total</th>
			<th>Total Invoice Value</th>
			<th>Comission</th>
			<th>Shipping Date</th>
			<th>Delivery Date</th>
			<th>Cancellation Date</th>
		</tr>      
		</thead>
		<tbody>
