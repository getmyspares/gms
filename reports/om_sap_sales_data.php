<?php
include('../app/bootstrap.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
error_reporting(0);
ini_set( "display_errors", 0);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$objectManager = $bootstraps->getObjectManager();
$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url=$storeManager->getStore()->getBaseUrl();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

function getResultData($request){
	global $connection;
	
	$query = createQueryString($request);	
	$perpage    = isset($request['perpage']) ? $request['perpage'] : 15;
	$page       = isset($request['page']) ? $request['page'] : 1;
	$countTotal = $connection->query($query)->rowCount();
	$offset     = ($page*$perpage)-$perpage;
	$total_page = ceil($countTotal/$perpage);
	
	$getU = $request;
	unset($getU['page']);
	
	$pagingData = '';
$pagingData = '';
	if($total_page > 1){
			
		if($page>1){
			$getU['page'] = 1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sap_sales.php?'.$str.'">First</a></li>';
			
			$getU['page'] = $page-1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/om_sap_sales.php?'.$str.'">Previous</a></li>';
	    } 		
		
		if($total_page > 10){
		$end = ($page+10) <= $total_page ? ($page+10) : $total_page;
		$start =  $end - 10;
		}else{
			$end = $total_page;
			$start =  1;
		}
		
		for($i=$start;$i<=$end;$i++){ 
			$getU['page'] = $i;
			$str = http_build_query($getU, '', '&');
			$active = ($page==$i) ? ' active':'';
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sap_sales.php?'.$str.'">'.$i.'</a></li>';
		}
		
		if($page < $total_page){
			$getU['page'] = $page+1;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button"><a href="/reports/om_sap_sales.php?'.$str.'">Next</a></li>';
			
			$getU['page'] = $total_page;
			$str = http_build_query($getU, '', '&');
			$pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sap_sales.php?'.$str.'">Last</a></li>';
		}
	
    }
	$query." ORDER BY order_increment_id DESC LIMIT $offset,$perpage";
    $datas = $connection->fetchAll($query." ORDER BY order_increment_id DESC LIMIT $offset,$perpage");
	
	$return = [];
	$return['datas']  = $datas;
	$return['paging'] = $pagingData;
	return $return;	
}

function exportData($request){
	global $connection;
	$query = createQueryString($request);
	$datas = $connection->fetchAll($query.' ORDER BY order_increment_id DESC');
	$columns = getColumns();
	$csvHeader = array_values($columns);
	$fp = fopen('php://memory', 'w');
	fputcsv( $fp, $csvHeader,",");
	foreach ($datas as $data) {
		$insertArr = array();
		foreach($columns as $key => $column){
			if($key=='order_increment_id' or $key=='invoice_increment_id'){
				$insertArr[] = "\t".$data[$key];
			} elseif($key=='order_date' || $key=='invoice_date' || $key=='shipping_date' || $key=='delivery_date' || $key=='cancellation_date' || $key=='return_request_date' || $key=='pickup_date' )
			{	if(!empty($data[$key]))
				{
					$insertArr[] = date("Y-m-d", strtotime($data[$key])); 
				} else 
				{
					$insertArr[] ="";
				}
			} elseif($key=='part_code')
			{
				$item_id = $data['item_id'];
				$query1="SELECT `product_id`  FROM `sales_order_item` WHERE `item_id` = '$item_id'";
				$entity_id = $connection->fetchOne($query1);
				$query="SELECT `value` FROM `catalog_product_entity_varchar` where attribute_id='197' and entity_id='$entity_id'";
				$item_model_number = $connection->fetchOne($query);
				$insertArr[]=$item_model_number;
			}else {
				$insertArr[] = $data[$key];
			}
			
		}
		fputcsv($fp, $insertArr, ",");
	}
	$filename = 'Sales Report-'.date('Y-m-d-H-i-s').'.csv';
	fseek($fp, 0);
	header('Content-Type: text/csv');
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header('Content-Disposition: attachment; filename="'.$filename.'";');
	fpassthru($fp);
	exit;    	
}

function createQueryString($request){
	$query = 'SELECT so.crm_res_id,so.sap_res_id,osr.*,oeas.`order_status`, oeas.`pickup_date`, oeas.`awb_number`, oeas.`delivery_date`
	FROM `om_sales_report` osr 
	JOIN om_ecom_api_status oeas ON osr.order_increment_id=oeas.increment_id 
	JOIN sales_order so ON osr.order_increment_id=so.increment_id ';
	$where = [];
	$whereStr = '';
	
	if( isset($request['search']) || isset($request['export_data']) ){
		
		if( isset($request['order_id']) && $request['order_id']!='' && isset($request['order_id_to']) && $request['order_id_to']!='' ){
			$where[] = "order_increment_id BETWEEN '".$request['order_id']."' AND '".$request['order_id_to']."'";
		}else{
			if( isset($request['order_id']) && $request['order_id']!='' ){
				$where[] = "order_increment_id LIKE '%".$request['order_id']."%'";
			}
			
			if( isset($request['order_id_to']) && $request['order_id_to']!='' ){
				$where[] = "order_increment_id LIKE '%".$request['order_id_to']."%'";
			}	
		}
		
		if( isset($request['status']) && $request['status']!='' ){
			if($request['status']=="refudnandrefunded")
			{
				$where[] = "order_status in ('Refund','Refunded')";
			}else {
				$where[] = "order_status LIKE '%".$request['status']."%'";
			}
		}
		if( isset($request['from_date']) && $request['from_date']!='' ){
			$date = date('Y-m-d H:i:s',strtotime($_GET['from_date'])-19800);
			$where[] = "order_date > '$date'";
		}
		if( isset($request['to_date']) && $request['to_date']!='' ){
			$date = date('Y-m-d H:i:s',strtotime($_GET['to_date'])-19800);
			$where[] = "order_date < '$date'";
		}
	}
	
	if(!empty($where)){
		$whereStr = ' WHERE '.implode(' AND ', $where) ." and osr.movement_type=oeas.movement_type";
	} else   {
		$whereStr = 'where osr.movement_type=oeas.movement_type';
	}
	//echo $query.$whereStr; exit;
	return $query.$whereStr;
}

function getColumns(){
	$columns = array(
		'order_increment_id' => 'Order Num',
		'order_status' => 'Order Status',
		'movement_type' => 'Movement type',
		'order_date' => 'Order Date',
		'order_type' => 'Order Type',
		'awb_number' => 'Awb Number',
		'payment_ref_num' => 'Payment Ref Num',
		'invoice_increment_id' => 'Invoice Ref Num',
		'invoice_date' => 'Invoice Date', 
		'return_request_date' => 'Return request  Date', 
		'return_request_reason' => 'Return request reason', 
		'seller_code' => 'Seller Code', 
		'pickup_date' => 'Pickup Date', 
		'seller_code' => 'Seller Code',
		'seller_name' => 'Seller Name',
		'seller_address' => 'Seller address',
		'seller_city' => 'Seller city',
		'seller_state' => 'Seller state',
		'seller_pincode' => 'Seller pincode',
		'buyer_id' => 'Buyer ID',
		'billing_name' => 'Billing name',
		'billing_address' => 'Billing address',
		'billing_city' => 'Billing city',
		'billing_state' => 'Billing state',
		'billing_pincode' => 'Billing pincode',
		'shipping_name' => 'Shipping name',
		'shipping_address' => 'Shipping address',
		'shipping_city' => 'Shipping city',
		'shipping_state' => 'Shipping state',
		'shipping_pincode' => 'Shipping pincode',
		'product_code' => 'Product Code',
		'product_category' => 'Product Category ',
		'bp_category' => 'BP Category ',
		'item' => 'Item',
		'item_description' => 'Item Description',
		'hsn_code' => 'HSN code',
		'product_gst_rate' => 'Product GST rate',
		'product_comission_category' => 'Product Comission Category', 
		'product_comission_percentage' => 'Product Comission(%)',
		'quantity' => 'Quantity', 
		'unit_of_measurement' => 'Unit of measurement', 
		'currency' => 'Currency',
		'gross_price_amt_inr' => 'Gross Price Amt(INR)',
		'discount_percentage' => 'Discount%',
		'type_of_discount' => 'Type of Discount',
		'coupon_number' => 'Coupon Number',
		'coupon_amount' => 'Coupon Amount',
		'total_discount' => 'Total Discount',
		'net_sales_value' => 'Net Sales value',
		'gst' => 'GST', 
		'product_invoice_value' => 'Product Invoice value', 		
		'shipping_gst_rate' => 'Shipping GST rate', 
		'shipping' => 'Shipping',
		'shipping_gst' => 'Shipping GST',
		'shipping_invoice_total' => 'Shipping Invoice Total',
		'total_invoice_value' => 'Total Invoice Value',
		'comission' => 'Comission',
		'shipping_date' => 'Shipping Date',
		'delivery_date' => 'Delivery Date',
		'cancellation_date' => 'Cancellation Date',
		'part_code'=>'Part Code',
		'crm_res_id' => 'CRM Order ID',	
		'sap_res_id' => 'SAP Order ID'
	);
	return $columns;
}
