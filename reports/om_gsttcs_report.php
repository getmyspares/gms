<?php
include('../app/bootstrap.php');

use  \Magento\Framework\App\Bootstrap;

$bootstraps = Bootstrap::create(BP, $_SERVER);
error_reporting(1);
ini_set("display_errors", 1);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$site_url     = $storeManager->getStore()->getBaseUrl();
$resource     = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection   = $resource->getConnection();
$cusomterRepo = $objectManager->get('Magento\Customer\Model\Customer');
$report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');


function getResultData($request)
{
	global $connection;

	$query = createQueryString($request);
	$perpage    = isset($request['perpage']) ? $request['perpage'] : 15;
	$page       = isset($request['page']) ? $request['page'] : 1;
	$countTotal = $connection->query($query)->rowCount();
	$offset     = ($page * $perpage) - $perpage;
	$total_page = ceil($countTotal / $perpage);
	$getU = $request;
	unset($getU['page']);

	$pagingData = '';
	if ($total_page > 1) {

		if ($page > 1) {
			$getU['page'] = 1;
			$str = http_build_query($getU, '', '&');
			$pagingData .= '<li class="paginate_button ' . $active . '"><a href="/reports/om_gsttcs_report.php?' . $str . '">First</a></li>';

			$getU['page'] = $page - 1;
			$str = http_build_query($getU, '', '&');
			$pagingData .= '<li class="paginate_button"><a href="/reports/om_gsttcs_report.php?' . $str . '">Previous</a></li>';
		}

		if ($total_page > 10) {
			$end = ($page + 10) <= $total_page ? ($page + 10) : $total_page;
			$start =  $end - 10;
		} else {
			$end = $total_page;
			$start =  1;
		}

		for ($i = $start; $i <= $end; $i++) {
			$getU['page'] = $i;
			$str = http_build_query($getU, '', '&');
			$active = ($page == $i) ? ' active' : '';
			$pagingData .= '<li class="paginate_button ' . $active . '"><a href="/reports/om_gsttcs_report.php?' . $str . '">' . $i . '</a></li>';
		}

		if ($page < $total_page) {
			$getU['page'] = $page + 1;
			$str = http_build_query($getU, '', '&');
			$pagingData .= '<li class="paginate_button"><a href="/reports/om_gsttcs_report.php?' . $str . '">Next</a></li>';

			$getU['page'] = $total_page;
			$str = http_build_query($getU, '', '&');
			$pagingData .= '<li class="paginate_button ' . $active . '"><a href="/reports/om_gsttcs_report.php?' . $str . '">Last</a></li>';
		}
	}
	$today = date('Y-m-d');
	$query =  $query . " LIMIT $offset,$perpage";
	$datas = $connection->fetchAll($query);
	$return = [];
	$return['datas']  = $datas;
	$return['paging'] = $pagingData;
	return $return;
}

function exportData($request)
{
	global $connection;
	global $objectManager;
	$query = createQueryString($request);
	$datas = $connection->fetchAll($query);
	$report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');
	$columns = $report_send_helper->getGstTcsReportColumns();
	$csvHeader = array_values($columns);
	$fp = fopen('php://memory', 'w');
	fputcsv($fp, $csvHeader, ",");
	foreach ($datas as $data) {
		$insertArr = array();
		$insertArr = $report_send_helper->generatGstTcsReportRow($data);
		fputcsv($fp, $insertArr, ",");
	}
	$filename = 'Gst_Tcs_Report-' . date('Y-m-d-H-i-s') . '.csv';
	fseek($fp, 0);
	header('Content-Type: text/csv');
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header('Content-Disposition: attachment; filename="' . $filename . '";');
	fpassthru($fp);
	exit;
}

function createQueryString($request)
{
	global $areaType, $lUserId;
	$query = "select * from om_gsttcs_report";
	$where = [];
	$whereStr = '';

	if (isset($request['search']) || isset($request['export_data'])) {

		if (isset($request['order_id']) && $request['order_id'] != '' && isset($request['order_id_to']) && $request['order_id_to'] != '') {
			$where[] = "order_inc_id BETWEEN '" . $request['order_id'] . "' AND '" . $request['order_id_to'] . "'";
		} else {
			if (isset($request['order_id']) && $request['order_id'] != '') {
				$where[] = "order_inc_id LIKE '%" . $request['order_id'] . "%'";
			}

			if (isset($request['order_id_to']) && $request['order_id_to'] != '') {
				$where[] = "order_inc_id LIKE '%" . $request['order_id_to'] . "%'";
			}
		}

		if (isset($request['invoice_id']) && $request['invoice_id'] != '' && isset($request['invoice_id_to']) && $request['invoice_id_to'] != '') {
			$where[] = "invoice_no BETWEEN '" . $request['invoice_id'] . "' AND '" . $request['invoice_id_to'] . "'";
		} else {
			if (isset($request['invoice_id']) && $request['invoice_id'] != '') {
				$where[] = "invoice_no LIKE '%" . $request['invoice_id'] . "%'";
			}

			if (isset($request['invoice_id_to']) && $request['invoice_id_to'] != '') {
				$where[] = "invoice_no LIKE '%" . $request['invoice_id_to'] . "%'";
			}
		}

		if (isset($request['from_date']) && $request['from_date'] != '' && isset($request['to_date']) && $request['to_date'] != '') {
			$fromData = $request['from_date'] . ' 00:00:00';
			$toData = $request['to_date'] . ' 23:59:59';
			$where[] = "posting_date BETWEEN '" . date('Y-m-d', strtotime($fromData)) . "' AND '" . date('Y-m-d', strtotime($toData)) . "'";
		} else {
			if (isset($request['from_date']) && $request['from_date'] != '') {
				$fromData = $request['from_date'] . ' 00:00:00';
				$where[] = "posting_date >= '" . date('Y-m-d h:i:s', strtotime($fromData)) . "'";
			}

			if (isset($request['to_date']) && $request['to_date'] != '') {
				$toData = $request['to_date'] . ' 23:59:59';
				$where[] = "posting_date <= '" . date('Y-m-d h:i:s', strtotime($toData)) . "'";
			}
		}
	}

	if (!empty($where)) {
		$whereStr = ' WHERE ' . implode(' AND ', $where) . "";
	}
	//echo $query.$whereStr;
	return $query . $whereStr;
}


?>

<?php

$columns = $report_send_helper->getGstTcsReportColumns();
if (isset($_GET['export_data'])) {
	exportData($_GET);
}
$resultsDatas = getResultData($_GET);
$results = $resultsDatas['datas'];
$paging  = $resultsDatas['paging'];
?>
<style>
	.exportbu {
		width: 21% !important;
	}
</style>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
	<link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
	<link rel="stylesheet" href="/reports/assets/datepicker.css">
	<link rel="stylesheet" href="/reports/assets/skin.css">
	<link rel="stylesheet" href="/reports/assets/style.css">
</head>

<body class="skin-blue">
	<div class="wrapper">

		<div class="content-wrapper">
			<section class="content">

				<div class="box box-default">
					<div class="box-body" style="padding:4px;">
						<div class="row">

							<div class="col-md-12">
								<form action="/reports/om_gsttcs_report.php?k=<?php echo time(); ?>" id="filter_user" method="get" autocomplete="off" novalidate="novalidate">
									<div class="col-md-2">
										<div class="form-group">
											<div class="input-group" style="width: 100%;">
												<label>Invoice ID FROM</label>
												<input type="text" name="invoice_id" placeholder="000000000" value="<?php echo @$_GET['invoice_id']; ?>" class="form-control" />
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<div class="input-group" style="width: 100%;">
												<label>Invoice ID TO</label>
												<input type="text" name="invoice_id_to" placeholder="000000000" value="<?php echo @$_GET['invoice_id_to']; ?>" class="form-control" />
											</div>
										</div>
									</div>

									<!-- 					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID FROM</label>
							<input type="text" name="order_id" placeholder="000000000" value="<?php echo @$_GET['order_id']; ?>" class="form-control" />
						  </div>
						</div>
					 </div>
					 <div class="col-md-2">
						<div class="form-group">
						  <div class="input-group" style="width: 100%;">
							<label>Order ID TO</label>
							<input type="text" name="order_id_to" placeholder="000000000" value="<?php echo @$_GET['order_id_to']; ?>" class="form-control" />
						  </div>
						</div>
					 </div> -->


									<div class="col-md-2">
										<div class="form-group">
											<div class="input-group" style="width: 100%;">
												<label>Pickup From Date</label>
												<input name="from_date" type="text" value="<?php echo @$_GET['from_date']; ?>" class="form-control datepicker" />
											</div>
										</div>
									</div>

									<div class="col-md-2">
										<div class="form-group">
											<div class="input-group" style="width: 100%;">
												<label>Pickup End Date</label>
												<input name="to_date" type="text" value="<?php echo @$_GET['to_date']; ?>" class="form-control datepicker" />
											</div>
										</div>
									</div>




									<div style="clear:both">&nbsp;</div>
									<button name="search" type="submit" class="btn bg-navy">Search</button>
									<button name="reset" type="button" onclick="location.href='/reports/om_gsttcs_report.php'" class="btn bg-navy">Reset</button>
									<button name="export_data" type="submit" class="btn bg-navy">Export</button>
							</div>

							<div style="clear:both">&nbsp;</div>
							<div class="col-sm-4">
								Per Page :
								<select name="perpage" onchange="this.form.submit()">
									<option <?php echo $_GET['perpage'] == 15 ? 'selected' : ""; ?> value="15">15</option>
									<option <?php echo $_GET['perpage'] == 30 ? 'selected' : ""; ?> value="30">30</option>
									<option <?php echo $_GET['perpage'] == 50 ? 'selected' : ""; ?> value="50">50</option>
									<option <?php echo $_GET['perpage'] == 100 ? 'selected' : ""; ?> value="100">100</option>
									<option <?php echo $_GET['perpage'] == 200 ? 'selected' : ""; ?> value="200">200</option>
								</select>
							</div>
							<div class="col-sm-8">
								<ul class="pagination pull-right" style="margin: 10px 0;">
									<?php echo $paging; ?>
								</ul>
							</div>

							</form>
						</div>
					</div>
				</div>

				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" style="font-size:13px;">
							<thead>
								<tr>
									<?php foreach ($columns as $column) { ?>
										<th><?php echo $column; ?></th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($results as $result) { ?>
									<tr>
										<?php foreach ($columns as $key => $column) { ?>
											<td title="<?php echo $result[$key]; ?>"><?php echo substr($result[$key], 0, 40); ?></td>
										<?php } ?>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>

				</div>


		</div>
		</section>

	</div>
	</div>


	<script src="/reports/assets/jquery.min.js"></script>
	<script src="/reports/assets/bootstrap.min.js"></script>
	<script src="/reports/assets/datepicker.js"></script>
	<script>
		$(function() {
			$('.datepicker').datepicker({
				autoclose: true
			})
		})
	</script>
</body>

</html>