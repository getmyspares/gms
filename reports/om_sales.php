<?php
/**TODO Remove reports/om_sales_date.php and move all the changes to Om GenerateReport's helper class Reports/SalesReport */
require_once('../reports/om_sales_data.php');
$seller_info = getSalesReportSeller();
?>
<?php
	$report_send_helper = $objectManager->create('OM\GenerateReport\Helper\EmailReport');
	$columns = $report_send_helper->getSalesColumns();
	if( isset($_GET['export_data']) ){
		exportData($_GET);
	}
	if( isset($_GET['download_invoices']) ){
		$to_date = $_GET['to_date'];
		$from_date = $_GET['from_date'];
		if(!isset($_GET['to_date']) || empty($_GET['to_date']) || !isset($_GET['from_date']) || empty($_GET['from_date']))
		{
			echo "<h3>To date and from date are required fields for invoice download</h3>";
			die();
		}
		
       if(count(explode('/',$_GET['to_date']))!=3)
        {
            $this->messageManager->addNoticeMessage("Please enter valid to date");
            echo "<h3>To date and from date are required fields for invoice download</h3>";
            die();
        }

        if(count(explode('/',$_GET['from_date']))!=3)
        {
            $this->messageManager->addNoticeMessage("Please enter valid from date");
            echo "<h3>To date and from date are required fields for invoice download</h3>";
            die();
        }

		downloadInvoice($to_date,$from_date);
	}


	// print_r(getResultData($_GET['status']));	
	
	$resultsDatas = getResultData($_GET);

    // echo'<pre>';print_r($resultsDatas);

    // exit(); 

	$tinfo = $resultsDatas['tdatas'][0];
	$results = $resultsDatas['datas'];

	$paging  = $resultsDatas['paging'];
?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/reports/assets/bootstrap.min.css">
    <link rel="stylesheet" href="/reports/assets/font-awesome.min.css">
    <link rel="stylesheet" href="/reports/assets/datepicker.css">
    <link rel="stylesheet" href="/reports/assets/skin.css">
    <link rel="stylesheet" href="/reports/assets/style.css">
</head>

<body class="skin-blue">
    <div class="wrapper">

        <div class="content-wrapper">
            <section class="content">

                <div class="box box-default">
                    <div class="box-body" style="padding:4px;">
                        <div class="row">

                            <div class="col-md-12">
                                <form action="/reports/om_sales.php?k=<?php echo time(); ?>" id="filter_user"
                                    method="get" autocomplete="off" novalidate="novalidate">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%;">
                                                <label>Order ID FROM</label>
                                                <input type="text" name="order_id" placeholder="000000000"
                                                    value="<?php echo @$_GET['order_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <?php if(isset($_GET['order_id']) && strlen($_GET['order_id'])==4):?>
                                        <span class="order-number-format-error">please use full order id for filtering ,
                                            skipping zeros will lead no result when to and from both are selected</span>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%;">
                                                <label>Order ID TO</label>
                                                <input type="text" name="order_id_to" placeholder="000000000"
                                                    value="<?php echo @$_GET['order_id_to']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%;">
                                                <label>Status</label>
                                                <select name="status[]" multiple
                                                    class="form-control file-filter required" style="width: 100%;"
                                                    tabindex="-1" aria-hidden="true">
                                                    <option value=''>-- select Status --</option>
                                                    <option
                                                        <?php echo @in_array('New',$_GET['status']) ? 'selected':''; ?>
                                                        value='New'>New</option>
                                                    <option
                                                        <?php echo @in_array('processing',$_GET['status']) ? 'selected':''; ?>
                                                        value='processing'>Processing</option>
                                                    <option
                                                        <?php echo @in_array('shipped',$_GET['status']) ? 'selected':''; ?>
                                                        value='shipped'>Shipped</option>
                                                    <option
                                                        <?php echo @in_array('delivered',$_GET['status']) ? 'selected':''; ?>
                                                        value='delivered'>Delivered</option>
                                                    <option
                                                        <?php echo @in_array('closed',$_GET['status']) ? 'selected':''; ?>
                                                        value='Refunded'>Refunded</option>
                                                    <option
                                                        <?php echo @in_array('refudnandrefunded',$_GET['status']) ? 'selected':''; ?>
                                                        value='refudnandrefunded'>Refund/Refunded</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%;">
                                                <label>From Date</label>
                                                <input name="from_date" type="text"
                                                    value="<?php echo @$_GET['from_date']; ?>"
                                                    class="form-control datepicker" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%;">
                                                <label>End Date</label>
                                                <input name="to_date" type="text"
                                                    value="<?php echo @$_GET['to_date']; ?>"
                                                    class="form-control datepicker" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2" <?php if($logged_in_seller){echo "style='display:none'";}?>>
                                        <div class="form-group">
                                            <div class="form-select">
                                                <label>Seller Name</label>
                                                <select name="seller_id" id="seller_id_field"
                                                    class=" form-control form-select">
                                                    <?php if($logged_in_seller):?>
                                                    <option <?php if(isset($_GET['seller_id'])) echo "selected"?>
                                                        value="<?php echo $_GET['seller_id'] ?>">
                                                        <?php echo $Seller_name_session ?></option>
                                                    <?php else:?>
                                                    <option selected value="">Select Seller</option>
                                                    <?php foreach($seller_info as $seller_data): ?>
                                                    <option
                                                        <?php if(isset($_GET['seller_id']) &&$_GET['seller_id']==$seller_data['seller_id']) echo "selected"?>
                                                        value="<?php echo $seller_data['seller_id']?>">
                                                        <?php echo $seller_data['seller_name']?></option>
                                                    <?php endforeach; ?>
                                                    <?php endif;?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="clear:both">&nbsp;</div>
                                    <div class="col-md-6  col-sm-6">
                                        <label style="width:100%;">&nbsp;</label>
                                        <input type="hidden" value="<?php echo time(); ?>" name="key" />
                                        <input type="hidden"
                                            value="<?php echo isset($_GET['areatype']) ? $_GET['areatype']:''; ?>"
                                            name="areatype" />
                                        <!--					  --><?php //if($_GET['seller_id']):?>
                                        <!--                        <input type="hidden" value="--><?php //echo isset($_GET['seller_id']) ? $_GET['seller_id']:''; ?>
                                        <!--" name="seller_id" />-->
                                        <!--                      --><?php //endif;?>

                                        <?php
					  $append = '';
					  if(isset($_GET['areatype']) && !empty($_GET['areatype'])){
						$append = '?areatype='.$_GET['areatype'].'&seller_id='.$_GET['seller_id'];
					  } 
					  ?>

                                        <button name="search" type="submit" class="btn bg-navy">Search</button>
                                        <button name="reset" type="button"
                                            onclick="location.href='/reports/om_sales.php<?php echo $append; ?>'"
                                            class="btn bg-navy">Reset</button>
                                        <button name="export_data" type="submit" class="btn bg-navy">Export
                                            Report</button>
                                        <button name="download_invoices" type="submit" class="btn bg-navy">Download
                                            Invoices</button>

                                    </div>

                                    <div class="col-md-6  exportbu col-sm-6">
                                        <label>Total Order - <?php echo $tinfo['total_orders'];?></label><br />
                                        <label>Total Invoiced - <?php echo $tinfo['total_invoiced'];?></label><br />
                                        <label>Total Refunded -
                                            <?php echo $tinfo['total_refunded']?$tinfo['total_refunded']:0;?></label>
                                    </div>
                                    <div style="clear:both">&nbsp;</div>
                                    <div class="col-sm-4">
                                        Per Page :
                                        <select name="perpage" onchange="this.form.submit()">
                                            <option <?php echo $_GET['perpage']==15 ? 'selected':""; ?> value="15">15
                                            </option>
                                            <option <?php echo $_GET['perpage']==30 ? 'selected':""; ?> value="30">30
                                            </option>
                                            <option <?php echo $_GET['perpage']==50 ? 'selected':""; ?> value="50">50
                                            </option>
                                            <option <?php echo $_GET['perpage']==100 ? 'selected':""; ?> value="100">100
                                            </option>
                                            <option <?php echo $_GET['perpage']==200 ? 'selected':""; ?> value="200">200
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-sm-8">
                                        <ul class="pagination pull-right" style="margin: 10px 0;">
                                            <?php echo $paging; ?>
                                        </ul>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-striped" style="font-size:13px;">
                                <thead>
                                    <tr>
                                        <?php foreach($columns as $column){ ?>
                                        <th><?php echo $column; ?></th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($results as $result){ ?>
                                    <tr>
                                        <?php foreach($columns as $key => $column){
							  	 ?>
                                <?php if($key=="exchange_awb_delivery_date"):?>
                                        <?php
								$query1="SELECT `order_id`  FROM `wk_rma_items` WHERE `item_id` = '$item_id'";
									$order_id_e = $connection->fetchOne($query1);
									if($order_id_e)
									{
										$order_id = $result['order_id'];
										$query1="SELECT `delivery_date`  FROM `ecomexpress_awb_exchange` WHERE `orderid` = '$order_id'";
										$exchange_awb_delivery_date = $connection->fetchOne($query1);
									}
								?>
                                        <td><?php echo $exchange_awb_delivery_date; ?></td>
                                        <?php elseif($key=="order_status"):
								/* get order commnets */
								$type = $result['report_type'];
								$order_status=$result['order_status'];
								if($type==1)
								{
									$order_id = $result['order_id'];
									$query1="SELECT `order_id`  FROM `wk_rma` WHERE `order_id` = '$order_id'";
									$order_rma = $connection->fetchOne($query1);
									if($order_rma)
									{
										$item_id = $result['item_id']; 
										$query2="SELECT `id`  FROM `om_sales_report` WHERE `report_type` = '7' and item_id='$item_id'";
										$entity_id_adadasd = $connection->fetchOne($query2);
										if(!empty($entity_id_adadasd))
										{
											$order_status="Refund";
										} else 
										{
											$order_status =  "Delivered";
										}	
									}
								}	
							?>
                                        <td><?php echo $order_status; ?></td>
                                        <?php else:?>
                                        <td title="<?php echo $result[$key]; ?>">
                                            <?php echo substr($result[$key],0,10); ?></td>
                                        <?php endif;?>
                                        <?php } ?>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>


    <script src="/reports/assets/jquery.min.js"></script>
    <script src="/reports/assets/bootstrap.min.js"></script>
    <script src="/reports/assets/datepicker.js"></script>
    <script>
    $(function() {
        $('.datepicker').datepicker({
            endDate: new Date(),
            autoclose: true
        })
    })
    </script>
    <style>
    .loader-div {
        width: 100px;
        margin: auto;
        text-align: center;
    }

    .loader {
        border: 16px solid #f3f3f3;
        /* Light grey */
        border-top: 16px solid #001f3f;
        /* Blue */
        border-radius: 50%;
        width: 100px;
        height: 100px;
        animation: spin 2s linear infinite;
        display: none;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    .order-number-format-error {
        color: #c87f7f !important;
    }
    </style>
    <style>
    .loader {
        display: block !important;
    }

    @media only screen and (max-width:1200px) and (min-width:600px){
         form#filter_user {
            display: block;
            flex-wrap: wrap;
            align-items: start;
            justify-content: space-between;
        }

       

        div[style="clear:both"] { 
            display: none;
        }

        form#filter_user .col-md-2 {width: 33.33%;float: left;}

        form#filter_user .col-md-2:nth-child(3) {
            float: right;
        }

        form#filter_user .col-md-2:nth-child(3) select.form-control.file-filter.required {
            height: 110px;
        }



        div[style="clear:both"] + .col-md-6.col-sm-6 > label {
            display: none;
        }

         div[style="clear:both"] + .col-sm-4 {
            margin-top: 15px;
        }
        table.table th {
            white-space: nowrap;
        }
    }
    .box.box-default .box-body {
            overflow-x: auto;
        }
    @media only screen and (max-width:600px){
        
		.box-body {
			width: 100%;
			overflow-x: auto;
		}

		form#filter_user > div {
			padding: 0;
			width: 100%;
		}

		table.table th {
			white-space: nowrap;
		}

		form#filter_user > div button {
			margin-bottom: 10px;
			margin-right: 10px;
		}

		form#filter_user > div.col-md-6.exportbu.col-sm-6, form#filter_user > div.col-md-12.col-sm-12.exportbu {
			width: 100% !important;
		}
	}


    </style>
</body>

</html>