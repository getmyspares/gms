<?php
include('app/bootstrap.php');
include 'barcode/vendor/autoload.php';
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
//$order_inc_id = base64_decode(urldecode($_GET['order'])); 
$order_inc_id = $_GET['order']; 
$rma_id = $_GET['rma_id']; 

if(empty($order_inc_id) || empty($rma_id))
{
	die("invalid parameters passed");
}
 
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$custom_helper = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 

$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 



$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id);
$order_iddd=$order->getId();  


$orderArray=$order->getData();
$created_at=$orderArray['created_at']; 
$created_at=date("d M,Y", strtotime($created_at));


 
$grand_total=$orderArray['grand_total']; 
$grand_total=number_format($grand_total, 2, '.', '');

$payment_method=$order->getPayment()->getMethod();
if($payment_method=='cashondelivery')
{
	$payment_method_type='COD';
	$payment_collect='Payment Due :'.$grand_total;
}	
else
{
	$payment_method_type='PPD'; 
	$payment_collect='';
}	



$seller_count=$custom_helper->count_order_sellers($order_inc_id);
$customerSession = $objectManager->get('Magento\Customer\Model\Session');
if($customerSession->isLoggedIn()) 
{
	$customer_id=$customerSession->getCustomer()->getId();
}	



$seller_array=$custom_helper->get_unique_seller_order($order_inc_id);

if (!in_array($customer_id, $seller_array)) {
	echo "Your business is your business and this is not your business";
	die; 
}	
  

$seller_detail=$custom_helper->get_seller_details($customer_id);
/* echo "<pre>";
	print_r($seller_detail);
echo "</pre>"; */

$seller_comp_name=$seller_detail['seller_comp_nam'];
$exchange_awb=$custom_helper->getExchangeAwb($order_iddd);
$awb = $exchange_awb;
if(!empty($awb))
{
	$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
	$barcode='<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($awb, $generator::TYPE_CODE_128,2,60)) . '">';
} else 
{
	$barcode="Awb Not Found";
}
  

$customername=$order->getCustomerName();
$shippingaddress=$order->getShippingAddress()->getStreet();

$street=$custom_helper->get_customer_order_adress($order_iddd,'street','shipping'); 
$city=$custom_helper->get_customer_order_adress($order_iddd,'city','shipping'); 
$region=$custom_helper->get_customer_order_adress($order_iddd,'region','shipping'); 
$postcode=$custom_helper->get_customer_order_adress($order_iddd,'postcode','shipping'); 
$telephone=$custom_helper->get_customer_order_adress($order_iddd,'telephone','shipping'); 


$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$route=$api_helpers->get_pincode_route($postcode);
 
		
$sql = "Select * FROM marketplace_saleslist where magerealorder_id='".$order_inc_id."' and  seller_id='".$customer_id."'"; 
$result = $connection->fetchAll($sql); 

$productarray=array();

$total_qty=0;

$product_name='';
$productName='';

$prod_dimension='';
$prodDimension='';

$product_weight='';
$productWeight='';
 

if(!empty($result))
{
	foreach($result as $row)
	{
		$product_id = $row['mageproduct_id'];
		$order_item_id = $row['order_item_id'];
		$query="SELECT id FROM `wk_rma_items` where rma_id='$rma_id' and item_id='$order_item_id'";
		$rma_item_id = $connection->fetchOne($query); 

		if(empty($rma_item_id))
		{
			continue;
		}

		$productId = $product_id; //Product Id
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
		$product_name .=$product->getName().',';
		$product_weightt=$product->getWeight();  
		
		$product_weight .=number_format($product_weightt, 2, '.', '').',';
		
		$productqty=$row['magequantity'];	 
		$total_qty=$total_qty+$productqty;
		
		$product_length=$custom_helper->get_product_length($product_id);
		$product_breath=$custom_helper->get_product_breath($product_id);
		$product_height=$custom_helper->get_product_height($product_id); 
		
		if($product_length=='')
		{
			$product_length=0;
		}
		if($product_breath=='')
		{
			$product_breath=0;
		}
		if($product_height=='')
		{
			$product_height=0; 
		}
		
		$product_length=number_format($product_length, 2, '.', '');
		$product_breath=number_format($product_breath, 2, '.', '');
		$product_height=number_format($product_height, 2, '.', '');
		
		$prod_dimension .=$product_length.'x'.$product_breath.'x'.$product_height.',';
		
	}
	

	$productName=substr($product_name,0,-1);
	$prodDimension=substr($prod_dimension,0,-1);
	$productWeight=substr($product_weight,0,-1);
}	




 
//mageproduct_id 


$html = ' 
<html>
<head>  
<style>
table{
font-size:10px;
border-collapse: collapse;
margin:0px auto;
background:#fff;
}
@page {margin:30px;}
table img{width:200px}

table tr th{
vertical-align:middle;
font-size:24px;
}
table tr th:first-child{
	text-align:left;
}
table tr th:last-child{
	text-align:right;
}
table tr th p{
	margin:0px;
	font-size:20px;
	width:100%;
}

table tr th, table tr td{
    padding:10px;
    width:25%;
    font-size:16px;
}
.border{border:1px solid #000}
</style>
</head>
<body style="font-family: Open Sans, sans-serif;font-size:10px;" >';
$html .='</div> 
	<table>
	<thead>
		<tr>
			
			<th>['.$payment_method_type.'] '.$payment_collect.'</th>
			<th colspan="2">
				ECOM EXPRESS<br/>  
				'.$barcode.'<br/>
				<p><small>'.$awb.'</small></p>
			</th>
			<th>['.$route.']</th> 
		</tr>
	</thead>
	<tbody>
		<tr class="border">
			<!--<td colspan="2"><strong>Shipper:</strong> '.$seller_comp_name.'</td>-->
			<td colspan="2"><strong>Shipper:</strong> PANASONIC INDIA PRIVATE LIMITED</td> 
			<td colspan="2"><strong>Order # :</strong> '.$order_inc_id.'</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="border" colspan="2"><strong>Consignee Details:</strong></td>
			<td class="border" colspan="2"><strong>Shipper Details:</strong></td>
		</tr>
		<tr>
			<td class="border" colspan="2">
				<p>'.$customername.'</p>
				<p>'.$street.'</p>
				<p>'.$city.', '.$region.', '.$postcode.'</p>
				<p>India</p>
				<p>T: '.$telephone.'</p> 
			</td>
			<td class="border" colspan="2">
				<p>Item Description : '.$productName.'</p> 
				<p>Quantity : '.$total_qty.'</p>
				<p>Dimension : '.$prodDimension.'</p>
				<p>Actual Weight : '.$productWeight.'</p>
				<p>Order Date :'.$created_at.'</p>
			</td> 
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" align="center"  class="border"><strong>IF UNDELIVERED RETURN TO</strong></td>
		</tr>
		<tr>
			<td colspan="4"  class="border">Store - '.$seller_detail['seller_name'].', '.$seller_detail['seller_comp_address'].', IN, Mobile - '.$seller_detail['seller_comp_mobile'].'</td>
		</tr> 
	</tbody>
	</table>
	
'; 
                       

$html .='</body></html>';

//echo $html; 
//die;
 
 

//==============================================================
//==============================================================
//==============================================================





include("pdf/mpdf.php");

$mpdf=new mPDF('c','A4-P','','',32,25,27,25,16,13);  

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
//$stylesheet = file_get_contents('mpdfstyletables.css');
//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);
$price_name='PackingSlip-ExchangeOrder'.date('Y-m-d'); 
$mpdf->Output($price_name.'.pdf','D');
//$mpdf->Output($price_name.'.pdf','I');
exit;


?>
