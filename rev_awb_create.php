<?php
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
     
include('app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();


$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');




$select = $connection->select()
		  ->from('wk_rma') 
		  ->where('final_status= ?','3');
$result = $connection->fetchAll($select);


 
if(!empty($result))
{

	foreach($result as $row)
	{
		$rma_id=$row['rma_id'];	
		$increment_id=$row['increment_id'];	
	}	


	$select = $connection->select()
			  ->from('rev_awb') 
			  ->where('order_id= ?',$increment_id);
	$resultss = $connection->fetchAll($select);
	
	

	if(empty($resultss)) 
	{ 	
		
		$select = $connection->select()
				  ->from('wk_rma_items') 
				  ->where('rma_id= ?',$rma_id);
		$result = $connection->fetchAll($select);

		foreach($result as $row) 
		{
			$item_idd=$row['item_id'];	
			$item_qty=$row['qty'];	
		}	


		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($increment_id); 
		$order_id=$order->getId();
				
				 
		foreach ($order->getAllItems() as $item)
		{
			$item_id=$item->getId();
			if($item_idd==$item_id)
			{
				$product_id=$item->getProductId();
			}		
		} 

		$custom_helpers->rev_ecom_api($increment_id,$product_id,$item_qty);	  
	}
	else
	{
		echo "All reverse are manified";   
	}	 	
} 
else
{
	echo "No rev is solved yet";   
}	