var config = {
    paths: {  
    		'owl-carousel': "Magento_Theme/owl-carousel/js/owl.carousel",
            'bootstrap': "Magento_Theme/bootstrap/js/bootstrap.bundle",
            'nivoSlider': "Magento_Theme/nivo-slider/js/nivoSlider",
        },   
    shim: {
    	 'owl-carousel': {
            deps: ['jquery']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'nivoSlider': {
            deps: ['jquery']
        },
         
    }
};
