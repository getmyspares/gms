/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'mage/mage',
    "mage/template",
    "Magento_Ui/js/modal/modal",
    "Magento_Ui/js/modal/alert",
    'Magento_Customer/js/model/authentication-popup'
], function ($,mage,template,modal,alert,authenticationPopup) {
    'use strict';
    $.widget('mage.mpqa_tab', {

        _create: function () {
            var self = this;
            var qaAnsForm = $('#qa-ans-form');
                qaAnsForm.mage('validation', {});
            var qaQuesForm= $('#qa-ques-form');
                qaQuesForm.mage('validation', {});
            //question modal
            var options_ques = {
                type: 'popup',responsive: true,innerScroll: true,title: 'Have Any Query?',modalClass: 'customquery',
                buttons: [{
                        text: 'Reset',
                        class:'customquery',
                        click: function () {
                            $('#qa-ques-form input,#qa-ques-form textarea').removeClass('mage-error');
                            $('#qa-ques-form')[0].reset();
                        } //handler on button click
                    },{
                        text: 'Submit Query',
                        class: 'wk-question-submit',
                        click: function () {
                            // -----save question
                            var su = $('#sub').val();
                            var cn = $('#content').val();
                            var replaced_text = cn;
                            var nickname=$('#qa_nickname').val();
                            var adurl = $('#adminurl').val();
                            var nm = "";
                            if (qaQuesForm.valid()!=false) {
                                var thisthis = $(this);
                                $.ajax({
                                    url     :   self.options.question_url,
                                    type    :   "POST",
                                    data    :   {pid:self.options.product_id,
                                                subj:$('#sub').val(), con:cn,aurl:adurl,nickname:nickname},
                                    dataType:   "html",
                                    showLoader: true,
                                    success :   function (content) {
                                        $('#qa-ques-form')[0].reset();

                                        alert({
                                            title: 'Success!',
                                            modalClass: 'form-success',
                                            content: 'Your query has been submitted',
                                            actions: {
                                                always: function () {
                                                    //location.reload();
                                                }
                                            }
                                        });
                                    }
                                });
                                this.closeModal();
                            }
                        } //handler on button click
                    }
                ]
            };
            var popup = modal(options_ques, $('#wk-qa-ask-qa'));

            //answer modal
            var options_ans = {
                type: 'popup',responsive: true,innerScroll: true,title: 'Submit Answer',modalClass: 'answer-class',
                buttons: [{
                        text: 'Reset',
                        class:'',
                        click: function () {
                            $('#qa-ans-form input,#qa-ans-form textarea').removeClass('mage-error');
                            $('#qa-ans-form')[0].reset();
                        } //handler on button click
                    },{
                        text: 'Submit Answer',
                        class: 'wk-answer-submit',
                        click: function () {
                            // -----save answer
                            if (qaAnsForm.valid()!=false) {
                                var thisthis = $(this);
                                $.ajax({
                                    url:self.options.submitanswer_url,
                                    data:$('#qa-ans-form').serialize(),
                                    type:'post',
                                    showLoader: true,
                                    dataType:'json',
                                    success:function (d) {
                                        alert({
                                            title: 'Success!',
                                            content: 'Answer submitted successfully',
                                            modalClass: 'answer-class-success',
                                            actions: {
                                                always: function () {
                                                    location.reload();
                                                }
                                            }
                                        });
                                        $('#qa-ans-form')[0].reset();
                                    }
                                });
                                this.closeModal();
                            }
                        } //handler on button click
                    }
                ]
            };
            var popup1 = modal(options_ans, $('#wk-qa-ask-data'));

// ------question search
            $(document).on('keypress','#searchqa', function (e) {
                if (e.which == 13) {
                    search();
                }
            });
            $(document).on('click','.search_button',function (e) {
                search();
            });
            function search()
            {
                var query = $('#searchqa').text(jQuery(this).val());
                if ($.trim(query)!='') {
                    $("body").append(jQuery("<div/>").addClass("filterurl_loader").append(jQuery("<div/>")));
                    $('.wk-qa-action').css('display','none');
                    $.ajax({
                        url     :   self.options.search_url,
                        type    :   "POST",
                        data    :   {pid:self.options.product_id,
                                    query:$('#searchqa').val()},
                        dataType:   "json",
                        success:function (data1) {
                            $('.all-questions').html('');
                            var ask_ques=template('#ask-question-template');
                            $.each(data1, function () {
                                var templateData = template('#question-answertemplate');
                                var questions = templateData({
                                                data: {
                                                    question_id: this['question_id'],
                                                    subject: this['subject'],
                                                    content: this['content'],
                                                    qa_nickname:this['qa_nickname'],
                                                    qa_date:this['qa_date'],
                                                    answer:this['answer'],
                                                    answer_id:this['answer_id'],
                                                    nickname:this['nickname'],
                                                    likes:this['likes'],
                                                    dislikes:this['dislikes'],
                                                    like_class:this['like_class'],
                                                    dislike_class:this['dislike_class'],
                                                    createdat:this['createdat'],
                                                    answer_count:this['count']
                                                }
                                            });
                                $('.all-questions').append(questions);
                            });

                            $('.all-questions').append(ask_ques);
                            if (data1.length==0) {
                                $(".no-result").show();
                                $('.no-result').css('display','block');
                            }
                                $('.wk-qa-action-label-search').html($("<span/>").html(query).text());
                                $('#wk-qa-action-search').css('display','inline-block');
                                $("body").find('.filterurl_loader').remove();
                                $(".pager").remove();
                        }
                    });
                }
            }
    // ---- recent url
            $(document).on('click','#recent', function () {
                $('.wk-qa-action').css('display','none');
                $("body").append(jQuery("<div/>").addClass("filterurl_loader").append(jQuery("<div/>")));
                $.ajax({
                    url     :   self.options.recent_url,
                    type    :   "POST",
                    data    :   {pid:self.options.product_id},
                    dataType:   "json",
                    success:function (data1) {
                        $('.all-questions').html('');
                        var ask_ques=template('#ask-question-template');
                        $.each(data1, function () {
                            var employeeTemplate = template('#question-answertemplate');
                            var employee = employeeTemplate({
                                                data: {
                                                    question_id: this['question_id'],
                                                    subject: this['subject'],
                                                    content: this['content'],
                                                    qa_nickname:this['qa_nickname'],
                                                    qa_date:this['qa_date'],
                                                    answer:this['answer'],
                                                    answer_id:this['answer_id'],
                                                    nickname:this['nickname'],
                                                    likes:this['likes'],
                                                    dislikes:this['dislikes'],
                                                    like_class:this['like_class'],
                                                    dislike_class:this['dislike_class'],
                                                    createdat:this['createdat'],
                                                    answer_count:this['count']
                                                }
                                            });

                            $('.all-questions').append(employee);
                        });
                         $('.all-questions').append(ask_ques);
                         $("body").find('.filterurl_loader').remove();
                         $(".pager").remove();
                         $('#wk-qa-action-recent').css('display','inline-block');
                    }
                });
            });
    // ---- most helpful
            $(document).on('click','#helpful', function () {
                $('.wk-qa-action').css('display','none');
                $("body").append(jQuery("<div/>").addClass("filterurl_loader").append(jQuery("<div/>")));
                $.ajax({
                    url     :   self.options.helpful_url,
                    type    :   "POST",
                    data    :   {pid:self.options.product_id},
                    dataType:   "json",
                    success:function (data1) {
                        $('.all-questions').html('');
                        var ask_ques=template('#ask-question-template');
                        $.each(data1, function () {
                            var employeeTemplate = template('#question-answertemplate');
                            var employee = employeeTemplate({
                                                data: {
                                                    question_id: this['question_id'],
                                                    subject: this['subject'],
                                                    content: this['content'],
                                                    qa_nickname:this['qa_nickname'],
                                                    qa_date:this['qa_date'],
                                                    answer:this['answer'],
                                                    answer_id:this['answer_id'],
                                                    nickname:this['nickname'],
                                                    likes:this['likes'],
                                                    dislikes:this['dislikes'],
                                                    like_class:this['like_class'],
                                                    dislike_class:this['dislike_class'],
                                                    createdat:this['createdat'],
                                                    answer_count:this['count']
                                                }
                                            });

                            $('.all-questions').append(employee);
                        });
                         $('.all-questions').append(ask_ques);
                         $("body").find('.filterurl_loader').remove();
                         $(".pager").remove();
                         $('#wk-qa-action-helpful').css('display','inline-block');
                    }
                });
            });
            $(document).on('click','.wk-qa-action-button',function () {
                location.reload();
            });
    // ------ more answer
            $(document).on('click','.qa-ansmore', function () {
                var questionid =    $(this).attr('dataid');
                var this_this=$(this);
                var logid = self.options.buyer_id;

                $("body").append(jQuery("<div/>").addClass("filterurl_loader").append(jQuery("<div/>")));
                $.ajax({
                    url: self.options.viewall_ans_url,
                    data:{quesid:questionid,custid:logid},
                    dataType:'json',
                    success:function (content) {
                        $(this_this).parent().hide();
                        $(this_this).parent().after(content['answer']);
                        $("body").find('.filterurl_loader').remove();
                    }
                });
            });
    // ------ like answer
            $(document).on('click','.like', function () {

                var this_this=$(this);
                var login=self.options.islogin;
                if (login==0) {
                    authenticationPopup.showModal();
                    return false;
                }
                $("body").append($("<div/>").addClass("filterurl_loader").append($("<div/>")));
                var ansid = $(this).attr('dataid');
                var logid = self.options.buyer_id;
                $.ajax({
                    url:self.options.reviewanswer_url,
                    data:{ansid:ansid,custid:logid,action:'like'},
                    success:function (content) {
                        if (content['action_result']==1) {
                            var count=$(this_this).next('span').html();
                            count++;
                            $(this_this).next('span').text(count);
                            $(this_this).addClass("liked").removeClass('like');
                            if (content['action']==1) {
                                $(this_this).siblings('.disliked').addClass("dislike").removeClass('disliked');
                                var dCount = $(this_this).siblings('.dislike').next('span').html();
                                dCount--;
                                $(this_this).siblings('.dislike').next('span').text(dCount);
                            }
                        }
                        $("body").find('.filterurl_loader').remove();
                    }
                });
            });

    // ------ dislike answer
            $(document).on('click','.dislike', function () {
                var ansid = $(this).attr('dataid');
                var logid = self.options.buyer_id;
                var this_this=$(this);
                var login=self.options.islogin;
                if (login==0) {
                    authenticationPopup.showModal();
                    return false;
                }
                $("body").append(jQuery("<div/>").addClass("filterurl_loader").append(jQuery("<div/>")));
                $.ajax({
                    url:self.options.reviewanswer_url,
                    data:{ansid:ansid,custid:logid,action:'dislike'},
                    success:function (content) {
                        if (content['action_result']==1) {
                            var count=$(this_this).next('span').html();
                            count++;
                            $(this_this).next('span').text(count);
                            $(this_this).addClass("disliked").removeClass('dislike');
                            if (content['action']==1) {
                                $(this_this).siblings('.liked').addClass("like").removeClass('liked');
                                var lCount = $(this_this).siblings('.like').next('span').html();
                                lCount--;
                                $(this_this).siblings('.like').next('span').text(lCount);
                            }
                        }
                        $("body").find('.filterurl_loader').remove();
                    }
                });

            });

            //pager link
            var listItems = $("ul.items.pages-items li.item a");
            listItems.each(function (idx, a) {

                var link=$(a).attr("href");
                link=link+'#wkqa.tab';
                $(a).attr("href",link);
            });

            $(document).on('click','.qa-ans', function () {
                var login=self.options.islogin;
                if (login==0) {
                    authenticationPopup.showModal();
                    return false;
                }

                var q_id=$(this).parents('.all-questions').find('.alogo').attr('id');
                q_id=q_id.substring(2);

                $("#question-id").val(q_id);
                $('#wk-qa-ask-data').modal('openModal');
            });


            $(document).on('click','.qa-question', function () {
                var login=self.options.islogin;
                if (login==0) {
                    authenticationPopup.showModal();
                    return false;
                }

                //In case if popup want to show in onclick event then use button in you form
                $('#wk-qa-ask-qa').modal('openModal');

            });

            $(document).on('click','.action-close', function () {
                $('#qa-ques-form')[0].reset();
                $('#qa-ans-form')[0].reset();
            });

            $('.search-form').submit(function () {
                var query = $('#wk-searchqa').val();
                if ($.trim(query) =='') {
                    event.preventDefault();
                }
            });
        },

    });
    return $.mage.mpqa_tab;
});
