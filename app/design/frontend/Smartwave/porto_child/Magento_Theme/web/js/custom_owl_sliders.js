require([
    'jquery',
	'owl.carousel/owl.carousel.min',
	'domReady!'
    ], function ($) {
		
		$.ajax({
			url : "/homebanner/",
			cache : false,
			success: function(data) {
				$("#banner-slider-demo-12").html(data); 
				$("#banner-slider-demo-12").owlCarousel({
				  items: 1,
				  autoplay: true,
				  autoplayTimeout: 5000,
				  autoplayHoverPause: true,
				  dots: true,
				  nav: true,
				  navRewind: true,
				  animateIn: 'fadeIn',
				  animateOut: 'fadeOut',
				  loop: true,
				  navText: ["<em class='porto-icon-chevron-left'></em>","<em class='porto-icon-chevron-right'></em>"]
				});	  
            }
		});
		
		//------------------------------------------------------------------
		
		
		setTimeout(function(){
			$("#brands-slider-demo-21 .owl-carousel").owlCarousel({
				autoplay: true,
				autoplayTimeout: 5000,
				autoplayHoverPause: true,
				margin: 0,
				nav: true,
				navText: ["<em class='porto-icon-left-open-huge'></em>","<em class='porto-icon-right-open-huge'></em>"],
				dots: false,
				loop: true,
				responsive: {
				  0: {
					items:1
				  },
				  640: {
					items:1
				  },
				  768: {
					items:1
				  },
				  992: {
					items:1
				  },
				  1200: {
					items:1
				  }
				}
			});
		}, 1000);
		
		
		
		//-----------------------------------------------------------------------------------
		
		
		setTimeout(function(){  
				$("#brands-slider-demo-21 .owl-carousel").owlCarousel({
				autoplay: true,
				autoplayTimeout: 5000,
				autoplayHoverPause: true,
				margin: 0,
				nav: true,
				navText: ["<em class='porto-icon-left-open-huge'></em>","<em class='porto-icon-right-open-huge'></em>"],
				dots: false,
				loop: true,
				responsive: {
				  0: {
					items:1
				  },
				  640: {
					items:1
				  },
				  768: {
					items:1
				  },
				  992: {
					items:1
				  },
				  1200: {
					items:1
				  }
				}
			  });
		    }, 1000);
		
		
		//-----------------------------------------------------------------
		
		
		setTimeout(function(){
			   $("#brands-slider-demo-2122 .owl-carousel").owlCarousel({
				autoplay: true,
				autoplayTimeout: 5000,
				autoplayHoverPause: true,
				margin: 0,
				nav: true,
				navText: ["<em class='porto-icon-left-open-huge'></em>","<em class='porto-icon-right-open-huge'></em>"],
				dots: false,
				loop: true,
				responsive: {
				  0: {
					items:1
				  },
				  640: {
					items:1
				  },
				  768: {
					items:1
				  },
				  992: {
					items:1
				  },
				  1200: {
					items:1
				  }
				}
			  });
		 }, 1000);
		
		
		//----------
		
		setTimeout(function(){
			$("#featured_product_list2 .owl-carousel").owlCarousel({
			  autoplay: true,
			  autoplayTimeout: 5000,
			  autoplayHoverPause: true,
			  loop: true,
			  navRewind: true,
			  margin: 0,
			  nav: true,
			  navText: ["<em class='porto-icon-left-open-huge'></em>","<em class='porto-icon-right-open-huge'></em>"],
			  dots: false,
			  responsive: {
				0: {
				  items:1
				},
				768: {
				  items:2
				},
				992: {
				  items:3
				},
				1200: {
				  items:4
				}
			  }
			});
		}, 1000);
		
		//----------
		
		setTimeout(function(){
			$("#featured_product_list .owl-carousel").owlCarousel({
			  autoplay: true,
			  autoplayTimeout: 5000,
			  autoplayHoverPause: true,
			  loop: true,
			  navRewind: true,
			  margin: 0,
			  nav: true,
			  navText: ["<em class='porto-icon-left-open-huge'></em>","<em class='porto-icon-right-open-huge'></em>"],
			  dots: false,
			  responsive: {
				0: {
				  items:1
				},
				768: {
				  items:2
				},
				992: {
				  items:3
				},
				1200: {
				  items:4
				}
			  }
			});
		}, 1000);
		
		//----------
		
		setTimeout(function(){
			$(".cus-owl-carousel").owlCarousel({
			  autoplay: true,
			  autoplayTimeout: 5000,
			  autoplayHoverPause: true,
			  loop: true,
			  navRewind: true,
			  margin: 0,
			  nav: true,
			  navText: ["<em class='porto-icon-left-open-huge'></em>","<em class='porto-icon-right-open-huge'></em>"],
			  dots: false,
			  responsive: {
				0: {
				  items:1
				},
				768: {
				  items:2
				},
				992: {
				  items:3
				},
				1200: {
				  items:4
				}
			  }
			}); 
		}, 1000);

});  
