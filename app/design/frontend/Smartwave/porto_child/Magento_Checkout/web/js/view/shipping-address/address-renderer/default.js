/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'underscore',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/shipping-service',
	'Magento_Ui/js/modal/modal'
], function ($, ko, Component, _, selectShippingAddressAction, quote, formPopUpState, checkoutData, customerData, shippingService, modal) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-address/address-renderer/default'
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super();
            this.isSelected = ko.computed(function () {
                var isSelected = false,
                    shippingAddress = quote.shippingAddress();

                if (shippingAddress) {
                    isSelected = shippingAddress.getKey() == this.address().getKey(); //eslint-disable-line eqeqeq
                    
                    /* Added By RKT */
                    //console.log(shippingAddress);
                    //console.log(this.address());
                    if(shippingAddress.postcode == this.address().postcode ) {
						this.showLockdownPopup(shippingAddress);
					}
                }

                return isSelected;
            }, this);

            return this;
        },
        
        
        /* Added By RKT */
		showLockdownPopup : function (address){
			jQuery('#lockdown_popup').remove();
			var defaultMsg = 'Due to covid  restriction, we are not shipping to the location selected by you.Please chose a different location';
			var pincode    = address.postcode;			
			var url        = '/pcheck/index/index/?pincode='+pincode;
			$.ajax({
				type: 'GET',
				url: url,
				dataType: "json",			
				success: function (output) {
					//console.log(output);
					if(output.is_active=='0'){
						if ( output.msg != null &&  output.msg != ''){
							defaultMsg = output.msg;
						}
						jQuery('<div id="lockdown_popup" tabindex="-1" role="dialog" aria-hidden="true">'+defaultMsg+'</div>').appendTo('body');
						var options = {
							type: 'popup',
							responsive: true,
							innerScroll: true,
							modalClass : 'lockdown_modal',
							title: 'Covid Restriction',
							buttons: []
						};       
						$('#lockdown_popup').modal(options).modal('openModal');				
					}
				},
				error: function(output){
					alert("fail");
				}
			});
		},
		
        /**
         * @param {String} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        /**
         * Get customer attribute label
         *
         * @param {*} attribute
         * @returns {*}
         */
        getCustomAttributeLabel: function (attribute) {
            var resultAttribute;

            if (typeof attribute === 'string') {
                return attribute;
            }

            if (attribute.label) {
                return attribute.label;
            }

            if (typeof this.source.get('customAttributes') !== 'undefined') {
                resultAttribute = _.findWhere(this.source.get('customAttributes')[attribute['attribute_code']], {
                    value: attribute.value
                });
            }

            return resultAttribute && resultAttribute.label || attribute.value;
        },

        /** Set selected customer shipping address  */
        selectAddress: function () {
            selectShippingAddressAction(this.address());
            checkoutData.setSelectedShippingAddress(this.address().getKey());
        },

        /**
         * Edit address.
         */
        editAddress: function () {
            formPopUpState.isVisible(true);
            this.showPopup();

        },

        /**
         * Show popup.
         */
        showPopup: function () {
            $('[data-open-modal="opc-new-shipping-address"]').trigger('click');
        }
    });
});
