/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('bss.pager', {

        _create: function () {
            $('.bss.pager a').on('click', function(e){
                e.preventDefault();
                $('#loadding-advancedreview').show();
                var postData = $("#cons-pros-fillter").serializeArray();
                var url = $(this).attr('href');
                $.ajax({
                    url : url,
                    type : "post",
                    data : postData,
                    dataType:"text",
                    success : function (result) {
                        if (result) {
                            $("#product-review-container").html(result);
                            $("#maincontent").trigger('contentUpdated');
                        }
                    }
                });
            });

            $('.bss.pager select').on('change', function(e){
                e.preventDefault();
                $('#loadding-advancedreview').show();
                var postData = $("#cons-pros-fillter").serializeArray();
                var url = this.value;
                $.ajax({
                    url : url,
                    type : "post",
                    data : postData,
                    dataType:"text",
                    success : function (result) {
                        if (result) {
                            $("#product-review-container").html(result);
                            $("#maincontent").trigger('contentUpdated');
                        }
                    }
                });
            });
        }
    });
    return $.bss.pager;
});