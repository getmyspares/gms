/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('bss.helpfulness', {

        _create: function () {
            $('.helpful-link').on('click', function(e){
                e.preventDefault();
                var url = $(this).attr("link");
                var parent = $(this).parent().parent(".review-helpfulness");
                $(this).hide();
                $(this).parent().find('img').show();
                $.ajax({
                    url : url,
                    type : "post",
                    dataType:"json",
                    success : function (result) {
                        if (result.type == 'success') {
                            $(parent).html(result.content);
                            $("#maincontent").trigger('contentUpdated');
                        }
                    }
                });
            })
        }
    });
    return $.bss.helpfulness;
});
