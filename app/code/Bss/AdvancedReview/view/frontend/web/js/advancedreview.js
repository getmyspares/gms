/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
function helpfulness(seft)
{
    url = jQuery(seft).attr("link");
    jQuery.ajax({
        url : url,
        type : "post",
        dataType:"json",
        success : function (result) {
            if (result.type == 'success') {
                html = "<div class='success message'><span>" + result.message + "</span></div>";
                el = jQuery(seft).parent().parent(".helpfulness");
                el.html(html);
                setTimeout(function () {
                    el.remove();
                },2000);
            } else if (result.type == 'error') {
                html = "<div class='error message'><span>" + result.message + "</span></div>";
                el = jQuery(seft).parent().parent(".helpfulness").find("error");
                setTimeout(function () {
                    el.hide();
                },2000);
            }
        }
    });
}

function ajaxLoad()
{
    jQuery('#loadding-advancedreview').show();
}

function abuseReport(seft)
{
    url = jQuery(seft).attr("link");
    jQuery.ajax({
        url : url,
        type : "post",
        dataType:"json",
        success : function (result) {
            if (result.type == 'success') {
                jQuery(seft).parent(".abuse_report").html(result.content);
            }
        }
    });
}

function updatePager(url)
{
    ajaxLoad();
    var postData = jQuery("#cons-pros-fillter").serializeArray();
    jQuery.ajax({
        url : url,
        type : "post",
        data : postData,
        dataType:"text",
        success : function (result) {
            if (result) {
                jQuery("#product-review-container").html(result);
            }
        }
    });
}

function submitForm(form)
{
    ajaxLoad();
    var postData = jQuery(form).serializeArray();
    var formURL = jQuery(form).attr("action");
    jQuery.ajax({
        url : formURL,
        type : "post",
        data : postData,
        dataType:"text",
        success : function (result) {
            if (result) {
                jQuery("#product-review-container").html(result);
            }
        }
    });
}