var config = {
    map: {
       '*': {
            'advancedreview': 'Bss_AdvancedReview/js/advancedreview',
            'report' : 'Bss_AdvancedReview/js/report',
            'helpfulness' : 'Bss_AdvancedReview/js/helpfulness',
            'review-form' : 'Bss_AdvancedReview/js/review-form',
            'pager' : 'Bss_AdvancedReview/js/pager',
        }
    },
    shim: {
        'advancedreview': {
            deps: ['jquery']
        }
    }
};
