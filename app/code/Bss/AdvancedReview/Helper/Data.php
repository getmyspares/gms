<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Helper;

use Magento\Store\Model\StoreManagerInterface as StoreId;
use Magento\Customer\Model\SessionFactory as CustomerSession;
use Bss\AdvancedReview\Model\ResourceModel\Helpfulness as Helpfulness;
use Bss\AdvancedReview\Model\ResourceModel\Report as Report;
use Bss\AdvancedReview\Model\Recommend as ModelRecommend;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_SECURE_IN_FRONTEND = 'web/secure/use_in_frontend';

    protected $_configSectionId = 'review_section';
    protected $storeId;
    protected $customerSession;
    protected $helpfulness;
    protected $report;
    protected $_modelRecommend;
    protected $_resigtry;
    protected $_orderConfig;
    protected $_modelOrder;
    protected $logger;
    protected $senderResolver;
    
    public function __construct(
        \Magento\Framework\Registry $resigtry,
        \Magento\Framework\App\Helper\Context $context,
        CustomerSession $customerSession,
        Helpfulness $helpfulness,
        StoreId $storeId,
        Report $report,
        ModelRecommend $modelRecommend,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $modelOrder,
        \Magento\Framework\Mail\Template\SenderResolverInterface $senderResolver
    ) {

        $this->_resigtry = $resigtry;
        $this->storeId = $storeId;
        $this->customerSession = $customerSession->create();
        $this->helpfulness = $helpfulness;
        $this->report = $report;
        $this->_modelRecommend = $modelRecommend;
        $this->_orderConfig = $orderConfig;
        $this->_modelOrder = $modelOrder;
        $this->logger = $context->getLogger();
        $this->senderResolver = $senderResolver;
        parent::__construct($context);
    }

    public function getConfig($path, $store = null, $scope = null)
    {
        if ($scope === null) {
            $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        }
        return $this->scopeConfig->getValue($path, $scope, $store);
    }

    public function moduleEnabled()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/general/advanced_enable');
    }

    public function helpfulnessEnable()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/helpfulness_option/helpfulness');
    }

    public function reportEnable()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/abuse_report/enable');
    }

    public function numberOfPros()
    {
        return (int)$this->getConfig($this->_configSectionId.'/pros_cons/pros_show_qty');
    }

    public function numberOfCons()
    {
        return (int)$this->getConfig($this->_configSectionId.'/pros_cons/cons_show_qty');
    }

    public function getCurrentStoreId()
    {
        return $this->storeId->getStore()->getId();
    }

    public function enableUserDefined()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/pros_cons/enable_user_defined');
    }

    public function allowGuestVoteHelpfulness()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/helpfulness_option/helpfulness_guest');
    }

    public function isUserLogged()
    {
        if ($this->customerSession->isLoggedIn()) {
            return $this->customerSession->getId();
        }
        return 0;
    }

    public function getCustomerName($customerId)
    {
        if ($customerId) {
            return $this->customerSession->getCustomer()->getName();
        } else {
            return "Guest";
        }
    }

    public function enableHelpfulness()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/helpfulness_option/helpfulness');
    }

    public function enableReport()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/abuse_report/enable');
    }
    public function allowGuestReport()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/abuse_report/report_guest');
    }

    public function registerHelpfulness($reviewId)
    {
        $reviews = $this->customerSession->getHelpfulnessReviews();
        $count = 0;
        if (is_array($reviews)) {
            $count = count($reviews);
        }
        
        if ($count) {
            $reviews[] = $reviewId;
        } else {
            $reviews = [$reviewId];
        }
        $this->customerSession->setHelpfulnessReviews($reviews);

        return $this;
    }

    public function isHelpfulnessRegistered($reviewId)
    {
        if ($userId = $this->customerSession->getCustomer()->getId()) {
            return $this->helpfulness->getCustomerHelpfulness($userId, $reviewId);
        } else {
            $reviews = $this->customerSession->getHelpfulnessReviews();
            if (isset($reviews)) {
                return in_array($reviewId, $reviews);
            }
            return false;
        }
    }

    public function registerReport($reviewId)
    {
        $reviews = $this->customerSession->getReportReviews();
        $count = 0;
        if (is_array($reviews)) {
            $count = count($reviews);
        }
        if ($count) {
            $reviews[] = $reviewId;
        } else {
            $reviews = [$reviewId];
        }
        $this->customerSession->setReportReviews($reviews);

        return $this;
    }

    public function unRegisterReport($reviewId)
    {
        $reviews = $this->customerSession->getReportReviews();
        if (($key = array_search($reviewId, $reviews)) !== false) {
            unset($reviews[$key]);
        }
        $this->customerSession->setReportReviews($reviews);
    }

    public function isReportRegistered($reviewId)
    {
        if ($userId = $this->customerSession->getCustomer()->getId()) {
            return $this->report->getCustomerReport($userId, $reviewId);
        } else {
            $reviews = $this->customerSession->getReportReviews();
            if (isset($reviews)) {
                return in_array($reviewId, $reviews);
            }
            return false;
        }
    }

    public function getAnswerOptions()
    {
        $html = "";
        $options = $this->getConfig($this->_configSectionId.'/recommend/answer_option');
        if ($options) {
            $options = unserialize($options);
            if (is_array($options)) {
                $html .= "";
                $i = 1;
                foreach ($options as $option) {
                    $html .= "<span><input id='recommend_field' class='input-text' name='recommend' type='radio' value='" . $i ."'><span>" . $option['answer'].'</span></span>';
                    $i++;
                }
                $html .= "";
            }
        }
        return $html;
    }

    public function recommendOptionArray($storeId = null)
    {
        if (!$storeId) {
            $storeId = $this->getCurrentStoreId();
        }
        $options = $this->getConfig($this->_configSectionId.'/recommend/answer_option', $storeId);
        $result = [];
        if ($options) {
            $options = unserialize($options);
            if (is_array($options)) {
                $i = 1;
                $result[0] = ['value' => '', 'label' => ''];
                foreach ($options as $option) {
                    $result[] = ['value' => $i, 'label' => $option['answer']];
                    $i++;
                }
            }
        }
        return $result;
    }

    public function getValueField($reviewId)
    {
        if ($this->enableRecommend()) {
            $options = $this->getConfig($this->_configSectionId.'/recommend/answer_option');
            if ($options) {
                $data = $this->_modelRecommend
                    ->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $reviewId])
                    ->getData();
                $count = count($data);
                if ($count) {
                    $index = (int)$data[0]['value'];
                    $options = unserialize($options);
                    $options = array_values($options);
                    $countOption = count($options);
                    if ($index !== 0 && $index <= $countOption) {
                        return $options[$index - 1]['answer'];
                    }
                }
            }
        }
        return false;
    }

    public function getValueRecommend($reviewId)
    {
        if ($this->enableRecommend()) {
            $options = $this->getConfig($this->_configSectionId.'/recommend/answer_option');
            if ($options) {
                $data = $this->_modelRecommend
                    ->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $reviewId])
                    ->getData();
                $count = count($data);
                if ($count) {
                    return (int)$data[0]['value'];
                }
            }
        }
        return false;
    }

    public function enableRecommend()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/recommend/enable');
    }

    public function valueAllowRecommend()
    {
        return (int)$this->getConfig($this->_configSectionId.'/recommend/recommend_allow');
    }

    public function canReview()
    {
        return $this->getConfig($this->_configSectionId.'/access_options/can_review');
    }

    public function oderStatus()
    {
        return $this->getConfig($this->_configSectionId.'/access_options/order_status');
    }

    public function isProductSold()
    {
        $productId = $this->_resigtry->registry('product')->getId();
        $customer_id = $this->customerSession->getCustomer()->getId();
        $orders = $this->_modelOrder->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('customer_id', $customer_id)
                ->addAttributeToFilter('status', ['in' => explode(',', $this->oderStatus())])
                ->addAttributeToSort('created_at', 'desc');
        
        foreach ($orders as $order) {
            $items = $order->getItemsCollection();
            foreach ($items as $item) {
                if ($productId === $item->getProductId()) {
                    return true;
                }
            }
        }
        return false;
    }

    public function enableSocialShare()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/social_share/enable');
    }

    public function getSocialSharing($url, $title)
    {
        if ($this->enableSocialShare()) {
            $options = $this->getConfig($this->_configSectionId.'/social_share/social');
            $options = unserialize($options);
            $options = array_values($options);
            $data = [];
            foreach ($options as $option) {
                if ($option['type'] == 'fb') {
                    $data[] = [
                        'url' => 'http://www.facebook.com/share.php?u=' . rawurlencode($url) . '&t=' . $title,
                        'img' => $option['img'],
                        'alt' => 'Facebook'
                        ];
                }
                if ($option['type'] == 'gg') {
                    $data[] = [
                        'url' => 'https://plus.google.com/share?url=' . rawurlencode($url),
                        'img' => $option['img'],
                        'alt' => 'Google+'
                        ];
                }
                if ($option['type'] == 'in') {
                    $data[] = [
                        'url' => 'https://www.linkedin.com/shareArticle?mini=true&url=' . rawurlencode($url) . '&title=' . $title,
                        'img' => $option['img'],
                        'alt' => 'LinkedIn'
                        ];
                }
                if ($option['type'] == 'tw') {
                    $data[] = [
                        'url' => 'https://twitter.com/intent/tweet?url=' . rawurlencode($url),
                        'img' => $option['img'],
                        'alt' => 'Twitter'
                        ];
                }
            }
            return $data;
        }
        return false;
    }

    public function enableSorting()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/sorting_filter/enable');
    }

    public function getAvailableLimmit()
    {
        $limits = explode(",", $this->getConfig($this->_configSectionId.'/sorting_filter/available_limmit_values'));
        $arrayLimit = [];
        foreach ($limits as $limit) {
            $arrayLimit[$limit] = $limit;
        }
        return $arrayLimit;
    }

    public function getSortBy()
    {
        $keys_array = explode(",", $this->getConfig($this->_configSectionId.'/sorting_filter/sorting'));
        $count = count($keys_array);
        if ($count) {
            $res_array = [];
            foreach (\Magento\Framework\App\ObjectManager::getInstance()->get('Bss\AdvancedReview\Model\Config\Source\Sorting')->toOptionArray() as $item) {
                if (in_array($item['value'], $keys_array)) {
                    if (!$this->enableHelpfulness() && $item['value'] == 'by_helpfulness') {
                        continue;
                    }
                    $res_array[] = ['value' => $item['value'], 'label' => __($item['label'])];
                }
            }
            return $res_array;
        } else {
            return false;
        }
    }

    public function getDefaultLimmit()
    {
        return (int)$this->getConfig($this->_configSectionId.'/sorting_filter/available_limmit');
    }

    public function getEnabledSendMail()
    {
        return $this->getConfig($this->_configSectionId.'/email_notification/enable');
    }

    public function getEmailSender()
    {
        $from = $this->getConfig($this->_configSectionId.'/email_notification/sender_name_identity');
        $result = $this->senderResolver->resolve($from);
        return $result['email'];
    }

    public function getEmailSenderName()
    {
        $from = $this->getConfig($this->_configSectionId.'/email_notification/sender_name_identity');
        $result = $this->senderResolver->resolve($from);
        return $result['name'];
    }

    public function sendNotification($review)
    {
        $product = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Catalog\Model\Product')->load($review->getEntityPkValue());
        $recipient = $this->getConfig($this->_configSectionId.'/email_notification/recipient_email');

        if ($recipient && $this->getEmailSenderName() && $this->getEmailSenderName() && $this->getEnabledSendMail()) {
            try {
                $sender = [
                            'name' => $this->getEmailSenderName(),
                            'email' => $this->getEmailSender()
                            ];

                $transport = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Mail\Template\TransportBuilder')
                ->setTemplateIdentifier($this->getConfig($this->_configSectionId.'/email_notification/email_template'))
                ->setTemplateOptions(
                    [
                        'area' => 'frontend',
                        'store' => $this->getCurrentStoreId(),
                    ]
                )
                ->setTemplateVars(
                    [
                        'product_name'  => $product->getName(),
                        'review_subject'=> $review->getTitle(),
                        'review_body'   => nl2br($review->getDetail()),
                        'review_id'     => $review->getReviewId(),
                        'nickname'      => $review->getNickname(),
                    ]
                )
                ->setFrom($sender)
                ->addTo($recipient)
                ->setReplyTo($this->getEmailSender())
                ->getTransport();
                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());
            }
        }
        return $this;
    }

    public function getSiteKey()
    {
        return  $this->getConfig($this->_configSectionId .'/recaptcha/site_key');
    }
    
    public function getSecretKey()
    {
        return  $this->getConfig($this->_configSectionId .'/recaptcha/secret_key');
    }

    public function enableAntispam()
    {
        return  $this->getConfig($this->_configSectionId .'/recaptcha/enable');
    }

    public function enableFillter()
    {
        return  $this->getConfig($this->_configSectionId .'/sorting_filter/filter');
    }

    public function allowFillterPros()
    {
        return  $this->getConfig($this->_configSectionId .'/sorting_filter/filter_pros');
    }

    public function allowFillterCons()
    {
        return  $this->getConfig($this->_configSectionId .'/sorting_filter/filter_cons');
    }
}
