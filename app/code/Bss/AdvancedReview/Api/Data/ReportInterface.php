<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Api\Data;
 
interface ReportInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'id';
    const STORE_ID = 'store_id';
    const REVIEW_ID = 'review_id';
    const CUSTOMER_NAME = 'customer_name';
    const ABUSED_AT = 'abused_at';
 
    /**
     * Get Id.
     *
     * @return int
     */
    public function getId();
 
    /**
     * Set Id.
     */
    public function setId($id);
 
    /**
     * Get StoreId.
     *
     * @return int
     */
    public function getStoreId();
 
    /**
     * Set StoreId.
     */
    public function setStoreId($storeId);
 
    /**
     * Get ReviewId.
     *
     * @return int
     */
    public function getReviewId();
 
    /**
     * Set ReviewId.
     */
    public function setReviewId($reviewId);
 
    /**
     * Get CustomerName.
     *
     * @return varchar
     */
    public function getCustomerName();
 
    /**
     * Set CustomerName.
     */
    public function setCustomerName($customerName);

    /**
     * Get AbuseAt.
     *
     * @return date time
     */
    public function getAbuseAt();
 
    /**
     * Set AbuseAt.
     */
    public function setAbuseAt($abuseAt);
}
