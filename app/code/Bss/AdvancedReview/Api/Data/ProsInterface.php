<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Api\Data;
 
interface ProsInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'id';
    const NAME = 'name';
    const STATUS = 'status';
    const OWNER = 'owner';
    const STORE_ID = 'store_id';
 
    /**
     * Get Id.
     *
     * @return int
     */
    public function getId();
 
    /**
     * Set Id.
     */
    public function setId($id);
 
    /**
     * Get Name.
     *
     * @return varchar
     */
    public function getName();
 
    /**
     * Set Name.
     */
    public function setName($name);
 
    /**
     * Get Status.
     *
     * @return int
     */
    public function getStatus();
 
    /**
     * Set Status.
     */
    public function setStatus($status);
 
    /**
     * Get Owner.
     *
     * @return int
     */
    public function getOwner();
 
    /**
     * Set Owner.
     */
    public function setOwner($owner);
 
    /**
     * Get StoreId.
     *
     * @return int
     */
    public function getStoreId();
 
    /**
     * Set StoreId.
     */
    public function setStoreId($storeId);
}
