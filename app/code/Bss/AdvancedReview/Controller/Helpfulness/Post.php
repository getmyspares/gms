<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Controller\Helpfulness;

use Bss\AdvancedReview\Helper\Data as Helper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class Post extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory = false;

    protected $helper;

    protected $customerSession;

    protected $jsonHelper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Helper $helper,
        CustomerSession $customerSession,
        JsonHelper $jsonHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
        $this->customerSession = $customerSession;
        $this->jsonHelper = $jsonHelper;
    }

    public function execute()
    {
        $vote = $this->getRequest()->getParam('vote');
        $reviewId = $this->getRequest()->getParam('reviewId');
        $resultPage = $this->resultPageFactory->create();

        if (!$this->helper->allowGuestVoteHelpfulness() && !$this->helper->isUserLogged()) {
            return $this->getResponse()->setBody($this->jsonHelper->jsonEncode(['type' => 'error', 'message' => __('You need log in to vote review')]));
        } elseif ($this->helper->isHelpfulnessRegistered($reviewId)) {
            return $this->getResponse()->setBody($this->jsonHelper->jsonEncode(['type' => 'error', 'message' => __('You can vote only once for the same review')]));
        } else {
            $value = '';
            if ($vote == 'Yes') {
                $value = 1;
            } elseif ($vote == 'No') {
                $value = 0;
            }
            if ($value !== '') {
                $data = [];
                $data['review_id'] = $reviewId;
                $data['customer_id'] = $this->customerSession->getId();
                $data['value'] = $value;
                $rowData = \Magento\Framework\App\ObjectManager::getInstance()->create('Bss\AdvancedReview\Model\Helpfulness');
                $rowData->setData($data);
                $rowData->save();
                $this->helper->registerHelpfulness($reviewId);
                $result['type'] = 'success';
                $result['content'] = $resultPage->getLayout()
                            ->createBlock('Bss\AdvancedReview\Block\Helpfulness')
                            ->setReviewId($reviewId)
                            ->setTemplate('Bss_AdvancedReview::product/view/list/helpfulness.phtml')
                            ->toHtml();
                return $this->getResponse()->setBody($this->jsonHelper->jsonEncode($result));
            }
        }
    }
}
