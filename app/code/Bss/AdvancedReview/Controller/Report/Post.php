<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Controller\Report;

use Bss\AdvancedReview\Helper\Data as Helper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class Post extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory = false;

    protected $helper;

    protected $customerSession;

    protected $jsonHelper;

    protected $date;

    protected $report;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Helper $helper,
        CustomerSession $customerSession,
        JsonHelper $jsonHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Bss\AdvancedReview\Model\Report $report,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
        $this->customerSession = $customerSession;
        $this->jsonHelper = $jsonHelper;
        $this->date = $date;
        $this->report = $report;
    }

    public function execute()
    {
        $vote = $this->getRequest()->getParam('vote');
        $reviewId = $this->getRequest()->getParam('reviewId');
        $resultPage = $this->resultPageFactory->create();
        $result = [];
        if ($vote == 'Yes') {
            if (!$this->helper->allowGuestReport() && !$this->helper->isUserLogged()) {
                return $this->getResponse()->setBody($this->jsonHelper->jsonEncode(['type' => 'error', 'message' => __('You need log in to report review')]));
            } elseif ($this->helper->isReportRegistered($reviewId)) {
                return $this->getResponse()->setBody($this->jsonHelper->jsonEncode(['type' => 'error', 'message' => __('You can report only once for the same review')]));
            } else {
                $data = [];
                $data['review_id'] = $reviewId;
                $data['customer_id'] = $this->customerSession->getCustomer()->getId();
                $data['customer_name'] = $this->helper->getCustomerName($data['customer_id']);
                $data['abuse_at'] = $this->date->gmtDate();
                $data['store_id'] = $this->helper->getCurrentStoreId();
                $rowData = $this->report;
                $rowData->setData($data);
                $rowData->save();
                $this->helper->registerReport($reviewId);
                $result['type'] = 'success';
            }
        } elseif ($vote == 'No') {
            if (!$this->helper->allowGuestReport() && !$this->helper->isUserLogged()) {
                return $this->getResponse()->setBody($this->jsonHelper->jsonEncode(['type' => 'error', 'message' => __('You need log in to report review')]));
            } elseif (!$this->helper->isReportRegistered($reviewId)) {
                return $this->getResponse()->setBody($this->jsonHelper->jsonEncode(['type' => 'error', 'message' => __("You can't report this review")]));
            } else {
                $collections = $this->report
                        ->getCollection()
                        ->addFieldToFilter('review_id', ['eq' => $reviewId])
                        ->addFieldToFilter('customer_id', ['eq' => $this->customerSession->getCustomer()->getId()]);
                foreach ($collections as $collection) {
                    $this->deleteReport($collection);
                }
                $this->helper->unRegisterReport($reviewId);
                $result['type'] = 'success';
            }
        }
        $result['content'] = $resultPage->getLayout()
                            ->createBlock('Bss\AdvancedReview\Block\Report')
                            ->setReviewId($reviewId)
                            ->setTemplate('Bss_AdvancedReview::product/view/list/report.phtml')
                            ->toHtml();
        return $this->getResponse()->setBody($this->jsonHelper->jsonEncode($result));
    }

    protected function deleteReport($collection)
    {
        $this->report->setId($collection->getId())->delete();
    }
}
