<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Controller\Adminhtml\Pros;
 
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Bss\AdvancedReview\Model\ResourceModel\Pros\CollectionFactory;
use Bss\AdvancedReview\Model\Pros as ModelPros;
 
class MassUpdate extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter.
     *
     * @var Filter
     */
    protected $_filter;
 
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;
 
    /**
     * @param Context           $context
     * @param Filter            $filter
     * @param CollectionFactory $collectionFactory
     */

    protected $_modelPros;
    
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ModelPros $modelPros
    ) {
    
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_modelPros = $modelPros;
        parent::__construct($context);
    }
 
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $status = (int) $this->getRequest()->getParam('status');
        $recordUpdate = 0;
        foreach ($collection->getItems() as $auctionProduct) {
            $this->updatePros($auctionProduct, $status);
            $recordUpdate++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been updated.', $recordUpdate)
        );
 
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }

    protected function updatePros($auctionProduct, $status)
    {
        $this->_modelPros->load($auctionProduct->getId());
        $this->_modelPros->setStatus($status);
        $this->_modelPros->save();
    }
 
    /**
     * Check update Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_AdvancedReview::advanced_review_edit');
    }
}
