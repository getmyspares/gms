<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Controller\Adminhtml\Cons;
 
class Save extends \Magento\Backend\App\Action
{
    protected $rowData;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Bss\AdvancedReview\Model\Cons $rowData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Bss\AdvancedReview\Model\Cons $rowData
    ) {
    
        parent::__construct($context);
        $this->rowData = $rowData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('advanced_review/cons/addRow');
            return;
        }
        try {
            $data['store_id'] = implode(",", $data['store_id']);
            $this->rowData->setData($data);
            if (isset($data['id'])) {
                $this->rowData->setId($data['id']);
            }
            $this->rowData->save();
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('advanced_review/cons/index');
    }
 
    /**
     * Check Category Map permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_AdvancedReview::advanced_review_edit');
    }
}
