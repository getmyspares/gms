<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Review\Model\Review as Review;
use Bss\AdvancedReview\Model\Recommend as Recommend;

class Summary extends \Magento\Framework\View\Element\Template
{
    /**
     * @var ReviewRendererInterface
     */
    protected $reviewRenderer;

    protected $modelReview;

    protected $resourceReview;

    protected $product;

    protected $reviewFactory;

    protected $storeManager;

    protected $modelRecommend;

    protected $helper;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $contextProduct,
        Context $context,
        Review $modelReview,
        Recommend $modelRecommend,
        \Bss\AdvancedReview\Model\ResourceModel\Review $resourceReview,
        \Magento\Catalog\Model\Product $product,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Bss\AdvancedReview\Helper\Data $helper,
        array $data = []
    ) {
        $this->reviewRenderer = $contextProduct->getReviewRenderer();
        $this->reviewFactory = $reviewFactory;
        $this->product = $product;
        $this->modelReview = $modelReview;
        $this->resourceReview = $resourceReview;
        $this->storeManager = $context->getStoreManager();
        $this->modelRecommend = $modelRecommend;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    public function getSummaryBlock()
    {
        $productId = (int)$this->getRequest()->getParam('id');
        return $this->resourceReview->getSummaryBlock($productId);
    }

    public function countSummaryBlock()
    {
        return count($this->getSummaryBlock());
    }

    public function getRatingSummary()
    {
        $productId = (int)$this->getRequest()->getParam('id');
        $product = $this->product->load($productId);
        $this->reviewFactory->create()->getEntitySummary($product, $this->storeManager->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();

        return $ratingSummary/20;
    }

    public function getReviewsSummaryHtml()
    {
        $productId = (int)$this->getRequest()->getParam('id');
        $product = $this->product->load($productId);
        return $this->reviewRenderer->getReviewsSummaryHtml($product, false, false);
    }

    public function getTotalReview()
    {
        $productId = (int)$this->getRequest()->getParam('id');
        $collection = $this->modelReview
                ->getCollection()
                ->addFieldToFilter('status_id', ['eq' => 1])
                ->addFieldToFilter('entity_pk_value', ['eq' => $productId]);
        $result = [];
        foreach ($collection as $col) {
            array_push($result, $col->getReviewId());
        }
        return $result;
    }

    public function getReviewRecommend()
    {
        $collection = $this->modelRecommend
                ->getCollection()
                ->addFieldToFilter('review_id', ['in' => $this->getTotalReview()])
                ->addFieldToFilter('value', ['eq' => $this->helper->valueAllowRecommend()]);
        return count($collection);
    }

    public function getReviewsCount()
    {
        $productId = (int)$this->getRequest()->getParam('id');
        $collection = $this->modelReview
                ->getCollection()
                ->addFieldToFilter('status_id', ['eq' => 1])
                ->addFieldToFilter('entity_pk_value', ['eq' => $productId]);
        return count($collection);
    }
}
