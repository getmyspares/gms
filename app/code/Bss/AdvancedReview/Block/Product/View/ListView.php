<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block\Product\View;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;
use Bss\AdvancedReview\Model\ReviewProsCons as ReviewProsCons;

class ListView extends \Magento\Review\Block\Product\View\ListView
{
    protected $reviewProsCons;

    protected $helper;

    protected $modelPros;

    protected $modelCons;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory,
        ReviewProsCons $reviewProsCons,
        \Bss\AdvancedReview\Helper\Data $helper,
        \Bss\AdvancedReview\Model\Pros $modelPros,
        \Bss\AdvancedReview\Model\Cons $modelCons,
        array $data = []
    ) {
        $this->reviewProsCons = $reviewProsCons;
        $this->helper = $helper;
        $this->modelPros = $modelPros;
        $this->modelCons = $modelCons;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $collectionFactory,
            $data
        );
    }

    public function getReviewsCollection()
    {
        if (null === $this->_reviewsCollection) {
            $this->_reviewsCollection = $this->_reviewsColFactory->create()->addStoreFilter(
                $this->_storeManager->getStore()->getId()
            )->addStatusFilter(
                \Magento\Review\Model\Review::STATUS_APPROVED
            )->addEntityFilter(
                'product',
                $this->getProduct()->getId()
            )
            ->fillterPros()
            ->fillterCons()
            ->setDateOrder();
        }
        return $this->_reviewsCollection;
    }

    public function fillterProsSelected($prosId)
    {
        $post = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface')->getPost();
        if ($post['pros']) {
            return array_key_exists($prosId, $post['pros']);
        }
        return false;
    }

    public function fillterConsSelected($consId)
    {
        $post = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface')->getPost();
        if ($post['cons']) {
            return array_key_exists($consId, $post['cons']);
        }
        return false;
    }

    public function getReviewIds()
    {
        $post = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface')->getPost();
        if ($post['cons'] || $post['pros']) {
            $reviewCollections = $this->_reviewsCollection;
        } else {
            $reviewCollections = $this->_reviewsColFactory->create()->addStoreFilter(
                $this->_storeManager->getStore()->getId()
            )->addStatusFilter(
                \Magento\Review\Model\Review::STATUS_APPROVED
            )->addEntityFilter(
                'product',
                $this->getProduct()->getId()
            );
        }
        $ids = [];
        foreach ($reviewCollections as $review) {
            if (!in_array($review->getId(), $ids)) {
                $ids[] = $review->getId();
            }
        }
        return $ids;
    }

    public function getProsCollection()
    {
        $collections = $this->reviewProsCons
                    ->getCollection()
                    ->addFieldToFilter('review_id', ['in' => $this->getReviewIds()])
                    ->addFieldToFilter('cons_id', ['eq' => 0]);
        $ids = [];
        foreach ($collections as $pros) {
            if (!in_array($pros->getProsId(), $ids)) {
                $ids[] = $pros->getProsId();
            }
        }
        return $ids;
    }

    public function getConsCollection()
    {
        $collections = $this->reviewProsCons
                    ->getCollection()
                    ->addFieldToFilter('review_id', ['in' => $this->getReviewIds()])
                    ->addFieldToFilter('pros_id', ['eq' => 0]);
        $ids = [];
        foreach ($collections as $cons) {
            if (!in_array($cons->getConsId(), $ids)) {
                $ids[] = $cons->getConsId();
            }
        }
        return $ids;
    }

    public function getHelper()
    {
        return $this->helper;
    }

    public function loadPros($prosId)
    {
        return $this->modelPros->load($prosId);
    }

    public function loadCons($consId)
    {
        return $this->modelCons->load($consId);
    }

    public function getUrlHelpfulness($reviewId)
    {
        return $this->getUrl('advanced_review/helpfulness/ajax', ['reviewId' => $reviewId]);
    }

    public function getUrlReport($reviewId)
    {
        return $this->getUrl('advanced_review/report/ajax', ['reviewId' => $reviewId]);
    }
}
