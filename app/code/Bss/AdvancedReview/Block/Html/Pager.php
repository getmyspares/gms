<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block\Html;

use Bss\AdvancedReview\Helper\Data as Helper;
use Magento\Framework\View\Element\Template\Context;

/**
 * Html pager block
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Pager extends \Magento\Theme\Block\Html\Pager
{
    protected $helper;
    /**
     * Current template name
     *
     * @var string
     */

    protected $_template = 'Bss_AdvancedReview::html/pager.phtml';

    protected $_type = "date";

    protected $_dir = "DESC";

    public function __construct(
        Helper $helper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * Retrieve pager limit
     *
     * @return array
     */
    public function getAvailableLimit()
    {
        return $this->helper->getAvailableLimmit();
    }

    /**
     * Return current page limit
     *
     * @return int
     */
    public function getLimit()
    {
        if ($this->_limit !== null) {
            return $this->_limit;
        }

        $limits = $this->getAvailableLimit();
        if ($limit = $this->getRequest()->getParam($this->getLimitVarName())) {
            if (isset($limits[$limit])) {
                return $limit;
            }
        }

        if ($this->helper->getDefaultLimmit()) {
            return $this->helper->getDefaultLimmit();
        }

        $limits = array_keys($limits);
        return $limits[0];
    }

    /**
     * @param string $type
     * @return string
     */
    public function getSortByUrl($type)
    {
        return $this->getPagerUrl(['type' => $type]);
    }

    /**
     * @param int $limit
     * @return string
     */
    public function getLimitUrl($limit)
    {
        return $this->getPagerUrl([$this->getLimitVarName() => $limit]);
    }

    public function getType()
    {
        if ($type = $this->getRequest()->getParam('type')) {
            return $type;
        }
        return $this->_type;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function isTypeCurrent($type)
    {
        return $type == $this->getType();
    }

    public function getDir()
    {
        if ($dir = $this->getRequest()->getParam('dir')) {
            if ($dir === 'ASC') {
                return 'DESC';
            }
            return 'ASC';
        }
        if ($this->_dir === 'ASC') {
            return 'DESC';
        }
        return 'ASC';
    }

    public function getSortDirUrl($dir)
    {
        return $this->getPagerUrl(['dir' => $dir]);
    }

    public function getHelper()
    {
        return $this->helper;
    }
}
