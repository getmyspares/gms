<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block;

use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Context;
use Magento\Customer\Model\Url;
use Magento\Review\Model\ResourceModel\Rating\Collection as RatingCollection;

class Form extends \Magento\Review\Block\Form
{
    protected $helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Review\Helper\Data $reviewData,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Url $customerUrl,
        array $data = [],
        \Bss\AdvancedReview\Helper\Data $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context, $urlEncoder, $reviewData, $productRepository, $ratingFactory, $messageManager, $httpContext, $customerUrl, $data);
    }

    protected function _construct()
    {
        parent::_construct();

        if ($this->helper->moduleEnabled()) {
            $this->setAllowWriteReviewFlag(true);
        
            if ($this->helper->canReview() == 0) {
                $this->setTemplate('form.phtml');
            } elseif ($this->helper->canReview() == 1) {
                if ($this->httpContext->getValue(Context::CONTEXT_AUTH)) {
                    $this->setTemplate('form.phtml');
                } else {
                    $queryParam = $this->urlEncoder->encode(
                        $this->getUrl('*/*/*', ['_current' => true]) . '#review-form'
                    );
                    $this->setLoginLink(
                        $this->getUrl(
                            'customer/account/login/',
                            [Url::REFERER_QUERY_PARAM_NAME => $queryParam]
                        )
                    );
                    $this->setTemplate('form/needLogin.phtml');
                }
            } elseif ($this->helper->canReview() == 2) {
                if (!$this->httpContext->getValue(Context::CONTEXT_AUTH)) {
                    $queryParam = $this->urlEncoder->encode(
                        $this->getUrl('*/*/*', ['_current' => true]) . '#review-form'
                    );
                    $this->setLoginLink(
                        $this->getUrl(
                            'customer/account/login/',
                            [Url::REFERER_QUERY_PARAM_NAME => $queryParam]
                        )
                    );
                    $this->setTemplate('form/needLogin.phtml');
                } elseif (!$this->helper->isProductSold()) {
                    $this->setTemplate('form/needBuy.phtml');
                } else {
                    $this->setTemplate('form.phtml');
                }
            }
        }
    }
}
