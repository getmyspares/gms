<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block;

use Magento\Framework\View\Element\Template\Context;
use Bss\AdvancedReview\Model\Pros as Model;
use Bss\AdvancedReview\Model\ReviewProsCons as ReviewProsConsModel;
use Bss\AdvancedReview\Helper\Data as Helper;

class Pros extends \Magento\Framework\View\Element\Template
{
    protected $model;

    protected $reviewId;

    protected $reviewProsConsModel;

    protected $helper;

    public function __construct(
        Context $context,
        Model $model,
        ReviewProsConsModel $reviewProsConsModel,
        Helper $helper,
        array $data = []
    ) {
        $this->model = $model;
        $this->helper = $helper;
        $this->reviewProsConsModel = $reviewProsConsModel;
        parent::__construct($context, $data);
    }

    public function getCollection()
    {
        return $this->model
                    ->getCollection()
                    ->addFieldToFilter('status', ['eq' => 1]);
    }

    public function getProsOptionArray()
    {
        $result = [];
        foreach ($this->getCollection() as $pros) {
            $result[] = ['value' => $pros->getId(), 'label' => $pros->getName()];
        }

        return $result;
    }

    public function setReviewId($reviewId)
    {
        $this->reviewId = $reviewId;
        return $this;
    }

    public function getReviewId()
    {
        return $this->reviewId;
    }

    public function getProsReview()
    {
        return $this->reviewProsConsModel
                    ->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $this->reviewId])
                    ->addFieldToFilter('cons_id', ['eq' => 0]);
    }

    public function getNameById($id)
    {
        $storeId = $this->helper->getCurrentStoreId();
        $pros = $this->model->load($id);
        $prosStore = $pros->getStoreId();
        if (in_array($storeId, $prosStore)) {
            if ($pros->getStatus()) {
                return $pros->getName();
            }
        }
        return false;
    }

    public function getHelper()
    {
        return $this->helper;
    }
}
