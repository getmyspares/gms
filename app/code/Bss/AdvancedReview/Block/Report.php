<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block;

use Magento\Framework\View\Element\Template\Context;
use Bss\AdvancedReview\Helper\Data as Helper;
use Bss\AdvancedReview\Model\Report as Model;

class Report extends \Magento\Framework\View\Element\Template
{
    protected $helper;

    protected $_reviewId;

    protected $model;


    public function __construct(
        Context $context,
        Helper $helper,
        Model $model,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->model = $model;
        parent::__construct($context, $data);
    }

    public function setReviewId($reviewId)
    {
        $this->_reviewId = $reviewId;
        return $this;
    }

    public function getReviewId()
    {
        return $this->_reviewId;
    }

    public function canReport()
    {
        if ($this->helper->allowGuestReport()) {
            return !($this->helper->isReportRegistered($this->_reviewId));
        } else {
            return ($this->helper->isUserLogged() && !($this->helper->isReportRegistered($this->_reviewId)));
        }
    }

    public function isReportRegistered()
    {
        return $this->helper->allowGuestReport() || $this->helper->isUserLogged();
    }

    public function getAction($vote = 'None')
    {
        return $this->getUrl('advanced_review/report/post', ['reviewId' => $this->_reviewId, 'vote' => $vote]);
    }
}
