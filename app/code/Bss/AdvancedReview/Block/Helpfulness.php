<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Block;

use Magento\Framework\View\Element\Template\Context;
use Bss\AdvancedReview\Helper\Data as Helper;
use Bss\AdvancedReview\Model\Helpfulness as Model;

class Helpfulness extends \Magento\Framework\View\Element\Template
{
    protected $helper;

    protected $reviewId;

    protected $model;


    public function __construct(
        Context $context,
        Helper $helper,
        Model $model,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->model = $model;
        parent::__construct($context, $data);
    }

    public function setReviewId($reviewId)
    {
        $this->reviewId = $reviewId;
        return $this;
    }

    public function getReviewId()
    {
        return $this->reviewId;
    }

    public function canShowHelpfulnessLink()
    {
        if ($this->helper->allowGuestVoteHelpfulness()) {
            return !($this->helper->isHelpfulnessRegistered($this->reviewId));
        } else {
            return ($this->helper->isUserLogged() && !($this->helper->isHelpfulnessRegistered($this->reviewId)));
        }
    }

    public function getAllCount()
    {
        $reviews = $this->model->getCollection()->addFieldToFilter('review_id', ['eq' => $this->reviewId]);
        return count($reviews);
    }

    public function getYesCount()
    {
        $yesReviews = $this->model->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $this->reviewId])
                    ->addFieldToFilter('value', ['eq' => 1]);
        return count($yesReviews);
    }

    public function getAction($vote = 'None')
    {
        return $this->getUrl('advanced_review/helpfulness/post', ['reviewId' => $this->reviewId, 'vote' => $vote]);
    }
}
