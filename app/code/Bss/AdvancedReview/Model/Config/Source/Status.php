<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model\Config\Source;

class Status implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(\Magento\Sales\Model\Order\Config $orderConfig)
    {
         $this->_orderConfig = $orderConfig;
    }

    public function toOptionArray()
    {

        $statuses = $this->_orderConfig->getStatuses();
        $order_array = [];
        foreach ($statuses as $code => $name) {
            if (in_array($code, ['pending','holded','complete', 'processing'])) {
                $order_array[] = ['value'=>$code,'label'=>$name];
            }
        }

        return $order_array;
    }
}
