<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model;

class Recaptcha extends \Magento\Framework\Model\AbstractModel
{
    const REQUEST_URL = 'https://www.google.com/recaptcha/api/siteverify';

    protected $client;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Zend_Http_Client $client,
        array $data = []
    ) {
        $this->client = $client;
        parent::__construct($context, $registry, null, null, $data);
    }

    public function _construct()
    {
        $this->_helper = \Magento\Framework\App\ObjectManager::getInstance()->get('Bss\AdvancedReview\Helper\Data');
        $this->_remoteip = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
    }

    public function verify($recaptcha_response)
    {
        $params = [
            'secret'   => $this->_helper->getSecretKey(),
            'response' => $recaptcha_response,
            'remoteip' => $this->_remoteip->getRemoteAddress(),
        ];
        
        $client = $this->getHttpClient();
        $client->setParameterPost($params);
        $errors = '';

        try {
            $response = $client->request('POST');
            $data = json_decode($response->getBody());
            if (array_key_exists('error-codes', $data)) {
                $errors = $data['error-codes'];
            }
        } catch (\Exception $e) {
            $data = ['success' => false];
        }

        return $errors;
    }
    public function setHttpClient(Varien_Http_Client $client)
    {
        $this->client = $client;
        
        return $this;
    }

    public function getHttpClient()
    {
                
        $this->client->setUri(self::REQUEST_URL);

        return $this->client;
    }
}
