<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model;

use Bss\AdvancedReview\Api\Data\ReviewProsConsInterface;

class ReviewProsCons extends \Magento\Framework\Model\AbstractModel implements ReviewProsConsInterface
{
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\AdvancedReview\Model\ResourceModel\ReviewProsCons');
    }

    protected $_cacheTag = 'bss_review_pros_cons';
 
    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'bss_review_pros_cons';
 
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }
 
    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
 
    /**
     * Get ReviewId.
     *
     * @return int
     */
    public function getReviewId()
    {
        return $this->getData(self::REVIEW_ID);
    }
 
    /**
     * Set ReviewId.
     */
    public function setReviewId($reviewId)
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }

    /**
     * Get ConsId.
     *
     * @return int
     */
    public function getConsId()
    {
        return $this->getData(self::CONS_ID);
    }
 
    /**
     * Set ConsId.
     */
    public function setConsId($consId)
    {
        return $this->setData(self::CONS_ID, $consId);
    }

    /**
     * Get ProsId.
     *
     * @return int
     */
    public function getProsId()
    {
        return $this->getData(self::PROS_ID);
    }
 
    /**
     * Set ProsId.
     */
    public function setProsId($prosId)
    {
        return $this->setData(self::PROS_ID, $prosId);
    }

    public function getSelectPros($reviewId)
    {
        $collection = $this->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $reviewId])
                    ->addFieldToFilter('cons_id', ['eq' => 0]);
        $result = [];
        foreach ($collection as $col) {
            $result[] = $col->getProsId();
        }
        return $result;
    }

    public function getSelectCons($reviewId)
    {
        $collection = $this->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $reviewId])
                    ->addFieldToFilter('pros_id', ['eq' => 0]);
        $result = [];
        foreach ($collection as $col) {
            $result[] = $col->getConsId();
        }
        return $result;
    }
}
