<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model;

use Bss\AdvancedReview\Api\Data\ProsInterface;

class Pros extends \Magento\Framework\Model\AbstractModel implements ProsInterface
{
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\AdvancedReview\Model\ResourceModel\Pros');
    }

    protected $_cacheTag = 'bss_pros';
 
    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'bss_pros';
 
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }
 
    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
 
    /**
     * Get Name.
     *
     * @return varchar
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }
 
    /**
     * Set Name.
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
 
    /**
     * Get Type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }
 
    /**
     * Set Type.
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }
 
    /**
     * Get Status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }
 
    /**
     * Set Status.
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
 
    /**
     * Get Owner.
     *
     * @return int
     */
    public function getOwner()
    {
        return $this->getData(self::OWNER);
    }
 
    /**
     * Set Owner.
     */
    public function setOwner($owner)
    {
        return $this->setData(self::OWNER, $owner);
    }
 
    /**
     * Get SortOrder.
     *
     * @return int
     */
    public function getStoreId()
    {
        return explode(",", $this->getData(self::STORE_ID));
    }
 
    /**
     * Set StoreId.
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }
}
