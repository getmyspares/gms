<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model;

use Bss\AdvancedReview\Api\Data\HelpfulnessInterface;

class Helpfulness extends \Magento\Framework\Model\AbstractModel implements HelpfulnessInterface
{
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\AdvancedReview\Model\ResourceModel\Helpfulness');
    }

 
    /**
     * Get ReviewId.
     *
     * @return int
     */
    public function getReviewId()
    {
        return $this->getData(self::REVIEW_ID);
    }

    /**
     * Set ReviewId.
     */
    public function setReviewId($reviewId)
    {
        return $this->getData(self::REVIEW_ID, $reviewId);
    }

    /**
     * Get Value.
     *
     * @return int
     */
    public function getValue()
    {
        return $this->getData(self::VALUE);
    }

    /**
     * Set Value.
     */
    public function setValue($value)
    {
        return $this->getData(self::VALUE, $value);
    }

    /**
     * Get CustomerId.
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId.
     */
    public function setCustomerId($customerId)
    {
        return $this->getData(self::CUSTOMER_ID, $customerId);
    }
}
