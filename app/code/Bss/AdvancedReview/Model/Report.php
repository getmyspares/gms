<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model;

use Bss\AdvancedReview\Api\Data\ReportInterface;

class Report extends \Magento\Framework\Model\AbstractModel implements ReportInterface
{
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\AdvancedReview\Model\ResourceModel\Report');
    }

    protected $_cacheTag = 'bss_abuse';
 
    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'bss_abuse';
 
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }
 
    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get StoreId.
     *
     * @return int
     */
    public function getStoreId()
    {
        return explode(",", $this->getData(self::STORE_ID));
    }
 
    /**
     * Set StoreId.
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }
 
    /**
     * Get ReviewId.
     *
     * @return varchar
     */
    public function getReviewId()
    {
        return $this->getData(self::REVIEW_ID);
    }
 
    /**
     * Set ReviewId.
     */
    public function setReviewId($reviewId)
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }
 
    /**
     * Get CustomerName.
     *
     * @return int
     */
    public function getCustomerName()
    {
        return $this->getData(self::CUSTOMER_NAME);
    }
 
    /**
     * Set CustomerName.
     */
    public function setCustomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME, $customerName);
    }
 
    /**
     * Get AbuseAt.
     *
     * @return int
     */
    public function getAbuseAt()
    {
        return $this->getData(self::ABUSED_AT);
    }
 
    /**
     * Set AbuseAt.
     */
    public function setAbuseAt($abuseAt)
    {
        return $this->setData(self::ABUSED_AT, $abuseAt);
    }
}
