<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Bss\AdvancedReview\Model\Pros as ModelPros;
use Bss\AdvancedReview\Model\ResourceModel\Pros\CollectionFactory as ProsCollection;
use Bss\AdvancedReview\Model\Cons as ModelCons;
use Bss\AdvancedReview\Model\ResourceModel\Cons\CollectionFactory as ConsCollection;
use Bss\AdvancedReview\Model\ReviewProsCons as ModelReviewProsCons;
use Bss\AdvancedReview\Model\Recommend as ModelRecommend;

class Review extends \Magento\Review\Model\ResourceModel\Review
{
    const STATUS_NOT_APPROVED = 1;

    protected $_resigtry;

    protected $_dataHelper;

    protected $clearCache =[];

    protected $logger;

    protected $_modelPros;

    protected $_modelCons;

    protected $_modelReviewProsCons;

    protected $_modelRecommend;

    protected $_resource;

    protected $modelVote;

    protected $model;

    protected $state;

    protected $prosCollection;

    protected $consCollection;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Review\Model\ResourceModel\Rating\Option $ratingOptions,
        \Magento\Framework\Registry $resigtry,
        \Bss\AdvancedReview\Helper\Data $dataHelper,
        \Psr\Log\LoggerInterface $logger,
        ModelPros $modelPros,
        ModelCons $modelCons,
        ModelReviewProsCons $modelReviewProsCons,
        \Magento\Review\Model\Rating $model,
        \Magento\Review\Model\Rating\Option\Vote $modelVote,
        ModelRecommend $modelRecommend,
        \Magento\Framework\App\State $state,
        ProsCollection $prosCollection,
        ConsCollection $consCollection
    ) {
        $this->_resigtry            = $resigtry;
        $this->_dataHelper          = $dataHelper;
        $this->logger              = $logger;
        $this->_modelPros           = $modelPros;
        $this->_modelCons           = $modelCons;
        $this->_modelReviewProsCons = $modelReviewProsCons;
        $this->_modelRecommend      = $modelRecommend;
        $this->modelVote = $modelVote;
        $this->model = $model;
        $this->state = $state;
        $this->prosCollection = $prosCollection;
        $this->consCollection = $consCollection;

        parent::__construct($context, $date, $storeManager, $ratingFactory, $ratingOptions, null);
    }

    private function is_admin()
    {
        return 'adminhtml' === $this->state->getAreaCode();
    }

    protected function _afterSave(AbstractModel $object)
    {
        $reviewId = $object->getId();
        if ($this->_dataHelper->enableUserDefined()) :
            $userPros = $this->_resigtry->registry('user_pros_review');
            $this->addNewPros($userPros, $reviewId);
            $userCons = $this->_resigtry->registry('user_cons_review');
            $this->addNewCons($userCons, $reviewId);

        endif;

        if ($pros = $this->_resigtry->registry('pros_review')) :
            $this->addReviewProsCons($reviewId, $pros);
        endif;

        if ($cons = $this->_resigtry->registry('cons_review')) :
            $this->addReviewProsCons($reviewId, $cons, "1");
        endif;

        if ($prosEdit = $this->_resigtry->registry('pros_review_edit')) :
            $this->deleteReviewProsCons($reviewId);
            $this->editReviewProsCons($reviewId, $prosEdit);
        endif;

        if ($consEdit = $this->_resigtry->registry('cons_review_edit')) :
            $this->deleteReviewProsCons($reviewId, "1");
            $this->editReviewProsCons($reviewId, $consEdit, "1");
        endif;

        if ($recommend = $this->_resigtry->registry('recommend_review_edit')) {
            $this->_modelRecommend->getCollection()
                ->addFieldToFilter('review_id', ['eq' => $reviewId])
                ->walk('delete');
            $data['review_id'] = $reviewId;
            $data['value'] = $recommend;
            $this->_modelRecommend->setData($data);
            $this->_modelRecommend->save();
        }

        if ($value = $this->_resigtry->registry('recommend_review')) :
            try {
                $data = [];
                $data['review_id'] = $reviewId;
                $data['value'] = $value;
                $this->_modelRecommend->setData($data);
                $this->_modelRecommend->save();
            } catch (\Exception $e) {
                $this->logger->debug(__($e->getMessage()));
            }
        endif;

        if ($this->_dataHelper->getEnabledSendMail() && !$this->is_admin()) {
            $this->_dataHelper->sendNotification($object);
        }
        
        parent::_afterSave($object);
    }

    protected function addNewPros($prosCollections, $reviewId)
    {
        if ($prosCollections != "") {
            $prosCollections = explode(",", $prosCollections);
            try {
                foreach ($prosCollections as $prosCollection) {
                    $this->addNew($prosCollection, $this->_modelPros);
                    $lastProsId = $this->prosCollection->create()->getLastItem()->getId();
                    $newPros = [$lastProsId => 'on'];
                    $this->addReviewProsCons($reviewId, $newPros);

                }
            } catch (\Exception $e) {
                $this->logger->debug(__($e->getMessage()));
            }
        }
    }

    protected function addNewCons($consCollections, $reviewId)
    {
        if ($consCollections != "") {
            $consCollections = explode(",", $consCollections);
            try {
                foreach ($consCollections as $consCollection) {
                    $this->addNew($consCollection, $this->_modelCons);
                    $lastConsId = $this->consCollection->create()->getLastItem()->getId();
                    $newCons = [$lastConsId => 'on'];
                    $this->addReviewProsCons($reviewId, $newCons, 1);

                }
            } catch (\Exception $e) {
                $this->logger->debug(__($e->getMessage()));
            }
        }
    }

    protected function addReviewProsCons($reviewId, $collections, $isCons = null)
    {
        if ($collections !="") {
            try {
                foreach ($collections as $key => $value) :
                    $this->addReview($this->_modelReviewProsCons, $reviewId, $key, $isCons);
                endforeach;
            } catch (\Exception $e) {
                $this->logger->debug(__($e->getMessage()));
            }
        }
    }

    protected function deleteReviewProsCons($reviewId, $isCons = null)
    {
        isset($isCons) ? $field = 'pros_id' : $field = 'cons_id';
        try {
            $this->_modelReviewProsCons->getCollection()
                    ->addFieldToFilter('review_id', ['eq' => $reviewId])
                    ->addFieldToFilter($field, ['eq' => 0])
                    ->walk('delete');
        } catch (\Exception $e) {
            $this->logger->debug(__($e->getMessage()));
        }
    }

    protected function editReviewProsCons($reviewId, $collections, $isCons = null)
    {
        if ($collections !="") {
            try {
                foreach ($collections as $value) :
                    $this->editReview($this->_modelReviewProsCons, $reviewId, $value, $isCons);
                endforeach;
            } catch (\Exception $e) {
                $this->logger->debug(__($e->getMessage()));
            }
        }
    }

    protected function editReview($modelReviewProsCons, $reviewId, $value, $isCons)
    {
        $modelReviewProsCons->setId(null);
        $modelReviewProsCons->setReviewId($reviewId);
        if ($isCons) {
            $modelReviewProsCons->setConsId($value);
            $modelReviewProsCons->setProsId("");
        } else {
            $modelReviewProsCons->setConsId("");
            $modelReviewProsCons->setProsId($value);
        }
        $this->_modelReviewProsCons->save();
    }

    /**
     * Action before delete
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _beforeDelete(AbstractModel $object)
    {
        // prepare rating ids, that depend on review
        $this->clearCache = [
            'ratingIds' => $this->_loadVotedRatingIds($object->getId()),
            'entityPkValue' => $object->getEntityPkValue(),
        ];
        
        return $this;
    }

    public function afterDeleteCommit(AbstractModel $object)
    {
        $this->aggregate($object);

        // reaggregate ratings, that depended on this review
        $this->_aggregateRatings($this->clearCache['ratingIds'], $this->clearCache['entityPkValue']);
        $this->clearCache = [];
        $this->_modelReviewProsCons->getCollection()
                ->addFieldToFilter('review_id', ['eq' => $object->getId()])
                ->walk('delete');

        return $this;
    }

    protected function addNew($prosCollection, $model)
    {
        $model->setId(null);
        $model->setStoreId($this->_dataHelper->getCurrentStoreId());
        $model->setOwner("1");
        $model->setStatus("0");
        $model->setName(trim($prosCollection));
        $model->save();
    }

    public function getSummaryBlock($productId)
    {
        $parametrs = $this->model->getCollection();

        $ratings = [];
        foreach ($parametrs as $parametr) {
            $ratings[$parametr->getId()]['name'] = $parametr->getRatingCode();

            $votes = $this->modelVote->getCollection();
            $votes->getSelect()
                    ->from(null, ['COUNT(*) as qty','value as mark'])
                    ->join(['review'=>$this->getTable('review')], 'main_table.review_id = review.review_id', ['status_id'])
                    ->join(['review_store'=>$this->getTable('review_store')], 'main_table.review_id = review_store.review_id', ['store_id'])
                    ->where('main_table.entity_pk_value = ?', $productId)
                    ->where('value > 0')
                    ->where('store_id = ?', $this->_dataHelper->getCurrentStoreId())
                    ->where('rating_id = ?', $parametr->getId())
                    ->where('review.status_id = 1')
                    ->group('value')
                    ->order('value DESC');

            $qtyVotes = 0;
            foreach ($votes as $vote) {
                $ratings[$parametr->getId()]['votes'][$vote->getMark()]['qty'] = $vote->getQty();
                $qtyVotes +=$vote->getQty();
            }
            $ratings[$parametr->getId()]['votes_qty'] = $qtyVotes;
            foreach ($votes as $vote) {
                $ratings[$parametr->getId()]['votes'][$vote->getMark()]['qty'] = $vote->getQty();
                $ratings[$parametr->getId()]['votes'][$vote->getMark()]['percent'] = round($vote->getQty()/$qtyVotes*100);
            }
        }
        return $ratings;
    }

    protected function addReview($modelReviewProsCons, $reviewId, $key, $isCons)
    {
        $modelReviewProsCons->setId(null);
        $modelReviewProsCons->setReviewId($reviewId);
        if ($isCons) {
            $modelReviewProsCons->setConsId($key);
            $modelReviewProsCons->setProsId("");
        } else {
            $modelReviewProsCons->setConsId("");
            $modelReviewProsCons->setProsId($key);
        }
        $this->_modelReviewProsCons->save();
    }
}
