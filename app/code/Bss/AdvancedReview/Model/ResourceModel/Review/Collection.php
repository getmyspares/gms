<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   BSS
 * @package    Bss_AdvancedReview
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AdvancedReview\Model\ResourceModel\Review;

class Collection extends \Magento\Review\Model\ResourceModel\Review\Collection
{
    const ORDER_BY_DATE         = 'date';
    const ORDER_BY_RATING       = 'by_rating';
    const ORDER_BY_HELPFULNESS  = 'by_helpfulness';

    protected $_ordering;

    protected $_notIsFirst;

    protected $_sorting;

    public function setCurrentOrdering()
    {
        $request = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface');
        $this->_ordering = $request->getParam('type');
        $this->_sorting = $request->getParam('dir');
    }

    public function clearOrdering()
    {
        $this->_orders = [];
        return $this;
    }

    public function fillterPros()
    {
        $post = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface')->getPost();
        if ($post['pros']) {
            $array = [];
            foreach ($post['pros'] as $key => $value) {
                $array[] = $key;
            }
            $this->getSelect()
                 ->join(
                     ['review_pros_tab' => $this->getTable('bss_review_pros_cons') ],
                     'main_table.review_id = review_pros_tab.review_id',
                     []
                 );
            foreach ($post['pros'] as $key => $value) {
                $inCond = $this->getConnection()->prepareSqlCondition('review_pros_tab.pros_id', ['in' => $key]);
                $this->getSelect()->where($inCond);
            }
        }
        return $this;
    }

    public function fillterCons()
    {
        $post = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface')->getPost();
        if ($post['cons']) {
            $array = [];
            foreach ($post['cons'] as $key => $value) {
                $array[] = $key;
            }
            $this->getSelect()
                 ->join(
                     ['review_cons_tab' => $this->getTable('bss_review_pros_cons') ],
                     'main_table.review_id = review_cons_tab.review_id',
                     []
                 );
            foreach ($post['cons'] as $key => $value) {
                $inCond = $this->getConnection()->prepareSqlCondition('review_cons_tab.cons_id', ['in' => $key]);
                $this->getSelect()->where($inCond);
            }
        }
        return $this;
    }

    public function setDateOrder($dir = 'DESC')
    {
        $this->setCurrentOrdering();

        if ($this->_sorting) {
            $dir = $this->_sorting;
        }
        if ($this->_ordering === self::ORDER_BY_DATE) {
            $this->clearOrdering()->setOrder('main_table.created_at', $dir);
        } elseif ($this->_ordering === self::ORDER_BY_RATING) {
            if (!$this->_notIsFirst) {
                $ratingTableName = $this->getTable('rating_option_vote');
                $this->_select->joinInner(['ov' => $ratingTableName], '(ov.review_id = main_table.review_id)', ['summa' => 'SUM(percent)'])->group('ov.review_id');
                $this->clearOrdering()->setOrder('summa', $dir);
                $this->_notIsFirst = true;
            }
        } elseif ($this->_ordering === self::ORDER_BY_HELPFULNESS) {
            if (!$this->_notIsFirst) {
                $this->getSelect()
                     ->joinLeft(
                         ['helpfulness' => $this->getTable('bss_helpfulness')],
                         'main_table.review_id = helpfulness.review_id',
                         ['all_count' => 'COUNT(helpfulness.id)', 'yes_count' => 'SUM(helpfulness.value)' ]
                     )
                     ->group('main_table.review_id');
                    $this->clearOrdering()->setOrder('yes_count', $dir);
                    $this->_notIsFirst = true;
            }
        }
        return $this;
    }

    public function getItems()
    {
        $this->setDateOrder();
        return parent::getItems();
    }
}
