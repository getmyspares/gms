<?php

namespace Meetanshi\IndianGst\Block\Order\Invoice;

use Meetanshi\IndianGst\Block\Order\Totals as OrderTotals;

class Totals extends OrderTotals
{
    protected function _initTotals()
    {
        parent::_initTotals();
        $this->removeTotal('base_grandtotal');
        return $this;
    }
}
