<?php

namespace Meetanshi\IndianGst\Observer;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\IndianGst\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Api\AddressRepositoryInterface;

class SaveGstToOrder implements ObserverInterface
{
    protected $quoteRepository;
    protected $customerFactory;
    protected $helper;
    protected $productFactory;
    protected $orderFactory;
    protected $categoryRepository;
    protected $storeManager;
    protected $quoteFactory;
    protected $orderSender;
    protected $checkoutSession;
    protected $scopeConfig;
    protected $resourceConfig;
    protected $cacheTypeList;
    protected $cacheFrontendPool;
    protected $addressRepository;
    protected $_objectManager;

    public function __construct(
        QuoteRepository $quoteRepository,
        CustomerFactory $customerFactory,
        Data $helper,
        ProductFactory $productFactory,
        OrderFactory $orderFactory,
        CategoryRepositoryInterface $categoryRepository,
        StoreManagerInterface $storeManager,
        QuoteFactory $quoteFactory,
        OrderSender $orderSender,
        Session $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        ResourceConfig $resourceConfig,
        AddressRepositoryInterface $addressRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
    ) {
        $this->customerFactory = $customerFactory;
        $this->quoteRepository = $quoteRepository;
        $this->helper = $helper;
        $this->productFactory = $productFactory;
        $this->orderFactory = $orderFactory;
        $this->categoryRepository = $categoryRepository;
        $this->storeManager = $storeManager;
        $this->quoteFactory = $quoteFactory;
        $this->orderSender = $orderSender;
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->_objectManager = $objectManager;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->addressRepository = $addressRepository;
    }

    public function execute(EventObserver $observer)
    {
        $countryCodes = 1;
        $order = $observer->getOrder();
        $shippingAddress = $order->getShippingAddress();
        $shippingPostcode = $shippingAddress->getPostcode();

        if ($shippingAddress->getCountryId() == "IN") {
            $isUnionTarritotial = $this->helper->getCheckUnionTerritory($shippingAddress->getRegion());
            $totals = 0;
            $quoteId = $order->getQuoteId();
            $quote = $this->quoteFactory->create()->load($quoteId);
            $excludingGst = $quote->getGstExclusive();
            $orderItems = $order->getAllVisibleItems();
            foreach ($orderItems as $item) {
                if ($item->getQuoteItemId()) {
                    $product = $this->productFactory->create()->load($item->getData('product_id'));
                    $productId = $item->getData('product_id');
                    
					$resource = $this->_objectManager->create('Magento\Framework\App\ResourceConnection');
					$_connection = $resource->getConnection();
					$sellerRegion = $_connection->fetchOne("SELECT region_id FROM `marketplace_userdata` mu WHERE mu.seller_id in (SELECT mp.seller_id FROM `marketplace_product` mp where mageproduct_id='".$productId."' and status = 1) and is_seller = 1 and seller_priority = 1");
                    if($sellerRegion){
                        $sellerRegion = $sellerRegion;
                    }
                    else{
                        $sql = "SELECT lattitude FROM `pincode_checker` where pincode = $shippingPostcode";
                    $customerLat = $_connection->fetchOne($sql);
                    $sql = "SELECT longitude FROM `pincode_checker` where pincode = $shippingPostcode";
                    $customerLong=$_connection->fetchOne($sql);
                    $sql = "SELECT seller_id FROM `marketplace_userdata` mu WHERE mu.seller_id in (SELECT mp.seller_id FROM `marketplace_product` mp where mageproduct_id='".$productId."' and status = 1) and is_seller = 1 ORDER BY mu.`seller_priority` ASC";
                    $results = $_connection->fetchAll($sql);
                    $distanceToMeasure = (float)100000;
                    foreach($results as $sellerIds){
                        $sId = $sellerIds['seller_id'];
                        $sql ="SELECT postcode FROM `seller_postcode` where seller_id = $sId";
                        $sellerPostCode = $_connection->fetchOne($sql);
                        $sql = "SELECT lattitude FROM `pincode_checker` where pincode = $sellerPostCode";
                        $sellerLat = $_connection->fetchOne($sql);
                        $sql = "SELECT longitude FROM `pincode_checker` where pincode = $sellerPostCode";
                        $sellerLong=$_connection->fetchOne($sql);
                        $point1 = array('lat' => $customerLat, 'long' => $customerLong);
                        $point2 = array('lat' => $sellerLat, 'long' => $sellerLong);
                        $lat1 = $point1['lat'];
                        $lon1 = $point1['long'];
                        $lat2 = $point2['lat'];
                        $lon2 = $point2['long'];
                        $theta = $lon1 - $lon2;
                        $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
                        $miles = acos($miles);
                        $miles = rad2deg($miles);
                        $miles = $miles * 60 * 1.1515;
                        $feet = $miles * 5280;
                        $yards = $feet / 3;
                        $kilometers = $miles * 1.609344;
                        $meters = $kilometers * 1000;
                        $distance = compact('kilometers'); 
                        foreach ($distance as $unit => $value) {
                            $sellerCustomerDistance = round((float)$value,4);
                            if($distanceToMeasure > $sellerCustomerDistance){
                                $distanceToMeasure = $sellerCustomerDistance;
                                $sellerId = $sId;
                            }
                            
                        }
                    }
                    $sellerRegion = $_connection->fetchOne("SELECT region_id FROM `marketplace_userdata` mu WHERE mu.seller_id = $sellerId");
                    }
					$custRegion   = $shippingAddress->getRegionId();
					
					//file_put_contents("/var/www/html/demo51.mobdigi.com/var/log/region.log" ,$sellerRegion."----".$custRegion);
        
                    $categoryIds = $product->getCategoryIds();
                    $productPriceAfterDiscount = ($product->getFinalPrice() * $product->getDiscountPercent()) / 100;
                    if (trim($item->getData('product_type')) === 'configurable') {
                        $productPrice = $tmep = $item->getRowTotal() - $item->getDiscountAmount();
                    } else {
                        $productPrice = $tmep = $item->getRowTotal() - $productPriceAfterDiscount;
                    }
                    $flag = false;
                    if (count($categoryIds) > 0) {
                        foreach ($categoryIds as $categoryId) {
                            $category = $this->categoryRepository->get($categoryId, $this->storeManager->getStore()->getId());
                            $catGstRate = $category->getCatGstRate();
                            $gstRateMinAmount = $category->getCatMinGstAmount();
                            $gstMinRate = $category->getCatMinGstRate();
                            if ($catGstRate) {
                                if ($category->getCatGstRate()) {
                                    $flag = true;
                                }
                                break;
                            }
                        }
                    }
                    if ($product->getGstRate()) {
                        $gstRate = $product->getGstRate();
                        $gstMinimumPrice = $product->getMinGstAmount();
                        if ($gstMinimumPrice > $productPrice) {
                            $gstRate = $product->getMinGstRate();
                        }
                        if ($this->helper->isCatalogExclusiveGst()) {
                            $gstAmount = ((($productPrice) * $gstRate) / 100);
                        } else {
                            $gstPercent = 100 + $gstRate;
                            $productPrice = ($productPrice) / $gstPercent;
                            $gstAmount = $productPrice * $gstRate;
                        }

                        //$sub = $item->getData('qty_ordered') * $gstAmount;
                        $sub = $gstAmount;
                        //if ($this->helper->canApplyTax('DIFF')) {
                        if( $sellerRegion != $custRegion ){
                            $item->setIgstAmount($sub);
                            $item->setBaseIgstAmount($sub);
                            $item->setIgstPercent($gstRate);
                        } else {
                            $item->setCgstAmount($sub / 2);
                            $item->setBaseCgstAmount($sub / 2);
                            $item->setCgstPercent($gstRate / 2);
                            if($isUnionTarritotial){
                                $item->setUtgstAmount($sub / 2);
                                $item->setBaseUtgstAmount($sub / 2);
                                $item->setUtgstPercent($gstRate / 2);
                            }else{
                                $item->setSgstAmount($sub / 2);
                                $item->setBaseSgstAmount($sub / 2);
                                $item->setSgstPercent($gstRate / 2);
                            }

                        }
                        $totals = $totals + $sub;
                    } elseif ($flag) {
                        if ($gstRateMinAmount > $productPrice) {
                            $catGstRate = $gstMinRate;
                        }
                        if ($this->helper->isCatalogExclusiveGst()) {
                            $gstAmount = ((($productPrice) * $catGstRate) / 100);
                        } else {
                            $gstPercent = 100 + $catGstRate;
                            $productPrice = ($productPrice) / $gstPercent;
                            $gstAmount = $productPrice * $catGstRate;
                        }

                        $sub = $gstAmount;
                        //if ($this->helper->canApplyTax('DIFF')) {
                        if( $sellerRegion != $custRegion ){
                            $item->setIgstAmount($sub);
                            $item->setBaseIgstAmount($sub);
                            $item->setIgstPercent($catGstRate);
                        } else {
                            $item->setCgstAmount($sub / 2);
                            $item->setBaseCgstAmount($sub / 2);
                            $item->setCgstPercent($catGstRate / 2);
                            if($isUnionTarritotial){
                                $item->setUtgstAmount($sub / 2);
                                $item->setBaseUtgstAmount($sub / 2);
                                $item->setUtgstPercent($catGstRate / 2);
                            }else{
                                $item->setSgstAmount($sub / 2);
                                $item->setBaseSgstAmount($sub / 2);
                                $item->setSgstPercent($catGstRate / 2);
                            }
                        }
                        $totals = $totals + $sub;
                    } else {
                        $rate = $this->helper->getRate();
                        if ($this->helper->getMinAmount() > $productPrice) {
                            $rate = $this->helper->getMinRate();
                        }
                        if ($this->helper->isCatalogExclusiveGst()) {
                            $gstAmount = ((($productPrice) * $rate) / 100);
                        } else {
                            $gstPercent = 100 + $rate;
                            $productPrice = ($productPrice) / $gstPercent;
                            $gstAmount = $productPrice * $rate;
                        }
                        $sub = $gstAmount;
                        //if ($this->helper->canApplyTax('DIFF')) {
                        if( $sellerRegion != $custRegion ){
                            $item->setIgstAmount($sub);
                            $item->setBaseIgstAmount($sub);
                            $item->setIgstPercent($rate);
                        } else {
                            $item->setCgstAmount($sub / 2);
                            $item->setBaseCgstAmount($sub / 2);
                            $item->setCgstPercent($rate / 2);
                            if($isUnionTarritotial){
                                $item->setUtgstAmount($sub / 2);
                                $item->setBaseUtgstAmount($sub / 2);
                                $item->setUtgstPercent($rate / 2);
                            }else{
                                $item->setSgstAmount($sub / 2);
                                $item->setBaseSgstAmount($sub / 2);
                                $item->setSgstPercent($rate / 2);
                            }
                        }
                        /* removed bug from the system , this let wrong gst calculation by dividing the total gst in half */
                        // $totals = $totals + ($sub / 2);
                        $totals = $totals + ($sub);
                    }
                    $item->setGstExclusive($excludingGst);
                }
            }
            
            if ($this->helper->isEnabled()) {
                /*if ($this->helper->canApplyTax('DIFF')) {
                    $order->setIgstAmount($quote->getIgstAmount());
                    $order->setBaseIgstAmount($quote->getBaseIgstAmount());
                } else {
                    $order->setCgstAmount($quote->getCgstAmount());
                    $order->setBaseCgstAmount($quote->getBaseCgstAmount());
                    if($isUnionTarritotial){
                        $order->setUtgstAmount($quote->getUtgstAmount());
                        $order->setBaseUtgstAmount($quote->getBaseUtgstAmount());
                    }else{
                        $order->setSgstAmount($quote->getSgstAmount());
                        $order->setBaseSgstAmount($quote->getBaseSgstAmount());
                    }
                }*/
                if( $sellerRegion != $custRegion ){
                    $order->setIgstAmount($totals);
                    $order->setBaseIgstAmount($totals);
                } else {
                    $order->setCgstAmount($totals/2);
                    $order->setBaseCgstAmount($totals/2);
                    if($isUnionTarritotial){
                        $order->setUtgstAmount($totals/2);
                        $order->setBaseUtgstAmount($totals/2);
                    }else{
                        $order->setSgstAmount($totals/2);
                        $order->setBaseSgstAmount($totals/2);
                    }
				}
                
                try {
                    $order->save();
                } catch (LocalizedException $e) {
                    return $e->getMessage();
                }
            }
            if ($this->helper->isShippingGst()) {
                if ($this->helper->canApplyShipping('SSAME')) {
                    $order->setShippingCgstAmount($quote->getShippingCgstAmount());
                    $order->setBaseShippingCgstAmount($quote->getBaseShippingCgstAmount());
                    if($isUnionTarritotial) {
                        $order->setShippingUtgstAmount($quote->getShippingUtgstAmount());
                        $order->setBaseShippingUtgstAmount($quote->getBaseShippingUtgstAmount());
                    }else{
                        $order->setShippingSgstAmount($quote->getShippingSgstAmount());
                        $order->setBaseShippingSgstAmount($quote->getBaseShippingSgstAmount());
                    }
                } else {
                    $order->setShippingIgstAmount($quote->getShippingIgstAmount());
                    $order->setBaseShippingIgstAmount($quote->getBaseShippingIgstAmount());
                }
                $order->save();
            }/**/
            $quoteRepository = $this->quoteRepository;
            $quote = $quoteRepository->get($order->getQuoteId());
            $customer = $this->customerFactory->create()->load($quote->getCustomer()->getId());
            if (!empty($customer->getId())) {
                $addressId = $quote->getShippingAddress()->getCustomerAddressId();
                if ($addressId != 0) {
                    $addressObject = $this->addressRepository->getById($addressId);
                    $buyerGst = $addressObject->getCustomAttribute('buyer_gst_number');
                    if (empty($buyerGst)) {
                        $quote = $quoteRepository->get($order->getQuoteId());
                        $buyerGst = $quote->getBuyerGstNumber();
                    } else {
                        $buyerGst = $addressObject->getCustomAttribute('buyer_gst_number')->getValue();
                    }
                } else {
                    $buyerGst = $quote->getShippingAddress()->getBuyerGstNumber();
                }

            } else {
                $quote = $quoteRepository->get($order->getQuoteId());
                $buyerGst = $quote->getBuyerGstNumber();
            }
            $order->setBuyerGstNumber($buyerGst);
            $shippingAddress = $order->getShippingAddress();
            $shippingAddress->setBuyerGstNumber($buyerGst);
        }
        return $this;
    }
}
