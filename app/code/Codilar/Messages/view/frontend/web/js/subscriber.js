requirejs([
    'jquery',   
    'Magento_Customer/js/customer-data',
    'toastr',
    'jquery/jquery.cookie'
], function($, customerData, toastr){
    var observableObject = customerData.get('messages');

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false, 
        "positionClass": "toast-top-right",
       "preventDuplicates": true,
        "onclick": null, 
        "showDuration": "300",
        "hideDuration": "2000",
        "timeOut": "2000",
        "maxOpened": "1",
        "autoDismiss": true,
        "extendedTimeOut": "2000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var showToastMessage = function(data){
        try{
            data.messages.forEach(function(message){
                switch(message.type){
                    case "success":
                        // alert("success");
                        var earlier_message = $.cookie('oldmessage');
                        console.log('earlier_message'+earlier_message);
                        console.log('message.text'+message.text);
                        if(earlier_message != message.text){
                            toastr.success(message.text, "");
                            $.cookie('oldmessage', message.text );
                        }
                        
                        
                        break;
                    case "error":
                        var error_message = $.cookie('errormessage');
                        console.log('error_message'+error_message);
                        console.log('message.text'+message.text);
                        if(error_message != message.text){
                            toastr.error(message.text, "");
                            $.cookie('errormessage', message.text );
                        }
                        break;
                    default:
                        toastr.info(message.text, "");
                }
            })
        }catch(e){}
    };

    /* Show initial messages */
    var previousMessages = $.cookieStorage.get('mage-messages');
    $.cookieStorage.set('mage-messages', '');
    showToastMessage({'messages' : previousMessages});

    observableObject.subscribe(showToastMessage);
});