<?php
/**
 * Mpreport system Helper Data
 *
 * @category  Webkul
 * @package   Webkul_Bareportsystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Bareportsystem\Helper;

use Webkul\Bareportsystem\Model\Product as SellerProduct;
use Magento\Sales\Model\ResourceModel\Order;
use Webkul\Marketplace\Model\SaleslistFactory;
use Magento\Framework\Locale\ListsInterface;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Webkul\Marketplace\Model\ResourceModel;

/**
 * Webkul Mpreportsystem Helper Data.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_marketplaceProductCollection;

    /**
     * @var Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory
     */
    protected $_salesStatusCollection;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var Webkul\Marketplace\Model\SaleslistFactory
     */
    protected $_marketplacesaleslist;

    /**
     * @var Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory
     */
    protected $_mpsaleslistCollection;

    /**
     * @var Magento\Framework\Locale\ListsInterface
     */
    protected $_listInterface;

    /**
     * @var Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_localeCurrency;

    /**
     * @var string
     */
    protected $_deploymentConfigDate;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_storeTime;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $_mpHelper;

    /**
     * @param \Framework\App\Helper\Context       $context
     * @param \Magento\Customer\Model\Session     $customerSession
     * @param \Catalog\Model\ProductFactory       $productFactory
     * @param \StoreManagerInterface              $storeManager
     * @param \Magento\Directory\Model\currency   $currency
     * @param \CurrencyInterface                  $localeCurrency
     * @param Product\CollectionFactory           $marketplaceProductCollection
     * @param Order\Status\CollectionFactory      $salesStatusCollection
     * @param \Catalog\Model\CategoryFactory      $categoryFactory
     * @param \Product\CollectionFactory          $productCollectionFactory
     * @param \Magento\Sales\Model\OrderFactory   $orderFactory
     * @param SaleslistFactory                    $saleslistFactory
     * @param ListsInterface                      $listInterface
     * @param RegionFactory                       $regionFactory
     * @param deploymentConfig                    $deploymentConfig
     * @param \Webkul\Marketplace\Helper\Data     $mpHelper
     * @param \TimezoneInterface                  $timezone
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        ResourceModel\Product\CollectionFactory $marketplaceProductCollection,
        Order\Status\CollectionFactory $salesStatusCollection,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        SaleslistFactory $saleslistFactory,
        ResourceModel\Saleslist\CollectionFactory $mpsaleslistFactory,
        ListsInterface $listInterface,
        RegionFactory $regionFactory,
        DeploymentConfig $deploymentConfig,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    ) {
        $this->_customerSession = $customerSession;
        $this->_productFactory = $productFactory;
        parent::__construct($context);
        $this->_currency = $currency;
        $this->_localeCurrency = $localeCurrency;
        $this->_storeManager = $storeManager;
        $this->_marketplaceProductCollection = $marketplaceProductCollection;
        $this->_salesStatusCollection = $salesStatusCollection;
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_orderFactory = $orderFactory;
        $this->_marketplacesaleslist = $saleslistFactory;
        $this->_mpsaleslistCollection = $mpsaleslistFactory;
        $this->_listInterface = $listInterface;
        $this->_regionFactory = $regionFactory;
        $this->_deploymentConfigDate = $deploymentConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_INSTALL_DATE
        );
        $this->_mpHelper = $mpHelper;
        $this->_storeTime = $timezone;
    }

     // get sales collection data
    public function getSalesCollection($paramData)
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		
		$tableName = 'taxation_report';
		$sql = "Select * FROM ". $tableName;
        $collection = $connection->fetchAll($sql);
		
        $sellerId = '';
        if (array_key_exists('seller_id', $paramData)) {
            $sellerId = $paramData['seller_id'];
        }
        $dateFilter = 0;
        if (array_key_exists(
            'wk_report_date_start',
            $paramData
        ) && array_key_exists(
            'wk_report_date_end',
            $paramData
        )) {
            if ($paramData['wk_report_date_start']!='' &&
                $paramData['wk_report_date_end']!=''
            ) {
                $dateFrom = $paramData['wk_report_date_start'];
                $dateTo = $paramData['wk_report_date_end'];
                $dateFilter = 1;
            }
        }
        $collection = $this->getSalesListCollection(
            $sellerId,
            $paramData
        );
      
       

        return $collection;
    } 

    // get customer count seeting from system config
  
    // get saleslist collection according to filer
    public function getSalesListCollection($sellerId, $data)
    {
        $sellerIdFlag = 0;
        if ($sellerId != '') {
            $sellerIdFlag = 1;
        }
        $productIds = [];
        $categoryFlag = 0;
        $orderIds = [];
        $orderFlag = 0;
        if (array_key_exists('categories', $data) &&
            is_array($data['categories'])
        ) {
            $productIds = $this->getProductIdsByCategoryIds(
                $data['categories']
            );
            $categoryFlag = 1;
        }
        if (array_key_exists(
            'orderstatus',
            $data
        ) && is_array(
            $data['orderstatus']
        )) {
         
            $orderFlag = 1;
        }
        $collection = $this->_marketplacesaleslist
            ->create()
            ->getCollection()
            ->addFieldToFilter(
                'order_id',
                ['neq' => 0]
            );
        if ($sellerIdFlag) {
            $collection->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            );
        }
        if ($categoryFlag) {
            $collection->addFieldToFilter(
                'mageproduct_id',
                ['in' => $productIds]
            );
        }
        if ($orderFlag) {
            $collection->addFieldToFilter(
                'order_id',
                ['in' => $orderIds]
            );
        } else {
           
        }
        return $collection;
    }

    public function getRegionById($regionId)
    {
        $regionModel = $this->_regionFactory->create()->load($regionId);
        return $regionModel;
    }
    public function getCurrentAmount($amount)
    {
        $currency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceCurrencyObject = $objectManager->get('Magento\Framework\Pricing\PriceCurrencyInterface');
        $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $store = $storeManager->getStore()->getStoreId();
        return $priceCurrencyObject->convert($amount, $store, $currency);
    }
    public function convertPrice($amount = 0, $store = null, $currency = null)
    {
        $currency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceCurrencyObject = $objectManager->get('Magento\Framework\Pricing\PriceCurrencyInterface');
        $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        if ($store == null) {
            $store = $storeManager->getStore()->getStoreId();
        }
        $rate = $priceCurrencyObject->convertAndFormat($amount, $includeContainer = true, $precision = 2, $store, $currency);
        return $rate;
    }
	
	/****Ramit*********/
	
	public function get_taxation_report()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		 
		$sql = "Select * FROM taxation_report";
		$results = $connection->fetchAll($sql);		
		return $results; 
	}	
	
	
	
}
