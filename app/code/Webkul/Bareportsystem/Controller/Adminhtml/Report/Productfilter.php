<?php
/**
 * Product filter controller admin
 *
 * @category Webkul
 * @package Webkul_Mpreportsystem
 * @author Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Bareportsystem\Controller\Adminhtml\Report;

use Magento\Backend\App\Action;
use Magento\Framework\Encryption\Helper\Security;
use Magento\Framework\Controller\Result;
use Webkul\Bareportsystem\Block\Adminhtml\Bareport;

class Productfilter extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;
    /**
     * @var Webkul\Mpreportsystem\Block\Adminhtml\Mpreport
     */
    protected $_bareportBlock;

    /**
     * @param Action\Context                      $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param Mpreport                            $mpreport
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Bareport $bareport
    ) {
        $this->_bareportBlock = $bareport;
        $this->_jsonHelper = $jsonHelper;
        parent::__construct($context);
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Webkul_Bareportsystem::bareports'
        );
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $returnData = '';
        $returnData = $this->_bareportBlock->getProductSales($params);
        return $this->getResponse()->representJson(
            $this->_jsonHelper->jsonEncode($returnData)
        );
    }
}
