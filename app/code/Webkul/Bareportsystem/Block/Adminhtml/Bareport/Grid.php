<?php
/**
 * Mpreport Admin Grid block
 * 
 * @category Webkul
 * @package Webkul_Mpreportsystem
 * @author Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Bareportsystem\Block\Adminhtml\Bareport;

use Webkul\Bareportsystem\Block\Adminhtml\Bareport;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var Webkul\Mpreportsystem\Block\Adminhtml\Mpreport\Grid
     */
    protected $_bareportBlock;
    protected $_countTotals = true;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data            $backendHelper
     * @param \Magento\Framework\Registry             $coreRegistry
     * @param Mpreport                                $blockMpreport
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        Bareport $blockBareport,
        array $data = []
    ) {
        $this->_bareportBlock = $blockBareport;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('salesgrid');
        $this->setDefaultSort('total_seller_amount');
        $this->setFilterVisibility(false);
        $this->setUseAjax(true);
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        $data = $this->_bareportBlock->getParamValues();
        $data['filter'] = 'year';
        $collection = $this->_bareportBlock->getSalesCollection($data);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'row',
            [
                'header'    => __('#'),
                'filter'    => false,
                'sortable'  =>  false,
                'index'     => 'row',
                'renderer'  => 'Webkul\Bareportsystem\Block\Adminhtml\Bareport\Grid\Renderrow',
            ]
        );
        $this->addColumn(
            'order_date',
            [
                'header'    => __('Date'),
                'filter'    => false,
                'index'     => 'order_date',
                'type'      => 'date',
            ]
        );
        $this->addColumn(
            'order_id',
            [
                'header'    => __('Order ID'),
                'filter'    => false,
                'index'     => 'order_id'
            ]
        );
        
       
        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getRowUrl($row)
    {
        return 'javascript:void(0)';
    }
    public function getcurrency()
    {
        return $currencyCode = $this->_storeManager
            ->getStore()
            ->getBaseCurrencyCode();
    }
    public function getGridUrl()
    {
        return $this->getUrl('bareportsystem/report/grid', ['_current' => true]);
    }
    public function getTotals()
    {
        $totals = new \Magento\Framework\DataObject();
        $fields = [
            'total_order_id' => 0,
            'total_item_qty' => 0,
            'total_seller_amount' => 0,
        ];
        foreach ($this->getCollection() as $item) {
            foreach ($fields as $field => $value) {
                $fields[$field] += $item->getData($field);
            }
        }
        $totals->setData($fields);
        return $totals;
    }
}
