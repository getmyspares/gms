<?php
/**
 * Mpreport system admin mpreport block
 *
 * @category  Webkul
 * @package   Webkul_Mpreportsystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Bareportsystem\Block\Adminhtml;

use Magento\Framework\Controller\Result;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Webkul\Marketplace\Model\SellerFactory;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;

class Bareport extends \Magento\Framework\View\Element\Template
{
    //url to get chart
    const GOOGLE_API_URL = 'http://chart.apis.google.com/chart';

    /**
     * @var \Webkul\Mpreportsystem\Helper\Data
     */
    protected $_helperdata;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * @var sales Collection
     */
    protected $_salesCollection;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var Webkul\Marketplace\Model\SellerFactory
     */
    protected $_salesPartnerFactory;

    /**
     * @var Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data $mpHelper
     */
    protected $_baHelper;

    /**
     * @param \View\Element\Template\Context        $context
     * @param \Webkul\Mpreportsystem\Helper\Data    $mpreportHelper
     * @param ProductRepository                     $productRepository
     * @param PriceCurrencyInterface                $priceCurrency
     * @param \Magento\Customer\Model\Session       $customerSession
     * @param SellerFactory                         $salesPartnerFactory
     * @param CollectionFactory                     $productCollection
     * @param \Webkul\Marketplace\Helper\Data       $mpHelper
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Bareportsystem\Helper\Data $bareportHelper,
        ProductRepository $productRepository,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Customer\Model\Session $customerSession,
        SellerFactory $salesPartnerFactory,
        CollectionFactory $productCollection,
        \Webkul\Marketplace\Helper\Data $baHelper,
        array $data = []
    ) {
        $this->_helperdata = $bareportHelper;
        $this->_productRepository = $productRepository;
        $this->_priceCurrency = $priceCurrency;
        $this->_customerSession = $customerSession;
        $this->_salesPartnerFactory = $salesPartnerFactory;
        $this->_productCollectionFactory = $productCollection;
        $this->_baHelper = $baHelper;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $data = $this->getParamValues();
        if ($this->getSalesCollection($data)) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'bareportsales.pager'
            )->setAvailableLimit([4 => 4, 8 => 8, 16 => 16])
            ->setCollection(
                $this->getSalesCollection($data)
            );
            $this->setChild('pager', $pager);
            $this->getSalesCollection($data)->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    // return customer session id
    public function getCustomerId()
    {
        return $this->_baHelper->getCustomerId();
    }

    // Give the current url of recently viewed page
    public function getCurrentUrl()
    {
        return $this->_urlBuilder->getCurrentUrl();
    }

    // get country sales graph
   public function randString(
        $charset = 'ABC0123456789'
    ) {
        $length = 6;
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count-1)];
        }
        return $str;
    }

    // get sales graph url
 // get top selling products graph
  // get secure parameter value
    public function getIsSecure()
    {
        return $this->getRequest()->isSecure();
    }

    // get best customer collection
    public function getBestCustomerCollection($data)
    {
        $returnArray = $this->_helperdata->getCustomerCollection($data);
        return $returnArray;
    }

    // get formatted price
    public function getFormatedPrice($price, $currency)
    {
        return $this->_priceCurrency->format(
            $price,
            true,
            2,
            null,
            $currency
        );
    }

    // get requested parameters value
    public function getParamValues()
    {
        return $this->getRequest()->getParams();
    }

    /**
     * get category name by category id
     * @param  int $categoryId
     * @return string
     */
    public function getCategoryName($categoryId)
    {
        return $this->_helperdata->getCategory($categoryId)->getName();
    }

    /**
     * get sales collection
     * @param  array $data
     * @return collection
     */
    public function getSalesCollection($data)
    {
        if (!$this->_salesCollection) {
            $this->_salesCollection = $this->_helperdata
                ->getSalesCollection($data);
        }
        return $this->_salesCollection;
    }

    /**
     * retrun all sellers name and id
     * @return array
     */
    public function getSellerArray()
    {
        $sellerArray = [];
        $customerEntityTable = $this->_productCollectionFactory
            ->create()->getTable('customer_grid_flat');
        $sellerDataCollection = $this->_salesPartnerFactory->create()
            ->getCollection();
        $sellerDataCollection->getSelect()
            ->join(
                $customerEntityTable.' as customer',
                'main_table.seller_id = customer.entity_id',
                ['customer_name'=>'name']
            );
        foreach ($sellerDataCollection as $sellerData) {
            $data = $sellerData->getData();
            $sellerArray[$data['seller_id']] = $data['shop_url'].
            ' ( '.
            $data['customer_name'].
            ' )';
        }
        return $sellerArray;
    } 
}
