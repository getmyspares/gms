<?php
namespace Webkul\Rmasystem\Ui\Component\Listing\Column\Allrma;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\App\ResourceConnection;

class Status extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $_resource;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Webkul\Rmasystem\Helper\Data $rmaHelper,
        ResourceConnection $resource,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->rmaHelper = $rmaHelper;
        $this->_resource = $resource;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $rmaId = $item['rma_id'];
                $connection = $this->_resource->getConnection();
                $rmaStatus = $connection->fetchOne("SELECT `status` FROM `wk_rma` WHERE rma_id = $rmaId");
                $item[$this->getData('name')] = $this->rmaHelper->getRmaStatusTitle($rmaStatus);
            }
        }
        return $dataSource;
    }
}
