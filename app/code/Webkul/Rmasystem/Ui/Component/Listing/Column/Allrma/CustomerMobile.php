<?php
namespace Webkul\Rmasystem\Ui\Component\Listing\Column\Allrma;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class CustomerMobile extends \Magento\Ui\Component\Listing\Columns\Column
{
    private $_objectManager = null;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->_objectManager = $objectManager;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $custometId = $this->_objectManager->get('\Webkul\Rmasystem\Model\Allrma')
                    ->load($item['rma_id'])->getCustomerId();
                $custometModel = $this->_objectManager->get('\Magento\Customer\Model\Customer')->load($custometId);
                $mobile = '';
                if ($custometModel->getId()) {
                    $mobile = $custometModel->getMobile();
                }
                $item[$this->getData('name')] = $mobile;
            }
        }
        return $dataSource;
    }
}
