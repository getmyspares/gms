<?php
namespace Webkul\Rmasystem\Ui\Component\Listing\Column\Allrma;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Catalog\Api\ProductRepositoryInterface;

class ProductName extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $_resource;

    protected $orderItemRepository;

    protected $productRepository;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ResourceConnection $resource,
        ItemRepository $orderItemRepository,
        ProductRepositoryInterface $productRepository,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->_resource = $resource;
        $this->orderItemRepository = $orderItemRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $rmaId = $item['rma_id'];
                $connection = $this->_resource->getConnection();
                $itemId = $connection->fetchOne("SELECT item_id FROM `wk_rma_items` WHERE rma_id = $rmaId");
                $productName = '';
                if($itemId) {
                    $mageItem = $this->orderItemRepository->get($itemId);
                    $productId = $mageItem->getProductId();
                    $product = $this->productRepository->getById($productId);
                    $productName = $product->getName();
                }

                $item[$this->getData('name')] = $productName;
            }
        }
        return $dataSource;
    }
}
