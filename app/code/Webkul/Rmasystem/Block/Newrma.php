<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Rmasystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Rmasystem\Block;

use Magento\Framework\Session\SessionManager;
use Webkul\Rmasystem\Helper\Filter;
use Webkul\Rmasystem\Model\ResourceModel\Allrma\CollectionFactory as AllRmaCollectionFactory;
use Webkul\Rmasystem\Model\ResourceModel\Reason\CollectionFactory as ReasonCollectionFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory as ShipmentCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * Customer Create new RMA block.
 */
class Newrma extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Magento\Framework\App\Config\ScopeConfigInterface;
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Webkul\Rmasystem\Model\ResourceModel\Resion\CollectionFactory
     */
    protected $_regionCollectionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory
     */
    protected $_orderShipmentCollectionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var Session
     */
    protected $_session;

    /**
     * @var Webkul\Rmasystem\Helper\Filter
     */
    protected $_filterSorting;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orderCollection;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;

    /**
     * @var AllRmaCollectionFactory
     */
    protected $rmaCollectionFactory;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @param Magento\Framework\View\Element\Template\Context     $context
     * @param Magento\Customer\Model\Session                      $customerSession
     * @param ShipmentCollectionFactory                           $orderShipmentCollectionFactory
     * @param OrderCollectionFactory                              $orderCollectionFactory
     * @param Magento\Directory\Model\Currency                    $currency
     * @param Magento\Framework\Stdlib\DateTime\DateTime          $date
     * @param ReasonCollectionFactory                             $regionCollectionFactory
     * @param AllRmaCollectionFactory                             $rmaCollectionFactory
     * @param OrderFactory                                        $orderFactory
     * @param SessionManager                                      $session
     * @param Filter                                              $filterSorting
     * @param []                                                  $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        ShipmentCollectionFactory $orderShipmentCollectionFactory,
        OrderCollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\App\ResourceConnection $resoureConnection,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        ReasonCollectionFactory $reasonCollectionFactory,
        AllRmaCollectionFactory $rmaCollectionFactory,
        OrderFactory $orderFactory,
        SessionManager $session,
        Filter $filterSorting,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_orderShipmentCollectionFactory = $orderShipmentCollectionFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_currency = $currency;
        $this->request = $request;
        $this->_date = $date;
        $this->_regionCollectionFactory = $reasonCollectionFactory;
        $this->rmaCollectionFactory = $rmaCollectionFactory;
        $this->orderFactory = $orderFactory;
        $this->_session = $session;
        $this->_filterSorting = $filterSorting;
        $this->connection = $resoureConnection->getConnection();
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context, $data);
    }

    /**
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('New RMA'));
    }

    /**
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getRmaCollection()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        $this->_session->unsFilterData();
        $this->_session->unsSortingSession();

        $allowedStatus =  $this->scopeConfig->getValue(
            'rmasystem/parameter/allow_order',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $allowedDays = $this->scopeConfig->getValue(
            'rmasystem/parameter/days',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $order_id = $this->request->getParam('oid');

        if (!$this->orderCollection) {
            $joinTable = $this->rmaCollectionFactory->create()->getTable('sales_order');
            /* ideally allowed status to be complete */
            if ($allowedStatus == 'complete') {
                $collection = $this->_orderShipmentCollectionFactory->create();

                $collection->getSelect()->join(
                    $joinTable.' as so',
                    'main_table.order_id = so.entity_id',
                    ['grand_total', 'so.increment_id', 'so.created_at']
                )->where('main_table.customer_id ='.$customerId);

                $collection->addFilterToMap('created_at', 'so.created_at');
                $collection->addFilterToMap('customer_id', 'so.customer_id');
                $collection->addFilterToMap('increment_id', 'so.increment_id');
                $collection->addFieldToFilter('customer_id', $customerId);
                $collection->addFieldToFilter('so.status', 'Delivered');
                if(!empty($order_id))
                {
                    $collection->addFieldToFilter('order_id',$order_id);
                }    
            } else {
                $collection = $this->_orderCollectionFactory->create()
                    ->addFieldToFilter(
                        'customer_id',
                        $customerId
                    )
                    ->addFieldToFilter(
                        'status',
                        ['neq' => 'canceled']
                    )
                    ->addFieldToFilter(
                        'status',
                        ['neq' => 'closed']
                    );
            }

            if ($allowedDays != '') {
                $todaySecond = time();
                $allowedSeconds = $allowedDays * 86400;
                $pastSecondFromToday = $todaySecond - $allowedSeconds;
                $validFrom = date('Y-m-d H:i:s', $pastSecondFromToday);
                if ($allowedStatus == 'complete') {
                    $collection->addFieldToFilter('so.delivery_date', ['gteq' => $validFrom]);
                }
            
            }
            
            $collection->setOrder('entity_id', 'desc');
            
            foreach($collection as $sKey => $orderdata){
                if(!empty($orderdata->getEntityId())){
                    $order_inc_id = $orderdata->getEntityId();
                    $sales_itemm_query = "SELECT product_id FROM `sales_order_item` where order_id='$order_inc_id'";
                    $sales_item_obj = $this->connection->fetchAll($sales_itemm_query);
                    if(!empty($sales_item_obj))
                    {
                        foreach ($sales_item_obj as $product_data)
                        {
                            $product_id = $product_data['product_id'];
                            $return_period_type = "SELECT `value` FROM `catalog_product_entity_int` where attribute_id=179 and entity_id='$product_id' and store_id=0";
                            $return_period_type_id = $this->connection->fetchOne($return_period_type);
                            if($return_period_type_id==33)
                            {
                                $collection->removeItemByKey($orderdata->getId());
                            }
                        }
                    
                    }
                }
            }
        $this->orderCollection = $collection;
        }

        return $this->orderCollection;
    }

    public function getResolutionTypes($status = 0)
    {
        if ($status==1) {
            return [
                ['value' => '1', 'label' =>  __('Exchange')],
                ['value' => '3', 'label' => __('Cancel Items')]
            ];
        } elseif ($status == 2) {
            return [
                ['value'=>'0', 'label' =>  __('Refund')],
                ['value' => '1', 'label' =>  __('Exchange')]
            ];
        } else {
            return [
                    ['value'=>'0', 'label' =>  __('Refund')],
                    ['value' => '1', 'label' =>  __('Exchange')],
                    ['value' => '3', 'label' => __('Cancel Items')]
                ];
        }
    }
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }

    /**
     * @return array
     */
    public function getSortingSession()
    {
        return $this->_filterSorting->getNewRmaSortingSession();
    }
    /**
     * @return array
     */
    public function getFilterData()
    {
        return $this->_filterSorting->getNewRmaFilterSession();
    }
    /**
     * @return array
     */
    public function getOrderCollection($orderId)
    {
        return $this->_orderCollectionFactory->create()
                    ->addFieldToFilter(
                        'entity_id',
                        $orderId
                    );
    }
    /**
     * @return array
     */
    public function getRegionCollection()
    {
        return $this->_regionCollectionFactory->create()
                    ->addFieldToFilter('status', 1);
    }
    /**
     *
     *
     * @param int $orderId
     * @return \Magento\Sales\Model\Order
     */
    public function getOrderModel($orderId)
    {
        return $this->orderFactory->create()->load($orderId);
    }

    /**
     * @param Decimal $price
     *
     * @return [type] [description]
     */
    public function getCurrency($price)
    {
        return $currency = $this->_currency->format($price);
    }

    /**
     * @param Decimal $price
     *
     * @return [type] [description]
     */
    public function getCurrencySymbol()
    {
        return $this->_currency->getCurrencySymbol();
    }

    /**
     * @param  String Date
     *
     * @return String Timestamp
     */
    public function getTimestamp($date)
    {
        return $date = $this->_date->timestamp($date);
    }


    /**
     *
     */
    public function getAllowedStatus()
    {
        return $this->scopeConfig->getValue(
            'rmasystem/parameter/allow_order',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    /**
     *
     */
    public function getPolicy()
    {
        return $this->scopeConfig->getValue(
            'rmasystem/parameter/returnpolicy',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    /**
     * Create Customer orders data for RMA
     * @return array
     */
    public function getRmaConfigData()
    {
        $collection = $this->getRmaCollection();
        $orderDetails = [];
        $configData = [];
        $allowedStatus = $this->getAllowedStatus();
        if ($allowedStatus == 'complete') {
            foreach ($collection as $value) {
                $isWalletSystemOrder = false;
                $orderId = $value->getOrderId();
                $orderCollection = $this->getOrderCollection($orderId);
                foreach ($orderCollection as $order) {
                    if ($order->isCanceled()) {
                        continue;
                    }
                    $items = $order->getAllVisibleItems();
                    foreach ($items as $item) {
                        if ($item->getSku() === 'wk_wallet_amount') {
                            $isWalletSystemOrder = true;
                            break;
                        }
                        $qtyOrdered = $item->getQtyOrdered();
                        $qtyShiped = $item->getQtyShipped();
                        if ($qtyOrdered == $qtyShiped) {
                            $orderDetails[] = [
                              'order_id' => $orderId,
                              'entity_id' => $order->getId(),
                              'increment_id' => $order->getIncrementId(),
                              'date' => date("Y-m-d", $this->getTimestamp($order->getCreatedAt())),
                              'grand_total_format' => $order->formatPrice($order->getGrandTotal()),
                              'grand_total' => $order->getGrandTotal(),
                              'customer_id' => $order->getCustomerId()
                            ];
                        }
                    }
                    if ($isWalletSystemOrder) {
                        break;
                    }
                }
                if ($isWalletSystemOrder) {
                    continue;
                }
            }
        } else {
            foreach ($collection as $order) {
                if (!$order->isCanceled()) {
                    $isWalletSystemOrder = false;
                    $items = $order->getAllVisibleItems();
                    foreach ($items as $item) {
                        if ($item->getSku() === 'wk_wallet_amount') {
                            $isWalletSystemOrder = true;
                            break;
                        }
                    }
                    if ($isWalletSystemOrder) {
                         continue;
                    }
                    $orderDetails[] = [
                      'entity_id' => $order->getId(),
                      'increment_id' => $order->getIncrementId(),
                      'date' => date("Y-m-d", $this->getTimestamp($order->getCreatedAt())),
                      'grand_total_format' => $order->formatPrice($order->getGrandTotal()),
                      'grand_total' => $order->getGrandTotal(),
                      'customer_id' => $order->getCustomerId()
                    ];
                }
            }
        }
        $orderDetails = array_unique($orderDetails, SORT_REGULAR);
        $configData['orderDetails'] = $orderDetails;
        $configData['allowedStatus'] = $allowedStatus;
        $configData['filterData'] = $this->getFilterData();

        return $configData;
    }
}
