<?php

namespace Webkul\Rmasystem\Model\Export;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\MetadataProvider;
use Magento\Customer\Model\Customer;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Ui\Model\Export\ConvertToCsv as ConvertToCsvParent;
/**
 * Class ConvertToCsv
 */
class ConvertToCsv extends ConvertToCsvParent
{
    /**
     * @var DirectoryList
     */
    protected $directory;
    /**
     * @var MetadataProvider
     */
    protected $metadataProvider;
    /**
     * @var int|null
     */
    protected $pageSize = null;
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var TimezoneInterface
     */
    private $timezone;
    /**
     * @var Customer
     */
    protected $customer;
    /**
     * @var ProductRepositoryInterface
     */
    private $productrepository;
    /**
     * @var ResourceConnection
     */
    protected $_resource;
    /**
     * @var ItemRepository
     */
    protected $orderItemRepository;

    /**
     * @param Filesystem $filesystem
     * @param Filter $filter
     * @param MetadataProvider $metadataProvider
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param ProductRepositoryInterface $productrepository
     * @param int $pageSize
     * @throws FileSystemException
     */
    public function __construct(
        Filesystem $filesystem,
        Filter $filter,
        \Webkul\Rmasystem\Helper\Data $helper, 
        MetadataProvider $metadataProvider,
        TimezoneInterface $timezone,
        Customer $Customer,
        ProductRepositoryInterface $productrepository,
        ResourceConnection $resource,
        ItemRepository $orderItemRepository,
        $pageSize = 200
    ) {
        $this->filter = $filter;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->metadataProvider = $metadataProvider;
        $this->customer = $Customer;
        $this->helper = $helper;
        $this->productrepository = $productrepository;
        $this->pageSize = $pageSize;
        parent::__construct($filesystem, $filter, $metadataProvider, $pageSize);
        $this->timezone = $timezone;
        $this->_resource = $resource;
        $this->orderItemRepository = $orderItemRepository;
    }
    /**
     * Returns CSV file
     *
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getCsvFile()
    {
        $component = $this->filter->getComponent();
        $name = md5(microtime());
        $file = 'export/' . $component->getName() . $name . '.csv';
        $this->filter->prepareComponent($component);
        $this->filter->applySelectionOnTargetProvider();
        $dataProvider = $component->getContext()->getDataProvider();
        $fields = $this->metadataProvider->getFields($component);
        $options = $this->metadataProvider->getOptions();
        $this->directory->create('export');
        $stream = $this->directory->openFile($file, 'w+');
        $stream->lock();
        $stream->writeCsv($this->metadataProvider->getHeaders($component));
        $i = 1;
        $searchCriteria = $dataProvider->getSearchCriteria()
            ->setCurrentPage($i)
            ->setPageSize($this->pageSize);
        $totalCount = (int)$dataProvider->getSearchResult()->getTotalCount();
        while ($totalCount > 0)
        {
            $items = $dataProvider->getSearchResult()->getItems();
            if ($component->getName() == 'rmasystem_allrma_listing')
            {
                foreach ($items as $item)
                {
                    $status = $item->getStatus();
                    $status_text = $this->helper->getRmaStatusTitle($status);
                    if($status != '')
                    {
                    	if($status == "0")
							$_status = __('Pending');
                    	if($status == "1")
							$_status = __('Processing');
						if($status == "2")
							$_status = __('Solved');
						if($status == "3")
							$_status = __('Declined');
						if($status == "4")
							$_status = __('Cancelled');


                        $item->setData('status', $status_text);
                    }

                    $resolutionType = $item->getResolutionType();
                    if($resolutionType != '')
                    {
                    	$resolution = '';
		                if($resolutionType == "0") {
		                    $resolution = 'Refund';
		                } elseif ($resolutionType == "1") {
		                    $resolution = 'Exchange';
		                } else {
		                    $resolution = 'Cancel Items';
		                }

                        $item->setData('resolution_type', $resolution);
                    }

                    $rmaId = $item->getRmaId();
                    if($rmaId)
                    {
		                $connection = $this->_resource->getConnection();
		                $itemId = $connection->fetchOne("SELECT item_id FROM `wk_rma_items` WHERE rma_id = $rmaId");
		                $productName = '';
		                $productPrice = 0;
                        $shopUrl = '';
		                if($itemId) {
		                    $mageItem = $this->orderItemRepository->get($itemId);
		                    $productId = $mageItem->getProductId();
                            $sellerId = $connection->fetchOne("SELECT seller_id FROM `marketplace_product` WHERE mageproduct_id = $productId");
                            if($sellerId) {
                                $shopUrl = $connection->fetchOne("SELECT shop_url FROM `marketplace_userdata` WHERE seller_id = $sellerId");
                            }
		                    $product = $this->productrepository->getById($productId);
		                    $productName = $product->getName();
		                    $productPrice = $product->getPrice();
		                }
                        $item->setData('product_name', $productName);
                        $item->setData('product_price', $productPrice);
                        $item->setData('product_seller', $shopUrl);
                    
                		$rmaReason = $connection->fetchOne("SELECT rma_reason FROM `wk_rma_items` WHERE rma_id = $rmaId");
                        $item->setData('rma_reason', $rmaReason);
                    }

                    $custId = $item->getCustomerId();
                    if($custId) {
                    	$customer = $this->customer->load($custId);
                    	$mobile = '';
		                if ($customer->getId()) {
		                    $mobile = $customer->getMobile();
		                }
		                $item->setData('customer_mobile', $mobile);
                    }

                    $this->metadataProvider->convertDate($item, $component->getName());
                    $stream->writeCsv($this->metadataProvider->getRowData($item, $fields, $options));
                }
            } elseif ($component->getName() == 'thesgroup_alertgrid_stock_listing')
            {
                foreach ($items as $item)
                {
                    $customerId = $item->getCustomerId();
                    if($customerId)
                    {
                        $customer = $this->customer->load($customerId);
                        $cName = $customer->getFirstname().' '.$customer->getLastname();
                        $item->setData('customer_name', $cName);
                    }
                    
                    $productId = $item->getProductId();
                    if($productId)
                    {
                        $product = $this->productrepository->getById($productId);
                        $pName = $product->getName();
                        $sku = $product->getSku();
                        $item->setData('product_name', $pName);
                        $item->setData('product_sku', $sku);
                    }

                    $this->metadataProvider->convertDate($item, $component->getName());
                    $stream->writeCsv($this->metadataProvider->getRowData($item, $fields, $options));
                }
            } else if($component->getName() == 'customer_listing')
            {
                $connection = $this->_resource->getConnection();
                $attribute_id = $connection->fetchOne("SELECT `attribute_id` FROM `eav_attribute` WHERE attribute_code ='gst_number'");
                foreach ($items as $item) {
                    $entity_id = $item['entity_id'];
                    $gst_number = $connection->fetchOne("SELECT `value` FROM `customer_entity_varchar` where attribute_id='$attribute_id' and entity_id='$entity_id'");
                    if(empty($gst_number))
                    {
                        $gst_number="";
                    }
                    $item->setData('gst_number', $gst_number);
                    $this->metadataProvider->convertDate($item, $component->getName());
                    $stream->writeCsv($this->metadataProvider->getRowData($item, $fields, $options));
                }

            } else
            {
                foreach ($items as $item) {
                    $this->metadataProvider->convertDate($item, $component->getName());
                    $stream->writeCsv($this->metadataProvider->getRowData($item, $fields, $options));
                }
            }
            $searchCriteria->setCurrentPage(++$i);
            $totalCount = $totalCount - $this->pageSize;
        }
        $stream->unlock();
        $stream->close();
        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true  // can delete file after use
        ];
    }
}