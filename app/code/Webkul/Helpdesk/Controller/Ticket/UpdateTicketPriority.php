<?php
declare(strict_types=1);

namespace Webkul\Helpdesk\Controller\Ticket;

class UpdateTicketPriority extends \Magento\Framework\App\Action\Action
{

    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger,\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,\Magento\Framework\App\ResourceConnection $resource,\Magento\Framework\App\Action\Context $context,
    \Magento\Framework\View\Result\PageFactory $pageFactory,\Webkul\Helpdesk\Helper\Data $helper)
    {
        $this->logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_resource = $resource;
        $this->helper = $helper;
        $this->_pageFactory = $pageFactory;
		return parent::__construct($context);
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $noOfDays = $this->_scopeConfig->getValue('helpdesk/ticketdeafult/changeticketpriority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $receiverInfoLevelOne =  $this->_scopeConfig->getValue('helpdesk/ticketdeafult/escalationemaillevelone', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $receiverInfoLevelTwo =  $this->_scopeConfig->getValue('helpdesk/ticketdeafult/escalationemailleveltwo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $connection = $this->_resource->getConnection();
        $selectQuery = "SELECT entity_id FROM helpdesk_tickets 
        WHERE `priority` = 2 AND DATE(created_at) < DATE_SUB(CURDATE(), INTERVAL $noOfDays DAY) AND STATUS in(1,2,3)";
        $selectQueryResults = $connection->fetchAll($selectQuery);
        if(count($selectQueryResults)){
            foreach($selectQueryResults as $results){
                $idArrays[] = $results['entity_id'];
            }
        $entityIds = "'" . implode ( "', '", $idArrays) . "'";
        $sql ="UPDATE helpdesk_tickets set priority = 1 where entity_id in($entityIds)";
        $connection->query($sql);
        $template_name = "helpdesk/email/helpdesk_escalation_email";
        $senderInfo = ['name'=>'admin','email'=>'support@getmyspares.com'];
        $receiverInfoOne =
                        [
                        'name'=>'agent',
                        'email'=>$receiverInfoLevelOne
                        ];
        //$senderInfo = $this->helper->getConfigHelpdeskEmail();
        foreach($idArrays as $ticketId){
            $emailTempVariables['ticket_id'] = $ticketId;
            $this->helper->sendMail(
                $template_name,
                $emailTempVariables,
                $senderInfo,
                $receiverInfoOne
            );
        }
        echo "ChangePriority is executed";
        die();
        }
        else{
           echo "No data to update";
        }
    }
}
