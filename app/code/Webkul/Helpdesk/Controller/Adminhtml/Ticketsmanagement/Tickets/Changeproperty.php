<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Helpdesk
 * @author    Webkul
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Helpdesk\Controller\Adminhtml\Ticketsmanagement\Tickets;

use Magento\Framework\Exception\AuthenticationException;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Changeproperty extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Webkul\Helpdesk\Logger\HelpdeskLogger
     */
    protected $_helpdeskLogger;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Webkul\Helpdesk\Model\TicketsFactory $ticketsFactory,
        \Webkul\Helpdesk\Model\EventsRepository $eventsRepo,
        \Webkul\Helpdesk\Helper\Tickets $ticketsHelper,
        \Webkul\Helpdesk\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\Helpdesk\Logger\HelpdeskLogger $helpdeskLogger
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_ticketsFactory = $ticketsFactory;
        $this->_eventsRepo = $eventsRepo;
        $this->_helpdeskLogger = $helpdeskLogger;
        $this->_helper = $helper;
        $this->_ticketsHelper = $ticketsHelper;
        $this->_storeManager=$storeManager;
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $wholedata = $this->getRequest()->getParams();
        $ticket = $this->_ticketsFactory->create()->load($wholedata["id"]);
        $from = "";
        if ($wholedata['field'] == "priority") {
            $from = $ticket->getPriority();

            $ticket->setPriority($wholedata['value']);
            $ticket->save();
            $this->_eventsRepo->checkTicketEvent("priority", $wholedata["id"], $from, $wholedata['value']);
        } elseif ($wholedata['field'] == "status") {
            $tickets_data = $this->_ticketsFactory->create()->load($wholedata["id"]);
                        $tickets_data = array_filter($tickets_data->getData());    
                        $receiverInfo = ['name'=>$tickets_data['fullname'],'email'=>$tickets_data['email']];
                        $emailTempVariables['name'] = $tickets_data['fullname'];
                        $emailTempVariables['ticket_id'] = $wholedata["id"];
                        $emailTempVariables['base_url'] = $this->_storeManager->getStore()->getBaseUrl();
                        $ticketStatus = $wholedata['value'];
                        if($ticketStatus == 1){
                            $ticketStatus = 'Open';
                        }
                        if($ticketStatus == 2){
                            $ticketStatus = 'New';
                        }
                        if($ticketStatus == 3){
                            $ticketStatus = 'Pending';
                        }
                        if($ticketStatus == 4){
                            $ticketStatus = 'Resolve';
                        }
                        if($ticketStatus == 5){
                            $ticketStatus = 'Close';
                        }
                        if($ticketStatus == 6){
                            $ticketStatus = 'Spam';
                        }
                        $emailTempVariables['status'] = $ticketStatus;
                        $adminEmail = $this->_helper->getConfigHelpdeskEmail();
                        $fullname ='Admin';
                        $senderInfo =
                            [
                                'name'=>$fullname,
                                'email'=>$adminEmail
                            ];
                        if($ticketStatus == 'Close'){
                            $template_name = "helpdesk/email/helpdesk_from_customer_satus_close";
                        }
                        else{      
                            $template_name = "helpdesk/email/helpdesk_from_customer_satus"; 
                        }
                        $this->_helper->sendMail(
                            $template_name,
                            $emailTempVariables,
                            $senderInfo,
                            $receiverInfo
                        );
            $from = $ticket->getStatus();
            $ticket->setStatus($wholedata['value']);
            $ticket->save();
            $this->_eventsRepo->checkTicketEvent("status", $wholedata["id"], $from, $wholedata['value']);
        } elseif ($wholedata['field'] == "agent") {
            $from = $ticket->getToAgent();
            $ticket->setToAgent($wholedata['value']);
            $ticket->save();
            $this->_eventsRepo->checkTicketEvent("agent", $wholedata["id"], $from, $wholedata['value']);
        } elseif ($wholedata['field'] == "group") {
            $from = $ticket->getToGroup();
            $ticket->setToGroup($wholedata['value']);
            $ticket->save();
            $this->_eventsRepo->checkTicketEvent("group", $wholedata["id"], $from, $wholedata['value']);
        } elseif ($wholedata['field'] == "type") {
            $from = $ticket->getType();
            $ticket->setType($wholedata['value']);
            $ticket->save();
            $this->_eventsRepo->checkTicketEvent("type", $wholedata["id"], $from, $wholedata['value']);
        }
        $this->_eventsRepo->checkTicketEvent("ticket", $wholedata["id"], "updated");
        $this->getResponse()->setHeader('Content-type', 'text/html');
        $this->getResponse()->setBody(1);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Helpdesk::tickets');
    }
}
