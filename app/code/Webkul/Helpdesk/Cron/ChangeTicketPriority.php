<?php
declare(strict_types=1);

namespace Webkul\Helpdesk\Cron;

class ChangeTicketPriority
{

    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger,\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,\Magento\Framework\App\ResourceConnection $resource)
    {
        $this->logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_resource = $resource;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $noOfDays = $this->_scopeConfig->getValue('helpdesk/ticketdeafult/changeticketpriority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $connection = $this->_resource->getConnection();
        $updatedDate = date("Y-m-d h:i:s");
        $selectQuery = "SELECT entity_id FROM helpdesk_tickets 
        WHERE `priority` = 2 AND DATE(created_at) < DATE_SUB(CURDATE(), INTERVAL $noOfDays DAY) AND STATUS in(1,2,3)";
        $selectQueryResults = $connection->fetchAll($selectQuery);
        if(count($selectQueryResults)){
            foreach($selectQueryResults as $results){
                $ticketId = $results['entity_id'];
                $idArrays[] = $results['entity_id'];
                $query = "UPDATE `helpdesk_tickets` SET `updated_at` ='".$updatedDate."' WHERE `entity_id` = $ticketId";
                $connection->query($query);
            }
        $entityIds = "'" . implode ( "', '", $idArrays) . "'";
        $sql ="UPDATE helpdesk_tickets set priority = 1 where entity_id in($entityIds)";
        $connection->query($sql);
        $this->logger->addInfo("ChangePriority is executed.");
        }
        else{
            $this->logger->addInfo("ChangePriority is executed with no data.");
        }
    }
}
