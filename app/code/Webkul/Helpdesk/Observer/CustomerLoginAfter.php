<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Helpdesk
 * @author    Webkul
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Helpdesk\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Framework\Message\ManagerInterface;

class CustomerLoginAfter implements ObserverInterface
{
    /**
     * @param SessionManager  $coreSession
     */
    public function __construct(
        SessionManager $coreSession
    ) {
        $this->_coreSession = $coreSession;
    }

    /**
     * customer Login event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_coreSession->unsTsCustomer();
    }
}
