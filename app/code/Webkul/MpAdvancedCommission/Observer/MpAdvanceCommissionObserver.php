<?php
/**
 * @category   Webkul
 * @package    Webkul_MpAdvancedCommission
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MpAdvancedCommission\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Webkul MpAdvancedCommission MpAdvanceCommissionObserver Observer Model.
 */
class MpAdvanceCommissionObserver implements ObserverInterface
{
    /**
     * @var eventManager
     */
    protected $_eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @param \Magento\Framework\Event\Manager            $eventManager
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param \Magento\Customer\Model\Session             $customerSession
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     */
    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $orderInstance Order */
        $productId = $observer->getId();
        if (!$productId) {
            $eventData = $observer->getData();
            $productId = $eventData[0]['id'];
        }
        $product = $this->_objectManager->create(
            'Magento\Catalog\Model\Product'
        )->load($productId);
        $categoryArray = [];
        
        $quoteId = $this->_checkoutSession->getLastQuoteId();

        $helper = $this->_objectManager->create(
            'Webkul\MpAdvancedCommission\Helper\Data'
        );
        $proCommission = 0;
        if (!empty($quoteId)) {
            $quote = $this->_objectManager->create(
                'Magento\Quote\Model\Quote'
            )->load($quoteId);
            $sellerData = $helper->getSellerData($quote);
            if ($helper->getUseCommissionRule()) {
                foreach ($sellerData as $sellerId => $row) {
                    $commissionRuleCollection = $this->_objectManager->create(
                        'Webkul\MpAdvancedCommission\Model\CommissionRules'
                    )
                    ->getCollection()
                    ->addFieldToFilter(
                        "price_from",
                        ["lteq" => round($row['total'])]
                    )
                    ->addFieldToFilter(
                        "price_to",
                        ["gteq" => round($row['total'])]
                    );
                    if (!count($commissionRuleCollection)) {
                        $commissionRuleCollection = $this->_objectManager->create(
                            'Webkul\MpAdvancedCommission\Model\CommissionRules'
                        )
                        ->getCollection()
                        ->addFieldToFilter(
                            "price_from",
                            ["lteq" => round($row['total'])]
                        )
                        ->addFieldToFilter(
                            "price_to",
                            "*"
                        );
                    }
                    foreach ($commissionRuleCollection as $commissionRule) {
                        if ($commissionRule->getCommissionType() == "percent") {
                            foreach ($row['details'] as $item) {
                                $categoryArray[$item['product_id']] = [
                                    "amount" => $item['price']*$commissionRule->getAmount()/100,
                                    "type" => $commissionRule->getCommissionType()
                                ];
                            }
                        } else {
                            foreach ($row['details'] as $item) {
                                $totalSellerAmount = $sellerData[$sellerId]['total'];
                                $perComPro = $commissionRule->getAmount()*100/$totalSellerAmount;
                                $categoryArray[$item['product_id']] = [
                                    "amount" => $item['price']*$perComPro/100,
                                    "type" => $commissionRule->getCommissionType()
                                ];
                            }
                        }
                        break;
                    }
                }
            }
            if (count($categoryArray)!==0) {
                foreach ($categoryArray as $key => $value) {
                    if ($key==$productId) {
                        $proCommission = $value['amount'];
                    }
                }
            } else {
                $proCommission = $product->getCommissionForProduct();
        
                if ($proCommission == 0 || $proCommission == '' || $proCommission==null) {
                    $categoryArray = [];
                    $sellerProduct = $this->_objectManager->create(
                        'Webkul\Marketplace\Helper\Data'
                    )->getSellerProductDataByProductId($productId);
                    $sellerId = '';
                    foreach ($sellerProduct as $key => $value) {
                        $sellerId = $value['seller_id'];
                    }

                    $seller = $this->_objectManager->create(
                        'Magento\Customer\Model\Customer'
                    )->load($sellerId);

                    $categoryCommission = [];

                    if ($seller->getCategoryCommission()) {
                        $categoryCommission = array_filter(
                            json_decode($seller->getCategoryCommission(), true)
                        );
                    }
                    $category = $this->_objectManager->create(
                        'Magento\Catalog\Model\Category'
                    );
                    foreach ($product->getCategoryIds() as $id) {
                        if (isset($categoryCommission[$id])) {
                            array_push($categoryArray, $categoryCommission[$id]);
                        } else {
                            foreach ($product->getCategoryIds() as $id) {
                                array_push($categoryArray, $category->load($id)->getCommissionForAdmin());
                            }
                        }
                    }
                    if (count($categoryArray)!==0 && $categoryArray[0]!=="" && $categoryArray[0]!==null) {
                        $proCommission = max($categoryArray);
                    }
                }
            }
            $productPrice = $product->getFinalPrice();
            if ($proCommission > $productPrice) {
                $mpGlobalCommission = $this->_objectManager->create(
                    'Webkul\Marketplace\Helper\Data'
                )->getConfigCommissionRate();
            
                $commType = $this->_objectManager->create(
                            'Webkul\Marketplace\Helper\Data'
                        )->getCommissionType();
                if ($commType == 'fixed') { 
                    $proCommission = ($productPrice*$mpGlobalCommission)/100;
                } else {
                    $proCommission = $mpGlobalCommission;
                }
            }
        }
        $this->_customerSession->setData('commission', $proCommission);
    }
}
