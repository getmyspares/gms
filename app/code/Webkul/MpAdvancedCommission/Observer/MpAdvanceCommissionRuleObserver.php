<?php
/**
 * @category   Webkul
 * @package    Webkul_MpAdvancedCommission
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MpAdvancedCommission\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Webkul MpAdvancedCommission MpAdvanceCommissionRuleObserver Observer Model.
 */
class MpAdvanceCommissionRuleObserver implements ObserverInterface
{
    /**
     * @var eventManager
     */
    protected $_eventManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @param \Magento\Framework\Event\Manager            $eventManager
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param \Magento\Checkout\Model\Session             $customerSession
     */
    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
        $this->_customerSession = $customerSession;
    }

    /**
     * Observer to calculate the admin commission based on commission rules
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $orderInstance Order */
        $order = $observer->getOrder();

        $helper = $this->_objectManager->create(
            'Webkul\MpAdvancedCommission\Helper\Data'
        );

        $sellerData = $helper->getSellerData($order);

        $commission = [];

        if ($helper->getUseCommissionRule()) {
            foreach ($sellerData as $sellerId => $row) {
                foreach ($row['details'] as $item) {
                    $commissionRuleCollection = $this->_objectManager->create(
                        'Webkul\MpAdvancedCommission\Model\CommissionRules'
                    )
                    ->getCollection()
                    ->addFieldToFilter(
                        "price_from",
                        ["lteq" => round($item['price'])]
                    )
                    ->addFieldToFilter(
                        "price_to",
                        ["gteq" => round($item['price'])]
                    );
                    if (!count($commissionRuleCollection)) {
                        $commissionRuleCollection = $this->_objectManager->create(
                            'Webkul\MpAdvancedCommission\Model\CommissionRules'
                        )
                        ->getCollection()
                        ->addFieldToFilter(
                            "price_from",
                            ["lteq" => round($row['total'])]
                        )
                        ->addFieldToFilter(
                            "price_to",
                            "*"
                        );
                    }
                    foreach ($commissionRuleCollection as $commissionRule) {
                        if ($commissionRule->getCommissionType() != "percent") {
                            $commission[$item['item_id']] = [
                                "amount" => $commissionRule->getAmount() > $row['total'] ? $row['total'] : $commissionRule->getAmount(),
                                "type" => $commissionRule->getCommissionType()
                            ];
                        } else {
                            $commission[$item['item_id']] = [
                                "amount" => $commissionRule->getAmount(),
                                "type" => $commissionRule->getCommissionType()
                            ];
                        }
                        break;
                    }
                }
            }
        }
        $this->_customerSession->setData('advancecommissionrule', $commission);
    }
}
