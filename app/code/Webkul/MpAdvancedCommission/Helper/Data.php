<?php
/**
 * @category   Webkul
 * @package    Webkul_MpAdvancedCommission
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MpAdvancedCommission\Helper;

/**
 * data helper.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }

    public function getUseCommissionRule()
    {
        return $this->scopeConfig->getValue(
            'mpadvancedcommission/options/use_commission_rule',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getCommissionType()
    {
        return $this->scopeConfig->getValue(
            'mpadvancedcommission/options/commission_type',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getSellerData($order)
    {
        $sellerData = [];
        $qty = 1;
        foreach ($order->getAllItems() as $item) {
            $sellerId = $this->getSellerIdByItem($item);
            if (!isset($sellerData[$sellerId]['details'])) {
                $sellerData[$sellerId]['details'] = [];
                $sellerData[$sellerId]['total'] = 0;
            }
            array_push(
                $sellerData[$sellerId]['details'],
                [
                    'product_id'=>$item->getProductId(),
                    'item_id'=>$item->getId(),
                    'price'=>$item->getPrice() * $qty
                ]
            );
            $sellerData[$sellerId]['total']+=$item->getPrice() * $qty;
        }
        return $sellerData;
    }

    public function getSellerIdByItem($item = [])
    {
        $mpassignproductId = 0;
        $sellerId = "";
        $options = $item->getProductOptions();
        if ($item->getQtyOrdered()) {
            $qty = $item->getQtyOrdered();
        } else {
            $qty = $item->getQty();
        }
        $itemOption = $this->_objectManager->create(
            'Magento\Quote\Model\Quote\Item\Option'
        )
        ->getCollection()
        ->addFieldToFilter('item_id', $item->getId())
        ->addFieldToFilter('code', 'info_buyRequest');
        
        if (count($itemOption)) {
            foreach ($itemOption as $option) {
                $temp = json_decode($option['value'], true);
                if (isset($temp['mpassignproduct_id'])) {
                    $mpassignproductId = $temp['mpassignproduct_id'];
                }
            }
        }

        if ($mpassignproductId) {
            $mpassignModel = $this->_objectManager->create(
                'Webkul\MpAssignProduct\Model\Items'
            )->load($mpassignproductId);
            $sellerId = $mpassignModel->getSellerId();
        } else {
            $collectionProduct = $this->_objectManager->create(
                'Webkul\Marketplace\Model\Product'
            )
            ->getCollection()
            ->addFieldToFilter(
                'mageproduct_id',
                $item->getProductId()
            );
            foreach ($collectionProduct as $value) {
                $sellerId = $value->getSellerId();
            }
        }
        if ($sellerId == "") {
            $sellerId = 0;
        }
        return $sellerId;
    }
}
