<?php
/**
 * @category   Webkul
 * @package    Webkul_MpAdvancedCommission
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MpAdvancedCommission\Controller\Adminhtml\CommissionRules;

class Grid extends Index
{
    /**
     * set page data
     *
     * @return $this
     */
    public function setPageData()
    {
        return $this;
    }
}
