<?php
/**
 * @category   Webkul
 * @package    Webkul_MpAdvancedCommission
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MpAdvancedCommission\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

/**
 * Webkul MpAdvancedCommission Category Commission Save controller.
 */
class Save extends Action
{
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $_categoryRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category
     */
    protected $_categoryResourceModel;

    /**
     * @param Action\Context                                    $context
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface  $categoryRepository
     * @param \Magento\Catalog\Model\ResourceModel\Category     $categoryResourceModel
     * @codeCoverageIgnore
     */
    public function __construct(
        Action\Context $context,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Category $categoryResourceModel
    ) {
        $this->_categoryRepository = $categoryRepository;
        $this->_categoryResourceModel = $categoryResourceModel;
        parent::__construct($context);
    }

    /**
     * Get Category tree action.
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        try {
            $decodedData = $this->_objectManager->get(
                'Magento\Framework\Json\Helper\Data'
            )->jsonDecode($data['object']);
            foreach ($decodedData as $key => $val) {
                $category = $this->_categoryRepository->get($key);
                $category->setCommissionForAdmin($val);
                $category->save();
            }
            $this->getResponse()->representJson(
                $this->_objectManager->get(
                    'Magento\Framework\Json\Helper\Data'
                )->jsonEncode(true)
            );
        } catch (\Exception $e) {
            $this->getResponse()->representJson(
                $this->_objectManager->get(
                    'Magento\Framework\Json\Helper\Data'
                )->jsonEncode('')
            );
        }
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return true;
    }
}
