/**
 * @category   Webkul
 * @package    Webkul_MpAdvancedCommission
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            categoryCommission: 'Webkul_MpAdvancedCommission/js/category-commission',
            customValidator : 'Webkul_MpAdvancedCommission/js/price-rule-validator'
        }
    }
};
