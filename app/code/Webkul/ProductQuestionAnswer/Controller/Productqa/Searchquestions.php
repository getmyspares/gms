<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Controller\Productqa;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Searchquestions extends Action
{
    /**
     * @var PageFactory
     */

    protected $_resultPageFactory;
    protected $_question;
    protected $_answer;
    protected $_review;
    protected $_timezone;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;
    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $timezone
     * @param \Webkul\ProductQuestionAnswer\Model\ReviewFactory $reviewFactory
     * @param \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory
     * @param \Webkul\ProductQuestionAnswer\Model\AnswerFactory $answerFactory
     * @param \Magento\Framework\Escaper $_escaper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Webkul\ProductQuestionAnswer\Model\ReviewFactory $reviewFactory,
        \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory,
        \Webkul\ProductQuestionAnswer\Model\AnswerFactory $answerFactory,
        \Magento\Framework\Escaper $_escaper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_question = $questionFactory;
        $this->_answer = $answerFactory;
        $this->_review = $reviewFactory;
        $this->_timezone = $timezone;
        $this->_escaper = $_escaper;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * ProductQuestionAnswer page.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if ($data) {
            $data['query'] = $this->_escaper->escapeHtml($data['query']);
            $response=[];
            $final_array=[];
            $questions=$this->_question->create()->getCollection()
                ->addFieldToFilter(
                    ['content','subject'],
                    [
                            ['like'=>'%'.$data['query'].'%'],
                            ['like'=>'%'.$data['query'].'%']
                        ]
                )
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_id', $data['pid']);
            foreach ($questions as $key) {
                $answers=$this->getQuestionAnswers($key->getQuestionId());
                if ($answers->getSize()) {
                    foreach ($answers as $ans) {
                        $likes=0;
                        $dislikes=0;
                        $reviews=$this->getReview($ans->getAnswerId());
                        $like='like';
                        $dislike='dislike';
                        foreach ($reviews as $rev) {
                            if ($rev->getLikeDislike()==1) {
                                $likes++;
                            } else {
                                $dislikes++;
                            }
                            if ($rev->getReviewFrom()==$this->getRequest()->getParam('custid')) {
                                if ($key->getLikeDislike()==1) {
                                    $like='liked';
                                } else {
                                    $dislike='disliked';
                                }
                            }
                        }
                        $response['likes']=$likes;
                        $response['dislikes']=$dislikes;
                        $response['like_class']=$like;
                        $response['dislike_class']=$dislike;
                        $response['answer_id']=$ans->getAnswerId();
                        $response['answer']=$ans->getContent();
                        $response['nickname']=$ans->getRespondNickname();
                        $response['createdat']=$this->_timezone->formatDate($ans->getCreatedAt(), \IntlDateFormatter::MEDIUM);
                    }
                }
                $response['count']=$this->getAnswerCount($key->getQuestionId());
                $response['qa_date']=$this->_timezone->formatDate($key->getCreatedAt(), \IntlDateFormatter::MEDIUM);
                $response['question_id']=$key->getQuestionId();
                $response['content']=$key->getContent();
                $response['subject']=$key->getSubject();
                array_push($final_array, $response);
            }
            $result = $this->_resultJsonFactory->create();
            $result->setData($final_array);
            return $result;
        }
    }

    public function getQuestionAnswers($qid)
    {
        return $this->_answer->create()->getCollection()
            ->addFieldToFilter('question_id', $qid)
            ->setPageSize(1);
    }

    public function getReview($id)
    {
        return $this->_review->create()->getCollection()->addFieldToFilter('answer_id', $id);
    }
    /**
     * get count of answer for each question
     * @return count
     */
    public function getAnswerCount($qid)
    {
        $collection = $this->_answer->create()->getCollection()
            ->addFieldToFilter('question_id', $qid);
        return $collection->getSize();
    }
}
