<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Controller\Productqa;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Submitanswer extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    protected $_answer;

    protected $_question;
    protected $_helper;
    protected $_product;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Webkul\ProductQuestionAnswer\Model\AnswerFactory $answerFactory,
        \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory,
        \Magento\Catalog\Model\ProductFactory $product,
        \Webkul\ProductQuestionAnswer\Helper\Data $helper,
        \Magento\Customer\Api\CustomerRepositoryInterface\Proxy $customerRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_answer=$answerFactory;
        $this->_helper=$helper;
        $this->_question=$questionFactory;
        $this->_product=$product;
        $this->_storeManager=$storeManager;
        $this->_resultJsonFactory=$resultJsonFactory;
        $this->customerRepository = $customerRepository;
        parent::__construct($context);
    }

    /**
     * ProductQuestionAnswer page.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (empty($data)) {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setUrl($this->_url->getBaseUrl());
        }
        $time = date('Y-m-d H:i:s');
        $data['respond_type']='Customer';
        $data['qa_nickname'] = strip_tags($data['qa_nickname']);
        $data['qa_answer'] = strip_tags($data['qa_answer']);
        $model=$this->_answer->create();
        $model->setQuestionId($data['question_id'])
            ->setRespondFrom($data['customer_id'])
            ->setRespondType($data['respond_type'])
            ->setRespondNickname($data['qa_nickname'])
            ->setContent($data['qa_answer'])
            ->setStatus(1)
            ->setCreatedAt($time);
        $id=$model->save()->getId();
        $mailToCustomer=$this->scopeConfig->getValue(
            'wkqa/general_settings/customer_email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (isset($id) && ($mailToCustomer)) {   //send response mail
            $question=$this->_question->create()->load($data['question_id']);
            $customer_id=$question->getBuyerId();
            $p_id=$question->getProductId();
            $query=$question->getSubject();
            $adminEmail=$this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $adminUsername = 'Admin';
            $customers=$this->customerRepository->getById($customer_id);
            $customer_name=$customers->getFirstName()." ".$customers->getLastName();
            $product=$this->_product->create()->load($p_id);
            $product_name=$product->getName();
            $url= $product->getProductUrl();
            $msg= __('You have got a new response on your Question.');

            $templateVars = [
                            'store' => $this->_storeManager->getStore(),
                            'customer_name' => $customer_name,
                            'link'          =>  $url,
                            'product_name'  => $product_name,
                            'message'   => $msg,
                            'answer'   => $data['qa_answer'],
                            'question' => $query
                        ];
            $to=[$customers->getEmail()];
            $from = ['email' => $adminEmail, 'name' => 'Admin'];
            $this->_helper->sendResponseMail($templateVars, $from, $to);
        }
        $final_array=[$id];
        $this->_helper->cleanFPC();
        $result = $this->_resultJsonFactory->create();
        $result->setData($final_array);
        return $result;
    }
}
