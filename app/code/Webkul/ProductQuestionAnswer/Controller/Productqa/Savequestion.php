<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Controller\Productqa;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Customer;

/**
 * Webkul ProductQuestionAnswer Productqa controller.
 */
class Savequestion extends Action
{
    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var Customer
     */
    protected $_customer;

    /**
     * @var Product
     */
    protected $_product;

    protected $_question;

    protected $_storeManager;
    protected $_transportBuilder;
    protected $_inlineTranslation;
    protected $_url;
    protected $_helper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @param Context   $context
     * @param Session   $customerSession
     * @param Customer  $customer
     * @param Product   $product
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customer,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface\Proxy $customerRepository,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Webkul\ProductQuestionAnswer\Helper\Data $helper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_customer = $customer;
        $this->_product = $product;
        $this->_customerSession = $customerSession;
        $this->_question=$questionFactory;
        $this->_storeManager=$storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->customerRepository = $customerRepository;
        $this->_resultJsonFactory=$resultJsonFactory;
        $this->_url=$context->getUrl();
        $this->_helper = $helper;
        parent::__construct($context);
    }

    /**
     * Savequestion Action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if ($data) {
            $time = date('Y-m-d H:i:s');
            $data['buyer_id']=$this->_customerSession->getId();
            if ($this->scopeConfig->getValue(
                'wkqa/general_settings/question_approval',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )) {
                $data['status']=0;
            } else {
                $data['status']=1;
            }
            $data['subj'] = strip_tags($data['subj']);
            $data['con'] = strip_tags($data['con']);
            $data['nickname'] = strip_tags($data['nickname']);
            $model = $this->_question->create();
            $data['created_time'] = $time;
            $model->setBuyerId($data['buyer_id']);
            $model->setSubject($data['subj']);
            $model->setContent($data['con']);
            $model->setProductId($data['pid']);
            $model->setQaNickname($data['nickname']);
            $model->setStatus($data['status']);
            $model->setCreatedAt($time);
            $result['qid']=$model->save()->getId();
            $result['status'] = 1;

            //Send Mail
            $mail_to_admin=$this->scopeConfig->getValue(
                'wkqa/general_settings/admin_email',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );

            if (isset($result['qid']) && ($mail_to_admin)) {
                $adminEmail=$this->scopeConfig->getValue(
                    'trans_email/ident_general/email',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $adminUsername = 'Admin';

                $seller_name='Admin';
                $seller_email=$adminEmail;
                $url = __('Please login to give response');

                $customers=$this->customerRepository->getById($data['buyer_id']);
                $customer_name=$customers->getFirstName()." ".$customers->getLastName();
                $product=$this->_product->create()->load($data['pid']);
                $product_name=$product->getName();
                $msg= __('I would like to inform that %1 asked a question on your product.', $customer_name);
                $templateVars = [
                                'store' => $this->_storeManager->getStore(),
                                'customer_name' => $customer_name,
                                'seller_name'   => $seller_name,
                                'link'          =>  $url,
                                'product_name'  => $product_name,
                                'message'   => $msg,
                                'question'   => $data['con']
                            ];
                $to = [$seller_email];
                $this->sendAdminMail($templateVars, $adminEmail, $to);
            }
            $this->_helper->cleanFPC();
            $arr = $this->_resultJsonFactory->create();
            $arr->setData($result);
            return $arr;
        }
    }

    public function sendAdminMail($templateVars, $adminEmail, $to)
    {
        try {
            $from = ['email' => $adminEmail, 'name' => 'Admin'];

            $templateOptions = ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $this->_storeManager->getStore()->getId()];
            $this->_inlineTranslation->suspend();
            $template_id = $this->scopeConfig->getValue(
                'wkqa/email/askproductquery_admin_template',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $transport = $this->_transportBuilder->setTemplateIdentifier($template_id)
                    ->setTemplateOptions($templateOptions)
                    ->setTemplateVars($templateVars)
                    ->setFrom($from)
                    ->addTo($to)
                    ->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->_inlineTranslation->resume();
        }
    }
}
