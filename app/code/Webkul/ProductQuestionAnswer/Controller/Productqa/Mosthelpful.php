<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Controller\Productqa;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;

/**
 * Webkul MpSellerCoupons Productqa Controller.
 */
class Mosthelpful extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    protected $_review;
    protected $_answer;
    protected $_question;
    protected $_timezone;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Webkul\ProductQuestionAnswer\Model\ReviewFactory $reviewFactory,
        \Webkul\ProductQuestionAnswer\Model\AnswerFactory $answerFactory,
        \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_review=$reviewFactory;
        $this->_answer=$answerFactory;
        $this->_resource=$resource;
        $this->_question=$questionFactory;
        $this->_timezone = $timezone;
        $this->_resultJsonFactory=$resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $pid = $this->getRequest()->getParam('pid');
        $questionCollection=$this->_question->create()->getCollection();
        $questionCollection->addFieldToFilter('product_id', $pid)
                           ->addFieldToFilter('status', 1);
        $q_id=[];
        foreach ($questionCollection as $key) {
            $q_id[]=$key->getQuestionId();
        }
        $collection=$this->_answer->create()->getCollection()
                    ->addFieldToFilter('question_id', ['in'=>$q_id]);
        $collection->getSelect()
        ->join(
            ['qa_rev' => $this->_resource->getTableName('wk_qarespondreview')],
            "main_table.answer_id = qa_rev.answer_id",
            ["count"=>"COUNT(qa_rev.like_dislike)",
             "question_id"=>"main_table.question_id"
            ]
        )
        ->group('main_table.question_id')
        ->where("qa_rev.like_dislike = 1");

        $collection->setOrder('count', 'DESC');
        $q_id=[];
        foreach ($collection as $key) {
            $q_id[]=$key->getQuestionId();
        }
        $final_array=[];
        $questions=$this->_question->create()->getCollection();
        $questions->addFieldToFilter('question_id', ['in'=>$q_id]);
        foreach ($questions as $key) {
            $response=[];
            $answers=$this->getQuestionAnswers($key->getQuestionId());
            if ($answers->getSize()) {
                foreach ($answers as $ans) {
                    $likes=0;
                    $dislikes=0;
                    $reviews=$this->getReview($ans->getAnswerId());
                    $like='like';
                    $dislike='dislike';
                    foreach ($reviews as $rev) {
                        if ($rev->getLikeDislike()==1) {
                            $likes++;
                        } else {
                            $dislikes++;
                        }
                        if ($rev->getReviewFrom()==$this->getRequest()->getParam('custid')) {
                            if ($key->getLikeDislike()==1) {
                                $like='liked';
                            } else {
                                $dislike='disliked';
                            }
                        }
                    }
                    $response['likes']=$likes;
                    $response['dislikes']=$dislikes;
                    $response['like_class']=$like;
                    $response['dislike_class']=$dislike;
                    $response['answer_id']=$ans->getAnswerId();
                    $response['answer']=$ans->getContent();
                    $response['nickname']=$ans->getRespondNickname();
                    $response['createdat']=$this->_timezone->formatDate($ans->getCreatedAt(), \IntlDateFormatter::MEDIUM);
                }
            }
            $response['count']=$this->getAnswerCount($key->getQuestionId());
            $response['qa_nickname']=$key->getQaNickname();
            $response['qa_date']=$this->_timezone->formatDate($key->getCreatedAt(), \IntlDateFormatter::MEDIUM);
            $response['question_id']=$key->getQuestionId();
            $response['content']=$key->getContent();
            $response['subject']=$key->getSubject();
            array_push($final_array, $response);
        }
        $result = $this->_resultJsonFactory->create();
        $result->setData($final_array);
        return $result;
    }

    public function getQuestionAnswers($qid)
    {
        return $this->_answer->create()->getCollection()
            ->addFieldToFilter('question_id', $qid)
            ->setPageSize(1);
    }

    public function getReview($id)
    {
        return $this->_review->create()->getCollection()->addFieldToFilter('answer_id', $id);
    }
    /**
     * get count of answer for each question
     * @return count
     */
    public function getAnswerCount($qid)
    {
        $collection = $this->_answer->create()->getCollection()
            ->addFieldToFilter('question_id', $qid);
        return $collection->getSize();
    }
}
