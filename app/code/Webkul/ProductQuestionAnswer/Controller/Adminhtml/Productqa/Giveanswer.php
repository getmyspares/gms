<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Controller\Adminhtml\Productqa;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Giveanswer extends Action
{
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    protected $_answer;

    protected $_question;

    protected $_product;

    protected $_qahelper;
    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;
    protected $_timezone;

    /**
     * @param Context       $context
     * @param PageFactory   $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Webkul\ProductQuestionAnswer\Model\AnswerFactory $answerFactory,
        \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Model\ProductFactory $product,
        \Webkul\ProductQuestionAnswer\Helper\Data $helper,
        \Magento\Customer\Api\CustomerRepositoryInterface\Proxy $customerRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->_answer=$answerFactory;
        $this->_question=$questionFactory;
        $this->_resultJsonFactory=$resultJsonFactory;
        $this->_timezone = $timezone;
        $this->_qahelper=$helper;
        $this->_product=$product;
        $this->customerRepository = $customerRepository;
        parent::__construct($context);
    }

    /**
     * Giveanswer page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $data=$this->getRequest()->getParams();
        $time = date('Y-m-d H:i:s');
        $data['respond_type']='Admin';
        $data['nickname']='Admin';
        $data['time']=$this->_timezone->formatDate($time, \IntlDateFormatter::MEDIUM,true);
        $data['content'] = strip_tags($data['content']);
        $model=$this->_answer->create();
        $model->setQuestionId($data['pid'])
            ->setRespondFrom(0)
            ->setRespondType($data['respond_type'])
            ->setRespondNickname($data['nickname'])
            ->setContent($data['content'])
            ->setStatus(1)
            ->setCreatedAt($time);
        $id=$model->save()->getId();
        $mailToCustomer=$this->scopeConfig->getValue(
            'wkqa/general_settings/customer_email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (isset($id) && ($mailToCustomer)) {   //send response mail
            $question=$this->_question->create()->load($data['pid']);
            $customer_id=$question->getBuyerId();
            $p_id=$question->getProductId();
            $query=$question->getSubject();
            $adminEmail=$this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $adminUsername = 'Admin';
            $customers=$this->customerRepository->getById($customer_id);
            $customer_name=$customers->getFirstName()." ".$customers->getLastName();
            $product=$this->_product->create()->load($p_id);
            $product_name=$product->getName();
            $url= $product->getProductUrl();
            $msg= __('You have got a new response on your Question.');

            $templateVars = [
                            'customer_name' => $customer_name,
                            'link'          =>  $url,
                            'product_name'  => $product_name,
                            'message'   => $msg,
                            'answer'   => $data['content'],
                            'question' => $query
                        ];
            $to=[$customers->getEmail()];
            $from = ['email' => $adminEmail, 'name' => 'Admin'];
            $this->_qahelper->sendResponseMail($templateVars, $from, $to);
        }
        $this->_qahelper->cleanFPC();
        $data['answer_id']=$id;
        $result = $this->_resultJsonFactory->create();
        $result->setData($data);
        return $result;
    }
}
