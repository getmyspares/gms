<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Controller\Adminhtml\Productqa;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Ui\Component\MassAction\Filter;

class Massdisable extends \Magento\Backend\App\Action
{
    protected $_question;
    protected $_qahelper;

    public function __construct(
        \Magento\Ui\Component\MassAction\Filter $filter,
        Action\Context $context,
        \Webkul\ProductQuestionAnswer\Model\QuestionFactory $questionFactory,
        \Webkul\ProductQuestionAnswer\Helper\Data $helper
    ) {
        $this->_filter = $filter;
        $this->_question = $questionFactory;
        $this->_qahelper = $helper;
        parent::__construct($context);
    }
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_ProductQuestionAnswer::manageqa');
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $questionCollection = $this->_question->create();
            $collection = $this->_filter->getCollection($questionCollection->getCollection());
            foreach ($collection as $ques) {
                $ques->setId($ques->getQuestionId())
                        ->setStatus(0)
                        ->save();
            }
            $this->_qahelper->cleanFPC();
            $this->messageManager->addSuccess(__('Question(s) disabled successfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }
}
