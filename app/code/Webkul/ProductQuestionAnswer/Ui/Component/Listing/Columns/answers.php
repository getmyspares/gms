<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class answers extends Column
{
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
    
        $this->_urlBuilder = $urlBuilder;
        $this->connection = $resourceConnection->getConnection();
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $storeId = $this->context->getFilterParam('store_id');
            foreach ($dataSource['data']['items'] as &$item) {
              $question_id =$item['question_id'];
              $query = "select * from wk_qaanswer where question_id ='$question_id'";
              $answersCollection = $this->connection->fetchAll($query);
              $answer=[];
              foreach($answersCollection as $answerData)
              {
                $answer[] = $answerData['content'];
              }
              $answer_string = implode("  |  ",$answer);
              $item[$this->getData('name')] = $answer_string;
            }
        }
        return $dataSource;
    }
}
