<?php
 
namespace Webkul\ProductQuestionAnswer\Api;
 
interface QuestionAnswerRepositoryInterface
{
    /**
     * Delete test by ID.
     */
    public function deleteById($testId);
}
