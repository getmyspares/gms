/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            mpqa_tab:        'Webkul_ProductQuestionAnswer/js/mpqa_tab',
            giveanswer:      'Webkul_ProductQuestionAnswer/js/giveanswer'
        }
    }
};