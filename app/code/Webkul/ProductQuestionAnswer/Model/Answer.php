<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ProductQuestionAnswer
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductQuestionAnswer\Model;

use Webkul\ProductQuestionAnswer\Api\Data\AnswerInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Answer extends \Magento\Framework\Model\AbstractModel implements AnswerInterface, IdentityInterface
{
    /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**#@+
     * answer Status
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * ProductQuestionAnswer Answer cache tag
     */
    const CACHE_TAG = 'wk_answer';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_answer';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_answer';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webkul\ProductQuestionAnswer\Model\ResourceModel\Answer');
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteFaq();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route Images
     *
     * @return \Webkul\ProductQuestionAnswer\Model\Answer
     */
    public function noRouteFaq()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\ProductQuestionAnswer\Api\Data\Answer
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
