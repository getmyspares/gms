<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Marketplace\Model\Seller\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Name implements OptionSourceInterface
{
    /**
     * @var \Webkul\Marketplace\Model\Seller
     */
    protected $marketplaceSeller;

    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(
    \Webkul\Marketplace\Model\Seller $marketplaceSeller,
    \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->marketplaceSeller = $marketplaceSeller;
        $this->connection = $resourceConnection->getConnection();
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $seller_data=$this->connection->fetchAll("SELECT a.seller_id,a.shop_title,a.shop_url,b.firstname,b.lastname FROM `marketplace_userdata` as a left join customer_entity as b on b.entity_id=a.seller_id where a.is_seller=1");
        foreach ($seller_data as $key => $seller) {
            $firstname=$seller['firstname'];
            $lastname=$seller['lastname'];
            $shop_url=$seller['shop_url'];
            $seller_id=$seller['seller_id'];
            $value=$firstname." ".$lastname;
            /* doing this as seller collection has the same filter , do not allow seller if postcode not present @ritesh31january2022*/
            $is_postcode_vaaialbale=$this->connection->fetchOne("SELECT * FROM `seller_postcode` where seller_id='$seller_id'");
            if($is_postcode_vaaialbale)
            {
                $options[] = [
                    'label' => $shop_url,
                    'value' => $value,
                ];
            }
        }
        return $options;
    }
}
