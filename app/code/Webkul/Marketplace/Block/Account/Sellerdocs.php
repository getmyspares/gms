<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Block\Account;

use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Webkul Marketplace Account Editprofile Block
 */
class Sellerdocs extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Webkul\Marketplace\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        // \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        DataPersistorInterface $dataPersistor,
        \Webkul\Marketplace\Helper\Data $helper,
        array $data = []
    ) {
        $this->_countryCollectionFactory = $countryCollectionFactory;
        // $this->_wysiwygConfig = $wysiwygConfig;
        $this->dataPersistor = $dataPersistor;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    public function getWysiwygConfig()
    {
        $config = $this->_wysiwygConfig->getConfig();
        $config = json_encode($config->getData());
    }
    
    public function getSellerDocsFields(){	
			$fldArr = array(
				'address_proof_aadhar_copy' => 'Address Proof – Aadhar copy',
				'application_form_signed_by_par' => 'Application form Signed by Partner',
				'audited_balance_sheet_and_fina' => 'Audited Balance sheet and financials for last 3 year',
				'bank_statement' => 'Bank Statement',
				'business_partner_information_f' => 'Business Partner Information for TPT',
				'certificate_of_incorporation' => 'Certificate of Incorporation',
				'gstn_certificate_copy' => 'GSTN Certificate copy',
				'moa_aoa' => 'MOA/AOA',
				'office_photographs_and_address' => '4~5 Office photographs and address proof',
				'pan_cad_copy' => 'PAN Card copy',
				'relationship_declaration_form' => 'Relationship Declaration Form',
				'sales_intermediary' => 'Sales intermediary',
				'tpt_questionnaire' => 'TPT Questionnaire',
				'trade_partner_assessment_form' => 'Trade Partner Assessment form'       
			); 
			return  $fldArr; 
	}

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}
