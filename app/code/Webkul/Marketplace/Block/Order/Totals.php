<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Marketplace\Block\Order;

use Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection;

class Totals extends \Magento\Sales\Block\Order\Totals
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helper;

    /**
     * @var Collection
     */
    protected $orderCollection;

    /**
     * Associated array of seller order totals
     * array(
     *  $totalCode => $totalObject
     * )
     *
     * @var array
     */
    protected $_totals;

    /**
     * @param \Webkul\Marketplace\Helper\Data                   $helper
     * @param \Magento\Framework\Registry                       $coreRegistry
     * @param Collection                                        $orderCollection
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array                                             $data
     */
    public function __construct(
        \Webkul\Marketplace\Helper\Data $helper,
        \Magento\Framework\Registry $coreRegistry,
        Collection $orderCollection,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->orderCollection = $orderCollection;
        parent::__construct(
            $context,
            $coreRegistry,
            $data
        );
    }

    /**
     * Get totals source object
     *
     * @return Order
     */
    public function getSource()
    {
        $collection = $this->orderCollection
        ->addFieldToFilter(
            'main_table.order_id',
            $this->getOrder()->getId()
        )->addFieldToFilter(
            'main_table.seller_id',
            $this->helper->getCustomerId()
        )->getSellerOrderTotals();
        return $collection;
    }


    protected function _initTotals()
    {
        $this->_totals = [];
        $source = $this->getSource();
       /*  echo "<pre>";
			print_r($source); 
		echo "</pre>"; */ 
	    $order = $this->getOrder(); 
        if (isset($source[0])) {
           $source = $source[0];
            $orderid = $source['order_id'];
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderid);
			$order_inc_id=$order->getIncrementId();
			$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
           $newArray =  $tax_helpers->order_amount_details($order_inc_id);
			
			$total_gstt=0;
			$total_gst=0;
			
			foreach ($order->getAllItems() as $item)
			{
				$item_id=$item->getId();
				$product_id=$item->getProductId();
				
				$productArray=$tax_helpers->order_item_details_product($order_inc_id,$product_id);
			
				$total_gst=$productArray[0]['total_gst'];
				$total_gstt=$total_gstt+$total_gst;
				 
			}   	
			
				 $total_gst=$total_gstt;
			
			 
				$newcal =  json_decode($newArray);
				/*   echo "<pre>";
					print_r($newcal);
				echo "</pre>";   */
				
				/*calculated value */
				//$subtotal = $newcal->subtotal;
				$total_order_amount = $newcal->grand_total;  
				$subtotal_order = $newcal->baseprice;
				$shipping_expense = $newcal->shipping_expense;
				//$total_gst = $newcal->total_gst;
				$net_admin_comm = $newcal->net_admin_comm;
				$total_discount = $newcal->total_discount;
				$total_amount_order = $newcal->total_amount;
				
				
				
				$adminArray = $tax_helpers->order_amount_details($order_inc_id); 
				$adminArray =  json_decode($adminArray); 
				
				$admin_base_comm=$adminArray->admin_base_comm; 
				$admin_comm_tax=$adminArray->admin_comm_tax; 
				
				$baseprice=$adminArray->baseprice;
				$gst_tcs_rate=$adminArray->gst_tcs_rate; 
				$gst_tcs=($baseprice*$gst_tcs_rate)/100;	
				
				$gross_comm_due=$adminArray->gross_comm_due; 
				$gst_gross_comm_due=$adminArray->gst_gross_comm_due; 
				$admin_comm_tds_rate=$adminArray->admin_comm_tds_rate; 
				$tds_pi_comm=($gross_comm_due*$admin_comm_tds_rate)/100;	
				
				
				$net_comm=$gross_comm_due+$gst_gross_comm_due; 
				
				$tds_pi_comm= number_format($tds_pi_comm, 2, '.', '');  
				
				$gross_remittance_pi=$net_comm+$gst_tcs-$tds_pi_comm;
				
				
				
				//$less_admin_commission = $newcal->net_admin_comm;
				$less_admin_commission = $gross_remittance_pi; 
				
				$total_discount = $newcal->total_discount;
				$pay_to_seller = $newcal->pay_to_seller;
				$pay_to_seller = $newcal->due_to_seller; 
				/*calculated  value end */
				$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
			 $newOnee = $tax_helpers->seller_order_page($order_inc_id);
			  $new =0;
				$new = json_decode($newOnee);
				$igst_amount = 0;
				$cgst_amount = 0;
				$sgst_amount = 0;
				$ugst_amount = 0;
				$total_admin_comm = 0;
				if(!empty($new))
				{
					
				$total_amount = $new->total_amount;
				$igst_amount = $new->igst_amount;
				$cgst_amount = $new->cgst_amount;
				$sgst_amount = $new->sgst_amount;
				$ugst_amount = $new->ugst_amount;
				$total_admin_comm = $new->total_admin_comm;
				$newGst = $igst_amount+$cgst_amount+$sgst_amount+$ugst_amount;
				$total_amount= $total_amount-($newGst);
				
				}
				
			$newShippings = $order->getData();
			$newShipping = $newShippings['base_shipping_amount'];
			if(!empty($newShipping))
			{
			  $shippingamount = $newShipping;
			}
			else
			{
				$shippingamount ='40';
			}
            $taxToSeller = $source['tax_to_seller'];
            $currencyRate = $source['currency_rate'];
            $subtotal = $source['magepro_price'];
            $adminSubtotal = $source['total_commission'];
           
            $refundedShippingAmount = $source['refunded_shipping_charges'];
            $couponAmount = $source['applied_coupon_amount'];
            $totaltax = $source['total_tax'];
            $totalCouponAmount = $source['coupon_amount'];

            $admintotaltax = 0;
            $vendortotaltax = 0;
            if (!$taxToSeller) {
                $admintotaltax = $totaltax;
            } else {
                $vendortotaltax = $totaltax;
            }

            $totalOrdered = $this->getOrderedAmount($source);

            $vendorSubTotal = $this->getVendorSubTotal($source);

            $adminSubTotal = $this->getAdminSubTotal($source);

            $this->_totals = [];

            $this->_totals['subtotal'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'subtotal',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $subtotal_order),
                    'label' => __('Taxable Value') 
                ]
            );
				
            $this->_totals['tax'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'tax',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $total_gst),
                    //baljit 'label' => __('Total Tax')
                    'label' => __('GST')
                ]
            );
			
            $this->_totals['shipping'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'shipping',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $shipping_expense),
                    'label' => __('Shipping & Handling')
                ]
            );
			
			  $this->_totals['total_order_amount'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'total_order_amount',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $total_order_amount),
                    'label' => __('Total Order Amount')
                ] 
            );
			
			  $this->_totals['less_admin_commission'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'less_admin_commission',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $less_admin_commission),
                    'label' => __('Less Admin Commission')
                ] 
            );
			
			  $this->_totals['total_discount'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'total_discount',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $total_discount),
                    'label' => __('Discount')
                ]
            );
			
			  $this->_totals['net_total'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'net_total',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $pay_to_seller),
                    'label' => __('Net Total')
                ]
            ); 
			
			
 

			/* 
            $this->_totals['admin_commission'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'admin_commission',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $net_admin_comm),
                    'label' => __('Total Admin Commission')
                ]
            );
			 $this->_totals['discount'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'discount',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $total_discount),
                    'label' => __('Discount')
                ]
            ); */
			
         /*    $this->_totals['vendor_total'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'vendor_total',
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $vendorSubTotal),
                    'label' => __('Total Vendor Amount')
                ]
            ); */
			
             

            if ($order->isCurrencyDifferent()) {
                $this->_totals['base_ordered_total'] = new \Magento\Framework\DataObject(
                    [
                        'code' => 'base_ordered_total',
                        'is_base' => 1,
                        'strong' => 1,
                        'value' => $totalOrdered,
                        'label' => __('Total Ordered Amount(in base currency)')
                    ]
                );
            }


            if ($order->isCurrencyDifferent()) {
                $this->_totals['base_vendor_total'] = new \Magento\Framework\DataObject(
                    [
                        'code' => 'base_vendor_total',
                        'is_base' => 1,
                        'value' => $vendorSubTotal,
                        'label' => __('Total Vendor Amount(in base currency)')
                    ]
                );
            }

            if ($order->isCurrencyDifferent()) {
                $this->_totals['base_admin_commission'] = new \Magento\Framework\DataObject(
                    [
                        'code' => 'base_admin_commission',
                        'is_base' => 1,
                        'value' => $adminSubTotal,
                        'label' => __('Total Admin Commission(in base currency)')
                    ]
                );
            }
        }
    }

    /**
     * get seller order totals array
     *
     * @param array|null $area
     * @return array
     */
    public function getOrderTotals($area = null)
    {
        $orderTotals = [];
        if ($area === null) {
            $orderTotals = $this->_totals;
        } else {
            $area = (string)$area;
            foreach ($this->_totals as $orderTotal) {
                $totalArea = (string)$orderTotal->getArea();
                if ($totalArea == $area) {
                    $this->_totals[] = $orderTotal;
                }
            }
        }
        return $orderTotals;
    }

    public function getOrderedAmount($source)
    {
		/*modify baljit  20-5-2019*/
		$orderid = $source['order_id'];
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderid);
			$newShippings = $order->getData();
			$newShipping = $newShippings['base_shipping_amount'];
			if(!empty($newShipping))
			{
			  $shippingamount = $newShipping;
			}
			else
			{
				$shippingamount ='40';
			}
			/*modify baljit  end 20-5-2019*/
        $subtotal = $source['magepro_price'];
		
        //$shippingamount = $source['shipping_charges'];
        $totalCouponAmount = $source['coupon_amount'];
        $totaltax = $source['total_tax'];
        return $subtotal+$shippingamount+$totaltax-$totalCouponAmount;
    }

    public function getVendorSubTotal($source)
    {
        $taxToSeller = $source['tax_to_seller'];
        $vendorSubtotal = $source['actual_seller_amount'];
        $shippingamount = $source['shipping_charges'];
        $refundedShippingAmount = $source['refunded_shipping_charges'];
        $couponamount = $source['applied_coupon_amount'];
        $totaltax = $source['total_tax'];

        $vendortotaltax = 0;
        if ($taxToSeller) {
            $vendortotaltax = $totaltax;
        }
        return $vendorSubtotal+$shippingamount+$vendortotaltax-$refundedShippingAmount-$couponamount;
    }

    public function getAdminSubTotal($source)
    {
        $taxToSeller = $source['tax_to_seller'];
        $adminSubtotal = $source['total_commission'];
        $totaltax = $source['total_tax'];

        $admintotaltax = 0;
        if (!$taxToSeller) {
            $admintotaltax = $totaltax;
        }
       // return $adminSubtotal+$admintotaltax;
       return $adminSubtotal;
    }

    /**
     * @return array
     */
    public function getLabelProperties()
    {
        $paymentCode = '';
        if($this->_order->getPayment()){
            $paymentCode = $this->getOrder()->getPayment()->getMethod();
        }
        if ($paymentCode == 'mpcashondelivery') {
            return 'colspan="8" class="mark"';
        }
        return 'colspan="7" class="mark"';
    }

    /**
     * Format total value based on order currency
     *
     * @param   \Magento\Framework\DataObject $total
     * @return  string
     */
    public function formatValue($total)
    {
        if ($total->getIsBase()) {
            if (!$total->getIsFormated()) {
                return $this->getOrder()->formatBasePrice($total->getValue());
            }
        } else {
            if (!$total->getIsFormated()) {
                return $this->getOrder()->formatPrice($total->getValue());
            }
        }
        return $total->getValue();
    }
}
