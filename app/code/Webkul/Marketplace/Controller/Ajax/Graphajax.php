<?php
namespace Webkul\Marketplace\Controller\Ajax;

class Graphajax extends \Magento\Framework\App\Action\Action
{
		protected $_pageFactory;
		public function __construct(
			\Magento\Framework\App\Action\Context $context,
			\Magento\Framework\View\Result\PageFactory $pageFactory)
		{
			$this->_pageFactory = $pageFactory;
			return parent::__construct($context);
		}

		public function execute()
		{
			$resultPage = $this->_pageFactory->create();
			$block = $resultPage->getLayout()
                ->createBlock('Webkul\Marketplace\Block\Account\Dashboard')
                ->setTemplate('Webkul_Marketplace::order/seller-ajax.phtml')
                ->toHtml();
               return $this->getResponse()->setBody($block);
		}
	}