<?php
	namespace Webkul\Marketplace\Controller\ItemWeight;

	class Index extends \Magento\Framework\App\Action\Action
	{
		protected $orderFactory;

		public function __construct(
			\Magento\Framework\App\Action\Context $context,
			\Magento\Framework\Message\ManagerInterface $messageManager,
			\Magento\Framework\App\Response\RedirectInterface $redirect,
			\Psr\Log\LoggerInterface $logger,
			\Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderFactory,
			\Magento\Catalog\Api\ProductRepositoryInterface $productRepo
		)
		{
			$this->productRepo = $productRepo;
			$this->orderFactory = $orderFactory;
			$this->logger = $logger;
			$this->messageManager = $messageManager;
			$this->redirect = $redirect;
			return parent::__construct($context);
		}

		public function execute()
		{			
			$input = $this->getRequest()->getPostValue();
			if (!empty($input['item_id'])) {
				$itemWeightArray = $input['item_id'];
				foreach($itemWeightArray as $item_id){
					$weight =  $input['weight_'.$item_id];
					try
					{
						/* Magento\Sales\Api\OrderItemRepositoryInterface should be used  @ritesh*/
						$orderItem  = $this->orderFactory->create()->addFieldToFilter( 'item_id',$item_id)->getFirstItem();
						$orderItem->setWeight($weight);
						$orderItem->save();
					
						$productRepoData = $this->productRepo->getById($orderItem->getProductId());
						$productRepoData->setWeight($weight);
						$productRepoData->save();
						$message=__('Weight has been updated successfully.');
					} catch(\Exception $e)
					{
						$errorMessage = $e->getMessage();
						$this->logger->debug($errorMessage);
						$message=__('Weight Cannot be Updated');
					}
				
				}
			} else 
			{
				$message=__('Weight Cannot be Updated. Invalid data provided');
			}
			$this->messageManager->addNoticeMessage($message);
			return $this->_redirect($this->redirect->getRefererUrl());
		}
	}