<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpMassUpload
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Marketplace\Controller\Order; 

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Customer;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\ImportExport\Model\Export\Adapter\Csv as AdapterCsv;
use Magento\Framework\View\Result\PageFactory;
 
class SellerDashboard extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
	
    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $_url;

    protected $collectionFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_session;

    /**
     * @var \Webkul\MpMassUpload\Helper\Data
     */
    protected $_massUploadHelper;

    /**
     * @var \Webkul\MpMassUpload\Helper\Export
     */
    protected $_helperExport;

    /**
     * @var \Webkul\Marketplace\Model\Product
     */
    protected $_mpProduct;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $marketplaceHelper;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @var AdapterCsv
     */
    protected $_writer;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory,
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Webkul\MpMassUpload\Helper\Data $massUploadHelper
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param AdapterCsv $writer
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $session,
		CollectionFactory $collectionFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Webkul\Marketplace\Helper\Data $massUploadHelper,
      
     
        AdapterCsv $writer,
        FileFactory $fileFactory
    ) { 
        $this->_resultPageFactory = $resultPageFactory;
        $this->_url = $url;
        $this->_session = $session;
		$this->orderRepository = $orderRepository;
        $this->_massUploadHelper = $massUploadHelper;
       $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        $this->_writer = $writer;
        parent::__construct($context);
    }
	
    /** 
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_url->getLoginUrl();
        if (!$this->_session->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

		
		 public function getCustomerId()
    {
        return $this->_session->getCustomerId();
    }
	
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
		$customerId='';
		    if (!($customerId = $this->getCustomerId())) {
            return false;
        }
		
		$resultPage = $this->_resultPageFactory->create();
              
		$resultPage->getConfig()->getTitle()->set(
			__('Marketplace Settlement Report')
		);
		return $resultPage;
                
    }
   
   
}
