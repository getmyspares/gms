<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpMassUpload
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Marketplace\Controller\Order; 

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Customer;
use Magento\Sales\Model\Order;
use Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\ImportExport\Model\Export\Adapter\Csv as AdapterCsv;
use Magento\Framework\View\Result\PageFactory;
 
class Export extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
	
    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $_url;

    protected $collectionFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_session;

    /**
     * @var \Webkul\MpMassUpload\Helper\Data
     */
    protected $_massUploadHelper;

    /**
     * @var \Webkul\MpMassUpload\Helper\Export
     */
    protected $_helperExport;

    /**
     * @var \Webkul\Marketplace\Model\Product
     */
    protected $_mpProduct;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $marketplaceHelper;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @var AdapterCsv
     */
    protected $_writer;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory,
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Webkul\MpMassUpload\Helper\Data $massUploadHelper
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param AdapterCsv $writer
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $session,
		CollectionFactory $collectionFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Webkul\Marketplace\Helper\Data $massUploadHelper,
      
     
        AdapterCsv $writer,
        FileFactory $fileFactory
    ) { 
        $this->_resultPageFactory = $resultPageFactory;
        $this->_url = $url;
        $this->_session = $session;
		$this->orderRepository = $orderRepository;
        $this->_massUploadHelper = $massUploadHelper;
       $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        $this->_writer = $writer;
        parent::__construct($context);
    }
	
    /** 
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_url->getLoginUrl();
        if (!$this->_session->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

		
		 public function getCustomerId()
    {
        return $this->_session->getCustomerId();
    }
	
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
		$customerId='';
		    if (!($customerId = $this->getCustomerId())) {
            return false;
        }
		
        $mphelper = $this->_objectManager->create('Webkul\Marketplace\Helper\Data');
        $isPartner = $mphelper->isSeller();
		
        if ($isPartner == 1) {
            if (count($this->getRequest()->getPost()) > 0) {
                try {
                    $helper = $this->_massUploadHelper;
					
                    //$sellerId = $this->marketplaceHelper->getCustomerId();
                    $data = $this->getRequest()->getParams();
					
					
					
                    $productsRow = [];
                    if (empty($data['order_type'])) {
                        $this->messageManager->addError(
                            __("Order type should be selected.")
                        );
                        return $this->resultRedirectFactory->create()->setPath(
                            '*/*/export',
                            ['_secure' => $this->getRequest()->isSecure()]
                        );
                    }
                    $productType = $data['order_type'];
					
					
			
                    $allowedAttributess = [];
                    if (!empty($data['custom_attributes'])) {
                        $allowedAttributess = $data['custom_attributes'];
                    }
                    $fileName = $productType.'_order.csv';
					
					
		$collectionOrders = $this->_objectManager->create(
            'Webkul\Marketplace\Model\Saleslist'
        )->getCollection()
        ->addFieldToFilter(
            'seller_id',
            ['eq' => $customerId]
        )
        ->addFieldToSelect('order_id')
        ->distinct(true);
        if ($buyerId = $this->getRequest()->getParam('customer_id')) {
            $buyerIds = $collectionOrders->getAllBuyerIds();
            if (in_array($buyerId, $buyerIds)) {
                $collectionOrders->addFieldToFilter('magebuyer_id', $buyerId);
            }
        }
		
					
					 foreach ($collectionOrders as $collectionOrder) {
            $tracking = $this->_objectManager->create(
                'Webkul\Marketplace\Helper\Orders'
            )->getOrderinfo($collectionOrder->getOrderId());

                $newCollecton = $collectionOrder->getOrderId();
				
				  $tracking = $this->orderRepository->get($collectionOrder->getOrderId());
				  //echo "testerrrrrrrrrrrrrrrrr".$tracking->getStatus();
				  
					 }
				
                    $productsRow = $this->exportOrder(
                        $productType,
                        $newCollecton, 
                        $allowedAttributess,
						$customerId
                    );
                    if (count($productsRow)) {
                        $writer = $this->_writer;
                        $writer->setHeaderCols($productsRow[0]);
                        foreach ($productsRow[1] as $dataRow) {
                            if (!empty($dataRow)) {
                                $writer->writeRow($dataRow);
                            }
                        }
                        $productsRow = $writer->getContents();

                        return $this->fileFactory->create(
                            $fileName,
                            $productsRow,
                            DirectoryList::VAR_DIR,
                            'text/csv'
                        );
                    } else { 
                        $this->messageManager->addError(
                            __("There is no Order type: %1 to export.", $productType)
                        );
                    }
                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/export',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());

                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/export',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
            } else {
                $resultPage = $this->_resultPageFactory->create();
              
                $resultPage->getConfig()->getTitle()->set(
                    __('Marketplace Order Export')
                );
                return $resultPage;
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
	
	  public function exportOrder($productType, $newCollecton, $allowedAttributess,$customerId) {
        $helper = $this->_massUploadHelper;
        $productsRow = [];
        $productsRow[0] = $this->prepareFileColumnRow($productType, $allowedAttributess);
        $productsDataRow = [];
       
			 $orderids = [];

        $mageProduct = $this->_objectManager->create(
            'Webkul\Marketplace\Model\Saleslist'
        )->getCollection()
        ->addFieldToFilter(
            'seller_id',
            ['eq' => $customerId]
        );
      
			$productData = $mageProduct->getData();
			 
			 foreach($productData as $neArray)
			 {
				
          
                $wholeData = [];
                /*Get Category Names by category id (set by comma seperated)*/
                
                $wholeData['orderid'] = $neArray['order_id'];
                $wholeData['name'] = $neArray['magepro_name'];
                $wholeData['price'] = $neArray['actual_seller_amount'];
                $wholeData['seller_id'] = $neArray['seller_id'];
                $wholeData['quantity'] = $neArray['magequantity'];
                $wholeData['created_at'] = $neArray['created_at'];
               
               
                /*Set Configurable Data*/
                $associatedData = [];
                if ($productType == 'configurable') {
                    $result = $this->getConfigurableData($mageProduct,$wholeData);
                    $wholeData = $result['parent'];
                    $associatedData = $result['child'];
                }
                 // /*Set Custom Options Values*/
               
                $productsDataRow[] = $wholeData;
                foreach ($associatedData as $value) {
                    $productsDataRow[] = $value;
                }
			}
           // }
        //}
        $productsRow[1] = $productsDataRow;
        return $productsRow;
    }

    /**
     * Prepare File Column Row
     *
     * @param string $productType
     *
     * @return array
     */
    public function prepareFileColumnRow($productType, $allowedAttributess)
    {
        $helper = $this->_massUploadHelper;
        $wholeData[] = 'orderid';
        $wholeData[] = 'name';
        $wholeData[] = 'price';
        $wholeData[] = 'seller_id';
        $wholeData[] = 'quantity';
        $wholeData[] = 'created_at';
       

        return $wholeData;
    }

   
   
}
