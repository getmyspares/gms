<?php

namespace Webkul\Marketplace\Controller\Order\Invoice;

use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Webkul\Marketplace\Controller\Order
{
    public function execute()
    {
        $helper = $this->_objectManager->create(
            'Webkul\Marketplace\Helper\Data'
        );
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            $param = $this->getRequest()->getParams();
            $incrementId = $param['real_order_id'];
            $sellerId = $this->_customerSession->getCustomerId();
            
            $invoiceHelper = $this->_objectManager->create(
                                'Webkul\Marketplace\Helper\Invoice'
                            );
            $invoiceHelper->downloadInvoices('', '', $sellerId, $incrementId);
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

}
