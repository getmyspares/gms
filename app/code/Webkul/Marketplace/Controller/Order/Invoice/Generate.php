<?php

namespace Webkul\Marketplace\Controller\Order\Invoice;

use Magento\Framework\App\Filesystem\DirectoryList;

class Generate extends \Webkul\Marketplace\Controller\Order
{
    public function execute()
    {
        $helper = $this->_objectManager->create(
            'Webkul\Marketplace\Helper\Data'
        );
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            $get = $this->getRequest()->getParams();
            $todate = date_create($get['invoice_to_date']);
            $to = date_format($todate, 'Y-m-d').' 23:59:59';
            $fromdate = date_create($get['invoice_from_date']);
            $from = date_format($fromdate, 'Y-m-d').' 00:00:00';

            try {
                $sellerId = $this->_customerSession->getCustomerId();
                $collection = $this->_objectManager->create(
                                        'Webkul\Marketplace\Model\Saleslist'
                                    )
                                    ->getCollection()
                                    ->addFieldToFilter(
                                        'seller_id',
                                        $sellerId
                                    )
                                    ->addFieldToFilter(
                                        'created_at',
                                        ['datetime' => true, 'from' => $from, 'to' => $to]
                                    )
                                    ->addFieldToSelect('order_id')
                                    ->addFieldToSelect('magerealorder_id')
                                    ->distinct(true);

                $invoiceHelper = $this->_objectManager->create(
                                    'Webkul\Marketplace\Helper\Invoice'
                                );

                foreach ($collection as $coll) {
                    $incrementId = $coll->getMagerealorderId();
                    $invoiceHelper->generatePdf($sellerId, $incrementId);
                }
                $invoiceHelper->downloadInvoices($from, $to, $sellerId, '');
                
                $this->messageManager->addSuccess("Invoice generated successfully");
                return $this->resultRedirectFactory->create()->setPath(
                        'marketplace/order/history',
                        [
                            '_secure'=>$this->getRequest()->isSecure() 
                        ]
                    );
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());

                return $this->resultRedirectFactory->create()->setPath(
                    'marketplace/order/history',
                    [
                        '_secure' => $this->getRequest()->isSecure(),
                    ]
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->messageManager->addError(
                    __('We can\'t print the invoice right now.')
                );

                return $this->resultRedirectFactory->create()->setPath(
                    'marketplace/order/history',
                    [
                        '_secure' => $this->getRequest()->isSecure(),
                    ]
                );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

}
