<?php

namespace Webkul\Marketplace\Controller\Order\Invoice;

use Magento\Framework\App\Filesystem\DirectoryList;

class Downloadall extends \Webkul\Marketplace\Controller\Order
{
    public function execute()
    {
        $helper = $this->_objectManager->create(
            'Webkul\Marketplace\Helper\Data'
        );
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            $param = $this->getRequest()->getParams();
            $incrementId = $param['real_order_id'];
            $sellerId = $this->_customerSession->getCustomerId();
            
            $invoiceHelper = $this->_objectManager->create(
                                'Webkul\Marketplace\Helper\Invoice'
                            );
            $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $sql = "SELECT ecom_order FROM `sales_order_grid` WHERE increment_id = '$incrementId'";
            $result = $connection->fetchOne($sql);
            $subDir = rand(100,999);
            $directory = realpath('pub').'/'.$subDir;
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            $invoiceHelper->generateInvoiceAtDir($incrementId, $directory);
            if($result == 'Yes'){
                $invoiceHelper->generateEcomInvoiceAtDir($incrementId, $directory);
                $invoiceHelper->generateShippingLableAtDir($incrementId, $directory);
            }

            $zipFileName = 'all_invoices_'.$incrementId.'.zip';
            $zipFileLocation = $directory.'/'.$zipFileName;

            $zip = new \ZipArchive();
            $zip->open($zipFileLocation, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            $files = new \RecursiveIteratorIterator(
                            new \RecursiveDirectoryIterator($directory),
                            \RecursiveIteratorIterator::LEAVES_ONLY
                        );

            foreach ($files as $name => $file) {
                if (!$file->isDir()) {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($directory) + 1);
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();

            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header('Content-Disposition: attachment; filename="'.$zipFileName.'"');
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".filesize($zipFileLocation));
            ob_end_flush();
            @readfile($zipFileLocation);
            
            foreach ($files as $name => $file) {
                if ($file->isDir()) {
                    rmdir($file->getRealPath());
                } else {
                    unlink($file->getRealPath());
                }
            }
            rmdir($directory);
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

}
