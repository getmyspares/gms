<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Order\Invoice;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Webkul Marketplace Order Invoice Printall Controller by date range.
 */
class Noecomship extends \Webkul\Marketplace\Controller\Order
{
    public function execute()
    {
		$post = $this->getRequest()->getPost();
		$order_id = $post['order_id'];
		
		$resource   = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $connection->query("UPDATE sales_order_grid SET ecom_order='No' WHERE entity_id='$order_id'");
        $order        = $this->_objectManager->create('Magento\Sales\Model\Order')->load($order_id);    
        $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
				$shipment     = $convertOrder->toShipment($order);
		
		foreach ($order->getAllVisibleItems() AS $orderItem) 
		{			
			if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
				continue;
			}
			$qtyShipped = $orderItem->getQtyToShip();                
			$shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
			$shipment->addItem($shipmentItem);
		}  
	 
		// Register shipment
		$shipment->register();
		$shipment->getOrder()->setIsInProcess(true);
	 
		try {
			// Save created shipment and order
			$shipment->save();
			$shipment->getOrder()->save();

			/* shipment sms */ 
			$cId = $order->getCustomerId();
			if($cId) {
				$mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$cId'");
				if($mobile) {
					$smsHelper = $this->_objectManager->create('OM\MobileOtp\Helper\SmsHelper');
					// $msg = "Shipment for your order number ".$order->getIncrementId()." is ready to ship.";
					$msg = "Dear User, shipment for your GetMySpares order ".$order->getIncrementId()."is processed and awaiting pickup by delivery partner";
					$smsHelper->send_custom_sms($mobile, $msg);
				}
			}

		} catch (\Exception $e) { 
			
			$this->messageManager->addError( 
				"Shipment Not Created Because It's already created or products ordered are out of stock"
			);    
			return false  ;
		   exit;
		} 
		 
		
	   $shipmentcreateddate = date('Y-m-d H:i:s', strtotime($post['shipping_date']));
	   $deliveryDate = date('Y-m-d H:i:s', strtotime($post['delivery_date']));

		$iscomplete = $connection->fetchOne("SELECT state FROM `sales_order` WHERE entity_id = '".$order_id."' ");
		if($iscomplete=='complete'){
			$connection->query("Update sales_order Set status='Delivered',pickup_date ='$shipmentcreateddate' ,delivery_date='$deliveryDate' where entity_id='$order_id'");
			$connection->query("Update sales_order_grid Set status='Delivered',delivery_date='$shipmentcreateddate' where entity_id='$order_id'");
			
			/* Also needed to update data in sales  and  settlement report @ritesh1april2021 */
			$connection->query("Update `om_ecom_api_status` Set shipping_type='Manually Delivered',order_status='Delivered',pickup_date ='$shipmentcreateddate' ,delivery_date='$deliveryDate' where order_id='$order_id' and movement_type='Forward'");

			$quewrrsds = "update om_sales_report set pickup_date='$shipmentcreateddate',delivery_date='$deliveryDate',order_status='Delivered' where order_id='$order_id' and report_type='1'";
			$connection->query($quewrrsds);
			
			/* Adding shipment  date and shipment id  also  in sales report  @ritesh1april2021 */
			$ship_date = $connection->fetchOne("SELECT created_at FROM `sales_shipment_grid` WHERE order_id='".$order_id."'");
			$ship_id = $connection->fetchOne("SELECT entity_id FROM `sales_shipment` WHERE order_id='".$order_id."'");
			$connection->query("Update om_sales_report set shipping_date='".$ship_date."',shipment_id='$ship_id' where order_id='$order_id'");
			

		}
		
		echo "success";
    }
}
