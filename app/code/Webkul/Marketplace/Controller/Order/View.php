<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Order;

/**
 * Webkul Marketplace Order View Controller.
 */
class View extends \Webkul\Marketplace\Controller\Order
{
    /**
     * Seller Order View page.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $helper = $this->_objectManager->create(
            'Webkul\Marketplace\Helper\Data'
        );
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            if ($order = $this->_initOrder()) {
                /*update notification flag*/
                $this->_updateNotification();
				
				/* Added By OM-Rkt */
				if( $order->getState()=='new' or 
					$order->getState()=='pending_payment' or 
					$order->getState()=='canceled'
				){
					return $this->resultRedirectFactory->create()->setPath('*/*/history',['_secure' => $this->getRequest()->isSecure()]);
					exit;
				}
				
				if( ($order->getData('subtotal_canceled') > 0) && 
					($order->getState()=='processing') && 
					($order->getInvoiceCollection()->count()>0) && 
					($order->getShipmentsCollection()->count()==0) 
				){
					$this->correctInvoiceData($order, 'Corrected - Seller order View'); 
				}
				/* Added By OM-Rkt */
				
						
                /** @var \Magento\Framework\View\Result\Page $resultPage */
                $resultPage = $this->_resultPageFactory->create();
                if ($helper->getIsSeparatePanel()) {
                    $resultPage->addHandle('marketplace_layout2_order_view');
                }
                $resultPage->getConfig()->getTitle()->set(
                    __('Order #%1', $order->getRealOrderId())
                );

                return $resultPage;
            } else {
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/history',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
	
	Public function correctInvoiceData($order, $comment = "")
    {
        if(!($order)){
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid Order'));
        }
        
		$productStockQty = [];
		foreach ($order->getAllVisibleItems() as $item) {
			$productStockQty[$item->getProductId()] = $item->getQtyCanceled();
			foreach ($item->getChildrenItems() as $child) {
				$productStockQty[$child->getProductId()] = $item->getQtyCanceled();
				$child->setQtyCanceled(0);
				$child->setTaxCanceled(0);
				$child->setDiscountTaxCompensationCanceled(0);
			}
			$item->setQtyCanceled(0);
			$item->setTaxCanceled(0);
			$item->setDiscountTaxCompensationCanceled(0);
		}

		$order->setSubtotalCanceled(0);
		$order->setBaseSubtotalCanceled(0);
		$order->setTaxCanceled(0);
		$order->setBaseTaxCanceled(0);
		$order->setShippingCanceled(0);
		$order->setBaseShippingCanceled(0);
		$order->setDiscountCanceled(0);
		$order->setBaseDiscountCanceled(0);
		$order->setTotalCanceled(0);
		$order->setBaseTotalCanceled(0);
		
		$order->setBaseTotalDue(0);
		$order->setTotalDue(0);
		$order->setBaseTotalPaid($order->getBaseGrandTotal());
		$order->setTotalPaid($order->getGrandTotal());
				
		if (!empty($comment)) {
			$order->addStatusHistoryComment($comment, false);
		}
		
		//$order->setInventoryProcessed(true);
		
		$order->setState("processing");
		$order->setStatus("New");

		$order->save();
    }
}
