<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\Notification;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;

/**
 * Webkul Marketplace Productlist controller.
 */
class Tierpriceupload extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helperData;

    /**
     * @var NotificationHelper
     */
    protected $notificationHelper;
    protected $_indexerFactory;
    public $error_message;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context                         $context
     * @param PageFactory                     $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Marketplace\Helper\Data $helperData
     * @param NotificationHelper              $notificationHelper
     * @param CollectionFactory               $collectionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Data $helperData,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        NotificationHelper $notificationHelper,
        \Magento\Framework\File\Csv $csv,
        CollectionFactory $collectionFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->helperData = $helperData;
        $this->_indexerFactory = $indexerFactory;
        $this->connection = $resource->getConnection();
        $this->csv = $csv;
        $this->notificationHelper = $notificationHelper;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $helper = $this->helperData;
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            // $resultPage = $this->_resultPageFactory->create();
            // return $resultPage;
            $files = $_FILES;
            
            if (!isset($files['tierpricesheet']) || empty($files['tierpricesheet']['tmp_name']) )
            {
                $this->messageManager->addError(
                    __('Invalid file upload attempt.Please Add a csv file')
                );
                return $this->resultRedirectFactory->create()->setPath(
                    '*/account/tierprice',
                    ['_secure' => $this->getRequest()->isSecure()]
                ); 
            } 

            if($files['tierpricesheet']['type']!="text/csv" && $files['tierpricesheet']['type']!="application/vnd.ms-excel")
            {
                $this->messageManager->addError(
                    __('Invalid file upload attempt.Please Add a csv file')
                );
                return $this->resultRedirectFactory->create()->setPath(
                    '*/account/tierprice',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }

            $file = $files['tierpricesheet'];
            $csvData = $this->csv->getData($file['tmp_name']);
            $status = $this->validateCsv($csvData);
            if($status=="No") {
                return $this->resultRedirectFactory->create()->setPath(
                    '*/account/tierprice',
                    ['_secure' => $this->getRequest()->isSecure()]
                ); 
                die();
            }

            $appState = $this->_objectManager->get('\Magento\Framework\App\State');
            $count=count($csvData);
            $tierPrices=array();
            foreach ($csvData as $row =>  $value) {
                if($row==0) {
                    continue;
                }    
                
                $sku = $value[0];
                $qty = $value[1];
                $price = $value[2];
                $ship_ecom = (strtolower($value[3])=="yes")?1:0;
                $shipping_free = (strtolower($value[4])=="yes")?1:0;
               
                
                if($row==1){
                    $previous_sku = $sku;
                }
                if(!empty($sku) && $row!=1)
                {
                    $this->saveTierPrice($previous_sku,$tierPrices);
                    $previous_sku = $sku;
                    $tierPrices=array();
                }

                $tierPrices[] = array(
                  'website_id'  => 0,
                  'cust_group'  => 32000,
                  'price_qty'   => $qty,
                  'price'       => $price,
                  'ship_ecom'       => $ship_ecom,
                  'shipping_free' => $shipping_free
                );

                if($row==($count-1))
                {
                    $this->saveTierPrice($previous_sku,$tierPrices);
                }
            }
        
            // $this->reIndexing();
            $this->messageManager->addNotice(
                __('Tier Prices has been updated successfully')
            );
            return $this->resultRedirectFactory->create()->setPath(
                '*/account/tierprice',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    public function saveTierPrice($sku,$tierPrices)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
        $product = $productRepository->get($sku);
        $product->setTierPrice($tierPrices);
        $product->save();

    }

    public function reIndexing(){
       $reindex = $this->_indexerFactory->create()->load('cataloginventory_stock');
       $reindex->reindexAll();
       $reindex = $this->_indexerFactory->create()->load('catalog_product_price');
       $reindex->reindexAll();
    }

    public function validateCsv($csvData){
        foreach ($csvData as $row => $data) {
            if($row==0)
            {
                $column1 = $data[0];
                $column2 = $data[1];
                $column3 = $data[2];
                $column4 = $data[3];
                $column5 = $data[4];
                
                if($column1!="sku")
                {
                    $this->messageManager->addError(
                        __('Column 1 should be named sku')
                    );
                    return "No";
                }
                if($column2!="qty")
                {
                    $this->messageManager->addError(
                        __('Column 2 should be named qty')
                    );
                    return "No";
                }
                if($column3!="price")
                {
                    $this->messageManager->addError(
                        __('Column 3 should be named price')
                    );
                    return "No";
                }

                if($column4!="ship_ecom")
                {
                    $this->messageManager->addError(
                        __('Column 4 should be named ship_ecom')
                    );
                    return "No";
                }
                if($column5!="shipping_free")
                {

                    $this->messageManager->addError(
                        __('Column 5 should be named shipping_free')
                    );
                    return "No";
                }
            }

            if ($row > 0){
                $row_count=$row+1;
                $sku = $data[0];
                $qty = (int)$data[1];   
                $price = (int)$data[2];
                $ship_ecom = strtolower($data[3]);
                $shipping_free = strtolower($data[4]);
                $yes_no_array=array("yes","no");    
                if(!empty($sku))
                {
                    $product_id = $this->checkSkuExist($sku);
                    if($product_id==false)
                    {
                        $this->messageManager->addError(
                            __("Sku on line number $row_count do not exist, Please check the csv again")
                        );
                        return "No";
                    }

                    $status = $this->checkProductSeller($product_id);
                    if($status==false)
                    {
                        $this->messageManager->addError(
                            __("Sku on line number $row_count do not belong to seller ,Seller is only allowed to edit its own proucts")
                        );
                        return "No";
                    }
                }
                
                
                if ((int)$qty!=$qty || (int)$qty < 0 )
                {
                    $this->messageManager->addError(
                        __("update  quantity should  be greator than zero on line number $row_count")
                    );
                    return "No";
                }
                if ((int)$price!=$price || (int)$price < 0 )
                {
                    $this->messageManager->addError(
                        __("Price should  be greator  than zero on line number $row_count")
                    );
                    return "No";
                }
                if(!in_array($ship_ecom,$yes_no_array))
                {
                    $this->messageManager->addError(
                        __("Ship on ecom should be either ( yes or no ) on line number $row_count")
                    );
                    return "No";
                }

                if(!in_array($shipping_free,$yes_no_array))
                {
                    $this->messageManager->addError(
                        __("Shipping Free should be either ( yes or no ) on line number $row_count")
                    );
                    return "No";
                }
            }
        } 
    }


    public function checkSkuExist($sku)
    {
        $query="select entity_id from catalog_product_entity where sku = '$sku'";
        $product_id = $this->connection->fetchOne($query);
        return $product_id?$product_id:false;
    }

    public function checkProductSeller($product_id)
    {
        $helper = $this->helperData;
        $seller_id = $helper->getCustomerId();
        $query = "SELECT mageproduct_id FROM `marketplace_product` where mageproduct_id='$product_id' and seller_id='$seller_id'";
        $mageproduct_id = $this->connection->fetchOne($query);
        if($mageproduct_id)
        {
            return true;
        }
        return false;
    }




}
