<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\Notification;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Webkul Marketplace Productlist controller.
 */
class Inventoryexport extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helperData;

    /**
     * @var NotificationHelper
     */
    protected $notificationHelper;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context                         $context
     * @param PageFactory                     $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Marketplace\Helper\Data $helperData
     * @param NotificationHelper              $notificationHelper
     * @param CollectionFactory               $collectionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\ResourceConnection $resource, 
        \Webkul\Marketplace\Helper\Data $helperData,
        NotificationHelper $notificationHelper,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        CollectionFactory $collectionFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->helperData = $helperData;
        $this->connection = $resource->getConnection();
        $this->notificationHelper = $notificationHelper;
        $this->collectionFactory = $collectionFactory;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_objectManager->get(
            'Magento\Customer\Model\Url'
        )->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $helper = $this->helperData;
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            // $resultPage = $this->_resultPageFactory->create();
            // return $resultPage;
            $sellerId = $helper->getCustomerId();
            $query = "SELECT * FROM `marketplace_product` where seller_id='$sellerId'";
            $seller_product_list = $this->connection->fetchAll($query);            
            $name = date('m_d_Y_H_i_s');
            $filepath = 'export/custom' . $name . '.csv';
            $this->directory->create('export');
            /* Open file */
            $stream = $this->directory->openFile($filepath, 'w+');
            $stream->lock();
            $columns = $this->getColumnHeader();
            foreach ($columns as $column) {
                $header[] = $column;
            }
            /* Write Header */
            $stream->writeCsv($header);
            foreach ($seller_product_list as $marketplace_product) {
                $product_id = $marketplace_product['mageproduct_id'];
                $quwery =  "SELECT * FROM `catalog_product_entity` as  a left join cataloginventory_stock_item  as b on a.`entity_id` = b.`product_id` where a.`entity_id` = '$product_id'";
                $pro = $this->connection->fetchAll($quwery);

                $quwery =  "SELECT * FROM `catalog_product_entity_decimal` where  entity_id='$product_id' and attribute_id='77'";
                $catalog_product_entity_decimal = $this->connection->fetchAll($quwery);

                $itemData = [];
                $itemData[] = $pro[0]['entity_id'];
                $itemData[] = $pro[0]['sku'];
                $itemData[] = $pro[0]['qty'];
                $itemData[] = $catalog_product_entity_decimal[0]['value'];
                $stream->writeCsv($itemData);    
            }
            $content = [];
            $content['type'] = 'filename'; // must keep filename
            $content['value'] = $filepath;
            $content['rm'] = '1'; //remove csv from var folder
    
            $csvfilename = "ProductInventoryExport_$name.csv";
            return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);

        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }


    /* Header Columns */
    public function getColumnHeader() {
        $headers = ['product_id','sku','qty','price'];
        return $headers;
    }
}
