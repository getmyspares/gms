<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Webkul\Marketplace\Model\Notification;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;

/**
 * Webkul Marketplace Productlist controller.
 */
class Inventoryupdate extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helperData;

    /**
     * @var NotificationHelper
     */
    protected $notificationHelper;
    protected $_indexerFactory;
    public $error_message;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context                         $context
     * @param PageFactory                     $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Marketplace\Helper\Data $helperData
     * @param NotificationHelper              $notificationHelper
     * @param CollectionFactory               $collectionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Data $helperData,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        NotificationHelper $notificationHelper,
        \Magento\Framework\File\Csv $csv,
        CollectionFactory $collectionFactory
    ) {
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->helperData = $helperData;
        $this->_indexerFactory = $indexerFactory;
        $this->connection = $resource->getConnection();
        $this->csv = $csv;
        $this->notificationHelper = $notificationHelper;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $helper = $this->helperData;
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            // $resultPage = $this->_resultPageFactory->create();
            // return $resultPage;
            $files = $_FILES;
            
            if (!isset($files['inventorydata']) || empty($files['inventorydata']['tmp_name']) )
            {
                $this->messageManager->addError(
                    __('Invalid file upload attempt.Please Add a csv file')
                );
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/productlist',
                    ['_secure' => $this->getRequest()->isSecure()]
                ); 
            } 

            if($files['inventorydata']['type']!="text/csv" && $files['inventorydata']['type']!="application/vnd.ms-excel")
            {
                $this->messageManager->addError(
                    __('Invalid file upload attempt.Please Add a csv file')
                );
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/productlist',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }

            $file = $files['inventorydata'];
            $csvData = $this->csv->getData($file['tmp_name']);
            $status = $this->validateCsv($csvData);
            if($status=="No") {
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/productlist',
                    ['_secure' => $this->getRequest()->isSecure()]
                ); 
                die();
            }
            foreach ($csvData as $row =>  $value) {
                if($row==0) {
                    continue;
                }    
                $product_id = $value[0];
                $sku = $value[1];
                $qty = $value[2];
                $price = $value[3];
                
                $query = "update cataloginventory_stock_item set qty='$qty' where product_id ='$product_id'";
                $this->connection->query($query);
                $query = "update inventory_source_item set quantity='$qty' where sku='$sku'";
                $this->connection->query($query);
                $query = "update `catalog_product_entity_decimal` set `value`='$price' where  entity_id='$product_id' and attribute_id='77'";
                $this->connection->query($query);
                $query = "update `catalog_product_index_price` set `price`='$price' where  entity_id='$product_id'";
                $this->connection->query($query);
            }
            $this->reIndexing();
            $this->messageManager->addNotice(
                __('Inventory has been updated successfully')
            );
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/productlist',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    public function reIndexing(){
       $reindex = $this->_indexerFactory->create()->load('cataloginventory_stock');
       $reindex->reindexAll();
       $reindex = $this->_indexerFactory->create()->load('catalog_product_price');
       $reindex->reindexAll();
    }

    public function validateCsv($csvData){
        foreach ($csvData as $row => $data) {
            if($row==0)
            {
                $column1 = $data[0];
                $column2 = $data[1];
                $column3 = $data[2];
                $column4 = $data[3];
                if($column1!="product_id")
                {
                    $this->messageManager->addError(
                        __('Column 1 should be named product_id')
                    );
                    return "No";
                }
                if($column2!="sku")
                {
                    $this->messageManager->addError(
                        __('Column 2 should be named sku')
                    );
                    return "No";
                }
                if($column3!="qty")
                {
                    $this->messageManager->addError(
                        __('Column 3 should be named qty')
                    );
                    return "No";
                }

                if($column4!="price")
                {
                    $this->messageManager->addError(
                        __('Column 3 should be named qty')
                    );
                    return "No";
                }
            }

            if ($row > 0){
                $product_id = $data[0];
                $qty = (int)$data[2];
                $price = (int)$data[3];
                
                $status = $this->checkProductSeller($product_id);
                if($status==false)
                {
                    $this->messageManager->addError(
                        __('Seller  can only upload its own products')
                    );
                    return "No";
                }
                if ((int)$qty!=$qty || (int)$qty < 0 )
                {
                    $this->messageManager->addError(
                        __('update  quantity should  be greator  than zero')
                    );
                    return "No";
                }
                if ((int)$price!=$price || (int)$price < 0 )
                {
                    $this->messageManager->addError(
                        __('Price should  be greator  than zero')
                    );
                    return "No";
                }
                

            }
        } 
    }

    public function checkProductSeller($product_id)
    {
        $helper = $this->helperData;
        $seller_id = $helper->getCustomerId();
        $query = "SELECT mageproduct_id FROM `marketplace_product` where mageproduct_id='$product_id' and seller_id='$seller_id'";
        $mageproduct_id = $this->connection->fetchOne($query);
        if($mageproduct_id)
        {
            return true;
        }
        return false;
    }




}
