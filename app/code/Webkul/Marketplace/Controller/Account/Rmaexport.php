<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;

/**
 * Webkul Marketplace Account Dashboard Controller.
 */
class Rmaexport extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context                         $context
     * @param PageFactory                     $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \OM\Rma\Helper\RmaReportHelper $rmaReportHelper,
        \Webkul\Marketplace\Helper\Data $webkulHelperData,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_customerSession = $customerSession;
        $this->rmaReportHelper=$rmaReportHelper;
        $this->webkulHelperData=$webkulHelperData;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    // public function dispatch(RequestInterface $request)
    // {
    //     $loginUrl = $this->_objectManager->get(
    //         'Magento\Customer\Model\Url'
    //     )->getLoginUrl();

    //     if (!$this->_customerSession->authenticate($loginUrl)) {
    //         $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
    //     }

    //     return parent::dispatch($request);
    // }

    /**
     * Seller Dashboard page.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $isPartner = $this->webkulHelperData->isSeller();
        if ($isPartner == 1) {
            $seller_id = $this->webkulHelperData->getCustomerId();
            $this->rmaReportHelper->downloadRmaReportBySellerId($seller_id);
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    private function OldeCsvProcess()
    {
                $helper = $this->_objectManager->create('Webkul\Marketplace\Helper\Data');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();

            $sql = "SELECT * FROM `wk_rma`  as a left  join `marketplace_orders` as  b on a.`order_id`=b.`order_id` where b.`seller_id`='$seller_id'";
            $result = $connection->fetchAll($sql);
            $header = array('order_id','customer_name','customer_email','customer_mobile','customer_remark','product_name','product_price','resolution_type','reason','rma_status','date','seller_remark');
            $directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
            $fileDirectoryPath = $directoryList->getPath('var');
            if(!is_dir($fileDirectoryPath))
            {
                mkdir($fileDirectoryPath, 0777, true);
            }
            $fileName = "rmalist_". date('Ymd_His').".csv";
            $filePath =  $fileDirectoryPath . '/' . $fileName;
            // $handle = fopen($filePath, 'w');
            $handle = fopen('php://memory', 'w');
            fputcsv($handle, $header);
            if(!empty($result))
            {
                foreach ($result as $rma_list)
                {

                    $customer_id =  $rma_list['customer_id'];
                    $rma_id =  $rma_list['rma_id'];
                    $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customer_id);
                    $item_id_query =  "select `item_id` from wk_rma_items where rma_id='$rma_id'";
                    $item_id = $connection->fetchOne($item_id_query);

                    $wk_rma_query = "SELECT * FROM `sales_order_item` where item_id='$item_id'";
                    $sales_order_tem = $connection->fetchAll($wk_rma_query);

                    $helper = $objectManager->create("Webkul\Rmasystem\Helper\Data");

                    /* report  data  */
                    $order_increment_id = $rma_list['increment_id'];
                    $customer_name = $customer->getFirstname()." ". $customer->getLastname();
                    $customer_email = $customer->getEmail();
                    $customer_mobile = $customer->getMobile();
                    $customer_remark = $rma_list['additional_info'];

                    if(!empty($sales_order_tem))
                    {
                        $sales_order_item = $sales_order_tem[0];
                        $product_ids=$sales_order_item['product_id'];
                        $product_name=$sales_order_item['name'];
                        $product_price=$sales_order_item['price'];
                    }

                    $resolution_type = $rma_list['resolution_type'] ? "Exchange":"Refund";

                    $reason = $helper->getRmaOrderStatusTitle($rma_list['customer_delivery_status']);
                    $rma_status = $helper->getRmaStatusTitle($rma_list['status']);
                    $date = $rma_list['created_at'];
                    $seller_remark = $rma_list['seller_remark'];

                   $row= array();
                   $row['order_id']=$order_increment_id;
                    $row['customer_name']= $customer_name;
                    $row['customer_email']=$customer_email;
                    $row['customer_mobile']=$customer_mobile;
                    $row['customer_remark']=$customer_remark;
                    $row['product_name']=$product_name;
                    $row['product_price']=$product_price;
                    $row['resolution_type']=$resolution_type ;
                    $row['reason']=$reason->__toString();
                    $row['rma_status']=$rma_status->__toString();
                    $row['date']=$date;
                    $row['seller_remark']=$seller_remark;
                    fputcsv($handle, $row);
                }

            }
            fseek($handle, 0);
            header('Content-Type: text/csv');
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Disposition: attachment; filename="'.$fileName.'";');
            fpassthru($handle);
            exit;
    }

    public function downloadCsv($file){
        if (file_exists($file)) {
            //set appropriate headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
        } 
    }
}
?>