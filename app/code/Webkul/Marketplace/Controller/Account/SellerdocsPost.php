<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Webkul Marketplace Account EditprofilePost Controller.
 */
class SellerdocsPost extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;
    
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $_mediaDirectory;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helper;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @param Context                                          $context
     * @param Session                                          $customerSession
     * @param FormKeyValidator                                 $formKeyValidator
     * @param \Magento\Framework\Stdlib\DateTime\DateTime      $date
     * @param Filesystem                                       $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Webkul\Marketplace\Helper\Data                  $helper
     * @param DataPersistorInterface                           $dataPersistor
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        FormKeyValidator $formKeyValidator,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Webkul\Marketplace\Helper\Data $helper,
        DataPersistorInterface $dataPersistor,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_customerSession = $customerSession;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_date = $date;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->helper = $helper;
        $this->dataPersistor = $dataPersistor;
        $this->_objectManager = $objectManager;
        parent::__construct(
            $context
        );
    }

    /**
     * Retrieve customer session object.
     *
     * @return \Magento\Customer\Model\Session
     */
    protected function _getSession()
    {
        return $this->_customerSession;
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_objectManager->get(
            'Magento\Customer\Model\Url'
        )->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    /**
     * Update Seller Profile Informations.
     *
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

		$block = $this->_objectManager->create('Webkul\Marketplace\Block\Account\Sellerdocs');
		$fldArr = $block->getSellerDocsFields();
				
        if ($this->getRequest()->isPost()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->resultRedirectFactory->create()->setPath(
                        'marketplace/account/sellerdocs',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }

				$fldArr = $block->getSellerDocsFields();

                $fields = $this->getRequest()->getParams();
                $sellerId = $this->helper->getCustomerId();
                $storeId = $this->helper->getCurrentStoreId();
                
				$autoId = 0;
				$collection = $this->_objectManager->create(
					'Webkul\Marketplace\Model\Seller'
				)
				->getCollection()
				->addFieldToFilter('seller_id', $sellerId);
				foreach ($collection as $value) {
					$autoId = $value->getId();
				}

				// Save seller data for current store
				$value = $this->_objectManager->create(
					'Webkul\Marketplace\Model\Seller'
				)->load($autoId);
				
				
				$value->setData('invoice_prefix',strtoupper($fields['invoice_prefix']));
				foreach($fldArr as $fldname => $fldval):
					$target = $this->_mediaDirectory->getAbsolutePath('docs/');
					try {
						/** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
						$uploader = $this->_fileUploaderFactory->create(
							['fileId' => $fldname]
						);
						$uname = $sellerId.'-'.$fldname;
						$uploader->setAllowedExtensions(['docs','docx','jpg','jpeg','png','pdf','ppt','xls','xlsx']);
						$uploader->setAllowRenameFiles(true);
						$result = $uploader->save($target,$uname.'.'.$uploader->getFileExtension()); 
						$fname = $uploader->getUploadedFileName();
						if ($result['file']) {
							$value->setData($fldname,$fname);
						}
					} catch (\Exception $e) {
						if ($e->getMessage() != 'The file was not uploaded.') {
							$this->messageManager->addError($e->getMessage());
						}
					}
				endforeach;
				
				$value->save(); 

				return $this->resultRedirectFactory->create()->setPath('marketplace/account/sellerdocs', ['_secure' => $this->getRequest()->isSecure()]);
               
            } catch (\Exception $e) {
				$msg = $e->getMessage();
				if($msg == 'Unique constraint violation found'){
					$this->messageManager->addError("Invoice prefix Already Exist.");
				}else{
					$this->messageManager->addError($e->getMessage());
				}
                
                return $this->resultRedirectFactory->create()->setPath( 'marketplace/account/sellerdocs', ['_secure' => $this->getRequest()->isSecure()] );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath( 'marketplace/account/sellerdocs', ['_secure' => $this->getRequest()->isSecure()] );
        }
    }
} 
