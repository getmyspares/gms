<?php

namespace Webkul\Marketplace\Controller\Adminhtml\Order\Invoice;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Download extends Action
{
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context       $context
     * @param PageFactory   $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Seller list page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $invoiceId = $this->getRequest()->getParam('invoice_id');
        $incrementId = $this->getRequest()->getParam('real_order_id');

        $invoiceHelper = $this->_objectManager->create(
                                    'Webkul\Marketplace\Helper\Invoice'
                                );
        $invoiceHelper->downloadInvoices('', '', '', $incrementId);

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/invoice/view',['_current' => true, 'invoice_id' => $invoiceId]);
        return $resultRedirect;
    }

    /**
     * Check for is allowed.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Marketplace::seller');
    }
}
