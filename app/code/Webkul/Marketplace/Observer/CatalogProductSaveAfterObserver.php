<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Marketplace\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;

/**
 * Webkul Marketplace CatalogProductSaveAfterObserver Observer.
 */
class CatalogProductSaveAfterObserver implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
private $_messageManager;
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param CollectionFactory                           $collectionFactory
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
		 \Magento\Framework\Message\ManagerInterface $messageManager,
        CollectionFactory $collectionFactory
    ) {
        $this->_objectManager = $objectManager;
        $this->_collectionFactory = $collectionFactory;
        $this->_date = $date;
		  $this->_messageManager = $messageManager;
    }

    /**
     * Product save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
           $productId = $observer->getProduct()->getId();
			 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory');
			$inventory_helpers->insert_inventory_master_report($productId,0); 
			  
            $status = $observer->getProduct()->getStatus();
            $productCollection = $this->_objectManager->create(
                'Webkul\Marketplace\Model\Product'
            ) 
            ->getCollection()
            ->addFieldToFilter(
                'mageproduct_id',
                $productId
            );
            foreach ($productCollection as $product) {
                if ($status != $product->getStatus()) {
                    $product->setStatus($status)->save();
                }
            }  
        } catch (\Exception $e) { 
            $this->_messageManager->addError($e->getMessage());
        }
    }   
}
