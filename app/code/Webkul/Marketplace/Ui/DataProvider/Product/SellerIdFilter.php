<?php 
namespace Webkul\Marketplace\Ui\DataProvider\Product; 
class SellerIdFilter implements \Magento\Ui\DataProvider\AddFilterToCollectionInterface 
{ 
    public function addFilter(
        \Magento\Framework\Data\Collection $collection,
        $field,
        $condition = null
    ) 
    { 
        
        if (isset($condition['like'])) 
        { 
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->create('Magento\Framework\App\ResourceConnection')->getConnection();

            $cond = $condition['like'];
            $result = $connection->fetchAll("SELECT entity_id FROM customer_grid_flat WHERE name LIKE '$cond'");

            $entityIds = '';
            if(!empty($result)) {
                $sellerIds = '';
                foreach ($result as $_result) {
                    $sellerIds .= $_result['entity_id'].',';
                }
                $sellerIds = rtrim($sellerIds,',');
                $result1 = $connection->fetchAll("SELECT mageproduct_id FROM `marketplace_product` WHERE seller_id IN ($sellerIds)");
                if(!empty($result1)) {
                    foreach ($result1 as $_result1) {
                        $entityIds .= $_result1['mageproduct_id'].',';
                    }
                    $entityIds = array_unique(rtrim($entityIds,','));
                    
                }
            }

            $collection->addFieldToFilter('entity_id', ['in' => $entityIds ]);
        } 

        if (isset($condition['eq'])) 
        { 
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->create('Magento\Framework\App\ResourceConnection')->getConnection();

            $cond = $condition['eq'];
            $result = $connection->fetchAll("SELECT entity_id FROM customer_grid_flat WHERE name LIKE '$cond'");

            $entityIds = '';
            if(!empty($result)) {
                $sellerIds = '';
                foreach ($result as $_result) {
                    $sellerIds .= $_result['entity_id'].',';
                }
                $sellerIds = rtrim($sellerIds,',');
                $result1 = $connection->fetchAll("SELECT mageproduct_id FROM `marketplace_product` WHERE seller_id IN ($sellerIds)");
                if(!empty($result1)) {
                    foreach ($result1 as $_result1) {
                        $entityIds .= $_result1['mageproduct_id'].',';
                    }
                    $entityIds = array_unique(rtrim($entityIds,','));
                }
            }

            $collection->addFieldToFilter('entity_id', ['in' => $entityIds ]);
        } 
    } 
}