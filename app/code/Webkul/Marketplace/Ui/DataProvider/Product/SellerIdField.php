<?php 
namespace Webkul\Marketplace\Ui\DataProvider\Product;
class SellerIdField implements \Magento\Ui\DataProvider\AddFieldToCollectionInterface 
{ 
    public function addField(
        \Magento\Framework\Data\Collection $collection,
        $field,
        $alias = null
    ) 
    { 
        $collection->joinField(
             'seller_id', 
             'marketplace_product', 
             'seller_id', 
             'mageproduct_id=entity_id',
             null, 
             'left' 
        );
    }
}