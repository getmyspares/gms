<?php
/**
 * IDS Infotech
 * 
 * 
 * @category   IDs
 * @package    Idsphone_Newphone
 
 */
 /**
 * IDS Infotech
 * IDS Infotech  Extension
 * 
 * @category   IDS Infotech
 * @package    Idsphone Newphone
 * @copyright  Copyright © 2006-2019 
 * @license   
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Idsphone_CustomerAttribute',
    __DIR__
);
