<?php

/**
 
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html
 
 */

namespace Baljit\Buynow\Controller\Cart;

class Add extends \Magento\Checkout\Controller\Cart\Add
{
    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */

    /* new orange mantra buy  now  */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        try {

            if (isset($params['qty']) && (int)$params['qty']<1) {

                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            } else
            {
                $this->messageManager->addNotice("Please enter a valid quantity");
                return $this->goBack();
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            if (!$product) {
                return $this->goBack();
            }

            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->get('Magento\Customer\Model\Session');
            
            if ($customerSession->isLoggedIn()) {
                $customer = $customerSession->getCustomer();
                $customer_id = $customer->getId();
    
                /* get current quote  */
                $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
                $quote = $cart->getQuote();
                $current_quote_id = $quote->getId();
    
                /* deactivate quote  */
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $quoteFactory =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
                $currentQuoteObj = $quoteFactory->create()->load($current_quote_id);
                $currentQuoteObj->setIsActive(false)->save();
    
                /* create new quote */
                
                $_customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterfaceFactory');
                $quoteFactory_2 =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
                $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                $storeId = $storeManager->getStore()->getStoreId();
                $customer   = $_customerRepository->create()->getById($customer_id);
                $quote_new    = $quoteFactory_2->create();
                $quote_new->setStoreId($storeId);
                $quote_new->assignCustomer($customer);
                $quote_new->setData('buy_now','1');
    
                /* below steps to entirely diactivate  currrent quote and create a  new  quote */
                /* add new product to cart */  
    
                $productRepository = $objectManager->get('Magento\Catalog\Model\ProductRepository');
                $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
                $dataObjectFactory = $objectManager->get('Magento\Framework\DataObject\Factory');
                $formKey = $objectManager->get('Magento\Framework\Data\Form\FormKey');
                $cartRepository = $objectManager->create('Magento\Quote\Api\CartRepositoryInterface');
                
                $quote_new->addProduct($product, $dataObjectFactory->create($params));

                $quote_new->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
                $quote_new->setTotalsCollectedFlag(false)->collectTotals();
                
                $cartRepository->save($quote_new);
                $cart_id=$quote_new->getId();

                /* $quote_new->setData('buy_now_quote_id',"$current_quote_id"); not working so this is workaround */
                $resources = $objectManager->create('Magento\Framework\App\ResourceConnection');
                $connection= $resources->getConnection();
                $sql = "UPDATE  `quote` set buy_now_quote_id='$current_quote_id' where entity_id ='$cart_id' ";
                $connection->query($sql);
                
                $sql = "select `item_id`,`row_total_incl_tax`,`price`,`qty` from quote_item  where quote_id ='$cart_id' ";
                $buy_now_items = $connection->fetchAll($sql);
                if($buy_now_items)
                {
                    $grandtotal = 0;
                    foreach($buy_now_items as $quote_tems)
                    {
                        $row_total_incl_tax = intval($quote_tems['row_total_incl_tax']);           
                        if(!$row_total_incl_tax)
                        {
                            $it_price = $quote_tems['price'];
                            $row_total_incl_tax=$quote_tems['price']*$quote_tems['qty'];
                            $item_id = $quote_tems['item_id'];
                            $sql = "update quote_item set row_total_incl_tax='$row_total_incl_tax',price_incl_tax='$it_price',row_total_with_discount='$row_total_incl_tax' where item_id ='$item_id'";
                            $buy_now_items = $connection->query($sql);          
                            $grandtotal=$grandtotal+$row_total_incl_tax;
                        }
                    }
                    if($grandtotal){
                        $sql = "update quote set subtotal='$grandtotal',grand_total='$grandtotal',subtotal_with_discount='$grandtotal' where entity_id ='$cart_id'";
                        $buy_now_items = $connection->query($sql);
                    }

                }
                
            }

            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                $baseUrl = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface')
                            ->getStore()->getBaseUrl();
                return $this->goBack($baseUrl.'checkout/', $product);
            } 
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice(
                    $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError(
                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($message)
                    );
                }
            }
            $url = $this->_checkoutSession->getRedirectUrl(true);
            if (!$url) {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                $url = $this->_redirect->getRedirectUrl($cartUrl);
            }
            return $this->goBack($url);
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $this->goBack();
        }
    }
    
    /* old baljeet  buy  now */ 
    public function execute_old()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            if (!$product) {
                return $this->goBack();
            }

            $cartProducts = $this->_objectManager->create('Baljit\Buynow\Helper\Data')
                             ->getConfig('buynow/general/keep_cart_products');
            if (!$cartProducts) {
                $this->cart->truncate(); //remove all products from cart
            } 

            $this->cart->addProduct($product, $params);
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }

            $this->cart->save();
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                $baseUrl = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface')
                            ->getStore()->getBaseUrl();
                return $this->goBack($baseUrl.'checkout/', $product);
            } 
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice(
                    $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError(
                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($message)
                    );
                }
            }
            $url = $this->_checkoutSession->getRedirectUrl(true);
            if (!$url) {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                $url = $this->_redirect->getRedirectUrl($cartUrl);
            }
            return $this->goBack($url);
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $this->goBack();
        }
    }
}
