# Magento 2 BuyNow 

This module add "BuyNow" button on product view page and list page to process directly checkout.


# Installation Instruction

* Copy the content of the repo to the <b>app/code/Prince/Buynow/</b> folder
* Run command:
<b>php bin/magento setup:upgrade</b>
* Run Command:
<b>php bin/magento setup:static-content:deploy</b>
* Now Flush Cache: <b>php bin/magento cache:flush</b>

<b>Store->Configuration->Mageprince->Buy Now->Settings</b>
