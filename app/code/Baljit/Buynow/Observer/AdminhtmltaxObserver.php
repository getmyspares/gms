<?php
/**
 * IDs Software.
 *
 * @category  IDs
 * @package   IDs
 * @author    IDs
 * @copyright Copyright (c) IDs Software Private Limited
 * @license   https://store.ids.com/license.html
 */
 
namespace Baljit\Buynow\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class AdminhtmltaxObserver implements ObserverInterface
{
    const XML_PATH_FAQ_URL = 'buynows/general/admin_comm_tax';

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * ConfigChange constructor.
     * @param RequestInterface $request
     * @param WriterInterface $configWriter
     */
    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;

    }

    public function execute(EventObserver $observer)
    {
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$global_comm_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\globalcomm'); 
		
		$global_comm_helpers->store_global_comm_temp_report();
		$global_comm_helpers->create_global_comm_report();
		
		 
		/* 
        $admin_comm_taxParams = $this->request->getParam('groups');
        foreach($admin_comm_taxParams as $key => $value) {
   
		$admin_comm = $value['fields']['button_titlee']['value'];
		$admin_comm_tax = $value['fields']['admin_comm_tax']['value'];
		$admin_comm_tds = $value['fields']['admin_comm_tds']['value'];
		$gst_tcs = $value['fields']['gst_tcs']['value'];
		$shipping_gst = $value['fields']['shipping_gst']['value'];
		$shipping_tds = $value['fields']['shipping_tds']['value'];
		$razorpay_tds = $value['fields']['razorpay_tds']['value'];
		$nodal_tax = $value['fields']['nodal_tax']['value'];
		}
		if(empty($admin_comm))
		{
			$admin_comm=0;
		}
		if(empty($admin_comm_tax))
		{
			$admin_comm_tax=0;
		}
		if(empty($admin_comm_tds))
		{
			$admin_comm_tds=0;
		}
		 if(empty($gst_tcs))
	    {
			$gst_tcs=0;
	    } 
		if(empty($shipping_gst))
		{
			$shipping_gst=0;
		}
		if(empty($shipping_tds))
		{
			$shipping_tds=0;
		}
		if(empty($razorpay_tds))
		{
			$razorpay_tds=0;
		}
		if(empty($nodal_tax))
		{
			$nodal_tax=0;
		}
		$newarray=array(  
			'admin_comm'=>$admin_comm,	
			'admin_comm_tax'=>$admin_comm_tax,	
			'admin_comm_tds'=>$admin_comm_tds,
			'gst_tcs'=>$gst_tcs,	  
			'shipping_gst'=>$shipping_gst,	 
			'shipping_tds'=>$shipping_tds,	
			'razorpay_tds'=>$razorpay_tds,  
			'nodal_tax'=>$nodal_tax,  
			'cod_tax'=>5  
		);	  
		 
		$json=json_encode($newarray);
		
		$today=date('y-m-d H:i:s');
		
       $sql="insert into commission_general_report (cr_date,report_details) values ('".$today."','".$json."')";
		$connection->query($sql);
        return $this;
		
		*/
    } 
}