<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\QuickBooksOnline\Observer\Customer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;

/**
 * Class Update
 */
class Register extends AbstractCustomer implements ObserverInterface 
{

    /**
     * Customer register success event
     *
     * @param Observer $observer
     * @return string|void
     */
    public function execute(Observer $observer)
    {
       if ($this->isConnected() && $this->isConnected() == 1) 
	   {
			
			//mail('ramit.j@idsil.com', 'register', 'aaaaaaaa');
			
           
			try {
				
                $eventName = $observer->getEvent()->getName();
                $customer = $observer->getEvent()->getCustomer();
                $id = $customer->getId();
                if ($id && $this->isEnabled()) {
                    if ($this->isImmediatelyMode()) 
					{
						$registryObject = ObjectManager::getInstance()->get('Magento\Framework\Registry');
                        if ($registryObject->registry('check_to_syn_customer'.$id) == null) 
						{
                            $this->_customer->sync($id, true);
                        }
				   
                    } else {
                        $this->addToQueue($id); 
                    }
                }
				
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
			
        }  
    }    
}
