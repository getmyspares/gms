<?php 
namespace Magenest\QuickBooksOnline\Controller\Connection;
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magenest\QuickBooksOnline\Helper\Oauth as OauthHelper;
use Magento\Framework\Exception\LocalizedException;
use Magenest\QuickBooksOnline\Model\OauthFactory as OauthModelFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Config\Model\Config as ConfigModel;
use Magenest\QuickBooksOnline\Model\Authenticate; 
use Magento\Framework\Validator\Exception;
use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Razorpay\Magento\Model\Config;

$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$path=$api_helpers->get_path();			


require_once $path.'/app/code/Razorpay/Razorpay/Razorpay.php';   
/** 
 * Class Index  
 * @package Magenest\Magenest\Controller\Connection
 */
class Razorpayorder extends \Magento\Framework\App\Action\Action 
{
  
    /**
     * Success constructor.
     * @param Context $context
     * @param Authenticate $authenticate
     */
	protected $authenticate;
	
	protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $orderRepository;
    
    protected $order;
    
	protected $orderModel;
	
	protected $orderSender;
	
	protected $logger;
	
	protected $customerRepository;
	
	protected $quoteRepository;

    public function __construct(
        Context $context,
        Authenticate $authenticate,
		\Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\Data\OrderInterface $order,
		\Magento\Sales\Model\OrderFactory $orderModel,
		\Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,           
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
		\Magento\Framework\App\Request\Http $request
    ) {
		parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );

        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
        $this->checkoutFactory = $checkoutFactory;
        $this->cache = $cache;
        $this->orderRepository = $orderRepository;
        $this->order           = $order;
        $this->connection = $resourceConnection->getConnection();
        $this->logger          = $logger;
		$this->orderModel = $orderModel;
		$this->orderSender = $orderSender;
		$this->storeManagement    = $storeManagement;
        $this->quoteRepository = $quoteRepository;   
        $this->customerRepository = $customerRepository;
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
        
        $this->authenticate = $authenticate;
	    $this->request = $request;
	    	  
        $this->key_id = $this->config->getConfigData(Config::KEY_PUBLIC_KEY);
        $this->key_secret = $this->config->getConfigData(Config::KEY_PRIVATE_KEY);
        $this->rzp = new Api($this->key_id, $this->key_secret);
    }
  
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {		
		$quote   = $this->getCustomerQuote();
		
		$sHelper  = $this->objectManagement->get('\OM\ExpressShipping\Helper\Data');
		$sInfo    = $sHelper->getExpressShippingPrice($quote);
		$totalShippingAmount = $sInfo['price'];
		
		$params  = $this->request->getParams();   		
		$amount  = $params['amount'];
		$receipt = 'MOBILE_'.$params['quote_id'];
		
		$subtotal = $quote->getSubtotal();
		$grntotal = $subtotal + $totalShippingAmount;		
		$amount  = (int) bcmul(round($grntotal, 2), 100.0);
				
		$order = $this->rzp->order->create(array(
		  'receipt' => $receipt,  
		  'amount' => $amount, 
		  'payment_capture' => 1,
		  'currency' => 'INR'
		  )
		);
		
		if(!empty($order))
		{	
						
			if (null !== $order && !empty($order->id)) {
				$razor_pay_id = $order->id;	
				// echo "<pre>";
				// print_r($quote->debug());
				// die();		
				$orderids = $this->cartManagement->placeOrder($quote->getId());
				$orderArr = explode(',',$orderids);        
				foreach($orderArr as $orderId){
					$ordernew = $this->order->load($orderId);
					$ordernew->setOCreatedBy('mobile');
					$ordernew->save();							
					$qid = $ordernew->getData('quote_id');
					$quote_om = $this->quoteRepository->get($qid);
					$sHelper  = $this->objectManagement->get('\OM\ExpressShipping\Helper\Data');
					
					$sInfo    = $sHelper->getExpressShippingPrice($ordernew,"mobile");
					$allowed       = $sInfo['allowed'];
					$shippingPrice = $sInfo['price'];
					if($totalShippingAmount==0){
						$shippingPrice = $totalShippingAmount;
					}
					$orderTotal = $ordernew->getSubtotal() + $shippingPrice;
					$ordernew->setBaseShippingAmount($shippingPrice);    
					$ordernew->setShippingAmount($shippingPrice);    
					$ordernew->setBaseGrandTotal($orderTotal);    
					$ordernew->setGrandTotal($orderTotal);
					$ordernew->setData('shipping_incl_tax',$shippingPrice);
					$ordernew->setData('base_shipping_incl_tax',$shippingPrice);
					$ordernew->setData('shipping_description',"Express Shipping");
					$ordernew->save();
					
					$parentQuote = $this->quoteRepository->get($params['quote_id']);
					$parentQuote->setIsActive(true)->save();
					
					$this->connection->query("UPDATE sales_order_grid set o_created_by='mobile',razorpay_order_id='".$razor_pay_id."', razorpay_total='".($amount/100)."' WHERE entity_id='".$orderId."' ");
				}						
				
			}			
			//echo  $order->id;
			echo  $order->id.'####'.$amount;	
		} 
		else
		{
			echo '0';
		}				
	}
	
	public function getCustomerQuote()
    {
		$params  = $this->request->getParams();   
		$quoteId = $params['quote_id'];
		$userId  = $params['user_id'];
        $quote = $this->quoteRepository->get($quoteId);					
		
        $firstName = $quote->getBillingAddress()->getFirstname() ?? 'null';
        $lastName = $quote->getBillingAddress()->getLastname() ?? 'null';
        
        $api_helpers = $this->objectManagement->create('Customm\Apii\Helper\Data'); 

        $store = $this->storeManagement->getStore();
        $websiteId = $store->getWebsiteId();
        $customer = $this->objectManagement->create('Magento\Customer\Model\Customer');       
        $customer->setWebsiteId($websiteId);
		$customer = $customer->load($userId);		  
        $customer = $this->customerRepository->getById($customer->getEntityId());
        $quote->assignCustomer($customer);
        $quote->setStore($store);       								
			
		$sHelper  = $this->objectManagement->get('\OM\ExpressShipping\Helper\Data');
		$sInfo    = $sHelper->getExpressShippingPrice($quote);
		
		$allowed       = $sInfo['allowed'];
		$shippingPrice = $sInfo['price'];
		
		$orderTotal = $quote->getSubtotal() + $shippingPrice;
		
		
		$shippingAddress = $quote->getShippingAddress();	
		
		$shippingRate = $this->objectManagement->create('\Magento\Quote\Model\Quote\Address\Rate');
		$shippingRate->setCode('express_shipping_express_shipping')->getPrice(1);
		$shippingAddress->setCollectShippingRates(false)
		    ->collectShippingRates()
    	    ->setShippingMethod('express_shipping_express_shipping');
		$quote->getShippingAddress()->addShippingRate($shippingRate);
				
		$quote->setPaymentMethod('razorpay');
		$quote->setInventoryProcessed(false);
		$quote->save(); 
		   
		$quote->getPayment()->importData(['method' => 'razorpay']);
		
		$quote->setBaseShippingAmount($shippingPrice);    
		$quote->setShippingAmount($shippingPrice); 
		$quote->setCustomerTaxClassId(null);   
		$quote->setBaseGrandTotal($orderTotal);    
		$quote->setGrandTotal($orderTotal);  
		$quote->save(); 
		$quote->collectTotals()->save();  
		
		$this->connection->query("update `quote_address` set shipping_description='Express Shipping',shipping_amount='".$shippingPrice."', base_shipping_amount='".$shippingPrice."',shipping_incl_tax='".$shippingPrice."',base_shipping_incl_tax='".$shippingPrice."',grand_total='".$orderTotal."',base_grand_total='".$orderTotal."' WHERE address_id='".$shippingAddress->getId()."'") ;
						
        return $quote;
    }
}
