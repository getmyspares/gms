<?php 
namespace Magenest\QuickBooksOnline\Controller\Connection;
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magenest\QuickBooksOnline\Helper\Oauth as OauthHelper;
use Magento\Framework\Exception\LocalizedException;
use Magenest\QuickBooksOnline\Model\OauthFactory as OauthModelFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Config\Model\Config as ConfigModel;
use Magenest\QuickBooksOnline\Model\Authenticate; 
use Magento\Framework\Validator\Exception;
use Razorpay\Api\Api;
use Razorpay\Magento\Model\Config;

$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$path=$api_helpers->get_path();			


require_once $path.'/app/code/Razorpay/razorpay/Razorpay.php';   
/** 
 * Class Index  
 * @package Magenest\Magenest\Controller\Connection
 */
class Razorpayment extends \Magento\Framework\App\Action\Action
{
  
    /**
     * Success constructor.
     * @param Context $context
     * @param Authenticate $authenticate
     */
	 protected $authenticate;

    public function __construct(
        Context $context,
        Authenticate $authenticate,
		\Magento\Framework\App\Request\Http $request
    ) {
        parent::__construct($context);
        $this->authenticate = $authenticate;
		 $this->request = $request;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
	
		// Create the Razorpay Order
		
		
		//return $order=$_GET['order'];  
		
		// $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/system.log');
		// $logger = new \Zend\Log\Logger();
		// $logger->addWriter($writer);
		// $logger->info('bbbbbbbbbb');

		
		
		 
		
		$params = $this->request->getParams();
		$order_id = $params['order'];  
		

		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$array=json_decode($api_helpers->get_razorpay_mode());
		
		
		$keyId=$array->keyId;
		$keySecret=$array->keySecret; 
		  
		//$keyId = 'rzp_live_qPKdEHLnGYR3Sb';  
		//$keySecret = 'BiIcBcGoOYs9K9UTZVnYP6fd';  
		$displayCurrency = 'INR';      
		      
		
		global $credit_card_per;
		global $debit_card_less;
		global $debit_card_greater;
		
		$credit_card_per = '1.95';
		$debit_card_less = '0.20';
		$debit_card_greater = '0.95';
		
		
		//$bankarray=array('HDFC','KKBK','YESB','ICIC');

		
		 
		
		$api = new Api($keyId, $keySecret);
		
		
			$razorpay_id=$order_id;		 
			
			$payments = $api->payment->fetch($razorpay_id);
			
			
			
			
			
			//$payments = $api->order->fetch($razorpay_id)->payments(); 
		 
			$text =  json_encode($payments->toArray());
			$obj = json_decode($text);
			
			/* echo "<pre>";
				print_r($obj);
			echo "</pre>";  */
			 
			
			$amount=$obj->amount/100; 
			//echo "<br>";
			$method=$obj->method; 
			//echo "<br>";
			$fee=$obj->fee/100; 
			//echo "<br>";
			$tax=$obj->tax/100; 
			$order_id=$obj->order_id; 
			
			
			
			
		
			
			/* $amount=$obj->amount/100; 
			//echo "<br>";
			$method=$obj->method; 
			//echo "<br>";
			$fee=$obj->fee/100; 
			//echo "<br>";
			$tax=$obj->tax/100;  */
			
			  
			
			$credit_card_per; 
			
			
			if($method=="card")
			{
				
				
				$payarray=array();
				
				$card_id=$obj->card_id;
				 
				$card_array=$this->get_card_details($card_id);
				
				/* echo "<pre>";
					print_r($card_array);
				echo "<pre>";   */

				$network=$card_array['network'];	
				$card_type=$card_array['type'];	
				$international=$card_array['international'];	
				$emi=$card_array['emi'];	
				
				$payarray=array(
					'payment'=>'card',
					'bank'=>'', 
					'card'=>$card_type,
					'network'=>$network,
					'international'=>$international,
					'emi'=>$emi,
					'amount'=>$amount,
					'fee'=>$fee, 
					'tax'=>$tax,
					'order_id'=>$order_id
				);	   

			}
			if($method=="emi")
			{
				
				
				$payarray=array();
				
				$card_id=$obj->card_id;
				 
				$card_array=$this->get_card_details($card_id);
				
				/* echo "<pre>";
					print_r($card_array);
				echo "<pre>";   */

				$network=$card_array['network'];	
				$card_type=$card_array['type'];	
				$international=$card_array['international'];	
				$emi=$card_array['emi'];	
				
				$payarray=array(
					'payment'=>'card',
					'bank'=>'', 
					'card'=>$card_type,
					'network'=>$network,
					'international'=>$international,
					'emi'=>$emi,
					'amount'=>$amount,
					'fee'=>$fee, 
					'tax'=>$tax,
					'order_id'=>$order_id
				);	   

			}
			else if($method=="wallet")
			{
				$payarray=array();
				$payarray=array(
					'payment'=>'wallet',
					'bank'=>'', 
					'card'=>'', 
					'network'=>'',
					'international'=>'',
					'emi'=>'',
					'amount'=>$amount,
					'fee'=>$fee,
					'tax'=>$tax,
					'order_id'=>$order_id 
				);	  
			} 
			else if($method=="netbanking")
			{ 
				
				$bank=$obj->bank; 
				
				$payarray=array();
				$payarray=array(
					'payment'=>'netbanking',
					'bank'=>$bank, 
					'card'=>'',  
					'network'=>'',
					'international'=>'',
					'emi'=>'',
					'amount'=>$amount,
					'fee'=>$fee,
					'tax'=>$tax,
					'order_id'=>$order_id
				);	
			} 
			else if($method=="upi")
			{
				$payarray=array();
				$payarray=array(
					'payment'=>'upi',
					'bank'=>'', 
					'card'=>'', 
					'network'=>'',
					'international'=>'',
					'emi'=>'',
					'amount'=>$amount,
					'fee'=>$fee,
					'tax'=>$tax,
					'order_id'=>$order_id
				);	
			}   
			
			
			if(!empty($payarray))
			{		
				echo json_encode($payarray);
			}
			
			
	

	}
	
	
	public function get_card_details($card_id)
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$array=json_decode($api_helpers->get_razorpay_mode());
		
		
		$keyId=$array->keyId;
		$keySecret=$array->keySecret; 
		  
		//$keyId = 'rzp_live_qPKdEHLnGYR3Sb';  
		//$keySecret = 'BiIcBcGoOYs9K9UTZVnYP6fd';  
		$displayCurrency = 'INR';      
		
		
		 
		
		$api = new Api($keyId, $keySecret);
		
		
		$payments = $api->card->fetch($card_id); 
		$text =  json_encode($payments->toArray());
		$obj = json_decode($text);
		
		
		$array=array('network'=>$obj->network,'type'=>$obj->type,'international'=>$obj->international,'emi'=>$obj->emi);
		
		return $array;
		
		
		
	}	


	public function razorpay_ipn()
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$array=json_decode($api_helpers->get_razorpay_mode());
		
		
		$keyId=$array->keyId;
		$keySecret=$array->keySecret; 
		
		
		$api = new Api($keyId, $keySecret);

		$json=file_get_contents('php://input');
		$webhookBody = json_decode($json);

		$webhookSecret='Webhook_secret';
		$webhookSignature = $api->header('X-Razorpay-Signature');
		$api->utility->verifyWebhookSignature($webhookBody, $webhookSignature, $webhookSecret);
		
		
		$api = new Api($keyId, $keySecret);
		$api->utility->verifyWebhookSignature($webhookBody, $webhookSignature, $webhookSecret);
		
		
		
		
	}	

		
	
	
}