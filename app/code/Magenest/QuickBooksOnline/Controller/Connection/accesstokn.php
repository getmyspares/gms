<?php 


namespace Magenest\QuickBooksOnline\Controller\Connection;

use \Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Validator\Exception;
use Magenest\QuickBooksOnline\Model\Authenticate;


/**
 * Class Index
 * @package Magenest\Magenest\Controller\Connection
 */
class Accesstokn extends \Magento\Framework\App\Action\Action
{
   protected $authenticate;

    /**
     * Success constructor.
     * @param Context $context
     * @param Authenticate $authenticate
     */
    public function __construct(
        Context $context,
        Authenticate $authenticate
    ) {
        parent::__construct($context);
        $this->authenticate = $authenticate;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */

          $websiteId = $this->getRequest()->getParam('website');
         $qboMode = $this->getRequest()->getParam('qbo_mode');
//        $url = $this->getUrl(
//            '*/*/success',
//            [
//                'website' => $websiteId,
//                'qbo_mode' => $qboMode
//            ]
//        );
      


        /** @var \Magento\Framework\Controller\Result\Redirect $redirectPage */
        $redirectPage = $this->resultFactory->create('redirect');
        $redirectPage->setPath('/');

        try {
            $link = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface')
                    ->getStore()->getBaseUrl() . 'qbonline/connection/success';
            $redirectUrl = $this->authenticate->redirectUrl($link);
            $this->_redirect($redirectUrl);
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $redirectPage;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $redirectPage;
        }
    }
}
