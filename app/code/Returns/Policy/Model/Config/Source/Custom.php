<?php

//Location: magento2_root/app/code/Returns/Policy/Model/Config/Source/Custom.php
namespace Returns\Policy\Model\Config\Source;

class Custom implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => 1, 'label' => __(' 1  Days')],
            ['value' => 2, 'label' => __(' 2  Days')],
            ['value' => 3, 'label' => __(' 3  Days')],
            ['value' => 4, 'label' => __(' 4  Days')],
            ['value' => 5, 'label' => __(' 5  Days')],
            ['value' => 6, 'label' => __(' 6  Days')],
            ['value' => 7, 'label' => __(' 7  Days')],
            ['value' => 8, 'label' => __(' 8  Days')],
            ['value' => 9, 'label' => __(' 9  Days')],
           ['value' => 10, 'label' => __('10  Days')],
           ['value' => 11, 'label' => __('11  Days')],
           ['value' => 12, 'label' => __('12  Days')],
           ['value' => 13, 'label' => __('13  Days')],
           ['value' => 14, 'label' => __('14  Days')],
           ['value' => 15, 'label' => __('15  Days')],
           ['value' => 16, 'label' => __('16  Days')],
           ['value' => 17, 'label' => __('17  Days')],
           ['value' => 18, 'label' => __('18  Days')],
           ['value' => 19, 'label' => __('19  Days')],
           ['value' => 20, 'label' => __('20  Days')],
           ['value' => 21, 'label' => __('21  Days')],
           ['value' => 22, 'label' => __('22  Days')],
           ['value' => 23, 'label' => __('23  Days')],
           ['value' => 24, 'label' => __('24  Days')],
           ['value' => 25, 'label' => __('25  Days')],
           ['value' => 26, 'label' => __('26  Days')],
           ['value' => 27, 'label' => __('27  Days')],
           ['value' => 28, 'label' => __('28  Days')],
           ['value' => 29, 'label' => __('29  Days')],
           ['value' => 30, 'label' => __('30  Days')],
           ['value' => 31, 'label' => __('31  Days')],
        ];
    }
}