<?php

namespace Razorpay\Magento\Controller\Payment;

use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Magento\Framework\Controller\ResultFactory;

class Vieworder extends \Razorpay\Magento\Controller\BaseController
{
    protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $orderRepository;

    protected $logger;
    
    protected $request;
    
    protected $quoteRepo;
    
	protected $storeManager;

	protected $orderModel;

    protected $customerRepository;
      
    protected $_objectManager;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Razorpay\Model\Config\Payment $razorpayConfig
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\Session $catalogSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
		\Magento\Store\Model\StoreManagerInterface $storeManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Magento\Quote\Model\QuoteRepository $quoteRepo,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );

        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
		$this->storeManagement    = $storeManagement;
        $this->customerRepository = $customerRepository;
        $this->checkoutFactory = $checkoutFactory;
        $this->cache = $cache;
        $this->_objectManager = $objectManager;
        $this->connection = $resourceConnection->getConnection();
        $this->orderRepository = $orderRepository;
        $this->quoteRepo    = $quoteRepo;
        $this->logger          = $logger;
        $this->orderModel = $orderModel;
        $this->request = $request;
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute()
    {
		$rzp_order_id = $this->request->getParam('rzp_order_id');
		if(!$rzp_order_id){
			exit('The Link You Followed Has Expired !!');
		}
		
		$orders = $this->connection->fetchAll("SELECT entity_id,razorpay_order_id,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$rzp_order_id."'  ORDER BY entity_id desc");    
		if($orders){     
			foreach($orders as $orow){				
				$order_id = $orow['entity_id'];
				$amnt     = $orow['razorpay_total']/100;
				$order    = $this->orderModel->create()->load($order_id);
			 }
			 
			if(!in_array($order->getStatus(),array('pending_payment','pending')) ){
				exit('Order is not in pending payment status. !!');
			}
		}else{
			exit('The Link You Followed Has Expired !!');
		}
		
		echo $this->createEmailContent($rzp_order_id);
		exit;
    }

    public function getOrderID()
    {
        return $this->catalogSession->getRazorpayOrderID();
    }

    protected function getMerchantPreferences()
    {
        try {
            $api = new Api($this->config->getKeyId(),"");
            $response = $api->request->request("GET", "preferences");
        } catch (\Razorpay\Api\Errors\Error $e) {
            echo 'Magento Error : ' . $e->getMessage();
        }

        $preferences = [];
        $preferences['embedded_url'] = Api::getFullUrl("checkout/embedded");
        $preferences['is_hosted'] = true;
        $preferences['image'] = $response['options']['image'];
        return $preferences;
    }

    public function getDiscount()
    {
        return ($this->getQuote()->getBaseSubtotal() - $this->getQuote()->getBaseSubtotalWithDiscount());
    }
    
    protected function createEmailContent($rzp_order_id){
		$storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url     = $storeManager->getStore()->getBaseUrl();			
	    $paymentLInk  = $site_url.'razorpay/payment/userorder?rzp_order_id='.$rzp_order_id;
		
		$message='<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td width="100%" valign="top" style="width:100.0%;padding:.75pt .75pt 20.25pt .75pt">
			<div align="center">
			<table  border="0" cellspacing="0" cellpadding="0" width="800" style="width:495.0pt;border-collapse:collapse">
			<tbody>
			<tr>
			<td colspan="4" valign="top" style="background:#3f549c;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="'.$site_url.'" target="_blank"><img border="0" width="180"  src="'.$site_url.'email_logo.jpg" alt="Main Website Store" ></a><u></u><u></u></span></p>
			</td>    
			</tr> 
			<tr><th>Name</th><th>Sku</th><th>Price</th><th>Qty</th></tr>';
		
		$orders = $this->connection->fetchAll("SELECT entity_id,razorpay_order_id,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$rzp_order_id."'  ORDER BY entity_id desc");  
		$subTotal       = 0;
		$shippingAmount = 0;
		$grandTotal     = 0;
		if($orders){     
			foreach($orders as $orow){				
				$order_id = $orow['entity_id'];
				$amnt     = $orow['razorpay_total']/100;
				$order    = $this->orderModel->create()->load($order_id);
				
				$items      = $order->getAllItems();	
				$saddr_info = $order->getShippingAddress();
				$saddr      = $saddr_info->getData();
				
				foreach($items as $item) {

					$message.= '<tr style="height:35px;"><td style="width:50%">'.$item->getName().'</td><td>'.$item->getSku().'</td><td style="text-align:center">'.round($item->getPrice(),2).'</td><td style="text-align:center">'.round($item->getQtyOrdered()).'</td></tr>';          

				}
				$subTotal       = $subTotal+$order->getSubtotal();
				$shippingAmount = $shippingAmount+$order->getShippingAmount();
				$grandTotal     = $grandTotal+$order->getGrandTotal();
			 }
		}
		
		$message.= '<tr style="border-bottom: solid #px;"><td colspan="4">&nbsp;</td></tr>';  	
		$message.= '<tr><td colspan="2"><h4>Sub Total</h4></td><td colspan="2"><h4>'.$subTotal.'</h4></td></tr>';  	
		$message.= '<tr><td colspan="2"><h4>Shipping & Handling</h4></td><td colspan="2"><h4>'.$shippingAmount.'</h4></td></tr>';
		$message.= '<tr><td colspan="2"><h4>Grand Total</h4></td><td colspan="2"><h4>'.$grandTotal.'</h4></td></tr>'; 
		
		$message.= '<tr><td colspan="2"><h4>Shipping Address</h4></td><td colspan="2">'.$saddr['street'].' '.$saddr['city'].' '.$saddr['region'].' '.$saddr['postcode'].'</td></tr>'; 
		$message.= '<tr><td colspan="4"><h3 style="margin-top:25px;"><center><a href="'.$paymentLInk.'">PROCEED TO PAY</a></center></h3></td></tr>';
		 
		$message.='</tbody>  
			</table>   
			'; 	
		
		return $message;
	}
    
}
