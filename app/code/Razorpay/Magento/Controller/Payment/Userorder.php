<?php

namespace Razorpay\Magento\Controller\Payment;

use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Magento\Framework\Controller\ResultFactory;

class Userorder extends \Razorpay\Magento\Controller\BaseController
{
    protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $orderRepository;

    protected $logger;
    
    protected $request;
    
    protected $quoteRepo;
    
    protected $orderModel;
    
    protected $_objectManager;
    
    protected $storeManager;

	protected $orderSender;
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Razorpay\Model\Config\Payment $razorpayConfig
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\Session $catalogSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
		\Magento\Store\Model\StoreManagerInterface $storeManagement,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Quote\Model\QuoteRepository $quoteRepo,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );

        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
        $this->checkoutFactory = $checkoutFactory;
        $this->cache = $cache;
        $this->_objectManager = $objectManager;
        $this->orderRepository = $orderRepository;
        $this->storeManagement    = $storeManagement;
        $this->quoteRepo    = $quoteRepo;
        $this->logger          = $logger;
        $this->request = $request;
        $this->connection = $resourceConnection->getConnection();
        $this->orderModel = $orderModel;
		$this->orderSender = $orderSender;
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute()
    {				
		if(empty($_POST['error']) === false){
				exit('Payment Failed. In case your money has already debited from your account, It will credit to your bank account within 5-7 business days.');
		}
			
        if(isset($_POST['razorpay_payment_id']))
        {

			$request = $this->getPostData();
            if((empty($request) === true) and (isset($_POST['razorpay_signature']) === true)) {
                $request['paymentMethod']['additional_data'] = [
                    'rzp_payment_id' => $_POST['razorpay_payment_id'],
                    'rzp_order_id' => $_POST['razorpay_order_id'],
                    'rzp_signature' => $_POST['razorpay_signature']
                ];
            }	
            
			$payment_id = $request['paymentMethod']['additional_data']['rzp_payment_id'];
			$this->validateSignature($request);
            
            $orders = $this->connection->fetchAll("SELECT entity_id,grand_total,razorpay_order_id,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$_POST['razorpay_order_id']."' ORDER BY entity_id desc");          
            foreach($orders as $orow){
                
                $order_id = $orow['entity_id'];
                $amnt     = $orow['razorpay_total'];
                $orderAmnt    = $orow['grand_total'];
                $order    = $this->orderModel->create()->load($order_id);
                
				if(!in_array($order->getStatus(),array('pending_payment','pending')) ){
					exit('Order is not in pending payment status. !!');
				}
                  
				$payment = $order->getPayment();
				$payment ->setStatus(1)
				    ->setTransactionId($payment_id)
				    ->setPreparedMessage(__('Razorpay transaction has been successful.'))
					->setAmountPaid($orderAmnt)
					->setLastTransId($payment_id)				
					->setIsTransactionClosed(true)
					->setShouldCloseParentTransaction(true)
					->registerCaptureNotification($orderAmnt,true );                    
				
				$invoice = $payment->getCreatedInvoice();
				
				$order->setState("processing");
				$order->setStatus("New");
				
				$this->checkoutSession->setForceOrderMailSentOnSuccess(true);
				$this->orderSender->send($order, true);
				
				$order->save();
								
				//update the Razorpay payment with corresponding created order ID of this quote ID
				$this->updatePaymentNote($payment_id, $order);
			}	
			echo "Order #".$order->getIncrementId().' Created Successfully !!';
        }
        else
        {
			$rzp_order_id = $this->request->getParam('rzp_order_id');
			if(!$rzp_order_id){
				exit('The Link You Followed Has Expired !!');
			}
			
			$orders = $this->connection->fetchAll("SELECT entity_id,razorpay_order_id,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$rzp_order_id."' ");    
			if($orders){     
				foreach($orders as $orow){				
					$order_id = $orow['entity_id'];
					$amnt     = $orow['razorpay_total'];
					$order    = $this->orderModel->create()->load($order_id);
				 }
			}else{
				exit('The Link You Followed Has Expired !!');
			}
		
			
			$amount = (int) bcmul(round($order->getGrandTotal(), 2), 100.0);

			$payment_action = $this->config->getPaymentAction();

			$maze_version = $this->_objectManager->get('Magento\Framework\App\ProductMetadataInterface')->getVersion();
			$module_version =  $this->_objectManager->get('Magento\Framework\Module\ModuleList')->getOne('Razorpay_Magento')['setup_version'];

			if ($payment_action === 'authorize'){
				$payment_capture = 0;
			} else {
				$payment_capture = 1;
			}

			$code = 400;


			if ($rzp_order_id)
			{
				$is_hosted = true;

				$merchantPreferences    = $this->getMerchantPreferences();
				$storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
				$baseUrl = $storeManager->getStore()->getBaseUrl();
						
				$firstName = $order->getCustomerFirstname();
				$lastName  = $order->getCustomerLastname();
				$telephone = $order->getShippingAddress()->getTelephone() ?? 'null';

				echo $form='
				<form id="custom_user_payment" method="POST" action="'.$merchantPreferences['embedded_url'].'">
				<input type="hidden" name="key_id" value="'.$this->key_id.'">
				<input type="hidden" name="name" value="Pay Online">
				<input type="hidden" name="amount" value="'.$amount.'">
				<input type="hidden" name="order_id" value="'.$rzp_order_id.'">
				<input type="hidden" name="notes[merchant_order_id]" value="">
				<input type="hidden" name="notes[merchant_quote_id]" value="'.$order->getQuoteId().'">
				<input type="hidden" name="prefill[name]" value="'.$firstName.' '.$lastName.'">
				<input type="hidden" name="prefill[contact]" value="'.$telephone.'">
				<input type="hidden" name="prefill[email]" value="'.$order->getCustomerEmail().'">
				<input type="hidden" name="callback_url" value="'.$baseUrl.'razorpay/payment/userorder">
				<input type="hidden" name="cancel_url" value="'.$baseUrl.'checkout/cart">
				<input type="hidden" name="_[integration]" value="magento">
				<input type="hidden" name="_[integration_version]" value="'.$module_version.'">
				<input type="hidden" name="_[integration_parent_version]" value="'.$maze_version.'">
				</form>
				<script>document.getElementById("custom_user_payment").submit();</script>';				
				exit;
			}
		}
    }

	protected function validateSignature($request)
    {
        $attributes = array(
            'razorpay_payment_id' => $request['paymentMethod']['additional_data']['rzp_payment_id'],
            'razorpay_order_id'   => $request['paymentMethod']['additional_data']['rzp_order_id'],
            'razorpay_signature'  => $request['paymentMethod']['additional_data']['rzp_signature'],
        );

        $this->rzp->utility->verifyPaymentSignature($attributes);
    }
    
	protected function updatePaymentNote($paymentId, $order)
    {
        //update the Razorpay payment with corresponding created order ID of this quote ID
        $this->rzp->payment->fetch($paymentId)->edit(
            array(
                'notes' => array(
                    'merchant_order_id' => $order->getIncrementId(),
                    'merchant_quote_id' => $order->getQuoteId()
                )
            )
        );
    }
    
    
    protected function getPostData()
    {
        $request = file_get_contents('php://input');

        return json_decode($request, true);
    }

    protected function getMerchantPreferences()
    {
        try {
            $api = new Api($this->config->getKeyId(),"");

            $response = $api->request->request("GET", "preferences");
        } catch (\Razorpay\Api\Errors\Error $e) {
            echo 'Magento Error : ' . $e->getMessage();
        }

        $preferences = [];

        $preferences['embedded_url'] = Api::getFullUrl("checkout/embedded");
        $preferences['is_hosted'] = true;
        $preferences['image'] = $response['options']['image'];
        
        return $preferences;
    }    
}
