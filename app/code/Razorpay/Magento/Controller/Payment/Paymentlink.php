<?php

namespace Razorpay\Magento\Controller\Payment;

use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Magento\Framework\Controller\ResultFactory;

class Paymentlink extends \Razorpay\Magento\Controller\BaseController
{
    protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $orderRepository;
    
    protected $order;
	
	protected $orderSender;

	const XML_PATH_PAYMENT_LINK = 'custom_template/email/payment_link';
    protected $_inlineTranslation;
    protected $_transportBuilder;
        
        

    protected $logger;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Razorpay\Model\Config\Payment $razorpayConfig
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\Session $catalogSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,    
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );

        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
        $this->checkoutFactory = $checkoutFactory;
        $this->cache = $cache;
        $this->orderRepository = $orderRepository;
        $this->order           = $order;
        $this->connection = $resourceConnection->getConnection();
        $this->logger          = $logger;
		$this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
            
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
    }
    
    public function execute()
    {
	    $this->createPendingOrder();
		exit;
    }
    
	public function sendPaymentEmail($customer_name,$email,$msg,$razorpayOrderId) 
    {
		$storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$panasonicHelper = $this->_objectManager->create('Panasonic\CustomUser\Helper\Data');
		$site_url=$storeManager->getStore()->getBaseUrl();			
		$customer_support_email = $panasonicHelper->getCustomerSupportEmail();
		$to = array($customer_support_email,$email);
		$scopeConfig = $this->_objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		$selectedTemplateId = $scopeConfig->getValue('custom_template/email/payment_link');
		$templateId = $selectedTemplateId;
      	$fromEmail = $this->getFromEmail(); 
        $fromName = 'GetMySpares'; 
        $toEmail = $email;
	    $paymentLInk  = $site_url.'razorpay/payment/vieworder?rzp_order_id='.$razorpayOrderId; 
 
        try {
            // template variables pass here
            $templateVars = [
                'siteUrl' =>$site_url,
                'customerName' => $customer_name,
                'paymentLink' => $paymentLInk,
            ];
 
            $storeId = $storeManager->getStore()->getId();
 
            $from = ['email' => $fromEmail, 'name' => $fromName];
            $this->_inlineTranslation->suspend();
 
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];
            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($toEmail)
                ->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
        	echo 'error'.$e->getMessage();
            //$this->logger->info($e->getMessage());
        }
	}
	
	public  function getFromEmail(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_custom2/email');
	}
		
	
    public function createPendingOrder()
    {
        $receipt_id = $this->getQuote()->getId();
                
		/* $amount = (int) (round($this->getQuote()->getGrandTotal(), 2) * 100); */
		$amount = (int) bcmul(round($this->getQuote()->getGrandTotal(), 2), 100.0);

		$payment_action = $this->config->getPaymentAction();

		$maze_version = $this->_objectManager->get('Magento\Framework\App\ProductMetadataInterface')->getVersion();
		$module_version =  $this->_objectManager->get('Magento\Framework\Module\ModuleList')->getOne('Razorpay_Magento')['setup_version'];

		$this->customerSession->setCustomerEmailAddress($_POST['email']);

		if ($payment_action === 'authorize') {
			$payment_capture = 0;
		} else {
			$payment_capture = 1;
		}

		try
		{
			$order = $this->rzp->order->create([
				'amount' => $amount,
				'receipt' => $receipt_id,
				'currency' => $this->getQuote()->getQuoteCurrencyCode(),
				'payment_capture' => $payment_capture,
				'app_offer' => ($this->getDiscount() > 0) ? 1 : 0
			]);

			$responseContent = [
				'message'   => 'Unable to create your order. Please contact support.',
				'parameters' => []
			];            

			if (null !== $order && !empty($order->id))
			{
				$razor_pay_id = $order->id;
				$this->getQuote()->getPayment()->setMethod(PaymentMethod::METHOD_CODE);
				if(!$this->customerSession->isLoggedIn()) {
					$this->getQuote()->setCheckoutMethod($this->cartManagement::METHOD_GUEST);
					$this->getQuote()->setCustomerEmail($this->customerSession->getCustomerEmailAddress());
				}
				$orderids = $this->cartManagement->placeOrder($this->getQuote()->getId());
				$orderArr = explode(',',$orderids);        
				foreach($orderArr as $orderId){
					$ordernew = $this->order->load($orderId);
					$ordernew->setOCreatedBy('Default');
					$ordernew->setState("new");
					$ordernew->setStatus("pending");
					$ordernew->setOCreatedBy('PaymentLink');  
					$ordernew->save();							
					$this->connection->query("UPDATE sales_order_grid set o_created_by='PaymentLink',razorpay_order_id='".$razor_pay_id."', razorpay_total='".($amount/100)."' WHERE entity_id='".$orderId."' ");
				}
				
				$email  = $_POST['email'];
				$mobile = $_POST['mobile'];	
				$name   = $_POST['name'];	
				$storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
				$baseUrl      = $storeManager->getStore()->getBaseUrl();		
				$paymentLInk  = $baseUrl.'razorpay/payment/vieworder?rzp_order_id='.$razor_pay_id; 
	    
				$smsHelper = $this->_objectManager->create('OM\MobileOtp\Helper\SmsHelper');
				$msg = "Dear $name, Your GetMySpares order cart is ready. Please review cart and make payment for your GetMySpares order at ".$paymentLInk;
				$smsHelper->send_custom_sms($mobile,$msg);
				
				$msg1 = "Click Here for Payment - <a href='".$paymentLInk."'>".$paymentLInk.'</a>';				
				$this->sendPaymentEmail($name,$email,$msg1,$razor_pay_id);
		
				return $razor_pay_id;					
			}

		} catch(\Razorpay\Api\Errors\Error $e) {
			$responseContent = [ 'message'   => $e->getMessage(), 'parameters' => [] ];
		} catch(\Exception $e) {
			$responseContent = [ 'message'   => $e->getMessage(), 'parameters' => [] ];
		}
    }


	protected function validateSignature($request)
    {
        $attributes = array(
            'razorpay_payment_id' => $request['paymentMethod']['additional_data']['rzp_payment_id'],
            'razorpay_order_id'   => $request['paymentMethod']['additional_data']['rzp_order_id'],
            'razorpay_signature'  => $request['paymentMethod']['additional_data']['rzp_signature'],
        );

        $this->rzp->utility->verifyPaymentSignature($attributes);
    }
    
	protected function updatePaymentNote($paymentId, $order)
    {
        //update the Razorpay payment with corresponding created order ID of this quote ID
        $this->rzp->payment->fetch($paymentId)->edit(
            array(
                'notes' => array(
                    'merchant_order_id' => $order->getIncrementId(),
                    'merchant_quote_id' => $order->getQuoteId()
                )
            )
        );
    }
    
    
    protected function getPostData()
    {
        $request = file_get_contents('php://input');

        return json_decode($request, true);
    }

    public function getOrderID()
    {
        return $this->catalogSession->getRazorpayOrderID();
    }

    protected function getMerchantPreferences()
    {
        try
        {
            $api = new Api($this->config->getKeyId(),"");
            $response = $api->request->request("GET", "preferences");
        }
        catch (\Razorpay\Api\Errors\Error $e)
        {
            echo 'Magento Error : ' . $e->getMessage();
        }

        $preferences = [];

        $preferences['embedded_url'] = Api::getFullUrl("checkout/embedded");
        $preferences['is_hosted'] = true;
        $preferences['image'] = $response['options']['image'];

        if(isset($response['options']['redirect']) && $response['options']['redirect'] === true)
        {
            $preferences['is_hosted'] = true;
        }

        return $preferences;
    }

    public function getDiscount()
    {
        return ($this->getQuote()->getBaseSubtotal() - $this->getQuote()->getBaseSubtotalWithDiscount());
    }
}
