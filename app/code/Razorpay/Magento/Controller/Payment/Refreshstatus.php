<?php

namespace Razorpay\Magento\Controller\Payment;

use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Magento\Framework\Controller\ResultFactory;
use Magento\CatalogInventory\Model\Indexer\Stock\Processor as StockProcessor;

class Refreshstatus extends \Razorpay\Magento\Controller\BaseController
{
    protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $request;

    protected $orderRepository;
    
    protected $order;
    
	protected $orderModel;
	
	protected $orderSender;
    
    protected $stockManagement;

    protected $stockIndexerProcessor;
        
    protected $logger;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Razorpay\Model\Config\Payment $razorpayConfig
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\Session $catalogSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Framework\App\Request\Http $request,
		\Magento\Sales\Model\Service\InvoiceService $invoiceService,
		\Magento\Sales\Model\OrderFactory $orderModel,
		\Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
		\Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
		\Magento\Framework\DB\TransactionFactory $transactionFactory,
		\Magento\CatalogInventory\Api\StockManagementInterface $stockManagement,
        StockProcessor $stockIndexerProcessor,

        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );
        $this->invoiceCollectionFactory = $invoiceCollectionFactory;
        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
        $this->checkoutFactory = $checkoutFactory;
        $this->cache = $cache;
        $this->transactionFactory = $transactionFactory;
        $this->orderRepository = $orderRepository;
        $this->order           = $order;
        $this->invoiceService = $invoiceService;
        $this->connection = $resourceConnection->getConnection();
        $this->logger          = $logger;
		$this->orderModel = $orderModel;
		$this->orderSender = $orderSender;
        $this->request = $request;    
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
    }
    
    public function execute()
    {
			$o_id = $this->request->getParam('razorpay_order_id');
			
			$pendingOrders = $this->connection->fetchAll("SELECT entity_id,grand_total,razorpay_order_id,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$o_id."'  ORDER BY entity_id desc") ;
			foreach($pendingOrders as $orow){				
				$order_id     = $orow['entity_id'];
				$razor_pay_id = $orow['razorpay_order_id'];
				$orderAmnt    = $orow['grand_total'];
				$amnt         = $orow['razorpay_total'];
				
				$order        = $this->orderModel->create()->load($order_id);
				
				if($this->request->getParam('schange')){
					$this->uncancleorder($order,'Un-Cancel Order By Admin');	
				}
				
				if(!in_array($order->getStatus(),array('pending_payment','pending')) ){
					exit('Order is not in pending payment status. !!');
				}
				
				$data = $this->rzp->order->fetch($razor_pay_id);
				$paymentInfo = $data->payments()->items;
				foreach($paymentInfo as $info){
					if($info->status=='captured'){
						$payment_id = $info->id;
					}
				}
				
				$rz_amount = $data->amount;
				$rz_status = $data->status;
				if($rz_status == 'paid'){
						$payment = $order->getPayment();
						$payment->setStatus(1)
							->setTransactionId($payment_id)
							->setPreparedMessage(__('Razorpay transaction has been successful .(Manaul Payment Refresh) .'))
							->setAmountPaid($orderAmnt)
							->setLastTransId($payment_id)				
							->setIsTransactionClosed(true)
							->setShouldCloseParentTransaction(true)
							->registerCaptureNotification($orderAmnt,true );                    
						
//						$invoice = $payment->getCreatedInvoice();


						$order->setState("processing");
						$order->setStatus("New");
						
						$this->checkoutSession->setForceOrderMailSentOnSuccess(true);
						$this->orderSender->send($order, true);
						
                        $order->save();

                        $this->createInvoice($order);

						//update the Razorpay payment with corresponding created order ID of this quote ID
						$this->updatePaymentNote($payment_id, $order);
						
						echo "Order #".$order->getIncrementId().' Updated Successfully !!';
				}
			}
			exit;
    }


    public function createInvoice($order)
    {
        try {
            $invoices = $this->invoiceCollectionFactory->create()
                ->addAttributeToFilter('order_id', array('eq' => $order->getId()));

            $invoices->getSelect()->limit(1);

            if ((int)$invoices->count() !== 0) {
                return null;
            }

            if(!$order->canInvoice()) {
                return null;
            }

            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $invoice->getOrder()->setCustomerNoteNotify(false);
            $invoice->getOrder()->setIsInProcess(true);
            $order->addStatusHistoryComment('Late Payment capture - Invoice Created', false);
            $transactionSave = $this->transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());
            $transactionSave->save();
        } catch (\Exception $e) {
            $order->addStatusHistoryComment('Exception message: '.$e->getMessage(), false);
            $order->save();
            return null;
        }

        return $invoice;
    }

	protected function validateSignature($request)
    {
        $attributes = array(
            'razorpay_payment_id' => $request['paymentMethod']['additional_data']['rzp_payment_id'],
            'razorpay_order_id'   => $request['paymentMethod']['additional_data']['rzp_order_id'],
            'razorpay_signature'  => $request['paymentMethod']['additional_data']['rzp_signature'],
        );

        $this->rzp->utility->verifyPaymentSignature($attributes);
    }
    
	protected function updatePaymentNote($paymentId, $order)
    {
        //update the Razorpay payment with corresponding created order ID of this quote ID
        $this->rzp->payment->fetch($paymentId)->edit(
            array(
                'notes' => array(
                    'merchant_order_id' => $order->getIncrementId(),
                    'merchant_quote_id' => $order->getQuoteId()
                )
            )
        );
    }
    
    
    protected function getPostData()
    {
        $request = file_get_contents('php://input');

        return json_decode($request, true);
    }

    public function getOrderID()
    {
        return $this->catalogSession->getRazorpayOrderID();
    }

    protected function getMerchantPreferences()
    {
        try
        {
            $api = new Api($this->config->getKeyId(),"");
            $response = $api->request->request("GET", "preferences");
        }
        catch (\Razorpay\Api\Errors\Error $e)
        {
            echo 'Magento Error : ' . $e->getMessage();
        }

        $preferences = [];

        $preferences['embedded_url'] = Api::getFullUrl("checkout/embedded");
        $preferences['is_hosted'] = true;
        $preferences['image'] = $response['options']['image'];

        if(isset($response['options']['redirect']) && $response['options']['redirect'] === true)
        {
            $preferences['is_hosted'] = true;
        }

        return $preferences;
    }

    public function getDiscount()
    {
        return ($this->getQuote()->getBaseSubtotal() - $this->getQuote()->getBaseSubtotalWithDiscount());
    }
    
	public function uncancleorder($order, $comment = "")
    {
        if(!($order)){
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid Order'));
        }

        if ($order->isCanceled()) {
            $state = 'pending_payment';
            $productStockQty = [];
            foreach ($order->getAllVisibleItems() as $item) {
                $productStockQty[$item->getProductId()] = $item->getQtyCanceled();
                foreach ($item->getChildrenItems() as $child) {
                    $productStockQty[$child->getProductId()] = $item->getQtyCanceled();
                    $child->setQtyCanceled(0);
                    $child->setTaxCanceled(0);
                    $child->setDiscountTaxCompensationCanceled(0);
                }
                $item->setQtyCanceled(0);
                $item->setTaxCanceled(0);
                $item->setDiscountTaxCompensationCanceled(0);
            }

            $order->setSubtotalCanceled(0);
            $order->setBaseSubtotalCanceled(0);
            $order->setTaxCanceled(0);
            $order->setBaseTaxCanceled(0);
            $order->setShippingCanceled(0);
            $order->setBaseShippingCanceled(0);
            $order->setDiscountCanceled(0);
            $order->setBaseDiscountCanceled(0);
            $order->setTotalCanceled(0);
            $order->setBaseTotalCanceled(0);
            
			$order->setBaseTotalDue(0);
			$order->setTotalDue(0);
			$order->setBaseTotalPaid($order->getBaseGrandTotal());
			$order->setTotalPaid($order->getGrandTotal());
			
		
            $order->setState($state)
                ->setStatus($order->getConfig()->getStateDefaultStatus($state));
            if (!empty($comment)) {
                $order->addStatusHistoryComment($comment, false);
            }
            
            //$order->setInventoryProcessed(true);

            $order->save();

        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('We cannot un-cancel this order.'));
        }
        return true;
    }
}
