<?php

namespace Razorpay\Magento\Controller\Payment;

use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Session\SessionManager;

class Order extends \Razorpay\Magento\Controller\BaseController
{
    protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $orderRepository;
    
    protected $order;
    
	protected $orderModel;
	
	protected $orderSender;
	
	protected $_coreSession;
	
	protected $quoteRepository;
               
    protected $logger;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Razorpay\Model\Config\Payment $razorpayConfig
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\Session $catalogSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\Data\OrderInterface $order,
		\Magento\Sales\Model\OrderFactory $orderModel,
		\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
		SessionManager $coreSession,
		\Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
            
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );

        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
        $this->checkoutFactory = $checkoutFactory;
        $this->cache = $cache;
        $this->orderRepository = $orderRepository;
        $this->order           = $order;
        $this->_coreSession    = $coreSession;
        $this->quoteRepository = $quoteRepository;   
        $this->connection = $resourceConnection->getConnection();
        $this->logger          = $logger;
		$this->orderModel = $orderModel;
		$this->orderSender = $orderSender;
            
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute()
    {
        $receipt_id = $this->getQuote()->getId();
        
        if(empty($_POST['error']) === false)
        {
            $this->checkoutSession->setPaymentFailData($_POST);
            $this->messageManager->addError(__('Payment Failed'));
            return $this->_redirect('checkout/cart');
        }

        if (isset($_POST['order_check']))
        {
            if (empty($this->cache->load("quote_processing_".$receipt_id)) === false)
            {
                $responseContent = [
                'success'   => true,
                'order_id'  => false,
                'parameters' => []
                ];

                # fetch the related sales order and verify the payment ID with rzp payment id
                # To avoid duplicate order entry for same quote
                $collection = $this->_objectManager->get('Magento\Sales\Model\Order')
                                                   ->getCollection()
                                                   ->addFieldToSelect('entity_id')
                                                   ->addFilter('quote_id', $receipt_id)
                                                   ->getFirstItem();

                $salesOrder = $collection->getData();

                if (empty($salesOrder['entity_id']) === false)
                {
                    $this->logger->warning("Razorpay inside order already processed with webhook quoteID:" . $receipt_id
                                    ." and OrderID:".$salesOrder['entity_id']);

                    $this->checkoutSession
                            ->setLastQuoteId($this->getQuote()->getId())
                            ->setLastSuccessQuoteId($this->getQuote()->getId())
                            ->clearHelperData();

                    $order = $this->orderRepository->get($salesOrder['entity_id']);

                    if ($order) {
                        $this->checkoutSession->setLastOrderId($order->getId())
                                           ->setLastRealOrderId($order->getIncrementId())
                                           ->setLastOrderStatus($order->getStatus());
                    }

                    $responseContent['order_id'] = true;
                }

            }
            else
            {
                //set the chache to stop webhook processing
                $this->cache->save("started", "quote_Front_processing_$receipt_id", ["razorpay"], 30);

                $this->logger->warning("Razorpay front-end order processing started quoteID:" . $receipt_id);

                $responseContent = [
                'success'   => false,
                'parameters' => []
                ];
            }

            $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $response->setData($responseContent);
            $response->setHttpResponseCode(200);
            return $response;
        }

        if(isset($_POST['razorpay_payment_id']))
        {
			$request = $this->getPostData();
            if((empty($request) === true) and (isset($_POST['razorpay_signature']) === true)) {
                $request['paymentMethod']['additional_data'] = [
                    'rzp_payment_id' => $_POST['razorpay_payment_id'],
                    'rzp_order_id' => $_POST['razorpay_order_id'],
                    'rzp_signature' => $_POST['razorpay_signature']
                ];
            }	
            
			$payment_id = $request['paymentMethod']['additional_data']['rzp_payment_id'];
			$this->validateSignature($request);
            
            $orders = $this->connection->fetchAll("SELECT entity_id,customer_id,razorpay_order_id,grand_total,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$_POST['razorpay_order_id']."'  ORDER BY entity_id desc");          
            foreach($orders as $orow){            
                $order_id = $orow['entity_id'];
                $amnt     = $orow['razorpay_total'];
                $orderAmnt   = $orow['grand_total'];
				$customer_id = $orow['customer_id'];
                $order    = $this->orderModel->create()->load($order_id);
                
                if( $order->getState()=='pending_payment' ){
					$quote_id = $this->connection->fetchOne("SELECT quote_id FROM `sales_order` WHERE entity_id='".$order_id."' ");
					
					$this->checkoutSession->setLastQuoteId($quote_id)
								->setLastSuccessQuoteId($quote_id);
				
					$this->checkoutSession->setLastOrderId($order_id)             
								->setLastRealOrderId($order->getIncrementId())
								->setLastOrderStatus($order->getStatus()); 
								
					$payment = $order->getPayment();
					$payment ->setStatus(1)
						->setTransactionId($payment_id)
						->setPreparedMessage(__('Razorpay transaction has been successful From Web.'))
						->setAmountPaid($orderAmnt)
						->setLastTransId($payment_id)				
						->setIsTransactionClosed(true)
						->setShouldCloseParentTransaction(true)
						->registerCaptureNotification($orderAmnt,true );                    
					
					$invoice = $payment->getCreatedInvoice();
					
					$order->setState("processing");
					$order->setStatus("New");
					
					$this->checkoutSession->setForceOrderMailSentOnSuccess(true);
					$this->orderSender->send($order, true);
					
					$order->save();
									
					//update the Razorpay payment with corresponding created order ID of this quote ID
					$this->updatePaymentNote($payment_id, $order);
				}     
			}
			
			$this->connection->query("update quote set is_active=0 WHERE customer_id='".$customer_id."' ");
						                                
            return $this->_redirect('checkout/onepage/success');				
												
        }
        else
        {
            if(empty($_POST['email']) === true)
            {
                $this->logger->warning("Email field is required");

                $responseContent = [
                    'message'   => "Email field is required",
                    'parameters' => []
                ];

                $code = 200;
            }
            else
            {

                // typecasting to  int  lead to 0.01  paise variation as it always  rounds up to 1 digit down  using bcmul instead as 1 paise was less  deducted at  razorpay due  to  this @ritesh1/02/2021
                
                /* $amount = (int) (round($this->getQuote()->getGrandTotal(), 2) * 100); */
                $amount = (int) bcmul(round($this->getQuote()->getGrandTotal(), 2), 100.0);

                $payment_action = $this->config->getPaymentAction();

                $maze_version = $this->_objectManager->get('Magento\Framework\App\ProductMetadataInterface')->getVersion();
                $module_version =  $this->_objectManager->get('Magento\Framework\Module\ModuleList')->getOne('Razorpay_Magento')['setup_version'];

                $this->customerSession->setCustomerEmailAddress($_POST['email']);

                if ($payment_action === 'authorize')
                {
                    $payment_capture = 0;
                }
                else
                {
                    $payment_capture = 1;
                }

                $code = 400;

                try
                {
					$this->_coreSession->setData('parent_quote_id', $receipt_id);
                    $order = $this->rzp->order->create([
                        'amount' => $amount,
                        'receipt' => $receipt_id,
                        'currency' => $this->getQuote()->getQuoteCurrencyCode(),
                        'payment_capture' => $payment_capture,
                        'app_offer' => ($this->getDiscount() > 0) ? 1 : 0
                    ]);

                    $responseContent = [
                        'message'   => 'Unable to create your order. Please contact support.',
                        'parameters' => []
                    ];            

                    if (null !== $order && !empty($order->id))
                    {
                        $razor_pay_id = $order->id;
                        
						$this->getQuote()->getPayment()->setMethod(PaymentMethod::METHOD_CODE);
						
						if(!$this->customerSession->isLoggedIn()) {
							$this->getQuote()->setCheckoutMethod($this->cartManagement::METHOD_GUEST);
							$this->getQuote()->setCustomerEmail($this->customerSession->getCustomerEmailAddress());
						}
						$orderids = $this->cartManagement->placeOrder($this->getQuote()->getId());
						$orderArr = explode(',',$orderids);        
						foreach($orderArr as $orderId){
							$ordernew = $this->order->load($orderId);
							$ordernew->setOCreatedBy('Default');  
							$ordernew->save();							
							$this->connection->query("UPDATE sales_order_grid set o_created_by='Default',razorpay_order_id='".$razor_pay_id."', razorpay_total='".($amount/100)."' WHERE entity_id='".$orderId."' ");
						}						
						
                        $is_hosted = true;
                        $merchantPreferences    = $this->getMerchantPreferences();
                        $responseContent = [
                            'success'           => true,
                            'rzp_order'         => $order->id,
                            'order_id'          => $receipt_id,
                            'amount'            => $order->amount,
                            'quote_currency'    => $this->getQuote()->getQuoteCurrencyCode(),
                            'quote_amount'      => round($this->getQuote()->getGrandTotal(), 2),
                            'maze_version'      => $maze_version,
                            'module_version'    => $module_version,
                            'is_hosted'         => $merchantPreferences['is_hosted'],
                            'image'             => $merchantPreferences['image'],
                            'embedded_url'      => $merchantPreferences['embedded_url'],
                        ];
                        $code = 200;
                        $this->catalogSession->setRazorpayOrderID($order->id);
                        
                        $p_q_id = $this->_coreSession->getData('parent_quote_id');
                        $parentQuote = $this->quoteRepository->get($p_q_id);
                        $parentQuote->setIsActive(true)->save();
                        
                    }

                }
                catch(\Razorpay\Api\Errors\Error $e)
                {
                    $responseContent = [
                        'message'   => $e->getMessage(),
                        'parameters' => []
                    ];
                }
                catch(\Exception $e)
                {
                    $responseContent = [
                        'message'   => $e->getMessage(),
                        'parameters' => []
                    ];
                }
            }

            //set the chache for race with webhook
            $this->cache->save("started", "quote_Front_processing_$receipt_id", ["razorpay"], 30);

            $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $response->setData($responseContent);
            $response->setHttpResponseCode($code);

            return $response;
        }
    }


	protected function validateSignature($request)
    {
        $attributes = array(
            'razorpay_payment_id' => $request['paymentMethod']['additional_data']['rzp_payment_id'],
            'razorpay_order_id'   => $request['paymentMethod']['additional_data']['rzp_order_id'],
            'razorpay_signature'  => $request['paymentMethod']['additional_data']['rzp_signature'],
        );

        $this->rzp->utility->verifyPaymentSignature($attributes);
    }
    
	protected function updatePaymentNote($paymentId, $order)
    {
        //update the Razorpay payment with corresponding created order ID of this quote ID
        $this->rzp->payment->fetch($paymentId)->edit(
            array(
                'notes' => array(
                    'merchant_order_id' => $order->getIncrementId(),
                    'merchant_quote_id' => $order->getQuoteId()
                )
            )
        );
    }
    
    
    protected function getPostData()
    {
        $request = file_get_contents('php://input');

        return json_decode($request, true);
    }

    public function getOrderID()
    {
        return $this->catalogSession->getRazorpayOrderID();
    }

    protected function getMerchantPreferences()
    {
        try
        {
            $api = new Api($this->config->getKeyId(),"");

            $response = $api->request->request("GET", "preferences");
        }
        catch (\Razorpay\Api\Errors\Error $e)
        {
            echo 'Magento Error : ' . $e->getMessage();
        }

        $preferences = [];

        $preferences['embedded_url'] = Api::getFullUrl("checkout/embedded");
        $preferences['is_hosted'] = true;
        $preferences['image'] = $response['options']['image'];

        if(isset($response['options']['redirect']) && $response['options']['redirect'] === true)
        {
            $preferences['is_hosted'] = true;
        }

        return $preferences;
    }

    public function getDiscount()
    {
        return ($this->getQuote()->getBaseSubtotal() - $this->getQuote()->getBaseSubtotalWithDiscount());
    }
}
