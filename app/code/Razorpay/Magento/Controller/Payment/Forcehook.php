<?php 

namespace Razorpay\Magento\Controller\Payment;

use Razorpay\Api\Api;
use Razorpay\Api\Errors;
use Razorpay\Magento\Model\Config;
use Razorpay\Magento\Model\PaymentMethod;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;

class Forcehook extends \Razorpay\Magento\Controller\BaseController
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $order;

    protected $api;

    protected $logger;

    protected $quoteManagement;

    protected $objectManagement;

    protected $storeManager;

    protected $customerRepository;

    protected $cache;

    const STATUS_APPROVED = 'APPROVED';

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Razorpay\Model\CheckoutFactory $checkoutFactory
     * @param \Razorpay\Magento\Model\Config $config
     * @param \Magento\Catalog\Model\Session $catalogSession
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository,
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagement
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Store\Model\StoreManagerInterface $storeManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\App\CacheInterface $cache,
        \Psr\Log\LoggerInterface $logger
    ) 
    {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $config
        );

        $keyId                 = $this->config->getConfigData(Config::KEY_PUBLIC_KEY);
        $keySecret             = $this->config->getConfigData(Config::KEY_PRIVATE_KEY);

        $this->api             = new Api($keyId, $keySecret);
        $this->order           = $order;
        $this->logger          = $logger;
        
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
        $this->quoteManagement    = $quoteManagement;
        $this->checkoutFactory    = $checkoutFactory;
        $this->catalogSession     = $catalogSession;
        $this->quoteRepository    = $quoteRepository;
        $this->storeManagement    = $storeManagement;
        $this->customerRepository = $customerRepository;
        $this->cache = $cache;
    }

    /**
     * Processes the incoming webhook
     */
    public function execute()
    {       
		exit('aa'); exit;
		//return $this->orderPaid();  
		$paymentId = 'pay_FVSVLKPCO2yCp3';
		$amount = 356.43;
        $quoteId   = '7871';
		$order = $this->order->load(219);	
        echo $orderRzpPaymentId = $order->getPayment()->getLastTransId();
		$eventmng = $this->objectManagement->create('Magento\Framework\Event\Manager');
        $quote = $this->getQuoteObject($quoteId);
		
        $eventmng->dispatch( 'sales_order_place_before',['order' => $order, 'quote' => $quote ] );
		$eventmng->dispatch( 'checkout_submit_all_after',['order' => $order, 'quote' => $quote ] );       
		
        $payment = $order->getPayment();        

        $payment->setAmountPaid($amount)
                ->setLastTransId($paymentId)
                ->setTransactionId($paymentId)
                ->setIsTransactionClosed(true)
                ->setShouldCloseParentTransaction(true);
        $order->save();
	    exit;
    }

    /**
     * Order Paid webhook
     * 
     * @param array $post
     */
    protected function orderPaid()
    {
		$paymentId = 'pay_FVSVLKPCO2yCp3';
        $quoteId   = '7871';

        //$this->cache->save("started", "quote_processing_$quoteId", ["razorpay"], 30);

        echo $amount    = number_format(35643/100, 2, ".", "");
		echo "<br />";
        $this->logger->warning("Razorpay Webhook processing started for Razorpay payment_id(:$paymentId)");

        $payment_created_time = '26 Aug 2020, 10:03:47 pm';

        //validate if the quote Order is still active
        $quote = $this->quoteRepository->get($quoteId);

        //exit if quote is not active
        if (!$quote->getIsActive())
        {
            $this->logger->warning("Razorpay Webhook: Quote order is inactive for quoteID: $quoteId and Razorpay payment_id(:$paymentId)");
                return;
        }
	
        //validate amount before placing order
        echo $quoteAmount = (int) (round($quote->getGrandTotal(), 2) * 100);
		echo "<br />";
        if ($quoteAmount !== 35643)
        {
            $this->logger->warning("Razorpay Webhook: Amount paid doesn't match with store order amount for Razorpay payment_id(:$paymentId)");
                return;
        }

        # fetch the related sales order and verify the payment ID with rzp payment id
        # To avoid duplicate order entry for same quote 
        $collection = $this->_objectManager->get('Magento\Sales\Model\Order')
                                           ->getCollection()
                                           ->addFieldToSelect('entity_id')
                                           ->addFilter('quote_id', $quoteId)
                                           ->getFirstItem();
        
		echo "sales";
        $salesOrder = $collection->getData();
        
        if (empty($salesOrder['entity_id']) === false)
        {
            $order = $this->order->load($salesOrder['entity_id']);
            $orderRzpPaymentId = $order->getPayment()->getLastTransId();

            if ($orderRzpPaymentId === $paymentId)
            {
                $this->logger->warning("Razorpay Webhook: Sales Order and payment already exist for Razorpay payment_id(:$paymentId)");
                return;
            }
        }

		$eventmng = $this->objectManagement->create('Magento\Framework\Event\Manager');

        $quote = $this->getQuoteObject($quoteId);

        $order = $this->quoteManagement->submit($quote);
        
        
        
        $eventmng->dispatch( 'sales_order_place_before',['order' => $order, 'quote' => $quote ] );
        //$eventmng->dispatch( 'sales_model_service_quote_submit_before',['order' => $order, 'quote' => $quote ] );
		$eventmng->dispatch( 'checkout_submit_all_after',['order' => $order, 'quote' => $quote ] );       
		
        $payment = $order->getPayment();        

        $payment->setAmountPaid($amount)
                ->setLastTransId($paymentId)
                ->setTransactionId($paymentId)
                ->setIsTransactionClosed(true)
                ->setShouldCloseParentTransaction(true);
        $order->save();
		
        $this->logger->warning("Razorpay Webhook Processed successfully for Razorpay payment_id(:$paymentId): and quoteID(: $quoteId) and OrderID(: ". $order->getEntityId() .")");
    }

    protected function getQuoteObject($quoteId)
    {
        $email = 'sirajjain33@gmail.com';

        $quote = $this->quoteRepository->get($quoteId);


        $firstName = $quote->getBillingAddress()->getFirstname() ?? 'null';
        $lastName = $quote->getBillingAddress()->getLastname() ?? 'null';


        $quote->getPayment()->setMethod(PaymentMethod::METHOD_CODE);

        $store = $this->storeManagement->getStore();

        $websiteId = $store->getWebsiteId();

        $customer = $this->objectManagement->create('Magento\Customer\Model\Customer');
        
        $customer->setWebsiteId($websiteId);

        //get customer from quote , otherwise from payment email
        if (empty($quote->getBillingAddress()->getEmail()) === false)
        {
            $customer = $customer->loadByEmail($quote->getBillingAddress()->getEmail());
        }
        else
        {
            $customer = $customer->loadByEmail($email);
        }
        
        //if quote billing address doesn't contains address, set it as customer default billing address
        if ((empty($quote->getBillingAddress()->getFirstname()) === true) and (empty($customer->getEntityId()) === false))
        {   
            $quote->getBillingAddress()->setCustomerAddressId($customer->getDefaultBillingAddress()['id']);
        }

        //If need to insert new customer 
        if (empty($customer->getEntityId()) === true)
        {
            $quote->setCustomerFirstname($firstName);
            $quote->setCustomerLastname($lastName);
            $quote->setCustomerEmail($email);
            $quote->setCustomerIsGuest(true);
        }
        else
        {
            $customer = $this->customerRepository->getById($customer->getEntityId());

            $quote->assignCustomer($customer);
        }

        $quote->setStore($store);

        $quote->save();

        return $quote;
    }

    /**
     * @return Webhook post data as an array
     */
    protected function getPostData() : array
    {
        $request = file_get_contents('php://input');

        return json_decode($request, true);
    }
}
