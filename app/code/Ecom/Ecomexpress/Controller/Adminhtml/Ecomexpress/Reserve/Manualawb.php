<?php

namespace Ecom\Ecomexpress\Controller\Adminhtml\Ecomexpress\Reserve;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;

class Manualawb extends \Magento\Backend\App\Action
{
	
	public function __construct(
        Context $context,
        
        ForwardFactory $resultForwardFactory
    ) {
        parent::__construct($context);
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
    }
   
	public function execute()
	{	
		$objectManager = $this->_objectManager;
        $resultRedirect = $this->resultRedirectFactory->create();

		if($awbIds = $this->getRequest()->getPostValue('selected'))
		{	
			$awb_helper = $this->_objectManager->create('OM\Rewrite\Helper\Fetchppdawb');
            $checkdata = $awb_helper->checkIfAwbUsed($awbIds);
           
			$ifalreadyused = $checkdata[0];
			$status_msg = $checkdata[1];
            if($ifalreadyused)
            {
                $this->messageManager->addErrorMessage($status_msg);
                return $resultRedirect->setPath('ecomexpress/ecomexpress/awb');
                die;
            }
            
            $requested_count = count($awbIds);
            $freeAwbCount = $awb_helper->getFreeAwbCount();
            $awb_left = $freeAwbCount-$requested_count;
            $minimim_awb = 10;
            if($awb_left<=$minimim_awb)
            {
                $status_msg="If we allow you to fetch $requested_count awbs , there will be less than $minimim_awb awb left in the frontend.Please fetch more awb first to complete this task (by clicking IMPORT PPD AWB) .";
                $this->messageManager->addErrorMessage($status_msg);
                return $resultRedirect->setPath('ecomexpress/ecomexpress/awb');
                die;   
            }
            

            $result = $awb_helper->manuallyAssing($awbIds);
            $this->messageManager->addSuccessMessage(__('Selected Awbs reserved for manual use'));
			return $this->_redirect('ecomexpress/ecomexpress/awb');
		} else 
        {
            $this->messageManager->addErrorMessage("Invalid data");
            return $this->_redirect('ecomexpress/ecomexpress/awb');
            die;
        }
	}

}