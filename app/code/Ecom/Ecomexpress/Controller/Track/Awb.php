<?php
/**
 * Copyright Â©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Ecom\Ecomexpress\Controller\Track;

class Awb extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Ecom\Ecomexpress\Helper\Data $ecomHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->ecomHelper = $ecomHelper;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $awb = $this->getRequest()->getParam('awb');
        /* typecasting to int to prevent xxs attacks */
        $awb=(int)$awb;

        if(!empty($awb))
        {
            $awb_track_array=[];

            $get_tracking_date = $this->ecomHelper->awbTracking($awb);
           
            if($get_tracking_date['status']==1)
            {
                $tracking_date = $get_tracking_date['data'];
                foreach ($tracking_date as  $value) {
                    $date = $value['date'];
                    $status = $value['status'];
                    $location = $value['location'];
                    $awb_track_array[] = array("date"=>$date,"status"=>$status,"location"=>$location);
                    ?> 
                    <div class="main-tracking-container">
                        <div class="date"><h4><?=$date?></h4></div>
                        <div class="status"><?=$status?></div> 
                        <div class="location">package location : <?=$location?></div>
                    </div>
                    <?php
                }
            }            
        }
    }
}
