<?php

namespace Ecom\Ecomexpress\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {
	
	public function execute_curl($url,$type,$params) {
		try {		
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_FAILONERROR,1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 120);			
			if ($type == 'post'){				
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			}			
			$retValue = curl_exec($ch);

			if ($errno = curl_errno($ch)) {
				$error_message = curl_strerror($errno);
				echo "cURL error ({$errno}):\n {$error_message}";
			}
			if (!$retValue)
			{
				return false;
			}			
			curl_close($ch);
			return $retValue;	
		}
		catch(Exception $e)
		{
			return	$e->getMessage();
		}
	}

	public function awbTracking(int $awb)
	{
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
			$modearray = json_decode($api_helpers->get_razorpay_mode());
			$mode = $modearray->mode;
			$username = $modearray->username;
			// $username = "panasonicindia90573_temp";
			$password = $modearray->password;
			// $password = "G8SMV7suncywG53w";


			$curl = curl_init();
			curl_setopt_array($curl, array(
					CURLOPT_URL => "https://plapi.ecomexpress.in/track_me/api/mawbd/?awb=$awb&username=$username&password=$password",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$xml=@simplexml_load_string($response); 
			if ($xml === false) {
					return  array("status"=>0,"data"=>array());
					
			} else {
					$status_object = @$xml->object->field[36];
					if(empty($status_object))
					{
					return  array("status"=>0,"data"=>array());
					}
					$count= $status_object->count();
					$awb_track_array = array();
					/* using for loop for a change  , reminds me of C language again @ritesh*/
					for(--$count;$count>=0;$count--)
					{ 
					$field = json_decode(json_encode($status_object->object[$count]),true)['field']; 
					$status = is_string($field[1])?$field[1]:"";
					$awb_track_array[]= ['date'=>$field[0],'status'=>$status,'location'=>$field[8]];
					}
					return  array("status"=>1,"data"=>$awb_track_array);
			}
	}
	
}