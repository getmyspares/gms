<?php

namespace Ecom\Ecomexpress\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;

class Ecomexpress extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements 

\Magento\Shipping\Model\Carrier\CarrierInterface 

{
	

	protected $_code = 'ecomexpress';
	protected $price = '0';
	protected $_logger;

	protected $_isFixed = true;
	protected $_rateResultFactory;
	protected $_rateMethodFactory;
	protected  $_rateResultErrorFactory;
	

	public function __construct(

	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 

	\Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory, 

	\Psr\Log\LoggerInterface $logger, 

	\Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory, 

	\Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory, 

	array $data = []) 

	{
		$this->_rateResultFactory = $rateResultFactory;	
		$this->_rateMethodFactory = $rateMethodFactory;
		$this->_rateResultErrorFactory= $rateErrorFactory;
		$this->_logger = $logger;
		$this->scopeconfig=$scopeConfig;
		parent::__construct ( $scopeConfig, $rateErrorFactory, $logger, $data );
	}

	public function collectRates(RateRequest $request) 
	{
		if($this->scopeconfig->getValue('carriers/ecomexpress/active')!="0"){
			$price = 0;
			$objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
			if (! $this->getConfigFlag ( 'active' )) {
					
				return false;
			}
			$result = $this->_rateResultFactory->create ();
			$quote = $objectmanager->create('Magento\Checkout\Model\Cart')->getQuote();
			$shippingAddress = $quote->getShippingAddress();
			$to_country = $shippingAddress->getCountryId();
			$to_city = $shippingAddress->getCity();
			$to_zipcode = $shippingAddress->getPostcode();
			$allowed = false;
			$coupon =$objectmanager->create('Magento\SalesRule\Model\Coupon');
 			$saleRule = $objectmanager->create('Magento\SalesRule\Model\Rule');
 			$couponCode = $quote->getCouponCode();
        	$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
        	$rule = $saleRule->load($ruleId);
        	$freeShippingCoupon = $rule->getSimpleFreeShipping();
		  
			if(!$this->scopeconfig->getValue('carriers/ecomexpress/sallowspecific')){
				$allowed = true;
			}else{
				$allowed_country = explode(',',$this->scopeconfig->getValue('carriers/ecomexpress/specificcountry'));
				$allowed = in_array($to_country,$allowed_country)==true ? true : false;
			}
			$model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->load($to_zipcode,'pincode');
			//echo $to_zipcode;
			//echo $to_country.'<br>'; 
			//var_dump($allowed);die('---');
			if($allowed && (!empty($model))){ 
				if($this->scopeconfig->getValue('carriers/ecomexpress/handling')){
					$price = $this->scopeconfig->getValue("carriers/ecomexpress/handling_charge");
				}
				$method = $this->_rateMethodFactory->create();
				$method->setCarrier($this->_code);
				$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
				$method->setMethod($this->scopeconfig->getValue('carriers/ecomexpress/name'));
				$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
							
                $helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
				$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
				$newPrice =$custom_helpers->ecom_price($to_zipcode);
				if($freeShippingCoupon){
					$newPrice = 0;
				}
				
				
				
                //$newPrice =  $helpers->ecom_price($to_zipcode);   
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();

				$customerSession = $objectManager->create('Magento\Customer\Model\Session');
				$customerSession->setcheckoutshippingSession($newPrice);

				$method->setPrice($newPrice);
				//$method->setPrice($price);
				$method->setCost($price);
				$result->append($method);
			}
			/*else if(in_array($to_country,$allowed_country)== true && sizeof($model)>0 )
			{ die('in elseif');
				if($this->scopeconfig->getValue('carriers/ecomexpress/handling') == true){
					$price = $this->scopeconfig->getValue("carriers/ecomexpress/handling_charge");
				}
				$method = $this->_rateMethodFactory->create();
				$method->setCarrier($this->_code);
				$method->setCarrierTitle($this->_code);
				$method->setMethod($this->_code);
				$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
				$method->setPrice($price);
				$method->setCost($price);
				$result->append($method);
			}*/
			else{
				if($this->scopeconfig->getValue('carriers/ecomexpress/showmethods')!="0"){
					$error =$this->_rateResultErrorFactory->create();
					$error->setCarrier($this->_code);
					$error->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
					$method->setMethod($this->scopeconfig->getValue('carriers/ecomexpress/name'));
					$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
					$error->setErrorMessage($this->scopeconfig->getValue('carriers/ecomexpress/specificerrmsg'));
					$result->append($error);
				}		
			}		
			return $result;
		}
		
	}
	

	public function getAllowedMethods() 

	{
		
		return [ 
				$this->_code => $this->getConfigData ( 'title' ) 
		];
	}
	public function isTrackingAvailable(){
	
		return true;
	}

}