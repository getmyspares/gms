<?php
namespace Ecom\Ecomexpress\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Awbtrack implements ArgumentInterface
{
    public function __construct() {

    }

    public function awbTracking(int $awb)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
        $modearray = json_decode($api_helpers->get_razorpay_mode());
        $mode = $modearray->mode;
        $username = $modearray->username;
        $username = "panasonicpvtltd402639_pro";
        $password = $modearray->password;
        $password = "6bk2LrZpC7UXhuZe";



        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://plapi.ecomexpress.in/track_me/api/mawbd/?awb=$awb&username=$username&password=$password",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $xml=@simplexml_load_string($response); 
        if ($xml === false) {
            return  array("status"=>0,"data"=>array());
            
        } else {
            $status_object = @$xml->object->field[36];
            if(empty($status_object))
            {
            return  array("status"=>0,"data"=>array());
            }
            $count= $status_object->count();
            $awb_track_array = array();
            /* using for loop for a change  , reminds me of C language again @ritesh*/
            for(--$count;$count>=0;$count--)
            { 
            $field = json_decode(json_encode($status_object->object[$count]),true)['field']; 
            $status = is_string($field[1])?$field[1]:"";
            $awb_track_array[]= ['date'=>$field[0],'status'=>$status,'location'=>$field[8]];
            }
            return  array("status"=>1,"data"=>$awb_track_array);
        }
    }
}