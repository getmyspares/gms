<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Binaryzero\Changecustomerpassword\Controller\Adminhtml\Password;

class Submit extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\DeploymentConfig $deploymentConfig,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_deploymentConfig = $deploymentConfig;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        $resultRedirect = $this->resultRedirectFactory->create();
        try{
            $request = $this->getRequest()->getParams();           
            $email = $request['email'];
            $password = $request['new_password'];
            if(empty($email))
            {
                $this->messageManager->addError( __('Email was not provided'));
                return $resultRedirect->setPath('change/password/index');
            }
            if(empty($password))
            {
                $this->messageManager->addError(__('Password was not provided'));
                return $resultRedirect->setPath('change/password/index');
            }
            /* time constrain could not  spend time  to  create models @ritesh*/
            $_resources = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $_resources->getConnection();
            $customerTable = $_resources->getTableName('customer_entity');
            $sql = "select * from `$customerTable` where email  = '$email'";
            $result  = $connection->fetchAll($sql);
            if(empty($result))
            {
               $this->messageManager->addError(__('Provided customer email do not exist'));
               return $resultRedirect->setPath('change/password/index');
            }
            // $crypt_key = $this->_deploymentConfig->get('crypt/key');
            $crypt_key ="96c691164f4625ec0ce4030828cccf3b";
            $password_string = $crypt_key.$password;
            $hashstring = ":$crypt_key:1";
            $sql2 = "UPDATE `customer_entity` SET `password_hash` = CONCAT(SHA2('$password_string', 256), '$hashstring') WHERE `email` = '$email'";
            // $sql2="update customer_entity_varchar set value = md5('testtest') where entity_id=$customer_entity_id and attribute_id in (select attribute_id from eav_attribute where attribute_code = 'password_hash' and entity_type_id = 1);"
            $result = $connection->query($sql2);
            $this->messageManager->addSuccess("Password Changed Successfully ");
            return $resultRedirect->setPath('change/password/index');

        }catch (\Exception $e){
            $this->messageManager->addExceptionMessage($e, __('We can\'t submit your request, Please try again.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $resultRedirect->setPath('change/password/index');
        }
    }
}

