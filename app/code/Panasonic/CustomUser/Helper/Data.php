<?php

/* TODO : wrongly structed class found , need to rewrite this class to maintain stability , need to add all dependencies to constructor @ritesh10march2021 */
namespace Panasonic\CustomUser\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
	protected $_inlineTranslation;
    protected $_transportBuilder;

    public function __construct(
    		\Magento\Framework\App\Helper\Context $context,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
			/* adding object manager to contructor @ritesh
				TODO : REMOVE ALLL INSTANCE OF OBJECT MANAGER FROM THIS BADLY WRITTEN CODE 
			*/
			\Magento\Framework\ObjectManagerInterface $objectmanager,
			\Magento\Framework\App\ResourceConnection $ResourceConnection,

      \OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder $transportBuilder
	) {
			parent::__construct($context);
			$this->_storeManager = $storeManager;
			$this->_inlineTranslation = $inlineTranslation;
			$this->_objectManager = $objectmanager;
			$this->_conection = $ResourceConnection->getConnection();
			$this->_transportBuilder = $transportBuilder;
		}

	public function isEcomOrder($order_id)
	{
		$status=1;
		$query= "select ecom_order from sales_order_grid where entity_id='$order_id'";
		$ecom_column_status = $this->_conection->fetchOne($query);
		if(strtolower($ecom_column_status)=='no' || $ecom_column_status=='0')
		{
			$status=0;
		}
		return $status;
	}

	public function RandomFunc($pin,$phone)
	{
		$path=getcwd();  
		include $path.'/sms/index.php';   
	}
	
	public  function sendEmailThroughCurl($to,$from,$msg,$subject)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$to_actual_array = array();
			if(is_array($to))
			{
				/* need array unique as sendgrid so not accept request if emails are duplicated for  any reason */
				$to=array_unique($to);
				foreach ($to as $to_emails)
				{
					$to_actual_array[]=array('email' => $to_emails);
				}
			} else 
			{
				$to_actual_array[]=array('email' => $to);
			}
			/* cannot get this from configuration as i am using the latest v3 version  @ritesh10march2021*/
			$sendgridurl = 'https://api.sendgrid.com/v3/mail/send';
			$token_encrypted = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('smtp/configuration_option/password');
			$_encryptor = $objectManager->create('\Magento\Framework\Encryption\EncryptorInterface');
			$token = $_encryptor->decrypt($token_encrypted);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $sendgridurl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$fied_array =array (
				'personalizations' => array (0 => array(
						'to' =>$to_actual_array
						),
					),
				'from' => 
					array (
						'email' => $from,
					),
				'subject' => $subject,
				'content' => array (0 => 
					array (
						'type' => 'text/html',
						'value' => "<html>$msg</html>",
					),
				),
			);
			$fied_array_json = json_encode($fied_array);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fied_array_json);
			$headers = array();
			$headers[] = "Authorization: Bearer $token";
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result = curl_exec($ch);
			curl_close($ch);
		}		
			 
		public  function getCustomerSupportEmail()
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_support/email');
		}
		
		public  function getFromEmail()
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			return $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_custom2/email');
		}

		
		public function test_email($message_text,$order_id,$subject)
		{
			$customer_support_email = $this->getCustomerSupportEmail();
			$from_email = $this->getFromEmail();
			$this->sendEmailThroughCurl($customer_support_email,$from_email,$message_text,$subject); 
		}	
		
		
		
		public function CustomResendOTP($pin,$phone,$email) 
		{
            
			$customer_support_email = $this->getCustomerSupportEmail();
			$to = array($customer_support_email,$email);
			$from_email = $this->getFromEmail();
			$message='
			<p>Hi,</p>
			<p>Greetings!</p>
			<p>You are just a step away from accessing your GetMySpares account</p>
			<p>We are sharing a verification code to access your account. The code is valid for 59 seconds and usable only once.</p>
			<p>This is to ensure that only you have access to your account.</p>
			<p>Your OTP: - '.$pin.'</p>
			<p>Expires in: - 59 seconds </p>
			<p> </p>
			<p>Best Regards, </p>
			<p>Team GetmySpares</p>'; 
			$subject="GetMySpares Account - your verification code for secure access";  
			$this->sendEmailThroughCurl($to,$from_email,$message,$subject); 
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
			$smsHelper->send_sms($phone,$pin);
		
		}
		
		public function CustomResendOTPMobile($pin,$phone,$email) 
		{ 
			
			$customer_support_email = $this->getCustomerSupportEmail();
			$to = array($customer_support_email,$email);
			$message='
			<p>Hi,</p>
			<p>Greetings!</p>
			<p>You are just a step away from accessing your GetMySpares account</p>
			<p>We are sharing a verification code to access your account. The code is valid for 59 seconds and usable only once.</p>
			<p>This is to ensure that only you have access to your account.</p>
			<p>Your OTP: - '.$pin.'</p>
			<p>Expires in: - 59 seconds </p>
			<p> </p>
			<p>Best Regards, </p>
			<p>Team GetmySpares</p>'; 
			$from_email = $this->getFromEmail();
			$subject="GetMySpares Account - your verification code for secure access";  
			$this->sendEmailThroughCurl($to,$from_email,$message,$subject);
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
			$smsHelper->send_sms($phone,$pin);
		}
		
 

		public function ForgetPasswordHelperOTP($pin,$phone,$message,$email)
		{
			$customer_support_email = $this->getCustomerSupportEmail();
			$to = array($customer_support_email,$email);
			$subject="GetMySpares Forget Password OTP";  
			$from_email = $this->getFromEmail();
			$this->sendEmailThroughCurl($to,$from_email,$message,$subject);
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
			$smsHelper->send_sms($phone,$pin);
			   
			
		}         
 
		public function individual_welcome($fname,$lname,$email) 
		{
			$customer_support_email = $this->getCustomerSupportEmail();
			$to = array($customer_support_email,$email);
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			$customer_name=$fname.' '.$lname;
			
			$message=' 
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td width="100%" valign="top" style="width:100.0%;padding:.75pt .75pt 20.25pt .75pt">
			<div align="center">
			<table  border="0" cellspacing="0" cellpadding="0" width="660" style="width:495.0pt;border-collapse:collapse">
			<tbody>
			<tr>
			<td valign="top" style="background:#3f549c;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="'.$site_url.'" target="_blank"><img border="0" width="180"  src="'.$site_url.'email_logo.jpg" alt="Main Website Store" ></a><u></u><u></u></span></p>
			</td>    
			</tr> 
			<tr>
			<td valign="top" style="background:white;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p ><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">
			'.$customer_name.',
			<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Welcome to GetMySpares Store.<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">To sign in to our site, use these credentials during checkout or on the
			<a href="'.$site_url.'customer/account/" target="_blank">My Account</a> page: <u></u><u></u></span></p>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Email:<u></u><u></u></span></b></p>
			</td>
			<td valign="top" style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="mailto:'.$email.'" target="_blank">'.$email.'</a><u></u><u></u></span></p>
			</td>
			</tr>
			<tr>
			<td style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Password:<u></u><u></u></span></b></p>
			</td>
			<td valign="top" style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><em><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Password you set when creating account</span></em><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><u></u><u></u></span></p>
			</td>
			</tr>
			</tbody>
			</table>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Forgot your account password? Click
			<a href="'.$site_url.'customer/account/forgotpassword" target="_blank" >
			here</a> to reset it. <u></u><u></u></span></p>
			</tr>       
			</tbody>  
			</table>   
			'; 	 	 
			$subject='Welcome To GetMySpares';
			$from_email = $this->getFromEmail();
			$to = array($customer_support_email,$email);
			//$this->sendEmailThroughCurl($to,$from_email,$message,$subject);

			$selectedTemplateId = $this->scopeConfig->getValue('custom_template/email/individual_welcome');
					$templateId = $selectedTemplateId; // template id
		        	$fromEmail = $this->getFromEmail(); // sender Email id
		        	$fromName = 'GetMySpares';             // sender Name
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'site_url' =>$site_url,
		                'customer_name' =>$customer_name,
		                'email'=>$email

		            ];
		 
		            $storeId = $this->_storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo $e->getMessage();
		        }

		} 
 
		
		public function admin_change_company_request($fname,$lname,$email) 
		{
			$customer_support_email = $this->getCustomerSupportEmail();
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$admin_email = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
			$to = array($customer_support_email,$admin_email);

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			 
			$customer_name=$fname.' '.$lname;
			  
			$message='  
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td width="100%" valign="top" style="width:100.0%;padding:.75pt .75pt 20.25pt .75pt">
			<div align="center">
			<table  border="0" cellspacing="0" cellpadding="0" width="660" style="width:495.0pt;border-collapse:collapse">
			<tbody>
			<tr>
			<td valign="top" style="background:#3f549c;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="'.$site_url.'" target="_blank"><img border="0" width="180"  src="'.$site_url.'email_logo.jpg" alt="Main Website Store" ></a><u></u><u></u></span></p>
			</td>    
			</tr> 
			<tr>
			<td valign="top" style="background:white;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p ><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">
			Hello Admin
			<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Company Change Request<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">We got new Request to change company information from below user
			</p>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Name:<u></u><u></u></span></b></p>
			</td>
			<td valign="top" style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">'.$customer_name.'<u></u><u></u></span></p>
			</td>  
			</tr>  
			
			<tr>
			<td style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Email:<u></u><u></u></span></b></p>
			</td>
			<td valign="top" style="padding:.75pt .75pt .75pt .75pt">
			<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">'.$email.'<u></u><u></u></span></p>
			</td>
			</tr>  
			 
			</tbody>
			</table>
			
			</tr>       
			</tbody>  
			</table>
			'; 		   
			$subject='Company Information Change Request';   
			$from_email = $this->getFromEmail();
			//$this->sendEmailThroughCurl($to,$from_email,$message,$subject);

			$selectedTemplateId = $this->scopeConfig->getValue('custom_template/email/admin_change_company_request');
					$templateId = $selectedTemplateId; // template id
		        	$fromEmail = $this->getFromEmail(); // sender Email id
		        	$fromName = 'GetMySpares';
		        	$to = array($customer_support_email,$admin_email);        // sender Name
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'site_url' =>$site_url,
		                'customer_name' =>$customer_name,
		                'email'=>$email

		            ];
		 
		            $storeId = $this->_storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo $e->getMessage();
		        }


		}   
		
		
		public function change_company_request($fname,$lname,$email,$status) 
		{
			     
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$customer_support_email = $this->getCustomerSupportEmail();
			$to = array($customer_support_email,$email);         
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			 
			$customer_name=$fname.' '.$lname;
			  
			$message='  
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td width="100%" valign="top" style="width:100.0%;padding:.75pt .75pt 20.25pt .75pt">
			<div align="center">
			<table  border="0" cellspacing="0" cellpadding="0" width="660" style="width:495.0pt;border-collapse:collapse">
			<tbody>
			<tr>
			<td valign="top" style="background:#3f549c;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="'.$site_url.'" target="_blank"><img border="0" width="180"  src="'.$site_url.'email_logo.jpg" alt="Main Website Store" ></a><u></u><u></u></span></p>
			</td>    
			</tr> 
			<tr>
			<td valign="top" style="background:white;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p ><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">
			Hello '.$customer_name.'
			<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Company Change Request<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Your Request of Change Company Information has been '.$status.'
			</p>
			</tr>       
			</tbody>  
			</table>
			</tr>       
			</tbody> 
			</table>
			'; 		   
			$subject='Company Information Change Request - '.$status;     
			$from_email = $this->getFromEmail();
			//$this->sendEmailThroughCurl($to,$from_email,$message,$subject);
			$selectedTemplateId = $this->scopeConfig->getValue('custom_template/email/company_request');
					$templateId = $selectedTemplateId; // template id
		        	$fromEmail = $this->getFromEmail(); // sender Email id
		        	$fromName = 'GetMySpares';
		        	$to = array($customer_support_email,$admin_email,$email);       // sender Name
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'site_url' =>$site_url,
		                'customer_name' =>$customer_name,
		                'status'=>$status

		            ];
		 
		            $storeId = $this->_storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo $e->getMessage();
		        }
			              
		}   
		 
		
		public function update_custom_data($user_id,$field,$value,$table) 
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$sql = "Select * FROM ".$table." where entity_id='".$user_id."' and attribute_id='".$field."'";
			$result = $connection->fetchAll($sql);  
			$count=count($result);
			if($count==0) 
			{
				$sql = "insert into ".$table." (attribute_id,entity_id,value) values ('".$field."','".$user_id."','".$value."')";		
				$connection->query($sql);     
			}
			else
			{
				$sql = "Update ".$table." Set value = '".$value."' where attribute_id='".$field."' and entity_id = '".$user_id."'";
				$connection->query($sql);     	
			}
		
		}
		
		 
		public function send_email($to,$message,$subject,$email)
		{
			$to=$email;
			$from_email = $this->getFromEmail();
			$this->sendEmailThroughCurl($to,$from_email,$message,$subject);
		}	
		
		
		public function company_request($fname,$lname,$email,$status) 
		{
			     
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$admin_email = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
			$customer_support_email = $this->getCustomerSupportEmail();	         
			$from_email = $this->getFromEmail();
			$to = array($customer_support_email,$admin_email,$email);
			   
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			 
			$customer_name=$fname.' '.$lname;
			  
			$message='  
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td width="100%" valign="top" style="width:100.0%;padding:.75pt .75pt 20.25pt .75pt">
			<div align="center">
			<table  border="0" cellspacing="0" cellpadding="0" width="660" style="width:495.0pt;border-collapse:collapse">
			<tbody>
			<tr>
			<td valign="top" style="background:#3f549c;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="'.$site_url.'" target="_blank"><img border="0" width="180"  src="'.$site_url.'email_logo.jpg" alt="Main Website Store" ></a><u></u><u></u></span></p>
			</td>    
			</tr> 
			<tr>
			<td valign="top" style="background:white;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p ><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">
			Hello '.$customer_name.'
			<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">This is to inform you that you are '.$status.' as company buyer.<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Please <a href="'.$site_url.'customer/account/login" target="_blank" > Click here </a>to login. 
			</p> 
			</tr>       
			</tbody>  
			</table> 
			</tr>        
			</tbody>  
			</table>    
			'; 		   
			$subject='GetMySpares account request '.$status;     
			//$this->sendEmailThroughCurl($to,$from_email,$message,$subject);
			$selectedTemplateId = $this->scopeConfig->getValue('custom_template/email/company_request');
					$templateId = $selectedTemplateId; // template id
		        	$fromEmail = $this->getFromEmail(); // sender Email id
		        	$fromName = 'GetMySpares';
		        	$to = array($customer_support_email,$admin_email,$email);       // sender Name
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'site_url' =>$site_url,
		                'customer_name' =>$customer_name,
		                'status'=>$status

		            ];
		 
		            $storeId = $this->_storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo $e->getMessage();
		        }
		} 
		public function dis_company_request($fname,$lname,$email,$status) 
		{
			     
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			$customer_support_email = $this->getCustomerSupportEmail();	         
			$from_email = $this->getFromEmail();   
			$to = array($customer_support_email,$email);
			$customer_name=$fname.' '.$lname;
			$message='  
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
			<tbody>
			<tr>
			<td width="100%" valign="top" style="width:100.0%;padding:.75pt .75pt 20.25pt .75pt">
			<div align="center">
			<table  border="0" cellspacing="0" cellpadding="0" width="660" style="width:495.0pt;border-collapse:collapse">
			<tbody>
			<tr>
			<td valign="top" style="background:#3f549c;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777"><a href="'.$site_url.'" target="_blank"><img border="0" width="180"  src="'.$site_url.'email_logo.jpg" alt="Main Website Store" ></a><u></u><u></u></span></p>
			</td>    
			</tr> 
			<tr>
			<td valign="top" style="background:white;padding:16.9pt 16.9pt 16.9pt 16.9pt">
			<p ><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">
			Hello '.$customer_name.'
			<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">This is to inform you that you are '.$status.' as company buyer.<u></u><u></u></span></p>
			<p><span style="font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#777777">Please <a href="'.$site_url.'customer/account/login" target="_blank" > Click here </a>to login. 
			</p> 
			</tr>       
			</tbody>  
			</table> 
			</tr>        
			</tbody>  
			</table>    
			'; 		   
			$subject='GetMySpares account request '.$status;     
			$this->sendEmailThroughCurl($to,$from_email,$message,$subject);
		} 
		
		public function check_product_have_seller($product_id)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$sql = "Select * FROM marketplace_product where entity_id='".$product_id."' and is_approved='1'";
			$result = $connection->fetchAll($sql);
			if(!empty($result))
			{ 
				/* echo "<pre>";
					print_r($result[0]);
				echo "</pre>";  */
				
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
				
				$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
				$CustomerModel->setWebsiteId(1);
				$CustomerModel->load($result[0]['seller_id']);
				$userId = $CustomerModel->getId();
				if($userId!='')
				{
					$sql_1 = "Select * FROM customer_entity_varchar where entity_id='".$result[0]['seller_id']."' and attribute_id='178'";
					$result_1 = $connection->fetchAll($sql_1);
					if(!empty($result_1)) {
						return $result_1[0]['value'];
						
						/* echo "<pre>";
							print_r($result_1[0]['value']);
						echo "</pre>";  */
					} 
				}
				
			} 	
			
			
		}	
		
		
		public function get_actual_prd_id($product_id,$order_id)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$sql = "Select * FROM marketplace_saleslist where order_id='".$order_id."' and order_item_id='".$product_id."'";
			$result = $connection->fetchAll($sql);
			
			return $result[0]['mageproduct_id']; 
			
			
		}	
		


		public function customer_phone($email)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
				
			$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
			$CustomerModel->setWebsiteId(1);
			$CustomerModel->loadByEmail($email);
			$userId = $CustomerModel->getId();
			if($userId!='')
			{
				$sql_1 = "Select * FROM customer_entity_varchar where entity_id='".$userId."' and attribute_id='178'";
				$result_1 = $connection->fetchAll($sql_1);
				if(!empty($result_1)) {
					/* echo "<pre>";
						print_r($result_1[0]['value']);
					echo "</pre>";  */
				}  
			}  
			
			
		} 

        

		public function send_order_sms($phone,$order_id) 
		{
			$phone;
			$order_id;     
			$path=getcwd();  
			include $path.'/sms/order.php';    
			 
			//return "Sms sent To Seller"; 
			
		}  	
		
		 
		public function buyerss_details($email)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
			$CustomerModel->setWebsiteId(1);
			$CustomerModel->loadByEmail($email);
			$userId = $CustomerModel->getId();
			
			if($userId!='')
			{
				$sql_1 = "Select * FROM customer_entity_varchar where entity_id='".$userId."' and attribute_id='178'";
				$result_1 = $connection->fetchAll($sql_1);
				if(!empty($result_1)) {
					return $result_1[0]['value'];
				}  
			}   
		} 
		
		
		public function seller_details($productid,$orderid){
			
			 // return $orderid;
			 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $sellerOrder = $objectManager->create('Webkul\Marketplace\Model\Orders')->getCollection()
					->addFieldToFilter('order_id', $orderid)
					->addFieldToFilter('product_ids', $productid);
					if(!empty($sellerOrder)){
						foreach ($sellerOrder as $info) {
							
							$sellerid = $info->getSellerId();
							if ($sellerid > 0) {
								$customer = $objectManager->get(
									'Magento\Customer\Model\Customer'
								)->load($sellerid);
								if ($customer) {
														
									$returnArray = [];
									$returnArray['seller_name'] = $customer->getName();
									$returnArray['seller_gst'] = $customer->getGstNumber();
									$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
									$returnArray['id'] = $customer->getId();
									
									
									$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			
									$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
									$connection = $resource->getConnection();
									
									$sql_1 = "Select * FROM customer_entity_varchar where entity_id='".$returnArray['id']."' and attribute_id='178'";
									$result_1 = $connection->fetchAll($sql_1);
									if(!empty($result_1)) {
										$returnArray['phone']=$result_1[0]['value'];
									} 
									
									
									return $returnArray;
								}
								
							}else{
								 $test = "No data";
								 return $test;
							}
						}
					}else{
						$error= "something went wrong";
						return $error;
					}
		}
	   public function seller_data($sellerid){
		   
		   if ($sellerid > 0) {
					$customer = $objectManager->get(
						'Magento\Customer\Model\Customer'
					)->load($sellerid);
					if ($customer) {
											
						$returnArray = [];
						$returnArray['seller_name'] = $customer->getName();
						$returnArray['seller_gst'] = $customer->getGstNumber();
						$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
						$returnArray['id'] = $customer->getId();

						return $returnArray;
					}
					
				}else{
					 $test = "No data";
					 return $test;
				}
		   
	   }
	   
	   public function ord_seller_details($order_id){
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            
			$sellerOrder = $objectManager->create('Webkul\Marketplace\Model\Orders')->getCollection()
					->addFieldToFilter('order_id', $order_id);
			foreach ($sellerOrder as $info) {
				
				$sellerid = $info->getSellerId();
				if ($sellerid > 0) {
					$customer = $objectManager->get(
						'Magento\Customer\Model\Customer'
					)->load($sellerid);
					if ($customer) {
											
						$returnArray = [];
						$returnArray['seller_name'] = $customer->getName();
						$returnArray['seller_gst'] = $customer->getGstNumber();
						$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
						$returnArray['id'] = $customer->getId();

						return $returnArray;
						// print_r($returnArray);
					}
					
				}else{
					 $test ="";
					 echo  $test;
				}
			}
			
		}
		 
		public function get_productid_marketplace($product_id,$orderId)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
			$shippingAddress = $order->getShippingAddress();
			$shippingPostcode = $shippingAddress->getPostcode();
			$sql = "Select seller_id FROM marketplace_product where entity_id='".$product_id."' and is_approved='1'";
			$result = $connection->fetchAll($sql);
			if(!empty($result))
			{	
			//$sellerid=$result[0]['seller_id'];
			$sql = "SELECT seller_id FROM `marketplace_orders` where product_ids = $product_id and order_id = $orderId";
            $sellerId = $connection->fetchOne($sql);
			
			
			$customer = $objectManager->get(
						'Magento\Customer\Model\Customer'
					)->load($sellerid);
		
			echo '
			<p>'.$customer->getCompanyNam().'</p>
			<p>GST('.$customer->getGstNumber().')</p>';
			}
				
			
			/* $sellerid = $result[0]['seller_id'];
				$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($sellerid);
				echo "
				<p>".$customer->getName()."</p>
				<p>GST (".$customer->getGstNumber().")</p>
				"; */
				
			
			
			 
			
		}
		
		public function customer_data($sellerid)
		{
		   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$sellerid;
			if($sellerid!='')
			{	
			$customer = $objectManager->get(
						'Magento\Customer\Model\Customer'
					)->load($sellerid);
			
				echo $customer->getGstNumber();
			}
			else
			{
				echo "N/A";
			}
			/* if ($customer) {
									
				$returnArray = [];
				$returnArray['seller_name'] = $customer->getName();
				$returnArray['seller_gst'] = $customer->getGstNumber();
				$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
				$returnArray['id'] = $customer->getId();

				return $returnArray;
			} */
			
			
			/* if ($sellerid > 0) {
					$customer = $objectManager->get(
						'Magento\Customer\Model\Customer'
					)->load($sellerid);
					if ($customer) {
											
						$returnArray = [];
						$returnArray['seller_name'] = $customer->getName();
						$returnArray['seller_gst'] = $customer->getGstNumber();
						$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
						$returnArray['id'] = $customer->getId();

						return $returnArray;
					}
					
				}else{
					 $test = "No data";
					 return $test;
				} */
		   
	   }
		
		public function get_awb($orderr)
		{
			//echo $order;

			
			
			$order_id = $orderr;
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
			$orderObj = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);

			$shippingAddressObj = $orderObj->getShippingAddress();

			$shippingAddressArray = $shippingAddressObj->getData();
			
			
			
			$zipcodeCollection = $objectManager->create( 'Ecom\Ecomexpress\Model\Pincode' )->load($shippingAddressArray['postcode'],'pincode');
			  
			 		
			//if(count($zipcodeCollection->getData())){
				$payment = $orderObj->getPayment()->getMethodInstance()->getCode();
				$pay_type = 'PPD';
				if($payment == 'cashondelivery' || $payment == 'phoenix_cashondelivery' || $payment == 'mst_cashondelivery')
					$pay_type = 'COD';
				$model = $objectManager->create ( 'Ecom\Ecomexpress\Model\Awb' )->getCollection()
					->addFieldToFilter('state',0)->addFieldToFilter('awb_type',$pay_type);
				if(count($model->getData())){
					$awb = $model->getFirstItem()->getAwb();
					//echo $awb;
					
					echo '
					<p class="cust_carr" style="display:none;">Ecom Express</p>
					<p class="awb" style="display:none;">'.$awb.'</p>
					';  
					  
					  
					//die;
					//return $awb;
				}else{  
				?><span class="awb-not-available"><?php echo $pay_type.' AWB number is not available';?></span>
			<?php } 
		} 
		
		 
		public function min_product_price_by_category($cat_id)
		{
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
 
			//$appState = $objectManager->get('\Magento\Framework\App\State');
			//$appState->setAreaCode('frontend');
			 
			$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
			$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
			$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
			
			
			
			$categoryId = $cat_id; // YOUR CATEGORY ID
			$category = $categoryFactory->create()->load($categoryId);
			 
			$categoryProducts = $category->getProductCollection()
										 ->addAttributeToSelect('*');
			 
			$product_count=0;
			$min_price=0;
			
			
			$productAarray=array(); 
			$priceArray=array(); 
			 
			$productAarray=$categoryProducts->getData();
			
			
			if(!empty($productAarray))
			{
				foreach ($categoryProducts as $product) 
				{
					$priceArray[]=$product->getFinalPrice();	
					
				} 
				$product_count=count($categoryProducts);
				$min_price=min($priceArray);
				$array=array('product_count'=>$product_count,'min_price'=>$min_price);
			}
			else
			{	
				$array=array('product_count'=>$product_count,'min_price'=>$min_price);
			} 
			
			
			return json_encode($array);
		}	
		
		
		public function get_brand_id($name)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$sql = "Select * FROM eav_attribute_option_value where value='".$name."' order by value_id desc limit 0,1";
			
			$result = $connection->fetchAll($sql);	
			
			
			return $result[0]['option_id'];  
		} 	 
		 
		
		
		
		
		/************************ Quick Book APi ******************/
		
		
			 
		
		
		public function get_access_key()
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$sql = "Select * FROM quick_book_api";
			$result = $connection->fetchAll($sql);
			
			$select = $connection->select()
                  ->from('quick_book_api'); 
		
			$result = $connection->fetchAll($select);
			
			
			return $result[0]['access_code'];
		}	
		 
		
		

		public function create_expense($amount,$magento_order_id)
		{
			$accesstoken=$this->get_access_key(); 
			 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($magento_order_id);
			$orderArray=$order->getData(); 
			
			$orderId=$order->getId();
			
			
			
			$select = $connection->select()
                  ->from('sales_payment_transaction') 
                  ->where('order_id = ?', $orderId);
		
			$result = $connection->fetchAll($select);
			
			
			
			if(!empty($result)) {
			
				$payment_id=$result[0]['txn_id'];
				
				$url=$site_url.'qbonline/connection/razorpayment/order/'.$payment_id;
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_URL, $url);
				
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				$response  = curl_exec($ch); 
				
				$result=json_decode($response);

				if($result->payment == 'card')
				{
					if($result->card == 'debit')
					{
						$PaymentType='CreditCard';	
						$payment_method=34; 
						$PaymentMethodRef=array(
							"value"=> $payment_method
						);	 
						
					} 
					else 
					{
						$PaymentType='CreditCard';	
						$payment_method=34; 
						$PaymentMethodRef=array(
							"value"=> $payment_method 
						);	
						
					} 
					
					if($result->network == 'Visa')
					{
						$AccountRef=array(
						  "value"=> "155",
						  "name"=> "Visa" 
						);	
					}
					else if($result->network == 'Mastercard')
					{
						$AccountRef=array(
						  "value"=> "163",
						  "name"=> "Mastercard" 
						);	
					} 
					else
					{
						$AccountRef=array(
						  "value"=> "164",
						  "name"=> "Others" 
						);	
					}
					
				} 
				else if($result->payment == 'wallet')
				{
					$PaymentType='Cash';	
					$payment_method=32;
					$payment_account=165; 
		
					 
					$AccountRef=array(
					  "value"=> $payment_account  
					);
					
					$PaymentMethodRef=array(
						"value"=> $payment_method
					);	   
				}  
				else if($result->payment == 'netbanking')
				{
					$PaymentType='Cash';	
					
					$payment_method=32;
					$payment_account=166;	
					$AccountRef=array(
					  "value"=> $payment_account
					);
					
					$PaymentMethodRef=array(
						"value"=> $payment_method
					);	
				}
				else if($result->payment == 'upi')
				{
					$PaymentType='Cash'; 	
					$payment_method=32;
					$payment_account=167;			
					$AccountRef=array( 
					  "value"=> $payment_account
					);
					
					$PaymentMethodRef=array(
						"value"=>$payment_method
					);	
				}	
				
				
				
				$invoice_id=$this->get_order_invoice_id($magento_order_id);	
				
				
				$payee=280; 
				$category=118;    
				
				$myArr = array(
				"AccountRef" => $AccountRef,  
				"PaymentType"=> $PaymentType,
				"DocNumber"=> 'invoice_'.$invoice_id,
				"EntityRef"=> array(  
				  "value"=> $payee    
				),   
				"PaymentMethodRef"=> $PaymentMethodRef,
				"Line"=> [
					array(
						"Amount"=> $result->fee,  
						"DetailType"=> "AccountBasedExpenseLineDetail",
						"Description"=> "RazorPay Fee",
						"AccountBasedExpenseLineDetail"=> array(
							"AccountRef"=> array(
								"value"=> "168"
							)  
							 
						),
						"LineNum" => 1, 
						"Id" => "1"
					),
					array(
						"Amount"=> $result->tax,   
						"DetailType"=> "AccountBasedExpenseLineDetail",
						"Description"=> "RazorPay GST",
						"AccountBasedExpenseLineDetail"=> array(
							"AccountRef"=> array(
								"value"=> "169"
							)
							  
						),
						"LineNum" => 2, 
						"Id" => "2"
					) 	
				] 
				);      
			
				return json_encode($myArr); 
			}
			else
			{
				return 'null';
			}    
			        
			 
		}	

		
		public function admin_commission($orderid)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderid);
			$OrderId  = $order->getData();
			$newOrderId = $OrderId['entity_id'];
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
				 	
					
			
			$select = $connection->select()
                  ->from('marketplace_saleslist') 
                  ->where('order_id = ?', $newOrderId);
		
			$result = $connection->fetchAll($select);
			
			
			
			if(!empty($result))
			{ 	
				foreach($result as $row)
				{
				   
					$total_commission=$row['total_commission'];    
					$total_tax=$row['total_tax'];
					//$comm_amount = $total_commission+$total_tax;
					$comm_amount = $total_commission;
					  
				} 
				
				$invoice_id=$this->get_order_invoice_id($orderid);	
				
				$payee=273;
				$payment_account=156;
				$payment_method=32;
				$category=118;
				
				
				$myArr = array(
					"AccountRef"=> array(
					  "value"=> $payment_account
					),
					"PaymentType"=> "CreditCard",
					"DocNumber"=> 'invoice_'.$invoice_id,
					"EntityRef"=> array(  
					  "value"=> $payee
					), 
					"PaymentMethodRef"=> array(
					  "value"=> $payment_method
					),
					"Line"=> [
					  array(
						"Amount"=> $comm_amount,    
						"DetailType"=> "AccountBasedExpenseLineDetail",
						"Description"=> "commission & Fee",
						"AccountBasedExpenseLineDetail"=> array(
							"AccountRef"=> array(
								"value"=> $category
							)   
						) 
					  )   
					]
				);   
				   
			 
				return json_encode($myArr);	 
			}
			else
			{ 
				return 'null';
			}    
			
		}
			
    
		public function noddle_amount($orderid)
		{
				
			
			$payment_method=35;
			$payment_account=155; // Visa
			$payee=272; 
			$category=139;   
			
				
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($orderid);
			
			
			
			
			$order_amount=$order->getPayment()->getAmountOrdered();
			$noddle_amount=$order_amount * 0.01;
			 
			$noddle_amount=number_format($noddle_amount, 2, '.', '');
			
			$invoice_id=$this->get_order_invoice_id($orderid);	
			
			
			$myArr = array(
				"AccountRef"=> array(
				  "value"=> $payment_account
				),
				"PaymentType"=> "Cash", 
				"DocNumber"=> 'invoice_'.$invoice_id, 
				"EntityRef"=> array(    
				  "value"=> $payee        
				), 
				"PaymentMethodRef"=> array(
				  "value"=> $payment_method  
				), 
				"Line"=> [
				  array(
					"Amount"=> $noddle_amount,    
					"DetailType"=> "AccountBasedExpenseLineDetail",
					"Description"=> "Nodal Bank Account", 
					"AccountBasedExpenseLineDetail"=> array(
						"AccountRef"=> array(
							"value"=> $category
						)     
					) 
				  )   
				]
			);   
			   
		  
			//echo json_encode($myArr);	 
			return json_encode($myArr);	 
			 
		}   

		public function shipping_chanrge($orderid) 
		{
			
			$payment_method=32; 
			$payment_account=157; // Visa
			$payee=274; 
			$category=118;      
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($orderid);
			
			$ship_amount=$order->getShippingAmount();
			
			$invoice_id=$this->get_order_invoice_id($orderid);	
			
			$myArr = array( 
				"AccountRef"=> array(
				  "value"=> $payment_account
				), 
				"PaymentType"=> "Cash", 
				"DocNumber"=> 'invoice_'.$invoice_id, 
				"EntityRef"=> array(    
				  "value"=> $payee         
				), 
				"PaymentMethodRef"=> array(
				  "value"=> $payment_method  
				), 
				"Line"=> [
				  array( 
					"Amount"=> $ship_amount,      
					"DetailType"=> "AccountBasedExpenseLineDetail",
					"Description"=> "Ecom",  
					"AccountBasedExpenseLineDetail"=> array(
						"AccountRef"=> array(
							"value"=> $category
						)        
					)   
				  )   
				]
			);    
			   
		     
			//echo json_encode($myArr);	 
			return json_encode($myArr);	 
			 
		}  
		
		
		public function gst_tax($orderid)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($orderid);
			 
			$newArray = $order->getData();       
			
			$cgst_amount=$newArray['cgst_amount'];
			$sgst_amount=$newArray['sgst_amount'];
			$igst_amount=$newArray['igst_amount'];
			
			$newGsts=$cgst_amount + $sgst_amount + $igst_amount;
			
			     
			
			$invoice_id=$this->get_order_invoice_id($orderid);	
			
			$payment_method=32; 
			$payment_account=159; // Visa
			$payee=279;  
			$category=118;      
			
			 
			
			$myArr = array( 
				"AccountRef"=> array(
				  "value"=> $payment_account
				), 
				"PaymentType"=> "Cash", 
				"DocNumber"=> 'invoice_'.$invoice_id, 
				"EntityRef"=> array(    
				  "value"=> $payee          
				), 
				"PaymentMethodRef"=> array(
				  "value"=> $payment_method  
				), 
				"Line"=> [
				  array( 
					"Amount"=> $newGsts,      
					"DetailType"=> "AccountBasedExpenseLineDetail",
					"Description"=> "GST",  
					"AccountBasedExpenseLineDetail"=> array(
						"AccountRef"=> array(
							"value"=> $category
						)         
					)   
				  )   
				]
			);    
			    
		     
			//echo json_encode($myArr);	 
			return json_encode($myArr);	 
			 
		 
		
		}	
		
		 
		
		public function vendor_charge($seller_id,$amount,$orderid) 
		{ 
			 
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($orderid);
			
			$invoice_id=$this->get_order_invoice_id($orderid);
			
		
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
                  ->from('vendor_qb_id') 
                  ->where('user_id = ?', $seller_id);
		
			$result = $connection->fetchAll($select); 
			
			
			
			$qbb_id='276';  // Default Seller Qb Id 
			 
			if(!empty($results))		
			{
				$qb_details=json_decode($results[0]['qb_details']);
				$qbb_id=$qb_details->Vendor->Id;
			} 
			 
			
			
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($orderid);

			$order_amount=$order->getPayment()->getAmountOrdered();
			$noddle_amount=$order_amount * 0.01;
			 
			$noddle_amount=number_format($noddle_amount, 2, '.', '');


			
			 
			$razor_amount=0; 
			
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			
			
			$orderId=$order->getId();
			
			
			
			$select = $connection->select()
                  ->from('sales_payment_transaction') 
                  ->where('order_id = ?', $orderId);
		
			$resultss = $connection->fetchAll($select); 
			
			
			
			
			if(!empty($resultss)) {
			
				$payment_id=$resultss[0]['txn_id'];
				
				$url=$site_url.'qbonline/connection/razorpayment/order/'.$payment_id;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				$response  = curl_exec($ch); 
				$result=json_decode($response);
				
				$fee=$result->fee;
				$tax=$result->tax;
				
				$razor_amount=$fee+$tax;
			
			}
			
			    
			
			$seller_amount=$amount;
			
			$deducted_amount=$noddle_amount+$razor_amount;
			
			 
			$total_amount=$seller_amount - $deducted_amount;
			

			$payment_method=32; 
			$payment_account=158; // Visa
			$payee=274; 
			$category=118;       
			
			  
			$myArr = array( 
				"AccountRef"=> array(
				  "value"=> $payment_account
				), 
				"PaymentType"=> "Cash", 
				"DocNumber"=> 'invoice_'.$invoice_id,  
				"EntityRef"=> array(   
				  "value"=> $qbb_id          
				),      
				"PaymentMethodRef"=> array(
				  "value"=> $payment_method  
				), 
				"Line"=> [    
				  array( 
					"Amount"=> $total_amount,        
					"DetailType"=> "AccountBasedExpenseLineDetail",
					"Description"=> "commission",   
					"AccountBasedExpenseLineDetail"=> array(
						"AccountRef"=> array(
							"value"=> $category
						)        
					)   
				  )   
				]
			);       
			     
		        
			//echo json_encode($myArr);	 
			return json_encode($myArr);	   
			   
		}   

		
		function get_order_invoice_id($magento_order_id)
		{
			 
				
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
                  ->from('sales_invoice_grid') 
                  ->where('order_increment_id = ?', $magento_order_id);
		
			$results = $connection->fetchAll($select); 
			  
			
			
			if(!empty($results))		
			{
				return $results[0]['increment_id'];
			}
			else
			{
				return 0;
			}     		
		}	
		
		
			
		
		
		function get_vendor_qb_id($seller_id)
		{
			 
				
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			   
			
			
			$select = $connection->select()
                  ->from('vendor_qb_id') 
                  ->where('user_id = ?', $seller_id);
		
			$results = $connection->fetchAll($select); 
			
			
			if(!empty($results))		
			{
				$qb_details=json_decode($results[0]['qb_details']);
				return $qb_details->Vendor->Id;
			}
			 	
		}	 
		 
		
		function get_seller_amount($seller_id,$order_id)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			   
			$sql = "Select sum(actual_seller_amount) 'amount' FROM marketplace_saleslist where seller_id='".$seller_id."' and magerealorder_id='".$order_id."'";
			$result = $connection->fetchAll($sql); 
			
			
			
			
			if(!empty($result))		
			{
				
				return $result[0]['amount'];
			}
			
		}	
		
		
		/*********Ecom Section*********/
		
				public function ecom_price($buyer_zipcode)
		{ 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			 
			
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');  
			 
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			
			
				
			 
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			 
			// get quote items collection
			$itemsCollection = $cart->getQuote()->getItemsCollection();
			 
			// get array of all items what can be display directly
			$itemsVisible = $cart->getQuote()->getAllVisibleItems();
			 
			// get quote items array
			$items = $cart->getQuote()->getAllItems();
			
			$itemcount=count($cart->getQuote()->getAllItems());
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			
			  
			
			if($itemcount!=0) 
			{ 
				foreach($items as $item) 
				{
					/* echo 'ID: '.$item->getProductId().'<br />'; 
					echo 'Name: '.$item->getName().'<br />';
					echo 'Sku: '.$item->getSku().'<br />';
					echo 'Quantity: '.$item->getQty().'<br />';
					echo 'Price: '.$item->getPrice().'<br />';
					echo "<br />";     */   
					 
					$qty=$item->getQty();
					$weight=$item->getWeight();
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					 
					$actual_prd_id=$item->getProductId();
					echo $actual_prd_id;
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					
					$select = $connection->select()
					->from('marketplace_product') 
					->where('mageproduct_id = ?', $actual_prd_id);
		
					$result = $connection->fetchAll($select); 
					
					if(!empty($result))
					{
						$seller_id=$result[0]['seller_id'];	
					}	
					else
					{
						$seller_id=29;
					}		
					
					/* if($seller_id==20)
					{
						$seller_zipcode='110001';		
					} 
					else 
					{
						$seller_zipcode='122002'; 				
						//$seller_zipcode='190023'; // Srinagar				
					}	 */	
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					
					$sellerArray=$custom_helpers->get_seller_details($seller_id);
					if(!empty($sellerArray))
					{
						$seller_zipcode=$sellerArray['zipcode'];	
					}	
					else 
					{
						$seller_zipcode='122002'; 	
					}
					 
					/* echo "<pre>";
						print_r($sellerArray);
					echo "</pre>";
					die; */
					
					$seller_zipcode;  
					
					  
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					
					
					
					
					/* echo "Buyer Ros=".$buyer_ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";   */  
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					/* echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	 */
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							}	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								
								if($buyer_region_code=="jnk" || $buyer_region=="nthest")
								{
									$results=0;	 	
								}
								
							}
							
							
							
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					}
					
									
					
						
					
					  
					$weight;
					
					
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					$base_price=$price->Base;
					$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					if($findWeight > 1){ 
						
						for($i=1; $i < $findWeight; $i++){
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$base_price+$ros_base+$up_base;
						}    
					} 
					else    
					{
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					  
					
					$charge;
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				}      
					$product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					$gst=$fuel_amount+$other_amount;
					
					$gst_amount=($net_total*18)/100; 
					      
					
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					

					if($ex_zone=='0')
					{
						$show_zone=$zone;
					}	
					else 
					{	
						$show_zone=$zone.'+'.$ex_zone;
					}		
					 
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$show_zone);  
					
					$array = array('success' => 1, 'result' => $result_array); 

			} 
			else 
			{
				
				$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$zone);
				
				$array = array('success' => 0, 'result' => $result_array); 
			}   		



			// return $grand_total;	 
					
			
			echo "<br>";
			echo json_encode($array);
		} 
		
		
		
		public function ecom_price_mobile($buyer_zipcode,$user_id)
		{ 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');  
			 
			
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');  
			 
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			
		
			
			
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			
			
				
			/* 
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			 
			// get quote items collection
			$itemsCollection = $cart->getQuote()->getItemsCollection();
			 
			// get array of all items what can be display directly
			$itemsVisible = $cart->getQuote()->getAllVisibleItems();
			 
			// get quote items array
			$items = $cart->getQuote()->getAllItems();
			
			$itemcount=count($cart->getQuote()->getAllItems());
			
			*/
			
			
			$token=$api_helpers->get_token();
			$quote_id=$api_helpers->get_quote_id($token,$user_id);
			$quote_id=json_decode($quote_id); 




			$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
			$allItems=$quote->getAllVisibleItems();

			$mainArrayy=array();

			
			
			
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			
			  
			
			if(!empty($allItems))
			{  
				foreach($allItems as $item) 
				{ 
					/* echo 'ID: '.$item->getProductId().'<br />'; 
					echo 'Name: '.$item->getName().'<br />';
					echo 'Sku: '.$item->getSku().'<br />';
					echo 'Quantity: '.$item->getQty().'<br />';
					echo 'Price: '.$item->getPrice().'<br />';
					echo "<br />";     */   
					 
					$qty=$item->getQty();
					$weight=$item->getWeight();
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					 
					$actual_prd_id=$item->getProductId();
					
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					
					$select = $connection->select()
					->from('marketplace_product') 
					->where('mageproduct_id = ?', $actual_prd_id);
		
					$result = $connection->fetchAll($select); 
					
					if(!empty($result))
					{
						$seller_id=$result[0]['seller_id'];	
					}	
					else
					{
						$seller_id=29;
					}		
					
					/* if($seller_id==20)
					{
						$seller_zipcode='110001';		
					} 
					else 
					{
						$seller_zipcode='122002'; 				
						//$seller_zipcode='190023'; // Srinagar				
					}	 */	
					
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					
					$sellerArray=$custom_helpers->get_seller_details($seller_id);
					if(!empty($sellerArray))
					{
						$seller_zipcode=$sellerArray['zipcode'];	
					}	
					else 
					{
						$seller_zipcode='122002'; 	
					}
					 
					/* echo "<pre>";
						print_r($sellerArray);
					echo "</pre>";
					die; */
					
					$seller_zipcode;  
					
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					
					
					
					
					/* echo "Buyer Ros=".$buyer_ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";   */  
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					/* echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	 */
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							}	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								
								if($buyer_region_code=="jnk" || $buyer_region=="nthest")
								{
									$results=0;	 	
								}
								
							}
							
							
							
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					} 
					
									
					
						
					
					  
					$weight;
					
					
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					$base_price=$price->Base;
					$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					if($findWeight > 1){ 
						
						for($i=1; $i < $findWeight; $i++){
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$base_price+$ros_base+$up_base;
						}    
					} 
					else    
					{
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					  
					
					$charge;
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				}      
					$product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					$gst=$fuel_amount+$other_amount;
					
					$gst_amount=($net_total*18)/100; 
					      
					
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					

					if($ex_zone=='0')
					{
						$show_zone=$zone;
					}	
					else 
					{	
						$show_zone=$zone.'+'.$ex_zone;
					}		
					 
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$show_zone);  
					
					$array = array('success' => 1, 'result' => $result_array); 

			} 
			else 
			{
				
				$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$zone);
				
				$array = array('success' => 0, 'result' => $result_array); 
			}   		



			$grand_total=number_format($grand_total, 2, '.', '');
							
			return  $grand_total;	      
					 
			
			//echo "<br>";
			
			//echo json_encode($array);
		} 
		
		
		
		
		public function ecom_price_by_product($buyer_zipcode,$product_id,$qty)
		{  
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$catQuery = $connection->fetchAll("SELECT category_id  FROM `catalog_category_product` WHERE `product_id` = $product_id");
			foreach($catQuery as $cats){
				$catId = $cats['category_id'];
				$catVal = $connection->fetchOne("SELECT value FROM `catalog_category_entity_int` where attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='is_shipping_charge_free') AND entity_id = $catId");
				if($catVal && $catVal=='1'){
					return 0;
				}
			}			
				
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
			$items = $cart->getQuote()->getAllItems();
			
			$freeSellers = array();
			foreach ($items as $item) {
				$p_pid = $item->getProduct()->getId();
				$p_qty = $item->getQty();
				$sellerId = $connection->fetchOne("SELECT seller_id FROM `marketplace_product` where mageproduct_id = $p_pid");				
				$noEcomm = $connection->fetchOne("SELECT COUNT(ship_ecom) FROM `catalog_product_entity_tier_price` where entity_id=$p_pid AND ship_ecom=0 AND (qty = $p_qty or qty < $p_qty)");
				if($noEcomm > 0){
					$freeSellers[] = $sellerId;
				}
			}	
		
			$sellerId_1 = $connection->fetchOne("SELECT seller_id FROM `marketplace_product` where mageproduct_id = $product_id");	
			if( in_array($sellerId_1,$freeSellers) ){
				return 0;
			}
			
			$qryS = "SELECT COUNT(shipping_free) FROM `catalog_product_entity_tier_price` where entity_id=$product_id AND shipping_free=1 AND (qty = $qty or qty < $qty)";
			$ifFreeSHip = $connection->fetchOne($qryS);
			if($ifFreeSHip > 0){
				return 0;
			}			
					
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			
			
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');  
			$neststatecodearray=array('mn','tr','ml','ar','sk','nl','mz'); 
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			
			//echo $buyer_zipcode.' '.$product_id.' '.$qty;		
			
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			/* state for ne calculation */
				
			
				
			 
			
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			
														
			
					$weight=$product->getWeight();
			
			
					 
					
					//$weight=$product->getWeight();
					/*
					echo "Weight=".$weight;
					echo "<br>";
					echo "Ecom Length=".$product->getEcomLength();
					echo "<br>"; 
					echo "Ecom Breadth=".$product->getEcomBreadth();
					echo "<br>";
					echo "Ecom Height=". $product->getEcomHeight();
					echo "<br>"; 
					*/
					  
					
					$vol_weight=($product->getEcomLength()*$product->getEcomBreadth()*$product->getEcomHeight())/5000;
					 
					if($vol_weight > $weight)  
					{
						$weight=$vol_weight;   
					}
					$weight;
					  		 		
					  
					
					
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					 
					$actual_prd_id=$product->getProductId();
					
					$select = $connection->select()
					->from('marketplace_product') 
					->where('mageproduct_id = ?', $product_id); 
		
					$result = $connection->fetchAll($select);  
					
					
					$count=count($result);
					
					if($count==0)
					{
						$seller_id='20';		  
					}	
					foreach($result as $row)
					{
						$seller_id=$row['seller_id'];			
					}	
					
					/* if($seller_id==20)
					{
						$seller_zipcode='110001';		
					} 
					else 
					{
						$seller_zipcode='122002'; 				
						//$seller_zipcode='190023'; // Srinagar				
					}	 */	
					 
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					
					$sellerArray=$custom_helpers->get_seller_details($seller_id);
					if(!empty($sellerArray))
					{
						$seller_zipcode=$sellerArray['zipcode'];	
					}	
					else 
					{
						$seller_zipcode='122002'; 	
					}
					 
					/* echo "<pre>";
						print_r($sellerArray);
					echo "</pre>";
					die; */
					
					$seller_zipcode;  
					
					  
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					
					
					
					
					/* echo "Buyer Ros=".$buyer_ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";   */  
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					/* echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	 */
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							}	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								if($buyer_region_code=="jnk" || $buyer_region=="nthest" || $seller_region=="nthest")
								{
									$results=0;	 	
								}
								
							}
							
							
							
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest" || $seller_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					}
					
									
					
						
					
					  
					$weight;
					
					
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					$base_price=$price->Base;
					$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					
					
					$findWeight = ceil($findWeight); 
					if($findWeight > 1)
					{ 
						
						for($i=1; $i <= $findWeight; $i++)   
						{
							//$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base+$base_price;
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base;
							 
							
						
						}      
					} 
					else    
					{ 
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}
					$buyer_state = $this->get_state_pincode($buyer_zipcode);
					$seller_state = $this->get_state_pincode($seller_zipcode);
			  
					/* Zone F  calculation for northeast states  */
					if($zone=="F") {
						$nespecialcharge=0;
						if(in_array($buyer_state,$neststatecodearray) || in_array($seller_state,$neststatecodearray)){
							/* add a surcharge of INR 50 per 500 gram */
							for($i=0; $i < $findWeight; $i++)   
							{
								$nespecialcharge += 50;		
							}
						}
						$charge = $charge + $nespecialcharge;
					}			 
					
					/*
					if($findWeight > 1)
					{ 
						
						for($i=1; $i < $findWeight; $i++){
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$base_price+$ros_base+$up_base;
						}    
					} 
					else    
					{
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					*/  
					
					$charge;
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				      
					$product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					
					$other_amount=0;
					
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					$gst=$fuel_amount+$other_amount;
					
					$helper = $objectManager->create('Baljit\Buynow\Helper\Data');
					$shipping_gst = $helper->getConfig('buynows/general/shipping_gst');
					if(empty($shipping_gst))
					{
						$shipping_gst=0;
					}
					 
					
					//$gst_amount=($net_total*18)/100; 
					$gst_amount=($net_total*$shipping_gst)/100; 
					      
					 
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					

					if($ex_zone=='0') 
					{
						$show_zone=$zone;
					}	 
					else 
					{	 
						$show_zone=$zone.'+'.$ex_zone;
					}		
					 
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'gst_rate'=>$shipping_gst,'grand_total'=>$grand_total,'zone'=>$show_zone);   
					
					$array = array('success' => 1, 'result' => $result_array); 
					
					$admin_comm = $helper->getConfig('buynows/general/logistic_pana_tax');
					if(empty($admin_comm))
					{
						$admin_comm=0;
					} 
					
					
					$grand_total=$grand_total+(($grand_total*$admin_comm)/100);    
					$grand_total=number_format($grand_total, 2, '.', '');
					 		
			return $grand_total;	     
			
		}


		public function ecom_price_by_product_copy($buyer_zipcode,$product_id,$qty)
		{
			  
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			
			
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');
			$neststatecodearray=array('mn','tr','ml','ar','sk','nl','mz');  
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			
			//echo $buyer_zipcode.' '.$product_id.' '.$qty;		
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			
			echo "<br> buyer_ros ".$buyer_ros;
			echo "<br> buyer_up_country ".$buyer_up_country;
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			
														
			
					$weight=$product->getWeight();
			
			
					 
					
					//$weight=$product->getWeight();
					/*
					echo "Weight=".$weight;
					echo "<br>";
					echo "Ecom Length=".$product->getEcomLength();
					echo "<br>"; 
					echo "Ecom Breadth=".$product->getEcomBreadth();
					echo "<br>";
					echo "Ecom Height=". $product->getEcomHeight();
					echo "<br>"; 
					*/
					  
					
					$vol_weight=($product->getEcomLength()*$product->getEcomBreadth()*$product->getEcomHeight())/5000;
					 
					if($vol_weight > $weight)  
					{
						$weight=$vol_weight;   
					}
					$weight;
					  		 		
					  
					
					
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					 
					$actual_prd_id=$product->getProductId();
					 
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					
					$select = $connection->select()
					->from('marketplace_product') 
					->where('mageproduct_id = ?', $product_id); 
		
					$result = $connection->fetchAll($select);  
					
					
					$count=count($result);
					
					if($count==0)
					{
						$seller_id='20';		  
					}	
					foreach($result as $row)
					{
						$seller_id=$row['seller_id'];			
					}	
					
					/* if($seller_id==20)
					{
						$seller_zipcode='110001';		
					} 
					else 
					{
						$seller_zipcode='122002'; 				
						//$seller_zipcode='190023'; // Srinagar				
					}	 */	
					 
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					echo "<br> seller id ".$seller_id;
					$sellerArray=$custom_helpers->get_seller_details($seller_id);
					if(!empty($sellerArray))
					{
						$seller_zipcode=$sellerArray['zipcode'];	
					}	
					else 
					{
						$seller_zipcode='122002'; 	
					}
					 
					/* echo "<pre>";
						print_r($sellerArray);
					echo "</pre>";
					die; */
					
					echo "<br> seller zipcode ". $seller_zipcode;  
					
					  
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					echo "<br> seller_ros ".$seller_ros;				
					echo "<br> seller_up_country ".$seller_up_country;
					
					/* echo "Buyer Ros=".$buyer_ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";   */  
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					/* echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	 */
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							} 	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								
								if($buyer_region_code=="jnk" || $buyer_region=="nthest" || $seller_region=="nthest") 
								{
									$results=0;	 	
								}
								
							}
							
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest" || $seller_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					}
					
									
					
						
					
					  
					$weight;
					
					
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					$base_price=$price->Base;
					$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					
					
					$findWeight = ceil($findWeight); 
					if($findWeight > 1)
					{ 
						
						for($i=1; $i <= $findWeight; $i++)   
						{
							//$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base+$base_price;
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base;
							 
							
						
						}      
					} 
					else    
					{ 
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}
					
					/* additional charges for ne states for the location */
					
					$buyer_state = $this->get_state_pincode($buyer_zipcode);
					$seller_state = $this->get_state_pincode($seller_zipcode);
					
					if($zone=="F") {
						$nespecialcharge=0;
						if(in_array($buyer_state,$neststatecodearray) || in_array($seller_state,$neststatecodearray)){
							/* add a surcharge of INR 50 per 500 gram */
							for($i=0; $i < $findWeight; $i++)   
							{
								$nespecialcharge += 50;		
							}
						}
						$charge = $charge + $nespecialcharge;
					}
					
					echo "<br> buyer state ".$buyer_state ;
					echo "<br> seller state " .$seller_state;
					echo "<br> buyer region " .$buyer_region;
					echo "<br> seller region " .$seller_region;
					echo "<br> product weight " .$weight;
					echo "<br>";

					/*
					if($findWeight > 1)
					{ 
						
						for($i=1; $i < $findWeight; $i++){
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$base_price+$ros_base+$up_base;
						}    
					} 
					else    
					{
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					*/  
					
					$charge;
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				      
					$product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					
					$other_amount=0;
					
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					$gst=$fuel_amount+$other_amount;
					
					$helper = $objectManager->create('Baljit\Buynow\Helper\Data');
					$shipping_gst = $helper->getConfig('buynows/general/shipping_gst');
					if(empty($shipping_gst))
					{
						$shipping_gst=0;
					}
					 
					
					//$gst_amount=($net_total*18)/100; 
					$gst_amount=($net_total*$shipping_gst)/100; 
					      
					 
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					

					if($ex_zone=='0') 
					{
						$show_zone=$zone;
					}	 
					else 
					{	 
						$show_zone=$zone.'+'.$ex_zone;
					}		
					 
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'gst_rate'=>$shipping_gst,'grand_total'=>$grand_total,'zone'=>$show_zone);   
					
					$array = array('success' => 1, 'result' => $result_array); 
					
					$admin_comm = $helper->getConfig('buynows/general/logistic_pana_tax');
					if(empty($admin_comm))
					{
						$admin_comm=0;
					} 
					
					
					$grand_total=$grand_total+(($grand_total*$admin_comm)/100);    
					$grand_total=number_format($grand_total, 2, '.', '');
			// return $result_array;
			echo json_encode($array);
			// return $grand_total;	     
			
		
		}
		
		
		public function ecom_price_by_product_array($buyer_zipcode,$product_id,$qty)
		{  
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			
			
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');  
			 
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			
			//echo $buyer_zipcode.' '.$product_id.' '.$qty;		
			
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			
			
				
			 
			
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			
														
			
					$weight=$product->getWeight();
					$ecom_weight=$weight;
			
					 
					
					//$weight=$product->getWeight();
					/*
					echo "Weight=".$weight;
					echo "<br>";
					echo "Ecom Length=".$product->getEcomLength();
					echo "<br>"; 
					echo "Ecom Breadth=".$product->getEcomBreadth();
					echo "<br>";
					echo "Ecom Height=". $product->getEcomHeight();
					echo "<br>"; 
					*/
					
					$ecom_length=$product->getEcomLength();       
					$ecom_breadth=$product->getEcomBreadth();       
					$ecom_height=$product->getEcomHeight();   	
					
					
					$vol_weight=($product->getEcomLength()*$product->getEcomBreadth()*$product->getEcomHeight())/5000;
					 
					if($vol_weight > $weight)  
					{
						$weight=$vol_weight;   
					}
					$weight;
					  		 		
					  
					
					
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					$charge_weight=$weight; 
					$actual_prd_id=$product->getProductId();
					 
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					
					$select = $connection->select()
					->from('marketplace_product') 
					->where('mageproduct_id = ?', $product_id); 
		
					$result = $connection->fetchAll($select);  
					
					
					$count=count($result);
					
					if($count==0)
					{
						$seller_id='20';		  
					}	
					foreach($result as $row)
					{
						$seller_id=$row['seller_id'];			
					}	
					
					/* if($seller_id==20)
					{
						$seller_zipcode='110001';		
					} 
					else 
					{
						$seller_zipcode='122002'; 				
						//$seller_zipcode='190023'; // Srinagar				
					}	 */	
					 
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					
					$sellerArray=$custom_helpers->get_seller_details($seller_id);
					if(!empty($sellerArray))
					{
						$seller_zipcode=$sellerArray['zipcode'];	
					}	
					else 
					{
						$seller_zipcode='122002'; 	
					}
					 
					/* echo "<pre>";
						print_r($sellerArray);
					echo "</pre>";
					die; */
					
					$seller_zipcode;  
					
					  
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					
					
					
					
					/* echo "Buyer Ros=".$buyer_ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";   */  
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					/* echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	 */
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							}	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								
								if($buyer_region_code=="jnk" || $buyer_region=="nthest" || $seller_region=="nthest")
								{
									$results=0;	 	
								}
								
							}
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest" || $seller_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					}
					
									
					
						
					
					  
					$weight;
					
					
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					//print_r($price);
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					$base_price=$price->Base;
					$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					
					$additional_price_new=0;
					$ros_additional_new=0;
					$up_additional_new=0;
					$base_price_new=0; 
					$ros_base_new=0;
					$up_base_new=0;
					
					
					$findWeight = ceil($findWeight); 
					if($findWeight > 1)
					{ 
						
						for($i=1; $i <= $findWeight; $i++)   
						{
							//$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base+$base_price;
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base;
							 
							
						
						}      
					} 
					else    
					{ 
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					
					
					
					
					
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				      
					 $product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					
					$other_amount=0; 
					
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					//$gst=$fuel_amount+$other_amount;
					
					$helper = $objectManager->create('Baljit\Buynow\Helper\Data');
					$shipping_gst = $helper->getConfig('buynows/general/shipping_gst');
					if(empty($shipping_gst))
					{
						$shipping_gst=0;
					}
					 
					
					//$gst_amount=($net_total*18)/100; 
					$gst_amount=($net_total*$shipping_gst)/100; 
					      
					 
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					
						
					if($ex_zone=='0')
					{
						$show_zone=$zone;
					}	 
					else 
					{	 
						$show_zone=$zone.'+'.$ex_zone;
					}		
					
					

					$logistic_pana_tax = $helper->getConfig('buynows/general/logistic_pana_tax');
					if(empty($logistic_pana_tax)) 
					{
						$logistic_pana_tax=0;
					}
					
					$logistic_pana_tax;   

					
					$grand_total=$grand_total+(($grand_total*$logistic_pana_tax)/100);   
					$grand_total=number_format($grand_total, 2, '.', '');	

					
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'gst_rate'=>$shipping_gst,'grand_total'=>$grand_total,'zone'=>$show_zone,'ecom_length'=>$ecom_length,'ecom_breadth'=>$ecom_breadth,'ecom_height'=>$ecom_height,'vol_weight'=>$vol_weight,'ecom_weight'=>$ecom_weight,'charge_weight'=>$charge_weight);    
					
					return $result_array;   
					
					 		
			    
			
		}

		
		
		
		

		public function ecom_price_by_product_test($buyer_zipcode,$product_id,$qty)
		{  
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			
			
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');  
			 
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			
			//echo $buyer_zipcode.' '.$product_id.' '.$qty;		
			
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			
			
				
			 
			
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			
														
			
					$weight=$product->getProductWeight();
			
			
					 
					
					$weight=$product->getProductWeight();
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					 
					$actual_prd_id=$product->getProductId();
					 
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					
					$select = $connection->select()
					->from('marketplace_product') 
					->where('mageproduct_id = ?', $product_id); 
		
					$result = $connection->fetchAll($select);  
					
					
					$count=count($result);
					
					if($count==0)
					{
						$seller_id='20';		  
					}	
					foreach($result as $row)
					{
						$seller_id=$row['seller_id'];			
					}	
					
					/* if($seller_id==20)
					{
						$seller_zipcode='110001';		
					} 
					else 
					{
						$seller_zipcode='122002'; 				
						//$seller_zipcode='190023'; // Srinagar				
					}	 */	
					 
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					
					$sellerArray=$custom_helpers->get_seller_details($seller_id);
					if(!empty($sellerArray))
					{
						$seller_zipcode=$sellerArray['zipcode'];	
					}	
					else 
					{
						$seller_zipcode='122002'; 	
					}
					 
					/* echo "<pre>";
						print_r($sellerArray);
					echo "</pre>";
					die; */
					
					
					
					  
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					
					
					
					
					/* echo "Buyer Ros=".$buyer_ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";   */  
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					/* echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	 */
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							}	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								
								if($buyer_region_code=="jnk" || $buyer_region=="nthest")
								{
									$results=0;	 	
								}
								
							}
							
							
							
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					}
					
									
					
						
					
					  
					$weight;
					
					
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					$base_price=$price->Base;
					$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					if($findWeight > 1){ 
						
						for($i=1; $i < $findWeight; $i++){
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$base_price+$ros_base+$up_base;
						}    
					} 
					else    
					{
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					  
					
					$charge;
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				      
					echo $product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					$gst=$fuel_amount+$other_amount;
					
					$gst_amount=($net_total*18)/100; 
					      
					 
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					

					if($ex_zone=='0')
					{
						$show_zone=$zone;
					}	 
					else 
					{	 
						$show_zone=$zone.'+'.$ex_zone;
					}		
					 
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$show_zone);  

					$grand_total=number_format($grand_total, 2, '.', '');
							
			return $grand_total;	    
			
		}	
		
		
		public function ecom_price_break($buyer_zipcode,$seller_code) 
		{ 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			 
			
			//$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
			$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida');
			
			$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
			$jnkarray=array('jnk','jku');  
			 
			 
			
			$buyer_city=$this->get_city_pincode($buyer_zipcode);
			
			if($buyer_city=='0')
			{
				return 0;
				die;	  
			}	  
			
			$buyer_region=$this->get_region_pincode($buyer_zipcode);
			$buyer_region_code=$this->get_region_code_pincode($buyer_zipcode);
			
			$buyer_ros=$this->ros_checker($buyer_zipcode);	
			$buyer_up_country=$this->up_country_checker($buyer_zipcode);	
			
			
				
			
				
			 
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			 
			// get quote items collection
			$itemsCollection = $cart->getQuote()->getItemsCollection();
			 
			// get array of all items what can be display directly
			$itemsVisible = $cart->getQuote()->getAllVisibleItems();
			 
			// get quote items array
			$items = $cart->getQuote()->getAllItems();
			
			$itemcount=count($cart->getQuote()->getAllItems());
			
			$sellerarray=array();
			
			
			$base_rate=30;
			$additional_rate=30;
			
			$product_total_charge=0;	
			
			
			
			$product_total_charge=0;
			$fuel_amount=0;
			$sub_total=0;
			$other_amount=0;
			$net_total=0;
			$gst_amount=0;
			$grand_total=0;
			$zone=0;
			
			$ros=0;  
			$up_country=0; 
			if($itemcount!=0) 
			{ 
				foreach($items as $item) 
				{
					/* echo 'ID: '.$item->getProductId().'<br />'; 
					echo 'Name: '.$item->getName().'<br />';
					echo 'Sku: '.$item->getSku().'<br />';
					echo 'Quantity: '.$item->getQty().'<br />';
					echo 'Price: '.$item->getPrice().'<br />';
					echo "<br />";     */   
					 
					$qty=$item->getQty();
					$weight=$item->getWeight();
					
					$prod_weight=$weight*$qty;
					$weight=$prod_weight;
					 
					$actual_prd_id=$item->getProductId();
					
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					
					$select = $connection->select()
						->from('marketplace_product') 
						->where('mageproduct_id = ?', $actual_prd_id);
					$result = $connection->fetchAll($select);
					
					
					$count=count($result);
					
					if($count==0)
					{
						$seller_id='1';		
					}	
					foreach($result as $row)
					{
						$seller_id=$row['seller_id'];			
					}	
					
					if($seller_id==20)
					{
						$seller_zipcode=$seller_code;		
					}
					else 
					{
						$seller_zipcode=$seller_code;  				
						//$seller_zipcode='190023'; // Srinagar				
					}		  
					
					$seller_zipcode;  
					
					 
					
					
					$results=0;
					
					$seller_city=$this->get_city_pincode($seller_zipcode); 
					
					$seller_region=$this->get_region_pincode($seller_zipcode); 
					$seller_region_code=$this->get_region_code_pincode($seller_zipcode); 
					
					
					$seller_ros=$this->ros_checker($seller_zipcode);	
					$seller_up_country=$this->up_country_checker($seller_zipcode);
					
					
					
					
					echo "Buyer Ros=".$ros; 
					echo "<br>";
					echo "Seller Ros=".$seller_ros;
					echo "<br>";
					echo "Buyer City=".$buyer_city;	
					echo "<br>";
					echo "Seller City=".$seller_city;	  
					echo "<br>";    
					
					if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
					{
						$zone="A";		
						$results=1;	 
					} 
					else if($buyer_city==$seller_city) // Inter City
					{
						$zone="A";		 
						$results=1;	 
					}   
					else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
					{
						$zone="C";		 
						$results=1;	  
					}
					
					
					echo $buyer_region_code.' '.$seller_region_code;
					echo "<br>";
					echo $ros;
					echo "<br>";
					echo $up_country;	
					
					
					if($buyer_ros==1 || $seller_ros==1)
					{
						$ros=1;	 
					}	
					
					if($buyer_up_country==1 || $seller_up_country==1)
					{
						$up_country=1;
					}	
					 
					// Inter Region
					
					if($results==0)
					{
						if($buyer_region==$seller_region) 
						{
							
							
							if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray)))
							{
								$zone="B"; 	 	 
								$results=1;	 	 
							}
							else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray)))
							{
								$zone="E";	 	 
								$results=1;	  	 
							}	
							else
							{		
								$zone="B";	 	 
								$results=1;	
							}  
							
							//$zone="B";	 	 
							//$results=1;	
							
							
							
						}
						else
						{
							
							if($buyer_region_code!=$seller_region_code)
							{
								$zone="D";  	
								$results=1; 
								
								if($buyer_region_code=="jnk" || $buyer_region=="nthest")
								{
									$results=0;	 	
								}
								
							}
							
							
							
							
							
							if($results==0)
							{
								if($buyer_region_code=="jnk")
								{
									$zone="E";	 	  
									$results=1;	 		
								}
								else if($buyer_region=="nthest")
								{
									$zone="F";	  	  
									$results=1;	 		
								}
								else
								{		
									if($ros!=0)
									{
										$zone="H";	   	 
										$results=1;		  
									} 
									else if($up_country!=0)
									{
										$zone="G"; 	  	 
										$results=1;		
									} 
									
								}
							}
							
						}
					}
					
									
					
						
					
					  
					echo "Weight=".$weight." kg";
					echo "<br>";
					$seller_city;
					
					
					
					$price='';
					
					$price=json_decode($this->get_freight_charge($zone));
					
					$ros_base=0;		
					$ros_additional=0;	
					
					$up_base=0;		
					$up_additional=0;	
					$ex_zone=0;
					 
					if($ros!=0)
					{
						$ros_base=10; 		
						$ros_additional=10;		
						$ex_zone='H';
					} 

					if($up_country!=0)
					{
						$up_base=5;		
						$up_additional=5;	 	
						$ex_zone='G';	
					}    
							
					
					
					$charge=0;
					$base_price=0;
					$additional_price=0; 
					$product_total_charge=0; 
					
					echo "Base Price=".$base_price=$price->Base;
					echo "<br>";
					echo "Additional Price=".$additional_price=$price->Additional;
					 
					$weight=$weight*1000;
					
					$findWeight = $weight/500; //total weight divided by your weight
					if($findWeight > 1){ 
						
						for($i=1; $i < $findWeight; $i++){
							$charge = $charge + $additional_price+$ros_additional+$up_additional+$base_price+$ros_base+$up_base;
						}    
					} 
					else    
					{
						//$charge = $base_price+$ros_base;
						$charge = $base_price+$ros_base+$up_base; 
					}   
					  
					
					$charge;
					
					$product_total_charge=$product_total_charge + $charge;
					
					
					 
					
					
					 
				}      
					$product_total_charge;
					$fuel_amount=($product_total_charge * 28)/100; 
					$sub_total=$product_total_charge + $fuel_amount;
					
					$other_amount=($sub_total*2)/100;
					
					
					if($other_amount < 35)
					{
						$other_amount=35;	
					}	 
					$other_amount;
					$net_total=$sub_total + $other_amount;	
					//$gst_amount=($net_total*18)/100;
					
					$gst=$fuel_amount+$other_amount;
					
					$gst_amount=($net_total*18)/100; 
					      
					
					$grand_total=$net_total+$gst_amount;
					//$grand_total=number_format($grand_total, 2, '.', '');
					

					if($ex_zone=='0')
					{
						$show_zone=$zone;
					}	
					else 
					{	
						$show_zone=$zone.'+'.$ex_zone;
					}		
					 
					$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$show_zone);  
					
					$array = array('success' => 1, 'result' => $result_array); 

			} 
			else 
			{
				
				$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'grand_total'=>$grand_total,'zone'=>$zone);
				
				$array = array('success' => 0, 'result' => $result_array); 
			}     
			return $grand_total;
			
		}      
		
		
		
		
		public function get_city_pincode($pincode)
		{ 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
				->from('pincode_details') 
				->where('pincode = ?', $pincode);
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result))
			{
				return strtolower($result[0]['city_name']);
			}	 
			else
			{
				return 0;
			}
			
			
			 
		}
		
		public function get_region_pincode($pincode)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
				->from('pincode_details') 
				->where('pincode = ?', $pincode);
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result))
			{	
				return strtolower($result[0]['region']);   
			}
			else
			{
				return 0; 
			}
			
		
			
			    
		} 
		
		public function get_region_code_pincode($pincode)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$sql = "Select region_code FROM pincode_details where pincode='".$pincode."'";
			$result = $connection->fetchAll($sql);
			
			
			if(!empty($result))
			{	
				return strtolower($result[0]['region_code']); 
			}
			else
			{
				return 0;   
			}
			
			
			 
			   
		} 
		
		public function get_state_pincode($pincode)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
				->from('pincode_details') 
				->where('pincode = ?', $pincode);
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result))
			{	
				return strtolower($result[0]['state']);   
			}
			else
			{
				return '110001';
			}
			   
		} 
		
		
		
		public function ros_checker($pincode)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			
			$select = $connection->select()
				->from('pincode_details_ros') 
				->where('pincode = ?', $pincode);
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result))
			{
				return 1;		
			}
			else   
			{
				return 0;
			}
			 
			   
		}

		public function up_country_checker($pincode)
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
				->from('pincode_details_upcountry') 
				->where('pincode = ?', $pincode);
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result)) 
			{
				return 1;		
			}
			else   
			{
				return 0;
			}
			 
			   
		}
		
		


		public function get_freight_charge($zone)
		{
			if($zone=="A") // Inter City
			{
				$array=array('Base'=>24,'Additional'=>24);	
			}
			else if($zone=="B") // Inter Region  
			{
				$array=array('Base'=>30,'Additional'=>30);	
			} 
			else if($zone=="C") // Metro  
			{
				$array=array('Base'=>37,'Additional'=>37);	
			}		
			else if($zone=="E") // JNK  
			{
				$array=array('Base'=>45,'Additional'=>45);	
			}
			else if($zone=="F") // NTHEST  
			{
				$array=array('Base'=>45,'Additional'=>45);	 
			}
			else if($zone=="D") // Rest Of India  
			{
				$array=array('Base'=>39,'Additional'=>39);	 
			}	
			else if($zone=="G") // Rest Of India  
			{
				$array=array('Base'=>5,'Additional'=>5);	  
			}	 
				
			   
			
			return json_encode($array); 
			
			 
		}
		
		
		 
		/******Api Funstions**************/ 
		 
		public function checkToken($token) 
		{
		
			$tableName = "mobile_token_auth";
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			if(!empty($token))
			{
				$select = $connection->select('id')
					->from($tableName) 
					->where('token = ?',$token);
				$id = $connection->fetchOne($select);
				if(!empty($id))
				{
					return 1;
				}
			}
			return 0;
			// return $_SESSION['app_accesstoken'];


			/* old useless  functionality of  token check removed @ritesh4122020*/
			// $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			// $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			// $connection = $resource->getConnection();
			
			// $select = $connection->select()
			// 	->from('token_details') 
			// 	->where('token = ?', $token);
			// $result = $connection->fetchAll($select);
			
			// if(!empty($result))
			// {
			// 	return 1;
			// }
			// else
			// {
			// 	return 0;
			// }
			
		}

		public function checkUserSeller($cust_id) 
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$sql = "Select * FROM marketplace_userdata where seller_id='".$cust_id."' limit 0,1";
			$result = $connection->fetchAll($sql);
			
			
			
			
			$is_seller=0;
			
			if(!empty($result)) {
				$is_seller=$result[0]['is_seller'];
			}     
			
			return $is_seller;
			
			
		}
		
		public function checkUserEmailConfirmation($cust_id) 
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$sql = "Select * FROM customer_entity where entity_id='".$cust_id."' and confirmation is NULL";
			$result = $connection->fetchAll($sql);
			
			if(!empty($result))			
			{
				return 1;		
			}
			else
			{
				return 0;
			} 
			
			
		}
		
		public function companyUserApproved($cust_id) 
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			
			
			$select = $connection->select()
                  ->from('customer_grid_flat') 
                  ->where('entity_id = ?', $cust_id);
                 
		
			$result = $connection->fetchAll($select);
			
			
			
			$approved=$result[0]['attribute_approved'];
			if($approved!=1)
			{
				return 0;	
			}	
			else
			{
				return 1;
			}
			
			
			
		}
		
		public function sellerUserApproved($cust_id)  
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
		
			$sql = "Select * FROM marketplace_userdata where seller_id='".$cust_id."'  limit 0,1";
			$result = $connection->fetchAll($sql);
			$count=count($result);
			$is_seller=0;
			if($count > 0)
			{ 
				
				if(!empty($result)) {
					$is_seller=$result[0]['is_seller'];
				}
			}
			
			if($is_seller==1)
			{
				return 1;	
			}
			else
			{
				return 0;
			}
			
		}
		
		
		public function getUserDetail($cust_id,$field) 
		{
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$fields = array($field);
			
			$select = $connection->select()
                  ->from('customer_entity',$fields) 
				  ->where('entity_id = ?', $cust_id);
                
		
			$result = $connection->fetchAll($select);
			
			
			return $result[0][$field];
		}
		
		public function get_product_status($product_id)
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$select = $connection->select()
                  ->from('catalog_product_entity_int') 
                  ->where('entity_id = ?', $product_id)
                  ->where('attribute_id = ?', '97')
                  ->where('store_id = ?', '1');
                 
		
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result)) 
			{	
				return $result[0]['value'];
			} 
			
		}	
		
		   
		 
	   
}
