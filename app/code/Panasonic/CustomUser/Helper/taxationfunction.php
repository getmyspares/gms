<?php

namespace Panasonic\CustomUser\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Taxationfunction extends AbstractHelper
{
       
	
	public function tax_parameter()  
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		
		$helper = $objectManager->create('Baljit\Buynow\Helper\Data');
		
		$admin_comm = $helper->getConfig('buynows/general/button_titlee');
		if(empty($admin_comm))
		{
			$admin_comm=0;
		}
		
		
		$admin_comm_tax = $helper->getConfig('buynows/general/admin_comm_tax');
		if(empty($admin_comm_tax))
		{
			$admin_comm_tax=0;
		}
		
		
		$admin_comm_tds = $helper->getConfig('buynows/general/admin_comm_tds');
		if(empty($admin_comm_tds))
		{
			$admin_comm_tds=0;
		}
		
		
	    $gst_tcs = $helper->getConfig('buynows/general/gst_tcs');
	    if(empty($gst_tcs))
	    {
			$gst_tcs=0;
	    } 
		
		
		$shipping_gst = $helper->getConfig('buynows/general/shipping_gst');
		if(empty($shipping_gst))
		{
			$shipping_gst=0;
		}
		
		
		$shipping_tds = $helper->getConfig('buynows/general/shipping_tds');
		if(empty($shipping_tds))
		{
			$shipping_tds=0;
		}
		
		
		$razorpay_tds = $helper->getConfig('buynows/general/razorpay_tds');
		if(empty($razorpay_tds))
		{
			$razorpay_tds=0;
		}
		
		
		
		$nodal_tax = $helper->getConfig('buynows/general/nodal_tax');
		if(empty($nodal_tax))
		{
			$nodal_tax=0;
		}
		
		$logistic_pana_comm = $helper->getConfig('buynows/general/logistic_pana_tax');
		if(empty($logistic_pana_comm))
		{
			$logistic_pana_comm=0;
		}
		
		
		$admin_comm;
		$admin_comm_tax;
		$admin_comm_tds;
		$gst_tcs;
		$shipping_gst;
		$shipping_tds;
		$razorpay_tds;
		$nodal_tax; 
		$logistic_pana_comm; 
		
		
		
		
		
		$tax=array(
			'igst'=>12,	
			'cgst'=>6,	
			'sgst'=>6,	
			'admin_comm'=>$admin_comm,	
			'admin_comm_tax'=>$admin_comm_tax,	
			'admin_comm_tds'=>$admin_comm_tds,
			'gst_tcs'=>$gst_tcs,	  
			'shipping_gst'=>$shipping_gst,	 
			'shipping_tds'=>$shipping_tds,	
			'razorpay_tds'=>$razorpay_tds,  
			'nodal_tax'=>$nodal_tax,  
			'logistic_pana_comm'=>$logistic_pana_comm,  
			'cod_tax'=>5,   
			'cod_charge'=>50    
		);	  
	
		 
		return json_encode($tax); 
		
	
	}	
	
	public function tax_order_parameter($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order_id=$order_inc_id;
		
		
		$select = $connection->select()
                  ->from('order_tax_list') 
                  ->where('order_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{	
			return $result[0]['tax_list'];
		}
		else
		{
			exit();
		}		
	}  	
	
	
	
	public function insert_tax_parameter($order_increment_id)  
	{
		$taxarray=$this->tax_parameter(); 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId(); 
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			$sellerarray[]=$custom_helpers->get_product_seller($order_id,$product_id);
			
		}        	
		 
		
		$resultarray=array_values(array_unique($sellerarray));
		
		if(!empty($resultarray))
		{
			$seller_id=$resultarray[0];	
		}	
		else  
		{
			$seller_id='29';   
		}
		
		$seller_id;
		
		
		$select = $connection->select()
				  ->from('marketplace_saleperpartner') 
				  ->where('seller_id = ?', $seller_id);
		
		$result = $connection->fetchAll($select);
		
		if(empty($result))
		{
			$seller_new_amount='0.0000';	
		}
		else 
		{
			$seller_new_amount=$result[0]['commission_rate'];	
						
		}  
		
		
		if($seller_new_amount!='0.0000')
		{ 
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$seller_new_amount;
			$taxarray=json_encode($taxarray);
		}
		 
		
		$sql = "insert into order_tax_list (order_id,tax_list) values ('".$order_increment_id."','".$taxarray."')";
		$connection->query($sql); 
	}
	
	
	public function get_order_seller_array($order_inc_id)
	{  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		
		
		if(!empty($order->getAllItems()))
		{	
			foreach ($order->getAllItems() as $item)
			{
				$product_id=$item->getId();
				$sellerarray[]=$this->tax_get_product_seller($order_id,$product_id);
				
			}  	
			$resultarray=array_values(array_unique($sellerarray));
			return $resultarray;
			
		}
		else
		{
			return 0;
		}
		
	}
	
	public function tax_get_product_seller($order_id,$product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		 
		$product_idd=$product_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
		if(!empty($order->getAllItems()))
		{	
			foreach ($order->getAllItems() as $item)
			{
				$item_id=$item->getId();
				
				if($item_id==$product_idd)
				{
					$product_id=$item->getproductId();			
				}	
			}  	 
		}
		
		
		$seller_id=0;
		
		$select = $connection->select()
				  ->from('marketplace_product') 
				  ->where('mageproduct_id = ?', $product_id);
		
		
		/*
		$select = $connection->select()
				  ->from('marketplace_saleslist') 
				  ->where('order_id = ?', $order_id)
				  ->where('order_item_id = ?', $product_id);
				  
		*/		  
	   
		$results = $connection->fetchAll($select);
		
		
		if(!empty($results))
		{
			return $results[0]['seller_id'];			
		}	
		else
		{
			return '29'; 
		}
		
	}
	
	
	public function tax_get_order_shipping_date($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
			
		$select = $connection->select()
                  ->from('ecomexpress_awb') 
                  ->where('orderid = ?', $order_id);	
			
			
	 
		$results = $connection->fetchAll($select);
		
		
		if(!empty($results)) {
			return $results[0]['updated_at'];
		}
		else
		{
			return 0;
		} 
		
	}
	
	
	public function tax_get_order_awb($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = $connection->select()
                  ->from('ecomexpress_awb') 
                  ->where('orderid = ?', $order_id);	
			
			
	 
		$results = $connection->fetchAll($select);
		 
		
		if(!empty($results)) {
			return $results[0]['awb'];
		}
		else 
		{
			return 0;
		} 
		
		
		   
		
	}
	
	//baljit modify order AWB
	
	public function tax_get_order_awbno($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$select = $connection->select() 
                  ->from('ecomexpress_awb') 
                  ->where('orderid = ?', $order_id);	
			
			
	 
		$results = $connection->fetchAll($select);
		 
		
		if(!empty($results)) {
			return $results[0]['awb'];
		}
		else 
		{
			return 0;
		} 
		
		
		   
		
	}
	
	
	public function tax_get_order_return_awbno($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$select = $connection->select() 
                  ->from('rev_awb') 
                  ->where('order_id = ?', $order_id);	
			
			
	 
		$results = $connection->fetchAll($select);
		 
		
		if(!empty($results)) {
			return $results[0]['awb'];
		}
		else 
		{
			return 0;
		} 
		
		
		   
		
	}
	
	
	
	public function tax_get_seller_array($seller_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$customer = $objectManager->get(
			'Magento\Customer\Model\Customer'
		)->load($seller_id);
		if ($customer) 
		{
								
			$returnArray = [];
			$returnArray['seller_name'] = $customer->getName();
			$returnArray['seller_gst'] = $customer->getGstNumber();
			$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
			$returnArray['seller_comp_address'] = $customer->getcompanyAdd();
			$returnArray['seller_comp_mobile'] = $customer->getmobile();
			$returnArray['id'] = $customer->getId();
			
			$seller_zipcode=$this->get_seller_zipcode($customer->getId());
			if($seller_zipcode!='0')
			{  
				$returnArray['seller_zipcode']=$seller_zipcode; 	
			}	
			else
			{
				$returnArray['seller_zipcode']='110001';	
			}  
			
			$select = $connection->select()
				->from('pincode_api') 
				->where('pincode = ?', $seller_zipcode);
			$result = $connection->fetchAll($select);
			
			
			if(!empty($result))
			{
				$returnArray['seller_state']=strtolower($result[0]['state']); 
				$returnArray['seller_city']=strtolower($result[0]['city']);
			}	 
			else
			{
				$returnArray['seller_state']='';
				$returnArray['seller_city']='';
			}
			
			
			
			
		}
		
		return json_encode($returnArray);
		
	}
	
	
	public function tax_admin_commission($order_total_amount,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$admin_comm_rate=$tax_rates_array->admin_comm;
		
		$admin_comm=($order_total_amount*$admin_comm_rate)/100;
		
		return $admin_comm;
		
	}
	
	public function tax_admin_commission_tax($order_total_amount,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$admin_comm_rate=$tax_rates_array->admin_comm_tax;
		
		$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id); 
		$admin_comm_tax=($admin_comm*$admin_comm_rate)/100;
		return $admin_comm_tax;
		
	}   
	
	public function tax_admin_commission_tds($order_total_amount,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
		 
		$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id); 
		$admin_comm_tax=($admin_comm*$admin_comm_tds_rate)/100;
		return $admin_comm_tax;
		 
	}   
	
	public function tax_logistic_tax($order_shipping_amount,$order_inc_id) 
	{
		//echo $order_shipping_amount;
		//echo "<br>";
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		//print_r($tax_rates_array);
		//	die;
		$shipping_gst_rate=$tax_rates_array->shipping_gst;
		
		$x=($order_shipping_amount*100)/(100+$shipping_gst_rate);
		$order_shipping_amount=$x; 
		
		$logistic_tax=($order_shipping_amount*$shipping_gst_rate)/100;
		
		//$total_amount=$order_shipping_amount - $logistic_tax;
		
		$total_amount= number_format($logistic_tax, 2, '.', '');
		
		return $total_amount;    
		
	} 
	
	public function tax_logistic_gst($order_shipping_amount,$order_inc_id) 
	{
		//echo $order_shipping_amount;
		//echo "<br>";
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$shipping_gst_rate=$tax_rates_array->shipping_gst;
		
		$x=($order_shipping_amount*100)/(100+$shipping_gst_rate);
		$order_shipping_amount=$x; 
		
		//$logistic_tax=($order_shipping_amount*$shipping_gst_rate)/100;
		
		//$total_amount=$order_shipping_amount - $logistic_tax;
		
		$total_amount= number_format($order_shipping_amount, 2, '.', '');
		
		return $total_amount;      
		
	} 
	
	
	public function tax_logistic_tds($order_shipping_amount,$order_inc_id) 
	{
	
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$shipping_tds_rate=$tax_rates_array->shipping_tds;
		
		$logistic_tax=($order_shipping_amount*$shipping_tds_rate)/100;
		
		$logistic_tax= number_format($logistic_tax, 2, '.', '');
		
		return $logistic_tax;  
		
	} 
	
	
	public function tax_razorpay_expense($magento_order_id)
	{
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('custom_order_razorpay') 
                  ->where('order_id = ?', $magento_order_id);
		
		$results = $connection->fetchAll($select);
		 
		 
		
		$fee=0;
		$tax=0;
		$amount=0; 
		
		$payment=0;
		$card=0; 
		$network=0;
		
		if(!empty($results))
		{
			$razorpay=$results[0]['razorpay'];
			if($razorpay!='')
			{	
				$response=$results[0]['response_details'];
				$result=json_decode($response);
				/* echo "<pre>";
				print_r($result);
				echo "</pre>"; */
				
				if($response!='') 
				{	 
					 
					
					
					if(!empty($result->fee))
					{
						$fee=$result->fee; 
					}

					if(!empty($result->tax))
					{
						$tax=$result->tax;
					}	

					if(!empty($result->payment))
					{
						$payment=$result->payment;
					}	

					if(!empty($result->card))
					{
						$card=$result->card;
					}	

					if(!empty($result->network))
					{
						$network=$result->network;
					}		
					
					
					
					
				}
				$amount=$fee+$tax; 
				
				
				$array=array('type'=>'online','fee'=>$fee,'tax'=>$tax,'amount'=>$amount,'payment'=>$payment,'card'=>$card,'network'=>$network);
				return json_encode($array);  
			} 
			else 
			{
				$array=array('type'=>'online','fee'=>$fee,'tax'=>$tax,'amount'=>$amount,'payment'=>$payment,'card'=>$card,'network'=>$network);
				return json_encode($array); 
				
			}
			
		} 
		else
		{
			$array=array('type'=>'cod','fee'=>$fee,'tax'=>$tax,'amount'=>$amount);
			return json_encode($array); 
		}  
		
		 
	}
	
	public function tax_razorpay_tds($razor_pay,$order_inc_id) 
	{
		
		$payment_fee=$razor_pay->fee;
		$payment_tax=$razor_pay->tax;
		$payment_amount=$razor_pay->amount;
		
		$razorpay_amount=$payment_amount;
		
		 
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$razorpay_tds_rate=$tax_rates_array->razorpay_tds; 
		
		$razorpay_tds=($razorpay_amount*$razorpay_tds_rate)/100;
		
		$razorpay_tds= number_format($razorpay_tds, 2, '.', '');
		
		return $razorpay_tds;  
		
	}  
	
	
	public function get_tax_razorpay_tds($razorpay_amount,$order_inc_id) 
	{
		
		 
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$razorpay_tds_rate=$tax_rates_array->razorpay_tds; 
		
		$razorpay_tds=($razorpay_amount*$razorpay_tds_rate)/100;
		
		$razorpay_tds= number_format($razorpay_tds, 2, '.', '');
		
		return $razorpay_tds;  
		
	}  
	

	public function tax_gst_tcs($total_gst_amount,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		
		/* echo "<pre>";
		print_r($tax_rates_array);
		echo "</pre>";  */ 
		
		$total_gst_amount;
		$gst_tcs_rate=$tax_rates_array->gst_tcs; 
		
		$gst_tcs=($total_gst_amount*$gst_tcs_rate)/100;
		
		$gst_tcs= number_format($gst_tcs, 2, '.', '');
		
		return $gst_tcs; 
		
	}  

	public function tax_gst_tcs_on_total_amount($total_ex_tax,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		
		/* echo "<pre>";
		print_r($tax_rates_array);
		echo "</pre>";  */ 
		
		$total_gst_amount;
		$gst_tcs_rate=$tax_rates_array->gst_tcs; 
		
		$gst_tcs=($total_ex_tax*$gst_tcs_rate)/100;
		
		$gst_tcs= number_format($gst_tcs, 2, '.', '');
		
		return $gst_tcs; 
		
	}  
	
	
	public function tax_get_nodal_tax($nodal_amount,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		
		/* echo "<pre>";
		print_r($tax_rates_array);
		echo "</pre>";  */ 
		
		$total_gst_amount;
		$gst_tcs_rate=$tax_rates_array->nodal_tax; 
		
		$gst_tcs=($nodal_amount*$gst_tcs_rate)/100;
		
		$gst_tcs= number_format($gst_tcs, 2, '.', '');
		
		return $gst_tcs;  
		
	}  
	
	
	public function tax_get_cod_tax($cod_amount,$order_inc_id) 
	{
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		
		/* echo "<pre>";
		print_r($tax_rates_array);
		echo "</pre>";  */ 
		
		$total_gst_amount;
		$gst_tcs_rate=$tax_rates_array->cod_tax; 
		$cod_tax=$gst_tcs_rate/100;
		$cod_tax=1 + $cod_tax;
		
		$actual_tax=$cod_amount/$cod_tax;
		
		return $actual_tax= number_format($actual_tax, 2, '.', '');
		
	}  
	
	
	
	
	
	public function taxation_report($order_inc_id) 
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		$buyer_zipcode=$order->getShippingAddress()->getPostcode();
		
		$buyer_state=$helpers->get_state_pincode($buyer_zipcode);
		
		
				/* echo "<pre>";
					print_r($orderArray);
				echo "</pre>";   */
			
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		
		$tax_amount=$orderArray['tax_amount']; // base price + tax
		
		$gst_tcs=$this->tax_gst_tcs($tax_amount,$order_inc_id); 
		$gst_tcs_on_total_amount=$this->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id);
		 
		$cgst_amount=$orderArray['cgst_amount']; // base price + tax
		$sgst_amount=$orderArray['sgst_amount']; // base price + tax
		$igst_amount=$orderArray['igst_amount']; // base price + tax
		
		if($igst_amount!=0  && $cgst_amount==0 && $sgst_amount==0)
		{
			$igst_amount=$tax_amount;		
			$cgst_amount=0;		
			$sgst_amount=0;		
		}	
		
		if($igst_amount==0  && $cgst_amount!=0 && $sgst_amount!=0)
		{
			$igst_amount=0;
			$cgst_amount=$tax_amount/2;		
			$sgst_amount=$tax_amount/2;		
		}	
		
		 
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		$sellerarray=$this->get_order_seller_array($order_inc_id);
		
		$count=count($sellerarray); 
		
		
		
		$tax_rates=$this->tax_parameter();
		$tax_rates_array=json_decode($this->tax_parameter());
		/* echo "<pre>";
			print_r($tax_rates_array);
		echo "</pre>"; */
		
		
		$payment_type='';
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0; 
		$payble_to_razorpay=0; 
		
		$razor_pay=json_decode($this->tax_razorpay_expense($order_inc_id));
		
		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		if(!empty($razor_pay))  
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			$razorpay_tds=$this->tax_razorpay_tds($razor_pay,$order_inc_id); 
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			//$payble_to_razorpay=$payment_fee-$razorpay_tds;
			
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		}
		else 
		{
			$payment_type='COD';
			$payble_to_razorpay='0';
			$seller_comm = $seller_comm -($noddle);
		}
		
		
		
		
		
		
		//if($count==1)	 
		//{
			$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
			
			$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
			foreach($sellerarray as $row)
			{
				$seller_id=$row;		
				$sellerdetails=$this->tax_get_seller_array($seller_id);
				$seller_details=json_decode($sellerdetails);
				/*
				echo "<pre>";
					print_r($seller_details);
				echo "</pre>";
				*/
				   
				$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
				$seller_name=$seller_details->seller_name;
				
				  
				
				$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id);
				$admin_comm_tax=$this->tax_admin_commission_tax($order_total_amount,$order_inc_id);
				$admin_comm_tds=$this->tax_admin_commission_tds($order_total_amount,$order_inc_id);
				
				$total_admin_comm=$admin_comm + $admin_comm_tax;
				
				
				$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
				$logistic_gst=$this->tax_logistic_gst($order_shipping_amount,$order_inc_id); 
				$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
				
				$logistic_pay=$order_shipping_amount-$logistic_tds;
				$logistic_pay=number_format($logistic_pay, 2, '.', '');
				
				 
				$pay_to_seller=$order_total_amount-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
				

				
				
				$igst_amount=0;
				$cgst_amount=0;
				$sgst_amount=0;
				$ugst_amount=0;
				
				if($buyer_state==$seller_state)
				{
					$igst_amount=$tax_amount;			
				}	
				else
				{
					$igst_amount=0;
					$cgst_amount=$tax_amount/2;		
					$sgst_amount=$tax_amount/2;		
				}
				
				
				
				$ship_cgst_amount=0; 
				$ship_sgst_amount=0;
				$ship_igst_amount=0;
				$ship_ugst_amount=0; 
				
				if($buyer_state==$seller_state)
				{
					$ship_igst_amount=$logistic_gst;			
				}	
				else
				{
					$ship_igst_amount=0;
					$ship_cgst_amount=$logistic_gst/2;		
					$ship_sgst_amount=$logistic_gst/2;		
				} 
				
				
				$total_cgst_amount=$cgst_amount + $ship_cgst_amount; 
				$total_sgst_amount=$sgst_amount + $ship_sgst_amount;
				$total_igst_amount=$igst_amount + $ship_igst_amount;
				$total_ugst_amount=$ugst_amount + $ship_ugst_amount;  
				 
				 
				
				
				$mainarray=array(
					'order_inc_id'=>$order_inc_id,
					'invoice_id'=>$invoice_id,
					'order_cr_date'=>$order_cr_date,
					'order_total_inc_tax'=>$order_total_inc_tax,
					'order_total_amount'=>$order_total_amount,
					'seller_id'=>$seller_id,
					'seller_igst'=>$igst_amount,
					'seller_cgst'=>$cgst_amount,
					'seller_sgst'=>$sgst_amount, 
					'seller_ugst'=>$ugst_amount,  
					'ship_igst'=>$ship_igst_amount, 
					'ship_cgst'=>$ship_cgst_amount, 
					'ship_sgst'=>$ship_sgst_amount, 
					'ship_ugst'=>$ship_ugst_amount,
					'total_cgst'=>$total_cgst_amount, 
					'total_sgst'=>$total_sgst_amount, 
					'total_igst'=>$total_igst_amount, 
					'total_ugst'=>$total_ugst_amount,  
					'gst_tcs'=>$gst_tcs,   
					'seller_name'=>$seller_name,
					'admin_comm'=>$admin_comm,
					'admin_comm_tax'=>$admin_comm_tax,
					'admin_comm_tds'=>$admin_comm_tds,
					'total_admin_comm'=>$total_admin_comm,
					'logistic_tax'=>$logistic_tax,
					'logistic_gst'=>$logistic_gst, 
					'order_shipping_amount'=>$order_shipping_amount,
					'logistic_tds'=>$logistic_tds,
					'logistic_pay'=>$logistic_pay,
					'payment_type'=>$payment_type,
					'payment_tax'=>$payment_tax,
					'payment_fee'=>$payment_fee,
					'payment_amount'=>$payment_amount,
					'razorpay_tds'=>$razorpay_tds,
					'payble_to_razorpay'=>$payble_to_razorpay,
					'gst_tcs_on_total_amount'=>$gst_tcs_on_total_amount,
					'seller_comm'=>$pay_to_seller 
				);
				
				
				 
				
				
				
				/* echo '
					<tr>
						<td>'.$order_inc_id.'</td>
						<td>'.$invoice_id.'</td>
						<td>'.$order_total_inc_tax.'</td>
						<td>'.$order_total_amount.'</td>
						<td>'.$seller_id.'</td>
						<td>'.$seller_name.'</td>
						<td>'.$admin_comm.'</td>
						<td>'.$admin_comm_tax.'</td>
						<td>'.$admin_comm_tds.'</td>
						<td>'.$total_admin_comm.'</td>
						<td>'.$logistic_tax.'</td>
						<td>'.$order_shipping_amount.'</td>
						<td>'.$logistic_tds.'</td>
						<td>'.$logistic_pay.'</td>
						<td>'.$payment_type.'</td>
						<td>'.$payment_tax.'</td>
						<td>'.$payment_fee.'</td>
						<td>'.$payment_amount.'</td>
						<td>'.$razorpay_tds.'</td>
						<td>'.$payble_to_razorpay.'</td>
						<td>'.$seller_comm.'</td>
						   
						   
					</tr>';	 */
				
				
				foreach ($order->getAllItems() as $item)
				{
					$product_id=$item->getId();
				}  
			
			//}

			/* echo "<pre>";
				print_r($mainarray);
			echo "</pre>";   */
 
			  
			
			$json_array=json_encode($mainarray);
			
			$sql="insert into taxation_report (order_inc_id,invoice_id,order_cr_date,order_total_inc_tax,order_total_amount,seller_id,igst,cgst,sgst,gst_tcs,seller_name,admin_comm,admin_comm_tax,admin_comm_tds,total_admin_comm,logistic_tax,logistic_gst,order_shipping_amount,logistic_tds,logistic_pay,payment_type,payment_tax,payment_fee,payment_amount,razorpay_tds,payble_to_razorpay,gst_tcs_on_total_amount,seller_comm) values ('".$order_inc_id."','".$invoice_id."','".$order_cr_date."','".$order_total_inc_tax."','".$order_total_amount."','".$seller_id."','".$igst_amount."','".$cgst_amount."','".$sgst_amount."','".$gst_tcs."','".$seller_name."','".$admin_comm."','".$admin_comm_tax."','".$admin_comm_tds."','".$total_admin_comm."','".$logistic_tax."','".$logistic_gst."','".$order_shipping_amount."','".$logistic_tds."','".$logistic_pay."','".$payment_type."','".$payment_tax."','".$payment_fee."','".$payment_amount."','".$razorpay_tds."','".$payble_to_razorpay."','".$gst_tcs_on_total_amount."','".$pay_to_seller."')"; 
			
			//$sql="insert into taxation_report_mini (order_id,invoice_id,cr_date,report_details) values ('".$order_inc_id."','".$invoice_id."','".$order_cr_date."','".$json_array."')";
			
			 
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$connection->query($sql);      
			  
			 
		} 
		
	}   
	
	
	public function taxation_report_mini($order_inc_id) 
	{ 
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		$buyer_zipcode=$order->getShippingAddress()->getPostcode();
		
		$buyer_state=$helpers->get_state_pincode($buyer_zipcode);
		
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$delivery_firstname.' '.$delivery_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		 
		$street = ''; 
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	
		
		/*  echo "<pre>";
				print_r($orderArray);
			echo "</pre>";   
		 */
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		//print_r($gstArray);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		$ship_cgst_amount=$gstArray['shipping_cgst'];   
		$ship_sgst_amount=$gstArray['shipping_sgst'];   
		$ship_igst_amount=$gstArray['shipping_igst'];   
		$ship_ugst_amount=$gstArray['shipping_utgst'];   
		 
		
		
		$total_cgst_amount=$cgst_amount + $ship_cgst_amount; 
		$total_sgst_amount=$sgst_amount + $ship_sgst_amount;
		$total_igst_amount=$igst_amount + $ship_igst_amount;
		$total_ugst_amount=$ugst_amount + $ship_ugst_amount;  
		
		 
		
		$orderDetailArray=$this->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/* echo "<pre>";
			print_r($orderDetailArray);
		echo "</pre>"; */
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		
		
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
				
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		$seller_address=$seller_details->seller_comp_address;
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		$delivery_name=str_replace(array('\'', '"'), '', $delivery_name); 
		$delivery_adress=str_replace(array('\'', '"'), '', $delivery_adress); 
		
		 
		$order_total_inc_tax=$orderDetailArray->total_amount;
		$order_total_amount=$orderDetailArray->subtotal;
		$gst_tcs=$orderDetailArray->gst_tcs_calculate_amount;
		
		$admin_comm=$orderDetailArray->admin_base_comm;
		$admin_comm_tax=$orderDetailArray->admin_comm_tax;
		$admin_comm_tds=$orderDetailArray->admin_comm_tds;
		$total_admin_comm=$orderDetailArray->net_admin_comm;
		
		$order_shipping_amount=$orderDetailArray->shipping_expense;
		$logistic_gst=$orderDetailArray->shipping_base;
		$logistic_tax=$orderDetailArray->shipping_gst;
		$logistic_tds=$orderDetailArray->logistic_tds;
		$due_to_logistic=$orderDetailArray->due_to_logistic;
		$payment_id=$orderDetailArray->payment_id;
		$gst_tcs_on_total_amount=$orderDetailArray->gst_tcs;
		$seller_comm=$orderDetailArray->pay_to_seller;
		
		
		$payment_type=$payment_id;	
		
		$payment_tax=0;
		$payment_fee=0;
		$payment_amount=0;
		$razorpay_tds=0;
		$payble_to_razorpay=0;
		
		if($payment_id!='COD')
		{
			$payment_type='Online';	
			$razorArray=$razorpay_helpers->razorpay_report_details($order_inc_id);
			$razorArray=json_decode($razorArray);							
			$payment_tax=$razorArray->razorpay_gst;
			$payment_fee=$razorArray->razorpay_charges;
			$payment_amount=$razorArray->razor_fee;
			$razorpay_tds=$orderDetailArray->razorpay_tds;
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			
			
		}	
		
		$seller_name=$orderDetailArray->seller_name;
				    
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,
			'invoice_id'=>$invoice_id,
			'seller_id'=>$seller_id,
			'order_cr_date'=>$order_cr_date,
			'awb' => 0,
			'order_total_inc_tax'=>$order_total_inc_tax,
			'order_total_amount'=>$order_total_amount,
			'seller_igst'=>$igst_amount,
			'seller_cgst'=>$cgst_amount,
			'seller_sgst'=>$sgst_amount, 
			'seller_ugst'=>$ugst_amount,  
			'ship_igst'=>$ship_igst_amount, 
			'ship_cgst'=>$ship_cgst_amount, 
			'ship_sgst'=>$ship_sgst_amount, 
			'ship_ugst'=>$ship_ugst_amount,
			'total_cgst'=>$total_cgst_amount, 
			'total_sgst'=>$total_sgst_amount, 
			'total_igst'=>$total_igst_amount, 
			'total_ugst'=>$total_ugst_amount,  
			'gst_tcs'=>$gst_tcs,  
			'seller_name'=>$seller_name,  
			'seller_address'=>$seller_address,  
			'buyer_name'=>$delivery_name,  
			'buyer_address'=>$delivery_adress,  
			'admin_comm'=>$admin_comm,  
			'admin_comm_tax'=>$admin_comm_tax,  
			'admin_comm_tds'=>$admin_comm_tds,  
			'total_admin_comm'=>$total_admin_comm,  
			'logistic_tax'=>$logistic_gst,  
			'logistic_gst'=>$logistic_tax,   
			'order_shipping_amount'=>$order_shipping_amount,  
			'logistic_tds'=>$logistic_tds,  
			'logistic_pay'=>$due_to_logistic,  
			'payment_type'=>$payment_type,   
			'payment_tax'=>$payment_tax,   
			'payment_fee'=>$payment_fee,   
			'payment_amount'=>$payment_amount,   
			'razorpay_tds'=>$razorpay_tds,   
			'payble_to_razorpay'=>$payble_to_razorpay,   
			'gst_tcs_on_total_amount'=>$gst_tcs_on_total_amount,   
			'seller_comm'=>$seller_comm,   
			
			
		);
				
		  

		/* echo "<pre>";
			print_r($mainarray);
		echo "</pre>";      */
 
			 
		$json_array=json_encode($mainarray);	
		
		$sql="insert into taxation_report_mini (order_id,invoice_id,cr_date,report_details) values ('".$order_inc_id."','".$invoice_id."','".$order_cr_date."','".$json_array."')";
			
		$connection->query($sql);	             
		
		
	}
	
	
	public function expense_report($order_increment_id)
	{
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		
		
		$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		
		$razor_pay=json_decode($this->tax_razorpay_expense($order_inc_id));

		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		$payment_type=0;
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0;  
		$payble_to_razorpay=0;    
		
		if($razor_pay->type=='online')
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			$razorpay_tds=$this->tax_razorpay_tds($razor_pay,$order_inc_id); 
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			//$payble_to_razorpay=$payment_fee-$razorpay_tds;
			
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		} 
		else 
		{
			$payment_type='COD';
			$seller_comm = $seller_comm -($noddle);
			$payble_to_razorpay='0';
		}
		
		
		
		$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id);
		$admin_comm_tax=$this->tax_admin_commission_tax($order_total_amount,$order_inc_id);
		$admin_comm_tds=$this->tax_admin_commission_tds($order_total_amount,$order_inc_id);
		
		$total_admin_comm=$admin_comm + $admin_comm_tax;
		
		$tax_amount=$orderArray['tax_amount']; // base price + tax

		

		$gst_tcs=$this->tax_gst_tcs($tax_amount,$order_inc_id); 
		$gst_tcs_on_total_amount=$this->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id); 
		$cgst_amount=$orderArray['cgst_amount']; // base price + tax
		$sgst_amount=$orderArray['sgst_amount']; // base price + tax
		$igst_amount=$orderArray['igst_amount']; // base price + tax
		 
		if($igst_amount!=0  && $cgst_amount==0 && $sgst_amount==0)
		{
			$igst_amount=$tax_amount;		
			$cgst_amount=0;		
			$sgst_amount=0;		
		}	
		
		if($igst_amount==0  && $cgst_amount!=0 && $sgst_amount!=0)
		{
			$igst_amount=0;
			$cgst_amount=$tax_amount/2;		
			$sgst_amount=$tax_amount/2;		
		}	
		
		$seller_comm='0';
		
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
		
		$pay_to_seller=$order_total_amount-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
		
		$mainarray=array(
			'order_increment_id'=>$order_increment_id,
			'invoice_id'=>$invoice_id,
			'order_created_at'=>$order_cr_date,
			'order_total_inc_tax'=>$order_total_inc_tax,
			'order_total_amount'=>$order_total_amount,
			'logistic_pay'=>$logistic_pay,
			'payble_to_razorpay'=>$payble_to_razorpay,
			'noddle'=>$noddle,
			'total_admin_comm'=>$total_admin_comm,
			'igst_amount'=>$igst_amount,
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'seller_comm'=>$pay_to_seller 
		);
		 
		  
		 echo "<pre>";
			print_r($mainarray);
		echo "</pre>";   

		$sql="insert into expensereport (order_increment_id,invoice_id,order_created_at,order_total_inc_tax,order_total_amount,logistic_pay,payble_to_razorpay,noddle,total_admin_comm,igst_amount,cgst_amount,sgst_amount,seller_comm) values ('".$order_increment_id."','".$invoice_id."','".$order_cr_date."','".$order_total_inc_tax."','".$order_total_amount."','".$logistic_pay."','".$payble_to_razorpay."','".$noddle."','".$total_admin_comm."','".$igst_amount."','".$cgst_amount."','".$sgst_amount."','".$pay_to_seller."')"; 
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		//$connection->query($sql); 	  
				 
	}
	

	public function expense_report_shipping_rest($order_increment_id)  
	{
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		/*  echo "<pre>";
		print_r($orderArray);
		echo "</pre>"; 
		 */
		
		$subtotal=$orderArray['subtotal'];
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		//print_r($gstArray);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		
		$orderDetailArray=$this->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/* echo "<pre>";
			print_r($orderDetailArray);
		echo "</pre>";   */
		
		$seller_id=$orderDetailArray->seller_id;
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		
		$seller_name=$seller_details->seller_name;
		$payment_id=$orderDetailArray->payment_id;
		$net_admin_comm=$orderDetailArray->net_admin_comm;
		
		
		$order_total_inc_tax=$orderDetailArray->total_amount;
		$order_total_amount=$orderDetailArray->subtotal;
		$order_shipping_amount=$orderDetailArray->shipping_expense;
		$payment_type=$payment_id;	
		
		$payment_tax=0;
		$payment_fee=0;
		$payment_amount=0;
		$razorpay_tds=0;
		$payble_to_razorpay=0;
		
		if($payment_id!='COD')
		{
			$payment_type='Online';	
			
			$razorArray=$razorpay_helpers->razorpay_report_details($order_inc_id);
			$razorArray=json_decode($razorArray);							
			
			//print_r($razorArray);
			
			$payment_tax=$razorArray->razorpay_gst;
			$payment_fee=$razorArray->razorpay_charges;
			$payment_amount=$razorArray->razor_fee;
			
			$razorpay_tds=$orderDetailArray->razorpay_tds;
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			
			
		}	
		
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		$mainarray=array(
			'order_increment_id'=>$order_inc_id,
			'invoice_id'=>$invoice_id,
			'order_created_at'=>$order_cr_date,
			'seller_name'=>$seller_name,
			'invoice_amount'=>$order_total_inc_tax,
			'payment_id'=>$payment_id,
			'payment_amount'=>$order_total_inc_tax,
			'basic_sale_amount'=>$order_total_amount,
			'igst_amount'=>$igst_amount,
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'ugst_amount'=>$ugst_amount, 
			'total_amount'=>$order_total_inc_tax, 
			'shipping_expense'=>$order_shipping_amount,
			'payble_to_razorpay'=>$payble_to_razorpay,
			'noddle'=>$noddle,
			'total_admin_comm'=>$net_admin_comm
		); 
		  
		  
		/* echo "<pre>";
			print_r($mainarray);
		echo "</pre>";      */
		
		 
		
		$json_array=json_encode($mainarray);
		
		 
		
		//$sql="insert into expensereport (order_increment_id,invoice_id,order_created_at,order_total_inc_tax,order_total_amount,logistic_pay,payble_to_razorpay,noddle,total_admin_comm,igst_amount,cgst_amount,sgst_amount,seller_comm) values ('".$order_increment_id."','".$invoice_id."','".$order_cr_date."','".$order_total_inc_tax."','".$order_total_amount."','".$logistic_pay."','".$payble_to_razorpay."','".$noddle."','".$total_admin_comm."','".$igst_amount."','".$cgst_amount."','".$sgst_amount."','".$pay_to_seller."')"; 
		
		
		$sql="INSERT INTO expense_report_shipping_rest(order_id, invoice_id, cr_date, report_details) VALUES ('".$order_increment_id."','".$invoice_id."','".$order_cr_date."','".$json_array."')";
		$connection->query($sql);  	   
		 	 
	}
	
	
	public function expense_report_shipping_rest_new($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		/* echo "<pre>";
		print_r($orderArray);
		echo "</pre>";  */
		
		
		$subtotal=$orderArray['subtotal'];
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		//print_r($gstArray);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		
		$orderDetailArray=$this->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/* echo "<pre>";
			print_r($orderDetailArray);
		echo "</pre>";   */
		
		$seller_id=$orderDetailArray->seller_id;
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		//print_r($seller_details);
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		
		$seller_name=$seller_details->seller_name;
		$seller_comp_nam=$seller_details->seller_comp_nam;
		$seller_state=$seller_details->seller_state;
		$seller_city=$seller_details->seller_city;  
		$seller_zipcode=$seller_details->seller_zipcode;  
		$seller_address=$seller_details->seller_comp_address;   
		 
		 
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		$seller_comp_nam=str_replace(array('\'', '"'), '', $seller_comp_nam); 
		
		
		$payment_id=$orderDetailArray->payment_id;
		$net_admin_comm=$orderDetailArray->net_admin_comm;
		
		
		$order_total_inc_tax=$orderDetailArray->total_amount;
		$order_total_amount=$orderDetailArray->subtotal;
		$order_shipping_amount=$orderDetailArray->shipping_expense;
		$payment_type=$payment_id;	
		
		$payment_tax=0;
		$payment_fee=0;
		$payment_amount=0;
		$razorpay_tds=0;
		$payble_to_razorpay=0;
		/*
		if($payment_id!='COD')
		{
			$payment_type='Online';	
			
			$razorArray=$razorpay_helpers->razorpay_report_details($order_inc_id);
			$razorArray=json_decode($razorArray);							
			
			//print_r($razorArray);
			
			$payment_tax=$razorArray->razorpay_gst;
			$payment_fee=$razorArray->razorpay_charges;
			$payment_amount=$razorArray->razor_fee;
			
			$razorpay_tds=$orderDetailArray->razorpay_tds;
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			
			
		}	
		
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		*/
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		
		$total_product_total=0;
		$total_fuel_amount=0;
		$total_after_fuel_added=0;
		$total_other_amount=0;
		$total_after_other_total=0;
		$total_grand_total=0;
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			
			$qty=$item->getQtyOrdered(); 
			$array=$helpers->ecom_price_by_product_array($buyer_pincode,$product_id,$qty);
			$awb_number = $this->tax_get_order_awb($order_id);
			
			/* echo "<pre>";
				print_r($array);
			echo "</pre>";    */
			
			$product_total=$array['product_total'];
			$fuel_amount=$array['fuel_amount'];
			$after_fuel_added=$array['after_fuel_added'];
			
			$other_amount=$array['other_amount'];
			$after_other_total=$array['after_other_total'];
			$gst_amount_ship=$array['gst_amount'];
			
			$ship_grand_total=$array['grand_total'];
			$product_total=$array['product_total'];
			
			
			$shipping_gst_amount=$array['gst_amount'];
			$shipping_gst_rate=$array['gst_rate'];
			$shipping_grand_total=$array['grand_total'];
			
			/*
			
			$total_product_total=$total_product_total+$product_total;
			$total_fuel_amount=$total_fuel_amount+$fuel_amount;
			$total_after_fuel_added=$total_after_fuel_added+$after_fuel_added;
			
			$total_other_amount=$total_other_amount+$other_amount;
			$total_after_other_total=$total_after_other_total+$after_other_total;
			
			$total_grand_total=$total_grand_total+$ship_grand_total;
			*/
			
			$total_product_total=$product_total;
			$total_fuel_amount=$fuel_amount;
			$total_after_fuel_added=$after_fuel_added;
			
			$total_other_amount=$other_amount;
			$total_after_other_total=$after_other_total;
			
			$total_grand_total=$ship_grand_total;
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			$ecom_length=$array['ecom_length']; 
			$ecom_breadth=$array['ecom_breadth'];     
			$ecom_height=$array['ecom_height'];   
			$vol_weight=$array['vol_weight'];   
			$ecom_weight=$array['ecom_weight'];   
			$charge_weight=$array['charge_weight'];   
			$zone=$array['zone'];   
			
			  
			$baljit_helper = $objectManager->create('Baljit\Buynow\Helper\Data');
			$shipping_gst = $baljit_helper->getConfig('buynows/general/shipping_gst');
			if(empty($shipping_gst))
			{
				$shipping_gst=0;
			}
			

			$logistic_pana_tax = $baljit_helper->getConfig('buynows/general/logistic_pana_tax');
			if(empty($logistic_pana_tax))
			{
				$logistic_pana_tax=0;
			}
			
			$logistic_pana_tax;
			
			
			$shipping_base=$shipping_grand_total-$shipping_gst_amount;
			
			$basic_shipping_charges=$after_other_total;
			$loading_charges=($basic_shipping_charges*$logistic_pana_tax)/100;
			$basic_shipping_loading_charges=$basic_shipping_charges+$loading_charges;		
			$shipping_gst_amount=($basic_shipping_loading_charges*$shipping_gst)/100;
			

			//$charge_weight=round($charge_weight);

			
			$charge_weights=$charge_weight*1000;
			$findWeight = $charge_weights/500;  
			$findWeight = ceil($findWeight); 
			$charge=0; 
			if($findWeight > 1)     
			{ 
				
				for($i=1; $i <= $findWeight; $i++){
					$charge = $charge + 0.5;
				}    
			}  
			else         
			{
				//$charge = $base_price+$ros_base;
				$charge = 0.5; 
			} 	 
			  
			/* $charge_weight=round($charge_weight*2)/2;
			$decimal=$charge_weight
			$charge_weight=
			
			if($charge_weight < 1)
			{
				$charge_weight=0.5;   
			}	 */	  
			 
			$mainarray=array(
			'order_increment_id'=>$order_inc_id,
			'invoice_id'=>$invoice_id,
			'order_created_at'=>$order_cr_date,
			'seller_name'=>$seller_comp_nam,
			'seller_state'=>$seller_state,
			'seller_city'=>$seller_city, 
			'seller_zipcode'=>$seller_zipcode, 
			'buyer_pincode'=>$buyer_pincode, 
			'buyer_city'=>$buyer_city,   
			
			'zone'=>$zone,  
			'ecom_length'=>$ecom_length,  
			'ecom_breadth'=>$ecom_breadth,  
			'ecom_height'=>$ecom_height,  
			'vol_weight'=>$vol_weight,   
			'ecom_weight'=>$ecom_weight,   
			'charge_weight'=>$charge,    
			'shipping_hsn'=>'996812',   
			'shipping_gst_type'=>'IGST',   
			 
			 
			
			'shipping_product_total'=>$total_product_total,
			'shipping_fuel_amount'=>$total_fuel_amount,
			'shipping_other_amount'=>$total_other_amount,   
			'basic_shipping_charges'=>$basic_shipping_charges,    
			'loading_charges'=>$loading_charges,    
			'basic_shipping_loading_charges'=>$basic_shipping_loading_charges,    
			 
			
			'shipping_hsn'=>'996812', 
			'shipping_gst_rate'=>$shipping_gst_rate, 
			'shipping_gst_type'=>'IGST', 
			'shipping_gst_amount'=>$shipping_gst_amount,    
			  
			'shipping_grand_total'=>$shipping_grand_total,   
			
			'shipping_expense'=>$shipping_grand_total,
				
		);  
		 
		$json_array=json_encode($mainarray);
		$sql="INSERT INTO expense_report_shipping_rest_new(order_id, invoice_id, cr_date, report_details,qty,awb) VALUES ('".$order_increment_id."','".$invoice_id."','".$order_cr_date."','".$json_array."','$qty','$awb_number')";
		
		$connection->query($sql);  	    
		 
		}	   
		
		 	 
	}
	
	  
	
	
	public function commission_settlement_report($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		/* echo "<pre>";
			print_r($orderArray);
		echo "</pre>";    */
		 
		
		$subtotal=$orderArray['subtotal'];
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		
		
		$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		//print_r($gstArray);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		 
		
		$orderDetailArray=$this->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/* echo "<pre>";
			print_r($orderDetailArray);
		echo "</pre>";  */
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		 
		
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
				
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		$seller_address=$seller_details->seller_comp_address;
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		
		$order_total_inc_tax=$orderDetailArray->total_amount;
		$order_total_amount=$orderDetailArray->subtotal;
		$gst_tcs=$orderDetailArray->gst_tcs_calculate_amount;
		
		$admin_comm=$orderDetailArray->admin_base_comm;
		$admin_comm_tax=$orderDetailArray->admin_comm_tax;
		$admin_comm_tds=$orderDetailArray->admin_comm_tds;
		$total_admin_comm=$orderDetailArray->net_admin_comm;
		
		$order_shipping_amount=$orderDetailArray->shipping_expense;
		$logistic_gst=$orderDetailArray->shipping_base;
		$logistic_tax=$orderDetailArray->shipping_gst;
		$logistic_tds=$orderDetailArray->logistic_tds;
		$due_to_logistic=$orderDetailArray->due_to_logistic;
		$payment_id=$orderDetailArray->payment_id;
		$gst_tcs_on_total_amount=$orderDetailArray->gst_tcs;
		$seller_comm=$orderDetailArray->pay_to_seller;
		$grand_total=$orderDetailArray->grand_total;
		$basic_sale_amount=$orderDetailArray->baseprice;
		
		$noddle_gst=$orderDetailArray->noddle_tax;
		$nodle_charges=$orderDetailArray->nodle_charges;
		$nodle_total=$orderDetailArray->nodle_total; 
		
		
		$payment_type=$payment_id;	
		
		$payment_tax=0;
		$payment_fee=0;
		$payment_amount=0;
		$razorpay_tds=0;
		$payble_to_razorpay=0;
		
		if($payment_id!='COD') 
		{
			$payment_type='Online';	
			
			$razorArray=$razorpay_helpers->razorpay_report_details($order_inc_id);
			$razorArray=json_decode($razorArray);							
			
			//print_r($razorArray);
			
			$payment_tax=$razorArray->razorpay_gst;
			$payment_fee=$razorArray->razorpay_charges;
			$payment_amount=$razorArray->razor_fee;
			
			$razorpay_tds=$orderDetailArray->razorpay_tds;
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			
			
		}	
		
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
		
		$cod_expenses=$orderDetailArray->cod_expenses;
		$cod_charge=$orderDetailArray->cod_charge;
		$cod_tax=$orderDetailArray->cod_tax;
		
		
		
		$mainarray=array(
			'order_increment_id'=>$order_inc_id,
			'invoice_id'=>$invoice_id,
			'order_created_at'=>$order_cr_date,
			'igst_amount'=>$igst_amount,
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'ugst_amount'=>$ugst_amount,
			'seller_id'=>$seller_id,
			'seller_name'=>$seller_name, 
			'invoice_amount'=>$grand_total, 
			'payment_id'=>$payment_id, 
			'payment_amount'=>$grand_total, 
			'basic_sale_amount'=>$basic_sale_amount, 
			'total_amount'=>$order_total_amount, 
			'shipping_basic_amount'=>$logistic_gst, 
			'shipping_gst'=>$logistic_tax,  
			'shipping_total'=>$order_shipping_amount, 
			'razorpay_charges'=>$payment_fee, 
			'razorpay_tax'=>$payment_tax, 
			'razorpay_total'=>$payment_amount, 
			'noddle_tax'=>$noddle_gst, 
			'nodle_charges'=>$nodle_charges, 
			'nodle_total'=>$nodle_total, 
			'gross_comm'=>$admin_comm, 
			'tds_admin_rate'=>$admin_comm_tds_rate, 
			'tds_admin_comm'=>$admin_comm_tds, 
			'net_admin_comm'=>$total_admin_comm, 
			'cod_expenses'=>$cod_expenses, 
			'cod_charge'=>$cod_charge, 
			'cod_tax'=>$cod_tax, 
			
			 
			
		);
		 
		  
		/* echo "<pre>";
			print_r($mainarray);
		echo "</pre>";      */ 
		 
		
		
		$json_array=json_encode($mainarray);
		
		    
		$sql="INSERT INTO commission_report(order_id, invoice_id, cr_date, report_details) VALUES ('".$order_increment_id."','".$invoice_id."','".$order_cr_date."','".$json_array."')";
		    
		    
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$connection->query($sql); 
		
				  
	}   
	
	
	
	
	public function ar_report($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		//$order_inc_id='000000218';
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		/* echo "<pre>";
			print_r($orderArray);
		echo "</pre>"; */
		
		$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
		
		$order_created_at=$orderArray['created_at'];
		$order_grand_total=$orderArray['grand_total'];
		$shippingaddress=$order->getShippingAddress()->getStreet();
		$buyer_name=$order->getShippingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname();
		
		$buyer_city = $order->getBillingAddress()->getCity();
		$buyer_state = $order->getBillingAddress()->getRegion();
		$buyer_pincode = $order->getBillingAddress()->getPostcode();
		$buyer_country_id = $order->getBillingAddress()->getCountryId();
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode.".";
		
		
		
		
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,	
			'invoice_id'=>$invoice_id,	
			'order_created_at'=>$order_created_at,	
			'order_grand_total'=>$order_grand_total,	
			'buyer_name'=>$buyer_name,	
			'buyer_address'=>$buyer_address	
		);
		
		
		
		/* echo "<pre>";
			print_r($mainarray);
		echo "</pre>";
		 */
		
		$sql="insert into ar_report (order_inc_id,invoice_id,order_created_at,order_grand_total,buyer_name,buyer_address) values ('".$order_inc_id."','".$invoice_id."','".$order_created_at."','".$order_grand_total."','".$buyer_name."','".$buyer_address."')"; 
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$connection->query($sql); 	  
				 
		 
				
	}	
		
		
	public function bank_report($order_inc_id) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		//$order_inc_id='000000218';
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		//print_r($gstArray); 
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		 
		
		$orderDetailArray=$this->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/* echo "<pre>";
			print_r($orderDetailArray);
		echo "</pre>";  */
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_created_at=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$order_grand_total=$orderDetailArray->grand_total;
		
		
		
		 
		
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
				
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		$seller_address=$seller_details->seller_comp_address;
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		

		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		
		
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;

		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		
		$street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	
		
		
		$payment_id=$orderDetailArray->payment_id;
		
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,	
			'invoice_id'=>$invoice_id,	
			'order_created_at'=>$order_created_at,	
			'order_grand_total'=>$order_grand_total,	
			'buyer_name'=>$buyer_name,	 
			'buyer_address'=>str_replace("'"," ",$buyer_address),	
			'shipping_address'=>str_replace("'"," ",$delivery_adress), 	
			'seller_name'=>$seller_name, 	 
			'utr_number'=>$payment_id,	  
			'utr_amount'=>$order_grand_total
		); 
		
		 
		
		// echo "<pre>";
		//	print_r($mainarray);
		// echo "</pre>";  
		
		
		$sql="insert into bank_report (order_inc_id,invoice_id,order_created_at,order_grand_total,buyer_name,buyer_address,shipping_address,seller_name,utr_number,utr_amount) values ('".$order_inc_id."','".$invoice_id."','".$order_created_at."','".$order_grand_total."','".$buyer_name."','".str_replace("'"," ",$delivery_adress)."','".$seller_name."','".str_replace("'"," ",$buyer_address)."','".$payment_id."','".$order_grand_total."')";
		 
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$connection->query($sql); 	  
		
		     
				 
	}		
		
	public function sales_report($order_inc_id)
	{
		//$order_inc_id='000000227';
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
		
		$orderArray=$order->getData();  			
		
		
		
		/* echo "<pre>";
			print_r($orderArray);
		echo "</pre>";  */ 
		
		
		$created_at=$orderArray['created_at'];
		$tax_amount=$orderArray['tax_amount'];
		$shipping_amount=$orderArray['shipping_amount'];
		$order_total_amount=$orderArray['subtotal_incl_tax'];
		$order_status=$orderArray['status'];
		$coupon_code=$orderArray['coupon_code'];
		$discount_description=$orderArray['discount_description'];
		$grand_total=$orderArray['grand_total'];
		
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');   
		
		
		$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_gst=$this->tax_logistic_gst($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		$shipping_date=$this->tax_get_order_shipping_date($order_id);
		if($shipping_date==0) 
		{ 
			$shipping_date='-';		 
		}

		if($coupon_code=='')
		{ 
			$coupon_code='-';		
		}	
		
		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		
		
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		
		$street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	

		
		$productName='';
		$productQty='';
		$productCat='';
		$productDescp='';
		$trimstring='';
		
		$productLength='';
		$productBreath='';
		$productHeight='';
		$productAbsWeight='';
		$productLogisticWeight='';
		$productDiscount='';
		$productDiscountAmount='';
		  
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			
			$prodnameee=str_replace('/','',$item->getName());
			$prodnameee=str_replace("'","",$prodnameee);
			$prodnameee=str_replace(".","",$prodnameee);
			   
			$productName .=$prodnameee.',';
			//$productName .=$item->getName().',';
			$productQty .=$item->getQtyOrdered().',';
			
			
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
			$categories = $product->getCategoryIds(); 
			$productDescp = $product->getDescription();
			
			
			$productDescp=str_replace('/','',$productDescp);
			$productDescp=str_replace("'","",$productDescp);
			$productDescp=str_replace('.','',$productDescp);
			 
			if (strlen($productDescp) > 25) {
				$trimstring .= substr($productDescp, 0, 25).',';
			} else {
				$trimstring .= $productDescp.',';
			}
			
			
			foreach($categories as $category){
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
				$productCat .=$cat->getName().',';
			}
			
			
			$sellerarray[]=$tax_helpers->tax_get_product_seller($order_inc_id,$product_id);
			
			$productLength .= $product->getData('ecom_length').',';
			$productBreath .= $product->getData('ecom_breadth').',';
			$productHeight .= $product->getData('ecom_height').',';
			
			$productAbsWeight .=$product->getWeight().',';
			
			$logistic_weight = $product->getData('ecom_length')*$product->getData('ecom_breadth')*$product->getData('ecom_height');
			if(!empty($logistic_weight)){
				 if($logistic_weight>1){
					 $logistic_weight = $logistic_weight/5000;
				 }else{
					 $logistic_weight = 0;
				 }
			}
			
			$productLogisticWeight .= $logistic_weight.',';
			
			
			$productDiscount .=round($product->getDiscountPercent()).'%'.',';
			$productDiscountAmount .=round($product->getDiscountAmount()).'%'.',';
			 
			
			
		}

		$resultarray=array_values(array_unique($sellerarray));
		
		$seller_details='';
		$seller_code='';
		$seller_name='';
		$seller_comp_address='';
		$seller_zipcode='';
		foreach($resultarray as $seller_id)
		{
			
			$seller_details=json_decode($tax_helpers->tax_get_seller_array($seller_id));		
			
			$seller_code .=$seller_details->id.',';
			$seller_name .=$seller_details->seller_name.',';
			$seller_comp_address .=$seller_details->seller_comp_address.',';
			$seller_zipcode .=$seller_details->seller_zipcode.',';
			
		}  	 
		
		
		$product_name=substr($productName,0,-1);
		$product_qty=substr($productQty,0,-1);
		$product_category=substr($productCat,0,-1);
		$product_description=substr($trimstring,0,-1);
		
		$product_length=substr($productLength,0,-1);
		$product_breath=substr($productBreath,0,-1);
		$product_height=substr($productHeight,0,-1);
		$product_abs_weight=substr($productAbsWeight,0,-1);
		$product_logistic_weight=substr($productLogisticWeight,0,-1);
		$product_discount=substr($productDiscount,0,-1);
		$product_discount_amount=substr($productDiscountAmount,0,-1);
		   
		
		$sellercode=substr($seller_code,0,-1);
		$sellername=substr($seller_name,0,-1);
		$sellercompaddress=substr($seller_comp_address,0,-1);
		$sellerzipcode=substr($seller_zipcode,0,-1);
		
		
		
		$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id);
		$admin_comm_tax=$this->tax_admin_commission_tax($order_total_amount,$order_inc_id);
		$admin_comm_tds=$this->tax_admin_commission_tds($order_total_amount,$order_inc_id);
		
		$total_admin_comm=$admin_comm + $admin_comm_tax;
		
		
		$payment_type='';
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0; 
		$payble_to_razorpay=0; 
		
		$razor_pay=json_decode($this->tax_razorpay_expense($order_inc_id));
		
		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		
		
		if($razor_pay->type=='online')
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			$razorpay_tds=$tax_helpers->tax_razorpay_tds($razor_pay,$order_inc_id); 
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			//$payble_to_razorpay=$payment_fee-$razorpay_tds;
			
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		}
		else 
		{
			$payment_type='COD';
			$payble_to_razorpay='0';
			$seller_comm = $seller_comm -($noddle);
		}
		 
		$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($created_at)); 
		$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
		
		$seller_city='';		
		$seller_state='';		
		$cod_charge='';		
		$cod_tax='';		
		$cod_expense='';		
		$delivery_date='';		
		
		$cgst_amount=$orderArray['cgst_amount']; // base price + tax
		$sgst_amount=$orderArray['sgst_amount']; // base price + tax
		$igst_amount=$orderArray['igst_amount']; // base price + tax

		
		
		
		
		$tax_amount=$igst_amount + $cgst_amount + $sgst_amount;		
		
		
		
		
		/* 
		echo '
		<tr>
			<td>'.$order_inc_id.'</td>
			<td>'.$invoice_id.'</td>
			<td>'.$created_at.'</td>
			<td>'.$buyer_name.'</td>
			<td>'.$buyer_city.'</td>
			<td>'.$buyer_state.'</td>
			<td>'.$buyer_pincode.'</td>
			<td>'.$delivery_name.'</td>
			<td>'.$delivery_adress.'</td>
			<td>'.$delivery_city.'</td>
			<td>'.$delivery_state.'</td>
			<td>'.$delivery_pincode.'</td> 
			<td>'.$product_name.'</td> 
			<td>'.$product_qty.'</td> 
			<td>'.$product_category.'</td> 
			<td>'.$grand_total.'</td> 
			<td>'.$sellercode.'</td> 
			<td>'.$sellername.'</td> 
			<td>'.$sellercompaddress.'</td>  
			<td>---</td> 
			<td>---</td> 
			<td>'.$sellerzipcode.'</td> 
			<td>'.$product_description.'</td> 
			<td>'.$tax_amount.'</td>  
			<td>'.$logistic_gst.'</td> 
			<td>'.$logistic_tax.'</td> 
			<td>'.$order_shipping_amount.'</td> 
			<td>'.$admin_comm.'</td> 
			<td>'.$admin_comm_tax.'</td> 
			<td>'.$admin_comm_tds.'</td> 
			<td>'.$total_admin_comm.'</td> 
			<td>'.$payment_type.'</td> 
			<td>'.$payment_tax.'</td> 
			<td>'.$payment_fee.'</td> 
			<td>'.$payment_amount.'</td> 
			<td>'.$razorpay_tds.'</td> 
			<td>'.$payble_to_razorpay.'</td> 
			<td>--</td> 
			<td>--</td> 
			<td>--</td>  
			<td>'.$order_status.'</td>  
			<td>'.$created_at.'</td>  
			<td>'.$shipping_date.'</td>  
			<td>--</td>  
			<td>'.$RealizationDays.'</td>
			<td>'.$product_length.'</td>
			<td>'.$product_breath.'</td>
			<td>'.$product_height.'</td>
			<td>'.$product_abs_weight.'</td>
			<td>'.$product_logistic_weight.'</td>
			<td>'.$product_discount.'</td> 
			<td>'.$discount_description.'</td>  
			<td>'.$product_discount_amount.'</td>
			<td>'.$coupon_code.'</td>
			<td>'.$coupon_code.'</td>
		</tr>         
		';  
		 */
		
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,	
			'invoice_id'=>$invoice_id,	
			'created_at'=>$created_at,	
			'buyer_name'=>$buyer_name,	
			'buyer_city'=>$buyer_city,	
			'buyer_state'=>$buyer_state,	
			'buyer_pincode'=>$buyer_pincode,	
			'delivery_name'=>$delivery_name,	
			'delivery_adress'=> str_replace("'"," ",$delivery_adress),	
			'delivery_city'=>$delivery_city,	
			'delivery_state'=>$delivery_state,	
			'delivery_pincode'=>$delivery_pincode,	
			'product_name'=>$product_name,	
			'product_qty'=>$product_qty,	
			'product_category'=>$product_category,	
			'grand_total'=>$grand_total,	
			'sellercode'=>$sellercode,	
			'sellername'=>$sellername,	
			'sellercompaddress'=>str_replace("'"," ",$sellercompaddress),	
			'seller_city'=>$seller_city,	
			'seller_state'=>$seller_state,	
			'sellerzipcode'=>$sellerzipcode,	
			'product_description'=>$product_description,	
			'tax_amount'=>$tax_amount,	
			'logistic_gst'=>$logistic_gst,	
			'logistic_tax'=>$logistic_tax,	
			'order_shipping_amount'=>$order_shipping_amount,	 
			'admin_comm'=>$admin_comm,	 
			'admin_comm_tax'=>$admin_comm_tax,	 
			'admin_comm_tds'=>$admin_comm_tds,	 
			'total_admin_comm'=>$total_admin_comm,	 
			'payment_type'=>$payment_type,	 
			'payment_tax'=>$payment_tax,	 
			'payment_fee'=>$payment_fee,	 
			'payment_amount'=>$payment_amount,	 
			'razorpay_tds'=>$razorpay_tds,	 
			'payble_to_razorpay'=>$payble_to_razorpay,	 
			'cod_charge'=>$cod_charge,	 
			'cod_tax'=>$cod_tax,	 
			'cod_expense'=>$cod_expense,	 
			'order_status'=>$order_status,	 
			'created_at'=>$created_at,	 
			'shipping_date'=>$shipping_date,	 
			'delivery_date'=>$delivery_date,	 
			'realization_days'=>$RealizationDays,	 
			'product_length'=>$product_length,	 
			'product_breath'=>$product_breath,	 
			'product_height'=>$product_height,	 
			'product_abs_weight'=>$product_abs_weight,	 
			'product_logistic_weight'=>$product_logistic_weight,	 
			'product_discount'=>$product_discount,	 
			'discount_description'=>$discount_description,	 
			'product_discount_amount'=>$product_discount_amount,	 
			'coupon_code'=>$coupon_code,	 
			'coupon_amount'=>$coupon_code 
		);
		 
		
		$json_data=json_encode($mainarray);
		
		$sql="insert into sales_report (order_id,invoice_id,created_at,sales_details) values ('".$order_inc_id."','".$invoice_id."','".$created_at."','".$json_data."')";
		
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$connection->query($sql); 	 
		   
		
		/* echo "<pre>"; 
			print_r($mainarray);
		echo "</pre>";   */
		
					
	}	
	
	
	public function sales_report_new($order_inc_id)
	{
		//$order_inc_id='000000227';
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		
		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		
		
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
		
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		
		$street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	
		
		
		$productName='';
		$productQty='';
		$productCat='';
		$productDescp='';
		$trimstring='';
		
		$productLength='';
		$productBreath='';
		$productHeight='';
		$productAbsWeight='';
		$productLogisticWeight='';
		$productDiscount='';
		$productDiscountAmount='';
		  
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			$product_idd=$item->getProductId();
			
			$prodnameee=str_replace('/','',$item->getName());
			$prodnameee=str_replace("'","",$prodnameee);
			$prodnameee=str_replace(".","",$prodnameee); 
			   
			$productName .=$prodnameee.',';
			//$productName .=$item->getName().',';
			$productQty .=$item->getQtyOrdered().',';
			
			
			
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_idd);
			$categories = $product->getCategoryIds(); 
			$productDescp = $product->getDescription();
			
			 
			
			
			$productDescp=str_replace('/','',$productDescp);
			$productDescp=str_replace("'","",$productDescp);
			$productDescp=str_replace('.','',$productDescp);
			 
			if (strlen($productDescp) > 25) {
				$trimstring .= substr($productDescp, 0, 25).',';
			} else {
				$trimstring .= $productDescp.',';
			}
			
			
			foreach($categories as $category){
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
				$productCat .=$cat->getName().',';
			}
			
			
			$sellerarray[]=$tax_helpers->tax_get_product_seller($order_inc_id,$product_idd);
			
			$productLength .= $product->getData('ecom_length').',';
			$productBreath .= $product->getData('ecom_breadth').',';
			$productHeight .= $product->getData('ecom_height').',';
			
			$productAbsWeight .=$product->getWeight().',';
			
			$logistic_weight = $product->getData('ecom_length')*$product->getData('ecom_breadth')*$product->getData('ecom_height');
			if(!empty($logistic_weight)){
				 if($logistic_weight>1){
					 $logistic_weight = $logistic_weight/5000;
				 }else{
					 $logistic_weight = 0;
				 }
			}
			
			$productLogisticWeight .= $logistic_weight.',';
			
			
			$productDiscount .=round($product->getDiscountPercent()).'%'.',';
			$productDiscountAmount .=round($product->getDiscountAmount()).'%'.',';
			 
			
			
		}
		
		
		
		$product_name=substr($productName,0,-1);
		$product_qty=substr($productQty,0,-1);
		$product_category=substr($productCat,0,-1);
		$product_description=substr($trimstring,0,-1);
		
		$product_length=substr($productLength,0,-1);
		$product_breath=substr($productBreath,0,-1);
		$product_height=substr($productHeight,0,-1);
		$product_abs_weight=substr($productAbsWeight,0,-1);
		$product_logistic_weight=substr($productLogisticWeight,0,-1);
		$product_discount=substr($productDiscount,0,-1);
		$product_discount_amount=substr($productDiscountAmount,0,-1);
		
		
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		//print_r($gstArray);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		 
		
		$orderDetailArray=$this->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/*  echo "<pre>";
			print_r($orderDetailArray);
		echo "</pre>";    */
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$grand_total=$orderDetailArray->grand_total;
		$sellername=$orderDetailArray->seller_name;
		
		
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		//print_r($seller_details);
		
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		$seller_address=$seller_details->seller_comp_address;
		$seller_city=$seller_details->seller_city;
		$seller_state=$seller_details->seller_state;
		$sellerzipcode=$seller_details->seller_zipcode; 
		
		 
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		
		$order_shipping_amount=$orderDetailArray->shipping_expense;
		$logistic_gst=$orderDetailArray->shipping_base;
		$logistic_tax=$orderDetailArray->shipping_gst;
		 
		$admin_comm=$orderDetailArray->admin_base_comm;
		$admin_comm_tax=$orderDetailArray->admin_comm_tax;
		$admin_comm_tds=$orderDetailArray->admin_comm_tds;
		$total_admin_comm=$orderDetailArray->net_admin_comm; 
		$payment_id=$orderDetailArray->payment_id;
		
		$cod_charge=$orderDetailArray->cod_charge;
		$cod_tax=$orderDetailArray->cod_tax;
		$cod_expense=$orderDetailArray->cod_expenses;
		$order_status=$orderDetailArray->order_status;
		
		
		
		$payment_type=$payment_id;	
		
		$payment_tax=0;
		$payment_fee=0;
		$payment_amount=0;
		$razorpay_tds=0;
		$payble_to_razorpay=0;
		
		if($payment_id!='COD') 
		{
			$payment_type='Online';	
			
			$razorArray=$razorpay_helpers->razorpay_report_details($order_inc_id);
			$razorArray=json_decode($razorArray);							
			
			//print_r($razorArray);
			
			$payment_tax=$razorArray->razorpay_gst;
			$payment_fee=$razorArray->razorpay_charges;
			$payment_amount=$razorArray->razor_fee;
			
			$razorpay_tds=$orderDetailArray->razorpay_tds;
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			
			
		}	
		
		
		$product_name = preg_replace('/[^A-Za-z0-9. -]/', '', $product_name);
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,	
			'invoice_id'=>$invoice_id,	
			'created_at'=>$order_cr_date,	
			'buyer_name'=>$buyer_name,	
			'buyer_city'=>$buyer_city,	
			'buyer_state'=>$buyer_state,	
			'buyer_pincode'=>$buyer_pincode,
			'delivery_name'=>$delivery_name,	
			'delivery_adress'=>str_replace("'"," ",$delivery_adress),	
			'delivery_city'=>$delivery_city,	
			'delivery_state'=>$delivery_state,	
			'delivery_pincode'=>$delivery_pincode,	
			'product_name'=>$product_name,	
			'product_description'=>$product_name,	
			'product_qty'=>$product_qty,	
			'product_category'=>$product_category,	
			'product_length'=>$product_length,	 
			'product_breath'=>$product_breath,	 
			'product_height'=>$product_height,	 
			'product_abs_weight'=>$product_abs_weight,	 
			'product_logistic_weight'=>$product_logistic_weight,	 
			'product_discount'=>$product_discount,	  
			'grand_total'=>$grand_total,	  
			'sellercode'=>$seller_id,	  
			'sellername'=>$sellername,	  
			'sellercompaddress'=>$seller_address,	  
			'seller_city'=>$seller_city,	  
			'seller_state'=>$seller_state,	  
			'sellerzipcode'=>$sellerzipcode,	   
			'tax_amount'=>$order_shipping_amount,	  
			'logistic_gst'=>$logistic_tax,	  
			'logistic_tax'=>$logistic_gst, 	  
			'order_shipping_amount'=>$order_shipping_amount,	  
			'admin_comm'=>$admin_comm,	  
			'admin_comm_tax'=>$admin_comm_tax,	  
			'admin_comm_tds'=>$admin_comm_tds,	  
			'total_admin_comm'=>$total_admin_comm,	  
			'payment_type'=>$payment_type,	  
			'payment_tax'=>$payment_tax,	  
			'payment_fee'=>$payment_fee,	   
			'payment_amount'=>$payment_amount,	  
			'razorpay_tds'=>$razorpay_tds,	  
			'payble_to_razorpay'=>$payble_to_razorpay,	  
			'cod_charge'=>$cod_charge,	  
			'cod_tax'=>$cod_tax,	  
			'cod_expense'=>$cod_expense,	  
			'order_status'=>$order_status,	  
			'shipping_date'=>'0',	  
			'delivery_date'=>'0',	  
			'realization_days'=>'0',	  
			'discount_description'=>'0',	  
			'product_discount_amount'=>'0',	  
			'coupon_code'=>'0',	  
			'coupon_amount'=>'0',	  
			
		);
		  
		

		/* echo "<pre>"; 
			print_r($mainarray);
		echo "</pre>";    */
		
		
		
		$json_data=json_encode($mainarray); 
		
		
		
		
		$sql="insert into sales_report (order_id,invoice_id,created_at,sales_details) values ('".$order_inc_id."','".$invoice_id."','".$order_cr_date."','".$json_data."')";   
		   
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$connection->query($sql); 	   
		  
		 
		
		
					
	}
	
	
	public function get_seller_zipcode($seller_id)
	{ 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('seller_postcode') 
                  ->where('seller_id = ?', $seller_id);
       
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return $result[0]['postcode'];
		}
		else 
		{
			return 0;
		}
		
		
	}
	
	
	
	public function success_page($orderId)
	{ 
		
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
       
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderId);
		$order_inc_id=$order->getIncrementId();
		
		$orderArray=$order->getData(); 
		$created_at=$order->getCreatedAt(); 
		
		
		
		
		$check=$this->check_order_tax_list_exist($order_inc_id);
		if($check==0) 
		{	
			//$tax_helpers->insert_tax_parameter($order_inc_id);   
			$common_helpers->report_insert_tax_parameter($order_inc_id);   
		}   
		
		
		
		
		
		
		$payment_method=$order->getPayment()->getMethod();
		
		
		$payment_section=$razorpay_helpers->razorpay_report_details($order_inc_id);
		
		$order_id=0;  
		
		if($payment_section!='0')
		{	
			$payment_section=json_decode($payment_section);
			$payment_id=$payment_section->payment_id;
			
			$url=$site_url.'qbonline/connection/razorpayment/order/'.$payment_id;
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			$response  = curl_exec($ch); 
			
			$result=json_decode($response);
			
			if(isset($result->order_id))
			{
				$order_id=$result->order_id; 			 
			} 	
			
			$check=$this->check_razorpay_exist($order_inc_id);
			$today=date('Y-m-d H:i:s'); 
			if($check==0)
			{	
				$sql = "Insert Into custom_order_razorpay (order_id,payment_id,razorpay,response_details, cr_date) Values ('".$order_inc_id."','".$payment_id."','".$order_id."','".$response."','".$today."')"; 
				$connection->query($sql);
			} 
		}
		
		$check=$this->check_taxation_report_mini_exist($order_inc_id);
		if($check==0) 
		{	
			$tax_helpers->taxation_report_mini($order_inc_id); 
		}  
		
		
		
		$check=$this->check_expense_report_shipping_rest_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->expense_report_shipping_rest($order_inc_id); 
		} 
		
		
		$check=$this->check_expense_report_shipping_rest_exist_new($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->expense_report_shipping_rest_new($order_inc_id); 
		} 
		 
		
		
		$check=$this->check_commission_settlement_report_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->commission_settlement_report($order_inc_id); 
		} 
		
		
		$check=$this->check_sales_report_exist($order_inc_id);
		if($check==0)
		{	 
			$tax_helpers->sales_report_new($order_inc_id); 
			
			/*
			
			$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
			$sellerphone=array();		
			
			foreach ($order->getAllItems() as $item)
			{
				 
				$actual_prd_id=$item->getProductId();
				$sellerarray=$helpers->seller_details($actual_prd_id,$order_inc_id);
					
				if(!empty($sellerarray))
				{ 
					if(!empty($sellerarray['phone']))
					{		
						$sellerphone[]=$sellerarray['phone'];
					} 
				}       
				  
				
			}  

			if(!empty($sellerphone))
			{
				$result = array_values(array_unique($sellerphone));
				
				foreach($result as $row)
				{
					//$helpers->send_order_sms($row,$order_inc_id);	
				}	  
			} 
			
              
			$sellerarray=array();

			foreach ($order->getAllItems() as $item)
			{
				$product_id=$item->getId();
				$sellerarray[]=$custom_helpers->get_product_seller($orderId,$product_id);
			}      	
			
			 
			$resultarray=array_values(array_unique($sellerarray));
			
			if(!empty($resultarray))
			{
				$seller_id=$resultarray[0];	
			}	
			else  
			{
				$seller_id='29';   
			}
			//$custom_helpers->route_payment($orderId,$seller_id); 
			*/
			    
		}
		
		
		$check=$this->check_bank_report_exist($order_inc_id);
		if($check==0)
		{	
			$tax_helpers->bank_report($order_inc_id); 
		}
		
		
		  
	
	}	
	
	
	public function check_sales_report_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('sales_report') 
                  ->where('order_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select); 
			
		
		
		if(!empty($result))
		{ 
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}
	
	
	public function check_commission_settlement_report_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('commission_report') 
                  ->where('order_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select);

		
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}
	
	
	public function check_expense_report_shipping_rest_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('expense_report_shipping_rest') 
                  ->where('order_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select);
		
		 
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}	
	
	public function check_expense_report_shipping_rest_exist_new($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('expense_report_shipping_rest_new')  
                  ->where('order_id = ?', $order_inc_id); 
       
		$result = $connection->fetchAll($select);
		
		 
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		 
		
	}	
	
	
	
	public function check_order_tax_list_exist($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
                  ->from('order_tax_list') 
                  ->where('order_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function check_razorpay_exist($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		
		$select = $connection->select()
                  ->from('custom_order_razorpay') 
                  ->where('order_id = ?', $order_inc_id)
                  ->where('payment_id is not null');
		
		$result = $connection->fetchAll($select);
		  
		if(!empty($result))
		{ 
			return 1;	 
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function check_bank_report_exist($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
			->from(
				['o' =>  $resource->getTableName('bank_report')]
			)->where('o.order_inc_id=?',$order_inc_id);
	 
		$result = $connection->fetchAll($select);
		 
		 
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function check_taxation_report_mini_exist($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
		
		
		$select = $connection->select()
                  ->from('taxation_report_mini') 
                  ->where('order_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select);
		
		 
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function seller_order_page($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		/*  echo "<pre>";
		print_r($orderArray);
		echo "</pre>"; 
		 */
		
		$subtotal=$orderArray['subtotal'];
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		
		
		$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		
		$razor_pay=json_decode($this->tax_razorpay_expense($order_inc_id));

		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		$payment_type=0;
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0;  
		$payble_to_razorpay=0;    
		
		if($razor_pay->type=='online')
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			//$razorpay_tds=$this->tax_razorpay_tds($razor_pay,$order_inc_id); 
			$razorpay_tds=0;  
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			//$payble_to_razorpay=$payment_fee-$razorpay_tds;
			
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		} 
		else 
		{
			$payment_type='COD';
			$seller_comm = $seller_comm -($noddle);
			$payble_to_razorpay='0';
		}
		
		
		
		if($razor_pay->type=='online')
		{	
			$transaction = $objectManager->create('\Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory')->create()->addOrderIdFilter($order_inc_id)->getFirstItem();

			$transactionId = $transaction->getData('txn_id');
			$transactionstatus = $transaction->getData('is_closed');
			if($transactionId=='')
			{
				$transactionId=0;		
			}	
			/*
			// print_r($transaction->getData());
			$payment = $orderDatamodel1->getPayment();
			$amount_paid = $payment->getAmountPaid(); */
		}
		else 
		{
			$transactionId = 'COD';
		}
		
		
		
		
		
		
		
		$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id);
		$admin_comm_tax=$this->tax_admin_commission_tax($order_total_amount,$order_inc_id);
		$admin_comm_tds=$this->tax_admin_commission_tds($order_total_amount,$order_inc_id);
		
		$total_admin_comm=$admin_comm + $admin_comm_tax;
		
		$tax_amount=$orderArray['tax_amount']; // base price + tax

		

		$gst_tcs=$this->tax_gst_tcs($tax_amount,$order_inc_id); 
		$gst_tcs_on_total_amount=$this->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id); 
		$cgst_amount=$orderArray['cgst_amount']; // base price + tax
		$sgst_amount=$orderArray['sgst_amount']; // base price + tax
		$igst_amount=$orderArray['igst_amount']; // base price + tax
		

		/* if($igst_amount!=0  && $cgst_amount==0 && $sgst_amount==0)
		{
			$igst_amount=$tax_amount;		
			$cgst_amount=0;		
			$sgst_amount=0;		
		}	
		
		if($igst_amount==0  && $cgst_amount!=0 && $sgst_amount!=0)
		{
			$igst_amount=0;
			$cgst_amount=$tax_amount/2;		
			$sgst_amount=$tax_amount/2;		
		}	
		 */  
		
		$buyer_zipcode=$order->getShippingAddress()->getPostcode();
		$buyer_state=$helpers->get_state_pincode($buyer_zipcode);	
		
		$sellerarray=$this->get_order_seller_array($order_inc_id);
		
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
					
		$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
		foreach($sellerarray as $row)
		{
			$seller_id=$row;		
			$sellerdetails=$this->tax_get_seller_array($seller_id);
			$seller_details=json_decode($sellerdetails);
			
			$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
			$seller_name=$seller_details->seller_name;
			
			$ugst_amount=0;
			
			/* if($buyer_state==$seller_state)
			{
				$igst_amount=$tax_amount;			
			}	
			else
			{
				$igst_amount=0;
				$cgst_amount=$tax_amount/2;		
				$sgst_amount=$tax_amount/2;		
			}
			 */
			
			
			
		}
		
		
		
		$seller_comm='0';
		
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
		
		$pay_to_seller=$order_total_amount-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
		
		
		
		$basic_sale_amount=$subtotal; 
		
		
		$basic_sale_amount=$subtotal; 
		
		$total_amount_xxx=$basic_sale_amount;
		$admin_comm=$this->tax_admin_commission($total_amount_xxx,$order_inc_id);
		$admin_comm_tax=$this->tax_admin_commission_tax($total_amount_xxx,$order_inc_id);
		$admin_comm_tds=$this->tax_admin_commission_tds($total_amount_xxx,$order_inc_id);    
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
		
		$total_admin_comm=$admin_comm + $admin_comm_tax; 
		
		$net_admin_comm=$total_admin_comm - $admin_comm_tds;
		$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
		
		
		
		$report_total_amount=$basic_sale_amount + $tax_amount; 
		$mainarray=array(
			'order_increment_id'=>$order_increment_id,
			'invoice_id'=>$invoice_id,
			'order_created_at'=>$order_cr_date,
			'seller_name'=>$seller_name,
			'invoice_amount'=>$order_total_inc_tax,
			'payment_id'=>$transactionId,
			'payment_amount'=>$order_total_inc_tax,
			'basic_sale_amount'=>$basic_sale_amount,
			'igst_amount'=>$igst_amount,
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'ugst_amount'=>$ugst_amount, 
			'total_amount'=>$report_total_amount, 
			'shipping_expense'=>$order_shipping_amount,
			'payble_to_razorpay'=>$payble_to_razorpay,
			'noddle'=>$noddle, 
			'total_admin_comm'=>$net_admin_comm
		); 
		 
		
		return $json_array=json_encode($mainarray); 
		
		   
		
		  
				 
	}
	
	
	public function get_order_gst_details($order_increment_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		
		$order_inc_id=$order_increment_id;
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();	
		
		
		/* echo "<pre>";
		print_r($orderArray);
		echo "</pre>"; */
		
		
		$order_shipping_amount=@$orderArray['shipping_amount'];
		
		$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		$logistic_base=$order_shipping_amount-$logistic_tax;
		
		
		
		$cgst_amount=0;
		$sgst_amount=0;
		$igst_amount=0;
		$utgst_amount=0;
		$total_gst=0;
		
		 
		/* $cgst_amount=$orderArray['cgst_amount']; // base price + tax
		$sgst_amount=$orderArray['sgst_amount']; // base price + tax
		$igst_amount=$orderArray['igst_amount']; // base price + tax */
		
		
		
		if(isset($orderArray['cgst_amount']))
		{
			$cgst_amount=$orderArray['cgst_amount'];
		}	
		
		if(isset($orderArray['sgst_amount']))
		{
			$sgst_amount=$orderArray['sgst_amount'];
		}	
		
		if(isset($orderArray['igst_amount']))
		{
			$igst_amount=$orderArray['igst_amount'];
		}	
		
		
		
		if(isset($orderArray['utgst_amount']))
		{
			$utgst_amount=$orderArray['utgst_amount'];
		}	
		
		$num_igst_amount=(int)$igst_amount;
		$num_sgst_amount=(int)$sgst_amount;
		
		
		$shipping_igst=0;
		$shipping_cgst=0;
		$shipping_sgst=0;
		$shipping_utgst=0;
		
		
		
		
		if($num_igst_amount!=0)
		{
			$shipping_igst=$logistic_tax;
		}
		else
		{	
			$logistic_tax=$logistic_tax/2;
			$shipping_cgst=$logistic_tax;
			if($num_sgst_amount!=0)
			{
				$shipping_sgst=$logistic_tax;
			}
			else
			{
				$shipping_utgst=$logistic_tax;
			}		
			
		}		
		
		
		
		
		$total_gst=$cgst_amount+$sgst_amount+$igst_amount+$utgst_amount; 
		$total_shipping_gst=$shipping_igst+$shipping_cgst+$shipping_sgst+$shipping_utgst;
		
		$array=array(
			'igst'=>$igst_amount,
			'cgst'=>$cgst_amount,
			'sgst'=>$sgst_amount,
			'utgst'=>$utgst_amount,
			'total_gst'=>$total_gst,
			'shipping_igst'=>$shipping_igst,
			'shipping_cgst'=>$shipping_cgst,
			'shipping_sgst'=>$shipping_sgst,
			'shipping_utgst'=>$shipping_utgst,
			'total_shipping_gst'=>$total_shipping_gst,
		
		);
		
		return $array;
		
	}	
	
	
	  
	public function order_amount_details($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		$order_inc_id=$order_increment_id;
		
		
		
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		 
		$check=$this->check_order_tax_list_exist($order_inc_id);
		if($check==0) 
		{	
			$common_helpers->report_insert_tax_parameter($order_inc_id);   
		}    
		
		
		
		 
		/*  echo "<pre>";
		print_r($orderArray);
		echo "</pre>";   */ 
		
		 
			
		
		$customer_id=$orderArray['customer_id'];
		$subtotal=$orderArray['subtotal'];
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		
		$order_cr_date=$orderArray['created_at'];
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		$order_total_amount=$orderArray['grand_total']; // base price + tax
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		
		
		$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		
		
		
		$razor_pay=json_decode($this->tax_razorpay_expense($order_inc_id));
		/* echo "</pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		$payment_type=0;
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0;  
		$payble_to_razorpay=0;    
		$payment_gateway_charges=0;    
		

		
		/*
			
		*/
		
		
		$razorpay_details=$razorpay_helpers->razorpay_report_details($order_inc_id);
		//print_r($razorpay_details);  
		//die;   
		
		
		
		
		
		$cod_expenses=0;
		$cod_charge=0;
		$cod_tax=0;
		
		$razor_pay_fee_amount=0;
		$razor_pay_gst_amount=0;
		
		$razor_payment=0;
		$razor_card=0;
		$razor_network=0;
		
		  
		if($razorpay_details=='0')
		{
			$payment_type='COD';
			$seller_comm = $seller_comm -($noddle);
			$payble_to_razorpay='0';
			$transactionId = 'COD';
			$noddle_amount = 0;
			
			$cod_expenses=0;
			$cod_charge=0;
			$cod_tax=0;
			
			
			$cod_expenses='50';
			$cod_charge=$this->tax_get_cod_tax($cod_expenses,$order_inc_id) ;
			$cod_tax=$cod_expenses - $cod_charge;
		}
		else
		{
			$razorpay_details=json_decode($razorpay_details);
			//print_r($razorpay_details);
			
			$payble_to_razorpay=@$razorpay_details->razorpay_total;
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$payment_amount=@$razorpay_details->razorpay_charges_terms; 
			if($payment_amount=='') 
			{
				$payment_amount=0;	  
			}	
			$transactionId = @$razorpay_details->payment_id;
			$noddle_amount = @$razorpay_details->nodal_amount;
			$seller_comm = $seller_comm -($payment_amount+$noddle);	
			
			
			$payment_gateway_charges=$payment_amount;
			
			$razor_pay_fee_amount=@$razorpay_details->razorpay_charges;
			$razor_pay_gst_amount=@$razorpay_details->razorpay_gst;
			
			$razor_payment=@$razorpay_details->razor_payment;
			$razor_card=@$razorpay_details->razor_card;
			$razor_network=@$razorpay_details->razor_network;
			
		}	
		
		
		
		
		
		
		
		$admin_comm=$this->tax_admin_commission($order_total_amount,$order_inc_id);
		$admin_comm_tax=$this->tax_admin_commission_tax($order_total_amount,$order_inc_id);
		$admin_comm_tds=$this->tax_admin_commission_tds($order_total_amount,$order_inc_id);
		
		$total_admin_comm=$admin_comm + $admin_comm_tax;
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst'];  
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$total_gst=$gstArray['total_gst']; 
		
		
		$tax_amount=$total_gst;

		

		
		
		
		
		
		
		 
		
		$buyer_zipcode=$order->getShippingAddress()->getPostcode();
		$buyer_state=$helpers->get_state_pincode($buyer_zipcode);	
		
		$total_admin_comm=0;
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getproductId(); 
			
			$seller=$custom_helpers->get_product_seller($order_id,$product_id);
			$sellerarray[]=$custom_helpers->get_product_seller($order_id,$product_id);
			
			$productArray=$tax_helpers->order_item_details_product($order_inc_id,$product_id);
			$comission=$productArray[0]['admin_comm'];  
			$total_admin_comm=$total_admin_comm+$comission;
		}  	
		
		
		$resultarray=array_values(array_unique($sellerarray));
		
		
		
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
					
		$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
		foreach($resultarray as $row)
		{
			$seller_id=$row;		
			$sellerdetails=$this->tax_get_seller_array($seller_id);
			$seller_details=json_decode($sellerdetails);
			
			$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
			$seller_name=$seller_details->seller_name;
			
			
		} 
		
		
		
		$seller_comm='0';
		
		$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
		
		
		
		
		
		
		$basic_sale_amount=$subtotal; 
		
		
				
		$total_amount_xxx=$basic_sale_amount;
		
		
		
		
		
		$report_total_amount=$basic_sale_amount + $tax_amount; 
		
		
		
		$order_subtotal=$basic_sale_amount-$total_gst;
		
		
		//$admin_comm=$this->tax_admin_commission($total_amount_xxx,$order_inc_id);
		$admin_comm=$total_admin_comm; 
		
		
		
		
		
		
		$total_amount_xxx;
		$admin_comm_tds=$this->tax_admin_commission_tds($total_amount_xxx,$order_inc_id);    
		
		$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		 
		//print_r($tax_rates_array); 
		
		$admin_comm_rate=$tax_rates_array->admin_comm;
		$shipping_gst_rate=$tax_rates_array->shipping_gst;
		
		$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
		$gst_tcs_rate=$tax_rates_array->gst_tcs;
		
		$admin_comm_tax_rate=$tax_rates_array->admin_comm_tax;
		//$admin_comm_tax=$this->tax_admin_commission_tax($total_amount_xxx,$order_inc_id);
		$admin_comm_tax=($admin_comm*$admin_comm_tax_rate)/100; 
		   
		
		$total_admin_comm=$admin_comm + $admin_comm_tax; 
		
		
		//$admin_comm_tds=(($admin_comm_rate/100)*$total_amount_xxx)*($admin_comm_tds_rate/100);
		$admin_comm_tds=($admin_comm*$admin_comm_tds_rate)/100;
		 
		$gst_tcs_on_total_amount=($order_subtotal*$gst_tcs_rate)/100;	   
		$net_comm=$admin_comm+$admin_comm_tax; 
		//$net_admin_comm=$total_admin_comm - $admin_comm_tds;
		$net_admin_comm=$net_comm+$gst_tcs_on_total_amount-$admin_comm_tds;
		
		
		
		
		$due_to_seller=$total_amount_xxx-$net_admin_comm;
		$due_to_seller=number_format($due_to_seller, 2, '.', '');
		
		
		$order_shipping_amount;
		
		 $logistic_tax;
		
		
		$logistic_base=$order_shipping_amount-$logistic_tax;
		
		$gst_tcs_rate=$tax_rates_array->gst_tcs;
		
		$gst_tcs_calculate_amount=$order_subtotal+$logistic_base; 
		
		//$gst_tcs_on_total_amount=$this->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id); 
		
		//$gst_tcs_on_total_amount=($gst_tcs_calculate_amount*$gst_tcs_rate)/100; 
		
		
		//$gst_tcs_on_total_amount=number_format($gst_tcs_on_total_amount, 2, '.', '');
		//$pay_to_seller=$order_total_amount-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
		$pay_to_seller=$due_to_seller-$gst_tcs_on_total_amount;
		
		
		
		
		
		
		$logistic_tds=$this->tax_logistic_tds($logistic_base,$order_inc_id);

		 
		
		$logistic_pay=$order_shipping_amount-$logistic_tds; 
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		
		
		$razorpay_tds_rate=$tax_rates_array->razorpay_tds;
		
		
		$razorpay_base_price=0;
		$razorpay_tds=0;
		$due_to_razorpay=0;
		//$payment_gateway_charges=2.719; 
		
		
		
		
		if($razorpay_details!='' || $razorpay_details!='0')
		{
			$razorpay_base_price=(($payment_gateway_charges)*($basic_sale_amount+$order_shipping_amount))/100;
			$razorpay_base_price=number_format($razorpay_base_price, 2, '.', '');
			$razorpay_tds=($razorpay_base_price*$razorpay_tds_rate)/100;
			 
			$due_to_razorpay=$razorpay_base_price-$razorpay_tds; 
			 
		} 
		
		
		$razorpay_tds=($razor_pay_fee_amount*$razorpay_tds_rate)/100;
		  
		
		$razorpay_tds=number_format($razorpay_tds, 2, '.', '');
		//$due_to_razorpay=number_format($due_to_razorpay, 2, '.', ''); 
		
		
		//$net_admin_comm=$net_admin_comm+$gst_tcs_on_total_amount; 
		
		//$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
		
		
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle_tax=$this->tax_get_nodal_tax($noddle,$order_inc_id);  
		$nodle_total=$noddle;
		$nodle_charges=$noddle - $noddle_tax; 
		$nodle_charges=0;  
		
		
		//$due_to_logistic=
		
		$mainarray=array(
			'order_increment_id'=>$order_increment_id,
			'customer_id'=>$customer_id,
			'invoice_id'=>$invoice_id,
			'order_created_at'=>$order_cr_date,
			'seller_id'=>$seller_id,
			'seller_name'=>$seller_name,
			'invoice_amount'=>$order_total_inc_tax,
			'payment_id'=>$transactionId,
			'baseprice'=>$order_subtotal,
			'total_gst'=>$total_gst,
			'subtotal'=>$basic_sale_amount,
			'shipping_expense'=>$order_shipping_amount,
			'shipping_base'=>$logistic_base,
			'shipping_gst'=>$logistic_tax,
			'grand_total'=>$order_total_inc_tax, 
			'admin_comm'=>$total_admin_comm, 
			'admin_comm_tds'=>$admin_comm_tds, 
			'due_to_seller'=>$due_to_seller, 
			'gst_tcs'=>$gst_tcs_on_total_amount,   
			'logistic_tds'=>$logistic_tds, 
			'payment_gateway_charges'=>$payment_gateway_charges, 
			'razorpay_tds'=>$razorpay_tds,  
			'razorpay_base_price'=>$razorpay_base_price, 
			'due_to_razorpay'=>$due_to_razorpay, 
			'total_discount'=>0, 
			'admin_base_comm'=>$admin_comm, 
			'admin_comm_tax'=>$admin_comm_tax,  
			
			
			'basic_sale_amount'=>$basic_sale_amount,
			'igst_amount'=>$igst_amount,
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'ugst_amount'=>$ugst_amount,  
			'total_amount'=>$order_total_inc_tax, 
			
			//'noddle'=>$noddle, 
			//'noddle_amount'=>$noddle_amount, 
			
			'pay_to_seller'=>$pay_to_seller, 
			'net_comm'=>$net_comm, 
			'net_admin_comm'=>$net_admin_comm, 
			'due_to_logistic'=>$logistic_pay, 
			'gst_tcs_calculate_amount'=>$gst_tcs_calculate_amount, 
			'noddle_tax'=>$noddle_tax, 
			'nodle_charges'=>$nodle_charges, 
			'nodle_total'=>$nodle_total, 
			'cod_expenses'=>$cod_expenses, 
			'cod_charge'=>$cod_charge, 
			'cod_tax'=>$cod_tax, 
			'order_status'=>$orderArray['status'], 
			'shipping_gst_rate'=>$shipping_gst_rate,
			'razor_pay_fee_amount'=>$razor_pay_fee_amount,
			'razor_pay_gst_amount'=>$razor_pay_gst_amount,
			'gross_comm_due'=>$admin_comm,
			'gst_gross_comm_due'=>$admin_comm_tax,
			'razor_payment'=>$razor_payment,
			'razor_card'=>$razor_card,
			'razor_network'=>$razor_network,
			'admin_comm_rate'=>$admin_comm_rate,
			'admin_comm_tds_rate'=>$admin_comm_tds_rate,
			'gst_tcs_rate'=>$gst_tcs_rate,
			
			
			
			
			/*
			
			'payment_amount'=>$order_total_inc_tax,
			'basic_sale_amount'=>$basic_sale_amount,
			'igst_amount'=>$igst_amount,
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'ugst_amount'=>$ugst_amount,  
			'total_amount'=>$report_total_amount, 
			'shipping_expense'=>$order_shipping_amount,
			'payble_to_razorpay'=>$payble_to_razorpay,
			'noddle'=>$noddle, 
			'total_admin_comm'=>$net_admin_comm
			*/
		); 
		
		
		
		

		 

		
		/* echo "<pre>"; 
			print_r($mainarray);
		echo "</pre>";  */
		 
		
		return $json_array=json_encode($mainarray);
		
		   
		
		  
				 
	}
	
	public function order_item_details($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$order_inc_id=$order_increment_id;
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		
		$orderArray=$order->getData();
		 
		$check=$this->check_order_tax_list_exist($order_inc_id);
		if($check==0) 
		{	
			$tax_helpers->insert_tax_parameter($order_inc_id);   
		} 
		
		$buyer_id=$orderArray['customer_id']; 
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst'];  
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		
		
		$shippingAmount=0; 
		$total_shippingAmount=0;
		$special_price=0;
		$sale=0;
		$discountAmount=0;
		$total_gst=0;
		$total_price=0;
		$total_gst=0;
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId(); 
			$product_name=$item->getName();
			$product_price=$item->getPrice();
			$product_qty=$item->getQtyOrdered();
			
			$row_total=$product_qty*$product_price;
			
			//$product = $productRepository->getById($product_id); 
			
			

			$select = $connection->select()
                  ->from('sales_order_item') 
                  ->where('order_id = ?', $order_id)
                  ->where('product_id = ?', $product_id);
       
			$result = $connection->fetchAll($select);
			
			$cgst_amount=0;	
			$sgst_amount=0;	
			$igst_amount=0;	
			$utgst_amount=0;	 
			$discount_amount=0;	 
			
			if(!empty($result))
			{
				$cgst_amount=$result[0]['cgst_amount'];	
				$sgst_amount=$result[0]['sgst_amount'];	
				$igst_amount=$result[0]['igst_amount'];	
				$discount_amount=$result[0]['discount_amount'];	
				
				if(isset($result[0]['utgst_amount']))
				{
					$utgst_amount=$result[0]['utgst_amount'];		
				}	
				
			}	 

			
			$total_gst=$cgst_amount+$sgst_amount+$igst_amount+$utgst_amount;
			$base_amount=$row_total-$total_gst;
			
			
			$taxable_value=$base_amount*$product_qty;
			
			
			
			
			$base_amount=number_format($base_amount, 2, '.', '');
			$taxable_value=number_format($taxable_value, 2, '.', ''); 
			$total_gst=number_format($total_gst, 2, '.', '');     
			
			//$total_gst=$api_helpers->custom_number_format($total_gst);     
			$cgst_amount=number_format($cgst_amount, 2, '.', ''); 
			$sgst_amount=number_format($sgst_amount, 2, '.', '');
			$igst_amount=number_format($igst_amount, 2, '.', '');
			$utgst_amount=number_format($utgst_amount, 2, '.', '');
			$discount_amount=number_format($discount_amount, 2, '.', '');
			$row_total=number_format($row_total, 2, '.', '');
			$product_price=number_format($product_price, 2, '.', '');
			
			
			$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
		
			//print_r($tax_rates_array);
			
			$total_amount_xxx=$row_total; 
			
			$admin_comm_rate=$tax_rates_array->admin_comm;
			$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
			
			
			$admin_comm=$this->tax_admin_commission($total_amount_xxx,$order_inc_id);
			$admin_comm_tax=$this->tax_admin_commission_tax($total_amount_xxx,$order_inc_id);
			
			
			$total_admin_comm=$admin_comm + $admin_comm_tax; 
			
			
			$admin_comm_tds=(($admin_comm_rate/100)*$total_amount_xxx)*($admin_comm_tds_rate/100);
			
			
			
			$net_admin_comm=$total_admin_comm - $admin_comm_tds;
			$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
			
			
			$due_to_seller=$total_amount_xxx-$net_admin_comm;
			$due_to_seller=number_format($due_to_seller, 2, '.', '');
			
			
			$pincode=$custom_helpers->get_customer_shipping_address($buyer_id,'postcode');
			$shippingAmount=0;
			if($pincode!='0')  
			{	
				$shippingAmount=$helpers->ecom_price_by_product($pincode,$product_id,$product_qty); 
			}   
			
			$product_base=$row_total-$total_gst;
			
			$order_shipping_amount=$shippingAmount;
			
			$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
			$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);

			$logistic_base=$order_shipping_amount-$logistic_tax;
			$gst_tcs_rate=$tax_rates_array->gst_tcs;
			$gst_tcs_calculate_amount=$base_amount+$logistic_base; 
					
			//$gst_tcs_on_total_amount=$this->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id); 

			$gst_tcs_on_total_amount=($gst_tcs_calculate_amount*$gst_tcs_rate)/100; 

			//$pay_to_seller=$order_total_amount-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
			$pay_to_seller=$due_to_seller-$gst_tcs_on_total_amount;
			
			
			$net_admin_comm=$net_admin_comm+$gst_tcs_on_total_amount;
		
			$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
			 
			
			$mainArray[]=array(
				'product_id'=>$product_id,	
				'product_name'=>$product_name,	
				'product_price'=>$product_price,	
				'product_qty'=>$product_qty,	
				'cgst_amount'=>$cgst_amount,	
				'sgst_amount'=>$sgst_amount,	
				'igst_amount'=>$igst_amount,	
				'utgst_amount'=>$utgst_amount,	 
				'base_amount'=>$base_amount,	 
				'taxable_value'=>$taxable_value,	 
				'discount_amount'=>$discount_amount,	 
				'total_gst'=>$total_gst,	   
				'row_total'=>$row_total, 	
				'total_price'=>$row_total, 	 
				'net_admin_comm'=>$net_admin_comm, 	
				'due_to_seller'=>$due_to_seller, 	 
				'pay_to_seller'=>$pay_to_seller, 	  
				'shipping_base'=>$logistic_base, 	  
				'shipping_gst'=>$logistic_tax, 	  
				'product_shipping'=>$order_shipping_amount, 	  
				'product_base'=>$product_base, 	  
			);
			
			
			
		}  
		 
		/* echo "<pre>";
			print_r($mainArray);
		echo "</pre>";  */
		 
		
		return json_encode($mainArray);
		
		
		
		
	}


	public function order_item_details_product($order_increment_id,$product_id)   
	{ 
		  
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$order_inc_id=$order_increment_id;
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		
		$orderArray=$order->getData();
		 
		$check=$this->check_order_tax_list_exist($order_inc_id);
		if($check==0) 
		{	
			$common_helpers->report_insert_tax_parameter($order_inc_id);     
		} 
		
		$product_admin_comm_rate=$common_helpers->admin_comm_by_product($order_inc_id,$product_id);  
		
		
		$buyer_id=$orderArray['customer_id']; 
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst'];  
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		
		
		$shippingAmount=0; 
		$total_shippingAmount=0;
		$special_price=0;
		$sale=0;
		$discountAmount=0;
		$total_gst=0;
		$total_price=0;
		$total_gst=0;
		
		
		
		
		
		foreach ($order->getAllItems() as $item)
		{
			
			$product_idd=$item->getProductId(); 
			if($product_id==$product_idd)
			{	
				$product_id=$item->getProductId(); 
				$product_name=$item->getName(); 
				$product_price=$item->getPrice();
				$product_qty=$item->getQtyOrdered();
				
				$row_total=$product_qty*$product_price;
				
				//$product = $productRepository->getById($product_id); 
				
				

				$select = $connection->select()
					  ->from('sales_order_item') 
					  ->where('order_id = ?', $order_id)
					  ->where('product_id = ?', $product_id);
		   
				$result = $connection->fetchAll($select);
				
				$cgst_amount=0;	
				$sgst_amount=0;	
				$igst_amount=0;	
				$utgst_amount=0;	 
				$discount_amount=0;	 
				
				if(!empty($result))
				{
					$cgst_amount=$result[0]['cgst_amount'];	
					$sgst_amount=$result[0]['sgst_amount'];	
					$igst_amount=$result[0]['igst_amount'];	
					$discount_amount=$result[0]['discount_amount'];	
					
					if(isset($result[0]['utgst_amount']))
					{
						$utgst_amount=$result[0]['utgst_amount'];		
					}	
					
				}	 

				
				$total_gst=$cgst_amount+$sgst_amount+$igst_amount+$utgst_amount;
				$base_amount=$row_total-$total_gst;
				
				
				$taxable_value=$base_amount*$product_qty;
				
				
				
				
				$base_amount=number_format($base_amount, 2, '.', '');
				$taxable_value=number_format($taxable_value, 2, '.', ''); 
				$total_gst=number_format($total_gst, 2, '.', '');     
				
				//$total_gst=$api_helpers->custom_number_format($total_gst);     
				$cgst_amount=number_format($cgst_amount, 2, '.', ''); 
				$sgst_amount=number_format($sgst_amount, 2, '.', '');
				$igst_amount=number_format($igst_amount, 2, '.', '');
				$utgst_amount=number_format($utgst_amount, 2, '.', '');
				$discount_amount=number_format($discount_amount, 2, '.', '');
				$row_total=number_format($row_total, 2, '.', '');
				$product_price=number_format($product_price, 2, '.', '');
				
				
				$tax_rates_array=json_decode($this->tax_order_parameter($order_inc_id));
			
				//print_r($tax_rates_array);
				
				$total_amount_xxx=$row_total; 
				
				$admin_comm_rate=$tax_rates_array->admin_comm;
				$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
				
				
				$admin_comm=$this->tax_admin_commission($total_amount_xxx,$order_inc_id);
				$admin_comm_tax=$this->tax_admin_commission_tax($total_amount_xxx,$order_inc_id);
				
				
				$total_admin_comm=$admin_comm + $admin_comm_tax; 
				
				
				$admin_comm_tds=(($admin_comm_rate/100)*$total_amount_xxx)*($admin_comm_tds_rate/100);
				
				
				
				$net_admin_comm=$total_admin_comm - $admin_comm_tds;
				$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
				
				
				$due_to_seller=$total_amount_xxx-$net_admin_comm;
				$due_to_seller=number_format($due_to_seller, 2, '.', '');
				
				
				//$pincode=$custom_helpers->get_customer_shipping_address($buyer_id,'postcode');
				$order_id=$order->getId();  
				$pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
				
				$shippingAmount=0; 
				if($pincode!='0')  
				{	
					$shippingAmount=$helpers->ecom_price_by_product($pincode,$product_id,$product_qty); 
				}   
				
				$product_base=$row_total-$total_gst;
				
				$order_shipping_amount=$shippingAmount;
				
				$logistic_tax=$this->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
				$logistic_tds=$this->tax_logistic_tds($order_shipping_amount,$order_inc_id);

				$logistic_base=$order_shipping_amount-$logistic_tax;
				$gst_tcs_rate=$tax_rates_array->gst_tcs;
				$gst_tcs_calculate_amount=$base_amount+$logistic_base; 
						
				//$gst_tcs_on_total_amount=$this->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id); 

				$gst_tcs_on_total_amount=($gst_tcs_calculate_amount*$gst_tcs_rate)/100; 

				//$pay_to_seller=$order_total_amount-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
				$pay_to_seller=$due_to_seller-$gst_tcs_on_total_amount;
				
				
				$net_admin_comm=$net_admin_comm+$gst_tcs_on_total_amount;
			
				$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
				//$mainArray=array(); 
				
				$product_discount=0;
				
				$net_sale_value=$product_base-$product_discount;
				$product_invoice_value=$net_sale_value+$total_gst;  
				//$admin_comm=(($product_invoice_value*$product_admin_comm_rate)/100);
				$admin_comm=(($net_sale_value*$product_admin_comm_rate)/100); 
				
				$mainArray[]=array(
					'product_id'=>$product_id,	
					'product_name'=>$product_name,	
					'product_price'=>$product_price,	
					'product_qty'=>$product_qty,	
					'cgst_amount'=>$cgst_amount,	
					'sgst_amount'=>$sgst_amount,	
					'igst_amount'=>$igst_amount,	
					'utgst_amount'=>$utgst_amount,	 
					'base_amount'=>$base_amount,	 
					'taxable_value'=>$taxable_value,	 
					'discount_amount'=>$discount_amount,	 
					'total_gst'=>$total_gst,	   
					'row_total'=>$row_total, 	
					'total_price'=>$row_total, 	 
					'net_admin_comm'=>$net_admin_comm, 	
					'due_to_seller'=>$due_to_seller, 	 
					'pay_to_seller'=>$pay_to_seller, 	  
					'shipping_base'=>$logistic_base, 	  
					'shipping_gst'=>$logistic_tax, 	  
					'product_shipping'=>$order_shipping_amount, 	  
					'product_base'=>$product_base, 	  
					'product_admin_comm_rate'=>$product_admin_comm_rate, 	  
					'product_discount'=>$product_discount, 	   
					'net_sale_value'=>$net_sale_value, 	   
					'product_invoice_value'=>$product_invoice_value, 	   
					'admin_comm'=>$admin_comm, 	   
				);
			}	
			
		}  
		 
		/* echo "<pre>";
			print_r($mainArray);
		echo "</pre>";  */
		 
		
		//return json_encode($mainArray);
		return $mainArray; 
		
		
		
		
	}



	


	public function order_item_details_new($order_increment_id)  
	{ 
		  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$order_inc_id=$order_increment_id;
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		
		$orderArray=$order->getData();
		//print_r($orderArray);
		//die;

		$buyer_id=$orderArray['customer_id']; 
		
		$check=$this->check_order_tax_list_exist($order_inc_id);
		if($check==0) 
		{	
			$tax_helpers->insert_tax_parameter($order_inc_id);   
		} 
		
		
		
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		$cgst_amount=(int)$gstArray['cgst']; 
		$sgst_amount=(int)$gstArray['sgst'];  
		$igst_amount=(int)$gstArray['igst']; 
		$ugst_amount=(int)$gstArray['utgst'];  
		
		
		
		$shippingAmount=0; 
		$total_shippingAmount=0;
		$special_price=0;
		$sale=0;
		$discountAmount=0;
		$total_gst=0;
		$total_price=0;
		$total_gst=0; 
		
		
		
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId(); 
			$product_qty=$item->getQtyOrdered();
			
			$pincode=$custom_helpers->get_customer_shipping_address($buyer_id,'postcode');
			if($pincode!='0') 
			{	
		
				echo $shippingAmount=$helpers->ecom_price_by_product($pincode,$product_id,$product_qty); 
				echo "<br>";
				 
				 
			}  
			
			
		}    
		 
		
		  
	} 
	 
	function get_order_configuration($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = "select * from order_tax_list where order_id='".$order_inc_id."' order by id desc limit 0,1";
        $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return $result[0]['tax_list']; 
		}
		else
		{
			return 0;
		}
	}		
	
	
	
	
}
