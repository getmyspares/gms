<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Commreport extends AbstractHelper
{
    
	
	
	public function store_category_comm_temp_report() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$comm_report_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\commreport'); 

		$sql="select * from category_comm_temp_report";
		$result = $connection->fetchAll($sql);
		$comm_json=$comm_report_helpers->category_comm_report_array();
		
		
		
		
		$today=date('Y-m-d H:i:s'); 
		if(empty($result))
		{
			$insert="insert into category_comm_temp_report (details,cr_date) values ('".$comm_json."','".$today."')";
			$connection->query($insert);  	
		}  	
		
		
		
		
		//category_comm_list
		
		
		
			
	}
	
	
	public function create_category_comm_report() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$comm_report_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\commreport'); 
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
		
		$json=$comm_report_helpers->category_comm_report_array();
		
		$sql = "Select * FROM category_comm_report";
		$result = $connection->fetchAll($sql); 
		
		if(empty($result))
		{	  
			$comm_report_helpers->create_new_row($json,'new'); 
		}    
		else  
		{ 
			$sql = "Select * FROM category_comm_temp_report";
			$results = $connection->fetchAll($sql); 
			$stored_json=$results[0]['details']; 
			
			
			
			
			
			//$it_1 = json_decode($stored_json);
			//$it_2 = json_decode($json);
			$change=0;
			if($stored_json!=$json)
			{
				$change=1;
			}	
			
			
			
			if($change==1)
			{
				$json; 
			}	
			
			
			
			//$difference = array_diff_assoc($it_1,$it_2);
			
			
			
			
			if($change==1)
			{
				$comm_report_helpers->create_new_row($json,'update');  	
				$comm_report_helpers->update_temp_table($json); 	
			}	 
			   
			
		}  	 
	}
	
	
	public function category_comm_report_array()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categories = $categoryHelper->getStoreCategories();
		
		$categoryArray=array();
		foreach ($categories as $category) 
		{
			
			$cat_id=$category->getId();
			$cat_comm=$common_helpers->get_single_cat_comm($cat_id);
			
			$categoryArray[]=array(
				'id'=>$category->getId(),
				'name'=>$category->getName(),
				'cat_comm'=>$cat_comm
			);
			
		} 
		
		
		return json_encode($categoryArray);
		
		
	}	
	
	
	public function update_temp_table($json) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$created_at=date('Y-m-d H:i:s');  
		 
		$sql="Update category_comm_temp_report set details='".$json."'";
		$connection->query($sql);    
		
	}	


	
	
	
	
	public function create_new_row($json,$reason) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$comm_report_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\commreport'); 
		$comm_json=$comm_report_helpers->category_comm_report_array();
		
		$today=date('Y-m-d H:i:s');
		
		if(empty($result))
		{
			$insert="insert into category_comm_report (details,cr_date,reason) values ('".$comm_json."','".$today."','".$reason."')";
			$connection->query($insert);  	 
		}			
	}	
	
	
	public function update_cat_list()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$comm_report_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\commreport'); 
		
		$comm_json=$comm_report_helpers->category_comm_report_array();
		$catArray=json_decode($comm_json);
		
		foreach($catArray as $cat)
		{
			
			$cat_id=$cat->id;
			$cat_name=$cat->name;
			
			$sql="select * from category_comm_list where cat_id='".$cat_id."' and name='".$cat_name."'";
			$result = $connection->fetchAll($sql);
			if(empty($result))
			{
				$insert="insert into category_comm_list (cat_id,name) values ('".$cat_id."','".$cat_name."')";
				$connection->query($insert);  
			}	
			
		}	
		
	}	
	
	
	
}