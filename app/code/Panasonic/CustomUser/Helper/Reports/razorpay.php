<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Razorpay extends AbstractHelper
{
    
	
	function razorpay_report($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
			
		if(!empty($order_id)) 
		{	
			$orderArray=$order->getData();  
			$payment_method=$order->getPayment()->getMethod(); 
			if($payment_method!='cashondelivery')
			{
				
				$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
				
				 

				$buyer_id=$orderArray['customer_id'];
				$created_at=$orderArray['created_at'];
				$subtotal=$orderArray['subtotal'];
				$gst_array=$sales_helpers->report_order_gst($order_inc_id);
				$total_gst=$gst_array['total_gst'];
				$shipping_amount=$orderArray['shipping_amount'];
				$grand_total=$orderArray['grand_total'];
				
				
				$buyer_billing=$sales_helpers->report_customer_billing($order_inc_id);
				$buyer_shipping=$sales_helpers->report_customer_shipping($order_inc_id);
				$buyer_name=$buyer_billing['buyer_name'];
				$buyer_city = $buyer_billing['buyer_city'];
				$buyer_state =$buyer_billing['buyer_state'];
				$buyer_pincode = $buyer_billing['buyer_pincode'];
				$buyer_country_id = $buyer_billing['buyer_country_id'];
				$buyer_address = $buyer_billing['buyer_address'];
				
				$shipping_city = $buyer_shipping['shipping_city'];
				$shipping_adress = $buyer_shipping['shipping_address'];
				
				
				
				
				$buyer_name=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $buyer_name);
				$buyer_city=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $buyer_city);
				$shipping_city=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $shipping_city); 
				$buyer_address=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $buyer_address);
				$shipping_adress=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $shipping_adress);
				
				
				
				
				
				/* echo "<pre>";
					print_r($orderArray);
				echo "</pre>";  */ 
				
				$payment_ref='';
				
				$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
				$check_mobile=$api_helpers->check_order_from_mobile($order_inc_id);
				
				if($check_mobile==0)   
				{
					$select = $connection->select()
						  ->from('sales_payment_transaction') 
						  ->where('order_id = ?', $order_id);
				
					$result = $connection->fetchAll($select); 
					if(!empty($result))
					{	
						$payment_id=$result[0]['txn_id'];
					}
					else
					{
						exit();
					}
				}
				else
				{
					$select = $connection->select()
						  ->from('mobile_sales_order') 
						  ->where('order_increment_id = ?', $order_inc_id);
					
					$result = $connection->fetchAll($select);
					
					$payment_id=$result[0]['payment_details'];  
				}	
			
				$payment_ref=$payment_id; 
				
				
				$razor=$razorpay_helpers->razorpay_info($payment_id);
				
				//print_r($razor);
				
				$razor_amount=0;
				$razor_fee=0;
				$razor_gst=0;
				
				if($razor!='0')	
				{ 
					if(isset($razor->amount))
					{	
						$razor_amount=$razor->amount;
					}
					if(isset($razor->fee))
					{	
						if( (int)$razor_amount == (int)$grand_total ){
							$razor_fee=$razor->fee;
						}else{
							$razor_fee = ($razor->fee/$razor_amount)*$grand_total;
						}
					}
					if(isset($razor->tax))
					{	
						$razor_gst=$razor->tax;
					}
					//$razor_fee=$razor->fee;
					//$razor_gst=$razor->tax; 
					
					
					$razorpay_total=($grand_total*$razor_fee)/100;
					$razorpay_total=number_format($razorpay_total, 2, '.', '');
					
					
					
					
					$razorpay_charges=$razor_fee-$razor_gst;
					if($razorpay_charges < 0)
					{
						$razorpay_charges=0;	
					}	
					
					
					$razorpay_charges=number_format($razorpay_charges, 2, '.', '');
					
					
					$razorpay_charges_terms=($razor_fee/$grand_total)*100;
					
					$razorpay_charges_terms=number_format($razorpay_charges_terms, 2, '.', ''); 
					
					$nodal_amount=$grand_total-$razor_fee;
					 
					if($nodal_amount < 0) 
					{
						$nodal_amount=0; 
					}  
					 
					$nodal_amount=number_format($nodal_amount, 2, '.', ''); 
					
					
					$subtotal=$grand_total-$total_gst-$shipping_amount;
					
					$subtotal=number_format($subtotal, 2, '.', ''); 
					
					$mainarray=array(  
						'order_inc_id'=>$order_inc_id,	
						'invoice_id'=>$invoice_id,	
						'created_at'=>$created_at,  
						'base_amount'=>$subtotal, 
						'gst_amount'=>$total_gst, 
						'logistic_amount'=>$shipping_amount, 
						'total_order_value'=>$grand_total,
						'buyer_id'=>$buyer_id, 	
						'buyer_name'=>$buyer_name, 	
						'billing_address'=>$buyer_address, 	
						'billing_city'=>$buyer_city, 	 
						'shipping_address'=>$shipping_adress, 	
						'shipping_city'=>$shipping_city, 	 
						'payment_id'=>$payment_ref, 	  
						'razorpay_charges_terms'=>$razorpay_charges_terms, 	  
						'razorpay_charges'=>$razorpay_charges, 	  
						'razorpay_gst'=>$razor_gst, 	  
						'razorpay_total'=>$razor_fee, 	  
						'nodal_amount'=>$nodal_amount, 	  
						'nodal_transfer_date'=>'', 	  
						'nodal_trf_payment_id'=>'', 	  
						'remarks'=>'', 	  
					);	   
					  
					
					/* echo "<pre>";
						print_r($mainarray);
					echo "</pre>";  */
					 
					
					$json=json_encode($mainarray);
				 
							
					$sql="INSERT INTO `razorpay_report`(`order_id`, `invoice_id`, `cr_date`, `details`) VALUES ('".$order_inc_id."','".$invoice_id."','".$created_at."','".$json."')";
					 
					$connection->query($sql);  
					 
				} 
			}
		}
		
	}	
	
	
	
	function razorpay_info($payment_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		

		$url=$site_url.'qbonline/connection/razorpayment/order/'.$payment_id;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$response  = curl_exec($ch); 
		if($response!='')
		{	
			return $result=json_decode($response);
		} 
		else
		{
			return 0; 
		}
		
	}

	function check_razorpay_report($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$select = $connection->select()
			  ->from('razorpay_report') 
			  ->where('order_id = ?', $order_inc_id); 
	
		$result = $connection->fetchAll($select); 
		if(!empty($result))
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}	

	
	function razorpay_report_details($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		if(!empty($order_id))
		{	
			$orderArray=$order->getData();  
			$payment_method=$order->getPayment()->getMethod(); 
			if($payment_method!='cashondelivery')
			{
				
				$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
				
				
				
				$buyer_id=$orderArray['customer_id'];
				$created_at=$orderArray['created_at'];
				$subtotal=$orderArray['subtotal'];
				$gst_array=$sales_helpers->report_order_gst($order_inc_id);
				$total_gst=$gst_array['total_gst'];
				$shipping_amount=$orderArray['shipping_amount'];
				$grand_total=$orderArray['grand_total'];
				
				$payment_ref='';
				
				$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
				$check_mobile=$api_helpers->check_order_from_mobile($order_inc_id);
				
				if($check_mobile==0)   
				{
					$select = $connection->select()
					->from('sales_payment_transaction') 
					->where('order_id = ?', $order_id); 
					
					$result = $connection->fetchAll($select); 
					if(!empty($result))
					{	
						$payment_id=$result[0]['txn_id'];
					}
					else
					{
						/* removed  die  statement vdue to  which application used  to die when condition met
							valid type  should  be return 0 @ritesh
						*/
						return 0;
					}
				}
				else
				{
					$select = $connection->select()
					->from('mobile_sales_order') 
					->where('order_increment_id = ?', $order_inc_id);
					
					$result = $connection->fetchAll($select);
					
					$payment_id=$result[0]['payment_details'];  
				}	
				
				$payment_ref=$payment_id; 
				
				
				$razor=$razorpay_helpers->razorpay_info($payment_id);
				
				//print_r($razor);
				//die;
				$razor_amount=0;
				$razor_fee=0;
				$razor_gst=0;
				
				if($razor!='0')	
				{ 
					if(isset($razor->amount))
					{	
						$razor_amount=$razor->amount;
					}
					if(isset($razor->fee))
					{	
						if( (int)$razor_amount == (int)$grand_total ){
							$razor_fee=$razor->fee;
						}else{
							$razor_fee = ($razor->fee/$razor_amount)*$grand_total;
						}
					}
					if(isset($razor->tax))
					{	
						$razor_gst=$razor->tax;
					}
					//$razor_fee=$razor->fee;
					//$razor_gst=$razor->tax; 
					
					
					$razorpay_total=($grand_total*$razor_fee)/100;
					$razorpay_total=number_format($razorpay_total, 2, '.', '');
					
					
					
					
					$razorpay_charges=$razor_fee-$razor_gst;
					if($razorpay_charges < 0)
					{
						$razorpay_charges=0;	
					}	
					
					
					$razorpay_charges=number_format($razorpay_charges, 2, '.', '');
					
					
					$razorpay_charges_terms=($razor_fee/$grand_total)*100;
					
					$razorpay_charges_terms=number_format($razorpay_charges_terms, 2, '.', ''); 
					
					$nodal_amount=$grand_total-$razor_fee;
					
					if($nodal_amount < 0) 
					{
						$nodal_amount=0; 
					}  
					
					$nodal_amount=number_format($nodal_amount, 2, '.', ''); 
					
					
					$subtotal=$grand_total-$total_gst-$shipping_amount;
					
					$subtotal=number_format($subtotal, 2, '.', ''); 
					
					
					if($razor_amount==0 || $razorpay_charges==0)
					{
						$razorpay_charges_terms=0;
					} 
					else
					{			
						$razorpay_charges_terms=($razorpay_charges/$razor_amount)*100;
					}
					$razorpay_charges_terms=number_format($razorpay_charges_terms, 2, '.', '');  
					
					$razor_payment=0;
					$razor_card=0;
					$razor_network=0;    
					
					if(isset($razor->payment))
					{	
						$razor_payment=$razor->payment;
					}
					
					if(isset($razor->card))
					{	
						$razor_card=$razor->card;
					}
					
					if(isset($razor->network))
					{	
						$razor_network=$razor->network;
					}
					
					
					$mainarray=array(   
						'payment_id'=>$payment_ref, 	  
						'razorpay_charges_terms'=>$razorpay_charges_terms, 	  
						'razorpay_charges'=>$razorpay_charges, 	  
						'razorpay_gst'=>$razor_gst, 	  
						'razorpay_total'=>$razor_fee, 	  
						'nodal_amount'=>$nodal_amount,  
						'razor_amount'=>$razor_amount,  
						'razor_fee'=>$razor_fee,  
						'razor_tax'=>$razor_gst,   
						'razor_payment'=>$razor_payment,   
						'razor_card'=>$razor_card,   
						'razor_network'=>$razor_network,   
						
					);	     
					
					
					return $json=json_encode($mainarray);
				} 
			}
			else
			{
				return 0;
			}		
		}
		
	}
	
	
	
	
}
