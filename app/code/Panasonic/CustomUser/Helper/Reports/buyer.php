<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Buyer extends AbstractHelper
{
    
	function get_buyer_reports() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
		
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		
		
		$today=date('Y-m-d'); 
		
		$yesterday=date('Y-m-d',strtotime("-1 days"));
		
		//$select = "SELECT * FROM `customer_entity` where DATE(created_at) in ('".$today."','".$yesterday."')";
		$select = "SELECT * FROM `customer_entity`";
               
		$result = $connection->fetchAll($select);
		
		if(!empty($result)) 
		{
		
			foreach($result as $row)
			{
				$customer_id=$row['entity_id'];	
				
				$select = $connection->select()
							  ->from('marketplace_userdata') 
							  ->where('seller_id = ?', $customer_id);
				$results = $connection->fetchAll($select);
				if(empty($results))
				{
					$sellerArray[]=$customer_id; 
				}
		 
			}
			if(!empty($sellerArray))
			{	
				foreach($sellerArray as $newarray)
				{
					$newcustomerId[] = $newarray;
					//$buyer_helpers->getBuyerData($newarray);
					
					
					$buyer_helpers->store_buyer_temp_data($newarray); 
					$buyer_helpers->create_buyer_report($newarray);  
				  
				}
			}
		}
				
		
		
	}

	
	function getBuyerData($newcustomerId)
	{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
			$customerObj = $objectManager->create('Magento\Customer\Model\Customer')
			->load($newcustomerId);
			$obj = $objectManager->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
			$buyer_ip =  '';
			$newData = $customerObj->getData();
			  $default_billing=$newData['default_billing'];
			  $default_shipping=$newData['default_shipping'];
			  $newdefault = $api_helpers->address_type($default_billing);
			 
			  $newAaary = $api_helpers->get_user_details($newcustomerId);
			  if(!empty($newcustomerId))
			  {
				$mobile='';
				if(!empty($newAaary[0]['mobile']))
				{
					 $mobile =  $newAaary[0]['mobile'];
				}
				else
				{ 
					$mobile='';
				}
				$buyer_typeofbusiness='';
			$buyer_firstname=$custom_helpers->shipping_address_billing_id($default_billing,'firstname','billing'); 
			$buyer_lastname=$custom_helpers->shipping_address_billing_id($default_billing,'lastname','billing'); 
			
			$buyer_name=$buyer_firstname.' '.$buyer_lastname;
			$buyer_city = $custom_helpers->shipping_address_billing_id($default_billing,'city','billing');
			$buyer_street = $custom_helpers->shipping_address_billing_id($default_billing,'street','billing');
			
			$buyer_state = $custom_helpers->shipping_address_billing_id($default_billing,'region','billing');
			
			
			$buyer_pincode = $custom_helpers->shipping_address_billing_id($default_billing,'postcode','billing');
			$buyer_country_id = $custom_helpers->shipping_address_billing_id($default_billing,'country_id','billing');
			$buyer_address='';
		    $buyer_street = '';
			$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
			
			$shipping_firstname=$custom_helpers->get_customer_shipping_address($default_billing,'firstname','shipping'); 
			$shipping_lastname=$custom_helpers->get_customer_shipping_address($default_billing,'lastname','shipping'); 

			$shipping_name=$shipping_firstname.' '.$shipping_lastname;
			$shipping_city = $custom_helpers->get_customer_shipping_address($default_billing,'city','shipping');
			$shipping_street = $custom_helpers->get_customer_shipping_address($default_billing,'street','shipping');

			$shipping_state = $custom_helpers->get_customer_shipping_address($default_billing,'region','shipping');
			$shipping_pincode = $custom_helpers->get_customer_shipping_address($default_billing,'postcode','shipping');
			$shipping_country_id = $custom_helpers->get_customer_shipping_address($default_billing,'country_id','shipping');
			
			$shipping_address = $shipping_street.$shipping_city.', '.$shipping_state.', '.$shipping_country_id.', '.$shipping_pincode;

		 
			 
			 /*  foreach($newAaary as $value)
			  {
				  echo $value['mobile'];  
			  
		     
			  } */
			  
			    // $newoneMobile = $newone;
				//print_r($newoneMobile);
                $buyer_id=$newData['entity_id'];
				$created_at=$newData['created_at'];
				$group_id=$newData['group_id'];		 	
				$created_at=$newData['created_at'];			
				$group_id=$newData['group_id'];			
				$firstname=$newData['firstname'];			
				$email=$newData['email'];	 		
				$lastname=$newData['lastname'];			
				$gender=$newData['gender'];	
				
				
				$rewards_subscription='';
         		
			    $groupRepository  = $objectManager->create('\Magento\Customer\Api\GroupRepositoryInterface');
				$group = $groupRepository->getById($group_id);
				
				$groupName = $group->getCode();
				
				$groupName = 'Company Buyer';
				
				if($group_id==1)
				{
					$groupName = 'Individual';
				}
				else if($group_id==5) 
				{
					$groupName = 'Technician';
				}	
				 
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			   $connection = $resource->getConnection();
				$attribute_approved='';
				$pan_number='';
				$gst_number='';
				$lock_expires='';
				
			   $select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('entity_id = ?', $newcustomerId)
                  ->where('attribute_id = ?', '172');
                  
			$result = $connection->fetchAll($select);
			
			
			$count=count($result);
			if($count!=0)
			{
				$gst_number=preg_replace("/[^a-zA-Z 0-9]+/", "",$result[0]['value']);
			} 
			//company information
			 $select = $connection->select()
                  ->from('customer_entity_int') 
                  ->where('entity_id = ?', $newcustomerId)
                  ->where('attribute_id = ?', '177');
                  
			$resultcomp = $connection->fetchAll($select);
			
			
			$counts=count($resultcomp);
		
			if($counts!=0) 
			{
				 $resultcomp= $resultcomp[0]['value'];
				
					
			} 
			
			//mobile get
			
			
			$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('entity_id = ?', $newcustomerId)
                  ->where('attribute_id = ?', '44');
                  
			$results = $connection->fetchAll($select);
			
			
			$counts=count($results);
			if($counts!=0)
			{
				$lock_expires=preg_replace("/[^a-zA-Z 0-9]+/", "",$results[0]['value']);
			} 
			
			
			
			$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('entity_id = ?', $newcustomerId)
                  ->where('attribute_id = ?', '175');
                  
			$resultt = $connection->fetchAll($select);
			
			
			$count=count($resultt);
			if($count!=0)
			{
				$pan_number=preg_replace("/[^a-zA-Z 0-9]+/", "",$resultt[0]['value']);
			} 
			 $accountconfirm=''; 
			 $buyer_status='';
			$accountManagement = $objectManager->get('\Magento\Customer\Api\AccountManagementInterface');
			$accountconfirm = $accountManagement->getConfirmationStatus($newcustomerId);
			if($accountconfirm =='account_confirmed' && $resultcomp=='1')
			{
				$buyer_status= 1;
				
			}
			else
			{
				$buyer_status=0;
			}
			

			$buyer_status=$buyer_helpers->get_customer_status($newcustomerId);			
			$buyer_locked=$buyer_helpers->get_customer_status($newcustomerId);	
			 
			$buyer_lockexpires="Un Locked";
			
			if($buyer_locked=="Locked")
			{
				$buyer_lockexpires="Locked";
			}


			
			$mianarray=array(
			'buyer_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_id),	
			'group_id'=>$group_id, 	
			'buyer_name'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_name),	
			'default_billing'=>preg_replace('/[^A-Za-z0-9. -]/', '', $default_billing),	
			'default_shipping'=>preg_replace('/[^A-Za-z0-9. -]/', '', $default_shipping),	
			'gender'=>preg_replace('/[^A-Za-z0-9. -]/', '', $gender),	
			'buyer_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_address),	
			'billing_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_city),	
			'billing_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_state),	
			'billing_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_pincode),		
			'billing_country_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_country_id),	
			'buyer_email'=>$email, 
			'buyer_mobile'=>preg_replace('/[^A-Za-z0-9. -]/', '', $mobile),
			'buyer_group'=>preg_replace('/[^A-Za-z0-9. -]/', '', $groupName),
			'created_at'=>$created_at,	 					
		    'buyer_status'=>$buyer_status,  
		    'buyer_panno'=>preg_replace('/[^A-Za-z0-9. -]/', '', $pan_number),
		    'buyer_gstnumber'=>preg_replace('/[^A-Za-z0-9. -]/', '', $gst_number),
		    'buyer_typeofbusiness'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_typeofbusiness),
		    'buyer_lockexpires'=>$buyer_lockexpires,  
		    'buyer_rewardtier'=>preg_replace('/[^A-Za-z0-9. -]/', '', $rewards_subscription),
		    'buyer_ip'=>$buyer_ip, 
				
		);
	/* 
		
		echo "<pre>";
			print_r($mianarray);
		echo "</pre>"; */
				 
		
		
		$json=json_encode($mianarray);
	
	
	
		$sql = "Select * FROM buyer_report where buyer_id='".$newcustomerId."'";
		$result = $connection->fetchAll($sql); 
		
		if(empty($result))
		{	  
		$sql="INSERT INTO `buyer_report`(`buyer_id`,`created_date`, `details`) VALUES ('".$newcustomerId."','".$created_at."','".$json."')";
		$connection->query($sql);  
		}  
		
		
		/* 
		if(empty($result))
		{
		//	
		}
		else
		{
		$resultd=$result[0];
		$newId = $resultd['buyer_id'];
		
		 if($newId==$newcustomerId)
		 {
				echo "";
	     }
		 } 
		else
		{ 
		
			$sql="INSERT INTO `buyer_report`(`buyer_id`,`created_date`, `details`) VALUES ('".$newcustomerId."','".$created_at."','".$json."')";
	       $connection->query($sql);  
		}
		*/
	
	}
	//return $newcustomerId;
	}
	
	
	public function get_customer_status($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
		
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
		
		$select = $connection->select()
                  ->from('customer_entity') 
                  ->where('entity_id = ?', $user_id);
                  
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			$status="Verified";      
				
			
			$group_id=$result[0]['group_id'];
			$company=0;
			if($group_id==4)
			{	
				$company=1;
				$select = $connection->select()
					  ->from('customer_entity_int') 
					  ->where('entity_id = ?', $user_id)
					  ->where('attribute_id = ?', '177'); 
					  
				$results = $connection->fetchAll($select);
				
				$value=0;
				
				if(!empty($results))
				{
					$value=$results[0]['value'];	
				}
				
						
			}
			
			$email_confirmation=$result[0]['confirmation'];
			$lock_expires=$result[0]['lock_expires'];
		
			
			
				
			if($email_confirmation!='')
			{
				$status="Email Not Confirmed";	
			}
			else if($company==1)
			{
				if($value==1)
				{
					$status="Verified";	
				}		
				else 
				{
					$status="Not Verified";	
				}		 
			}		
			else if($lock_expires!='')
			{
				$status="Locked";	
			}	
			  
			return $status;	
		}	
		else
		{
			return "Deleted";
		}		
		
		
	}


	function store_buyer_temp_data($newcustomerId)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
			
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 
		
		$mianarray=$buyer_helpers->customer_report_array($newcustomerId);
		 
		
		/* echo "<pre>";
			print_r($mianarray);
		echo "</pre>"; 
		die; */  
				  
		
		
		$json=json_encode($mianarray);
		
	
		$sql = "Select * FROM customer_temp_report where user_id='".$newcustomerId."'";
		$result = $connection->fetchAll($sql); 
		
		if(empty($result))
		{	  
			$sql="INSERT INTO `customer_temp_report`(`user_id`,`user_details`) VALUES ('".$newcustomerId."','".$json."')";
			$connection->query($sql);
		}   
		 
		   
	
	}
	 
	public function create_buyer_report($newcustomerId) 
	{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection(); 
			
			$mianarray=$buyer_helpers->customer_report_array($newcustomerId);
					 
			
			 
			/* echo "<pre>";
				print_r($mianarray); 
			echo "</pre>";	     */
			
			
			$default_shipping=$mianarray[0]['default_shipping'];
			$default_billing=$mianarray[0]['default_billing'];
			
			
			
			$json=json_encode($mianarray); 
			$today=date('Y-m-d h:i:s');  
		
			$sql = "Select * FROM buyer_report where buyer_id='".$newcustomerId."'";
			$result = $connection->fetchAll($sql); 
			
			if(empty($result))
			{	  
				$buyer_helpers->create_new_row($newcustomerId,$json,'new'); 
			}    
			else
			{
					
					$sql = "Select * FROM customer_temp_report where user_id='".$newcustomerId."'";
					$results = $connection->fetchAll($sql); 
					
					/* echo "<pre>";
						print_r($results); 
					echo "</pre>";	  */ 
					
					$user_details=$results[0]['user_details'];	
					$user_details=json_decode($user_details);	
					
					/* echo "<pre>";
						print_r($user_details); 
					echo "</pre>";		 */
					
					// My Profile Change
					
					
					$original_myAccount_json=json_encode($mianarray[2]);
					$stored_myAccount_json=json_encode($user_details[2]);
					
					$check_change=$buyer_helpers->check_data_change($original_myAccount_json,$stored_myAccount_json);
					if($check_change==1)
					{
						$user_details[0]=$mianarray[0];
						$user_details[2]=$mianarray[2]; 
						
						/* echo "<pre>";
							print_r($user_details); 
						echo "</pre>"; */
						
						$json=json_encode($user_details);   
						
						$buyer_helpers->create_new_row($newcustomerId,$json,'My Profile');  	
						$buyer_helpers->update_temp_user_table($newcustomerId,$json); 	
						 
						//echo "account";
					
					}	
					 
					
					
					if($default_shipping!=0)  
					{        
					 
						$billing_change=0;	 
						$shipping_change=0;	 
						$bil_check_change=0;	  
						$ship_check_change=0;	 

						$original_myAccount_json=json_encode($mianarray[3]);
						$stored_myAccount_json=json_encode($user_details[3]);
							
						$bil_check_change=$buyer_helpers->check_data_change($original_myAccount_json,$stored_myAccount_json);

						if($bil_check_change==1)
						{
							$billing_change=1;	     
						}	

						$original_myAccount_json=json_encode($mianarray[4]);
						$stored_myAccount_json=json_encode($user_details[4]);

						$ship_check_change=$buyer_helpers->check_data_change($original_myAccount_json,$stored_myAccount_json);
						if($ship_check_change==1)  
						{
							$shipping_change=1;	
						 
						} 	
						
						 
						
						/* echo $billing_change;
						echo "<br>";
						echo $shipping_change;  */
						
						
						if($billing_change==1 && $shipping_change==1)
						{
							$user_details[0]=$mianarray[0];
							$user_details[3]=$mianarray[3];  
							$user_details[4]=$mianarray[4];  
							
							$json=json_encode($user_details);   
							
							$buyer_helpers->create_new_row($newcustomerId,$json,'billing_shipping');  	
							$buyer_helpers->update_temp_user_table($newcustomerId,$json); 	
							
						}
						else if($billing_change==1 && $shipping_change==0)
						{
							$user_details[0]=$mianarray[0];
							$user_details[3]=$mianarray[3];  
							
							$json=json_encode($user_details);   
							
							$buyer_helpers->create_new_row($newcustomerId,$json,'billing');  	
							$buyer_helpers->update_temp_user_table($newcustomerId,$json); 	
							
							
						}  
						else if($billing_change==0 && $shipping_change==1)
						{
							$user_details[0]=$mianarray[0];
							$user_details[4]=$mianarray[4];  
							
							$json=json_encode($user_details);   
							
							$buyer_helpers->create_new_row($newcustomerId,$json,'shipping');  	
							$buyer_helpers->update_temp_user_table($newcustomerId,$json); 	
							
						}	   
						
					}  
					
					
					
					
			}		
			 
		
	} 

	
				
	public function create_new_row($user_id,$json,$reason) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$created_at=date('Y-m-d H:i:s');  
		

		$sql="INSERT INTO `buyer_report`(`buyer_id`,`created_date`, `details`,`reason`) VALUES ('".$user_id."','".$created_at."','".$json."','".$reason."')"; 
		$connection->query($sql);
		
	}	  
	
	
	public function update_temp_user_table($user_id,$json) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$created_at=date('Y-m-d H:i:s');  
		 
		$sql="Update customer_temp_report set user_details='".$json."' where user_id='".$user_id."'";
		$connection->query($sql);    
		
	}	
	 
	public function customer_report_array($newcustomerId)
	{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
			$customerObj = $objectManager->create('Magento\Customer\Model\Customer')
			->load($newcustomerId);
			$obj = $objectManager->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
			$buyer_ip =  '';
			$newData = $customerObj->getData();
			
			/* echo "<pre>";  
				print_r($newData);	 
			echo "</pre>";  	 */
			
			  
			  $default_billing=$newData['default_billing'];
			  $default_shipping=$newData['default_shipping'];
			  $newdefault = $api_helpers->address_type($default_billing);
			 
			  $newAaary = $api_helpers->get_user_details($newcustomerId);
			  if(!empty($newcustomerId))
			  {
				$mobile='';
				if(!empty($newAaary[0]['mobile']))
				{
					 $mobile =  $newAaary[0]['mobile'];
				}
				else
				{ 
					$mobile='';
				}
				$buyer_typeofbusiness='';
				
				$buyer_firstname=$custom_helpers->shipping_address_billing_id($default_billing,'firstname','billing'); 
				$buyer_lastname=$custom_helpers->shipping_address_billing_id($default_billing,'lastname','billing'); 
				
				$buyer_firstname=$newData['firstname'];
				$buyer_lastname=$newData['lastname']; 
				$buyer_email=$newData['email'];  
				
						

				
				$billing_firstname = $custom_helpers->shipping_address_billing_id($default_billing,'firstname','billing');
				$billing_lastname = $custom_helpers->shipping_address_billing_id($default_billing,'lastname','billing');
				$buyer_city = $custom_helpers->shipping_address_billing_id($default_billing,'city','billing');
				$buyer_street = $custom_helpers->shipping_address_billing_id($default_billing,'street','billing');
				
				$buyer_state = $custom_helpers->shipping_address_billing_id($default_billing,'region','billing');
				
				
				$buyer_pincode = $custom_helpers->shipping_address_billing_id($default_billing,'postcode','billing');
				$buyer_country_id = $custom_helpers->shipping_address_billing_id($default_billing,'country_id','billing');
				$buyer_address='';
				
				$buyer_address = $buyer_street;
				
				
				$default_shipping;
				
				$shipping_firstname=$custom_helpers->shipping_address_billing_id($default_shipping,'firstname','shipping'); 
				$shipping_lastname=$custom_helpers->shipping_address_billing_id($default_shipping,'lastname','shipping'); 

				
				$shipping_city = $custom_helpers->shipping_address_billing_id($default_shipping,'city','shipping');
				$shipping_street = $custom_helpers->shipping_address_billing_id($default_shipping,'street','shipping');

				$shipping_state = $custom_helpers->shipping_address_billing_id($default_shipping,'region','shipping');
				$shipping_pincode = $custom_helpers->shipping_address_billing_id($default_shipping,'postcode','shipping');
				$shipping_country_id = $custom_helpers->shipping_address_billing_id($default_shipping,'country_id','shipping');
				 
				$shipping_address = $shipping_street;

			
				 
				 /*  foreach($newAaary as $value)
				  {
					  echo $value['mobile'];  
				  
				 
				  } */
				  
					// $newoneMobile = $newone;
					//print_r($newoneMobile);
					$buyer_id=$newData['entity_id'];
					$created_at=$newData['created_at'];
					$group_id=$newData['group_id'];		 	
					$created_at=$newData['created_at'];			
					$group_id=$newData['group_id'];			
					$firstname=$newData['firstname'];			
					$email=$newData['email'];	 		
					$lastname=$newData['lastname'];			
					$gender=$newData['gender'];	
					
					
					$rewards_subscription='';
					
					$groupRepository  = $objectManager->create('\Magento\Customer\Api\GroupRepositoryInterface');
					$group = $groupRepository->getById($group_id);
					
					$groupName = $group->getCode();
					
					$groupName = 'Company Buyer';
					
					if($group_id==1)
					{
						$groupName = 'Individual';
					}
					else if($group_id==5) 
					{
						$groupName = 'Technician';
					}	
					 
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				   $connection = $resource->getConnection();
					$attribute_approved='';
					$pan_number='';
					$gst_number='';
					$lock_expires='';
					
				   $select = $connection->select()
					  ->from('customer_entity_varchar') 
					  ->where('entity_id = ?', $newcustomerId)
					  ->where('attribute_id = ?', '172');
					  
				$result = $connection->fetchAll($select);
				
				
				$count=count($result);
				if($count!=0)
				{
					$gst_number=preg_replace("/[^a-zA-Z 0-9]+/", "",$result[0]['value']);
				} 
				//company information
				 $select = $connection->select()
					  ->from('customer_entity_int') 
					  ->where('entity_id = ?', $newcustomerId)
					  ->where('attribute_id = ?', '177');
					  
				$resultcomp = $connection->fetchAll($select);
				
				
				$counts=count($resultcomp);
			
				if($counts!=0) 
				{
					 $resultcomp= $resultcomp[0]['value'];
					
						
				} 
				
				//mobile get
				
				
				$select = $connection->select()
					  ->from('customer_entity_varchar') 
					  ->where('entity_id = ?', $newcustomerId)
					  ->where('attribute_id = ?', '44');
					  
				$results = $connection->fetchAll($select);
				
				
				$counts=count($results);
				if($counts!=0)
				{
					$lock_expires=preg_replace("/[^a-zA-Z 0-9]+/", "",$results[0]['value']);
				} 
				
				
				
				$select = $connection->select()
					  ->from('customer_entity_varchar') 
					  ->where('entity_id = ?', $newcustomerId)
					  ->where('attribute_id = ?', '175');
					  
				$resultt = $connection->fetchAll($select);
				
				
				$count=count($resultt);
				if($count!=0)
				{
					$pan_number=preg_replace("/[^a-zA-Z 0-9]+/", "",$resultt[0]['value']);
				} 
				 $accountconfirm=''; 
				 $buyer_status='';
				$accountManagement = $objectManager->get('\Magento\Customer\Api\AccountManagementInterface');
				$accountconfirm = $accountManagement->getConfirmationStatus($newcustomerId);
				if($accountconfirm =='account_confirmed' && $resultcomp=='1')
				{
					$buyer_status= 1;
					
				}
				else
				{
					$buyer_status=0;
				}
				

				$buyer_status=$buyer_helpers->get_customer_status($newcustomerId);			
				$buyer_locked=$buyer_helpers->get_customer_status($newcustomerId);	
				 
				$buyer_lockexpires="Un Locked";
				
				if($buyer_locked=="Locked")
				{
					$buyer_lockexpires="Locked";
				}
				
				$company_nam='';
				$company_add='';
				if($group_id==4)
				{	
					$company_nam=@$newData['company_nam'];
					$company_add=@$newData['company_add'];
				}

				
				$mianarray=array(
				'buyer_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_id),	
				'group_id'=>$group_id, 	
				'buyer_firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_firstname),	
				'buyer_lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_lastname),	
				'default_billing'=>preg_replace('/[^A-Za-z0-9. -]/', '', $default_billing),	
				'default_shipping'=>preg_replace('/[^A-Za-z0-9. -]/', '', $default_shipping),	
				'gender'=>preg_replace('/[^A-Za-z0-9. -]/', '', $gender),	
				'billing_firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $billing_firstname),	
				'billing_lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $billing_lastname),	
				'buyer_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_address),	
				'billing_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_city),	
				'billing_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_state),	
				'billing_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_pincode),		
				'billing_country_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_country_id),	
				'shipping_firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_firstname),	 
				'shipping_lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_lastname),	  
				'shipping_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_address),	 
				'shipping_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_city),	
				'shipping_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_state),	
				'shipping_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_pincode),		
				'buyer_email'=>$email,   
				'buyer_mobile'=>preg_replace('/[^A-Za-z0-9. -]/', '', $mobile),
				'buyer_group'=>preg_replace('/[^A-Za-z0-9. -]/', '', $groupName),
				'created_at'=>$created_at,	 					
				'buyer_status'=>$buyer_status,  
				'buyer_panno'=>preg_replace('/[^A-Za-z0-9. -]/', '', $pan_number),
				'buyer_gstnumber'=>preg_replace('/[^A-Za-z0-9. -]/', '', $gst_number),
				'buyer_typeofbusiness'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_typeofbusiness),
				'buyer_lockexpires'=>$buyer_lockexpires,  
				'buyer_rewardtier'=>preg_replace('/[^A-Za-z0-9. -]/', '', $rewards_subscription),
				'company_name'=>preg_replace('/[^A-Za-z0-9. -]/', '', $company_nam),
				'company_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $company_add),
				'buyer_ip'=>$buyer_ip,   
				 	
			);
			
			/* echo "<pre>";
				print_r($mianarray);
			echo "</pre>";  */
			
			
			$mygroup=array(
				'group_id'=>$group_id, 	
				'buyer_group'=>preg_replace('/[^A-Za-z0-9. -]/', '', $groupName),
			);
			
			
			if($group_id==4)
			{
				$myprofile=array(
					'firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_firstname),		
					'lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_lastname),		
					'email'=>$email,		
					'buyer_mobile'=>$mobile,
					'buyer_panno'=>preg_replace('/[^A-Za-z0-9. -]/', '', $pan_number),
					'buyer_gstnumber'=>preg_replace('/[^A-Za-z0-9. -]/', '', $gst_number),
					'company_name'=>preg_replace('/[^A-Za-z0-9. -]/', '', $company_nam),
					'company_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $company_add),
					'gender'=>'',
					'buyer_lockexpires'=>$buyer_lockexpires,  
					'buyer_rewardtier'=>preg_replace('/[^A-Za-z0-9. -]/', '', $rewards_subscription),
					'buyer_ip'=>$buyer_ip,   
					'buyer_status'=>$buyer_status,  
				);
			}
			else
			{
				$myprofile=array(
					'firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_firstname),		
					'lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_lastname),		
					'email'=>$email,		
					'buyer_mobile'=>$mobile,
					'gender'=>'',
					'buyer_lockexpires'=>$buyer_lockexpires,  
					'buyer_rewardtier'=>preg_replace('/[^A-Za-z0-9. -]/', '', $rewards_subscription),
					'buyer_ip'=>$buyer_ip,   
					'buyer_status'=>$buyer_status,  
				);
			}		
			
			
			$mybillingArray=array(
				'default_billing'=>preg_replace('/[^A-Za-z0-9. -]/', '', $default_billing),	
				'billing_firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $billing_firstname),	
				'billing_lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $billing_lastname),	
				'billing_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_address),	
				'billing_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_city),	
				'billing_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_state),	
				'billing_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_pincode),		
				'billing_country_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_country_id),			
			);
			
			
			$myshippingArray=array(
				'default_shipping'=>preg_replace('/[^A-Za-z0-9. -]/', '', $default_shipping),	
				'shipping_firstname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_firstname),	 
				'shipping_lastname'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_lastname),	  
				'shipping_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_address),	 
				'shipping_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_city),	
				'shipping_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_state),	
				'shipping_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $shipping_pincode),	  
				'shipping_country_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_country_id),		
			); 
			 
			
			/* echo "<pre>";
				print_r($mygroup);
			echo "</pre>"; 

			echo "<pre>";
				print_r($myprofile);
			echo "</pre>"; 

			echo "<pre>";
				print_r($mybillingArray);
			echo "</pre>"; 	
			
			echo "<pre>";
				print_r($myshippingArray);
			echo "</pre>"; 	
			  */
			
			$Array[]=$mianarray;
			$Array[]=$mygroup;
			$Array[]=$myprofile;
			$Array[]=$mybillingArray;
			$Array[]=$myshippingArray;
			
			

		return $Array; 
		//return $mianarray;

		
		}		
	}


	public function check_data_change($json_1,$json_2)
	{
		$items=$json_1; 


		$items2=$json_2;

		$it_1 = json_decode($items, TRUE);
		$it_2 = json_decode($items2, TRUE);
		
		if(is_array($it_1) && is_array($it_2))
		{	
			$result_array = array_diff($it_1,$it_2);
			if(!empty($result_array)){     
			return 1;
			}
			else
			{
				return 0;
			}	
			
			
		}
		return 0;
	}


	public function create_buyer_report_updated($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
		
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		
		$buyer_helpers->store_buyer_temp_data($user_id); 
		$buyer_helpers->create_buyer_report($user_id);      
		
		
	}	
			

	function check_customer_deleted() 
	{ 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
		
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		
					
		$sql="select * from customer_temp_report where deleted is null";
		$result = $connection->fetchAll($sql);
		foreach($result as $row)
		{
			$user_id=$row['user_id'];
			
			$check_this_seller=$api_helpers->check_user_seller($user_id);   
			$json=$row['user_details']; 
			
			if($check_this_seller==0)     
			{	
				$sqls="select * from customer_entity where entity_id='".$user_id."'";	   
				$results = $connection->fetchAll($sqls);
				if(empty($results))
				{
					$user_id; 
					
					$sql="update customer_temp_report set deleted='1' where user_id='".$user_id."'";
					$connection->query($sql);   
					
					
					$buyer_helpers->create_new_row($user_id,$json,'Deleted');      
					
					
				}		
			}  
			
			

			
		}	
		
		
	}
	
	
	public function check_customer()    
	{ 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
		
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		
					
		$sql="select a.entity_id,a.default_billing,a.default_shipping,b.* from customer_entity as a
		inner join customer_temp_report as b
		on a.entity_id=b.user_id
		where b.deleted is null and a.default_billing is not null";
		$result = $connection->fetchAll($sql);
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$user_details=$row['user_details'];	
				$user_details=$row['user_details'];	
				$user_default_billing=$row['default_billing'];	
				$user_default_shipping=$row['default_shipping'];	
				$user_details_array=json_decode($row['user_details']);	
				
				/* echo "<pre>";
					print_r($user_details_array);
				echo "</pre>";  */
				
				$user_id=$row['user_id']; 	
				$default_billing=$user_details_array[3]->default_billing;
				$default_shipping=$user_details_array[4]->default_shipping;  
				
				  
				
				
				
				if($default_billing=='' && $default_shipping=='' && $user_default_shipping!=0 && $user_default_billing!=0)   
				{
					
					$mianarray=$buyer_helpers->customer_report_array($user_id);
					
								
					
					
					$user_details_array[0]=$mianarray[0];
					$user_details_array[3]=$mianarray[3];  
					$user_details_array[4]=$mianarray[4];  	  
				
					/* echo "<pre>";  
						print_r($user_details_array);
					echo "</pre>";	 */  
					
					$json=json_encode($user_details_array);   
					
					
					 
					 
					
					$buyer_helpers->create_new_row($user_id,$json,'Billing & Shipping Address');  	
					$buyer_helpers->update_temp_user_table($user_id,$json); 	 
					
				}		
				
			} 
		}
		
		
	}
	
	
	
}