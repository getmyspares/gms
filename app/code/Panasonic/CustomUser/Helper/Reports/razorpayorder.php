<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Razorpayorder extends AbstractHelper
{
    public function update_quote_order()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$razorpay_order_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpayorder');

		$sql="select a.*,b.* from razorpay_orders as a inner join quote as b on a.quote_id=b.entity_id where a.order_id is null and a.status in ('captured','authorized')";
		//$sql="select a.*,b.* from razorpay_orders as a inner join quote as b on a.quote_id=b.entity_id";
		$select = $sql;
		$result = $connection->fetchAll($select); 
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$quote_id=$row['quote_id'];		
				$id=$row['id'];		
				$order=$razorpay_order_helpers->get_quote_order_by_id($quote_id);
				
				
				if($order==1)
				{
					$sql="update razorpay_orders set order_id='1' where id='".$id."'";
					$connection->query($sql);  		
				}    
				else
				{
					
					date_default_timezone_set('Asia/Kolkata'); 
					$timezone_object = date_default_timezone_get(); 
					$today=date('Y-m-d H:i:s');
					
					$select = "select * from mobile_sales_order where cart_id='".$quote_id."' and order_increment_id is not null";
					$resultss = $connection->fetchAll($select); 
					if(!empty($resultss))  
					{
						$sql="update razorpay_orders set order_id='1' where id='".$id."'";
						$connection->query($sql);  			
					}
					
					
					$sql="update razorpay_orders set cr_date='".$today."' where id='".$id."'";
					$connection->query($sql);  	  
					
					
				}		
					
			}
		}		
	}

	public function get_quote_order_by_id($quote_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
			  ->from('sales_order') 
			  ->where('quote_id= ?',$quote_id);
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return 1;	
		}		
		else
		{
			return 0; 
		}		
	}

	public function create_manual_order($quote_id) 
	{
		
		
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$quoteFactory = $objectManager->create('\Magento\Quote\Model\QuoteFactory');
		$quoteArray = $quoteFactory->create()->load($quote_id);	
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository'); 
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$razorpay_order_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpayorder');
		$Quote=$quoteArray->getData();
		
		/* echo "<pre>";
		print_r($Quote);
		echo "</pre>"; */
		
		
		 
		
		$customerArray=array(
			'id'=>$Quote['customer_id'],	
			'email'=>$Quote['customer_email'],	
			'customer_firstname'=>$Quote['customer_firstname'],	
			'customer_lastname'=>$Quote['customer_lastname'],	
		);
		
		
		$options = $objectManager->create('Magento\Quote\Model\Quote');
		$_quote = $options->load($quote_id);
		
		$items = $_quote->getAllItems();
		
		foreach($items as $item)
		{
			$itemarray[]=array(
				'product_id'=>$item->getProductId(),
				'qty'=>$item->getQty(),
			);	 	
		}	
		
		
		foreach($itemarray as $k=>$v) {
			$mainArray[$v['product_id']] = $v['qty'];
		}
		
		
		
		$productIds = $mainArray;
		
		
		$ship_id=$_quote->getShippingAddress()->getCustomerAddressId(); 
		$ship_firstname=$_quote->getShippingAddress()->getFirstName();
		$ship_lastname=$_quote->getShippingAddress()->getLastName();
		$ship_street=$_quote->getShippingAddress()->getStreet();
		$ship_city=$_quote->getShippingAddress()->getCity();
		$ship_region=$_quote->getShippingAddress()->getRegion();
		$ship_region_id=$_quote->getShippingAddress()->getRegionId();
		$ship_postcode=$_quote->getShippingAddress()->getPostCode();
		$ship_country_id=$_quote->getShippingAddress()->getCountryId();
		$ship_phone=$_quote->getShippingAddress()->getTelephone();
		
		
		$billing_id=$_quote->getBillingAddress()->getCustomerAddressId(); 
		$billing_firstname=$_quote->getBillingAddress()->getFirstName();
		$billing_lastname=$_quote->getBillingAddress()->getLastName();
		$billing_street=$_quote->getBillingAddress()->getStreet();
		$billing_city=$_quote->getBillingAddress()->getCity();
		$billing_region=$_quote->getBillingAddress()->getRegion();
		$billing_region_id=$_quote->getBillingAddress()->getRegionId();
		$billing_postcode=$_quote->getBillingAddress()->getPostCode();
		$billing_country_id=$_quote->getBillingAddress()->getCountryId();
		$billing_phone=$_quote->getBillingAddress()->getTelephone();
		
		
		
		
		  
		$shippingArray=array(
			
			'id'=>$ship_id,	
			'firstname'=>$ship_firstname,	
			'lastname'=>$ship_lastname,	
			'street'=>$ship_street,	
			'city'=>$ship_city,	
			'region'=>$ship_region,	
			'region_id'=>$ship_region_id,	
			'postcode'=>$ship_postcode,	
			'country'=>$ship_country_id,	
			'telephone'=>$ship_phone,	
		);   


		$billingArray=array(
			
			'id'=>$billing_id,	
			'firstname'=>$billing_firstname,	
			'lastname'=>$billing_lastname,	
			'street'=>$billing_street,	
			'city'=>$billing_city,	
			'region'=>$billing_region,	
			'region_id'=>$billing_region_id,	
			'postcode'=>$billing_postcode,	
			'country'=>$billing_country_id,	
			'telephone'=>$billing_phone,	
		);    
		
		
		$select = $connection->select()
			  ->from('razorpay_orders') 
			  ->where('quote_id = ?', $quote_id);
			 
		$results = $connection->fetchAll($select);
		if(!empty($results))
		{
			$payment_id=$results[0]['payment_id'];	
			$payment_details=$payment_id;
		}	
		else
		{
			echo "2";
			die;
		}		
		
		
		
		/* echo "<pre>";
			print_r($shippingArray);
		echo "</pre>"; 

		echo "<pre>";
			print_r($billingArray);
		echo "</pre>";  */ 
		
		
		$mainarray=array( 
			'quote_id'=>$quote_id,
			'customer'=>$customerArray,
			'shipping'=>$shippingArray,
			'billing'=>$billingArray,
			'product'=>$productIds,
		); 
		
		
		
		
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$store = $storeManager->getStore();
		$websiteId = $storeManager->getStore()->getWebsiteId();
		
		$customer_email=$mainarray['customer']['email'];
		
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
		$customer = $customerFactory->setWebsiteId($websiteId)->loadByEmail($customer_email);
		$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customer->getId());
		
		$user_id=$customer->getId();
		
		$region_id=$mainarray['shipping']['region_id'];
		if($region_id=='')
		{
			echo '0';
			die;
		}		
		foreach($productIds as $key=>$value)
		{
			$product_id=$key;
			$seller_id=$api_helpers->get_product_seller_id_mobile($product_id);
			$sellerArray[]=$seller_id;
		
		}	
		
		$array = array_unique($sellerArray);
		
		$check_split=count($array);
		
		if($check_split > 1)
		{
			echo "This is multi seller order"; 
			die;
		}
		else
		{
			foreach($array as $row)
			{
				$seller_id=$row;	
			}
			
			
			
			
			
			
			foreach($productIds as $key=>$value)
			{
				$product_id=$key;
				$qtyy=$value;	
				$qty=$value;	
				$product_idd=$product_id;
				
				
				$payment_method='online';
				$payment_detail=$payment_details;
				
				$cr_date=date('Y-m-d H:i:s');
				
			$insert="insert into mobile_sales_order (user_id,cart_id,address_id,product_id,qty,payment_method,payment_details,cr_date,manual_order) values ('".$user_id."','".$quote_id."','".$ship_id."','".$product_id."','".$qty."','".$payment_method."','".$payment_detail."','".$cr_date."','1')"; 	
			$connection->query($insert);		 
			  	     
				
			}
			$api_helpers->get_mobile_order_manual(); 	  
			$api_helpers->clear_cart($user_id); 	
			
			
			$update="update quote set is_active='0' and reserved_order_id='1' where entity_id='".$quote_id."'"; 	
			$connection->query($update);	     
			
			echo "1";   
			
				 		
		}	
		
		  
		
		
	}
	
	
	public function is_order_online($order_id,$shippingprice,$payment_method,$payment_details) 
	{    
		    
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$store = $storeManager->getStore();
		$websiteId = $storeManager->getStore()->getWebsiteId();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
		
		//$shippingprice='100';  
	
		$order->setShippingMethod('ecomexpress_Shipping & Handling Fee');  
		$order->setShippingDescription('E-Com Express - Shipping & Handling Fee'); 
		

		
		
		$payment_array=array('method_title'=>'Pay Online');
		$payment_info=json_encode($payment_array);
		 
		$order_id=$order->getId();
		
		
		
			 	
		$gst_array=json_decode($api_helpers->get_order_gst_total($order->getId()));

		$total_gst=$gst_array->total_gst;
		$total_cgst=$gst_array->total_cgst;
		$total_sgst=$gst_array->total_sgst;
		$total_igst=$gst_array->total_igst;
		$total_utgst=$gst_array->total_utgst;
		
		  
		$order->setCgstAmount($total_cgst);
		$order->setBaseCgstAmount($total_cgst);
		
		$order->setSgstAmount($total_sgst);
		$order->setBaseSgstAmount($total_sgst);
		
		$order->setIgstAmount($total_igst);
		$order->setBaseIgstAmount($total_igst);  
		
		$order->setUtgstAmount($total_utgst); 
		$order->setBaseUtgstAmount($total_utgst);     
		
		/* if($payment_method=='online')
		{	
			$sql="update sales_order_payment set method='razorpay',last_trans_id='".$payment_details."',additional_information='".$payment_info."' where parent_id='".$order_id."'";  
			$connection->query($sql);       	         
		}   */ 
		     
		$order->setShippingAmount($shippingprice);
		$order->setBaseShippingAmount($shippingprice);
		
		$subtotal=$order->getSubtotal();
		 
		
		$order->setGrandTotal($subtotal + $shippingprice); 
		$order->save(); 	  
		
		
		$api_helpers->create_mobile_order_invoice($order_id);     
		     																			
		   
	}
	
	


	
}