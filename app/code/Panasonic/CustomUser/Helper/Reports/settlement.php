<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Settlement extends AbstractHelper
{
	/* constructor added  as per  magento  standard @ritesh */
	/* Remark : wrong practice  followed , refactoring needed  */
	/* TODO  : Inject Dependencies  through di  instead of  object manger */
	public function __construct(
		\Magento\Framework\App\ResourceConnection $resource
    ) {
		$this->_resource = $resource;
	}

	public function create_settlement_report($order_inc_id,$type='normal')
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$buyer_id=$order->getCustomerId();  
 		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$cgst_amount_n=round($cgst_amount); 
		$sgst_amount_n=round($sgst_amount); 
		$igst_amount_n=round($igst_amount); 
		$ugst_amount_n=round($ugst_amount); 
		$gst_nature='CGST/SGST/UTGST';	
		
		if($igst_amount_n!=0)	
		{
			$gst_nature='IGST';	
		}	
		
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		/* echo "<pre>"; 
			print_r($orderDetailArray);
		echo "</pre>";   */
		
		$nodal_charges=0;
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$baseprice=$orderDetailArray->baseprice;
		$grand_total=$orderDetailArray->grand_total;
		$total_discount=$orderDetailArray->total_discount;
		$total_gst=$orderDetailArray->total_gst;
		$subtotal=$orderDetailArray->subtotal;
		$shipping_expense=$orderDetailArray->shipping_expense;
		$shipping_base=$orderDetailArray->shipping_base;
		$shipping_gst=$orderDetailArray->shipping_gst;
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		$logistic_tds=$orderDetailArray->logistic_tds; 
		$gst_tcs=$orderDetailArray->gst_tcs; 
		$nodal_charges=$orderDetailArray->nodle_charges; 
		$gross_comm_due=$orderDetailArray->gross_comm_due; 
		$gst_gross_comm_due=$orderDetailArray->gst_gross_comm_due; 
		$razorpay_tds=$orderDetailArray->razorpay_tds; 
		$razor_method=$orderDetailArray->razor_payment; 
		$razor_card_type=$orderDetailArray->razor_card; 
		$razor_network=$orderDetailArray->razor_network;   
		$razor_pay_fee_amount=$orderDetailArray->razor_pay_fee_amount; 
		$razor_pay_gst_amount=$orderDetailArray->razor_pay_gst_amount; 
		$admin_comm_rate=$orderDetailArray->admin_comm_rate; 
		$admin_comm_tds_rate=$orderDetailArray->admin_comm_tds_rate; 
		$admin_comm_tds=$orderDetailArray->admin_comm_tds; 
		$gst_tcs_rate=$orderDetailArray->gst_tcs_rate; 
		$net_comm=$orderDetailArray->net_comm; 
		$net_admin_comm=$orderDetailArray->net_admin_comm; 
		$razor_pay_total_charge_amount=$razor_pay_fee_amount+$razor_pay_gst_amount;
		$net_amount_rec_nodal=$grand_total-$razor_pay_total_charge_amount;
		$net_remittance_logistic=$shipping_expense-$logistic_tds;
		//$balance_in_nodal=$net_amount_rec_nodal-$net_remittance_logistic-$logistic_tds;
		$shipping_gst_tcs=($shipping_base*1)/100; 
		$amount_due_to_logistic=$shipping_expense-$logistic_tds-$shipping_gst_tcs;
		//$net_comm=$gross_comm_due+$gst_gross_comm_due; 
		//$gst_tcs=($baseprice*$gst_tcs_rate)/100;	    
		$tds_pi_comm=$admin_comm_tds;	  
		$gross_remittance_pi=$net_admin_comm; 
		//$net_remittance_pi=$gross_remittance_pi-$razor_pay_total_charge_amount-$nodal_charges;
		$payment_gateway_charges_net_tds=$razor_pay_fee_amount-$razorpay_tds; 
		//$net_remittance_pi=$gross_remittance_pi-$payment_gateway_charges_net_tds-$nodal_charges+$logistic_tds+$shipping_gst_tcs;
		$net_remittance_pi=$gross_remittance_pi-$payment_gateway_charges_net_tds-$nodal_charges+$logistic_tds+$shipping_gst_tcs-$razor_pay_gst_amount; 
		$nodal_balance=$net_amount_rec_nodal-$nodal_charges;
		$balance_in_nodal=$nodal_balance-$amount_due_to_logistic;
		//$balance_in_nodal_pi=$balance_in_nodal-$net_remittance_pi;
		$balance_in_nodal_pi=$balance_in_nodal-$net_remittance_pi;  
		$remittance_to_seller=$grand_total-$shipping_expense-$gross_remittance_pi;
		$payment_type='COD';	 
		if($payment_id!='COD') 
		{
			$payment_type='Online';	
		}	
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		$seller_address=$seller_details->seller_comp_address;
		$seller_zipcode=$seller_details->seller_zipcode;
		$seller_state=$seller_details->seller_state;
		$seller_city=$seller_details->seller_city;
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($order_cr_date)); 
		$settlement_due_date = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
		$cgst_amount_n=round($cgst_amount); 
		$sgst_amount_n=round($sgst_amount); 
		$igst_amount_n=round($igst_amount); 
		$ugst_amount_n=round($ugst_amount); 
		$shipping_igst=0;
		$shipping_cgst=0;
		$shipping_sgst=0;
		$shipping_ugst=0;
		$shipping_gst_t=0;
		if($igst_amount_n!=0)
		{
			$shipping_igst=$shipping_gst;
		}	
		else if($cgst_amount_n!=0 && $sgst_amount_n!=0)
		{
			
			$shipping_gst_t=$shipping_gst/2;
			$shipping_cgst=$shipping_gst_t;
			$shipping_sgst=$shipping_gst_t;
		}	
		else if($cgst_amount_n!=0 && $ugst_amount!=0)
		{
			
			$shipping_gst_t=$shipping_gst/2;
			$shipping_cgst=$shipping_gst_t;
			$shipping_ugst=$shipping_gst_t; 
		}	
		$razor_pay_gst_igst=0;
		$razor_pay_gst_cgst=0;
		$razor_pay_gst_sgst=0;
		$razor_pay_gst_ugst=0;
		$razor_pay_gst_gst_t=0; 
		$razor_pay_gst_gst_rate=0;  
		if($igst_amount_n!=0)
		{
			$razor_pay_gst_igst=$razor_pay_gst_amount;
		}	
		else if($cgst_amount_n!=0 && $sgst_amount_n!=0)
		{
			
			$razor_pay_gst_gst_t=$razor_pay_gst_amount/2;
			$razor_pay_gst_cgst=$razor_pay_gst_gst_t;
			$razor_pay_gst_sgst=$razor_pay_gst_gst_t; 
		}	
		else if($cgst_amount_n!=0 && $ugst_amount!=0)
		{
			
			$razor_pay_gst_gst_t=$razor_pay_gst_amount/2;
			$razor_pay_gst_cgst=$razor_pay_gst_gst_t;
			$razor_pay_gst_ugst=$razor_pay_gst_gst_t; 
		}	
		
		if($razor_pay_fee_amount=='0')
		{
			$razor_pay_gst_gst_rate=0;
		}
		else	
		{	
			$razor_pay_gst_gst_rate=($razor_pay_gst_amount/$razor_pay_fee_amount)*100; 
		}
		$razor_pay_gst_amount.' '.$razor_pay_fee_amount.' '.$razor_pay_gst_gst_rate;
			
		if($razor_pay_gst_gst_rate >=5 && $razor_pay_gst_gst_rate< 12)		
		{
			$razor_pay_gst_gst_rate=5;	
		}	
		else if($razor_pay_gst_gst_rate >=12 && $razor_pay_gst_gst_rate< 18)		
		{
			$razor_pay_gst_gst_rate=12;	
		}
		else if($razor_pay_gst_gst_rate >=18 && $razor_pay_gst_gst_rate< 28)		
		{
			$razor_pay_gst_gst_rate=18;	
		}
		else if($razor_pay_gst_gst_rate >=28 && $razor_pay_gst_gst_rate< 30)		
		{
			$razor_pay_gst_gst_rate=28;	
		}	
		$gst_gross_comm_due_igst=0;
		$gst_gross_comm_due_cgst=0;
		$gst_gross_comm_due_sgst=0;
		$gst_gross_comm_due_ugst=0;
		$gst_gross_comm_due_t=0; 
		if($igst_amount_n!=0) 
		{
			$gst_gross_comm_due_igst=$gst_gross_comm_due;
		}	
		else if($cgst_amount_n!=0 && $sgst_amount_n!=0)
		{
			
			$gst_gross_comm_due_t=$gst_gross_comm_due/2;
			$gst_gross_comm_due_cgst=$gst_gross_comm_due_t;
			$gst_gross_comm_due_sgst=$gst_gross_comm_due_t; 
		}	
		else if($cgst_amount_n!=0 && $ugst_amount!=0)
		{
			
			$gst_gross_comm_due_t=$gst_gross_comm_due/2;
			$gst_gross_comm_due_sgst=$gst_gross_comm_due_t;
			$gst_gross_comm_due_ugst=$gst_gross_comm_due_t; 
		}	
		$buyer_id=$orderDetailArray->customer_id;
		$payment_id=$orderDetailArray->payment_id;

		$delivery_date='';
		$creditmemo_grand_amount='';
		$refundsettlementbalance='';
		if($type=='refund')
		{
			$payment_type='refund';
			$payment_id='Offline Refund';
			$creditmemo_details = $this->getCreditMemo($order_id);
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			$creditmemo_grand_amount = $creditmemo_details['grand_total'];
			$invoice_id = $credit_memo_increment_id;
			$order_cr_date = $credit_memo_created_date;
			$today = date("Y-m-d",strtotime($credit_memo_created_date));
			$net_amount_rec_nodal='0';
			$nodal_balance='0';
			$logistic_tds='0';
			$shipping_gst_tcs='0';
			$amount_due_to_logistic='0';
			$balance_in_nodal='0';
			$gross_comm_due='0';
			$gst_gross_comm_due='0';
			$net_comm='0';
			$gst_tcs='0';
			$tds_pi_comm='0';
			$net_remittance_pi='0';
			$balance_in_nodal_pi='0';
			$remittance_to_seller='0';
			$refundsettlementbalance = ($grand_total-$creditmemo_grand_amount-$razor_pay_total_charge_amount) ;
		}
		$mainarray=array(  
			'order_inc_id'=>$order_inc_id,		
			'order_date'=>$order_cr_date,	
			'buyer_id'=>$buyer_id,	
			'order_type'=>$payment_type,		
			'payment_id'=>$payment_id,		
			'invoice_id'=>$invoice_id,	 	
			'invoice_date'=>$order_cr_date,	  
			'seller_id'=>$seller_id,	  	 
			'seller_name'=>$seller_name,	   	 	
			'delivery_date'=>$delivery_date,	   	 	
			'settlement_due_date'=>$settlement_due_date,	   	 	
			'basic_sale_amount'=>$baseprice,	   	 	
			'gst_nature'=>$gst_nature,	    	 	
			'total_gst'=>$total_gst,	    	 	
			'listed_price'=>$subtotal,	    	 	
			'shipping_basic'=>$shipping_base,	    	 	
			'shipping_gst'=>$shipping_gst,	     	 	
			'shipping_igst'=>$shipping_igst,	     
			'shipping_cgst'=>$shipping_cgst,	     
			'shipping_sgst'=>$shipping_sgst,	     
			'shipping_ugst'=>$shipping_ugst,	   
			'shipping_gst_rate'=>$shipping_gst_rate,	       
			'razor_method'=>$razor_method,	       
			'razor_card_type'=>$razor_card_type,	       
			'razor_network'=>$razor_network,	         
			'total_shipping'=>$shipping_expense,	     	 	
			'total_sales'=>$grand_total,	      	 	
			'payment_gateway'=>$grand_total,	      	 	
			'razor_pay_fee_amount'=>$razor_pay_fee_amount,	      	 	
			'razor_pay_gst_amount'=>$razor_pay_gst_amount,	       	 
			'razor_pay_gst_igst'=>$razor_pay_gst_igst,	       	 
			'razor_pay_gst_cgst'=>$razor_pay_gst_cgst,	       	 
			'razor_pay_gst_sgst'=>$razor_pay_gst_sgst,	       	 
			'razor_pay_gst_ugst'=>$razor_pay_gst_ugst,	         	 
			'razor_pay_gst_gst_rate'=>$razor_pay_gst_gst_rate,	        	 
			'razor_pay_total_charge_amount'=>$razor_pay_total_charge_amount,	       	 	
			'razorpay_tds'=>$razorpay_tds,	       	 	
			'razorpay_charges_net_tds'=>$payment_gateway_charges_net_tds,	       	 	
			'net_amount_rec_nodal'=>$net_amount_rec_nodal,	       	 	
			'nodal_charges'=>$nodal_charges,	       	 	
			'nodal_balance'=>$nodal_balance,	       	 	
			'logistic_tds'=>$logistic_tds,	       	 	
			'shipping_gst_tcs'=>$shipping_gst_tcs,	        	 	
			'amount_due_to_logistic'=>$amount_due_to_logistic,	          	 	
			'net_remittance_logistic'=>$net_remittance_logistic,	       	 	
			'balance_in_nodal'=>$balance_in_nodal,	        	 	
			'gross_comm_due'=>$gross_comm_due,	        	 	
			'gst_gross_comm_due'=>$gst_gross_comm_due,	   
			'gst_gross_comm_due_igst'=>$gst_gross_comm_due_igst,	   
			'gst_gross_comm_due_cgst'=>$gst_gross_comm_due_cgst,	   
			'gst_gross_comm_due_sgst'=>$gst_gross_comm_due_sgst,	   
			'gst_gross_comm_due_ugst'=>$gst_gross_comm_due_ugst,	   
			'net_comm'=>$net_comm,	         	 	
			'gst_tcs'=>$gst_tcs,	        	 	
			'tds_pi_comm'=>$tds_pi_comm,	        	 	
			'gross_remittance_pi'=>$gross_remittance_pi,	        	 	
			'net_remittance_pi'=>$net_remittance_pi,	        	 	
			'balance_in_nodal_pi'=>$balance_in_nodal_pi,	        	 	
			'remittance_to_seller'=>$remittance_to_seller,	        	 	
			'admin_comm_per'=>$admin_comm_rate,
			'refunded_amount'=>$creditmemo_grand_amount,
			'refund_settlement_balance'=>$refundsettlementbalance

		);
		$json=json_encode($mainarray); 
		$today=date('Y-m-d H:i:s');
		$sql="insert into settlement_report (order_id,details,cr_date,type) values ('".$order_inc_id."','".$json."','".$today."','$type')";
		$connection->query($sql); 	       
	}	
	
	public function check_settlement_report($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$select = $connection->select()
                  ->from('settlement_report') 
                  ->where('order_id = ?', $order_inc_id);
		$result = $connection->fetchAll($select);	
		if(!empty($result))
		{
			return 1;
		}
		else 
		{
			return 0;
		}		
	}
	
	public function checkiforderrefunded($increment_id)
	{   /* code  cleaned @ritesh */
		$connection = $this->_resource->getConnection();
		$refunded_payment_query = "SELECT `entity_id` FROM `sales_order` where `state`='closed' and increment_id='$increment_id'";
		$results = $connection->fetchAll($refunded_payment_query);
		$status =  !empty($results) ? $results[0]['entity_id']:0;
		return $status;
	}

	public function getCreditMemo($order_id)
	{	/* code  cleaned @ritesh */
		$connection = $this->_resource->getConnection();
		$select = "select * from sales_creditmemo where order_id = $order_id";
		$result = $connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
	}

	public function check_refund_settlement_report($order_inc_id)
	{ /* refactoring done  objectmanger removed */
		$isorderrefunded =  $this->checkiforderrefunded($order_inc_id);
		if(empty($isorderrefunded)){ /* here it means opposite as  0  will generate report */ return 1;  }
		$connection = $this->_resource->getConnection();
		$select = "Select  * from settlement_report where order_id='$order_inc_id' and type='refund'";
		$result = $connection->fetchAll($select);	
		$status =!empty($result) ? 1:0;   
		return $status;
	}
}