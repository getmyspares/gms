<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Globalcomm extends AbstractHelper
{
    
	public function store_global_comm_temp_report() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$global_comm_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\globalcomm'); 

		$sql="select * from global_comm_temp_report";
		$result = $connection->fetchAll($sql);
		$global_json=$global_comm_helpers->global_comm_report_array();
		
		 
		
		
		$today=date('Y-m-d H:i:s'); 
		if(empty($result))
		{
			$insert="insert into global_comm_temp_report (details,cr_date) values ('".$global_json."','".$today."')";
			$connection->query($insert);  	
		}  	
			
	}
	
	public function create_global_comm_report() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$global_comm_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\globalcomm'); 
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
		
		$json=$global_comm_helpers->global_comm_report_array();
		
		$sql = "Select * FROM global_comm_report";
		$result = $connection->fetchAll($sql); 
		
		if(empty($result))
		{	  
			$global_comm_helpers->create_new_row($json,'new'); 
		}    
		else  
		{ 
			$sql = "Select * FROM global_comm_temp_report";
			$results = $connection->fetchAll($sql); 
			$stored_json=$results[0]['details']; 
			
			
			$it_1 = json_decode($stored_json);
			$it_2 = json_decode($json);
			
			$it_1=(array)$it_1;
			$it_2=(array)$it_2; 
			
			$difference = array_diff_assoc($it_1, $it_2);
			//print_r($difference);
			if(!empty($difference))
			{
				$global_comm_helpers->create_new_row($json,'update');  	
				$global_comm_helpers->update_temp_table($json); 	
			}	 
			
			
		}  	 
	}
	
	public function update_temp_table($json) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$created_at=date('Y-m-d H:i:s');  
		 
		$sql="Update global_comm_temp_report set details='".$json."'";
		$connection->query($sql);    
		
	}	


	
	
	
	
	public function create_new_row($json,$reason) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$global_comm_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\globalcomm'); 
		$global_json=$global_comm_helpers->global_comm_report_array();
		$today=date('Y-m-d H:i:s');
		
		if(empty($result))
		{
			$insert="insert into global_comm_report (details,cr_date,reason) values ('".$global_json."','".$today."','".$reason."')";
			$connection->query($insert);  	
		}			
	}	
	
	
	public function global_comm_report_array()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		return $tax_helpers->tax_parameter();  	
	}	
	
	
}