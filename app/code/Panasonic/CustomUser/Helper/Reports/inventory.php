<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Inventory extends AbstractHelper
{
    
	public function insert_inventory_master_report_1($product_id,$delete=null)  	       	 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		
		  	 
				

		//foreach ($collection as $product){
			
			/* echo "<pre>";
				print_r($product->getData());
			echo "</pre>";  
			die; */
			$product_category='';  
			$bp_category='';
			$sub_category=''; 
			
			$description=$product->getDescription();
			$description=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $description);
			
			$short_description=$product->getShortDescription();
			$short_description=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $short_description);
			 
			 
			$product_sku=$product->getSku();
			
			
			$product_id=$product->getId(); 
			
			//if($product_id=='16393')
			//{
			
			$product_name=$product->getName();
			$product_name=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $product_name);
			
			$created_at=$product->getCreatedAt();
			
			$seller_id=$api_helpers->get_product_seller_id_mobile($product_id);
			$sellerarray=$custom_helpers->get_seller_details($seller_id);
			
			//print_r($sellerarray);
			
			
			$seller_name=$sellerarray['seller_name'];
			 
			$specialprice = '';
			$specialPriceFromDate = '';
			$specialPriceToDate = '';
			$costofitem = '';
			$discount_price = '';
			$discount_per = '';
			
			$product_price=$product->getPrice();
			$visibility=$product->getVisibility();
			$status=$product->getStatus();
			
			$length=$product->getProductLength();
			$breadth=$product->getProductBreadth();
			$height=$product->getProductHeight();
			$weight=$product->getProductWeight();
			$manufacturer=$product->getManufacturer(); 	
			
			$url_key=$product->getUrlKey();
			$hsn=$product->getHsn(); 
			$gst_rate=$product->getGstTax(); 
			$warranty_term=$product->getWarrantyDetails(); 
			$is_category_type_battery=$product->getIsCategoryTypeBattery(); 
			$is_category_type_dangerous=$product->getIsCategoryTypeDangerous(); 
			$part_type=$product->getPartType(); 
			
			
			$price=$api_helpers->get_product_prices($product_id);		
			$orgprice = $api_helpers->get_product_prices($product_id);
			$specialprice = $api_helpers->get_product_special_prices($product_id);
			$specialfromdate = $api_helpers->get_product_special_from_date($product_id);
			$specialtodate =  $api_helpers->get_product_special_to_date($product_id);
			
			
			$today = time(); 
		
			$price_return=0;
			if(!is_null($specialfromdate) && !is_null($specialtodate))
			{
				$specialfromdate=strtotime($specialfromdate);
				$specialtodate=strtotime($specialtodate);
				
				if($today >= $specialfromdate &&  $today <= $specialtodate)
				{
					$price_return=1;			
				}	
			}
			
			
			if($price_return==1)
			{	
				$discount_price = $product->getSpecialPrice();
				$specialPriceFromDate = $product->getSpecialFromDate();
				$specialPriceToDate = $product->getSpecialToDate(); 
				$costofitem = $product->getCost(); 
				
				$orignal_price=$product->getPrice();
				$selling_price=$product->getSpecialPrice();
				
				if($selling_price < $orignal_price)
				{
					$discount=$orignal_price-$selling_price;
					$discount_per=($discount/$orignal_price)*100;	
					$discount_per=round($discount_per);
				}	 
			}     
			
			if($delete==1) 
			{
				$status='Delete';		
			}
			else 
			{		
				if($status==2)
				{ 
					$status='Disabled';	
				}
				else
				{
					$status='Enabled';;
				}
			}  
			
			
			
			
			if(!empty($manufacturer))
			{	
				$manufacturer=$inventory_helpers->get_manufacture_name($manufacturer);
			}  
			
			$partType=0;
			if($part_type==235)
			{
				$partType=1;	
			}	 
			
			$categories = $product->getCategoryIds();
			
			$parent_cat_nam='';
			$parent_cat_name='';
			
			$child_cat_nam='';
			$child_cat_name='';
			
			$selected_cat_nam='';	
			$selected_cat_name='';	 
			$array=array();
			if(!empty($categories))
			{	
				foreach($categories as $category_id)
				{
					
					$cat_name=$inventory_helpers->get_category_name($category_id);
					$selected_cat_nam .=$cat_name.',';
					$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
					
					
					$parentCategories = $cat->getParentCategories();
					$childrenCategories = $cat->getChildrenCategories();
					$parent_cat_namArray=array();
					
					if(!empty($parentCategories))
					{
						foreach($parentCategories as $parent)
						{
							$parentArray=$parent->getData();
							
							//print_r($parentArray);
							
							if($parentArray['is_active']==1)
							{	
								if($parentArray['entity_id']!=$category_id)
								{
									//$parent_cat_namArray[]=$parentArray['name'];		
									$parent_cat_nam .=	$parentArray['name'].',';		
								} 
							}	
						}	 
					}	
					
					
					
					if(empty($parentCategories))
					{
						if(!empty($childrenCategories))
						{
							foreach($childrenCategories as $child)
							{
								
								$childArray=$child->getData();
								//print_r($childArray);
								
								if($childArray['is_active']==1)
								{	
									if($childArray['entity_id']!=$category_id)
									{
										$child_cat_nam .=	$childArray['name'].',';		
									} 
								}	
								
								
							}	 
						}
					}
					
					
					
					
					
					
					
					//
					//$child_cat_name=substr($child_cat_nam,0,-1);
					//$array=array('selected_cat_name'=>$selected_cat_name,'parent_cat_name'=>$parent_cat_name,'child_cat_name'=>$child_cat_name);
					
				}
				$selected_cat_name=substr($selected_cat_nam,0,-1);
				$parent_cat_name=substr($parent_cat_nam,0,-1);
				$parent_cat_name=str_replace("Default Category,","",$parent_cat_name);
				
				$child_cat_name=substr($child_cat_nam,0,-1);
				$child_cat_name=str_replace("Default Category,","",$child_cat_name); 
			} 
			
			if($visibility=="1")
			{
				$visibility='Not Visible Individually';		
			}	
			else if($visibility=="2")
			{
				$visibility='Catalog';		
			}	
			else if($visibility=="3")
			{
				$visibility='Search';		
			}	
			else if($visibility=="4") 
			{
				$visibility='Catalog,Search';	 	
			}	
			
			
			$mainArray=array(
				'product_id'=>$product_id,
				'product_name'=>$product_name,
				'created_at'=>$created_at,
				'description'=>$description, 
				'attribute'=>'Default', 
				'sku'=>$product_sku,   
				'seller_id'=>$seller_id,  
				'seller_name'=>$seller_name,   
				'product_price'=>$product_price,  
				'visibility'=>$visibility,  
				'status'=>$status,  
				'short_description'=>$short_description,  
				'length'=>$length,   
				'breadth'=>$breadth,   
				'height'=>$height,   
				'dimension'=>'',   
				'weight'=>$weight,   
				'manufacturer'=>$manufacturer,   
				'color'=>'black',   
				'manufacturer_country'=>'IN',    
				'url_key'=>$url_key,   
				'tax_class'=>'',   
				'hsn_code'=>$hsn,   
				'gst_rate'=>$gst_rate,    
				'warranty_term'=>$warranty_term,    
				'is_category_type_battery'=>$is_category_type_battery,    
				'is_category_type_dangerous'=>$is_category_type_dangerous,     
				'universal_parts'=>$partType,      
				'discount_per'=>$discount_per,  
			    'specialPriceFromDate'=>$specialPriceFromDate,  
				'specialPriceToDate'=>$specialPriceToDate,  
				'costofitem'=>$costofitem,   
				'product_category'=>$selected_cat_name,
				'bp_category'=>$parent_cat_name,
				'sub_category'=>$child_cat_name,
				'discount'=>0,
				'discount_price'=>$discount_price,
				
				  
				
			);	 
				
			/* echo "<pre>";
				print_r($mainArray);	
			echo "</pre>";    */
			 
			
			
			$json=json_encode($mainArray);
			
			$today_cr=date("Y-m-d H:i:s");
			
			$sql="INSERT INTO `inventory_master`(`product_id`, `details`, `product_cr_date`, `cr_date`) VALUES ('".$product_id."','".$json."','".$created_at."','".$today_cr."')";
			//echo "<br>";  
			  
			$connection->query($sql);      
			 
			
		//}
		/* echo "<pre>";
			print_r($mainArray);
		echo "</pre>"; */


		
	}


	public function get_product_parent_sub_category($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$product = $productRepository->getById($product_id);
		$categories = $product->getCategoryIds();
		$parent_cat_nam='';
		$parent_cat_name='';
		
		$child_cat_nam='';
		$child_cat_name='';
		
		$selected_cat_nam='';	
		$selected_cat_name='';	 
		$array=array();
		if(!empty($categories))
		{	
			foreach($categories as $category_id)
			{
				
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
				$selected_cat_id=$cat->getId();
				$selected_cat_nam .=$cat->getName().','; 
				 
				$parentCategories = $cat->getParentCategories();
				$childrenCategories = $cat->getChildrenCategories();
				
				if(!empty($parentCategories))
				{
					foreach($parentCategories as $parent)
					{
						$parentArray=$parent->getData();
						if($parentArray['is_active']==1)
						{	
							if($parentArray['entity_id']!=$selected_cat_id)
							{
								$parent_cat_nam .=	$parentArray['name'].',';		
							} 
						}	
					}	 
				}	
				
				
				
				if(!empty($childrenCategories))
				{
					foreach($childrenCategories as $child)
					{
						/* echo "<pre>";
							print_r($child->getData());
						echo "</pre>"; */
						
						
						$childArray=$child->getData();
						if($childArray['is_active']==1)
						{	
							if($childArray['entity_id']!=$selected_cat_id)
							{
								$child_cat_nam .=	$childArray['name'].',';		
							} 
						}	
						
						
					}	 
				}	
			
			}
			
			$selected_cat_name=substr($selected_cat_nam,0,-1);
			$parent_cat_name=substr($parent_cat_nam,0,-1);
			$child_cat_name=substr($child_cat_nam,0,-1);
			$array=array('selected_cat_name'=>$selected_cat_name,'parent_cat_name'=>$parent_cat_name,'child_cat_name'=>$child_cat_name);
		} 	 
		/* echo $selected_cat_name;
		echo "<br>";
		echo $parent_cat_name; 
		echo "<br>";
		echo $child_cat_name; */ 
		
		
		
		return $array;
		
		
	}		
	
	
	public function get_manufacture_name($manf_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				  ->from('eav_attribute_option_value') 
				  ->where('option_id = ?', $manf_id);
		 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['value'];	
		}
		
	}	
	
	
	public function get_category_name($cat_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		
		$select = $connection->select()
				  ->from('catalog_category_entity_varchar') 
				  ->where('entity_id = ?', $cat_id)
				  ->where('attribute_id = ?', '45'); 
		 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['value'];	
		}
	}	
	
	public function get_parent_category($cat_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
		$select = $connection->select()
				  ->from('catalog_category_entity') 
				  ->where('parent_id = ?', $cat_id); 
		 
		$result = $connection->fetchAll($select);
		$parentArray=array();
		if(!empty($result))
		{
			$parentArray[]=$result[0]['entity_id'];
		}

		return $parentArray; 
		
	}	
	
	public function get_product_list_by_date()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$fromDate = date('Y-m-d H:i:s', strtotime('-1 week'));
		
		$toDate = date('Y-m-d H:i:s');
		
		
		
		$collection = $productCollection->addAttributeToSelect('*')
			->addAttributeToFilter('status', ['in' =>'1,2'])
					->addFieldToFilter('updated_at', array( 
			'from' =>$fromDate,
			'to' => $toDate,  
			'date' => true,
			))
			->setOrder('sort_order', 'ASC')	
			->load();   
			 
		$count=count($collection);
		if($count > 0)
		{	
			foreach ($collection as $product){
				$product_id=$product->getId();
				$inventory_helpers->insert_inventory_master_report($product_id);	
			}	
		} 
		
		
	}	
	
	
	public function get_product_array($product_id,$delete=null) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');

		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();


		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		

		$product_category='';  
		$bp_category='';
		$sub_category=''; 

		$description=$product->getDescription();
		$description=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $description);

		$short_description=$product->getShortDescription();
		$short_description=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $short_description);
		 
		 
		$product_sku=$product->getSku();


		$product_id=$product->getId(); 

		//if($product_id=='16393')
		//{

		$product_name=$product->getName();
		$product_name=str_replace( array( "\'", "," , ";", "<", ">", ".", "'",":" ), '', $product_name);

		$created_at=$product->getCreatedAt();

		$seller_id=$api_helpers->get_product_seller_id_mobile($product_id);
		$sellerarray=$custom_helpers->get_seller_details($seller_id);

		//print_r($sellerarray);


		$seller_name=$sellerarray['seller_name'];
		 
		$specialprice = '';
		$specialPriceFromDate = '';
		$specialPriceToDate = '';
		$costofitem = '';
		$discount_price = '';
		$discount_per = '';

		$product_price=$product->getPrice();
		$visibility=$product->getVisibility();
		$status=$product->getStatus();

		$length=$product->getProductLength();
		$breadth=$product->getProductBreadth();
		$height=$product->getProductHeight();
		$weight=$product->getProductWeight();
		$manufacturer=$product->getManufacturer(); 	

		$url_key=$product->getUrlKey();
		$hsn=$product->getHsn(); 
		$gst_rate=$product->getGstTax(); 
		$warranty_term=$product->getWarrantyDetails(); 
		$is_category_type_battery=$product->getIsCategoryTypeBattery(); 
		$is_category_type_dangerous=$product->getIsCategoryTypeDangerous(); 
		$part_type=$product->getPartType(); 


		$price=$api_helpers->get_product_prices($product_id);		
		$orgprice = $api_helpers->get_product_prices($product_id);
		$specialprice = $api_helpers->get_product_special_prices($product_id);
		$specialfromdate = $api_helpers->get_product_special_from_date($product_id);
		$specialtodate =  $api_helpers->get_product_special_to_date($product_id);

		$today = time(); 
				
		$price_return=0;
		if(!is_null($specialfromdate) && !is_null($specialtodate))
		{
			$specialfromdate=strtotime($specialfromdate);
			$specialtodate=strtotime($specialtodate);
			
			if($today >= $specialfromdate &&  $today <= $specialtodate)
			{
				$price_return=1;			
			}	
		}


		if($price_return==1)
		{	
			$discount_price = $product->getSpecialPrice();
			$specialPriceFromDate = $product->getSpecialFromDate();
			$specialPriceToDate = $product->getSpecialToDate(); 
			$costofitem = $product->getCost(); 
			
			$orignal_price=$product->getPrice();
			$selling_price=$product->getSpecialPrice();
			
			if($selling_price < $orignal_price)
			{
				$discount=$orignal_price-$selling_price;
				$discount_per=($discount/$orignal_price)*100;	
				$discount_per=round($discount_per);
			}	 
		}     

		if($delete==1) 
		{
			$status='Delete';		
		}
		else 
		{		
			if($status==2)
			{ 
				$status='Disapproved';	
			} 
			else
			{
				$status='Approved';
			}
		}  




		if(!empty($manufacturer))
		{	
			$manufacturer=$inventory_helpers->get_manufacture_name($manufacturer);
		}  

		$partType=0;
		if($part_type==235)
		{
			$partType=1;	
		}	 

		$categories = $product->getCategoryIds();

		$parent_cat_nam='';
		$parent_cat_name='';

		$child_cat_nam='';
		$child_cat_name='';

		$selected_cat_nam='';	
		$selected_cat_name='';	 
		$array=array();
		if(!empty($categories))
		{	
			foreach($categories as $category_id)
			{
				
				$cat_name=$inventory_helpers->get_category_name($category_id);
				$selected_cat_nam .=$cat_name.',';
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
				
				
				$parentCategories = $cat->getParentCategories();
				$childrenCategories = $cat->getChildrenCategories();
				$parent_cat_namArray=array();
				
				if(!empty($parentCategories))
				{
					foreach($parentCategories as $parent)
					{
						$parentArray=$parent->getData();
						
						//print_r($parentArray);
						
						if($parentArray['is_active']==1)
						{	
							if($parentArray['entity_id']!=$category_id)
							{
								//$parent_cat_namArray[]=$parentArray['name'];		
								$parent_cat_nam .=	$parentArray['name'].',';		
							} 
						}	
					}	 
				}	
				
				
				
				if(empty($parentCategories))
				{
					if(!empty($childrenCategories))
					{
						foreach($childrenCategories as $child)
						{
							
							$childArray=$child->getData();
							//print_r($childArray);
							
							if($childArray['is_active']==1)
							{	
								if($childArray['entity_id']!=$category_id)
								{
									$child_cat_nam .=	$childArray['name'].',';		
								} 
							}	
							
							
						}	 
					}
				}
				
				
				
				
				
				
				
				//
				//$child_cat_name=substr($child_cat_nam,0,-1);
				//$array=array('selected_cat_name'=>$selected_cat_name,'parent_cat_name'=>$parent_cat_name,'child_cat_name'=>$child_cat_name);
				
			}
			$selected_cat_name=substr($selected_cat_nam,0,-1);
			$parent_cat_name=substr($parent_cat_nam,0,-1);
			$parent_cat_name=str_replace("Default Category,","",$parent_cat_name);
			
			$child_cat_name=substr($child_cat_nam,0,-1);
			$child_cat_name=str_replace("Default Category,","",$child_cat_name); 
		} 

		if($visibility=="1")
		{
			$visibility='Not Visible Individually';		
		}	
		else if($visibility=="2")
		{
			$visibility='Catalog';		
		}	
		else if($visibility=="3")
		{
			$visibility='Search';		
		}	
		else if($visibility=="4") 
		{
			$visibility='Catalog,Search';	 	
		}	


		$mainArray=array(
			'product_id'=>$product_id,
			'product_name'=>$product_name,
			'created_at'=>$created_at,
			'description'=>$description, 
			'attribute'=>'Default', 
			'sku'=>$product_sku,   
			'seller_id'=>$seller_id,  
			'seller_name'=>$seller_name,   
			'product_price'=>$product_price,  
			'visibility'=>$visibility,  
			'status'=>$status,  
			'short_description'=>$short_description,  
			'length'=>$length,   
			'breadth'=>$breadth,   
			'height'=>$height,   
			'dimension'=>'',   
			'weight'=>$weight,   
			'manufacturer'=>$manufacturer,   
			'color'=>'black',   
			'manufacturer_country'=>'IN',    
			'url_key'=>$url_key,   
			'tax_class'=>'',   
			'hsn_code'=>$hsn,   
			'gst_rate'=>$gst_rate,    
			'warranty_term'=>$warranty_term,    
			'is_category_type_battery'=>$is_category_type_battery,    
			'is_category_type_dangerous'=>$is_category_type_dangerous,     
			'universal_parts'=>$partType,      
			'discount_per'=>$discount_per,  
			'specialPriceFromDate'=>$specialPriceFromDate,  
			'specialPriceToDate'=>$specialPriceToDate,  
			'costofitem'=>$costofitem,   
			'product_category'=>$selected_cat_name,
			'bp_category'=>$parent_cat_name,
			'sub_category'=>$child_cat_name,
			'discount'=>0,
			'discount_price'=>$discount_price,
			
			  
			
		);	 
			
		/* echo "<pre>";
			print_r($mainArray);	
		echo "</pre>";    */
					
		return $mainArray;	
	}	
	
	
	public function create_temp_product_report($product_id,$delete)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');

		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$productArray=$inventory_helpers->get_product_array($product_id,$delete);	
		
		$json=json_encode($productArray);
		
		$select="select * from product_temp_report where product_id='".$product_id."'";
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			$today=date('Y-m-d H:i:s');
			$sql="insert into product_temp_report (product_id,product_details,cr_date) values ('".$product_id."','".$json."','".$today."')";	
			$connection->query($sql);
		}
		
		/* echo "<pre>";
			print_r($productArray);
		echo "</pre>"; */
	
	
	}	
	
	public function create_master_product_report($product_id,$delete)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');	
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$productArray=$inventory_helpers->get_product_array($product_id,$delete);	 
		$mianarray=$productArray;
		
		/* echo "<pre>";
			print_r($mianarray);
		echo "</pre>";  */
		  
		
		$json=json_encode($productArray);

		$today_cr=date("Y-m-d H:i:s"); 
		
		$sql = "Select * FROM inventory_master where product_id='".$product_id."'";
		$result = $connection->fetchAll($sql); 
		
		if(empty($result))
		{	  
			$inventory_helpers ->create_new_row($product_id,$json,'new');  
		}     
		else
		{	
				$sql = "Select * FROM product_temp_report where product_id='".$product_id."'";
				$results = $connection->fetchAll($sql); 
				
				$product_details=$results[0]['product_details'];	
				$product_details=json_decode($product_details);	
				
				/* echo "<pre>"; 
						print_r($product_details); 
				echo "</pre>";	 */
					
				
				$original_json=json_encode($mianarray);
				$stored_json=json_encode($product_details);
				
				$check_change=$buyer_helpers->check_data_change($original_json,$stored_json);
				
				if($check_change!=0)
				{
					$json=json_encode($mianarray);  
					  
					$inventory_helpers->create_new_row($product_id,$json,'product_admin');  
					$inventory_helpers->update_temp_product_table($product_id,$json);  	
				}  		
								
				/*
				
				if($mianarray['product_name']!=$product_details->product_name)
				{
					
					$product_details->product_name=$mianarray['product_name'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'product_name');  
					$inventory_helpers->update_temp_product_table($product_id,$json);  
				} 

				if($mianarray['description']!=$product_details->description)
				{
					
					$product_details->description=$mianarray['description'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'description');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 

				if($mianarray['seller_id']!=$product_details->seller_id)
				{
					
					$product_details->seller_id=$mianarray['seller_id'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'seller_id');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['seller_name']!=$product_details->seller_name)
				{
					
					$product_details->description=$mianarray['seller_name'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'seller_name');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 

				if($mianarray['product_price']!=$product_details->product_price)
				{
					
					$product_details->product_price=$mianarray['product_price'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'product_price');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 

				if($mianarray['status']!=$product_details->status)
				{
					
					$product_details->status=$mianarray['status'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'status');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 

				if($mianarray['short_description']!=$product_details->short_description)
				{
					
					$product_details->short_description=$mianarray['short_description'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'short_description');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['length']!=$product_details->length)
				{
					
					$product_details->length=$mianarray['length'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers->create_new_row($product_id,$json,'length');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['breadth']!=$product_details->breadth)
				{
					
					$product_details->breadth=$mianarray['breadth'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'breadth');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['height']!=$product_details->height)
				{
					
					$product_details->height=$mianarray['height'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'height');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['dimension']!=$product_details->dimension)
				{
					
					$product_details->dimension=$mianarray['dimension'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'dimension');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['weight']!=$product_details->weight)
				{
					
					$product_details->weight=$mianarray['weight'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'weight');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['manufacturer']!=$product_details->manufacturer)
				{
					
					$product_details->manufacturer=$mianarray['manufacturer'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'manufacturer');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['color']!=$product_details->color)
				{
					
					$product_details->color=$mianarray['color'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'color');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['manufacturer_country']!=$product_details->manufacturer_country)
				{
					
					$product_details->manufacturer_country=$mianarray['manufacturer_country'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'manufacturer_country');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['tax_class']!=$product_details->tax_class)
				{
					
					$product_details->tax_class=$mianarray['tax_class'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'tax_class');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['hsn_code']!=$product_details->hsn_code)
				{
					
					$product_details->hsn_code=$mianarray['hsn_code'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'hsn_code');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['gst_rate']!=$product_details->gst_rate)
				{
					
					$product_details->gst_rate=$mianarray['gst_rate'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'gst_rate');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['warranty_term']!=$product_details->warranty_term)
				{
					
					$product_details->warranty_term=$mianarray['warranty_term'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'warranty_term');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['is_category_type_battery']!=$product_details->is_category_type_battery)
				{
					
					$product_details->is_category_type_battery=$mianarray['is_category_type_battery'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'is_category_type_battery');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['is_category_type_dangerous']!=$product_details->is_category_type_dangerous)
				{
					
					$product_details->is_category_type_dangerous=$mianarray['is_category_type_dangerous'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'is_category_type_dangerous');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 	
				
				if($mianarray['universal_parts']!=$product_details->universal_parts)
				{
					
					$product_details->universal_parts=$mianarray['universal_parts'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'universal_parts');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 

				if($mianarray['discount_per']!=$product_details->discount_per)
				{
					
					$product_details->discount_per=$mianarray['discount_per'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'discount_per');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['specialPriceFromDate']!=$product_details->specialPriceFromDate)
				{
					
					$product_details->specialPriceFromDate=$mianarray['specialPriceFromDate'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'specialPriceFromDate');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['specialPriceToDate']!=$product_details->specialPriceToDate)
				{
					
					$product_details->specialPriceToDate=$mianarray['specialPriceToDate'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'specialPriceToDate');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['costofitem']!=$product_details->costofitem)
				{
					
					$product_details->costofitem=$mianarray['costofitem'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'costofitem');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['product_category']!=$product_details->product_category)
				{
					
					$product_details->product_category=$mianarray['product_category'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'product_category');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['bp_category']!=$product_details->bp_category)
				{
					
					$product_details->bp_category=$mianarray['bp_category'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'bp_category');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['sub_category']!=$product_details->sub_category)
				{
					
					$product_details->sub_category=$mianarray['sub_category'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'sub_category');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['discount']!=$product_details->discount)
				{
					
					$product_details->discount=$mianarray['discount'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'discount');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				if($mianarray['discount_price']!=$product_details->discount_price)
				{
					
					$product_details->discount_price=$mianarray['discount_price'];
					
					$json=json_encode($product_details);  
					  
					$inventory_helpers ->create_new_row($product_id,$json,'discount_price');  
					$inventory_helpers->update_temp_product_table($product_id,$json); 
				} 
				
				
				*/
				  
				
				
				
		}		 
		
		
		
		
	
	
	}	
	
	
	public function insert_inventory_master_report($product_id,$delete)  	       	 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$inventory_helpers->create_temp_product_report($product_id,$delete);  
		$inventory_helpers->create_master_product_report($product_id,$delete);  
		
		
		
	}	
	
	
	
	public function create_new_row($product_id,$json,$reason) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$created_at=date('Y-m-d H:i:s');  
		
		$sql="INSERT INTO `inventory_master`(`product_id`, `details`, `cr_date`, `reason`) VALUES ('".$product_id."','".$json."','".$created_at."','".$reason."')"; 
		$connection->query($sql);      
		
	}	  
	
	
	public function update_temp_product_table($product_id,$json) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$created_at=date('Y-m-d H:i:s');  
		 
		$sql="Update product_temp_report set product_details='".$json."' where product_id='".$product_id."'";
		$connection->query($sql);        
		
	}	   
	 
	
	
	
	
}