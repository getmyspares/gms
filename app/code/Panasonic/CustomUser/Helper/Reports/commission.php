<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Commission extends AbstractHelper
{ 
    public function commission_general()
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		
		$helper = $objectManager->create('Baljit\Buynow\Helper\Data');
		
		$admin_comm = $helper->getConfig('buynows/general/button_titlee');
		if(empty($admin_comm))
		{
			$admin_comm=0;
		}
		
		
		$admin_comm_tax = $helper->getConfig('buynows/general/admin_comm_tax');
		if(empty($admin_comm_tax))
		{
			$admin_comm_tax=0;
		}
		
		
		$admin_comm_tds = $helper->getConfig('buynows/general/admin_comm_tds');
		if(empty($admin_comm_tds))
		{
			$admin_comm_tds=0;
		}
		
		
	    $gst_tcs = $helper->getConfig('buynows/general/gst_tcs');
	    if(empty($gst_tcs))
	    {
			$gst_tcs=0;
	    } 
		
		
		$shipping_gst = $helper->getConfig('buynows/general/shipping_gst');
		if(empty($shipping_gst))
		{
			$shipping_gst=0;
		}
		
		
		$shipping_tds = $helper->getConfig('buynows/general/shipping_tds');
		if(empty($shipping_tds))
		{
			$shipping_tds=0;
		}
		
		
		$razorpay_tds = $helper->getConfig('buynows/general/razorpay_tds');
		if(empty($razorpay_tds))
		{
			$razorpay_tds=0;
		}
		
		
		
		$nodal_tax = $helper->getConfig('buynows/general/nodal_tax');
		if(empty($nodal_tax))
		{
			$nodal_tax=0;
		}
		
		$admin_comm;
		$admin_comm_tax;
		$admin_comm_tds;
		$gst_tcs;
		$shipping_gst;
		$shipping_tds;
		$razorpay_tds;
		$nodal_tax; 
		
		
		
		
		
		$array=array(
			'admin_comm'=>$admin_comm,	
			'admin_comm_tax'=>$admin_comm_tax,	
			'admin_comm_tds'=>$admin_comm_tds,
			'gst_tcs'=>$gst_tcs,	  
			'shipping_gst'=>$shipping_gst,	 
			'shipping_tds'=>$shipping_tds,	
			'razorpay_tds'=>$razorpay_tds,  
			'nodal_tax'=>$nodal_tax,  
			'cod_tax'=>5  
		);	  
		
		
		echo "<pre>";
			print_r($array);
		echo "</pre>";
		
		$json=json_encode($array);
		
		$today=date('y-m-d H:i:s');
		
		//$sql="insert into commission_general_report (cr_date,report_details) values ('".$today."','".$json."')";
		//$connection->query($sql);
	    	

	}		
	   
}