<?php

namespace Panasonic\CustomUser\Helper\Reports;
use \Magento\Framework\App\Helper\AbstractHelper;

class Gsttcs extends AbstractHelper
{
	public function __construct(
		\Magento\Framework\App\ResourceConnection $resource
    ) {
		$this->_resource = $resource;
	}

	function check_order_return($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('wk_rma') 
                  ->where('increment_id = ?', $order_inc_id);
       
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return $return_date=$result[0];	
		}	
		else
		{
			return 0;	
		}
		
	}	
	
	
	function report_get_tax_list($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
					->from('order_tax_list') 
					->where('order_id = ?', $order_inc_id);   
		    
		$result = $connection->fetchAll($select);  
		if(!empty($result)) { 
			
			$tax_array=json_decode($result[0]['tax_list']);		
			return $tax_array; 
		}
		else {
			return 0;
		}
	}	
	
	function report_order_gst($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();	
		
		$orderArray=$order->getData();  	
		
		$cgst_amount=0;
		$sgst_amount=0;
		$igst_amount=0;
		$utgst_amount=0; 
		
		if(isset($orderArray['cgst_amount']))
		{
			$cgst_amount=$orderArray['cgst_amount'];
		}
		
		if(isset($orderArray['sgst_amount']))
		{
			$sgst_amount=$orderArray['sgst_amount'];
		}
		
		if(isset($orderArray['igst_amount']))
		{
			$igst_amount=$orderArray['igst_amount'];
		}
		
		if(isset($orderArray['utgst_amount']))
		{
			$utgst_amount=$orderArray['utgst_amount'];
		}
		
		
		
		$total_gst=$cgst_amount+$sgst_amount+$igst_amount+$utgst_amount;
		
		$array=array(
			'cgst_amount'=>$cgst_amount,
			'sgst_amount'=>$sgst_amount,
			'igst_amount'=>$igst_amount,
			'utgst_amount'=>$utgst_amount,
			'total_gst'=>$total_gst,
		);
		
		return $array; 
		
	}	
	
	
	function report_customer_billing($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();


		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');

		$buyer_address='';
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		} 

		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;

		
		$array=array(
			'buyer_name'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_name),	
			'buyer_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_city),	
			'buyer_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_state),	
			'buyer_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_pincode),		
			'buyer_country_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_country_id),	
			'buyer_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_address),		
		);
		
		return $array; 
		
		
	}	
	
	
	function report_customer_shipping($order_inc_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();


		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','shipping');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','shipping');

		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');

		$buyer_address='';
		$buyer_street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}  

		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;

		
		$array=array(
			'shipping_name'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_name),	
			'shipping_city'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_city),	
			'shipping_state'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_state),	
			'shipping_pincode'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_pincode),		
			'shipping_country_id'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_country_id),	
			'shipping_address'=>preg_replace('/[^A-Za-z0-9. -]/', '', $buyer_address),		
		);
		
		
		return $array; 
		
		
	}	 
	
	
	function sales_report($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
		
		$orderArray=$order->getData();  			
		
		/* echo "<pre>";
			print_r($orderArray);
		echo "</pre>";  */
		
		$buyer_id=$orderArray['customer_id'];
		$created_at=$orderArray['created_at'];
		$tax_amount=$orderArray['tax_amount'];
		$shipping_amount=$orderArray['shipping_amount'];
		$order_total_amount=$orderArray['subtotal_incl_tax'];
		$order_status=$orderArray['status'];
		$coupon_code=$orderArray['coupon_code'];
		$discount_description=$orderArray['discount_description'];
		$grand_total=$orderArray['grand_total'];
		
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');   
		
		
		$tax_list_array=$sales_helpers->report_get_tax_list($order_inc_id);
		if($tax_list_array=='0')
		{
			die;	 
		}	

	
		
		$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_gst=$tax_helpers->tax_logistic_gst($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$tax_helpers->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		$shipping_date=$tax_helpers->tax_get_order_shipping_date($order_id);
		if($shipping_date==0) 
		{ 
			$shipping_date='-';		 
		}

		if($coupon_code=='')
		{ 
			$coupon_code='-';		
		}	
		
		
		
		
		
		$buyer_billing=$this->report_customer_billing($order_inc_id);
		$buyer_shipping=$this->report_customer_shipping($order_inc_id);
		$gst_array=$this->report_order_gst($order_inc_id);
		
		
		
		
		
		
		$buyer_name=$buyer_billing['buyer_name'];
		$buyer_city = $buyer_billing['buyer_city'];
		$buyer_state =$buyer_billing['buyer_state'];
		$buyer_pincode = $buyer_billing['buyer_pincode'];
		$buyer_country_id = $buyer_billing['buyer_country_id'];
		$buyer_address = $buyer_billing['buyer_address'];
		
		
		
		$delivery_name=$buyer_shipping['shipping_name'];
		$delivery_city = $buyer_shipping['shipping_city'];
		$delivery_state = $buyer_shipping['shipping_state'];
		$delivery_pincode = $buyer_shipping['shipping_pincode'];
		$delivery_country_id = $buyer_shipping['shipping_country_id'];
		
		$delivery_adress = $buyer_shipping['shipping_address'];
		
		
		$productName='';  
		$productQty='';
		$productCat='';
		$productDescp='';
		$trimstring='';
		
		$productLength='';
		$productBreath='';
		$productHeight='';
		$productAbsWeight='';
		$productLogisticWeight='';
		$productDiscount='';
		$productDiscountAmount='';
		$productHsn='';
		$productGstRate='';
		  
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			$product_idd=$item->getProductId();
			
			$prodnameee=str_replace('/','',$item->getName());
			$prodnameee=str_replace("'","",$prodnameee);
			$prodnameee=str_replace(".","",$prodnameee); 
			   
			$productName .=$prodnameee.',';
			//$productName .=$item->getName().',';
			$productQty .=$item->getQtyOrdered().',';
			
			
			
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_idd);
			
			
			
			$categories = $product->getCategoryIds(); 
			$productDescp = $product->getDescription();
			$productHsn .= $product->getHsn().',';
			$productGstRate .= $product->getGstRate().',';
			 
			
			 
			
			
			$productDescp=str_replace('/','',$productDescp);
			$productDescp=str_replace("'","",$productDescp);
			$productDescp=str_replace('.','',$productDescp);
			 
			if (strlen($productDescp) > 25) {
				$trimstring .= substr($productDescp, 0, 25).',';
			} else {
				$trimstring .= $productDescp.',';
			}
			 
			
			foreach($categories as $category){
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
				$productCat .=$cat->getName().',';
			}
			
			
			$sellerarray[]=$tax_helpers->tax_get_product_seller($order_inc_id,$product_idd);
			
			$productLength .= $product->getData('ecom_length').',';
			$productBreath .= $product->getData('ecom_breadth').',';
			$productHeight .= $product->getData('ecom_height').',';
			
			$productAbsWeight .=$product->getWeight().',';
			
			$logistic_weight = $product->getData('ecom_length')*$product->getData('ecom_breadth')*$product->getData('ecom_height');
			if(!empty($logistic_weight)){
				 if($logistic_weight>1){
					 $logistic_weight = $logistic_weight/5000;
				 }else{
					 $logistic_weight = 0;
				 }
			}
			
			$productLogisticWeight .= $logistic_weight.',';
			
			
			$productDiscount .=round($product->getDiscountPercent()).'%'.',';
			$productDiscountAmount .=round($product->getDiscountAmount()).'%'.',';
			 
			
			
		}

		$resultarray=array_values(array_unique($sellerarray));
		
		$seller_details='';
		$seller_code='';
		$seller_name='';
		$seller_comp_address='';
		$seller_zipcode='';
		
		
		
		
		
		
		foreach($resultarray as $seller_id)
		{
			
			$seller_details=json_decode($tax_helpers->tax_get_seller_array($seller_id));		
			
			$seller_code .=$seller_details->id.',';
			$seller_name .=$seller_details->seller_name.',';
			$seller_comp_address .=$seller_details->seller_comp_address.',';
			$seller_zipcode .=$seller_details->seller_zipcode.',';
			
		}  	 
		
		
		$product_name=substr($productName,0,-1);
		$product_qty=substr($productQty,0,-1);
		$product_category=substr($productCat,0,-1);
		$product_description=substr($trimstring,0,-1);
		
		$product_length=substr($productLength,0,-1);
		$product_breath=substr($productBreath,0,-1);
		$product_height=substr($productHeight,0,-1);
		$product_abs_weight=substr($productAbsWeight,0,-1);
		$product_logistic_weight=substr($productLogisticWeight,0,-1);
		$product_discount=substr($productDiscount,0,-1);
		$product_discount_amount=substr($productDiscountAmount,0,-1);
		$product_hsn=substr($productHsn,0,-1);
		$product_gst_rate=substr($productGstRate,0,-1);
		   
		
		$sellercode=substr($seller_code,0,-1);
		$sellername=substr($seller_name,0,-1);
		$sellercompaddress=substr($seller_comp_address,0,-1);
		$sellerzipcode=substr($seller_zipcode,0,-1);
		
		
		
		$admin_comm=$tax_helpers->tax_admin_commission($order_total_amount,$order_inc_id);
		$admin_comm_tax=$tax_helpers->tax_admin_commission_tax($order_total_amount,$order_inc_id);
		$admin_comm_tds=$tax_helpers->tax_admin_commission_tds($order_total_amount,$order_inc_id);
		
		$total_admin_comm=$admin_comm + $admin_comm_tax;
		
		
		$payment_type='';
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0; 
		$payble_to_razorpay=0;  
		
		$razor_pay=json_decode($tax_helpers->tax_razorpay_expense($order_inc_id));
		
		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		
		
		if($razor_pay->type=='online')
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			$razorpay_tds=$tax_helpers->tax_razorpay_tds($razor_pay,$order_inc_id); 
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			//$payble_to_razorpay=$payment_fee-$razorpay_tds;
			
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		}
		else 
		{
			$payment_type='COD';
			$payble_to_razorpay='0'; 
			$seller_comm = $seller_comm -($noddle);
		}
		 
		$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($created_at)); 
		$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
		
		$seller_city='';		
		$seller_state='';		
		$cod_charge='';		
		$cod_tax='';		
		$cod_expense='';		
		$delivery_date='';		

		$product_name = preg_replace('/[^A-Za-z0-9. -]/', '', $product_name);
		
		$payment_ref='';
		$payment_method=$order->getPayment()->getMethod();
		if($payment_method!='cashondelivery')
		{
			$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
			$check_mobile=$api_helpers->check_order_from_mobile($order_inc_id);
			
			if($check_mobile==0)   
			{
				$select = $connection->select()
					  ->from('sales_payment_transaction') 
					  ->where('order_id = ?', $orderId);
		    
				$result = $connection->fetchAll($select); 
				
				$payment_id=$result[0]['txn_id'];
			}
			else
			{
				$select = $connection->select()
					  ->from('mobile_sales_order') 
					  ->where('order_increment_id = ?', $order_inc_id);
				
				$result = $connection->fetchAll($select);
				
				$payment_id=$result[0]['payment_details'];  
			}	
		
			$payment_ref=$payment_id;
		}	
		
		
		
		//echo $sales_helpers->check_order_return($order_inc_id);
		
		
		$total_gst=$gst_array['total_gst'];
		$shipping_gst_rate=$tax_list_array->shipping_gst;	 
		

		/*	
		$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_gst=$tax_helpers->tax_logistic_gst($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$tax_helpers->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		*/
		
		$shipping_gst_amount=$order_shipping_amount-$logistic_gst;
		if($shipping_gst_amount < 0)
		{
			$shipping_gst_amount=0;		
		}		
		
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,	
			'invoice_id'=>$invoice_id,	
			'created_at'=>$created_at,
			'order_status'=>$order_status,	 
			'movement_type'=>'',	  
			'payment_type'=>$payment_type,	 	 
			'payment_ref'=>$payment_ref,	 	 
			'return_req_date'=>'',	 	 
			'return_req_reason'=>'',	 
			'buyer_id'=>$buyer_id,		
			'billing_name'=>$buyer_name,		
			'billing_address'=>$buyer_address,		
			'billing_city'=>$buyer_city,		
			'billing_state'=>$buyer_state,		
			'billing_pincode'=>$buyer_pincode,	 	
			'shipping_name'=>$buyer_name,		
			'shipping_address'=>$buyer_address,		
			'shipping_city'=>$buyer_city,		
			'shipping_state'=>$buyer_state,		
			'shipping_pincode'=>$buyer_pincode,	 	
			'bp_group'=>'',	 	
			'profit_center'=>'',	
			'product_category'=>$product_category,		
			'product_name'=>$product_name,	
			'product_description'=>$product_name,	 
			'hsn_code'=>$product_hsn,	 
			'gst_rate'=>$product_gst_rate,	 
			'product_qty'=>$product_qty,	 
			'unit_measurement'=>'',	 
			'currency'=>'',	 
			'gross_price_amount'=>$grand_total,	
			'discount'=>'',
			'discount_type'=>'',
			'discount_amount'=>'',
			'coupon_number'=>'',
			'coupon_amount'=>'',
			'total_discount'=>'', 
			'net_sale_value'=>'', 
			'gst_amount'=>$total_gst,    
			'product_invoice_value'=>'',    
			'shipping_sac_code'=>'',    
			'shipping_gst_rate'=>$shipping_gst_rate,     
			'shipping'=>$logistic_gst,     
			'shipping_gst_amount'=>$shipping_gst_amount,      
			'shipping_invoice_total'=>$order_shipping_amount,  
			'total_invoice_value'=>'',  
			'shipping_date'=>'',  
			'delivery_date'=>'',  
			'cancellation_date'=>'',   
			'recognition_date'=>'',   
			'recognition_status'=>'',   
		); 	 
		
		 
		/* echo "<pre>"; 
			print_r($mainarray);
		echo "</pre>";   
		die; */
		
		
		$mainarray=array(
			'order_inc_id'=>$order_inc_id,	
			'invoice_id'=>$invoice_id,	
			'created_at'=>$created_at,	
			'buyer_name'=>$buyer_name,	
			'buyer_city'=>$buyer_city,	
			'buyer_state'=>$buyer_state,	
			'buyer_pincode'=>$buyer_pincode,	
			'delivery_name'=>$delivery_name,	
			'delivery_adress'=>$delivery_adress,	
			'delivery_city'=>$delivery_city,	
			'delivery_state'=>$delivery_state,	
			'delivery_pincode'=>$delivery_pincode,	
			'product_name'=>$product_name,	
			'product_qty'=>$product_qty,	
			'product_category'=>$product_category,	
			'grand_total'=>$grand_total,	
			'sellercode'=>$sellercode,	
			'sellername'=>$sellername,	
			'sellercompaddress'=>$sellercompaddress,	
			'seller_city'=>$seller_city,	
			'seller_state'=>$seller_state,	
			'sellerzipcode'=>$sellerzipcode,	
			'product_description'=>$product_name,	 
			'tax_amount'=>$tax_amount,	
			'logistic_gst'=>$logistic_gst,	
			'logistic_tax'=>$logistic_tax,	
			'order_shipping_amount'=>$order_shipping_amount, 	 
			'admin_comm'=>$admin_comm,	 
			'admin_comm_tax'=>$admin_comm_tax,	 
			'admin_comm_tds'=>$admin_comm_tds,	 
			'total_admin_comm'=>$total_admin_comm,	 
			'payment_type'=>$payment_type,	 
			'payment_tax'=>$payment_tax,	 
			'payment_fee'=>$payment_fee,	 
			'payment_amount'=>$payment_amount,	 
			'razorpay_tds'=>$razorpay_tds,	 
			'payble_to_razorpay'=>$payble_to_razorpay,	 
			'cod_charge'=>$cod_charge,	 
			'cod_tax'=>$cod_tax,	 
			'cod_expense'=>$cod_expense,	 
			'order_status'=>$order_status,	 
			'created_at'=>$created_at,	 
			'shipping_date'=>$shipping_date,	 
			'delivery_date'=>$delivery_date,	 
			'realization_days'=>$RealizationDays,	 
			'product_length'=>$product_length,	 
			'product_breath'=>$product_breath,	 
			'product_height'=>$product_height,	 
			'product_abs_weight'=>$product_abs_weight,	 
			'product_logistic_weight'=>$product_logistic_weight,	 
			'product_discount'=>$product_discount,	 
			'discount_description'=>$discount_description,	 
			'product_discount_amount'=>$product_discount_amount,	 
			'coupon_code'=>$coupon_code,	 
			'coupon_amount'=>$coupon_code 
		);
		 
	
	}	
	
	
	
	public function report_sale_report($order_inc_id,$type=null)    
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$this->insert_shipping_report($order_inc_id,'shipping'); 
		$type='gsttcs';
		foreach ($order->getAllItems() as $item)
		{
			$item_id=$item->getId();
			$product_id=$item->getProductId();
			$product_qty=$item->getQtyOrdered();
			if($type=='refund') {
				$isrefunded =  $this->checkIfItemRefunded($product_id,$item_id);
				if($isrefunded) {
					$this->insert_sales_report($order_inc_id,$product_id,$item_id,$product_qty,$type);
				}	
			} else {
				$this->insert_sales_report($order_inc_id,$product_id,$item_id,$product_qty,$type);
			}
		}      
	}	

	public function checkIfItemRefunded($product_id,$item_id)
	{	/* code  cleaned @ritesh */
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$select = "select entity_id from sales_creditmemo_item where product_id=$product_id and order_item_id=$item_id";
		$result = $connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0]['entity_id'];
		return $status;
	}

	public function insert_shipping_report($order_inc_id,$type='shipping') 
	{   /*code cleaned    @ritesh*/
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		$shippingAddressObj = $order->getShippingAddress();
		$shippingAddressArray = $shippingAddressObj->getData(); 
		$customer_shipping_state = $shippingAddressArray['region'];
		$customerID = $orderArray['customer_id'];
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerID);
		$customer_gstin = $customerObj->getData('gst_number');
		
		$items = $order->getAllVisibleItems();
		$product_qty = count($items);	
		foreach ($order->getAllItems() as $item)
		{
			$item_id=$item->getId();
			$product_qty=$product_qty+1;
		}	 
		
		
		$buyer_id=$orderArray['customer_id']; 
		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$grand_total=$orderDetailArray->grand_total;
		$total_discount=$orderDetailArray->total_discount;
		$total_gst=$orderDetailArray->total_gst;
		
		
		$shipping_base=$orderDetailArray->shipping_base;
		
		$shipping_gst=$orderDetailArray->shipping_gst;
		
		$shipping_expense=$orderDetailArray->shipping_expense;
		
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		
		$payment_type='COD';	 
		
		if($payment_id!='COD') 
		{
			$payment_type='Online';	 
		}	
		
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		//print_r($seller_details);
		
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_state=$seller_details->seller_state;
		$seller_gst=$seller_details->seller_gst;
		$pana_state=0;
		$pana_gst=0; 
		$supply_type='Intra-State';
		$seller_state=strtolower($seller_state);
		if($seller_state=="new delhi") { 
			$seller_state='Delhi';	
		}  	
		
		if(strtolower($seller_state)==strtolower($customer_shipping_state))
		{
			$supply_type='Intra-State';	
		} 
		/* elseif( $customer_shipping_pincode==$seller_zipcode){
			loophole  exist in the current setup , condition to check loophole ,need  to  fix @ritesh
			$supply_type='Intra-State';
		} */ else  {
			$supply_type='Inter-State';	
		}

		$sql_pana="select * from gst_tcs_panasonic where state_code='".$seller_state."'";
		$results_pana = $connection->fetchAll($sql_pana);    
		if(!empty($results_pana))
		{	
			$pana_state=$results_pana[0]['state_code'];
			$pana_gst=$results_pana[0]['gst']; 
		}
		
		$document_date=date('Y-m-d',strtotime($order_cr_date));
		$posting_date=date("Y-m-t", strtotime($order_cr_date));
		
		$product_name='Shipping & Handling';
		$product_hsn='996812';
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$igst_amount_int=round($igst_amount);
		$cgst_amount_int=round($cgst_amount);
		$sgst_amount_int=round($sgst_amount);
		$utgst_amount_int=round($ugst_amount);
		
		$igst_rate=0;
		$cgst_rate=0;
		$sgst_rate=0;
		$utgst_rate=0;
		
		$igst_gst_amount=0;
		$cgst_gst_amount=0;
		$sgst_gst_amount=0;
		$utgst_gst_amount=0;
		
		$igst_gst_amount_tcs=0;
		$cgst_gst_amount_tcs=0;
		$sgst_gst_amount_tcs=0;
		$utgst_gst_amount_tcs=0;
		$gst_tcs_rate=$orderDetailArray->gst_tcs_rate; 
		$value_supplier_return=0;
		$shipping_base=$orderDetailArray->shipping_base;
		$shipping_expense=$orderDetailArray->shipping_expense;
		$gross_price=$shipping_base;
		$net_amount_lible_tcs=$gross_price-$value_supplier_return;
		
		if($igst_amount_int!=0) {
			$igst_rate = $gst_tcs_rate;
			$igst_gst_amount = $total_gst;
			$igst_gst_amount_tcs = ($net_amount_lible_tcs*$gst_tcs_rate)/100;
		}	
		
		if($cgst_amount_int!=0 && $sgst_amount_int!=0) {
			$cgst_rate=$gst_tcs_rate/2;
			$sgst_rate=$gst_tcs_rate/2;
			$cgst_gst_amount=$total_gst/2;
			$sgst_gst_amount=$total_gst/2;
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$sgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}	
		
		if($cgst_amount_int!=0 && $utgst_amount_int!=0) {
			$cgst_rate=$gst_tcs_rate/2;
			$utgst_rate=$gst_tcs_rate/2;
			$cgst_gst_amount=$total_gst/2;
			$utgst_gst_amount=$total_gst/2;
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$utgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}

		if($supply_type=='Inter-State') {
			$total_gst_tcs=$igst_gst_amount_tcs;
		} else {
			$total_gst_tcs= $cgst_gst_amount_tcs+$sgst_gst_amount_tcs+$utgst_gst_amount_tcs;
		}

		$transection_type='Regular Supply';
		$document_type="Tax Invoice";
		$orignal_invoice_date='';
		$orignal_invoice='';
		if($type=='refund')
		{
			$transection_type="Sales returns / Deficiency in service (Credit Note)";
			$creditmemo_details = $this->getCreditMemo($order_id);
			$document_type = "Credit Note";
			$orignal_invoice_date = $document_date;
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			$orignal_invoice = $invoice_id;
			$invoice_id = $credit_memo_increment_id;
			$document_date = $credit_memo_created_date;
			$net_amount_lible_tcs= - $net_amount_lible_tcs;
			$igst_gst_amount= -$igst_gst_amount;
			$cgst_gst_amount= -$cgst_gst_amount;
			$sgst_gst_amount= -$sgst_gst_amount;
			$utgst_gst_amount= -$utgst_gst_amount;
			$total_gst_tcs = - $total_gst_tcs;
			$igst_gst_amount_tcs = -$igst_gst_amount_tcs; 
			$cgst_gst_amount_tcs = -$cgst_gst_amount_tcs;
			$sgst_gst_amount_tcs = -$sgst_gst_amount_tcs;
			$posting_date = date("Y-m-t", strtotime($credit_memo_created_date));
			$value_supplier_return = -$gross_price;
			$gross_price="";
		}
		if(!empty($customer_gstin)) {
			$is_customer_registered = "Registered";
		} else {
			$is_customer_registered = "";
		}

		$mainArray=array(    
			'order_inc_id'=>$order_inc_id,		
			'order_date'=>$order_cr_date,		
			'invoice_id'=>$invoice_id,	 	
			'invoice_date'=>$order_cr_date,	 	
			'seller_id'=>$seller_id,	  	 
			'seller_gst'=>$seller_gst,	   	 
			'seller_register'=>'Registered',	   	 
			'seller_state'=>$seller_state,	   	 
			'gms_ecom_gst'=>$pana_gst,	   	 
			'ecom_state'=>$pana_state,	   	 
			'transection_type'=>$transection_type,	   	 
			'document_type'=>$document_type,	   	 
			'orignal_gst'=>'',	   	 
			'orignal_invoice'=>$orignal_invoice,	   	  
			'orignal_invoice_date'=>$orignal_invoice_date,	   	  
			'document_no'=>'',	   	   
			'document_date'=>$document_date,	   	 
			'posting_date'=>$posting_date, 	   	 
			'buyer_place'=>$pana_state,	   	  
			'supply_type'=>$supply_type,	
			'product_name'=>$product_name,	
			'product_hsn'=>$product_hsn,
			'unit_measurement'=>'number',
			'product_qty'=>$product_qty,
			'gross_price_seller'=>$gross_price,   
			'value_supplier_return'=>$value_supplier_return,   
			'net_amount_lible_tcs'=>$net_amount_lible_tcs,   
			
			'igst_rate'=>$igst_rate,
			'cgst_rate'=>$cgst_rate,
			'sgst_rate'=>$sgst_rate,
			'utgst_rate'=>$utgst_rate, 
			'igst_gst_amount'=>$igst_gst_amount, 
			'cgst_gst_amount'=>$cgst_gst_amount, 
			'sgst_gst_amount'=>$sgst_gst_amount, 
			'utgst_gst_amount'=>$utgst_gst_amount, 	 
			
			'igst_gst_amount_tcs'=>$igst_gst_amount_tcs, 
			'cgst_gst_amount_tcs'=>$cgst_gst_amount_tcs, 
			'sgst_gst_amount_tcs'=>$sgst_gst_amount_tcs, 
			'utgst_gst_amount_tcs'=>$utgst_gst_amount_tcs, 
			'total_gst_tcs'=>$total_gst_tcs,   		
			'total_invoice_value'=>$shipping_expense,  
			'buyer_state'=>$customer_shipping_state,
			'customer_gstin_number'=>$customer_gstin,
			'is_customer_registered'=>$is_customer_registered,
		);
		
		$json_data=json_encode($mainArray); 
		$sql="insert into gst_tcs_report (order_id,invoice_id,created_at,sales_details,type) values ('".$order_inc_id."','".$invoice_id."','".$order_cr_date."','".$json_data."','$type')";    
		$connection = $resource->getConnection();
		$connection->query($sql); 	      
	
	}	
	
	public function getCreditMemo($order_id)
	{	/* code  cleaned @ritesh */
		$connection = $this->_resource->getConnection();
		$select = "select * from sales_creditmemo where order_id = $order_id";
		$result = $connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
	}

	public function insert_sales_report($order_inc_id,$product_id,$item_id,$product_qty,$type='gsttcs') 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		$customerID = $orderArray['customer_id'];
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerID);
		$customer_gstin = $customerObj->getData('gst_number');
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		$check_return=$api_helpers->check_product_in_return($order_id,$item_id);	
		
		$return_req_date='';
		$return_req_reason='';
		$pickup_date='';	 
		
		if($check_return==1)
		{	
			$movement_type='Forward';
			$array=$sales_helpers->get_forward_order_tracking($order_id);
			$order_status=$array['status'];
			$pickup_date=$array['pickup_date'];
		}
		else
		{
			$movement_type='Return';
			$array=$sales_helpers->get_return_order_tracking($order_id,$product_id);
			
			$order_status=$array['status'];
			$pickup_date=$array['pickup_date'];
			
			$returnArray=$sales_helpers->order_return_details($order_id,$item_id);
			$return_req_date=$returnArray['rma_created_at'];
			$return_req_reason=$returnArray['rma_reason'];  
		}		
		
		$awb=$tax_helpers->tax_get_order_awb($order_id);
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$grand_total=$orderDetailArray->grand_total;
		$total_discount=$orderDetailArray->total_discount;
		$total_gst=$orderDetailArray->total_gst;
		$shipping_expense=$orderDetailArray->shipping_expense;
		$shipping_base=$orderDetailArray->shipping_base;
		$shipping_gst=$orderDetailArray->shipping_gst;
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		
		$payment_type='COD';	 
		
		if($payment_id!='COD') 
		{
			$payment_type='Online';	
		}	
		
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		//print_r($seller_details);
		
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		
		$seller_zipcode=$seller_details->seller_zipcode;
		$seller_state=$seller_details->seller_state;
		
		$seller_gst=$seller_details->seller_gst;
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		
		
		
		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		
		
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
		
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		
		$street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	
		
		if($seller_state=="new delhi")
		{ 
			$seller_state='Delhi';	
			//$seller_statee='delhi';	
		}

		if(strtolower($seller_state)==strtolower($delivery_state))
		{
			$supply_type='Intra-State';	
		} 
		/* elseif($delivery_pincode == $seller_zipcode){
			loophole exist in the  current system condition to check loophole  which passed
			$supply_type='Intra-State';
		}  */
		else  {
			$supply_type='Inter-State';	
		}		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
		$categories = $product->getCategoryIds();
		$product_name = $product->getName();
		$product_hsn = $product->getHsn();
		$product_gst_rate = $product->getGstRate();
		
		$product_name = preg_replace('/[^A-Za-z0-9. -]/', '', $product_name);
			
		$parent_cat_nam='';
		$parent_cat_name='';
		
		$child_cat_nam='';
		$child_cat_name='';
		
		$selected_cat_nam='';	
		$selected_cat_name='';	 
		$product_invoice_value='';	 
		
		$array=array();
		
		
		//$seller_city=$helpers->get_city_pincode($seller_zipcode);
		//$seller_state=$helpers->get_region_pincode($seller_zipcode); 
		
		
		$discount_per='';  
		$discount_type=''; 
		$discount_amount=''; 
		$coupon_number='';
		$shipping_sac_code='';
		
		
		$shipping_date='';			
		$delivery_date='';	  			
		$cancel_date='';	
		
		
		$productArray=$tax_helpers->order_item_details_product($order_inc_id,$product_id);
		/* echo "<pre>";
			print_r($productArray);
		echo "</pre>";   */   
		
		
		$grand_total=$productArray[0]['row_total'];
		$total_gst=$productArray[0]['total_gst'];
		$gross_price=$productArray[0]['product_base'];
		$total_discount=$productArray[0]['product_discount'];  
		$net_sale_value=$productArray[0]['net_sale_value'];
		$product_invoice_value=$productArray[0]['product_invoice_value'];
		$comission=$productArray[0]['admin_comm'];
		
		
		
		$shipping_base=$productArray[0]['shipping_base'];
		$shipping_gst=$productArray[0]['shipping_gst'];
		$shipping_expense=$productArray[0]['product_shipping'];
		$product_admin_comm=$productArray[0]['product_admin_comm_rate'];
		
		$total_invoice_value=$product_invoice_value+$shipping_expense;  
		 
		
		
		$value_supplier_return=0;
		$net_amount_lible_tcs=$gross_price-$value_supplier_return;
		
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		
		$igst_amount_int=round($igst_amount);
		$cgst_amount_int=round($cgst_amount);
		$sgst_amount_int=round($sgst_amount);
		$utgst_amount_int=round($ugst_amount);
		
		
		$igst_rate=0;
		$cgst_rate=0;
		$sgst_rate=0;
		$utgst_rate=0;
		
		
		$igst_gst_amount=0;
		$cgst_gst_amount=0;
		$sgst_gst_amount=0;
		$utgst_gst_amount=0;
		
		$igst_gst_amount_tcs=0;
		$cgst_gst_amount_tcs=0;
		$sgst_gst_amount_tcs=0;
		$utgst_gst_amount_tcs=0;
		
		$gst_tcs_rate=$orderDetailArray->gst_tcs_rate; 
		
		
		if($igst_amount_int!=0)
		{
			
			$igst_rate = $gst_tcs_rate;
			$igst_gst_amount = $total_gst;
			$igst_gst_amount_tcs = ($net_amount_lible_tcs*$gst_tcs_rate)/100;
		}
		
		if($cgst_amount_int!=0 && $sgst_amount_int!=0)
		{
			$cgst_rate=$gst_tcs_rate/2;
			$sgst_rate=$gst_tcs_rate/2;
			
			$cgst_gst_amount=$total_gst/2;
			$sgst_gst_amount=$total_gst/2;
			
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$sgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}	
		
		if($cgst_amount_int!=0 && $utgst_amount_int!=0)
		{
			$cgst_rate=$gst_tcs_rate/2;
			$utgst_rate=$gst_tcs_rate/2;
			
			$cgst_gst_amount=$total_gst/2;
			$utgst_gst_amount=$total_gst/2;
			
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$utgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}	
		
		if($supply_type=='Inter-State')
		{
			$total_gst_tcs=$igst_gst_amount_tcs;
		} else 
		{
			$total_gst_tcs=$cgst_gst_amount_tcs+$sgst_gst_amount_tcs+$utgst_gst_amount_tcs;
			
		}
		
		$seller_state=strtolower($seller_state);
		if($seller_state=="new delhi") { 
			$seller_state='Delhi';	
		}  	
		
		$pana_state=0;
		$pana_gst=0;
		
		$sql_pana="select * from gst_tcs_panasonic where state_code='".$seller_state."'";
		$results_pana = $connection->fetchAll($sql_pana);    
		if(!empty($results_pana))
		{	
			$pana_state=$results_pana[0]['state_code'];
			$pana_gst=$results_pana[0]['gst']; 
		}
		
		$document_date=date('Y-m-d',strtotime($order_cr_date));
		$posting_date=date("Y-m-t", strtotime($order_cr_date));
		
		$transection_type='Regular Supply';
		$document_type="Tax Invoice";
		$orignal_invoice_date='';
		$orignal_invoice='';
		if($type =='refund')
		{
			$transection_type="Sales returns / Deficiency in service (Credit Note)";
			$creditmemo_details = $this->getCreditMemo($order_id);
			$document_type = "Credit Note";
			$orignal_invoice_date = $document_date;
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			$orignal_invoice = $invoice_id; 
			$invoice_id = $credit_memo_increment_id;
			$document_date = $credit_memo_created_date;
			$net_amount_lible_tcs= - $net_amount_lible_tcs;
			$igst_gst_amount= -$igst_gst_amount;
			$cgst_gst_amount= -$cgst_gst_amount;
			$sgst_gst_amount= -$sgst_gst_amount;
			$utgst_gst_amount= -$utgst_gst_amount;
			$total_gst_tcs = - $total_gst_tcs;
			$igst_gst_amount_tcs = -$igst_gst_amount_tcs; 
			$cgst_gst_amount_tcs = -$cgst_gst_amount_tcs;
			$sgst_gst_amount_tcs = -$sgst_gst_amount_tcs;
			$value_supplier_return = -$gross_price;
			$gross_price="";
			$posting_date = date("Y-m-t", strtotime($credit_memo_created_date));
		}

		if(!empty($customer_gstin)) {
			$is_customer_registered = "Registered";
		} else {
			$is_customer_registered = "";
		}
		
		$mainArray=array(   
			'order_inc_id'=>$order_inc_id,		
			'order_date'=>$order_cr_date,		
			'invoice_id'=>$invoice_id,	 	
			'invoice_date'=>$order_cr_date,	 	
			'seller_id'=>$seller_id,	  	 
			'seller_gst'=>$seller_gst,	   	 
			'seller_register'=>'Registered',	   	 
			'seller_state'=>$seller_state,	   	 
			'gms_ecom_gst'=>$pana_gst,	   	 
			'ecom_state'=>$pana_state,	   	 
			'transection_type'=>$transection_type,	   	 
			'document_type'=>$document_type,	   	 
			'orignal_gst'=>'',	   	 
			'orignal_invoice'=>$orignal_invoice,	   	 
			'orignal_invoice_date'=>$orignal_invoice_date,	   	  
			'document_no'=>'',	   	 
			'document_date'=>$document_date,	   	 
			'posting_date'=>$posting_date, 	   	 
			'buyer_place'=>$pana_state,	   	  
			'supply_type'=>$supply_type,	   	 
			'product_name'=>$product_name,
			'product_hsn'=>$product_hsn,
			'unit_measurement'=>'number', 
			'product_qty'=>$product_qty,
			'gross_price_seller'=>$gross_price,   
			'value_supplier_return'=>$value_supplier_return,   
			'net_amount_lible_tcs'=>$net_amount_lible_tcs,   
			'product_gst_rate'=>$product_gst_rate,
			'igst_rate'=>$igst_rate,
			'cgst_rate'=>$cgst_rate,
			'sgst_rate'=>$sgst_rate,
			'utgst_rate'=>$utgst_rate, 
			'igst_gst_amount'=>$igst_gst_amount, 
			'cgst_gst_amount'=>$cgst_gst_amount, 
			'sgst_gst_amount'=>$sgst_gst_amount,  
			'utgst_gst_amount'=>$utgst_gst_amount, 
			'igst_gst_amount_tcs'=>$igst_gst_amount_tcs, 
			'cgst_gst_amount_tcs'=>$cgst_gst_amount_tcs, 
			'sgst_gst_amount_tcs'=>$sgst_gst_amount_tcs, 
			'utgst_gst_amount_tcs'=>$utgst_gst_amount_tcs, 
			'total_gst'=>$total_gst,   		
			'total_gst_tcs'=>$total_gst_tcs,   		
			'total_invoice_value'=>$product_invoice_value,
			'buyer_state'=>$delivery_state,
			'customer_gstin_number'=>$customer_gstin,
			'is_customer_registered'=>"$is_customer_registered",
		); 
		$json_data=json_encode($mainArray);
		$sql="insert into gst_tcs_report (order_id,invoice_id,created_at,sales_details,type) values ('".$order_inc_id."','".$invoice_id."','".$order_cr_date."','".$json_data."','".$type."')";    
		$connection = $resource->getConnection();
		$connection->query($sql); 	     
	}	
	
	public function get_forward_order_tracking($order_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		
		$approved=0;	
		$processed=0;	
		$shipped=0;	 
		$delivered=0;	
		$status=0;	
		$pickup_date=0;	
		$delivered_dates=0;	
		
		 
		
		$tracking_array=$api_helpers->get_order_tracking($order_id);   
		
		
		if($tracking_array=='0')  
		{
			$approved=1;	 
			$processed=1;	
			$status="Processing";
		}
		else
		{
			$approved=1;	
			$processed=1;	
			$tracking_arrayy=json_decode($tracking_array);	
			
			/* echo "<pre>";
			print_r($tracking_arrayy);
			echo "</pre>"; */
			
			
			$trackArray=$tracking_arrayy[36]->object;
				
			
			$pickup=$tracking_arrayy[9];
			
			
			if(!is_object($pickup))
			{
				$shipped=1;	
				$status="Shipped";
				$pickup_date=$pickup;
			}	
			

			$delivered_date=$tracking_arrayy[21];
			if(!is_object($delivered_date))
			{
				$delivered=1;	
				$status="Delivered";
				$delivered_dates=$delivered_date;
			}

		
		}		
	 
		
		$array=array(
			'pickup_date'=>$pickup_date,	
			'delivered_date'=>$delivered_dates,	
			'status'=>$status,	
		);
		
		return $array;  
	
	}


	public function get_return_order_tracking($order_id)
	{ 
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		
		$approved=0;	
		$processed=0;	
		$shipped=0;	 
		$delivered=0;	
		$status=0;	
		
		$pickup_date=0;
		$delivered_dates=0;
		 
		$tracking_array=$api_helpers->get_order_tracking($order_id);   
		
		if($tracking_array=='0')
		{
			$approved=1;	 
			$processed=1;	
			$status="Approved";
		}
		else
		{
			$approved=1;	
			$processed=1;	
			$tracking_arrayy=json_decode($tracking_array);	
			
			$trackArray=$tracking_arrayy[36]->object;
				
			
			$pickup=$tracking_arrayy[9];
			
			if(!is_object($pickup))
			{
				$shipped=1;	
				$status="Shipped";
				$pickup_date=$pickup;
			}	
			

			$delivered_date=$tracking_arrayy[21];
			if(!is_object($delivered_date))
			{
				$delivered=1;	
				$status="Delivered";
				$delivered_dates=$delivered_date;
			}

		
		}		
	
		
		$array=array(
			'pickup_date'=>$pickup_date,	
			'delivered_date'=>$delivered_dates,	
			'status'=>$status,	
		);
		
		return $array;  
	
	}

	
	
	public function order_return_details($order_id,$item_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$select = $connection->select()
                  ->from('wk_rma') 
                  ->where('order_id = ?', $order_id);
		
		$results = $connection->fetchAll($select);
		$cr_date='';
		$rma_reason='';
		
		if(!empty($results))
		{
			$cr_date=$results[0]['created_at'];	
		}	
		
		
		$select = $connection->select()
                  ->from('wk_rma_items') 
                  ->where('order_id = ?', $order_id)
                  ->where('item_id = ?', $item_id);
		
		$results = $connection->fetchAll($select);
		if(!empty($results))
		{
			$rma_reason=$results[0]['rma_reason'];	
		}	
		
		
		$array=array(
			'rma_created_at'=>$cr_date,
			'rma_reason'=>$rma_reason,
		);
		return $array;
		
	} 

	public function check_gst_tcs_report($increment_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$select = $connection->select()
                  ->from('gst_tcs_report')  
                  ->where('order_id = ?', $increment_id);
		
		$results = $connection->fetchAll($select);
		if(!empty($results))
		{
			return 1;
		}	
		else
		{
			return 0;
		}		
	}	
	
	public function checkiforderrefunded($increment_id){
		$connection = $this->_resource->getConnection();
		$refunded_payment_query = "SELECT * FROM `sales_order` where `state`='closed' and increment_id='$increment_id'";
		$results = $connection->fetchAll($refunded_payment_query);
		$status =  !empty($results) ? 1:0;
		return $status;
	}

	public function check_return_gst_tcs_report($increment_id)
	{
		$isorderrefunded =  $this->checkiforderrefunded($increment_id);
		if(empty($isorderrefunded)){ /* here it means opposite as  0  will generate report */ return 1;  }
		$connection = $this->_resource->getConnection();
		$select = "select * from `gst_tcs_report` where order_id= $increment_id and type='refund'";
		$results = $connection->fetchAll($select);
		$status =  !empty($results) ? 1:0;
		return $status;
	}
		
	
	public function get_product_id_item_id($order_id,$item_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$select = $connection->select()
                  ->from('sales_order_item') 
                  ->where('order_id = ?', $order_id)
                  ->where('item_id = ?', $item_id);
		
		$results = $connection->fetchAll($select);
		if(!empty($results))
		{
			return $results[0]['product_id'];
		}	 
		else
		{
			return 0;
		}		
	}	
	
		
	   
}