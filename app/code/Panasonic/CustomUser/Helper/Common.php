<?php

namespace Panasonic\CustomUser\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Common extends AbstractHelper
{ 
    
	
	public function report_insert_tax_parameter($order_increment_id)  
	{
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$taxarray=$tax_helpers->tax_parameter();  
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId(); 
		
		 
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			$productarray[]=$item->getProductId();
			$sellerarray[]=$custom_helpers->get_product_seller($order_id,$product_id);
			
		}        	
		
		
		$resultarray=array_values(array_unique($sellerarray));
		
		
		if(!empty($resultarray))
		{
			$seller_id=$resultarray[0];	
		}	
		else  
		{
			$seller_id='29';   
		}
		
		$seller_id;
		
		
		
		$product_category=''; 
		$bp_category='';
		$sub_category=''; 
		
		$final_comm=0;
		$final_admin_comm=0;
		
		$count_product=count($productarray);
		$final_comm_array=array();
		$temp_comm=0;
		if($count_product > 1)
		{
			//echo "Multiple Product";
			//echo "<br>";
			
			foreach($productarray as $prod_row)
			{
				$final_comm=$common_helpers->get_multiple_product_logic($prod_row);
				$temp_comm=$temp_comm+$final_comm;
			}   
			
			
			$final_admin_comm=$temp_comm;

			
		}
		else
		{
			//echo "Single Product";
			//echo "<br>";
			
			foreach($productarray as $prod_row)
			{
				$final_comm=$common_helpers->get_single_product_logic($prod_row);
			} 

			$final_admin_comm=$final_comm;
			
		}
		
		
		if($final_admin_comm==0)
		{
			// check seller have commission
			$select = $connection->select()
					  ->from('marketplace_saleperpartner') 
					  ->where('seller_id = ?', $seller_id);
			
			$result = $connection->fetchAll($select);
			
			if(empty($result))
			{
				$seller_new_amount='0.0000';	
			}
			else 
			{
				$seller_new_amount=$result[0]['commission_rate'];	
							
			}
			 
			if($seller_new_amount!='0.0000')
			{ 
				$taxarray=json_decode($taxarray);
				$taxarray->admin_comm=$seller_new_amount;
				$taxarray=json_encode($taxarray);
			}	
		}	
		else
		{
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$final_admin_comm;
			$taxarray=json_encode($taxarray);
		}
		
		
		
		//return $taxarray;    
		
		$sql = "insert into order_tax_list (order_id,tax_list) values ('".$order_increment_id."','".$taxarray."')";
		$connection->query($sql);  
		
		 
		 
	}
	
	public function admin_comm_by_product($order_increment_id,$product_id)  
	{
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$taxarray=$tax_helpers->tax_parameter();  
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId(); 
		
		$sellerarray=array(); 
		foreach ($order->getAllItems() as $item)
		{
			//$product_id=$item->getId();
			$product_idd=$item->getProductId();
			if($product_idd==$product_id)
			{	
				$productarray[]=$item->getProductId();
				$sellerarray[]=$custom_helpers->get_product_seller($order_id,$product_id);
			}
		}        	
		
		
		
		
		if(!empty($sellerarray))
		{	
			$resultarray=array_values(array_unique($sellerarray));
		}
		
		if(!empty($resultarray))
		{
			$seller_id=$resultarray[0];	
		}	
		else  
		{
			$seller_id='29';   
		}
		
		 $seller_id;
		
		
		
		$product_category=''; 
		$bp_category='';
		$sub_category=''; 
		
		$final_comm=0;
		$final_admin_comm=0;
		
		$count_product=count($productarray);
		$final_comm_array=array();
		$temp_comm=0;
		if($count_product > 1)
		{
			//echo "Multiple Product";
			//echo "<br>";
			
			foreach($productarray as $prod_row)
			{
				$final_comm=$common_helpers->get_multiple_product_logic($prod_row);
				$temp_comm=$temp_comm+$final_comm;
			}   
			
			
			$final_admin_comm=$temp_comm;

			
		}
		else
		{
			//echo "Single Product";
			//echo "<br>";
			
			foreach($productarray as $prod_row)
			{
				$final_comm=$common_helpers->get_single_product_logic($prod_row);
			} 

			$final_admin_comm=$final_comm;
			
		}
		
		
		return $final_admin_comm; 
		
		/*
		if($final_admin_comm==0)
		{
			// check seller have commission
			$select = $connection->select()
					  ->from('marketplace_saleperpartner') 
					  ->where('seller_id = ?', $seller_id);
			
			$result = $connection->fetchAll($select);
			
			if(empty($result))
			{
				$seller_new_amount='0.0000';	
			}
			else 
			{
				$seller_new_amount=$result[0]['commission_rate'];	
							
			}
			 
			if($seller_new_amount!='0.0000')
			{ 
				$taxarray=json_decode($taxarray);
				$taxarray->admin_comm=$seller_new_amount;
				$taxarray=json_encode($taxarray);
			}	
		}	
		else
		{
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$final_admin_comm;
			$taxarray=json_encode($taxarray);
		}
		*/
		
		
		
		 
		 
	}
	
	public function report_insert_tax_parameter_old($order_increment_id)  
	{
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		
		
		$taxarray=$tax_helpers->tax_parameter();  
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId(); 
		
		 
		 
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			$productarray[]=$item->getProductId();
			$sellerarray[]=$custom_helpers->get_product_seller($order_id,$product_id);
			
		}        	
		
		
		$resultarray=array_values(array_unique($sellerarray));
		
		
		if(!empty($resultarray))
		{
			$seller_id=$resultarray[0];	
		}	
		else  
		{
			$seller_id='29';   
		}
		
		$seller_id;
		
		//check category have comm
		$single_cat_comm=0;
		foreach($productarray as $prod_row)
		{
			$prod_row;
			
			$category_comm=$common_helpers->get_category_comm($seller_id,$prod_row);		
			$single_cat_comm=$single_cat_comm+$category_comm;
		} 
		
		$single_cat_comm;  
		 
		
		
		  
		// check seller have comm in category
		$category_total_comm=0;
		$select = $connection->select()
				  ->from('customer_entity_varchar') 
				  ->where('entity_id = ?', $seller_id)
				  ->where('attribute_id = ?', '203');
		
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			foreach($productarray as $prod_row)
			{
				
				$category_comm=$common_helpers->get_seller_product_category_comm($seller_id,$prod_row);		
				$category_total_comm=$category_total_comm+$category_comm;
			}
		}
		
		
		$category_total_comm;
		
			
		
		//print_r($productarray);
		
		
		
		// check seller have commission
		$select = $connection->select()
				  ->from('marketplace_saleperpartner') 
				  ->where('seller_id = ?', $seller_id);
		
		$result = $connection->fetchAll($select);
		
		if(empty($result))
		{
			$seller_new_amount='0.0000';	
		}
		else 
		{
			$seller_new_amount=$result[0]['commission_rate'];	
						
		}  
		
		/*
		if($seller_new_amount!='0.0000')
		{ 
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$seller_new_amount;
			$taxarray=json_encode($taxarray);
		}
		

		if($category_total_comm!=0)
		{
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$category_total_comm;
			$taxarray=json_encode($taxarray);  		
		}	
		*/
		
		if($single_cat_comm!=0) // Check category have comm
		{
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$single_cat_comm;
			$taxarray=json_encode($taxarray);  	
		}
		else if($category_total_comm!=0) // Check category per seller have comm
		{
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$category_total_comm;
			$taxarray=json_encode($taxarray);  			
		}
		else if($seller_new_amount!='0.0000') // Check seller have comm
		{ 
			$taxarray=json_decode($taxarray);
			$taxarray->admin_comm=$seller_new_amount;
			$taxarray=json_encode($taxarray);
		}
		
		
		echo $taxarray;
		
		   
		
		//$sql = "insert into order_tax_list (order_id,tax_list) values ('".$order_increment_id."','".$taxarray."')";
		//$connection->query($sql); 
	}
	
	
	function get_seller_product_category_comm($seller_id,$product_id)
	{
		$seller_id.' '.$product_id;	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$product = $productRepository->getById($product_id);
		$categories = $product->getCategoryIds();
		$total_comm=0;
		if(!empty($categories))
		{	
			foreach($categories as $category)
			{
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
				$catArray[]=$cat->getId();
				//echo $cat->getName(); 
			}
			
			
			
			if(!empty($catArray))
			{
				foreach($catArray as $cat_id)
				{
					$cat_id;	
					
					$comm=$common_helpers->get_seller_saved_cat_comm($seller_id,$cat_id);
					$total_comm=$total_comm+$comm; 
				}	
			}	
		
		
		}
		
		return $total_comm;
		  
	}	
	
	
	function get_seller_saved_cat_comm($seller_id,$cat_id)
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				  ->from('customer_entity_varchar') 
				  ->where('entity_id = ?', $seller_id)
				  ->where('attribute_id = ?', '203');
		
		$result = $connection->fetchAll($select);
		
		$json=json_decode($result[0]['value']);
		$comm=0;
		foreach($json as $key => $value)
		{
			//echo $key.'-'.$value;
			if($cat_id==$key)
			{
				if($value!='')
				{
					$comm=$value;	
				}
				else
				{
					$comm=0;
				}
			}
		}	 
		 
		return $comm; 
		
		
	}	
	
	function get_category_comm($seller_id,$product_id)
	{
		$seller_id.' '.$product_id;	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$product = $productRepository->getById($product_id);
		$categories = $product->getCategoryIds();
		$total_comm=0;
		if(!empty($categories))
		{	
			foreach($categories as $category)
			{
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
				$catArray[]=$cat->getId();
				//echo $cat->getName(); 
			}
			
			if(!empty($catArray))
			{
				foreach($catArray as $cat_id)
				{
					$cat_comm=$common_helpers->get_single_cat_comm($cat_id);
					$total_comm=$total_comm+$cat_comm;
				}	
			}	
		}

		return $total_comm;

		
	}


	function get_single_cat_comm($cat_id)
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$comm=0;
		$select = $connection->select()
				  ->from('catalog_category_entity_varchar') 
				  ->where('entity_id = ?', $cat_id)
				  ->where('attribute_id = ?', '202');
		
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			$comm=$result[0]['value'];	
		}	
			
		return $comm; 
		
		
	}	

	
	
	function get_product_categories_comission($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		$categories=''; 
		$categories = $product->getCategoryIds();
		$parentArray=array();
		
		$final_comm=0;
		
		
		
	}	
	   
	function get_multiple_product_logic($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		$categories=''; 
		$categories = $product->getCategoryIds();
		$parentArray=array();
		$comm_array=array();
		$final_array=array();

		if(!empty($categories))
		{
			$count_category=count($categories);	
			if($count_category > 1)
			{
				//echo "Multiple  Categories";
				//echo "<br>";
				foreach($categories as $category_id)
				{
					$comm_array=$common_helpers->get_category_comm_multiple_logic($category_id);
				}
				
				foreach($comm_array as $key=>$val)
				{
					if($val==0)
					{
						unset($comm_array[$key]);
					}		
				}	
				
				$final_array = array_values($comm_array);
				return min($final_array);
				
				//print_r($final_array);
				
			}
			else
			{
				//echo "Single Category";  
				//echo "<br>";
				foreach($categories as $category_id)
				{
					return $common_helpers->get_category_comm_single_logic($category_id);
				}
			}
		}
		else
		{
			return 0;
		}
		
	}	
	
	
	function get_single_product_logic($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		$categories=''; 
		$categories = $product->getCategoryIds();
		$parentArray=array();
		$comm_array=array();
		$final_array=array(); 
 
		if(!empty($categories))
		{
			$count_category=count($categories);	
			if($count_category > 1)
			{
				//echo "Multiple  Categories";
				//echo "<br>";
				foreach($categories as $category_id)
				{
					$comm_array=$common_helpers->get_category_comm_multiple_logic($category_id);
				}
				
				foreach($comm_array as $key=>$val)
				{
					if($val==0)
					{
						unset($comm_array[$key]);
					}		
				}	
				
				$final_array = array_values($comm_array);
				return min($final_array); // assign minimum comm to admin
				 
				//print_r($final_array); 
				
			}
			else
			{ 
				//echo "Single Category";
				//echo "<br>";
				foreach($categories as $category_id)
				{
					return $common_helpers->get_category_comm_single_logic($category_id);
				}
			}
		}
		else  
		{
			return 0;
		}
		
	}
	
	
	function get_category_comm_single_logic($category_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		/*
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		$categories=''; 
		$categories = $product->getCategoryIds();
		*/
		$parentArray=array();
		
		//echo $category_id;	
		//echo "<br>";
		
		$curr_comm=$common_helpers->get_single_cat_comm($category_id);
		$final_comm=$curr_comm; 
		$curr_comm;
		//echo "<br>";   
		
		if($curr_comm==0) 
		{
			$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
					
			$parentCategories = $cat->getParentCategories();
			$childrenCategories = $cat->getChildrenCategories();
			
			$parentIdArray = array();
			
			if(!empty($parentCategories))
			{
				foreach($parentCategories as $parent)
				{
					$parentArray=$parent->getData();
					if( $parentArray && array_key_exists('is_active',$parentArray) )
					{
						if($parentArray['is_active']==1)
						{	
							if($parentArray['entity_id']!=$category_id)
							{
								/* echo "<pre>";
									print_r($parentArray);
								echo "</pre>";  */
								
								$parentIdArray[]=array(
									'id'=>$parentArray['entity_id'],
									'name'=>$parentArray['name'],
									'level'=>$parentArray['level'],
								);
								
							} 
						}	
					}
				}	  
			}	

			if(!empty($parentIdArray))
			{
				
				$parentIdArrays = array_column($parentIdArray, 'level');
				array_multisort($parentIdArrays, SORT_DESC, $parentIdArray);
				
				/* echo "<pre>";
					print_r($parentIdArray);
				echo "</pre>";  */
				
				
				foreach($parentIdArray as $row)
				{
					$cat_comm=$common_helpers->get_single_cat_comm($row['id']);
					//echo "<br>"; 
					if($cat_comm!=0)  
					{ 
						$final_comm=$cat_comm;	
						break;
					}	
				}
			}	
		}	
		
		return $final_comm;
	}	
	
	function get_category_comm_multiple_logic($category_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		/*
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		$categories=''; 
		$categories = $product->getCategoryIds();
		*/
		$parentArray=array();
		$category_comm_array=array();
		
		//echo $category_id;	
		//echo "<br>"; 
		 
		$curr_comm=$common_helpers->get_single_cat_comm($category_id);
		//$final_comm=$curr_comm; 
		
		$category_comm_array[]=$curr_comm;
		if($curr_comm==0) 
		{
			$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
					
			$parentCategories = $cat->getParentCategories();
			$childrenCategories = $cat->getChildrenCategories();
			
			if(!empty($parentCategories))
			{
				foreach($parentCategories as $parent)
				{
					$parentArray=$parent->getData();
					if($parentArray['is_active']==1)
					{	
						if($parentArray['entity_id']!=$category_id)
						{
							/* echo "<pre>";
							print_r($parentArray);
							echo "</pre>"; */
							
							$parentIdArray[]=array(
								'id'=>$parentArray['entity_id'],
								'name'=>$parentArray['name'],
								'level'=>$parentArray['level'],
							);
							
						} 
					}	
				}	 
			}	

			if(!empty($parentIdArray))
			{
				
				$parentIdArrays = array_column($parentIdArray, 'level');
				array_multisort($parentIdArrays, SORT_DESC, $parentIdArray);
				
				/* echo "<pre>";
					print_r($parentIdArray);
				echo "</pre>";  */
				
				
				foreach($parentIdArray as $row)
				{
					$cat_comm=$common_helpers->get_single_cat_comm($row['id']);
					//echo "<br>"; 
					if($cat_comm!=0)
					{
						$final_comm=$cat_comm;	
						$category_comm_array[]=$final_comm;
					}	
				}
			}	
		}
		
		return $category_comm_array;
			
	}	
	   
	   
	   
}
