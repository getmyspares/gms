<?php

namespace Panasonic\CustomUser\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;
use Razorpay\Api\Api;


$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');  
$path=$api_helpers->get_path();	  

require_once $path.'/app/code/Razorpay/razorpay/Razorpay.php';

//require_once '/data/html/app/code/Razorpay/razorpay/Razorpay.php';   

require_once $path.'/sms/vendor/autoload.php';   
 
use Twilio\Rest\Client;  
 
class Customfunction extends AbstractHelper
{ 
       
	public function product_pi_model($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		
		$select = $connection->select()
                  ->from('catalog_product_entity_varchar') 
                  ->where('attribute_id = ?', '199')
                  ->where('entity_id = ?', $product_id);
        $result = $connection->fetchAll($select);
  		
		
		if(!empty($result))
		{
			return $result[0]['value']; 
		} 
	}

	public function product_model($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		 
		
		$select = $connection->select()
                  ->from('catalog_product_entity_int') 
                  ->where('attribute_id = ?', '167')
                  ->where('entity_id = ?', $product_id);
        $result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return $result[0]['value'];  
		}
		else
		{
			return 0;
		}
	}
	
	
	public function model_already_exist($model)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		$select = $connection->select()
                  ->from('eav_attribute_option_value') 
                  ->where('value = ?', $model);
        $result = $connection->fetchAll($select);
		
		 
		if(!empty($result))
		{
			return 1;  
		}
		else
		{
			return 0;
		}
	}

	public function getExchangeAwb($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = $connection->select()
							->from('ecomexpress_awb_exchange',['awb']) 
							->where('orderid = :orderid');
		$awb = $connection->fetchOne($select,['orderid'=>$order_id]);
		return !empty($awb) ? $awb:null;
	}
	
	
	public function razorpay_expense($magento_order_id)
	{
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		 
		
		
		
		$select = $connection->select()
                  ->from('custom_order_razorpay') 
                  ->where('order_id = ?', $magento_order_id);
        $results = $connection->fetchAll($select);
		
		$amount=0;
		if(!empty($results))
		{
			$razorpay=$results[0]['razorpay'];
			if($razorpay!='')
			{	
				$response=$results[0]['response_details'];
				$result=json_decode($response);
				$fee=$result->fee;
				$tax=$result->tax;
				
				$amount=$fee+$tax;
				
				return $amount;
			}
			else 
			{
				return $amount;
			}
			
		}
		else
		{
			return 'COD';
		}
		
		 
	}	

	
	public function custom_noddle_amount($orderid)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($orderid);
		
		/* echo "<pre>";
			print_r($order->getData());
		echo "</pre>"; */
		$orderArray=$order->getData();
		
		 
		
		//$order_amount=$order->getPayment()->getAmountOrdered();
		$order_amount=$orderArray['grand_total'];
		$noddle_amount=$order_amount * 0.01;
		 
		return $noddle_amount;
	} 
	
	public function custom_admin_commission($orderid)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		
		
		$select = $connection->select()
                  ->from('marketplace_saleslist') 
                  ->where('magerealorder_id = ?', $orderid);
        $results = $connection->fetchAll($select);
		
		
		$total_commission=0;
		if(!empty($results))
		{
			foreach($results as $row)
			{
				$total_commission=$total_commission + $row['total_commission']; 	
			}	
		}  


		return $total_commission;	
		
	}
	
	
	public function custom_gst_tax($orderid,$product_id)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		
		$select = $connection->select()
                  ->from('marketplace_saleslist') 
                  ->where('magerealorder_id = ?', $orderid)
                  ->where('order_item_id = ?', $product_id);
        $results = $connection->fetchAll($select);	
		
		$total_commission=0;
		if(!empty($results))
		{
			foreach($results as $row)
			{
				$total_commission=$total_commission + $row['total_tax']; 	
			}	 
		}   


		return $total_commission;	
		
		
	 
	
	}	
	
	public function custom_gst_tax_expense($orderid)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		
		
		$select = $connection->select()
                  ->from('marketplace_saleslist') 
                  ->where('magerealorder_id = ?', $orderid);
        $results = $connection->fetchAll($select);	

		
		$total_commission=0;
		if(!empty($results))
		{
			foreach($results as $row)
			{
				$total_commission=$total_commission + $row['total_tax']; 	
			}	 
		}   


		return $total_commission;	
		
		
	 
	
	}	
	
	
	public function custom_seller_comission($orderid)
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		
		$select = $connection->select()
                  ->from('marketplace_saleslist') 
                  ->where('magerealorder_id = ?', $orderid);
        $results = $connection->fetchAll($select);	
		
		
		$total_commission=0;
		if(!empty($results))
		{
			foreach($results as $row)
			{
				$total_commission=$total_commission + $row['actual_seller_amount']; 	
			}	
		}
		


		return $total_commission;	
	 
	
	}	
	
	public function order_assigned_awb($order_inc_id,$product_id) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		
		$order_id=$order->getId();
		
		
		
		$sql = "Select * FROM rev_awb where order_id='0' and status='0' and product_id!='".$product_id."' order by id asc limit 0,1";
		$results = $connection->fetchAll($sql);
		
		
		
		
		if(!empty($results))
		{
			return $results[0]['awb'];	
		}
		else
		{
			return $awb;
		} 
		 
		 
	}	
	
	
	public function order_customer_array($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		$customername=$order->getCustomerName();
		$shippingaddress=$order->getShippingAddress()->getStreet();
		
		$street=$this->get_customer_order_adress($order_id,'street','shipping'); 
		$city=$this->get_customer_order_adress($order_id,'city','shipping'); 
		$region=$this->get_customer_order_adress($order_id,'region','shipping'); 
		$postcode=$this->get_customer_order_adress($order_id,'postcode','shipping'); 
		$telephone=$this->get_customer_order_adress($order_id,'telephone','shipping'); 
		
		if($telephone=='')
		{
			$telephone='11111111111';	
		}	

		/* "REVPICKUP_NAME": "Pia Bhaa",
		"REVPICKUP_ADDRESS1": "Test Address 1",
		"REVPICKUP_ADDRESS2": "Change Address 2",
		"REVPICKUP_ADDRESS3": "Change Address 3",
		"REVPICKUP_CITY": "Delhi",
		"REVPICKUP_PINCODE": "111111",
		"REVPICKUP_STATE": "DL",
		"REVPICKUP_MOBILE": "1111111111",
		"REVPICKUP_TELEPHONE": "012-3456789", */
		
		
		$street_array=explode(PHP_EOL,$street);
		
		
		$street_1='';
		$street_2='';
		$street_3='';
		
		if(!empty($street_array[0]))
		{
			$street_1=$street_array[0];	
		}
		if(!empty($street_array[1]))
		{
			$street_2=$street_array[1];	
		}
		if(!empty($street_array[2]))
		{
			$street_3=$street_array[2];	
		}	
		
		
		
		$customerarray=array(
			'REVPICKUP_NAME'=>$customername,
			'REVPICKUP_CITY'=>$city, 
			'REVPICKUP_PINCODE'=>$postcode, 
			'REVPICKUP_STATE'=>$region, 
			'REVPICKUP_MOBILE'=>$telephone, 
			'REVPICKUP_TELEPHONE'=>$telephone, 
		);
		 
		
		
		
		$customeraddressarray['REVPICKUP_ADDRESS1']=$street_1;
		$customeraddressarray['REVPICKUP_ADDRESS2']=$street_2;
		$customeraddressarray['REVPICKUP_ADDRESS3']=$street_3;
		
		  
		$resultarray = array_merge($customerarray, $customeraddressarray);
		
		return $resultarray;
		
		
	}

	public function get_customer_order_adress($order_id,$field,$address_type)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$sql = "Select ".$field." FROM sales_order_address where parent_id='".$order_id."' and address_type='".$address_type."'";
		$results = $connection->fetchAll($sql);
		if(!empty($results)) {
				return $results[0][$field];
		}   
		  
		
	}
	
	
	public function get_product_details_array($order_inc_id,$product_idds,$qty)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		$product_name='';
		$total_product_amount=0;
		$total_product_weight=0;
		
		$total_product_length=0;
		$total_product_breath=0;
		$total_product_height=0;
		
		
		$i=0;
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			if($product_id==$product_idds) 
			{	
				$product_name .=$item->getName().','; 
				$product_amount=$item->getPrice();
				$product_weight=$item->getWeight();
				
				$product_length=$this->get_product_length($product_id);
				$product_breath=$this->get_product_breath($product_id);
				$product_height=$this->get_product_height($product_id); 
				
				$total_product_amount=$total_product_amount+$product_amount;
				$total_product_weight=$total_product_weight+$product_weight;
				
				$total_product_length=$total_product_length+$product_length;
				$total_product_breath=$total_product_breath+$product_breath;
				$total_product_height=$total_product_height+$product_height;
			}
			
			
		$i++; 
		}
		 
		  
		$vol_weight=($total_product_length+$total_product_breath+$total_product_height)/5000;
		
		
		$productName=substr($product_name, 0, -1);
		
		//$piece=$this->count_order_item($order_inc_id)
		
		$array=array( 
			'ITEM_DESCRIPTION'=>$productName,
			'PIECES'=>$qty, 
			'COLLECTABLE_VALUE'=> $total_product_amount,
			'DECLARED_VALUE'=>$total_product_amount,
			'ACTUAL_WEIGHT'=>$total_product_weight,
			'VOLUMETRIC_WEIGHT'=>$vol_weight,
			'LENGTH'=>$total_product_length,
			'BREADTH'=>$total_product_breath,
			'HEIGHT'=>$total_product_height
		);    
		
		return $array;
		
		
	}
	
	
	public function get_product_length($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('catalog_product_entity_decimal') 
                  ->where('entity_id = ?', $product_id)
                  ->where('attribute_id = ?', '205');
                 
		$results = $connection->fetchAll($select);
		 
		 
		if(!empty($results))
		{
			return $results[0]['value'];
		}
		else
		{
			return 0;
		}
	}
	
	public function get_product_breath($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_decimal') 
                  ->where('entity_id = ?', $product_id)
                  ->where('attribute_id = ?', '206');
                 
		$results = $connection->fetchAll($select);
		
		
		if(!empty($results))
		{
			return $results[0]['value'];
		}
		else
		{
			return 0;
		}
	}
	
	public function get_product_height($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_decimal') 
                  ->where('entity_id = ?', $product_id)
                  ->where('attribute_id = ?', '207');
                 
		$results = $connection->fetchAll($select);
		
		
		if(!empty($results))
		{
			return $results[0]['value'];
		}
		else
		{
			return 0; 
		}
	}
	
	public function get_seller_details_array($order_inc_id,$product_idss)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId(); 
			if($product_idss==$product_id)
			{	
				$product_idd=$item->getId(); 
				//$seller=$this->get_product_seller($order_id,$product_id);
				$sellerarray[]=$this->get_product_seller($order_id,$product_idd);
			} 
		}  	
		
		
		$resultarray=array_values(array_unique($sellerarray));
		
		
		
		$count=count($resultarray);
		if($count==1)
		{
			foreach($resultarray as $row)
			{
				$sellerarray=$this->get_rev_seller_array($row,$order_inc_id,$product_idss);
			}
		} 	
		else
		{
			$sellerarray=array();
		}
		 
		
		return $sellerarray; 
		
		
	}	
	
	
	public function get_rev_seller_array($seller_id,$order_inc_id,$product_idss)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		 
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId(); 
	
		foreach ($order->getAllItems() as $item)
		{
			
			$product_id=$item->getProductId();
			if($product_id==$product_idss)
			{
				/* echo "<pre>";
					print_r($item->getData());
				echo "</pre>"; */
				
				$itemArray=$item->getData();
				
				$discount_amount=$item->getDiscountAmount();
				$total_amount=$itemArray['row_total_incl_tax'];
				$cgst_amount=$itemArray['cgst_amount'];
				$sgst_amount=$itemArray['sgst_amount'];
				$igst_amount=$itemArray['igst_amount'];
				
				$cgst_percent=$itemArray['cgst_percent'];
				$sgst_percent=$itemArray['sgst_percent'];
				$igst_percent=$itemArray['igst_percent'];
				

				$total_gst_tax=$cgst_amount+$sgst_amount+$igst_amount;
				
				
			}	
		} 
	
	
	
		$customer = $objectManager->get(
			'Magento\Customer\Model\Customer'
		)->load($seller_id);
		if ($customer) {
								
			$select = $connection->select()
                  ->from('seller_postcode') 
                  ->where('seller_id = ?', $seller_id);
                 
		
			$result = $connection->fetchAll($select);
			$zipcode='122002';
			if(!empty($result))
			{	
				$zipcode=$result[0]['postcode']; 
			}
			
			
			$returnArray = [];
			$returnArray['seller_name'] = $customer->getName();
			$returnArray['seller_gst'] = $customer->getGstNumber();
			$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
			$returnArray['seller_comp_address'] = $customer->getcompanyAdd();
			$returnArray['seller_comp_mobile'] = $customer->getmobile();
			$returnArray['id'] = $customer->getId();
			$returnArray['zipcode'] = $zipcode;
						
		}
		
		$seller_id=null; 
		
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$invoice_id=$helpers->get_order_invoice_id($order_inc_id);	
		
		$cr_date=$order->getCreatedAt();
		$cr_date=date("d-M-Y", strtotime($cr_date));
		
		 
		
		
		
		$gst_total=$total_gst_tax; 
		
		
		
		$array=array(
			'VENDOR_ID'=>$seller_id,	
			'DROP_NAME'=>$returnArray['seller_name'],	
			'DROP_ADDRESS_LINE1'=>$returnArray['seller_comp_nam'],	 
			'DROP_ADDRESS_LINE2'=>$returnArray['seller_comp_address'], 	 
			'DROP_PINCODE'=>$returnArray['zipcode'],	  
			'DROP_MOBILE'=>$returnArray['seller_comp_mobile'],	
			'DROP_PHONE'=>$returnArray['seller_comp_mobile'],	
			'EXTRA_INFORMATION'=>'Test Info',	
			'DG_SHIPMENT'=>'False',
			'ADDITIONAL_INFORMATION'=>array(
				'SELLER_TIN'=>null,
				'INVOICE_NUMBER'=>$invoice_id, 
				'INVOICE_DATE'=>$cr_date,
				"ESUGAM_NUMBER"=> null,
				"ITEM_CATEGORY"=> null,
				"PACKING_TYPE"=> null,
				"PICKUP_TYPE"=> null,
				"RETURN_TYPE"=> null, 
				"PICKUP_LOCATION_CODE"=> null,
				"SELLER_GSTIN"=>$returnArray['seller_gst'],
				"GST_HSN"=> null,
				"GST_ERN"=> null,
				"GST_TAX_NAME"=> null,
				"GST_TAX_BASE"=> null,
				"GST_TAX_RATE_CGSTN"=> $cgst_percent,
				"GST_TAX_RATE_SGSTN"=> $sgst_percent,
				"GST_TAX_RATE_IGSTN"=> $igst_percent, 
				"GST_TAX_TOTAL"=> $gst_total, 
				"GST_TAX_CGSTN"=> $cgst_amount,
				"GST_TAX_SGSTN"=> $cgst_amount, 
				"GST_TAX_IGSTN"=> $cgst_amount,
				"DISCOUNT"=>$discount_amount  
			) 
		);   
		
		return $array; 
			 
			
	}
	
	public function get_product_seller($order_id,$product_id)
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$product_idd=$product_id; 
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
		if(!empty($order->getAllItems()))
		{	
			foreach ($order->getAllItems() as $item)
			{
				$item_id=$item->getId();
				
				if($item_id==$product_idd)
				{
					$product_id=$item->getproductId();			
				}	
			}  	 
		}
		
		
		$seller_id=0;

		/* swapped for seller priority changes */
		$select = $connection->select()
				  ->from('marketplace_orders') 
				  ->where('order_id = ?', $order_id);
		
		
		/* $select = $connection->select()
				  ->from('marketplace_saleslist') 
				  ->where('order_id = ?', $order_id)
				  ->where('order_item_id = ?', $product_id); */
				 
		$results = $connection->fetchAll($select);
		
		if(!empty($results))
		{
			$seller_id=$results[0]['seller_id'];
			return $seller_id;
		}
		else
		{
			return '29';
		}   
		
		
		
	}


	public function rev_ecom_api($order_id,$product_id,$qty,$rma_id,$item_id) 
	{
		try {  
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
			$site_url=$storeManager->getStore()->getBaseUrl();
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection(); 
			
			
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_id); 
			 
			
			$product_ids=$product_id;          
			
			
			
			$order_awb=$custom_helpers->order_assigned_awb($order_id,$product_ids);
			if($order_awb!=0) 
			{
				$mainarray=array(
					'AWB_NUMBER'=>$order_awb,	
					'ORDER_NUMBER'=>$order_id,	
					'PRODUCT'=>'REV' 	
				);	 
				
				
				
				$order_customer=$custom_helpers->order_customer_array($order_id);
				/* echo "<pre>"; 
					print_r($order_customer);
				echo "</pre>";  */
				
				$product_array=$custom_helpers->get_product_details_array($order_id,$product_ids,$qty); 
				
				/* echo "<pre>"; 
					print_r($product_array);
				echo "</pre>";  */
				 
				
				
				
				$seller_array=$custom_helpers->get_seller_details_array($order_id,$product_ids); 
				
				/* echo "<pre>"; 
					print_r($seller_array);
				echo "</pre>";  */
				
				
				
				$mainarray = array_merge($mainarray,$order_customer,$product_array,$seller_array);
			
				/* echo "<pre>"; 
					print_r($mainarray);
				echo "</pre>"; */
				
				
				$apiarray=array('SHIPMENT'=>$mainarray);
				$apiarray=array('ECOMEXPRESS-OBJECTS'=>$apiarray);
				
				
				//echo json_encode($apiarray);  
				
				$modearray=json_decode($api_helpers->get_razorpay_mode());
				//print_r($modearray);  
				$mode=$modearray->mode;
				$username=$modearray->username;
				$password=$modearray->password;
				$ecom_url=$modearray->url;	
				
				
				
				$url=$ecom_url.'apiv2/manifest_awb_rev_v2/'; 
					
				$params = array(
					'username' => $username,
					'password' => $password, 
					'json_input' => json_encode($apiarray)
				);  
	              
				$fields_string = http_build_query($params);

				//open connection
				$ch = curl_init(); 

				//set the url, number of POST vars, POST data
				curl_setopt($ch,CURLOPT_URL, $url);
				curl_setopt($ch,CURLOPT_POST, 1);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$output = curl_exec($ch); 
				//print_r($output);
				curl_close($ch);
				
				$result=json_decode($output);
				
				/* echo "<pre>";
					print_r($result);
				echo "</pre>";    */
				
				

				
				$today=date('Y-m-d');
			
				$sql = "update rev_awb set order_id='".$order_id."',product_id='".$product_ids."',status='Assigned',update_date='".$today."' where awb='".$order_awb."'"; 
				$connection->query($sql); 
				
				
				$sql="update wk_rma_items set manifest='1' where rma_id='".$rma_id."' and item_id='".$item_id."'";
				$connection->query($sql);  
				    
				
				
				
			
			}   
				 
		}
		catch(Exception $e) {
		  echo "Api Not working for Product ID  - ".$product_id;
		}   
	}	 

	
	public function count_order_sellers($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			
			$seller=$this->get_product_seller($order_id,$product_id);
			$sellerarray[]=$this->get_product_seller($order_id,$product_id);
			
		}  	
		
		
		$resultarray=array_values(array_unique($sellerarray));
		
		/* echo "<pre>";
			print_r($resultarray);
		echo "</pre>"; */
		
		$count=count($resultarray);
		
		
		return $count;
		
	 
	}
	
	public function get_multiple_seller_details_array($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			
			$seller=$this->get_product_seller($order_id,$product_id);
			$sellerarray[]=$this->get_product_seller($order_id,$product_id);
			
		}  	
		
		
		$resultarray=array_values(array_unique($sellerarray));
		
		foreach($resultarray as $row)
		{
			$seller_id=$row;
			$product_id=get_product_by_seller($row,$order_inc_id);	 	
		} 	
	}
	
	
	
	
	
	
	public function update_razor_pay_resonse($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		 
		
		$select = $connection->select()
                  ->from('custom_order_razorpay') 
                  ->where('order_id = ?', $order_id);
                 
		$results = $connection->fetchAll($select);
		
		
		if(!empty($results))
		{
			$razor=$results[0]['razorpay'];
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$site_url=$storeManager->getStore()->getBaseUrl();
			
			$amount=0; 
			if($razor!='')  
			{
				$razorpay_id=$razor;	
				$url=$site_url.'qbonline/connection/razorpaytax/order/'.$razorpay_id;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
				//curl_setopt($ch, CURLOPT_POST, true); 
				//curl_setopt($ch, CURLOPT_POSTFIELDS,$parse);  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				$response  = curl_exec($ch); 
				
				//$result=json_decode($response);
				
				$sql = "update custom_order_razorpay set response_details='".$response."' where order_id='".$order_id."'";
				$connection->query($sql);	 
				//echo "<br>";  
			}  
			else
			{
				return 'COD';
			}  
			
		}
		else 
		{
			return 'COD';
		}
		   
		
	}
	
	
	public function update_product_model($product_ids)
	{
		$product_id=$product_ids;
					
		$pi_model=$this->product_pi_model($product_id); 
		$model=$this->product_model($product_id); 
		  
		  
		//$pi_model='Testing Model';  
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$pi_model;
		
		$check_model=$this->model_already_exist($pi_model);
		
		 
		if($model==0)  
		{   
			if($pi_model!='')
			{
				//echo $pi_model;   		
				
				$sql = "Select * FROM eav_attribute_option order by sort_order desc limit 0,1";
				$result = $connection->fetchAll($sql);
				
				
				
				$sort_order=$result[0]['sort_order'];
				$sort_order=$sort_order+1;	

				if($check_model==0)			
				{
					
					$sql = "Insert Into  eav_attribute_option (attribute_id, sort_order) Values ('167','".$sort_order."')";
					$connection->query($sql);
					$insert_id=$connection->lastInsertId();

					$sqls = "Insert Into  eav_attribute_option_value (option_id,store_id, value) Values ('".$insert_id."','0','".$pi_model."')";
					$connection->query($sqls);
					
				}
				else
				{ 
					
					$sql = "Select * FROM eav_attribute_option_value where value='".$pi_model."'";
					$result = $connection->fetchAll($sql);
					$insert_id=$result[0]['option_id']; 
				}
				 
				
				
				$sqls = "Delete from catalog_product_entity_int where attribute_id='167' and entity_id='".$product_id."'";
				$connection->query($sqls); 
				
				
				$sqls = "Insert Into  catalog_product_entity_int (attribute_id,store_id,entity_id,value) Values ('167','0','".$product_id."','".$insert_id."')";
				$connection->query($sqls);
			} 	 
			
		 
		}
			
	}	
	
	
	public function check_product_discount($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_int') 
                  ->where('attribute_id = ?', '231')
                  ->where('entity_id = ?', $product_id);
                 
		
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result)) 
		{
			return 1;  
		}
		else
		{
			return 0;
		} 
		
		
	}

	
	public function assign_product_discount($product_id)
	{
		//$product_id = 9;  
		$productId=$product_id;
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$currentproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
		$currentproduct->getName(); 
		$specialprice = $currentproduct->getSpecialPrice();
		$orgprice = $currentproduct->getPrice();
		
		$specialfromdate = $currentproduct->getSpecialFromDate();
		$specialtodate = $currentproduct->getSpecialToDate();
		$today = time();
		
		$level=0;
		
		if ($specialprice!='')
        {
			if ($specialprice < $orgprice) 
			{
				if(($specialfromdate!='') && ($specialtodate!='')) 
				{
					if (($today >= strtotime($specialfromdate)) && ($today <= strtotime($specialtodate)))
					{   
						
						$loss=$orgprice-$specialprice;
						$discount=($loss*100)/$orgprice;
						//$discount=($specialprice/$orgprice)*100;
						$discount=number_format($discount, 2, '.', '');
						
						if($discount >= 1 && $discount <= 10)
						{
							$level=128;	
						}
						else if($discount >= 11 && $discount <= 20)
						{
							$level=129;	
						}
						else if($discount >= 21 && $discount <= 30)
						{
							$level=130;	
						}
						else if($discount >= 31 && $discount <= 40)
						{
							$level=131;	
						}
						else if($discount >= 41 && $discount <= 50)
						{
							$level=132;	
						}	
						else if($discount >= 51 && $discount <= 60)
						{
							$level=133;	
						}
						else if($discount >= 61 && $discount <= 70)
						{
							$level=134;	
						}
						else if($discount >= 71 && $discount <= 80)
						{
							$level=135;	
						}
						else if($discount >= 81 && $discount <= 90)
						{
							$level=136;	
						}
						else
						{
							$level=138;	 
						} 
						 
						
						//echo $currentproduct->getId().' - '.$orgprice.' - '.$specialprice.' - '.$specialfromdate.' - '.$specialtodate.' - '.$discount.' - '.$level."<br>";  
					}
				}			
			}	 
		}	
 
		
		if($level!=0)
		{
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			$prev_product_discount=$this->check_product_discount($product_id); 
			if($prev_product_discount==0) 
			{
				$sqls = "Insert Into catalog_product_entity_int (attribute_id,store_id,entity_id,value) Values ('231','0','".$product_id."','".$level."')";
				$connection->query($sqls);		
			}     
		}	
		
	}	
	
	
	public function get_product_special_price($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_decimal') 
                  ->where('attribute_id = ?', '78')
                  ->where('entity_id = ?', $product_id);
                 
		
		$result = $connection->fetchAll($select);
		
		
		
		if(!empty($result)) 
		{
			return $result[0]['value'];  
		}
		else
		{
			return '';
		}  
	}	

	public function get_product_special_from_date($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_decimal') 
                  ->where('attribute_id = ?', '79')
                  ->where('entity_id = ?', $product_id);
                 
		
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result)) 
		{
			return $result[0]['value'];  
		}
		else
		{
			return '';
		}  
	}	

	public function get_product_special_to_date($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_decimal') 
                  ->where('attribute_id = ?', '80')
                  ->where('entity_id = ?', $product_id);
                 
		
		$result = $connection->fetchAll($select);
		 
		
		if(!empty($result)) 
		{
			return $result[0]['value'];  
		}
		else
		{
			return '';
		}  
	}	

	public function get_customer_shipping_address($customer_id,$field)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		//$customer_id=20; 
		$customerID = $customer_id;
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerID);

		$billingId = $customer->getDefaultShipping();
		
		
		
		$sql = "Select $field FROM customer_address_entity where  entity_id='".$billingId."'";
		$result = $connection->fetchAll($sql);
		
		if(!empty($result))
		{
			return $result[0][$field]; 
		}	
		
	} 
 

	public function shipping_address_billing_id($billingId,$field)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$sql = "Select $field FROM customer_address_entity where  entity_id='".$billingId."'";
		$result = $connection->fetchAll($sql);
		if(!empty($result))
		{
			return $result[0][$field]; 
		}	
		
	} 	
	
	
	public function check_customer_mobile($mobile)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('attribute_id = ?', '178')
                  ->where('value = ?', $mobile);
                 
		
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return 1;  
		}
		else
		{
			return 0;
		}
		
	} 


	public function get_unique_seller_order($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('marketplace_saleslist') 
                  ->where('magerealorder_id = ?', $order_inc_id);
                 
		
		$result = $connection->fetchAll($select);
		
		
		
		foreach($result as $row)
		{
			$sellerarray[]=$row['seller_id'];	
		}

		$final=array_values(array_unique($sellerarray));
		return $final; 
	} 
	
	public function get_seller_details($seller_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$customer = $objectManager->get(
			'Magento\Customer\Model\Customer'
		)->load($seller_id);
		if ($customer) {
								
			$select = $connection->select()
                  ->from('seller_postcode') 
                  ->where('seller_id = ?', $seller_id);
                 
		
			$result = $connection->fetchAll($select);
			$zipcode='122002';
			if(!empty($result))
			{	
				$zipcode=$result[0]['postcode']; 
			}
			$returnArray = [];
			$returnArray['seller_name'] = $customer->getName();
			$returnArray['seller_gst'] = $customer->getGstNumber();
			$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
			$returnArray['seller_comp_address'] = $customer->getcompanyAdd();
			$returnArray['seller_comp_mobile'] = $customer->getmobile();
			$returnArray['seller_comp_pan'] = $customer->getPanNumber(); 
			$returnArray['id'] = $customer->getId(); 
			$returnArray['zipcode'] = $zipcode; 
			$returnArray['state'] = $customer->getResource()->getAttribute('state')->getSource()->getOptionText($customer->getData('state'));

			$cin_number='';
			$sql = "Select * FROM customer_entity_varchar where entity_id='".$seller_id."' and attribute_id='264'";
			$result = $connection->fetchAll($sql);  
			$count=count($result);
			if($count!=0)
			{
				$cin_number=$result[0]['value'];
			}
			
			$returnArray['cin_number']=$cin_number;
			
		
		
		}		
		
		return $returnArray;
	
	}	
	
	
	public function get_order_awb($order_inc_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id);
		$order_id = $order->getId(); 
		
		
		
		$select = $connection->select()
                  ->from('ecomexpress_awb') 
                  ->where('orderid = ?', $order_id);
                 
		
		$result = $connection->fetchAll($select);
		
		return $result[0]['awb'];
   
		
	} 
	
	
	public function get_postcode_valid($pincode)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('pincode_details') 
                  ->where('pincode = ?', $pincode);  
                 
		
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			return 1;		
		}
		else
		{
			return 0;
		}
		
	} 
	
	
	public function assigned_product_tax($product_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$collection = $productCollection->addAttributeToSelect('*')
					->addAttributeToFilter('status', array('eq' => 1))
					->load();
		
		$taxValue=0;
		
		$select = $connection->select()
                  ->from('catalog_product_entity_varchar') 
                  ->where('attribute_id = ?', '254')
                  ->where('entity_id = ?', $product_id);
                 
		
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			$taxValue=str_replace('%','',$result[0]['value']);	
		}  	 
		
		if($taxValue > 0) {
			$sql = "Delete FROM `catalog_product_entity_decimal` WHERE `entity_id`='".$product_id."' and attribute_id='233'";
			$connection->query($sql); 
			
			$sql = "Delete FROM `catalog_product_entity_varchar` WHERE `entity_id`='".$product_id."' and attribute_id='232'";
			$connection->query($sql); 
		
		
			$sql = "Delete FROM `catalog_product_entity_varchar` WHERE `entity_id`='".$product_id."' and attribute_id='234'";
			$connection->query($sql); 
		
		
			$sql = "INSERT INTO catalog_product_entity_varchar(attribute_id, store_id, entity_id, value) VALUES ('232','0','".$product_id."','".$taxValue."')";
			$connection->query($sql); 
		
			$sql = "INSERT INTO catalog_product_entity_varchar(attribute_id, store_id, entity_id, value) VALUES ('234','0','".$product_id."','".$taxValue."')";
			$connection->query($sql); 
			
			 
			$sql = "INSERT INTO catalog_product_entity_decimal(attribute_id, store_id, entity_id, value) VALUES ('233','0','".$product_id."','1')";
			$connection->query($sql);  
		}  	    
			      
		  
		
	} 
	
	
	public function assigned_product_hsn($product_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$collection = $productCollection->addAttributeToSelect('*')
					->addAttributeToFilter('status', array('eq' => 1))
					->load();
		
		
		
		$select = $connection->select()
                  ->from('catalog_product_entity_varchar') 
                  ->where('attribute_id = ?', '227')
                  ->where('entity_id = ?', $product_id);
                 
		
		$result = $connection->fetchAll($select);
		
		
		if(!empty($result))
		{
			$act_hsn=$result[0]['value'];	
			
			$sql = "INSERT INTO catalog_product_entity_varchar(attribute_id, store_id, entity_id, value) VALUES ('235','0','".$product_id."','".$act_hsn."')";
			$connection->query($sql); 
		}	
		 
		 
	} 
	
	
	
	
	function get_url_type($url) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $site_url=$storeManager->getStore()->getBaseUrl();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		$path=str_replace('https://getmyspares.com/','',$url);	 
		$path=str_replace('https://getmyspares.in/','',$url);	
		$path=str_replace('staging/','',$path);	 
		$path=str_replace('http://demo16.mobdigi.com/','',$path);
		
		$select = $connection->select()
                ->from('url_rewrite')
                ->where('request_path = ?', $path);
                 

        $result = $connection->fetchAll($select);
		
		/* echo "<pre>";
		print_r($result);
		echo "</pre>";
		die; */
		if(!empty($result))
		{
			$array=array('success'=>'1','entity_type'=>$result[0]['entity_type'],'entity_id'=>$result[0]['entity_id'],'url_rewrite_id'=>$result[0]['entity_id']);
		}
		else  
		{
			$array=array('success'=>'1','entity_type'=>'category','entity_id'=>0,'url_rewrite_id'=>0);  
		}  
		
		return json_encode($array);
		
	
	}	
	
	
	public function get_order_txn_id($order_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $site_url=$storeManager->getStore()->getBaseUrl();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		
		
		
		$select = $connection->select()
                ->from('sales_payment_transaction')
                ->where('order_id = ?', $order_id);
		$result = $connection->fetchAll($select);		
		if(!empty($result))
		{
			return $result[0]['txn_id'];
		}
		else  
		{
			return 0;
		}   
	}

	
		
	
	public function route_payment($order_id,$seller_id) 
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $site_url=$storeManager->getStore()->getBaseUrl();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(); 
		
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		
		 
		//$order_id=$row;
		$txn_id=$custom_helpers->get_order_txn_id($order_id); 
		
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  
		$order_inc_id=$order->getIncrementId();
		$order_array=array();
		
		$order_array=$order->getData();
		
		$order_total=$order->getGrandTotal();
		$order_subtotal=$order->getSubTotal(); 
		$created_at=$order->getCreatedAt(); 
		
		$order_total_amount=$order->getsubtotalInclTax(); 
		$razor_pay=$custom_helpers->razorpay_expense($order_inc_id);
		if($razor_pay!='COD') {	
			
			
			$payment_id=$custom_helpers->get_order_payment_id($order_inc_id);
			
			$razor_amount=$razor_pay;
			
			$admin_amount='';
			$paramssss='';
			$paramsss=''; 
			
			$paramssss=$helpers->admin_commission($order_inc_id);
			
			if($paramssss!='null')
			{	
				$exparrayy=json_decode($paramssss);
				
				
				$admin_amount=$exparrayy->Line[0]->Amount;
				
			}	 
			
			
			$paramssss=$helpers->noddle_amount($order_inc_id);
			$exparrayy=json_decode($paramssss); 
			$nodal_amount=$exparrayy->Line[0]->Amount;
			
			$seller_amount=$helpers->get_seller_amount($seller_id,$order_inc_id); 
			
			$paramssss=$helpers->gst_tax($order_inc_id);
			$exparrayy=json_decode($paramssss); 
			$gst_amount=$exparrayy->Line[0]->Amount;
			
			
			$paramssss=$helpers->shipping_chanrge($order_inc_id);
			$exparrayy=json_decode($paramssss); 
			$shipping_amount=$exparrayy->Line[0]->Amount;
			
			$total_amount_xxx=$order_subtotal;
		
			
			$admin_comm=$tax_helpers->tax_admin_commission($total_amount_xxx,$order_inc_id);
			$admin_comm_tax=$tax_helpers->tax_admin_commission_tax($total_amount_xxx,$order_inc_id);
			$admin_comm_tds=$tax_helpers->tax_admin_commission_tds($total_amount_xxx,$order_inc_id);    

			$tax_rates_array=json_decode($tax_helpers->tax_order_parameter($order_inc_id));
			$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;

			$total_admin_comm=$admin_comm + $admin_comm_tax; 

			$net_admin_comm=$total_admin_comm - $admin_comm_tds;
			$net_admin_comm=number_format($net_admin_comm, 2, '.', '');
			
			
			$gst_tcs_on_total_amount=$tax_helpers->tax_gst_tcs_on_total_amount($order_total_amount,$order_inc_id); 
			
			$payment_amount=$razor_pay;
			
			//$pay_to_seller=$total_amount_xxx-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
			//$pay_to_seller=$total_amount_xxx-$total_admin_comm-$gst_tcs_on_total_amount-$payment_amount;
			
			$pay_to_seller=$total_amount_xxx-$net_admin_comm-$gst_tcs_on_total_amount; 
			
			
			$order_shipping_amount=$order->getShippingInclTax(); 
			$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		
		
			$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
			$logistic_tds=$tax_helpers->tax_logistic_tds($order_shipping_amount,$order_inc_id);
			
			$razorpay_tds=$tax_helpers->get_tax_razorpay_tds($payment_amount,$order_inc_id); 
			
			$pay_to_razorpay=$razor_pay-$razorpay_tds;	
			$pay_to_panasonic=$net_admin_comm+$logistic_tds+$gst_tcs_on_total_amount;
 
			$pay_to_shipping=$shipping_amount-$logistic_tds;
			
 
			  
			/*
			echo "subtotal=".$order_subtotal;
			echo "<br>";
			
			echo "Razor Pay=".$pay_to_razorpay;
			echo "<br>";
			echo "Admin=".$net_admin_comm;
			echo "<br>";
			echo "Nodal=".$nodal_amount;
			echo "<br>";
			echo "GST=".$gst_amount;
			echo "<br>";
			echo "Shipping=".$shipping_amount;
			echo "<br>"; 
			echo "Seller Pay=".$pay_to_seller; 
			echo "<br>"; 
			
			
			
			echo $payment_id;
			echo "<br>";
			*/
			
			
			/*
			echo "Pay to Seller=".$pay_to_seller;
			echo "<br>"; 
			echo "Pay to Admin=".$pay_to_panasonic;  
			echo "<br>"; 
			echo "Pay to Shipping=".$pay_to_shipping;  
			echo "<br>";
			echo "GST TCS=".$gst_tcs_on_total_amount;  
			echo "<br>";
			echo "Logistics TCS=".$logistic_tds;  
			echo "<br>";
			//echo "Razorpay=".$razor_pay;  
			//echo "<br>";
			//echo "Razorpay TCS=".$razorpay_tds;  
			//echo "<br>";
			//echo "Pay to Razorpay=".$pay_to_razorpay;   
			echo "<br>";
			echo "Net Panasonic comm=".$net_admin_comm;  
			*/
			
			$seller_acc_id=$this->get_seller_account_id($seller_id);
			
			if($seller_acc_id!='0') 
			{ 
				
				$this->route_payment_amount($pay_to_panasonic,$pay_to_seller,$pay_to_shipping,$payment_id,$seller_acc_id);
			}   
			        
		 	 
		}
		
		
	}	
	
	
	public function route_payment_amount($pay_to_panasonic,$pay_to_seller,$pay_to_shipping,$payment_id,$seller_acc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $site_url=$storeManager->getStore()->getBaseUrl();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(); 
		
		$keyId = 'rzp_test_eJQaxE8Xjzqjus';
		$keySecret = 'PifmETTkJhs6QDEAWoqhGbWB';
		$displayCurrency = 'INR';
		
		
		$api = new Api($keyId, $keySecret);  
		  
		
		
		$created_at=date('Y-m-d'); 
		$today=date('Y-m-d H:i:s');
		$timestamp = strtotime($today);
		$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($created_at)); 
		$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));

		$asd=strtotime($RealizationDays);
			 
		//die;	 
			 
		
		$transfer = $api->payment->fetch($payment_id)->transfer(array(
		  'transfers' => [
				
				[
					'account' => 'acc_CzkDDd4BQUnlZl', // Admin amount 
					'amount' => round($pay_to_panasonic*100),  
					'currency' => 'INR', 
					'on_hold' => true, 
					'on_hold_until' => $asd,
					'notes' => array(
						'PaymentType' => 'Admin' 
					)		
				],
				[		
					'account' => $seller_acc_id, // Seller 
					'amount' => round($pay_to_seller*100),  
					'currency' => 'INR', 
					'on_hold' => true,  
					'on_hold_until' => $asd,
					'notes' => array(
						'PaymentType' => 'Seller'  
					)			
				],
				[
					'account' => 'acc_CzkKrfJmyRjlNL', // Logistics 
					'amount' => round($pay_to_shipping*100),  
					'currency' => 'INR',  
					'on_hold' => true, 
					'on_hold_until' => $asd,
					'notes' => array(
						'PaymentType' => 'Logistics'  
					)   			
				] 
			]  
		)
		);

		$text =  json_encode($transfer->toArray());
		$obj = json_decode($text);

		     

		$itemArray=$obj->items;


		/* echo "<pre>"; 
			print_r($itemArray);
		echo "</pre>"; */

		/* echo "Transection Id=".$trans_id=$itemArray[0]->id;
		echo "<br>";
		echo "Source=".$source=$itemArray[0]->source;
		echo "<br>";
		echo "recipient=".$recipient=$itemArray[0]->recipient;
		echo "<br>";
		$amount=$itemArray[0]->amount;
		echo "Amount Transfer=".$amount=$amount/100; */

			
	}	
	
	
	
	public function get_order_payment_id($magento_order_id)
	{
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$select = $connection->select()
                  ->from('custom_order_razorpay') 
                  ->where('order_id = ?', $magento_order_id);
        $results = $connection->fetchAll($select);
		
		$amount=0;
		if(!empty($results))
		{
			return $razorpay=$results[0]['payment_id'];
		}
		
		 
	}		
	
	public function get_seller_account_id($seller_id)
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $site_url=$storeManager->getStore()->getBaseUrl();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		
		$sql="select * from marketplace_userdata where seller_id='".$seller_id."' and company_locality is not null";
		
		
		$result = $connection->fetchAll($sql);		
		
		
		if(!empty($result))
		{
			return $result[0]['company_locality'];
		}
		else   
		{
			return 0;  
		}   
		
		
					
	}	
	
	public function send_sms($phone,$pin)
	{
		$msg   = "Spare Parts - Please complete your process by entering OTP - ".$pin;
		$phone = $phone;
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
		return $smsHelper->smsCurlRequest($msg,$phone);
		
		/*try {  
			// $sid    = "ACf65431d42cfec245c5eaad3b48a085fd";
			// $token  = "3d5997316017dd03c2f6f47676d516b0";
			
			$sid    = "AC927db94863fa1840c73ba79f4633fbf3";
			$token  = "abe4d84a06369a861edd2c0c2f49206a"; 
			
			$twilio = new Client($sid, $token);

			$message = $twilio->messages
			  ->create("+91".$phone, 
				 array("from" => "+19362618055", "body" => "Spare Parts - Please complete your process by entering OTP - ".$pin)
			);     
			
			$message=1;
		}  
		catch(Exception $e) {
		  $message=0;  
		}*/	
	}	
	
	
	public function checkout_cart_summary()
    { 		
         
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		   
		 
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
        $saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
		// get quote items collection
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		 
		// get array of all items what can be display directly
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		 
		// get quote items array
		$items = $cart->getQuote()->getAllItems();
		
		$cart->getQuote()->getId(); 
		 
		 
		$total_gst=0;
		foreach ($items as $item) {
            if ($item->getId()) {
                
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
				
				$shippingGstRate = $product->getGstRate();
				
				
				$gstPercent = 100 + $shippingGstRate;
				$rowTotal = $item->getRowTotal();
                $discountAmount = $item->getDiscountAmount();
				
				
                $productPrice = ($rowTotal - $discountAmount) / $gstPercent;
                $gstAmount = $productPrice * $shippingGstRate;
				
				//$gstAmount = ((($rowTotal - $discountAmount) * $shippingGstRate) / 100);
				
				 
				$total_gst = $total_gst + $gstAmount;
				
            }
        }
		
		
		
		$subTotal = $cart->getQuote()->getSubtotal();
		$grandTotal = $cart->getQuote()->getGrandTotal(); 
		
		
		$cartarray=(array)$itemsCollection->getData();
		
		/* echo "<pre>";
			print_r($cartarray);
		echo "</pre>";    */
		
		$tax_amount=0;
        $shippingAmount=0;
        $shippingAmountt=0;
		
		if(!empty($cartarray)) 
		{
		
			$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
            
            foreach($cartarray as $row)
            {
                $tax_amount=$tax_amount + $row['tax_amount'];        
                
                
                
                
                
                if($customerSession->isLoggedIn()) {
                
                    $customer_id=$customerSession->getCustomerId();
                    
                    $pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
                    //$shippingAmount=$data_helpers->ecom_price($pincode);
                
                    $product_id=$row['product_id'];
                    $qty=$row['qty'];
                    
                    
                    $shippingAmount=$data_helpers->ecom_price_by_product($pincode,$product_id,$qty);
                    $shippingAmountt=$shippingAmountt+$shippingAmount;
                
                }
                
                
                
            }    
            
            
            $shippingAmountt;
            
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            //$customerSession = $objectManager->create('Magento\Customer\Model\Session');
            //$shippingAmount = $customerSession->getcheckoutshippingSession();
            //$shippingAmountt = $customerSession->getcheckoutshippingSession();
            
            if($customerSession->isLoggedIn())
            {
                $shippingAmount=$shippingAmountt;
            }	
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();

				//$customerSession = $objectManager->create('Magento\Customer\Model\Session');
				//$shippingAmount = $customerSession->getcheckoutshippingSession();
				//$shippingAmountt = $customerSession->getcheckoutshippingSession();
				
			$grandTotal=$subTotal+$total_gst;
			$order_total=$grandTotal + $shippingAmount;
			
			
			if($customerSession->isLoggedIn())
            {
                $shippingAmount=$shippingAmountt;
            }
			  
			 

			$shipping=number_format($shippingAmount, 2, '.', '');
			
			$base_price= $subTotal-$total_gst;
			//$base_price=bcdiv($base_price, 1,2); 
			$base_price=number_format($base_price, 2, '.', ''); 
			
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
			$couponCode = $cart->getQuote()->getCouponCode();
			$couponCode = $cart->getQuote()->getCouponCode();
	        $ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
	        $rule = $saleRule->load($ruleId);
	        $freeShippingCoupon = $rule->getSimpleFreeShipping();
			if($freeShippingCoupon){
               $shipping = 0 ;
            }
            $order_total=$subTotal + $shipping;  
			  /* <div class="text_section">        
				<i class="fa fa-exclamation" aria-hidden="true"></i> Processing charge of ₹ 50 will be applicable on COD
			</div>*/
			
			echo ' 
			<div class="cart-summary">
			<div class="price_details">
				<h2>Summary</h2>
			</div>
			
			<div class="cart-totals">
				<table>		
						<tbody>	
							<tr>
								<td>Base Price</td>
								<td> &#8377; '.$base_price.'</td>
							</tr>
							<tr>
								<td>GST</td>
								<td> &#8377; '.number_format($total_gst, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Sub Total</td>
								<td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Shipping and Handling</td>
								<td>&#8377; '.$shipping.'</td>
							</tr>	 
							<tr class="total_row"> 
								<td>Order Total</td>
								<td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
							</tr>
							<tr style="display:none;">
								<td>&nbsp;</td>
								<td class="act_cart_total">'.number_format($order_total, 2, '.', '').'</td>
							</tr>
							  
							
						</tbody>  
					</table>
				</div>
				</div>
			';	  
		} 
		else 
		{
			echo "<p>No Item In Cart</p>";
		} 
	 
	}
	
	function get_seller_dashboard_total($seller_id)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
        
        $select = $connection->select()
                    ->from('marketplace_orders')
                    ->where('seller_id = ?', $seller_id)
                    ->where('order_status = ?', 'complete');
                      
        $result = $connection->fetchAll($select);
         
        $total_payment_amount=0;            
        $totalshipping=0;            
        $total_admin_comm=0;            
        $total_seller_amount=0;        
             
        
        if(!empty($result))
        {
            
            foreach($result as $row)
            {
                $order_id=$row['order_id'];    
                $order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
                $order_inc_id=$order->getIncrementId();
                
                $amount_array=$tax_helpers->seller_order_page($order_inc_id);
                $amountSummary=json_decode($amount_array);
                
                
                /* echo "<pre>";
                print_r($amountSummary);
                echo "</pre>"; */
                
                
                $payment_amount=$amountSummary->payment_amount;
                $shipping_expense=$amountSummary->shipping_expense;
                $total_admin_comm=$amountSummary->total_admin_comm;
                
                $seller_amount=$payment_amount-($shipping_expense+$total_admin_comm);
                
                
                 
                $mainArray[]=array(
                    'payment_amount'=>$payment_amount,
                    'shipping'=>$shipping_expense,
                    'admin_comm'=>$total_admin_comm,
                    'seller_amount'=>$seller_amount
                
                );
                 
            }
            
            
            if(!empty($mainArray))
            {
                foreach($mainArray as $row)
                {
                    $payment_amount=$row['payment_amount'];            
                    $shipping=$row['shipping'];            
                    $admin_comm=$row['admin_comm'];            
                    $seller_amount=$row['seller_amount'];            
                    
                    $total_payment_amount=$total_payment_amount+$payment_amount;
                    $totalshipping=$totalshipping+$shipping;
                    $total_admin_comm=$total_admin_comm+$admin_comm;
                    $total_seller_amount=$total_seller_amount+$seller_amount;
                    
                    
                }    
            }
            
            
            
        }    
        
        $return_array=array('total_payment_amount'=>$total_payment_amount,'totalshipping'=>$totalshipping,'total_admin_comm'=>$total_admin_comm,'total_seller_amount'=>$total_seller_amount);
            
        return json_encode($return_array);        
    }
	
	
	
	function update_product() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		
		$stockStateInterface = $objectManager->get('Magento\CatalogInventory\Api\StockStateInterface');
		$stockRegistry = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
		
		
		$today = date("Y-m-d"); 
		$from = date("Y-m-d h:i:s"); 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();

		//$tax_helpers->success_page('14');	   
		  
		$temp=rand(1,3);   
		if($temp==3)   
		{	
			foreach($orderDatamodel as $orderDatamodel1){
				$orderArray=$orderDatamodel1->getData();
				$created_at=$orderArray['created_at'];
				
				$created_at=date("Y-m-d", strtotime($created_at)); 
				if($created_at==$today) 
				{	
					//print_r($orderArray);
					$order_id=$orderArray['entity_id'];  
					
					$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  
					$qty=rand(5001,8000);   
					foreach ($order->getAllItems() as $item)
					{
						$product_id=$item->getProductId();
						
						$sql = "update cataloginventory_stock_status set qty='".$qty."' where product_id='".$product_id."'";
						$connection->query($sql);	
						
						$sql = "update cataloginventory_stock_item set qty='".$qty."' where product_id='".$product_id."'";
						$connection->query($sql);	
						
					}     
					
					 
				}  
			}
		}
		
		
		
	}	
	
	
	function get_current_quote_value()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
		$couponCode = $cart->getQuote()->getCouponCode();

		$quote = $cart->getQuote();
		echo $quoteId = $quote->getId();	
	
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		// get quote items collection
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		 
		// get array of all items what can be display directly
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		 
		// get quote items array
		$items = $cart->getQuote()->getAllItems();
		
		 
		$total_gst=0;
		$shippingAmountt=0;
		foreach ($items as $item) 
		{
			
			$product_id=$item->getProductId();
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
				
			$shippingGstRate = $product->getGstRate();
				
				
			$gstPercent = 100 + $shippingGstRate;
			$rowTotal = $item->getRowTotal();
			$discountAmount = $item->getDiscountAmount();
			
			
			$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
			$gstAmount = $productPrice * $shippingGstRate;
			
			//$gstAmount = ((($rowTotal - $discountAmount) * $shippingGstRate) / 100);
			
			$total_gst = $total_gst + $gstAmount;	
		
			
			$qty=$item->getQty();
			
			
			if($customerSession->isLoggedIn()) {
			
				$customer_id=$customerSession->getCustomerId();
				
				$pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
				//$shippingAmount=$data_helpers->ecom_price($pincode);
			
				$shippingAmount=$data_helpers->ecom_price_by_product($pincode,$product_id,$qty);
				$shippingAmountt=$shippingAmountt+$shippingAmount;
			
			}
           
			
			
			
		}	
		

		$subTotal = $cart->getQuote()->getSubtotal();
		$base_amount=$subTotal-$total_gst;
		$grandTotal = $cart->getQuote()->getGrandTotal(); 
		
		$order_total=$subTotal + $shippingAmountt;
		$array=array('base'=>$base_amount,'gst'=>$total_gst,'subtotal'=>$subTotal,'shipping'=>$shippingAmountt,'order_total'=>$order_total);
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$sql = "update quote set grand_total='".$order_total."',base_grand_total='".$order_total."' where entity_id='".$quoteId."'";
		//$connection->query($sql);	
		
					
		echo "<pre>";
			print_r($array);
		echo "</pre>";
		
		
		
		
		
	
	}	
	
	
	function ecom_price($buyer_pincode)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
		
		$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
		$jnkarray=array('jnk','jku');  
		 
		$buyer_pincode; 
		
		$buyer_city=$helpers->get_city_pincode($buyer_pincode);
		$buyer_zipcode=$buyer_pincode;
		
		if($buyer_city=='0')
		{
			return 0;
			die;	  
		}	  
		
		
		$buyer_region=$helpers->get_region_pincode($buyer_zipcode);
		$buyer_region_code=$helpers->get_region_code_pincode($buyer_zipcode);
		
		$buyer_ros=$helpers->ros_checker($buyer_zipcode);	
		$buyer_up_country=$helpers->up_country_checker($buyer_zipcode);	
			
		
		$sellerarray=array();
			
			
		$base_rate=30;
		$additional_rate=30;
		
		$product_total_charge=0;	
		
		
		
		$product_total_charge=0;
		$fuel_amount=0;
		$sub_total=0;
		$other_amount=0;
		$net_total=0;
		$gst_amount=0;
		$grand_total=0;
		$zone=0;
		
		$ros=0;  
		$up_country=0; 
		
		
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	 
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 
		// get quote items collection
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		 
		// get array of all items what can be display directly
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		 
		// get quote items array
		$items = $cart->getQuote()->getAllItems();
		 
		$total_gst=0;
		$shippingAmountt=0;
		$prod_weight=0;
		
		$customerSession = $objectManager->create('Magento\Customer\Model\Session');
		
		
		$user_id=$customerSession->getCustomer()->getId();
		
		
		$total_shipping=0;
		
		$mainArrayy=array();
		
		foreach ($items as $item) 
		{
			
			$itemId = $item->getItemId();
			$productId = $item->getProductId();
			$qty=$item->getQty();
			
			$mainArrayy[]=array(
				'item_id'=>$itemId,
				'product_id'=>$productId,
				'qty'=>$qty
			);		
	
			
		}	
		
		
		
		$shippingAmount=0; 
		$total_shippingAmount=0;
		$special_price=0;
		$sale=0;
		$discountAmount=0;
		$total_gst=0;
		$total_price=0;
		if(!empty($mainArrayy)) 
		{
			foreach($mainArrayy as $row) 
			{
				$id = $row['product_id'];
				$qty= $row['qty'];
				$item_id= $row['item_id'];
				
				$priduct_check=$api_helpers->check_product_exist($id);
				if($priduct_check=='')
				{
					$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product of '.$id.' not found');	
					echo json_encode($result_array);	  
					die;  
				}	 
				
				$product = $productRepository->getById($id); 
				$check_price=$api_helpers->get_product_price($id);
				$orignal_price=$product->getPrice();	 
				
				
				
				
				$check_price=$api_helpers->get_product_price($id);
				
				if($check_price==0)
				{
					$price=$product->getPrice() * $qty;	
				}
				else
				{
					$price=$check_price * $qty;			
				}	 	
				
				

				$shippingGstRate = $product->getGstRate();

				$gstPercent = 100 + $shippingGstRate;
				
				
				
				
				
				$rowTotal = $price;	 

				$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
						
				$gstAmount = $productPrice * $shippingGstRate;
				$total_gst = $total_gst + $gstAmount;
				$total_price=$total_price + $rowTotal;
				
				$pincode=$buyer_pincode;
				if($pincode!='0') 
				{	
			
					$shippingAmount=$data_helpers->ecom_price_by_product($pincode,$id,$qty); 

					$connection->query("UPDATE quote_item SET item_ship_price=$shippingAmount WHERE item_id='".$item_id."'");
					$total_shippingAmount=$total_shippingAmount+$shippingAmount;
					 
				}  
				
				
				
			}
			


			return $total_shippingAmount; 
		 
		}
		
	}	
	
	function add_shipping_return_price($shipping_rate,$shipping_base)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$shipping_base=$shipping_base+75;
		$shipping_gst_amount=($shipping_base*$shipping_rate)/100;
		$total_shipping=$shipping_base+$shipping_gst_amount;
		
		
		$array=array(
			'shipping_base'=>$shipping_base,		
			'shipping_gst_amount'=>$shipping_gst_amount,		
			'total_shipping'=>$total_shipping,		
		);
		
		return $array;
		
	}	
	
	
	public function store_customer_selected_pincode($pincode)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$customerSession = $objectManager->create('Magento\Customer\Model\Session');

		if ($customerSession->isLoggedIn()) 
		{
			 $user_id=$customerSession->getCustomerId();  // get Customer Id
		}
		
		 
		
		$select = $connection->select()
                  ->from('customer_selected_pincode') 
                  ->where('user_id = ?', $user_id);
        $result = $connection->fetchAll($select);   
  		
		
		if(empty($result)) 
		{
			$sql="INSERT INTO `customer_selected_pincode`(`user_id`, `pincode`) VALUES ('".$user_id."','".$pincode."')";
		} 
		else
		{
			$sql="update customer_selected_pincode set pincode='".$pincode."' where user_id='".$user_id."'";  
		}		
		
		
		 $sql;
		
		$connection->query($sql); 
		
	}
	
	
}  
