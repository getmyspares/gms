<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Customer\Controller\Account;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Helper\Address;
use Magento\Framework\UrlFactory;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Customer\Model\Registration;
use Magento\Framework\Escaper;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Controller\AbstractAccount;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Customer\Api\Data\CustomerInterface;                 


/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreatePost extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;
	const XML_PATH_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/email_template';
	
	 //const NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD = 'registered_no_password';

    /**
     * Welcome email, when confirmation is enabled
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_CONFIRMATION = 'confirmation';

    /**
     * Confirmation email, when account is confirmed
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_CONFIRMED = 'confirmed';  
	const XML_PATH_CONFIRMED_EMAIL_TEMPLATE = 'customer/create_account/email_confirmed_template';
    /**
     * @var \Magento\Customer\Helper\Address
     */
    protected $addressHelper;
	 protected $_mediaDirectory;
     protected $_fileUploaderFactory;
    /**
     * @var \Magento\Customer\Model\Metadata\FormFactory
     */
    protected $formFactory;

    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    protected $subscriberFactory;
        
    /**
     * @var \Magento\Customer\Api\Data\RegionInterfaceFactory
     */
    protected $regionDataFactory;

    /**
     * @var \Magento\Customer\Api\Data\AddressInterfaceFactory
     */
    protected $addressDataFactory;

    /**
     * @var \Magento\Customer\Model\Registration
     */
    protected $registration;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory
     */
    protected $customerDataFactory;

    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $customerUrl;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Customer\Model\CustomerExtractor
     */
    protected $customerExtractor;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlModel;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AccountRedirect
     */
    private $accountRedirect;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     * @var Validator
     */
    private $formKeyValidator;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param AccountManagementInterface $accountManagement
     * @param Address $addressHelper
     * @param UrlFactory $urlFactory
     * @param FormFactory $formFactory
     * @param SubscriberFactory $subscriberFactory
     * @param RegionInterfaceFactory $regionDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param CustomerUrl $customerUrl
     * @param Registration $registration
     * @param Escaper $escaper
     * @param CustomerExtractor $customerExtractor
     * @param DataObjectHelper $dataObjectHelper
     * @param AccountRedirect $accountRedirect
     * @param Validator $formKeyValidator
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        Address $addressHelper,
        UrlFactory $urlFactory,
        FormFactory $formFactory,
        SubscriberFactory $subscriberFactory,
        RegionInterfaceFactory $regionDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        CustomerInterfaceFactory $customerDataFactory,
        CustomerUrl $customerUrl,
        Registration $registration,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        DataObjectHelper $dataObjectHelper,
        AccountRedirect $accountRedirect,
		Filesystem $filesystem,
		\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
		CustomHelper $helper,
		
		
        Validator $formKeyValidator = null
    ) {
        $this->session = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->accountManagement = $accountManagement;
        $this->addressHelper = $addressHelper;
        $this->formFactory = $formFactory;
        $this->subscriberFactory = $subscriberFactory;
        $this->regionDataFactory = $regionDataFactory;
        $this->addressDataFactory = $addressDataFactory;
        $this->customerDataFactory = $customerDataFactory;
        $this->customerUrl = $customerUrl;
        $this->registration = $registration;
        $this->escaper = $escaper;
        $this->customerExtractor = $customerExtractor;
        $this->urlModel = $urlFactory->create();
        $this->dataObjectHelper = $dataObjectHelper;
		 $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->accountRedirect = $accountRedirect;
        $this->formKeyValidator = $formKeyValidator ?: ObjectManager::getInstance()->get(Validator::class);
		   
		$this->helper = $helper;   
        parent::__construct($context);
    }  

    /**
     * Retrieve cookie manager
     *
     * @deprecated 100.1.0
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated 100.1.0
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    /**
     * Add address to customer during create account
     *
     * @return AddressInterface|null
     */
    protected function extractAddress()
    {
        if (!$this->getRequest()->getPost('create_address')) {
            return null;
        }

        $addressForm = $this->formFactory->create('customer_address', 'customer_register_address');
        $allowedAttributes = $addressForm->getAllowedAttributes();

        $addressData = [];

        $regionDataObject = $this->regionDataFactory->create();
        foreach ($allowedAttributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            $value = $this->getRequest()->getParam($attributeCode);
            if ($value === null) {
                continue;
            }
            switch ($attributeCode) {
                case 'region_id':
                    $regionDataObject->setRegionId($value);
                    break;
                case 'region':
                    $regionDataObject->setRegion($value);
                    break;
                default:
                    $addressData[$attributeCode] = $value;
            }
        }
        $addressDataObject = $this->addressDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $addressDataObject,
            $addressData,
            \Magento\Customer\Api\Data\AddressInterface::class
        );
        $addressDataObject->setRegion($regionDataObject);

        $addressDataObject->setIsDefaultBilling(
            $this->getRequest()->getParam('default_billing', false)
        )->setIsDefaultShipping(
            $this->getRequest()->getParam('default_shipping', false)
        );
        return $addressDataObject;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
        $resultRedirect->setUrl($this->_redirect->error($url));

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Invalid Form Key. Please refresh the page.')]
        );
    }
 
    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {   
        return null;
    }

    /**
     * Create customer account action
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
		
        /** @var Redirect $resultRedirect */
		$group_id = $this->_request->getParam('group_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->session->isLoggedIn() || !$this->registration->isAllowed()) {
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }

        if (!$this->getRequest()->isPost()
            || !$this->formKeyValidator->validate($this->getRequest())
        ) {
            $url = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
            return $this->resultRedirectFactory->create()
                ->setUrl($this->_redirect->error($url));
        }
		
		
		
		
		   
        $this->session->regenerateId();
		
		$phone = $this->getRequest()->getParam('mobile');
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		 
		//Select Data from table
		$sql = "Select * FROM customer_entity_varchar where value='".$phone."'";
		$result = $connection->fetchAll($sql);
		$count=count($result);
		if($count > 0)
		{
			$resultRedirect = $this->resultRedirectFactory->create();
			$resultRedirect->setPath('customer/account/create/');
			$this->messageManager->addError(
				__(
					'Entered mobile number is already in use'
				)  
			);   
			
			/* $customerSession->unsCustomerRegData();
			$customerSession->unsPinValue(); */
			return $resultRedirect; 		
			die;
		}   
		
		
        try {
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
			$customerSession = $objectManager->get('Magento\Customer\Model\Session');
			
            $address = $this->extractAddress();
			
            $addresses = $address === null ? [] : [$address];
			
            $customer = $this->customerExtractor->extract('customer_account_create', $this->_request);
            $customer->setAddresses($addresses);
				 
		/* 	$customer->setGroupId($group_id);
			$customer->setCustomAttribute('group_id',$group_id);
          $this->customerRepository->save($customer); */
 
			
            $password = $this->getRequest()->getParam('password');
            $confirmation = $this->getRequest()->getParam('password_confirmation');
            $redirectUrl = $this->session->getBeforeAuthUrl();
			
			
			
            $this->checkPasswordConfirmation($password, $confirmation);
			
			/* echo "<pre>";
				print_r($_POST); 
			echo "</pre>";   */ 
			
			$userId=0;
			
			$errorcode=0;
			$pincodeSession=$customerSession->getPinValue(); 
			$completeUser=array();
			
			if($pincodeSession=='')
			{
				$pincodeSession=0;		
			} 
			
			$group_id=$_POST['group_id'];
			$is_seller=$_POST['is_seller'];
			 
			
			if($group_id!=1 || $is_seller==1 || $group_id!=5)      
			{      
				// Company
				$customer = $this->accountManagement
					->createAccount($customer, $password, $redirectUrl);

				if ($this->getRequest()->getParam('is_subscribed', false)) {
					$this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
				}

				$this->_eventManager->dispatch(
					'customer_register_success',
					['account_controller' => $this, 'customer' => $customer]
				);
				
				
				if($group_id==4)
				{
					$resultRedirect = $this->resultRedirectFactory->create();
					$resultRedirect->setPath('customer/account/create/');
					$this->messageManager->addSuccess(
						__(
							'Your account has been created successfully and will be confirmed by admin shortly'
						)  
					);   
					
					  
				}	
				else
				{	
					$confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
					if ($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
						$email = $this->customerUrl->getEmailConfirmationUrl($customer->getEmail());
						// @codingStandardsIgnoreStart
						$this->messageManager->addSuccess(
							__(
								'You must confirm your account. Please check your email for the confirmation link or <a href="%1">click here</a> for a new link.',
								$email
							)  
						);  
						// @codingStandardsIgnoreEnd
						$url = $this->urlModel->getUrl('*/*/index', ['_secure' => true]);
						$resultRedirect->setUrl($this->_redirect->success($url));
						 						
						//$resultRedirect->setPath('/');  
					} else {    
						$this->session->setCustomerDataAsLoggedIn($customer);
						$this->messageManager->addSuccess($this->getSuccessMessage());
						$requestedRedirect = $this->accountRedirect->getRedirectCookie();
						if (!$this->scopeConfig->getValue('customer/startup/redirect_dashboard') && $requestedRedirect) {
							$resultRedirect->setUrl($this->_redirect->success($requestedRedirect));
							$this->accountRedirect->clearRedirectCookie();
							return $resultRedirect;
						}
						$resultRedirect = $this->accountRedirect->getRedirect();
					}   
				}
				
				if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
					$metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
					$metadata->setPath('/');
					$this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
				}
				
				
				$custID=$customer->getId();
				
				
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();
				 
 
				$sql = "update customer_grid_flat set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
				$connection->query($sql);
				
				$sql = "update customer_entity set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
				$connection->query($sql);      
				  
				
				$customerSession->unsCustomerRegData();
				$customerSession->unsPinValue();    
				  
				return $resultRedirect; 	  
				die;     
			}
			else
			{  
				if($pincodeSession==0)
				{    
					$CustomerRegData=json_encode($_POST);
					
					$pin = mt_rand(1000, 9999);				  
					
					$today=date('Y-m-d H:i:s');			
					$phone=$_POST['mobile']; 
					$group_id=$_POST['group_id'];
					  
					   	
					
					$completeUser=array(
						'pin'=>$pin,	
						'phone'=>$phone,	
						'group_id'=>$group_id,	
						'cr_time'=>$today	
					);  
					
					
					$userPinPhoneJson=json_encode($completeUser);
					  
					 
					$customerSession->setUserPinPhoneJson($userPinPhoneJson); //set value in customer session
					$customerSession->getUserPinPhoneJson(); //Get value from customer session 
							 
					$customerSession->setCustomerRegData($CustomerRegData); //set value in customer session
					$customerSession->getCustomerRegData(); //Get value from customer session 
					
					   
					
					$customerSession->setCustomerRegData($CustomerRegData); //set value in customer session
					$customerSession->getCustomerRegData(); //Get value from customer session 
					
					 
					
					$customerSession->setPinValue($pin); //set value in customer session
					$customerSession->getPinValue(); //Get value from customer session
					
									
					$email = $_POST['email'];	  
						   
					$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
					$CustomerModel->setWebsiteId(1);
					$CustomerModel->loadByEmail($email);
					$userId = $CustomerModel->getId();
					 
					if($userId==0)   
					{
						$resultRedirect = $this->resultRedirectFactory->create();
						$resultRedirect->setPath('customer/account/create/');
						$this->messageManager->addSuccess(
							__(
								'Your OTP has been sent to your mobile number'
							)  
						);
											
						$this->helper->CustomResendOTP($pin,$phone,$email);    

						return $resultRedirect;            	 
					}          		   
					else    
					{ 
						
						$site_url='https://store.idsil.com/customer/account/forgotpassword/customer/account/forgotpassword';
						$resultRedirect = $this->resultRedirectFactory->create();
						$resultRedirect->setPath('customer/account/login/');
						 
						$customerSession->unsCustomerRegData();
						$customerSession->unsPinValue();
						
						$resultRedirect = $this->resultRedirectFactory->create();
						$resultRedirect->setPath('customer/account/create/');
						$this->messageManager->addError(
							__( 
								'There is already an account with this email address. If you are sure that it is your email address, <a href="'.$site_url.'">click here</a> to get your password and access your account.'
							)         
						);    
						  
						
						return $resultRedirect;      
					}      
					         	
					 
					die();           
				}  
				else  
				{  	  	
					   
					   
					$firstname = $_POST['firstname'];  	   		
					$lastname = $_POST['lastname'];  	
					$email = $_POST['email'];	  
					$smscode = $_POST['smscode'];   	   		
					$phone = $_POST['mobile']; 			
					$CustomerDataa=json_decode($customerSession->getUserPinPhoneJson());
					
					$custphone=$CustomerDataa->phone;
					$group_id=$CustomerDataa->group_id;
					 
					$minutes = (time() - strtotime($CustomerDataa->cr_time)) / 60;
					$minutes = round($minutes);
								
					if($minutes > 4)
					{   
						$resultRedirect = $this->resultRedirectFactory->create();
						$resultRedirect->setPath('customer/account/create/');
						$this->messageManager->addError(
							__(
								'OTP Pin has been expired'
							)  
						);   
						 
						//$customerSession->unsCustomerRegData();
						//$customerSession->unsPinValue();
							 
						return $resultRedirect;      	
					}
					else if($smscode!=$pincodeSession) 
					{ 
						$resultRedirect = $this->resultRedirectFactory->create();
						$resultRedirect->setPath('customer/account/create/');
						$this->messageManager->addError(
							__(
								'Your OTP is not valid'
							)  
						);   
						return $resultRedirect;      	
					}
					else
					{
						if($group_id==1 || $group_id==5)
						{   
							$customer = $this->accountManagement
								->createAccount($customer, $password, $redirectUrl);
							
							
							
							                    
							/* if ($this->getRequest()->getParam('is_subscribed', false)) {
								$this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
							} */

							$this->_eventManager->dispatch(
								'customer_register_success',
								['account_controller' => $this, 'customer' => $customer]
							);
							
							$this->session->setCustomerDataAsLoggedIn($customer);
							
							$custID=$customer->getId();
							  
							$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
							$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
							$connection = $resource->getConnection();
							
			 
							$sql = "update customer_entity set confirmation = NULL WHERE entity_id='".$custID."' ";
							$connection->query($sql);  
							  
							$resultRedirect = $this->resultRedirectFactory->create();
							$resultRedirect->setPath('/'); 
							$this->messageManager->addSuccess(
								__(
									'Welcome to board'
								)  
							);    
							
							  
							
							$this->helper->individual_welcome($firstname,$lastname,$email) ;                  
							               
							
							$customerSession->unsCustomerRegData();
							$customerSession->unsPinValue();
							return $resultRedirect; 
						}             
					}   
				}
			}
			
        } catch (StateException $e) {   
            $url = $this->urlModel->getUrl('customer/account/forgotpassword');
            
            $message = __(
                'There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.',
                $url
            );
            // @codingStandardsIgnoreEnd
            $this->messageManager->addError($message);
        } catch (InputException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addError($this->escaper->escapeHtml($error->getMessage()));
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t save the customer.'));
        }

        $this->session->setCustomerFormData($this->getRequest()->getPostValue());
        $defaultUrl = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
        return $resultRedirect->setUrl($this->_redirect->error($defaultUrl));
    }

    /**
     * Make sure that password and password confirmation matched
     *
     * @param string $password
     * @param string $confirmation
     * @return void
     * @throws InputException
     */
    protected function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

    /**
     * Retrieve success message
     *
     * @return string
     */
    protected function getSuccessMessage()   
    {
        if ($this->addressHelper->isVatValidationEnabled()) {
            if ($this->addressHelper->getTaxCalculationAddressType() == Address::TYPE_SHIPPING) {
                // @codingStandardsIgnoreStart
                $message = __(
                    'If you are a registered VAT customer, please <a href="%1">click here</a> to enter your shipping address for proper VAT calculation.',
                    $this->urlModel->getUrl('customer/address/edit')
                );
                // @codingStandardsIgnoreEnd
            } else {
                // @codingStandardsIgnoreStart
                $message = __(
                    'If you are a registered VAT customer, please <a href="%1">click here</a> to enter your billing address for proper VAT calculation.',
                    $this->urlModel->getUrl('customer/address/edit')
                );
                // @codingStandardsIgnoreEnd
            }
        } else {
            $message = __('Thank you for registering with %1.', $this->storeManager->getStore()->getFrontendName());
        }
        return $message;
    }
	  
	
	private function getEmailNotification()
    {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        } 
    }
	
	
	
}
