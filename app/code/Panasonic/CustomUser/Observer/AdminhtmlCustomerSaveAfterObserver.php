<?php
/**
 * IDs Software.
 *
 * @category  IDs
 * @package   IDs
 * @author    IDs
 * @copyright Copyright (c) IDs Software Private Limited
 * @license   https://store.ids.com/license.html
 */

namespace Panasonic\CustomUser\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Panasonic\CustomUser\Helper\Data as CustomHelper;
/**
 * IDs  AdminhtmlCustomerSaveAfterObserver Observer.
 */
class AdminhtmlCustomerSaveAfterObserver implements ObserverInterface
{ 
    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

   
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Store manager.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $_messageManager;

    protected $_mediaDirectory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Product\Collection
     */
    protected $_sellerProduct;

    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    protected $_jsonDecoder;
	
	
    protected $storeManager;

	protected $scopeConfig;
    /**
     * @param Filesystem                                       $filesystem,
     * @param \Magento\Framework\ObjectManagerInterface        $objectManager,
     * @param \Magento\Framework\Stdlib\DateTime\DateTime      $date,
     * @param \Magento\Framework\Message\ManagerInterface      $messageManager,
     * @param \Magento\Store\Model\StoreManagerInterface       $storeManager,
     * @param \Magento\Catalog\Api\ProductRepositoryInterface  $productRepository,
     * @param CollectionFactory                                $collectionFactory,
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param ProductCollection                                $sellerProduct
     * @param \Magento\Framework\Json\DecoderInterface         $jsonDecoder
     */  
    public function __construct(
        Filesystem $filesystem,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
       
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        
        \Magento\Framework\Json\DecoderInterface $jsonDecoder,
		
		// ------- Custom Code 
		ScopeConfigInterface $scopeConfig,
		\Magento\Framework\App\Helper\Context $context,
		CustomHelper $helper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
		
		
    ) {
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_objectManager = $objectManager;
        $this->_messageManager = $messageManager;
       
        $this->_storeManager = $storeManager;
        $this->_date = $date;
        $this->_jsonDecoder = $jsonDecoder;
		// ------- Custom Code 
		$this->_scopeConfig = $context;
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
		$this->scopeConfig = $scopeConfig;
		$this->helper = $helper;
    }

    /**
     * admin customer save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
	 
	 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		
        $customer = $observer->getCustomer();
        $customerid = $customer->getId();
        $postData = $observer->getRequest()->getPostValue();
		$approv_seller = 0;
		if(!empty($postData['is_seller_add'])){
			$approv_seller = $postData['is_seller_add'];
		}
		
		
		    
		  
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$tableName = $resource->getTableName('company_user_approval'); 
		
		$companyAppro = $postData['customer']['attribute_approved'];
		$sql = "Select * FROM ". $tableName." where cust_id = '".$customerid."'";
        $result = $connection->fetchAll($sql);
		
		
		if(!empty($result)){
			// already approved;
			$approved_value = 1;
		}else{	
			// echo 'not approved';
			$approved_value = 0;
		}
		

		$already_welcomed = 0;
		
		$tableName2 = $resource->getTableName('company_user_welcome');
		$sql2 = "Select * FROM ".$tableName2." where cust_id = '".$customerid."'";
		$wel_result = $connection->fetchAll($sql2);
		//print_r($wel_result);
		
		if(!empty($wel_result)){
			$already_welcomed = 1;
		}
		
		
		
		/*
		**** Code to check if the welcome email is send or not to company buyer for first time
		*/	
		
		$store_id = $postData['customer']['store_id'];
		$firstname = $postData['customer']['firstname'];
		$lastname = $postData['customer']['lastname'];
		$to_email = $postData['customer']['email'];
		$sender_email = $this->scopeConfig->getValue('trans_email/ident_support/email',ScopeInterface::SCOPE_STORE);
		$sender_name  = $this->scopeConfig->getValue('trans_email/ident_support/name',ScopeInterface::SCOPE_STORE);
				
		if($companyAppro == 1 ){
			if($approved_value == 0){
				$sql = "Insert Into " . $tableName . " (id, cust_id, created_date) Values ('','$customerid',NOW())";
				$connection->query($sql);
				
				$fname = $firstname;
				$lname = $lastname;
				$email = $to_email;
				$status ="approved";
				
				$this->helper->company_request($fname,$lname,$email,$status);

				$this->_messageManager->addSuccess(__("Customer is notificed by email about approval."));
				if($already_welcomed == 0 ){
					
					$sql3 = "Insert Into ".$tableName2." (id, cust_id, created_date) Values ('','$customerid',NOW())";
					$connection->query($sql3);
					
					$fname = $firstname;
					$lname = $lastname;
					$email = $to_email;
					
					$this->helper->individual_welcome($fname,$lname,$email);
					
				}

				$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
				$check_this_seller=$api_helpers->check_user_seller($customerid);
				
				if($check_this_seller==0)    
				{	
					$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
					$buyer_helpers->create_buyer_report_updated($customerid);      	   
				}  

				
				 return $this;
			}	
		}
		
		if($companyAppro==0 ){  
			if($approved_value == 1){
				$delete_val = "Delete FROM " . $tableName." where cust_id = '".$customerid."'";
				$connection->query($delete_val);
				
				$fname = $firstname;
				$lname = $lastname;
				$email = $to_email;
				$status ="disapproved";
				
				$this->helper->dis_company_request($fname,$lname,$email,$status);
				
				$this->_messageManager->addSuccess(__("Customer is notificed by email about disapproval."));
				
				
				$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
				$check_this_seller=$api_helpers->check_user_seller($customerid); 
				
				if($check_this_seller==0)    
				{	
					$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
					$buyer_helpers->create_buyer_report_updated($customerid);      	   
				}  
				
				
				return $this;

			}
		} 
		
		
		
		if($approv_seller==1){

			if($already_welcomed == 0 ){
				
				$fname = $firstname;
				$lname = $lastname;
				$email = $to_email;
				
				$this->helper->individual_welcome($fname,$lname,$email);
				
				
				
				 
				 
				return $this;
			}			

		} 
		
		
		
		
        // return $this;
    }
}
