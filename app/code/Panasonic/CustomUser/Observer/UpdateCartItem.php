<?php
namespace Panasonic\CustomUser\Observer;
  
use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\Framework\App\RequestInterface;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Checkout\Model\Cart;
use \Magento\Framework\Message\ManagerInterface ;
use Magento\Quote\Model\QuoteRepository;

use \Magento\Checkout\Model\Session as CheckoutSession;
  
class UpdateCartItem implements ObserverInterface
{	
	/** @var CheckoutSession */
    protected $checkoutSession;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;
	
	
	protected $formKey;   
	protected $cart;
	protected $product;
/**
     * @param CheckoutSession $checkoutSession
     * @param ProductFactory $productLoader
     * @param RequestInterface $request
     * @param ProductRepository $productRepository
     * @param Cart $cart
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */

    public function __construct(
        
		\Magento\Checkout\Model\Session $checkoutSession,

        \Magento\Framework\Message\ManagerInterface $messageManager,
		
		\Magento\Framework\Data\Form\FormKey $formKey,
		\Magento\Checkout\Model\Cart $cart,
		\Magento\Catalog\Model\Product $product,
		
		
		array $data = []
    )
    {	
		
		$this->checkoutSession = $checkoutSession;
		
        
        $this->_messageManager = $messageManager;
		
		$this->formKey = $formKey;
		$this->cart = $cart;
		$this->product = $product;
		
    }
  
        /**
     * add to cart event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		
		$cart = $observer->getEvent()->getCart();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$qty_limit = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('example_section_1/general_1/qty_limit_num');
		
		  
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		if($customerSession->isLoggedIn()) {
		   $custom_data = $customerSession->getData();
		   $custom_group_id = $custom_data['customer_group_id'];
		}else{
			$custom_group_id ="";
		}
		
		$items = $observer->getCart()->getQuote()->getItems();
		$info = $observer->getInfo()->getData();

		foreach ($items as $item) {
			$itemId = $item->getId();
			$old_qty = $item->getQty();
			// echo 'Item '. $item->getName() .' change qty from ' . $item->getQty() . ' to ' . $info[$item->getId()]['qty'] . '.<br>';
			// die();
			if($custom_group_id){
				if($custom_group_id == "1"){
					$current_item_qty ="";
					if(!empty($info[$item->getId()])){
						$current_item_qty = $info[$item->getId()]['qty'];
					}else{
						continue;
					}
					if($current_item_qty > $qty_limit){
					
						$sku = $item->getSku();
						$productId = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($item->getSku());

						$message = 'The purchase quantity of the '.$item->getName().' should be equal or less than '.$qty_limit.'.';
						$this->_messageManager->addError(__($message));
						//Remove the Item from the card
						$cart->getQuote()->removeItem($itemId);
						//add the product again with old quantity 
						$params = array(
									'form_key' => $this->formKey->getFormKey(),
									'product' => $productId, 
									'qty'   =>$old_qty               
								);              
						//Load the product based on productID   
						$_product = $this->product->load($productId);       
						$this->cart->addProduct($_product, $params);
						$this->cart->save();
					
					}
				}
			}else{

				$current_item_qty ="";
				if(!empty($info[$item->getId()])){
					$current_item_qty = $info[$item->getId()]['qty'];
				}else{
					continue;
				}
				if($current_item_qty > $qty_limit){
					
					$sku = $item->getSku();
					$productId = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($item->getSku());
										
				
				
					$message = 'The purchase quantity of the '.$item->getName().' should be equal or less than '.$qty_limit.'.';
					$this->_messageManager->addError(__($message));
					//Remove the Item from the card
					$cart->getQuote()->removeItem($itemId);
					//add the product again with old quantity 
					$params = array(
								'form_key' => $this->formKey->getFormKey(),
								'product' => $productId, //product Id
								'qty'   =>$old_qty //quantity of product                
							);              
					//Load the product based on productID   
					$_product = $this->product->load($productId);       
					$this->cart->addProduct($_product, $params);
					$this->cart->save();
					
				}
			}
		}

    }
}