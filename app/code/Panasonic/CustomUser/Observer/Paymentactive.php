<?php
namespace Panasonic\CustomUser\Observer;

use Magento\Framework\Event\ObserverInterface;

class Paymentactive implements ObserverInterface 
{
    protected $_checkoutSession;

    public function __construct(\Magento\Checkout\Model\Session $checkoutSession) {
        $this->_checkoutSession = $checkoutSession;
    }
    public function execute(\Magento\Framework\Event\Observer $observer) {
		
		//echo $this->_checkoutSession->getQuote()->getShippingAddress()->getRegion();
		
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $_session=$objectManager->get('Magento\Checkout\Model\Session');
        $method = $observer->getEvent()->getMethodInstance();
        $result = $observer->getEvent()->getResult();
		/* $om = \Magento\Framework\App\ObjectManager::getInstance();
		$context = $om->get('Magento\Framework\App\Http\Context');
		$isLoggedIn = $context->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
		 */
	//echo $method->getCode();
		 $new = $this->_checkoutSession->getData();
		$ship = isset($new['guest_checkout']) ? $new['guest_checkout'] : '';
		 
		$methodd=$method->getCode();
		  
		 
		if($ship == 1)
        {
			if($methodd == "cashondelivery") 
			{
				return $result->setData('is_available', false);
			}
		}  
		else 
		{
			return $result->setData('is_available', true);
		}  

    }
}
?>