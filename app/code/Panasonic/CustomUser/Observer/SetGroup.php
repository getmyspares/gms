<?php

namespace Panasonic\CustomUser\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class SetGroup implements ObserverInterface
{    
    private $customerRepository;
	protected $_eventObject = 'customer';
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
         CustomerRepositoryInterface $customerRepository
    )
    {
        $this->_request = $request;
        $this->customerRepository = $customerRepository;
    }

    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {          
        $id = $observer->getEvent()->getCustomer()->getId();
        $customer = $this->customerRepository->getById($id);
		
        $groupId = $this->_request->getParam(4);  
      	
        $customer->setGroupId($groupId);    
        $this->customerRepository->save($customer);    
    }
} 