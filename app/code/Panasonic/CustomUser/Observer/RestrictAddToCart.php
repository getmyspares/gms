<?php
namespace Panasonic\CustomUser\Observer;
  
use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\Framework\App\RequestInterface;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Checkout\Model\Cart;
use \Magento\Framework\Message\ManagerInterface ;

use \Magento\Checkout\Model\Session as CheckoutSession;
  
class RestrictAddToCart implements ObserverInterface
{	
	/** @var CheckoutSession */
    protected $checkoutSession;
    /**
     * @var ProductFactory
     */
    protected $_productloader;
    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var Rationinglogic|Data
     */
    protected $_helperData;
    /**
     * @var Cart
     */
    protected $_cart;
    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

/**
     * @param CheckoutSession $checkoutSession
     * @param ProductFactory $productLoader
     * @param RequestInterface $request
     * @param ProductRepository $productRepository
     * @param Cart $cart
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */

    public function __construct(
        
		\Magento\Checkout\Model\Session $checkoutSession,
        // ProductFactory $productLoader,
        // RequestInterface $request,
         ProductRepository $productRepository,
        // cart $cart,
        \Magento\Framework\Message\ManagerInterface $messageManager,
		array $data = []
    )
    {	
		$this->checkoutSession = $checkoutSession;
        // $this->_productloader = $productLoader;
        // $this->_request = $request;
         $this->_productRepository = $productRepository;
        // $this->_helperData = $helperData;
        // $this->_cart = $cart;
        $this->_messageManager = $messageManager;
    }
  
    /**
     * add to cart event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		
		/**
		* 
		* Get the global quantity limit for Individual Buyer
		*
		**/
		// die('dsadsadsa');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$qty_limit = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('example_section_1/general_1/qty_limit_num');
		
		/**
		* 
		* Check for customer is logged or not and check the customergroup id
		*
		**/		
				  
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		if($customerSession->isLoggedIn()) {
		   $custom_data = $customerSession->getData();
		   $custom_group_id = $custom_data['customer_group_id'];
		}else{
			$custom_group_id ="";
		}
		
		$product_qty = $product_id = "";
		$product_id = $observer->getRequest()->getParam('product');
		$product_qty = $observer->getRequest()->getParam('qty');
		if($product_qty==""){
			$product_qty= 1;
		}
		//--- Getting the product qty in card
		$product_cart_qty = "";
		$cart = $objectManager->get('\Magento\Checkout\Model\Session')->getQuote();
		$result = $cart->getAllVisibleItems();
		
		$items = $cart->getAllItems();

		foreach ( $items as $item) {
			if ($item->getProductId() == $product_id) {
				$product_cart_qty = $item->getQty();
			}
		}
		if($product_cart_qty){
			$product_qty += $product_cart_qty;
		}
				
		if($custom_group_id != "4"){
				if($product_qty > $qty_limit){
					
					$this->_messageManager->addError(__('The purchase quantity of the product should be equal or less than '.$qty_limit.'.'));
					//set false if you not want to add product to cart
					$observer->getRequest()->setParam('product', false);
					return $this;
				}
		}
        return $this;
    }
}