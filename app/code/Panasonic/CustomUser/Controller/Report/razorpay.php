<?php
 
namespace Panasonic\CustomUser\Controller\Report;
 
use Magento\Framework\App\Action\Context;

class Razorpay extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        if(!isset($_GET['access']))
		{
			die;	 
		}	
		else
		{
			
			
			$string  = 'yehhaiadminsection'; 
			$encoded = $_GET['access'];
			$decoded = str_rot13($_GET['access']);
			
			
			
			 $encoded.' '.$decoded;
			if($string!=$decoded)
			{
				die;	
			}
				
		}	 
		
		$resultPage = $this->_resultPageFactory->create();
		 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\RazorPay')
                ->setTemplate('Panasonic_CustomUser::razorpay.phtml')
                ->toHtml();
        $this->getResponse()->setBody($block); 
		 
		
	}  
	
	
}