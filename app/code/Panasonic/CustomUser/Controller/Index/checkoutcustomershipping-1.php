<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Checkoutcustomershipping extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
          
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$customer_id=$_POST['customer_id'];
		$row=$_POST['row'];
		
		$select = $connection->select()
                  ->from('customer_entity') 
                  ->where('entity_id = ?', $customer_id);
                      
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			die;	 
		}	 
		   
		  
		
		if($row!='null')  
		{
			
			$connection = $resource->getConnection();
						
			
			$select = $connection->select() 
                  ->from('customer_address_entity') 
                  ->where('parent_id = ?', $customer_id);
			
			
			$billing_id=$result[$row]['entity_id'];
			
			$firstname=$custom_helpers->shipping_address_billing_id($billing_id,'firstname');
			$lastname=$custom_helpers->shipping_address_billing_id($billing_id,'lastname');
			$mobile=$custom_helpers->shipping_address_billing_id($billing_id,'telephone');
			$street=$custom_helpers->shipping_address_billing_id($billing_id,'street');
			$city=$custom_helpers->shipping_address_billing_id($billing_id,'city');
			$region=$custom_helpers->shipping_address_billing_id($billing_id,'region');
			$country=$custom_helpers->shipping_address_billing_id($billing_id,'country_id'); 
			$pincode=$custom_helpers->shipping_address_billing_id($billing_id,'postcode'); 
			
			
			$valid=$custom_helpers->get_postcode_valid($pincode);
			
			

			
			$name=$firstname.' '.$lastname;
			$address=$street.' '.$city.' '.$region.' '.$country;
			  
			if($mobile!='')
			{
				$mobile='Mob : '.$mobile;	
			}
			
			$data=$name.$mobile.'<br>'.$address.'<span>Change</span>'; 
			
			
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			 
			// get quote items collection
			$itemsCollection = $cart->getQuote()->getItemsCollection();
			 
			// get array of all items what can be display directly
			$itemsVisible = $cart->getQuote()->getAllVisibleItems();
			 $items = $cart->getQuote()->getAllItems();
			// get quote items array
			$total_gst=0;
		foreach ($items as $item) {
            if ($item->getId()) {
                /*
				$product = $this->productFactory->create()->load($item->getProductId());
                if ($product->getTypeId() == 'configurable') {
                    $product = $this->productRepository->get($item->getSku());
                    $productPrice = $product->getFinalPrice();
                } else {
                    $productPrice = $product->getFinalPrice();
                }
                $categoryIds = $product->getCategoryIds();
                $productPriceAfterDiscount = ($productPrice * $product->getDiscountPercent()) / 100;
                $productPrice = $productPrice - $productPriceAfterDiscount;
                $flag = false;
                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $categoryId) {
                        $category = $this->categoryRepository->get($categoryId, $this->storeManager->getStore()->getId());
                        $shippingGstRate = $category->getCatGstRate();
                        $gstRateMinAmount = $category->getCatMinGstAmount();
                        $gstMinRate = $category->getCatMinGstRate();
                        if ($shippingGstRate) {
                            $gstMinRate = $category->getCatMinGstRate();
                            if ($category->getCatGstRate()) {
                                $flag = true;
                            }
                            break;
                        }
                    }
                }
				*/
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
				
				$shippingGstRate = $product->getGstRate();
				
				
				$gstPercent = 100 + $shippingGstRate;
				$rowTotal = $item->getRowTotal();
                $discountAmount = $item->getDiscountAmount();
				
				
                $productPrice = ($rowTotal - $discountAmount) / $gstPercent;
                $gstAmount = $productPrice * $shippingGstRate;
				
				//$gstAmount = ((($rowTotal - $discountAmount) * $shippingGstRate) / 100);
				
				 
				$total_gst = $total_gst + $gstAmount;
				
            }
        }
			
			
			$subTotal = $cart->getQuote()->getSubtotal();
			$grandTotal = $cart->getQuote()->getGrandTotal(); 
			
			
			
			$cartarray=(array)$itemsCollection->getData();
			
			/* echo "<pre>";
				print_r($cartarray);
			echo "</pre>";   */
			
			$tax_amount=0;
			
			if(!empty($cartarray)) 
			{
			
				foreach($cartarray as $row)
				{
					$tax_amount=$tax_amount + $row['tax_amount'];		
				}	
				
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();

				$customerSession = $objectManager->create('Magento\Customer\Model\Session');
				$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
				$shippingAmount = $helpers->ecom_price($pincode); 	
				$shippingAmountt = $helpers->ecom_price($pincode); 	 
				
				 
				$grandTotal=$subTotal+$total_gst;
				
				$order_total=$grandTotal + $shippingAmount;
				
				/*
				if($customerSession->isLoggedIn()) {
				
					$customer_id=$customerSession->getCustomerId();
					
					$pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
					$shippingAmount=$data_helpers->ecom_price($pincode);
				}
				*/  
				  
				$shipping=number_format($shippingAmount, 2, '.', ''); 
				 
				$base_price= $subTotal-$total_gst;
			
			
				$order_total=$subTotal + $shipping;  
				  
				
				$cart_summary= ' 
				<div class="cart-summary">
				<div class="price_details">
					<h2>Summary</h2>
				</div>
				<div class="text_section">        
					<i class="fa fa-exclamation" aria-hidden="true"></i> Processing charge of ₹ 50 will be applicable on COD
				</div>
				<div class="cart-totals">
					<table>
						<tbody>	
							<tr>
								<td>Base Price</td>
								<td> &#8377; '.number_format($base_price, 2, '.', '').'</td>
							</tr>
							<tr>
								<td>GST</td>
								<td> &#8377; '.number_format($total_gst, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Sub Total</td>
								<td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Shipping and Handling</td>
								<td>&#8377; '.$shipping.'</td>
							</tr>	 
							<tr class="total_row"> 
								<td>Order Total</td>
								<td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
							</tr>
							<tr style="display:none;">
								<td>&nbsp;</td>
								<td class="act_cart_total">'.number_format($order_total, 2, '.', '').'</td>
							</tr>
							 
							
						</tbody>  
					</table>
					</div>
					</div>
			';	  
			}  
			else 
			{
				$cart_summary='<p>No Item In Cart</p>';
			}
		
			$array=array('data'=>$data,'cart_summary'=>$cart_summary,'valid'=>$valid);
			echo json_encode($array); 
		 
		}    
		else
		{	
			$firstname=$custom_helpers->get_customer_shipping_address($customer_id,'firstname');
			$lastname=$custom_helpers->get_customer_shipping_address($customer_id,'lastname');
			$mobile=$custom_helpers->get_customer_shipping_address($customer_id,'telephone');
			$street=$custom_helpers->get_customer_shipping_address($customer_id,'street');
			$city=$custom_helpers->get_customer_shipping_address($customer_id,'city');
			$region=$custom_helpers->get_customer_shipping_address($customer_id,'region');
			$country=$custom_helpers->get_customer_shipping_address($customer_id,'country_id'); 
			$pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode'); 
			$name=$firstname.' '.$lastname;
			$address=$street.' '.$city.' '.$region.' '.$country;
			
			$name=$firstname.' '.$lastname;
			$address=$street.' '.$city.' '.$region.' '.$country;
			  
			if($mobile!='')
			{
				$mobile='Mob : '.$mobile;	
			}	  
			$data=$name.$mobile.'<br>'.$address.'<span>Change</span>'; 
			 
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			 
			// get quote items collection
			$itemsCollection = $cart->getQuote()->getItemsCollection();
			 
			// get array of all items what can be display directly
			$itemsVisible = $cart->getQuote()->getAllVisibleItems();
			 
			// get quote items array
			$items = $cart->getQuote()->getAllItems();
			$total_gst=0;
		foreach ($items as $item) {
            if ($item->getId()) {
                /*
				$product = $this->productFactory->create()->load($item->getProductId());
                if ($product->getTypeId() == 'configurable') {
                    $product = $this->productRepository->get($item->getSku());
                    $productPrice = $product->getFinalPrice();
                } else {
                    $productPrice = $product->getFinalPrice();
                }
                $categoryIds = $product->getCategoryIds();
                $productPriceAfterDiscount = ($productPrice * $product->getDiscountPercent()) / 100;
                $productPrice = $productPrice - $productPriceAfterDiscount;
                $flag = false;
                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $categoryId) {
                        $category = $this->categoryRepository->get($categoryId, $this->storeManager->getStore()->getId());
                        $shippingGstRate = $category->getCatGstRate();
                        $gstRateMinAmount = $category->getCatMinGstAmount();
                        $gstMinRate = $category->getCatMinGstRate();
                        if ($shippingGstRate) {
                            $gstMinRate = $category->getCatMinGstRate();
                            if ($category->getCatGstRate()) {
                                $flag = true;
                            }
                            break;
                        }
                    }
                }
				*/
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
				
				$shippingGstRate = $product->getGstRate();
				
				
				$gstPercent = 100 + $shippingGstRate;
				$rowTotal = $item->getRowTotal();
                $discountAmount = $item->getDiscountAmount();
				
				
                $productPrice = ($rowTotal - $discountAmount) / $gstPercent;
                $gstAmount = $productPrice * $shippingGstRate;
				
				//$gstAmount = ((($rowTotal - $discountAmount) * $shippingGstRate) / 100);
				
				   
				$total_gst = $total_gst + $gstAmount;
				
            }
        }
			
			
			$subTotal = $cart->getQuote()->getSubtotal();
			$grandTotal = $cart->getQuote()->getGrandTotal(); 
			
			
			
			$cartarray=(array)$itemsCollection->getData();
			
			/* echo "<pre>";
				print_r($cartarray);
			echo "</pre>";   */
			
			$tax_amount=0;
			
			if(!empty($cartarray)) 
			{
			
				foreach($cartarray as $row)
				{
					$tax_amount=$tax_amount + $row['tax_amount'];		
				}	
				
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();

				$customerSession = $objectManager->create('Magento\Customer\Model\Session');
				$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
				$shippingAmount = $helpers->ecom_price($pincode); 	
				$shippingAmountt = $helpers->ecom_price($pincode); 	 
				
				
				$grandTotal=$subTotal+$total_gst;
				
				$order_total=$grandTotal + $shippingAmount;
				
				if($customerSession->isLoggedIn()) {
				
					$customer_id=$customerSession->getCustomerId();
					
					$pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
					$shippingAmount=$data_helpers->ecom_price($pincode);
				}
				  
				  
				$shipping=number_format($shippingAmount, 2, '.', ''); 
				 
				$base_price= $subTotal-$total_gst;
			
			
				$order_total=$subTotal + $shipping;    
				
				$cart_summary= '
				<div class="cart-summary">
				<div class="price_details">
					<h2>Summary</h2>
				</div>
				<div class="text_section">        
					<i class="fa fa-exclamation" aria-hidden="true"></i> Processing charge of ₹ 50 will be applicable on COD
				</div>
				<div class="cart-totals">
					<table>
						<tbody>	
							<tr>
								<td>Base Price</td>
								<td> &#8377; '.number_format($base_price, 2, '.', '').'</td>
							</tr>
							<tr>
								<td>GST</td>
								<td> &#8377; '.number_format($total_gst, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Sub Total</td>
								<td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Shipping and Handling</td>
								<td>&#8377; '.$shipping.'</td>
							</tr>	 
							<tr class="total_row"> 
								<td>Order Total</td>
								<td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
							</tr>
							<tr style="display:none;">
								<td>&nbsp;</td>
								<td class="act_cart_total">'.number_format($order_total, 2, '.', '').'</td>
							</tr>
							 
							
						</tbody>  
					</table>
					</div>
					</div>
			';	  
			}  
			else 
			{
				$cart_summary='<p>No Item In Cart</p>';
			}
		
			$array=array('data'=>$data,'cart_summary'=>$cart_summary);
			echo json_encode($array);
		}       
		  
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}