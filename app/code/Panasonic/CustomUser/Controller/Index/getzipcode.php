<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Getzipcode extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $latitude=$_POST['lat']; 
        $longitude=$_POST['lng'];  
				
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helpers = $objectManager->create('Ecom\Ecomexpress\Helper\Data');
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$latitude.",".$longitude."&sensor=false&key=AIzaSyBDxzfCX6R16v9VgSM3GoF0Z6MAezcL35g";
		$result_string = file_get_contents($url);
		$result = json_decode($result_string, true);
		// $result['results'][0]['geometry']['location'];
		//continous error on var/log/exception.log so placed it in isset condition @ritesh9august2021
		if(isset($result['results'][0]['address_components']))
		{
			$resultarray=$result['results'][0]['address_components'];
			foreach($resultarray as $key => $row)
			{
				if($row['types'][0]=='postal_code')
				{
					echo $resultarray[$key]['long_name'];
				}	
				
			}	
		}
	}     
}