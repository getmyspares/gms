<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Cartsummary extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
  
    public function execute()
    {
        
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
        $coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
        $saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
		  
		
		$customerSession = $objectManager->get('Magento\Customer\Model\Session'); 
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		 
		// get quote items collection
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		 
		// get array of all items what can be display directly
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		 
		// get quote items array
		$items = $cart->getQuote()->getAllItems();
		 
		$total_gst=0;
		foreach ($items as $item) {
            if ($item->getId()) {
                /*
				$product = $this->productFactory->create()->load($item->getProductId());
                if ($product->getTypeId() == 'configurable') {
                    $product = $this->productRepository->get($item->getSku());
                    $productPrice = $product->getFinalPrice();
                } else {
                    $productPrice = $product->getFinalPrice();
                }
                $categoryIds = $product->getCategoryIds();
                $productPriceAfterDiscount = ($productPrice * $product->getDiscountPercent()) / 100;
                $productPrice = $productPrice - $productPriceAfterDiscount;
                $flag = false;
                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $categoryId) {
                        $category = $this->categoryRepository->get($categoryId, $this->storeManager->getStore()->getId());
                        $shippingGstRate = $category->getCatGstRate();
                        $gstRateMinAmount = $category->getCatMinGstAmount();
                        $gstMinRate = $category->getCatMinGstRate();
                        if ($shippingGstRate) {
                            $gstMinRate = $category->getCatMinGstRate();
                            if ($category->getCatGstRate()) {
                                $flag = true;
                            }
                            break;
                        }
                    }
                }
				*/
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
				
				$shippingGstRate = $product->getGstRate();
				
				
				$gstPercent = 100 + $shippingGstRate;
				$rowTotal = $item->getRowTotal();
                $discountAmount = $item->getDiscountAmount();
				
				
                $productPrice = ($rowTotal - $discountAmount) / $gstPercent;
                //$gstAmount = $productPrice * $shippingGstRate;
				
				$gstAmount = $productPrice * $shippingGstRate;
				//$gstAmount = ((($rowTotal - $discountAmount) * $shippingGstRate) / 100);
				
				 
				$total_gst = $total_gst + $gstAmount;
				
            }
        }
		
		$subTotal = $cart->getQuote()->getSubtotal();
		$grandTotal = $cart->getQuote()->getGrandTotal();

		
        $couponCode = $cart->getQuote()->getCouponCode();
        $ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
        $rule = $saleRule->load($ruleId);
        $freeShippingCoupon = $rule->getSimpleFreeShipping();
		$cartarray=(array)$itemsCollection->getData();
		
		/*  echo "<pre>";
			print_r($cartarray);
		echo "</pre>";    */
		
		$tax_amount=0;
        $shippingAmount=0;
        $shippingAmountt=0;
        if(!empty($cartarray))
        {
            
            $data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
            
            foreach($cartarray as $row)
            {
                $tax_amount=$tax_amount + $row['tax_amount'];        
                
                
                
                
                
                if($customerSession->isLoggedIn()) {
                
                    $customer_id=$customerSession->getCustomerId();
                    
                    $pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
                    //$shippingAmount=$data_helpers->ecom_price($pincode);
                
                    $product_id=$row['product_id'];
                    $qty=$row['qty'];
                    
                    
                    $shippingAmount=$data_helpers->ecom_price_by_product($pincode,$product_id,$qty);
                    $shippingAmountt=$shippingAmountt+$shippingAmount;
                
                }
                
                
                
            }    
            
            
            $shippingAmountt;
            
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            //$customerSession = $objectManager->create('Magento\Customer\Model\Session');
            //$shippingAmount = $customerSession->getcheckoutshippingSession();
            //$shippingAmountt = $customerSession->getcheckoutshippingSession();
            
            if($customerSession->isLoggedIn())
            {
                $shippingAmount=$shippingAmountt;
            }
             
            
                
            
            
            $grandTotal=$subTotal+$total_gst;
            
            $order_total=$grandTotal + $shippingAmount;
            
            
             
              
            $shipping=number_format($shippingAmount, 2, '.', '');

            if($freeShippingCoupon){
               $shipping = 0 ;
            }
            
            $base_price= $subTotal-$total_gst;
            
            
            $order_total=$subTotal + $shipping;
           
            echo '
                <table>
                    <tbody>    
                        <tr>
                            <td>Base Price</td>
                            <td> &#8377; '.number_format($base_price, 2, '.', '').'</td>
                        </tr>
                        <tr>
                            <td>GST</td>
                            <td> &#8377; '.number_format($total_gst, 2, '.', '').'</td>
                        </tr>
                          
                        <tr>
                            <td>Sub Total</td>
                            <td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
                        </tr><tr>
                            <td>Shipping and Handling</td>
                            <td>&#8377; '.$shipping.'</td>
                        </tr>
                        <tr>
                            <td>Order Total</td>
                            <td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
                        </tr>    
                    </tbody>
                </table>
            ';      
        }
        else
        {
            echo "<p>No Item In Cart</p>";
        }
  
	 
	}
	
}