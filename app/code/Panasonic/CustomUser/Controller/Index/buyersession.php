<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Framework\Controller\ResultFactory;
class Buyersession extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	public function __construct(Context $context, 
	\Magento\Framework\View\Result\PageFactory $resultPageFactory, 
	ResultFactory $resultFactory,
	\Magento\Store\Model\StoreManagerInterface $storeManager, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->resultFactory = $resultFactory;
		$this->helper = $helper;  
        parent::__construct($context); 
    }
 
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$customer_id =  $customerSession->getbuyerSession(); 

		$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($customer_id);

		$accountManagement = $objectManager->get('\Magento\Customer\Api\AccountManagementInterface');
		$customer_account_confirmation_status = $accountManagement->getConfirmationStatus($customer_id);
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_urls=$storeManager->getStore()->getBaseUrl();
		$site_url = $site_urls; 
		$customer_section_url  =  $site_url."customer/account/login";
		$this->messageManager->addSuccess(__('Thanks ! Please confirm  your email id to confirm  your registration'));
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
		$resultRedirect->setUrl($customer_section_url);
		return $resultRedirect; 
		die(); 
		
		// $customerSession->setCustomerAsLoggedIn($customer);

		if($customerSession->getIsCheckoutRedirect()=="yes")
		{
			$customerSession->setIsCheckoutRedirect("no");
			$site_url = $this->_url->getUrl('checkout', ['_secure' => true]); 
		
		}

		?>          
		<style>     
		
		.loader_1{z-index:99999 !important}
		@-webkit-keyframes spin {
		 0% { -webkit-transform: translate(-50%,-50%) rotate(0deg); }
		 100% { -webkit-transform: translate(-50%,-50%) rotate(360deg); }
		}

		@keyframes spin {
		 0% { transform: translate(-50%,-50%) rotate(0deg); }
		 100% { transform:translate(-50%,-50%)  rotate(360deg); }
		}
		.loader_1 span { position: absolute;  left: 50%;  top: 50%;  transform: translate(-50%,-50%);}
		.loader_1 span:before{content:'';border: 2px solid #f3f3f3; border-radius: 50%; border-top: 2px solid #3f549c;border-bottom: 2px solid #3f549c;width: 120px; height: 120px; -webkit-animation: spin 2s linear infinite;  animation: spin 2s linear infinite;    display: inline-block; top: 50%;  left: 50%;  position: absolute;}
		
		
		.loader_1 {  
			position: fixed;
			left: 0px; 
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: #fff;
			background-size: 10%;
		} 

		.loader_1 p {
		   text-align: center;
		   margin-top: 13%;
		       font-family: 'Exo';
			   font-size:20px;
			   color:#3d3d3f !important
			   font-weight:500;
		} 
		     
		</style>
		
		<div class="loader_1">
			<!--<p>Please wait for 5 seconds. You are redirecting to Dashboard....</p>-->
			<span><img src="<?php echo $site_url.'Customimages/fav-icon.ico';?>" alt="loader"></span> 
		</div>     
		  
		<script>
			// window.location.href="<?php echo $site_url; ?>";
		</script>          
	<?php   
	}        
	
	
	
	
}