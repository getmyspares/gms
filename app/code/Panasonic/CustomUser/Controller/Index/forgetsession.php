<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Forgetsession extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
        parent::__construct($context);
    }
 
    public function execute()
    {
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_urls=$storeManager->getStore()->getBaseUrl();
		 $site_url=$site_urls; 
		
		
		/* $customerSession->unsForgetOtpPinExpire();       
		$customerSession->unsForgetcountdown();         
		$customerSession->unsForgetPinJson();         
		$customerSession->unsForgetcountdown();       
		
		$customerSession->unsForgetOtpTimerMin();           
		$customerSession->unsForgetOtpTimerSec();      
		  
				
		$customerSession->setForgetOtpTimerSec('NaN');      
		
		
		$customerSession->unsForgetExpireSessionMin();         
		$customerSession->unsForgetExpireSessionSec();    
		
		$customerSession->setForgetExpireSessionSec('NaN');     
		  	   
		$customerSession->unsForgetResendbutton(); */	
		 
		 
		$customerSession->unsForgetPinJson();
		 
		$customerSession->unsForgetOtpCheck();  
		$customerSession->unsForgetResendbutton();  
		   
		$customerSession->setForgetallwell(1);
		$customerSession->unsForgetresendcount();   
		$customerSession->unsForgetcountdown();  
		
		$customerSession->unsForgetperson();    
		 
		 
		?>          
		<style>     
		
		.loader_1{z-index:99999 !important}
		@-webkit-keyframes spin {
		 0% { -webkit-transform: translate(-50%,-50%) rotate(0deg); }
		 100% { -webkit-transform: translate(-50%,-50%) rotate(360deg); }
		}

		@keyframes spin {
		 0% { transform: translate(-50%,-50%) rotate(0deg); }
		 100% { transform:translate(-50%,-50%)  rotate(360deg); }
		}
		.loader_1 span { position: absolute;  left: 50%;  top: 50%;  transform: translate(-50%,-50%);}
		.loader_1 span:before{content:'';border: 2px solid #f3f3f3; border-radius: 50%; border-top: 2px solid #3f549c;border-bottom: 2px solid #3f549c;width: 120px; height: 120px; -webkit-animation: spin 2s linear infinite;  animation: spin 2s linear infinite;    display: inline-block; top: 50%;  left: 50%;  position: absolute;}
		
		
		.loader_1 {  
			position: fixed;
			left: 0px; 
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: #fff;
			background-size: 10%;
		} 

		.loader_1 p {
		   text-align: center;
		   margin-top: 13%;
		       font-family: 'Exo';
			   font-size:20px;
			   font-weight:500;
			   color:#3d3d3f !important
		} 
		     
		</style>
		<script>
		 require([
            'jquery',
            'owl.carousel/owl.carousel.min'
          ], function ($) {
			
		
			if(jQuery('body').hasClass('customer-account-forgotpassword'))
			{
				//alert('dfr');
				jQuery('.loader_1').remove();
			}  
		  })
		</script>
		
		<div class="loader_1">
			<!--<p>Please wait for 5 seconds. You are redirecting to Dashboard....</p>-->
			<span><img src="<?php echo $site_url.'/fav-icon.ico';?>" alt="loader"></span>
		</div>     
		<script> 
			window.location.href="<?php echo $site_url; ?>";
		</script>           
		<?php 	
	     
	}         
	
	
	
	
}