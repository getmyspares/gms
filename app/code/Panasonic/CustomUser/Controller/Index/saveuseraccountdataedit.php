<?php
 
namespace Panasonic\CustomUser\Controller\Index;
  
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper;     
class Saveuseraccountdataedit extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;    
	public $_storeManager;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;     
        parent::__construct($context);  
    }
 
    public function execute()
    {
		
		$resultPage = $this->_resultPageFactory->create();
		 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\Saveuseraccountdetailedit')
                ->setTemplate('Panasonic_CustomUser::saveuseraccountdetailedit.phtml')
                ->toHtml();  
         $this->getResponse()->setBody($block);            
		
	}               
	
	
	
	
}