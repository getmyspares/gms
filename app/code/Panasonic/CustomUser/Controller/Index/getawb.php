<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
		$order_id=33;   
			 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		//$order_id = $_POST['order'];
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_id);
		$address = $order->getShippingAddress();
		  
		$zipcodeCollection = $objectManager->create( 'Ecom\Ecomexpress\Model\Pincode' )->load($address->getPostcode(),'pincode');
		 
				
		if(count($zipcodeCollection->getData())){
			$payment = $order->getPayment()->getMethodInstance()->getCode();
			$pay_type = 'PPD';
			if($payment == 'cashondelivery' || $payment == 'phoenix_cashondelivery' || $payment == 'mst_cashondelivery')
				$pay_type = 'COD';
			$model = $this->_objectManager->create ( 'Ecom\Ecomexpress\Model\Awb' )->getCollection()
				->addFieldToFilter('state',0)->addFieldToFilter('awb_type',$pay_type);
			if(count($model->getData())){
				$awb = $model->getFirstItem()->getAwb();
				echo $awb;//die;
				//return $awb;
			}else{
				echo $pay_type.' AWB number is not available';
			} 
		}else{ 
			echo 'Pincode is not serviceable';
		}  
		
		
		
		
		
	 
	}
	
}