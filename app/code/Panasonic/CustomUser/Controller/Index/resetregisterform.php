<?php
 
namespace Panasonic\CustomUser\Controller\Index;
   
use Magento\Framework\App\Action\Context;   
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
class Resetregisterform extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;   
	 private $customerRepository;
	 private $encryptor;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, CustomHelper $helper, CustomerRepositoryInterface $customerRepository, Encryptor $encryptor) 
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->helper = $helper;    
		$this->customerRepository = $customerRepository;
		 $this->encryptor = $encryptor;    
        parent::__construct($context);   
    }
 
    public function execute()  
    {   
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$today=date('Y-m-d H:i:s');
		
		
		
		
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_urls=$storeManager->getStore()->getBaseUrl();
		$site_url=$site_urls; 
		
		
		  
		$customerSession->unsPinValue(); 
		$customerSession->unsOTPFormShow();        
		$customerSession->unsOtpPinExpire();       
		$customerSession->unscountdown();    

		$customerSession->unsExpireSessionMin();     
		$customerSession->unsExpireSessionSec();      


		$customerSession->unsCustomerRegData(); 
		$customerSession->unsRegister_resend();  	 
		
		$customerSession->setRegister_allwell(1); 
		
		$customerSession->unsRegisterFormSubmit(); 
		
		$customerSession->unsRegisterresendcount();  
		     
		?>          
		<style>     
		
		.loader_1{z-index:99999 !important}
		@-webkit-keyframes spin {
		 0% { -webkit-transform: translate(-50%,-50%) rotate(0deg); }
		 100% { -webkit-transform: translate(-50%,-50%) rotate(360deg); }
		}

		@keyframes spin {
		 0% { transform: translate(-50%,-50%) rotate(0deg); }
		 100% { transform:translate(-50%,-50%)  rotate(360deg); }
		}
		.loader_1 span { position: absolute;  left: 50%;  top: 50%;  transform: translate(-50%,-50%);}
		.loader_1 span:before{content:'';border: 2px solid #f3f3f3; border-radius: 50%; border-top: 2px solid #3f549c;border-bottom: 2px solid #3f549c;width: 120px; height: 120px; -webkit-animation: spin 2s linear infinite;  animation: spin 2s linear infinite;    display: inline-block; top: 50%;  left: 50%;  position: absolute;}
		
		
		.loader_1 {  
			position: fixed;
			left: 0px; 
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: #fff;
			background-size: 10%;
		} 

		.loader_1 p {
		   text-align: center;
		   margin-top: 13%;
		       font-family: 'Exo';
			   font-size:20px;
			   font-weight:500;
			   color:#3d3d3f !important
		} 
		     
		</style>
		<div class="loader_1"> 
			<!--<p>Please wait for 5 seconds. You are redirecting to Dashboard....</p>-->
			<span><img src="<?php echo $site_url.'Customimages/fav-icon.ico';?>" alt="loader"></span>
		</div>         
		<script>
			window.location.href="<?php echo $site_url; ?>";
		</script>           
		<?php 	 

		
	}        
	
	
	
	
}