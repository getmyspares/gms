<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Createorder extends \Magento\Framework\App\Action\Action 
{
    protected $_resultPageFactory;
	public $_storeManager;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
        parent::__construct($context); 
    }
 
    public function execute()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$razorpay_order_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpayorder');
		$quote_id=$_REQUEST['quote_id'];  
		
		
		$razorpay_order_helpers->create_manual_order($quote_id);
		
		  
	}        
	
	
	
	
}