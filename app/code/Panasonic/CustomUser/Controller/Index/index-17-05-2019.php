<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
	
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$order_inc_id='000000198';
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		
		/* echo "<pre>";
			print_r($orderArray);
		echo "</pre>"; */
		
		
		$order_total_inc_tax=$orderArray['grand_total']; // base price + tax
		
		//$order_total_amount='1000';
		$order_total_amount=$orderArray['subtotal_incl_tax']; // base price + tax
		
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');    
		$sellerarray=$tax_helpers->get_order_seller_array($order_inc_id);
		
		$count=count($sellerarray); 
		
		
		
		$tax_rates=$tax_helpers->tax_parameter();
		$tax_rates_array=json_decode($tax_helpers->tax_parameter());
		/* echo "<pre>";
			print_r($tax_rates_array);
		echo "</pre>"; */
		
		
		$payment_type='';
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0; 
		$payble_to_razorpay=0; 
		
		$razor_pay=json_decode($tax_helpers->tax_razorpay_expense($order_inc_id));
		
		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		if($razor_pay->type=='online')
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			$razorpay_tds=$tax_helpers->tax_razorpay_tds($payment_amount,$order_inc_id); 
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		}
		else 
		{
			$payment_type='COD';
			$seller_comm = $seller_comm -($noddle);
		}
		echo '
		<table style="width:100%;" border="1">
		<thead>
		<tr>
			<th>Order ID</th>
			<th>Invoice ID</th>
			<th>Order Total Inc Tax</th> 
			<th>Order Total Exc Tax</th> 
			<th>Seller ID</th>
			<th>Seller Name</th>
			<th>Admin Commission Subtotal</th>
			<th>Commission Tax</th>
			<th>Commission TDS</th>
			<th>Admin Commission</th>
			<th>Logistic Tax('.$tax_rates_array->shipping_gst.'%)</th>
			<th>Logistic Amount</th>
			<th>Logistic TDS('.$tax_rates_array->shipping_tds.'%)</th> 
			<th>Payable to Logistic</th> 
			<th>Payment Type</th> 
			<th>Razorpay Tax</th> 
			<th>Razorpay Fee</th> 
			<th>Razorpay Amount</th> 
			<th>Razorpay TDS</th> 
			<th>Payble to Razorpay</th> 
			<th>Seller Due</th> 
			   
		</tr> 
		</thead>
		<tbody>
		';
		
		
		if($count==1)	
		{
			$data_helper = $objectManager->create('Panasonic\CustomUser\Helper\Data');
			
			$invoice_id=$data_helper->get_order_invoice_id($order_inc_id); 
			foreach($sellerarray as $row)
			{
				$seller_id=$row;		
				$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
				$seller_details=json_decode($sellerdetails);
				$seller_name=$seller_details->seller_name;
				
				$admin_comm=$tax_helpers->tax_admin_commission($order_total_amount,$order_inc_id);
				$admin_comm_tax=$tax_helpers->tax_admin_commission_tax($order_total_amount,$order_inc_id);
				$admin_comm_tds=$tax_helpers->tax_admin_commission_tds($order_total_amount,$order_inc_id);
				
				$total_admin_comm=$admin_comm + $admin_comm_tax;
				
				
				$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
				$logistic_tds=$tax_helpers->tax_logistic_tds($order_shipping_amount,$order_inc_id);
				
				$logistic_pay=$order_shipping_amount-$logistic_tds;
				$logistic_pay=number_format($logistic_pay, 2, '.', '');
				
				
				
				
				echo '
					<tr>
						<td>'.$order_inc_id.'</td>
						<td>'.$invoice_id.'</td>
						<td>'.$order_total_inc_tax.'</td>
						<td>'.$order_total_amount.'</td>
						<td>'.$seller_id.'</td>
						<td>'.$seller_name.'</td>
						<td>'.$admin_comm.'</td>
						<td>'.$admin_comm_tax.'</td>
						<td>'.$admin_comm_tds.'</td>
						<td>'.$total_admin_comm.'</td>
						<td>'.$logistic_tax.'</td>
						<td>'.$order_shipping_amount.'</td>
						<td>'.$logistic_tds.'</td>
						<td>'.$logistic_pay.'</td>
						<td>'.$payment_type.'</td>
						<td>'.$payment_tax.'</td>
						<td>'.$payment_fee.'</td>
						<td>'.$payment_amount.'</td>
						<td>'.$razorpay_tds.'</td>
						<td>'.$payble_to_razorpay.'</td>
						<td>'.$seller_comm.'</td>
						   
						   
					</tr>';	
				
				
				foreach ($order->getAllItems() as $item)
				{
					$product_id=$item->getId();
				}  
			
			}	
		} 	
		echo '
		</tbody>
		</html>
		';			
	
            

      
		
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}