<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$order_inc_id='000000227';
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		
		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
		
		$orderArray=$order->getData();  			
		
		
		
		/* echo "<pre>";
			print_r($orderArray);
		echo "</pre>";  */ 
		
		
		$created_at=$orderArray['created_at'];
		$tax_amount=$orderArray['tax_amount'];
		$shipping_amount=$orderArray['shipping_amount'];
		$order_total_amount=$orderArray['subtotal_incl_tax'];
		$order_status=$orderArray['status'];
		$coupon_code=$orderArray['coupon_code'];
		$discount_description=$orderArray['discount_description'];
		$grand_total=$orderArray['grand_total'];
		
		
		$order_shipping_amount=$orderArray['shipping_incl_tax']; // base price + tax
		$order_shipping_amount=number_format($order_shipping_amount, 2, '.', '');   
		
		
		$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		$logistic_gst=$tax_helpers->tax_logistic_gst($order_shipping_amount,$order_inc_id); 
		$logistic_tds=$tax_helpers->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_pay=$order_shipping_amount-$logistic_tds;
		$logistic_pay=number_format($logistic_pay, 2, '.', '');
		
		$shipping_date=$tax_helpers->tax_get_order_shipping_date($order_id);
		if($shipping_date==0)
		{ 
			$shipping_date='-';		 
		}

		if($coupon_code=='')
		{ 
			$coupon_code='-';		
		}	
		
		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		
		
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		
		$street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	

		
		$productName='';
		$productQty='';
		$productCat='';
		$productDescp='';
		$trimstring='';
		
		$productLength='';
		$productBreath='';
		$productHeight='';
		$productAbsWeight='';
		$productLogisticWeight='';
		$productDiscount='';
		$productDiscountAmount='';
		  
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getId();
			$productName .=$item->getName().',';
			$productQty .=$item->getQtyOrdered().',';
			
			
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
			$categories = $product->getCategoryIds(); 
			$productDescp = $product->getDescription();
			
			if (strlen($productDescp) > 25) {
				$trimstring .= substr($productDescp, 0, 25).',';
			} else {
				$trimstring .= $productDescp.',';
			}
			
			
			foreach($categories as $category){
				$cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
				$productCat .=$cat->getName().',';
			}
			
			
			$sellerarray[]=$tax_helpers->tax_get_product_seller($order_inc_id,$product_id);
			
			$productLength .= $product->getData('ecom_length').',';
			$productBreath .= $product->getData('ecom_breadth').',';
			$productHeight .= $product->getData('ecom_height').',';
			
			$productAbsWeight .=$product->getWeight().',';
			
			$logistic_weight = $product->getData('ecom_length')*$product->getData('ecom_breadth')*$product->getData('ecom_height');
			if(!empty($logistic_weight)){
				 if($logistic_weight>1){
					 $logistic_weight = $logistic_weight/5000;
				 }else{
					 $logistic_weight = 0;
				 }
			}
			
			$productLogisticWeight .= $logistic_weight.',';
			
			
			$productDiscount .=round($product->getDiscountPercent()).'%'.',';
			$productDiscountAmount .=round($product->getDiscountAmount()).'%'.',';
			 
			
			
		}

		$resultarray=array_values(array_unique($sellerarray));
		
		$seller_details='';
		$seller_code='';
		$seller_name='';
		$seller_comp_address='';
		$seller_zipcode='';
		foreach($resultarray as $seller_id)
		{
			
			$seller_details=json_decode($tax_helpers->tax_get_seller_array($seller_id));		
			
			$seller_code .=$seller_details->id.',';
			$seller_name .=$seller_details->seller_name.',';
			$seller_comp_address .=$seller_details->seller_comp_address.',';
			$seller_zipcode .=$seller_details->seller_zipcode.',';
			
		}  	 
		
		
		$product_name=substr($productName,0,-1);
		$product_qty=substr($productQty,0,-1);
		$product_category=substr($productCat,0,-1);
		$product_description=substr($trimstring,0,-1);
		
		$product_length=substr($productLength,0,-1);
		$product_breath=substr($productBreath,0,-1);
		$product_height=substr($productHeight,0,-1);
		$product_abs_weight=substr($productAbsWeight,0,-1);
		$product_logistic_weight=substr($productLogisticWeight,0,-1);
		$product_discount=substr($productDiscount,0,-1);
		$product_discount_amount=substr($productDiscountAmount,0,-1);
		   
		
		$sellercode=substr($seller_code,0,-1);
		$sellername=substr($seller_name,0,-1);
		$sellercompaddress=substr($seller_comp_address,0,-1);
		$sellerzipcode=substr($seller_zipcode,0,-1);
		
		
		
		$admin_comm=$tax_helpers->tax_admin_commission($order_total_amount,$order_inc_id);
		$admin_comm_tax=$tax_helpers->tax_admin_commission_tax($order_total_amount,$order_inc_id);
		$admin_comm_tds=$tax_helpers->tax_admin_commission_tds($order_total_amount,$order_inc_id);
		
		$total_admin_comm=$admin_comm + $admin_comm_tax;
		
		
		$payment_type='';
		$payment_fee=0;
		$payment_tax=0;
		$payment_amount=0;
		$razorpay_tds=0; 
		$payble_to_razorpay=0; 
		
		$razor_pay=json_decode($tax_helpers->tax_razorpay_expense($order_inc_id));
		
		$seller_comm=$custom_helpers->custom_seller_comission($order_inc_id);
		$noddle=$custom_helpers->custom_noddle_amount($order_inc_id);
		$noddle=number_format($noddle, 2, '.', '');
		
		/* echo "<pre>";
			print_r($razor_pay);
		echo "</pre>"; */
		
		
		
		if($razor_pay->type=='online')
		{	
			$payment_type=$razor_pay->type;
			$payment_fee=$razor_pay->fee;
			$payment_tax=$razor_pay->tax;
			$payment_amount=$razor_pay->amount;
			$razorpay_tds=$tax_helpers->tax_razorpay_tds($razor_pay,$order_inc_id); 
			$payble_to_razorpay=$payment_amount-$razorpay_tds;
			//$payble_to_razorpay=$payment_fee-$razorpay_tds;
			
			$payble_to_razorpay=number_format($payble_to_razorpay, 2, '.', '');    
			
			$seller_comm = $seller_comm -($payment_amount+$noddle);
		}
		else 
		{
			$payment_type='COD';
			$payble_to_razorpay='0';
			$seller_comm = $seller_comm -($noddle);
		}
		 
		$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($created_at)); 
		$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
		
		
		echo '
		<table border="1" style="width:100%">
		<thead> 
		<tr>
			<th>Order ID</th>
			<th>Invoice ID</th>
			<th>Created Date</th>
			<th>Buyer Name</th>
			<th>Buyer City</th>
			<th>Buyer State</th>
			<th>Buyer Pincode</th>
			<th>Delivery Name</th>
			<th>Delivery Address</th>
			<th>Delivery City</th>
			<th>Delivery State</th>
			<th>Delivery Pincode</th>
			<th>Item</th>
			<th>Quantity</th>
			<th>Product Category</th>
			<th>Gross Price Amt(INR)</th>
			<th>Seller Code</th>
			<th>Seller Name</th>
			<th>Seller address</th>
			<th>Seller city</th>
			<th>Seller state</th>
			<th>Seller pincode</th>
			<th>Mat Descrip</th>
			<th>Tax Amount</th>
			<th>Shipping</th>
			<th>Shipping Tax</th>
			<th>Total</th>
			<th>Comission Subtotal</th>
			<th>Comission Tax</th>
			<th>Comission TDS</th>
			<th>Admin Comission</th>
			<th>Payment Type</th>
			<th>Razorpay Tax</th>
			<th>Razorpay Fee</th>
			<th>Razorpay Amount</th>
			<th>Razorpay TDS</th>
			<th>Payble To Razorpay</th>
			<th>COD Charges</th>
			<th>COD Tax</th>
			<th>Total COD expense</th>
			<th>Order Status</th>
			<th>Order Date</th>
			<th>Shipping date</th>
			<th>Delivery date</th>
			<th>Realization date</th>
			<th>Length</th>
			<th>Breath</th>
			<th>Height</th>
			<th>Absolute wt</th>
			<th>Logistic wt</th>
			<th>Discount%</th>
			<th>Reason of discount</th>
			<th>Discount cost</th>
			<th>Coupon used</th>
			<th>Counpon amount</th>
		</tr>     
		</thead>
		<tbody> 
		';
		
		echo '
		<tr>
			<td>'.$order_inc_id.'</td>
			<td>'.$invoice_id.'</td>
			<td>'.$created_at.'</td>
			<td>'.$buyer_name.'</td>
			<td>'.$buyer_city.'</td>
			<td>'.$buyer_state.'</td>
			<td>'.$buyer_pincode.'</td>
			<td>'.$delivery_name.'</td>
			<td>'.$delivery_adress.'</td>
			<td>'.$delivery_city.'</td>
			<td>'.$delivery_state.'</td>
			<td>'.$delivery_pincode.'</td> 
			<td>'.$product_name.'</td> 
			<td>'.$product_qty.'</td> 
			<td>'.$product_category.'</td> 
			<td>'.$grand_total.'</td> 
			<td>'.$sellercode.'</td> 
			<td>'.$sellername.'</td> 
			<td>'.$sellercompaddress.'</td>  
			<td>---</td> 
			<td>---</td> 
			<td>'.$sellerzipcode.'</td> 
			<td>'.$product_description.'</td> 
			<td>'.$tax_amount.'</td>  
			<td>'.$logistic_gst.'</td> 
			<td>'.$logistic_tax.'</td> 
			<td>'.$order_shipping_amount.'</td> 
			<td>'.$admin_comm.'</td> 
			<td>'.$admin_comm_tax.'</td> 
			<td>'.$admin_comm_tds.'</td> 
			<td>'.$total_admin_comm.'</td> 
			<td>'.$payment_type.'</td> 
			<td>'.$payment_tax.'</td> 
			<td>'.$payment_fee.'</td> 
			<td>'.$payment_amount.'</td> 
			<td>'.$razorpay_tds.'</td> 
			<td>'.$payble_to_razorpay.'</td> 
			<td>--</td> 
			<td>--</td> 
			<td>--</td>  
			<td>'.$order_status.'</td>  
			<td>'.$created_at.'</td>  
			<td>'.$shipping_date.'</td>  
			<td>--</td>  
			<td>'.$RealizationDays.'</td>
			<td>'.$product_length.'</td>
			<td>'.$product_breath.'</td>
			<td>'.$product_height.'</td>
			<td>'.$product_abs_weight.'</td>
			<td>'.$product_logistic_weight.'</td>
			<td>'.$product_discount.'</td> 
			<td>'.$discount_description.'</td>  
			<td>'.$product_discount_amount.'</td>
			<td>'.$coupon_code.'</td>
			<td>'.$coupon_code.'</td>
		</tr>         
		';
		
		
		echo '
		</tbody>
		</table>
		';
		
		
		
		
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}