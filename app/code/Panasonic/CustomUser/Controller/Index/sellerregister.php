<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Sellerregister extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$customerSession->setSellerValue(1); 
		
		?>
		<style>     
		
		.loader_1{z-index:99999 !important}
		@-webkit-keyframes spin {
		 0% { -webkit-transform: translate(-50%,-50%) rotate(0deg); }
		 100% { -webkit-transform: translate(-50%,-50%) rotate(360deg); }
		}

		@keyframes spin {
		 0% { transform: translate(-50%,-50%) rotate(0deg); }
		 100% { transform:translate(-50%,-50%)  rotate(360deg); }
		}
		.loader_1 span { position: absolute;  left: 50%;  top: 50%;  transform: translate(-50%,-50%);}
		.loader_1 span:before{content:'';border: 2px solid #f3f3f3; border-radius: 50%; border-top: 2px solid #3f549c;border-bottom: 2px solid #3f549c;width: 120px; height: 120px; -webkit-animation: spin 2s linear infinite;  animation: spin 2s linear infinite;    display: inline-block; top: 50%;  left: 50%;  position: absolute;}
		
		
		.loader_1 {  
			position: fixed;
			left: 0px; 
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: #fff;
			background-size: 10%;
		} 

		.loader_1 p {
		   text-align: center;
		   margin-top: 13%;
		       font-family: 'Exo';
			   font-size:20px;
			   font-weight:500;
			   color:#3d3d3f !important
		} 
		     
		</style> 
		
		<div class="loader_1">
			
			<span><img src="<?php echo $site_url; ?>Customimages/fav-icon.ico" alt="loader"></span>
		</div>   
		<script>
			window.location.href="<?php echo $site_url;  ?>customer/account/create/";
		</script>   
		<?php
		 
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}