<?php
 
namespace Panasonic\CustomUser\Controller\Index;
   
use Magento\Framework\App\Action\Context;   
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;

class Resetpassword extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;   
	 private $customerRepository;
	 private $encryptor;
	 
	 protected $resultFactory;
    protected $url;
	 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, CustomHelper $helper, CustomerRepositoryInterface $customerRepository, Encryptor $encryptor, 
	UrlInterface $url,
    ResultFactory $resultFactory
	) 
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->helper = $helper;    
		$this->customerRepository = $customerRepository;
		$this->encryptor = $encryptor;    
		 
		$this->resultFactory = $resultFactory;
		$this->url = $url; 
		 
        parent::__construct($context);   
    }
 
    public function execute()  
    {   
		//print_r($_REQUEST);  
		$customerId=$_POST['user_id'];
		$customerId = filter_var($customerId, FILTER_SANITIZE_NUMBER_INT);
		$password=$_POST['new_password']; 
		
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$today=date('Y-m-d H:i:s');
		
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$forgetjson=json_decode($customerSession->getForgetPinJson(),true);
		//print_r($forgetjson);
		
		 $sel_user_id=$forgetjson['userId'];
		
		
		
		
		
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
		$resultRedirect->setUrl($this->url->getUrl('customer/account/login'));
		
		
		if($sel_user_id == $customerId)
		{
			$customer = $this->customerRepository->getById($customerId);
			$this->customerRepository->save($customer, $this->encryptor->getHash($password, true));
			
			$this->messageManager->addSuccess(__('Password has been reset successfully'));
			return $resultRedirect;  
		}
		else
		{
			$this->messageManager->addError(__('Customer id not matched '));
			return $resultRedirect;   
		}
		 
		$customerSession->unsForgetPinJson();
		 
		$customerSession->unsForgetOtpCheck();  
		$customerSession->unsForgetResendbutton();  
		
		$customerSession->setForgetallwell(1);
        $customerSession->unsForgetcountdown();   
				  
		
	  
		     
		          
	  
    }      
	
	
	
	
}