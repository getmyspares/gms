<?php
	namespace Panasonic\CustomUser\Controller\Index;
 
	use Magento\Framework\App\Action\Context;
	use Panasonic\CustomUser\Helper\Data as CustomHelper; 

	class Checkwishlist extends \Magento\Framework\App\Action\Action{
		protected $_resultPageFactory;
		public $_storeManager;
		protected $_checkoutSession;
		
		public function __construct(
			Context $context,
			\Magento\Framework\View\Result\PageFactory $resultPageFactory,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Checkout\Model\Session $checkoutSession,
			CustomHelper $helper
		){
			$this->_resultPageFactory = $resultPageFactory;
			$this->_storeManager=$storeManager;
			$this->helper = $helper;  
			$this->_checkoutSession = $checkoutSession;
			parent::__construct($context);
		}
 
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$request = $objectManager->get('\Magento\Framework\App\RequestInterface');
		
		$messageManager = $objectManager->get('Magento\Framework\Message\ManagerInterface');
		

		$customerSession = $objectManager->create('Magento\Customer\Model\Session');
		if ($customerSession->isLoggedIn()) {
			$customerId = $customerSession->getCustomer()->getId();
		}else{
			$customerId = "";
			echo 'Not logged in';
		}
		
		//echo '<pre>';
		
		$product_id = $request->getParam('product_id');
		
		
		
		if(!empty($customerId)){
			
				$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
				$wishlistAdd = $wishList->create()->loadByCustomerId($customerId, true);
				$message ='';
			


				$_in_wishlist = false;
				foreach ($objectManager->get('Magento\Wishlist\Helper\Data')->getWishlistItemCollection() as $_wishlist_item){
					if($product_id == $_wishlist_item->getProduct()->getId()){
					$_in_wishlist = true;
					}
				}
				
				if($_in_wishlist){

					 //$wish = $this->wishlist->loadByCustomerId($customerId);
						$items = $objectManager->get('Magento\Wishlist\Helper\Data')->getWishlistItemCollection();
						foreach ($items as $item) 
						{
							if ($item->getProductId() == $product_id) 
							{
								$item->delete();
								$wishlistAdd->save();   
								$message ="This product has been removed from Wishlist";								
							}
						}

				}else{

					// Add if product is not in wishlist
					$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);

					$wishlistAdd->addNewItem($product);
					$wishlistAdd->save();
					
					$message = $product->getName()." is added to Wishlist";


                } 
             if(!empty($message)){
				 
				// get latest collection of product in wishlist 
				$wishlist = $objectManager->get('\Magento\Wishlist\Model\Wishlist');
				$wishlist_collection = $wishlist->loadByCustomerId($customerId, true)->getItemCollection();

				$i=0;
				foreach ($wishlist_collection as $item) {
					$i++;
				}
				//echo $message;
				echo $array=json_encode(array('count_num'=>$i, "message"=>$message));
				//$messageManager->addSuccess(__($message));
				return;
			 }
		}
	}
}