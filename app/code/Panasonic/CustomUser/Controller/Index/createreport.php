<?php
/* code refactored  @ritesh*/
namespace Panasonic\CustomUser\Controller\Index;
use Magento\Framework\App\Action\Context;
class Createreport extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory; 
	 protected $_checkoutSession;
	public function __construct(Context $context,
	 \Magento\Framework\View\Result\PageFactory $resultPageFactory 
	 )
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		$settlement_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\settlement'); 
		$buyer_helpers->check_customer();   
		$buyer_helpers->check_customer_deleted();    
		$razorpay_order_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpayorder');
		$gsttcs_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\gsttcs');
		$razorpay_order_helpers->update_quote_order();       
		// $api_helpers->forward_tracking();       
		         
		$today=date('Y-m-d');
		$yesterday=date('Y-m-d',strtotime("-1 days"));
		
		$select="select * from sales_order where DATE(created_at) in ('".$today."','".$yesterday."') OR DATE(updated_at) in ('".$today."','".$yesterday."') ";
		
		// $select = "select * from sales_order where increment_id in ('000004109')";
		// $select = "select * from sales_order where increment_id in ('000002739','000002753','000002755','000002766','000002770','000002784','000002832','000002842','000002846','000002867','000002905','000003441')";
		$results = $connection->fetchAll($select);
		
		if(!empty($results)) {
			foreach($results as $row) 
			{
				$order_id=$row['entity_id'];
				$order_status=$row['status'];
				if($order_status!='')
				{	
					$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
					$increment_id=$order->getIncrementId();
					
					$check = $tax_helpers->check_order_tax_list_exist($increment_id);
					// $check_sales_report=$sales_helpers->check_sales_report($increment_id);
					// $check_refund_sales_report = $sales_helpers->check_return_sales_report($increment_id);
					// $check_settlement=$settlement_helpers->check_settlement_report($increment_id);
					// $check_refund_settlement=$settlement_helpers->check_refund_settlement_report($increment_id);
					// $check_gst_tcs_report=$gsttcs_helpers->check_gst_tcs_report($increment_id);
					// $check_refund_gst_tcs_report=$gsttcs_helpers->check_return_gst_tcs_report($increment_id);
					$check_razorpay_report=$razorpay_helpers->check_razorpay_report($increment_id);
					
					if($check==0) {	
						$common_helpers->report_insert_tax_parameter($increment_id);
					}     
					$tax_helpers->success_page($order_id);  
					
					// if($check_sales_report==0) {
					// 	$sales_helpers->report_sale_report($increment_id);
					// }	

					// if($check_refund_sales_report==0) {	
					// 	$type = "7";  i have assgined this  status for credit memo @ritesh
					// 	$sales_helpers->report_sale_report($increment_id,$type);
					// }

					// if($check_settlement==0) {	
					// 	$settlement_helpers->create_settlement_report($increment_id);
					// }   
					// if($check_refund_gst_tcs_report==0) {
					// 	$type = "refund"; 
					// 	$gsttcs_helpers->report_sale_report($increment_id,$type);
					// }
					// if($check_refund_settlement==0) {
					// 	$type = "refund"; 
					// 	$settlement_helpers->create_settlement_report($increment_id,$type);
					// }
					// if($check_gst_tcs_report==0) {	
					// 	$gsttcs_helpers->report_sale_report($increment_id);    
					// }	 
					
					if($check_razorpay_report==0) {
						$razorpay_helpers->razorpay_report($increment_id); 
					}  	
				}
			} 
		}
		else {
			echo "No order today";	
		}	
		// run this here so  that report created  even if manifest fails
		$api_helpers->create_forward_manifest();

		//$api_helpers->customer_reset_token(); 
		//$api_helpers->automatic_update_user_token(); 
	} 	  
}
