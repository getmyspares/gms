<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Expensereport extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$delimiter = ",";
		$filename = "Expense_" . date('Y-m-d') . ".csv";
		 
		//create a file pointer
		$f = fopen('php://memory', 'w');
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
		
		echo '
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
		
		<script>
		$(document).ready( function () {
			$("#example").DataTable( {
				dom: "Bfrtip",
				buttons: [
					{ extend: "excel", text: "Export In Excel" }
				],
				"pageLength": 100
			} );
		} );
		</script>
		 
		<table border="1" style="width:100%" id="example"> 
		<thead>
		<tr>
			<th>Invoice Num</th>
			<th>Invoice Date</th>
			<th>Invoice Amount</th>
			<th>Seller Expense</th>
			<th>Shipping Expense</th>
			<th>Razor Pay</th>
			<th>Nodal</th>
			<th>Panasonic Comm</th>
			<th>GST</th>
		</tr>
		</thead>
		<tbody>
		'; 
		
		
		
		foreach($orderDatamodel as $orderDatamodel1){
			$order=$orderDatamodel1->getData();
			
			/* echo "<pre>";
			print_r($order);
			echo "</pre>";
			 */
			
			$order_idd=$order['entity_id'];
			$order_increment_id=$order['increment_id'];
			$order_status=$order['status'];
			$order_created_at=$order['created_at'];
			$order_created_at=date("d-m-Y", strtotime($order_created_at));
			$order_grand_total=$order['grand_total']; 
			$order_shipping_amount=$order['shipping_amount'];
			
			$invoice_id=$helpers->get_order_invoice_id($order_increment_id);	
			if($invoice_id!='0') 
			{
			$order_id=$order_increment_id;   
				
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_id);
			  
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
			$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
			
			$seller_comm=$custom_helpers->custom_seller_comission($order_increment_id);
			  
			
				
			//$shipping_charge=number_format($total_amount, 2, '.', '');	
			
			//$shipping_charge=$helpers->shipping_chanrge($orderid);	
				
			
			$razorpay_amount=$custom_helpers->razorpay_expense($order_increment_id);   
			if($razorpay_amount!='COD')
			{ 	
				$razorpay_amount=number_format($razorpay_amount, 2, '.', '');
			} 
			$noddle=$custom_helpers->custom_noddle_amount($order_increment_id);
			$noddle=number_format($noddle, 2, '.', '');
			
			$admin_commission=$custom_helpers->custom_admin_commission($order_increment_id);
			$admin_commission=number_format($admin_commission, 2, '.', '');
			
			$gst_amount=$custom_helpers->custom_gst_tax_expense($order_increment_id);
			$gst_amount=number_format($gst_amount, 2, '.', '');
			if($razorpay_amount!='COD'){
				$seller_comm = $seller_comm -($razorpay_amount+$noddle);
			}else{
				$seller_comm = $seller_comm -($noddle);
			} 
			 echo ' 
				<tr>
					<td>'.$invoice_id.'</td> 
					<td>'.$order_created_at.'</td>
					<td>'.$order_grand_total.'</td>
					<td>'.$seller_comm.'</td> 
					<td>'.$order_shipping_amount.'</td>
					<td>'.$razorpay_amount.'</td> 
					<td>'.$noddle.'</td>
					<td>'.$admin_commission.'</td>
					<td>'.$gst_amount.'</td>
				</tr> 
			'; 
			
			
			$mainarray[]=array(
				'invoice_id'=>$invoice_id,
				'order_created_at'=>$order_created_at,
				'order_grand_total'=>$order_grand_total,
				'seller_comm'=>($seller_comm),
				'order_shipping_amount'=>$order_shipping_amount,
				'razorpay_amount'=>$razorpay_amount,
				'noddle'=>$noddle,
				'admin_commission'=>$admin_commission,
				'gst_amount'=>$gst_amount
			);
			
			
			/* $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			if($invoice_id=='000000112')
			{
				$sql = "Insert Into expensereport (invoice_id, order_created_at, order_grand_total, seller_comm,order_shipping_amount,razorpay_amount,noddle,admin_commission,gst_amount) Values ('".$invoice_id."','".$order_created_at."','".$order_grand_total."','".$seller_comm."','".$order_shipping_amount."','".$razorpay_amount."','".$noddle."','".$admin_commission."','".$gst_amount."')"; 
				$connection->query($sql);
			} */
			
			
			} 
			//echo $order_id.' '.$order_increment_id.' '.$order_status.' '.$invoice_id.' '.$order_created_at"<br>";
			 
		}
		
		/* echo '
		</tbody>
		</html> 
		';  
		
		/* echo "<pre>";
			print_r($mainarray);
		echo "</pre>"; */
 
		  
		
		
		/* 		
		
		$fields = array('Invoice Num', 'Invoice Date', 'Invoice Amount', 'Seller Expense','Shipping Expense','Razor Pay','Nodal','Panasonic Comm','GST');
		fputcsv($f, $fields, $delimiter); 
		
		  
		
		
		foreach($mainarray as $row) 
		{
			
			$invoice_id=$row['invoice_id'];
			
			$order_created_at=$row['order_created_at'];
			$order_grand_total=$row['order_grand_total'];
			$seller_comm=$row['seller_comm'];
			$order_shipping_amount=$row['order_shipping_amount'];
			$razorpay_amount=$row['razorpay_amount'];
			$noddle=$row['noddle']; 
			$admin_commission=$row['admin_commission'];
			$gst_amount=$row['gst_amount'];
			
			
			$lineData = array($invoice_id, $order_created_at, $order_grand_total, $seller_comm,$order_shipping_amount,$razorpay_amount,$noddle,$admin_commission,$gst_amount);
			fputcsv($f, $lineData, $delimiter);	
			  
		}	
		
	
		fseek($f, 0);  
		
		//set headers to download file rather than displayed
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '";');
		
		//output all remaining data on a file pointer
		fpassthru($f);   
		*/
		   
		
	}  
	
	 
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}