<?php
 
namespace Panasonic\CustomUser\Controller\Index;
   
use Magento\Framework\App\Action\Context;   
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
class Checkforgexpire extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;   
	 private $customerRepository;
	 private $encryptor;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, CustomHelper $helper, CustomerRepositoryInterface $customerRepository, Encryptor $encryptor) 
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->helper = $helper;    
		$this->customerRepository = $customerRepository;
		 $this->encryptor = $encryptor;    
        parent::__construct($context);   
    }
 
    public function execute()  
    {   
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$today=date('Y-m-d H:i:s');
		
		$timeout=$_POST['timeout'];
		if($timeout==1)
		{
			$min=$_POST['min'];
			$sec=$_POST['sec'];
			
			
			$customerSession->setForgetExpireSessionMin($min);
			$customerSession->setForgetExpireSessionSec($sec); 
		}
		else
		{		
			$customerSession->unsForgetOtpPinExpire();       
			$customerSession->unsForgetcountdown();         
			$customerSession->unsForgetPinJson();         
			$customerSession->unsForgetcountdown();       
			
			$customerSession->unsForgetOtpTimerMin();           
			$customerSession->unsForgetOtpTimerSec();      
			  
			        
			$customerSession->setForgetOtpTimerSec('NaN');      
			
			
			$customerSession->unsForgetExpireSessionMin();         
			$customerSession->unsForgetExpireSessionSec();    
			       
			$customerSession->unsForgetResendbutton();		
			echo "1";           
		}
		   
		
	}      
	
	
	
	
}