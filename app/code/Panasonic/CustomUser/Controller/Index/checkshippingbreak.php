<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Checkshippingbreak extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		$request=$objectManager->get('\Magento\Framework\App\RequestInterface');
		
		/*
		$orderid=$request->getParam('orderid');

		$array=$common_helpers->report_insert_tax_parameter($orderid); 
		$array=json_decode($array);
		$arrays=(array)$array;
		unset($arrays['igst']);
		unset($arrays['cgst']);
		unset($arrays['sgst']);
		
 
		echo "Admin Comm=".$arrays['admin_comm'];
		*/
		/* echo "<pre>";
			print_r($arrays);
		echo "</pre>"; */ 
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$buyer_pincode=$request->getParam('buyer');
		$seller=$request->getParam('seller');
		
		
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		if(!$customerSession->isLoggedIn()) 
		{
			echo "Please login into the site";
			die;
			
		}
		
		
		$user_id=$customerSession->getCustomer()->getId();
		
		
		
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	 
		$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 


		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		//$address=$api_helpers->check_pincode_address_exists($user_id);
		
		$token=$api_helpers->get_token(); 
		$quote_id=json_decode($api_helpers->get_quote_id($token,$user_id));
		
		
		$cart_total=0;
		
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$allItems=$quote->getAllVisibleItems();
		
		if(empty($allItems))
		{
			echo "Cart empty";
			die;	
		}	
		
		$mainArrayy=array();
		
		foreach ($allItems as $item) 
		{
		//item id of particular item
		$itemId = $item->getItemId();
		$productId = $item->getProductId();
		$qty=$item->getQty();
		
		
			$mainArrayy[]=array(
				'item_id'=>$itemId,
				'product_id'=>$productId,
				'qty'=>$qty
			);		
			
			
			
			
		}
		
		$shippingAmount=0; 
		$total_shippingAmount=0;
		$special_price=0;
		$sale=0;
		$discountAmount=0;
		$total_gst=0;
		$total_price=0;
		if(!empty($mainArrayy))
		{
			foreach($mainArrayy as $row) 
			{
				$id = $row['product_id'];
				$qty= $row['qty'];
				$item_id= $row['item_id']; 
				
				$priduct_check=$api_helpers->check_product_exist($id);
				if($priduct_check=='')
				{
					$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product of '.$id.' not found','quote_id'=>$quote_id);	
					echo json_encode($result_array);	  
					die;  
				}	 
				
				$product = $productRepository->getById($id); 
				$check_price=$api_helpers->get_product_price($id);
				$orignal_price=$product->getPrice();	 
				
				/*
				if($check_price!=0)
				{
					$special_price=$check_price;	
					$sale = round((($orignal_price - $special_price)/$orignal_price) * 100);
				}
				$orignal_price=number_format($orignal_price, 2, '.', '');
				$special_price=number_format($special_price, 2, '.', '');
				*/
				
				$orignal_price=$product->getPrice();	 
				$special_price=$product->getSpecialPrice();	 
				
				$product_iddd=$product->getId();
				$is_special=0; 
				$price=$api_helpers->get_product_prices($product_iddd);		
				$orgprice = $api_helpers->get_product_prices($product_iddd);
				$specialprice = $api_helpers->get_product_special_prices($product_iddd);
				$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
				$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
				$today = time(); 
				
				if (!$specialprice)
				{	
					$specialprice = $orgprice;
				}
				else
				{
					if(!is_null($specialfromdate) && !is_null($specialtodate))
					{
						$specialfromdate=strtotime($specialfromdate);
						$specialtodate=strtotime($specialtodate);
						
						if($today >= $specialfromdate &&  $today <= $specialtodate)
						{
							$is_special=1;			
						}	
					}
				}
				
				$sale=0;
				
				if($is_special==1)
				{
					
					$orignal_price=number_format($product->getPrice(), 2, '.', '');
					$special_price=number_format($product->getSpecialPrice(), 2, '.', '');
					$sale = round((($orignal_price - $special_price)/$orignal_price) * 100);
					$final_price=$special_price;
				}   	
				else
				{
					$special_price=0;
					$orignal_price= number_format($product->getPrice(), 2, '.', '');
					$special_price = number_format($product->getSpecialPrice(), 2, '.', '');
					$final_price=$orignal_price;
					
					
				}	
				
				
				$seller_id=$api_helpers->get_product_seller_id_mobile($id);
				
				$sellerarray=$custom_helpers->get_seller_details($seller_id);	
				
				
				$mainArray[]=array(
					'id'=>$id,		
					'qty'=>$qty,		 
					'item_id'=>$item_id,		 
					'product_name'=>$product->getName(),		
					'product_image'=>$_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl(),	
					'orignal_price'=>$orignal_price,	
					'special_price'=>$special_price,	
					'weight'=>$product->getProductWeight(), 
					'seller_pincode'=>$sellerarray['zipcode'],	 
					
				);   
				
				//Order Summary
				
				$check_price=$api_helpers->get_product_price($id);
				
				if($check_price==0)
				{
					$price=$product->getPrice() * $qty;	
				}
				else
				{
					$price=$check_price * $qty;			
				}	 	
				
				

				$shippingGstRate = $product->getGstRate();

				$gstPercent = 100 + $shippingGstRate;
				
				
				
				
				
				$rowTotal = $price;	 

				$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
						
				$gstAmount = $productPrice * $shippingGstRate;
				$total_gst = $total_gst + $gstAmount;
				$total_price=$total_price + $rowTotal;
				
				$pincode=$buyer_pincode;
				$shippingAmount=$data_helpers->ecom_price_by_product_test($pincode,$id,$qty); 
				$total_shippingAmount=$total_shippingAmount+$shippingAmount;
				
				
				
			}
			

		  

			


		 $total_shippingAmount;
			  


			
		$user_array=$mainArray; 
		$total_base_amount=$total_price - $total_gst;

		// echo $total_gst.' '.$total_price.' '.$total_base_amount;
		// die();



		 

		$total_gst=number_format($total_gst, 2, '.', '');
		$total_base_amount=number_format($total_base_amount, 2, '.', '');
		  
		$subtotal=$total_gst + $total_base_amount;
		$total_shippingAmount=number_format($total_shippingAmount, 2, '.', '');

		$order_total=$subtotal + $total_shippingAmount;

		$total_gst.' '.$total_base_amount.' '.$subtotal.' '.$order_total;	

		$subtotal=number_format($subtotal, 2, '.', '');
		$order_total=number_format($order_total, 2, '.', '');
		 
		$summary_array=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total);


		$cart_total=0;
				
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$quoteItems=$quote->getAllVisibleItems();
		$items = $quote->getAllItems();
		if(!empty($items))
		{	
			foreach($items as $item)   
			{
				$product_id=$item->getProductId();
				$qty=$item->getQty(); 
				$cart_total=$cart_total+$qty;
			}		
		} 

		

		$result_array=array('cart' => $user_array,'summary'=> $summary_array); 	

		

		}
		
		
		 
		
		echo "<pre>";
			print_r($result_array);
		echo "</pre>";
		//ecom_price_by_product($buyer_zipcode,$product_id,$qty);
		
		
		 
		
		
		
		
		  
		
	}  
	
	
	
	
	
}