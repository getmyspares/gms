<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Checkoutsummaryaftershipping extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
         
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		
		$customerSession = $objectManager->create('Magento\Customer\Model\Session');
		
		
		$customer_id=@$_POST['customer_id'];
		$pincode=@$_POST['pincode']; 
		$customerSession->setNewPostcode($pincode);
		 
		//$customer_id='31'; 
		//$pincode='400709';
		
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			
			$customerSession = $objectManager->get('Magento\Customer\Model\Session'); 
			
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			$itemsCollection = $cart->getQuote()->getItemsCollection();
			 
			// get array of all items what can be display directly
			$itemsVisible = $cart->getQuote()->getAllVisibleItems();
			 $items = $cart->getQuote()->getAllItems();
			// get quote items array
			$total_gst=0;
		foreach ($items as $item) {
            if ($item->getId()) {
                /*
				$product = $this->productFactory->create()->load($item->getProductId());
                if ($product->getTypeId() == 'configurable') {
                    $product = $this->productRepository->get($item->getSku());
                    $productPrice = $product->getFinalPrice();
                } else {
                    $productPrice = $product->getFinalPrice();
                }
                $categoryIds = $product->getCategoryIds();
                $productPriceAfterDiscount = ($productPrice * $product->getDiscountPercent()) / 100;
                $productPrice = $productPrice - $productPriceAfterDiscount;
                $flag = false;
                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $categoryId) {
                        $category = $this->categoryRepository->get($categoryId, $this->storeManager->getStore()->getId());
                        $shippingGstRate = $category->getCatGstRate();
                        $gstRateMinAmount = $category->getCatMinGstAmount();
                        $gstMinRate = $category->getCatMinGstRate();
                        if ($shippingGstRate) {
                            $gstMinRate = $category->getCatMinGstRate();
                            if ($category->getCatGstRate()) {
                                $flag = true;
                            }
                            break;
                        }
                    }
                }
				*/
				
				
				$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
				$custom_helpers->store_customer_selected_pincode($pincode);	
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
				
				$shippingGstRate = $product->getGstRate();
				
				
				$gstPercent = 100 + $shippingGstRate;
				$rowTotal = $item->getRowTotal();
                $discountAmount = $item->getDiscountAmount();
				
				
                $productPrice = ($rowTotal - $discountAmount) / $gstPercent;
                $gstAmount = $productPrice * $shippingGstRate;
				
				//$gstAmount = ((($rowTotal - $discountAmount) * $shippingGstRate) / 100);
				
				 
				$total_gst = $total_gst + $gstAmount;
				
            }
        }
			
			
			$subTotal = $cart->getQuote()->getSubtotal();
			$grandTotal = $cart->getQuote()->getGrandTotal(); 
			
			
			
			$cartarray=(array)$itemsCollection->getData();
			
			/* echo "<pre>";
				print_r($cartarray);
			echo "</pre>";   */
			
			$tax_amount=0;
			$shippingAmountt=0;
			if(!empty($cartarray)) 
			{
			
				$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
            
            foreach($cartarray as $row)
            {
                $tax_amount=$tax_amount + $row['tax_amount'];        
                
                
                
                
                
                if($customerSession->isLoggedIn()) {
                
                    $customer_id=$customerSession->getCustomerId();
                    
					
					
                    //$pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
                    //$shippingAmount=$data_helpers->ecom_price($pincode);
                
                    $product_id=$row['product_id'];
                    $qty=$row['qty'];
                    
                    
                    $shippingAmount=$data_helpers->ecom_price_by_product($pincode,$product_id,$qty);
                    $shippingAmountt=$shippingAmountt+$shippingAmount;
                
                }
                
                
                
            }    
            
            
            $shippingAmountt;
            
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            //$customerSession = $objectManager->create('Magento\Customer\Model\Session');
            //$shippingAmount = $customerSession->getcheckoutshippingSession();
            //$shippingAmountt = $customerSession->getcheckoutshippingSession();
            
            if($customerSession->isLoggedIn())
            {
				$shippingAmount=$shippingAmountt;
				$customer_id=$customerSession->getCustomerId();
				$customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customer_id);
				$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
				$quote_date = $cart->getQuote();
				if(count($customer->getAddresses()) <= 0)
				{
					$quote_shipping_adddress =  $quote_date->getShippingAddress()->getData();
					
					if(count($quote_shipping_adddress))
					{	
						$first_name = $quote_shipping_adddress['firstname'];
						$lastname = $quote_shipping_adddress['lastname'];
						$street = $quote_shipping_adddress['street'];
						$city = $quote_shipping_adddress['city'];
						$postcode = $quote_shipping_adddress['postcode'];
						$telephone = $quote_shipping_adddress['telephone'];
						$company = $quote_shipping_adddress['company'];
						$regionId = $quote_shipping_adddress['region'];
						$regionName = $quote_shipping_adddress['region_id'];
						$addresss = $objectManager->get('\Magento\Customer\Model\AddressFactory');
						$address = $addresss->create();
						$address->setCustomerId($customer_id)
						->setFirstname($first_name)
						->setLastname($lastname)
						->setCountryId('IN')
						->setRegionId($regionId)
						->setRegion($regionName)
						->setPostcode($postcode)
						->setCity($city)
						->setTelephone($telephone)
						->setCompany($company)
						->setStreet($street)
						->setIsDefaultBilling('1')
						->setIsDefaultShipping('1')
						->setSaveInAddressBook('1');
						if(!empty($regionId) && !empty($postcode) && !empty($telephone))
						{
							$address->save();
						}
						
					}
					
				}
            }
             
              
                
            
            
            $grandTotal=$subTotal+$total_gst;
            
            $order_total=$grandTotal + $shippingAmount;
            
            
             
              
            $shipping=number_format($shippingAmount, 2, '.', '');
            
            $base_price= $subTotal-$total_gst;
            
            
            $order_total=$subTotal + $shipping; 
				  
				 
				$cart_summary= ' 
				<div class="cart-summary">
				<div class="price_details">
					<h2>Summary</h2>
				</div>
				<!--
				<div class="text_section">        
					<i class="fa fa-exclamation" aria-hidden="true"></i> Processing charge of ₹ 50 will be applicable on COD
				</div>
				--> 
				<div class="cart-totals">
					<table>
						<tbody>	
							<tr>
								<td>Base Price</td>
								<td> &#8377; '.number_format($base_price, 2, '.', '').'</td>
							</tr>
							<tr>
								<td>GST</td>
								<td> &#8377; '.number_format($total_gst, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Sub Total</td>
								<td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Shipping and Handling</td>
								<td>&#8377; '.$shipping.'</td>
							</tr>	 
							<tr class="total_row"> 
								<td>Order Total</td>
								<td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
							</tr>
							<tr style="display:none;">
								<td>&nbsp;</td>
								<td class="act_cart_total">'.number_format($order_total, 2, '.', '').'</td>
							</tr>
							 
							
						</tbody>  
					</table>
					</div>
					</div>
			';	  
			}   
			else 
			{
				$cart_summary='<p>No Item In Cart</p>';
			}
			
			echo $cart_summary;
			
		 
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}