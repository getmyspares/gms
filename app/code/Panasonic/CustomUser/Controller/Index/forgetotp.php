<?php
 
namespace Panasonic\CustomUser\Controller\Index;
   
use Magento\Framework\App\Action\Context;   
use Panasonic\CustomUser\Helper\Data as CustomHelper; 

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;

class Forgetotp extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;   
	protected $resultFactory;
    protected $url;
 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, CustomHelper $helper,
		UrlInterface $url,
        ResultFactory $resultFactory
	) 
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->helper = $helper;    
		$this->resultFactory = $resultFactory;
		$this->url = $url;
        parent::__construct($context);
    }
 
    public function execute()
    {                
        $otp=$_POST['forget_otp'];  
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$forgetpassword=$customerSession->getForgetPinJson();
		 
		$forgetarray=json_decode($forgetpassword);
		$pin=$forgetarray->pin;
		

		$minutes = (time() - strtotime($forgetarray->cr_time)) / 60;
		$minutes = round($minutes);
		
				
		if($minutes > 2)  
		{   
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account/forgotpassword/'));
		 
			$this->messageManager->addError(__('Otp pin has been expired'));
			//$customerSession->setForgetcountdown(1); 	
			return $resultRedirect;    
			
			
		}
		else if($otp!=$pin)
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account/forgotpassword/'));
		 
			$this->messageManager->addError(__('Otp pin did not matched'));
			//$customerSession->setForgetcountdown(1); 	
			return $resultRedirect;    
		} 
		else
		{
			$customerSession->setForgetOtpCheck(1); 
			$customerSession->getForgetOtpCheck();
			
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account/forgotpassword/'));
		  
			//$this->messageManager->addSuccess(__('Otp pin matched'));
			return $resultRedirect;      
			  
			
		} 
		     
    }
	
	
	
	
}