<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Updateproductmodel extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		
		
		$csvFile = 'seller_prd_example.csv';

		$csv = $this->readCSV($csvFile);
		$resultarray=array_filter($csv);
		
		/* echo "<pre>";
		print_r($resultarray); 
		echo "</pre>";  
		die; */
		
		if(!empty($csv))
		{ 
			
			$i=0;
			foreach($resultarray as $row)
			{
				if($i!=0)	
				{ 
						
					$sku=$row['4'];
					$product = $productRepository->get($sku);

					$product_id=$product->getEntityId();
					
						
					
					$pi_model=$helpers->product_pi_model($product_id); 
					$model=$helpers->product_model($product_id); 
					  
					  
					//$pi_model='Testing Model';  
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					$pi_model;
					
					$check_model=$helpers->model_already_exist($pi_model);
					
					 

					if($model==0)  
					{ 
						if($pi_model!='')
						{
							//echo $pi_model;   		
							
							$sql = "Select * FROM eav_attribute_option order by sort_order desc limit 0,1";
							$result = $connection->fetchAll($sql);
							$sort_order=$result[0]['sort_order'];
							$sort_order=$sort_order+1;	

							if($check_model==0)			
							{
								
								$sql = "Insert Into  eav_attribute_option (attribute_id, sort_order) Values ('167','".$sort_order."')";
								$connection->query($sql);
								$insert_id=$connection->lastInsertId();

								$sqls = "Insert Into  eav_attribute_option_value (option_id,store_id, value) Values ('".$insert_id."','0','".$pi_model."')";
								$connection->query($sqls);
								
							}
							else
							{
								
								$sql = "Select * FROM eav_attribute_option_value where value='".$pi_model."'";
								$result = $connection->fetchAll($sql);
								$insert_id=$result[0]['option_id']; 
							}
							
							
							
							$sqls = "Delete from catalog_product_entity_int where attribute_id='167' and entity_id='".$product_id."'";
							$connection->query($sqls); 
							
							
							$sqls = "Insert Into  catalog_product_entity_int (attribute_id,store_id,entity_id,value) Values ('167','0','".$product_id."','".$insert_id."')";
							$connection->query($sqls);
						} 	 
						
					 
					}
					else  
					{
						echo $product_id." - Model already assigned";
					} 
						
					
				}
				 
			$i++;
			}		
		}
		
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}