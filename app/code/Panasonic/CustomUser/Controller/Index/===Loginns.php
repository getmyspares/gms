<?php
namespace Panasonic\CustomUser\Controller\Index;

class Loginns extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		
		 $data = $this->getRequest()->getParams();
		$firstKey = $this->array_key_first($data);
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$customer_id =  $firstKey; 
		  
		
		$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($customer_id);
		$customerSession = $objectManager->create('Magento\Customer\Model\Session');
		$customerSession->setCustomerAsLoggedIn($customer);
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_urls=$storeManager->getStore()->getBaseUrl();
		$site_url=$site_urls; 
		
		$resultRedirect = $this->resultRedirectFactory->create();
    $route = 'customer/account'; // w/o leading '/'
     $url = 'https://dev.idsil.com/'.$route;
   
    $resultRedirect->setUrl($url);
    return $resultRedirect; 
	}   
	
	public  function array_key_first(array $array)
    {
        if (count($array)) {
            reset($array);
            return key($array);
        }

        return null;
    }
}  