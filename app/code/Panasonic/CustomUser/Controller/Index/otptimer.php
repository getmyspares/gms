<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Otptimer extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
        parent::__construct($context);
    }
 
    public function execute()
    {
       
        $timeover=$_POST['timeover'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
		$customerSession->unsRegister_resend();  
		
		$customerSession->unsRegister_allwell(); 	
		 
		if($timeover==0)
		{
			$customerSession->unsOtpPinExpire();       
			$customerSession->unscountdown();       
		}     
		else
		{
			$min=$_POST['min'];
			$sec=$_POST['sec'];
			
			
			$customerSession->setOtpTimerMin($min);
			$customerSession->setOtpTimerSec($sec); 
		}  
		
		
	}           
	
	
	
	
}