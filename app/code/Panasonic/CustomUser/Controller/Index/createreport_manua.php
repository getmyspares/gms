<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Createreport extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory; 
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 

		$site_url=$storeManager->getStore()->getBaseUrl();

		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');

		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');

		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface'); 
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
		$settlement_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\settlement'); 
		$buyer_helpers->check_customer();   
		$buyer_helpers->check_customer_deleted();    
		     
		
		$razorpay_order_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpayorder');
		
		$gsttcs_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\gsttcs');
		
		$razorpay_order_helpers->update_quote_order();       
		$api_helpers->forward_tracking();       
		$api_helpers->create_forward_manifest();         
		    

		   
		  
		$today=date('Y-m-d');
		 
		//$today=date('2019-12-17');
		$yesterday=date('Y-m-d',strtotime("-1 days"));
		// $today='2020-06-20';

		
		//$select="select * from sales_order where DATE(created_at) in ('".$today."','".$yesterday."')";
		$increment_ids="000000903,000000904,000000905,000000906,000000907,000000908";
		// $increment_ids="000000886,000000887";

		// $select="select * from sales_order where DATE(created_at) in ('".$today."','".$yesterday."')";
		$select="select * from sales_order where increment_id IN($increment_ids)";
		
		// $select="select * from sales_order";  
		$results = $connection->fetchAll($select);
		if(!empty($results)) {
		  
			
		// foreach($results as $row) 
		// {	
		// 	$order_id=$row['entity_id'];
		// 	$order_status=$row['status'];
		// 	if($order_status!=''){
		// 		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
		// 		$increment_id=$order->getIncrementId();
		// 		$gsttcs_helpers->report_sale_report($increment_id);
		// 	}
		// }	
		foreach($results as $row) 
		{
			
			/* echo "<pre>";
			print_r($results);
			echo "</pre>"; */
			
			
			$order_id=$row['entity_id'];
			$order_status=$row['status'];
			if($order_status!='')
			{	
				$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
				echo $increment_id=$order->getIncrementId();
				echo "<br>";
				
				
/* 				$check=$tax_helpers->check_order_tax_list_exist($increment_id);
				if($check==0)  
				{	
					$common_helpers->report_insert_tax_parameter($increment_id);   
				}     

				$tax_helpers->success_page($order_id);  
				 */
					 
				
				$check_sales_report=$sales_helpers->check_sales_report($increment_id);
				if($check_sales_report==0)
				{
					$sales_helpers->report_sale_report($increment_id);
				}	
				 
				
			/* 	$check_settlement=$settlement_helpers->check_settlement_report($increment_id);
				if($check_settlement==0)   
				{	
					$settlement_helpers->create_settlement_report($increment_id);
				}   
				
				  
				
				$check_gst_tcs_report=$gsttcs_helpers->check_gst_tcs_report($increment_id);
				if($check_gst_tcs_report==0)   
				{	
					$gsttcs_helpers->report_sale_report($increment_id);    
				}	 
				
				
				$check_razorpay_report=$razorpay_helpers->check_razorpay_report($increment_id);
				if($check_razorpay_report==0) 
				{
					$razorpay_helpers->razorpay_report($increment_id); 
				}  */	
				 
				
			
			}
				
		} 
		}
		else
		{
			echo "No order today";	
		}		
		   
		//$api_helpers->customer_reset_token(); 
		//$api_helpers->automatic_update_user_token(); 
			
		
		
	} 	  
	 
	 
}
