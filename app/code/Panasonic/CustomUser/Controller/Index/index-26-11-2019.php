<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		
	 	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 

		$site_url=$storeManager->getStore()->getBaseUrl();

		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');

		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 


		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
				
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');
		
		
		$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
		$inventory_helpers->insert_inventory_master_report('16430');

		
		
		
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		$buyer_helpers->get_buyer_reports(); 
		 
		
		
		$today = date("Y-m-d"); 
		$from = date("Y-m-d h:i:s"); 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
		
		
		
		foreach($orderDatamodel as $orderDatamodel1){
			$orderArray=$orderDatamodel1->getData();
			$created_at=$orderArray['created_at'];
			
			$created_at=date("Y-m-d", strtotime($created_at)); 
			if($created_at==$today)
			{	
				$order_id=$orderArray['entity_id'];
				$increment_id=$orderArray['increment_id'];
				//echo "<br>";
				$tax_helpers->success_page($order_id);  	 
				 
				$check_razorpay_report=$razorpay_helpers->check_razorpay_report($increment_id);
				if($check_razorpay_report==0) 
				{
					$razorpay_helpers->razorpay_report($increment_id); 
				}  
				
			}		 
			
		}	
		
		
		
		//$tax_helpers->taxation_report_mini('000000516');	   
		//$tax_helpers->sales_report_new('000000516');	   
		   
		/*
		foreach($orderDatamodel as $orderDatamodel1){
			$orderArray=$orderDatamodel1->getData();
			$created_at=$orderArray['created_at'];
			
			$created_at=date("Y-m-d", strtotime($created_at)); 
			
			if($created_at==$today)
			{	
				//print_r($orderArray);
				$order_id=$orderArray['entity_id']; 
				$increment_id=$orderArray['increment_id'];
				
				
				$tax_helpers->success_page($order_id);	 
				
				
				
				echo $increment_id;
				echo "<br>";
			}    
		*/	
			
				/*
				$order_id=$orderArray['entity_id']; 
				$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
				$payment_method=$order->getPayment()->getMethod();
				
				$tax_helpers->success_page($order_id); 
				echo $order->getIncrementId();
				echo "<br>";  
				*/
				/*
				if($payment_method=='cashondelivery')
				{
						
					$tax_helpers->success_page($order_id); 
					echo $order->getIncrementId();
					echo "<br>";
				}  
				*/
				 
				
				/*
				$check=$tax_helpers->check_taxation_report_mini_exist('000000509');
				
				if($check=='0')
				{	 
					$tax_helpers->taxation_report_mini('000000509'); 
				}   
				*/	    
			 
		//}
		
		
		/*
		foreach($orderDatamodel as $orderDatamodel1){
			$orderArray=$orderDatamodel1->getData();
			
			$order_id=$orderArray['entity_id']; 
			$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
		*/	
			/*
			$order_inc_id=$order->getIncrementId();
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection(); 
				
				foreach ($order->getInvoiceCollection() as $invoice) 
				{
                    $invoiceIncrementID = $invoice->getIncrementId();
                    $invoice_id = $invoice->getId();
				}	 
			
			echo $update = "update marketplace_orders set invoice_id='".$invoice_id."' where order_id='".$order_id."'";	
			$connection->query($update);
			echo "<br>";    
			*/
			/*
			$invoice_id=$data_helpers->get_order_invoice_id($order_inc_id); 
			$invoice_id = ltrim($invoice_id, '0'); 
			echo $update = "update marketplace_orders set invoice_id='".$invoice_id."' where order_id='".$order_id."'";
			$connection->query($update);	   
			echo "<br>";   
			
			*/ 
			
			
			
			
		
		//}
		
		  
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}