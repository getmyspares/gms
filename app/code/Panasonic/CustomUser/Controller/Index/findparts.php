<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Findparts extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager; 
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$resultPage = $this->_resultPageFactory->create();
		 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\Findparts')
                ->setTemplate('Panasonic_CustomUser::findparts.phtml')
                ->toHtml(); 
        $this->getResponse()->setBody($block);   
			 
	 
	}
	
}