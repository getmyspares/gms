<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Updateqty extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		//$productId=$_POST['prod_id'];
		$qty=$_POST['qty'];
		$productId = $_POST['prod_id'];  
        
		/*
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product = $objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
		$cart = $objectManager->create('Magento\Checkout\Model\Cart');  
		$formKey = $objectManager->create('\Magento\Framework\Data\Form\FormKey')->getFormKey();  
		$params = array(
							'form_key' => $formKey,
							'product' => $productId, //product Id
							'qty'   =>0, //quantity of product                
							  
							); 
		$cart->addProduct($product, $params);
		$cart->save();
		
		*/
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
        $cartId=$cart->getQuote()->getId();

        $itemId = $productId;
        $itemQty = $qty; 

        $quote = $objectManager->get('\Magento\Quote\Api\CartRepositoryInterface'); 
        $quote->getActive($cartId);
        $cartitems = $cart->getQuote()->getAllItems();
        $cartitems->setquoteId($cartId);
        $cartitems->setitemId($itemId);
        $cartitems->setqty($itemQty);

        $quoteItems[] = $cartitems;
        $quote->setItems($quoteItems);
        $this->quoteRepository->save($quote);
        $quote->collectTotals();
		 
		
		
		echo "done";
		
		
		
		
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}