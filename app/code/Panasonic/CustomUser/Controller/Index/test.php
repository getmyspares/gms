<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Test extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        /*
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		
		echo ' 
			<a href="'.$site_url.'report.php?id=2">App</a>
			<br>
			<a href="'.$site_url.'report.php?id=1">Reports</a>
			<br>
			<a href="'.$site_url.'report.php?id=3">Site</a>
		';
		
		*/
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 

		$site_url=$storeManager->getStore()->getBaseUrl();

		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');

		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 
		$razorpay_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\razorpay');

		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
		
		
		$buyer_helpers->get_buyer_reports(); 
		
		$from_date=date('Y-m-d');
		$to_date=date('Y-m-d');
		
		//$from_date=date('Y-m-d');
		$from_date=date('2019-12-19'); 
		//$to_date=date('2019-12-16');
		
		
		$now = new \DateTime();
		$OrderFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
		$orderCollection = $OrderFactory->create()->addFieldToSelect(array('*'));
	//	$orderCollection->addFieldToFilter('created_at', ['lteq' => $today])->addFieldToFilter('created_at', ['gteq' => $today]);
		
		//$orderCollection->addFieldToFilter('created_at', ['lteq' => $now->format('2019-12-04 H:i:s')]);
		
		$orderCollection->addAttributeToFilter('created_at', array('from'=>$from_date, 'to'=>$to_date));
		$orderDatamodel=$orderCollection->getData();
		
		//$today=date('Y-m-d');
		$today=date('2019-12-17');
		$yesterday=date('Y-m-d',strtotime("-1 days"));
		
		$select="select * from sales_order where DATE(created_at) in ('".$today."','".$yesterday."')";
		//$select="select * from sales_order where DATE(created_at) in ('".$today."')";
		 
		$results = $connection->fetchAll($select);
		
		//print_r($results);
		//die;
					
		foreach($results as $row) 
		{
			
				$order_id=$row['entity_id'];
				$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
				echo $increment_id=$order->getIncrementId();
				echo "<br>";
				  
				 
				$tax_helpers->success_page($order_id);  	 
				
				
				$check_sales_report=$sales_helpers->check_sales_report($increment_id);
				if($check_sales_report==0)
				{
					$sales_helpers->report_sale_report($increment_id);
				}	
				
				$check_razorpay_report=$razorpay_helpers->check_razorpay_report($increment_id);
				if($check_razorpay_report==0) 
				{
					$razorpay_helpers->razorpay_report($increment_id); 
				}    
		}
	
	} 	
	
	
}