<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Forgetotptimer extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
        parent::__construct($context);
    }
 
    public function execute()
    {
       
       
		$timeover=$_POST['timeover'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
		$customerSession->unsForgetperson();   
		   
		if($timeover==0)
		{
			$customerSession->unsForgetOtpPinExpire();       
			$customerSession->unsForgetcountdown();     
			$customerSession->setForgetResendbutton(1);	
		}             
		 
		
		 
	}           
	
	
	
	
}