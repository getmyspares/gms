<?php
 
namespace Panasonic\CustomUser\Controller\Index;
  
use Magento\Framework\App\Action\Context;
     
class Getcustomaccountedit extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory; 
	public $_storeManager;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
        parent::__construct($context);
    }
 
    public function execute()
    {
		
		/*
		$resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__(' heading '));
 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\Display')
                ->setTemplate('Panasonic_CustomUser::sayhello.phtml')
                ->toHtml();    
        $this->getResponse()->setBody($block);    
		*/

		$resultPage = $this->_resultPageFactory->create();
		 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\Changecompany')
                ->setTemplate('Panasonic_CustomUser::changecompany.phtml')
                ->toHtml(); 
        $this->getResponse()->setBody($block); 


		
		
	}         
	
	
	
	
}