<?php
 
namespace Panasonic\CustomUser\Controller\Index;
   
use Magento\Framework\App\Action\Context;   
use Panasonic\CustomUser\Helper\Data as CustomHelper; 

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;

class Resendotp extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;   
	
	protected $resultFactory;
    protected $url;
 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, CustomHelper $helper,
	UrlInterface $url,
    ResultFactory $resultFactory
	) 
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->helper = $helper;    
		
		$this->resultFactory = $resultFactory;
		$this->url = $url;
		
        parent::__construct($context);
    }
 
    public function execute()
    {   
        /* $resultPage = $this->_resultPageFactory->create();
        return $resultPage; */
		
		/* $resultPage = $this->_resultPageFactory->create();
		 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\Resendotp')
                ->setTemplate('Panasonic_CustomUser::resendotp.phtml')
                ->toHtml();     
         $this->getResponse()->setBody($block);       */

		$phone=$_POST['user_phone'];
		$email=$_POST['user_email'];  
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
		$customerData=$customerSession->getCustomerRegData(); //Get value from customer session 
		$pincode=$customerSession->getPinValue();   
		
		
		$UserPinPhoneJson=$customerSession->getUserPinPhoneJson();
		$UserPinPhone=json_decode($UserPinPhoneJson);
		
		
		$resendcount=$customerSession->getRegisterresendcount();   
		if($resendcount=='')    
		{
			$customerSession->setRegisterresendcount(1);  	
		}
		else
		{
			$resendcount=$resendcount+1;
			$customerSession->setRegisterresendcount($resendcount);  	
		} 	 
		   
		 
		if($resendcount > 2)   
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customuser/index/resetregisterform'));
			
			return $resultRedirect;    
			die; 
		}        
		
		
		
		
		
		$customerSession->setOTPSent(1);         
		$customerSession->setcountdown(1);       
		
		$customerSession->unsOtpTimerMin();
		$customerSession->unsOtpTimerSec();
		 
		  
		//print_r($UserPinPhone);   
		  
		
			$today=date('Y-m-d H:i:s');		
			$pin = mt_rand(1000, 9999);		
			$completeUser=array(
				'pin'=>$pin,	
				'phone'=>$phone,	
				'cr_time'=>$today	
			);
				
				
			$userPinPhoneJson=json_encode($completeUser);
			    
			
			$customerSession->setUserPinPhoneJson($userPinPhoneJson); //set value in customer session
			$customerSession->getUserPinPhoneJson(); //Get value from customer session 
			
			$customerSession->setPinValue($pin); //set value in customer session
			$customerSession->getPinValue(); //Get value from customer session
		  
			$customerSession->setRegister_resend(1);        
			
			$customerSession->setResendbtndbgya(1);  
			   
			$this->helper->CustomResendOTP($pin,$phone,$email);       
			
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account/create/'));
		 
			$this->messageManager->addSuccess(__('Otp pin Sent'));
			  
			return $resultRedirect;


		 
		
		     
    }
	
	
	
	
}