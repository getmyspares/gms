<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Checkoutcustomershipping extends \Magento\Framework\App\Action\Action
{ 
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
          
		$resultPage = $this->_resultPageFactory->create();
		 
		$block = $resultPage->getLayout()
                ->createBlock('Panasonic\CustomUser\Block\Checkoutcustomershipping')
                ->setTemplate('Panasonic_CustomUser::checkoutcustomershipping.phtml')
                ->toHtml(); 
        $this->getResponse()->setBody($block);         
		  
	}    
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}