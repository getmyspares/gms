<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Checkoutsummarypayment extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory; 
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
   
    public function execute()
    {
        
		$payment=$_POST['payment'];
		
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl(); 
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		// get quote items collection
		$itemsCollection = $cart->getQuote()->getItemsCollection(); 
		 
		// get array of all items what can be display directly
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		 
		// get quote items array
		$items = $cart->getQuote()->getAllItems();
		 
		/* foreach($items as $item) {
			echo 'ID: '.$item->getProductId().'<br />';
			echo 'Name: '.$item->getName().'<br />';
			echo 'Sku: '.$item->getSku().'<br />';
			echo 'Quantity: '.$item->getQty().'<br />';
			echo 'Price: '.$item->getPrice().'<br />';
			echo "<br />";       

			
			
		}  */
		
		
		$subTotal = $cart->getQuote()->getSubtotal();
		$grandTotal = $cart->getQuote()->getGrandTotal(); 
		
		
		$cartarray=(array)$itemsCollection->getData();
		
	/* 	echo "<pre>";
			print_r($cartarray);
		echo "</pre>";   */
		
		$tax_amount=0;
		
		if(!empty($cartarray)) 
		{
		
			foreach($cartarray as $row)
			{
				$tax_amount=$tax_amount + $row['tax_amount'];		
			}	
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();

				$customerSession = $objectManager->create('Magento\Customer\Model\Session');
				$shippingAmount = $customerSession->getcheckoutshippingSession();

				
			$grandTotal=$subTotal+$tax_amount;
			
			
			$cod_amount=0;
			if($payment=='cod')
			{
				$cod_amount=50; 	
			}	
			
			$order_total=$grandTotal + $shippingAmount + $cod_amount;
			
			
			if($customerSession->isLoggedIn()) {
				
				$customer_id=$customerSession->getCustomerId();
				
				$pincode=$custom_helpers->get_customer_shipping_address($customer_id,'postcode');
				$shippingAmount=$data_helpers->ecom_price($pincode);
			}
			 
			  
			$shipping=number_format($shippingAmount, 2, '.', ''); 
			
			if($payment=='cod')
			{
				echo '
				<div class="cart-summary">
				<div class="price_details">
					<h2>Summary</h2>
				</div>
				<div class="text_section">        
					<i class="fa fa-exclamation" aria-hidden="true"></i> Processing charge of ₹ 50 will be applicable on COD
				</div>
				<div class="cart-totals">
					<table>
						<tbody>	
							<tr>
								<td>Base Price</td>
								<td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
							</tr>
							<tr>
								<td>GST</td>
								<td> &#8377; '.number_format($tax_amount, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Sub Total</td>
								<td> &#8377; '.number_format($grandTotal, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Shipping and Handling</td>
								<td>&#8377; '.$shipping.'</td>
							</tr>	 
							<tr>
								<td>Cash on Delivery Fee</td>
								<td>&#8377; '.$cod_amount.'</td>
							</tr>	 
							<tr> 
								<td>Order Total</td>
								<td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
							</tr>	
						</tbody>  
					</table> 
					</div>
					</div>
				';	  
			}
			else
			{
				echo '
				<div class="cart-summary">
				<div class="price_details">
					<h2>Summary</h2>
				</div>
				<div class="text_section">        
					<i class="fa fa-exclamation" aria-hidden="true"></i> Processing charge of ₹ 50 will be applicable on COD
				</div>
				<div class="cart-totals">
					<table>
						<tbody>	
							<tr>
								<td>Base Price</td>
								<td> &#8377; '.number_format($subTotal, 2, '.', '').'</td>
							</tr>
							<tr>
								<td>GST</td>
								<td> &#8377; '.number_format($tax_amount, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Sub Total</td>
								<td> &#8377; '.number_format($grandTotal, 2, '.', '').'</td>
							</tr>	
							<tr>
								<td>Shipping and Handling</td>
								<td>&#8377; '.$shipping.'</td>
							</tr>	 
							<tr>
								<td>Order Total</td>
								<td> &#8377; '.number_format($order_total, 2, '.', '').'</td>
							</tr>	
						</tbody>  
					</table>
					</div>
					</div>
				';	  
			}
		} 
		else 
		{
			echo "<p>No Item In Cart</p>";
		} 
		
	  
	}
	
}