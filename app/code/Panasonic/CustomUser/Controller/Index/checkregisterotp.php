<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;
use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\NoSuchEntityException;

class Checkregisterotp extends \Magento\Framework\App\Action\Action
{
	protected $_resultPageFactory;
	public $_storeManager;
	public function __construct(Context $context, 
		\Magento\Framework\View\Result\PageFactory $resultPageFactory, 
		\Magento\Store\Model\StoreManagerInterface $storeManager, 
		CustomerRepositoryInterface $customerRepository,
        CustomerSession $customerSession,
		CustomHelper $helper, 
		UrlInterface $url,
		ResultFactory $resultFactory
	)
  {
		$this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		$this->resultFactory = $resultFactory;
		$this->url = $url;   
		$this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;   
		parent::__construct($context);
  }
 
	public function execute() 
	{
       
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		// $CustomerDataa=json_decode($customerSession->getUserPinPhoneJson());
		$minutes=60;		
		// $minutes = (time() - strtotime($CustomerDataa->cr_time)) / 60;
		// $minutes = round($minutes);
		$customer_date = $customerSession->getCustomerRegisterOtpDetails();
		$customer_date_array=json_decode($customer_date,true);
		$isotpconfirmed = $customer_date_array['is_otp_confirmed'];
		if($isotpconfirmed!='yes')
		{
			
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setRefererOrBaseUrl();
			//$resultRedirect->setUrl($this->url->getUrl());
			$this->messageManager->addError(__('Otp does not match'));  
			return $resultRedirect;     		
			die;
		}
		
		if($minutes > 1 && false)       
		{    
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setRefererOrBaseUrl();
			//$resultRedirect->setUrl($this->url->getUrl());
			$this->messageManager->addError(__('Otp Expired')); 	
			return $resultRedirect; 
			$customerSession->setOtpPinExpire(1);  
			$customerSession->unscountdown();   
		}
		else
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
			$customerSession = $objectManager->get('Magento\Customer\Model\Session');
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$state = $objectManager->get('\Magento\Framework\App\State');
			//$state->setAreaCode('frontend'); 
			$CustomerRegData=json_encode($_POST);
			$customerSession->setCustomerRegData($CustomerRegData); 
			$customerData=$customerSession->getCustomerRegData(); //Get value from customer session 
			$customerArray=json_decode($customerData);	
			$group_id=$customerArray->group_id;
			$email=$customerArray->email;
			$firstname=$customerArray->firstname;
			$lastname=$customerArray->lastname;
			$password=$customerArray->password;
			$mobile=$customerArray->mobile;
			
			if(isset($_POST['city']))
			{
				$city = $customerArray->city;
			}
			if(isset($_POST['area_of_interest']))
			{
				$interest = $customerArray->area_of_interest;
			}
			
			
			
			if(isset($customerArray->is_seller))
			{
				$is_seller = $customerArray->is_seller;
			} else{
				$is_seller=0;
			}
			
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			$check_mobile=$custom_helpers->check_customer_mobile($mobile);
			$userIdcheck='';
			$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
			$CustomerModel->setWebsiteId(1);
			$CustomerModel->loadByEmail($email);
			$userIdcheck = $CustomerModel->getId();

			if($userIdcheck!='')
			{
				$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
				$resultRedirect->setRefererOrBaseUrl();
				//$resultRedirect->setUrl($this->url->getUrl());
				
				$this->messageManager->addError(
					__(
						'Enter Email ID already Exists' 
					)      
				);  
				return $resultRedirect;     		
				die;
			}
			
			
			if($check_mobile > 0)
			{
				$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
				$resultRedirect->setRefererOrBaseUrl();
				//$resultRedirect->setUrl($this->url->getUrl());
				 
				$this->messageManager->addError(
					__(
						'Enter Mobile number already exists'
					)      
				); 
				return $resultRedirect;     		
				die;
			}
			
			
			
			$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');
			$websiteId = $storeManager->getWebsite()->getWebsiteId();
			$store = $storeManager->getStore();  // Get Store ID
			$storeId = $store->getStoreId();
			$customer = $customerFactory->create();
			$customer->setWebsiteId($websiteId);
			$customer->setEmail($email);
			$customer->setFirstname($firstname);
			$customer->setLastname($lastname);
			$customer->setPassword($password);
			$customer->setMobile($mobile); 

			if($group_id=="4" || $is_seller=='1')
			{

				$company_nam = $customerArray->company_nam;
				$company_add = $customerArray->company_add;
				$gst_number = $customerArray->gst_number;
				$pan_number= $customerArray->pan_number;
				
				$customer->setData("company_nam",$company_nam);
				$customer->setData("company_add",$company_add);
				$customer->setData("gst_number",$gst_number);
				$customer->setData("pan_number",$pan_number);

			}
			

			if(isset($_POST['city']))
			{
				$city = $customerArray->city;
				$customer->setData("city",$city);
			}
			
			if(isset($_POST['area_of_interest']))
			{
				$interest = $customerArray->area_of_interest;
				$customer->setData("area_of_interest",implode(',',$interest));
			}

			
			
			$customer->save();
			$custID=$customer->getId();
			/* complete survey if  present  */
			if(isset($_POST['answer_more']) && $_POST['answer_more']=="yes")
			{
				$this->fillSurvey($_POST,$custID);
			}

			
			/* Seller Region & ZipCode Save By OM */
			$sellerId = $custID;
			
			if(isset($_POST['seller_zipcode'])) {
				$seller_zipcode = $_POST['seller_zipcode'];			
				$resource = $objectManager->create('Magento\Framework\App\ResourceConnection');
				$_connection = $resource->getConnection();
				$postcode_exist = $_connection->fetchOne("SELECT `id` FROM `seller_postcode` WHERE `seller_id` LIKE '$sellerId'");
				if(!$postcode_exist){
					$_connection->query("INSERT INTO `seller_postcode` (`seller_id`, `postcode`) VALUES ('$sellerId', '$seller_zipcode')");
				}else{
					$_connection->query("UPDATE `seller_postcode` set `postcode`='$seller_zipcode' WHERE `seller_id` LIKE '$sellerId'");
				}
			}		

			/* dispartching events for other  extensions to  hook  @ritesh15122020*/	
			$this->_eventManager->dispatch(
                'customer_register_success',
                ['account_controller' => $this, 'customer' => $customer]
			);

			try {
        
            	$customer = $this->customerRepository->getById($custID);
		        } catch (NoSuchEntityException $exception) {
		            throw new Exception(__('The wrong customer account is specified.'));
		        }
				$this->customerSession->setCustomerDataAsLoggedIn($customer);

			// $this->_eventManager->dispatch(
			// 	'customer_save_after_data_object',
			// 	[
			// 		'customer_data_object' => $customer,
			// 		'orig_customer_data_object' => $customer,
			// 		'delegate_data' =>[],
			// 	]
			// );
			
			
			$scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
			$isconfirmationrequired = $scopeConfig->getValue('customer/create_account/confirm');
			
			$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($custID);
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			
			$stateid = $connection->fetchOne("select state_id from `om_city_state` where city like '".$city."'");
			if($stateid){
				$customer->setData("state",$stateid);
				$customer->save();
			}

			if($isconfirmationrequired)
			{
				$customer->sendNewAccountEmail('confirmation', '', $customer->getStoreId());
			} else 
			{
				$sql = "update customer_entity set confirmation = NULL WHERE entity_id='".$custID."' ";
				$connection->query($sql);  
			
				$sql = "update customer_grid_flat set confirmation = NULL WHERE entity_id='".$custID."' ";
				$connection->query($sql);   
			   
				$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
				$helpers->individual_welcome($firstname,$lastname,$email) ; 				
			}

			/* auto  approve company  buyers @ritesh29012021*/
			if($group_id=="4" )
			{
				$sql = "update customer_grid_flat set attribute_approved = 1 WHERE entity_id='".$custID."' ";
				$connection->query($sql);
				$sql = "update `customer_entity_int` set `value` = 1 where attribute_id='177' and entity_id = '$custID'";
				$connection->query($sql);      
			} 
			
			$sql = "update customer_grid_flat set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
			$connection->query($sql);
			
			$sql = "update customer_entity set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
			$connection->query($sql);      

			$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
			$buyer_helpers->create_buyer_report_updated($custID); 	   
            
			$customerSession->unsOTPSent();  	
			$customerSession->unsCustomerRegData();
			$customerSession->unsPinValue(); 
			$customerSession->unsOTPFormShow(); 
			$customerSession->unsCustomerRegisterOtpDetails();
			
			$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($custID);
			// $customerSession->setCustomerAsLoggedIn($customer);	  
			
			$customerSession->setbuyerSession($custID); 
			$customerSession->setRegistrationSuccusfulNeedConfirmation("yes");
			$messageManagercustom = $objectManager->create('Magento\Framework\Message\ManagerInterface');
			// $messageManagercustom->addSuccessMessage(__('Please confirm  your email id to confirm your registration'));
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setRefererOrBaseUrl();
			//$resultRedirect->setUrl($this->url->getUrl());
			return $resultRedirect; 
		} 
	}  
	public function fillSurvey($postdata,$customer_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->create('Magento\Framework\App\ResourceConnection');
		$_connection = $resource->getConnection();

		$name = isset($postdata['firstname'])?$postdata['firstname']:"none";
		$email = isset($postdata['email'])?$postdata['email']:"none";
		$mobile = isset($postdata['mobile'])?$postdata['mobile']:"none";
	
		if(isset($postdata['dealer_of_product']) && !empty($postdata['dealer_of_product']))
		{
			$dealer_of_product = implode(",",$postdata['dealer_of_product']);
			if(isset($postdata['dealer_of_product_please_specify']) && !empty($postdata['dealer_of_product_please_specify']))
			{
				$dealer_of_product .= ",".$postdata['dealer_of_product_please_specify'];
			}
		}

		if(isset($postdata['dealing_in']) && !empty($postdata['dealing_in']))
		{
			$dealing_in = implode(",",$postdata['dealing_in']);
			if(isset($postdata['dealing_in_please_specify']) && !empty($postdata['dealing_in_please_specify']))
			{
				$dealing_in .= ",".$postdata['dealing_in_please_specify'];
			}
		}

		if(isset($postdata['procure_from']) && !empty($postdata['procure_from']))
		{
			$procure_from = implode(",",$postdata['procure_from']);
			if(isset($postdata['procure_from_please_specify']) && !empty($postdata['procure_from_please_specify']))
			{
				$procure_from .= ",".$postdata['procure_from_please_specify'];
			}
		}
		
		if(isset($postdata['difficult_spares_to_get']))
		{
			$difficult_spares_to_get = $postdata['difficult_spares_to_get'];
		}
		if(isset($postdata['like_to_buy_online']))
		{
			$like_to_buy_online = $postdata['like_to_buy_online'];
		}
		if(isset($postdata['not_buy_online_reason']))
		{
			$not_buy_online_reason = $postdata['not_buy_online_reason'];
		}
		if(isset($postdata['like_to_sell']))
		{
			$like_to_sell = $postdata['like_to_sell'];
		}
    
		$query = "INSERT INTO `om_bussiness_survey` set 
			`name`='$name',		
			`email_id`='$email',		
			`mobile_number`='$mobile',		
			`dealer_of_product`='$dealer_of_product',		
			`dealing_in`='$dealing_in',		
			`procure_from`='$procure_from',		
			`difficult_spares_to_get`='$difficult_spares_to_get',		
			`like_to_buy_online`='$like_to_buy_online',		
			`not_buy_online_reason`='$not_buy_online_reason',		
			`customer_id`='$customer_id',		
			`like_to_sell`='$like_to_sell'
		";
		$_connection->query($query);
	}
	
}
