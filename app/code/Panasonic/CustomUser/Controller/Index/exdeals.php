<?php
 
namespace Panasonic\CustomUser\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
class Exdeals extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
	public $_storeManager;
	 protected $_checkoutSession;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, CustomHelper $helper)
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_storeManager=$storeManager;
		$this->helper = $helper;  
		 $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }
 
    public function execute()
    {
        
		$now = date('Y-m-d H:i:s');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 $custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$collectionfirst =$objectManager->create('Magento\Catalog\Model\Product')->getCollection();            
		$collectionfirst->addAttributeToSelect('special_from_date')
		->addAttributeToSelect('special_to_date')
		->addAttributeToFilter('special_price', ['neq' => ''])
		->addAttributeToFilter(
				'special_from_date',
				['lteq' => date('Y-m-d H:i:s', strtotime($now))]
			)

		 ->addAttributeToFilter(
				'special_to_date',
				['gteq' => date('Y-m-d H:i:s', strtotime($now))]

		);
		$excitingDeals = $collectionfirst->getData();
		
		
		 
		foreach ($excitingDeals as $product){
			$ex_product[]=$product['entity_id'];
		}	
		if(!empty($ex_product))
		{	
		echo "<pre>";
			print_r($ex_product);
		echo "</pre>"; 
		}    
		
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$collection = $productCollection->addAttributeToSelect('*')
					->load();
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		foreach ($collection as $product){
			$productId=$product->getId();
			
			if(!empty($ex_product)){
				if(!in_array($productId, $ex_product)) 
				{
					//echo $productId;
					$sqls = "delete from catalog_product_entity_int where attribute_id='231' and entity_id='".$productId."'";
					//echo "<br>";   
					$connection->query($sqls);		
					  
				}
				else   
				{
					$productId;
					$product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId); 
					$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
					
					$specialprice=$custom_helpers->get_product_special_price($productId);
					$orgprice = $product->getPrice();
					
					
					if ($specialprice < $orgprice) 
					{
						$loss=$orgprice-$specialprice;
						$discount=($loss*100)/$orgprice;
						//$discount=($specialprice/$orgprice)*100;
						$discount=number_format($discount, 2, '.', '');
						
						if($discount >= 1 && $discount <= 10)
						{
							$level=128;	
						}
						else if($discount >= 11 && $discount <= 20)
						{
							$level=129;	
						}
						else if($discount >= 21 && $discount <= 30)
						{
							$level=130;	
						}
						else if($discount >= 31 && $discount <= 40)
						{
							$level=131;	
						}
						else if($discount >= 41 && $discount <= 50)
						{
							$level=132;	
						}	
						else if($discount >= 51 && $discount <= 60)
						{
							$level=133;	
						}
						else if($discount >= 61 && $discount <= 70)
						{
							$level=134;	
						}
						else if($discount >= 71 && $discount <= 80)
						{
							$level=135;	
						}
						else if($discount >= 81 && $discount <= 90)
						{
							$level=136;	
						}
						else
						{
							$level=138;	 
						} 
						
						$level;
						
						
						$sqls = "delete from catalog_product_entity_int where attribute_id='231' and entity_id='".$productId."'";
						$connection->query($sqls);		
							
						
						$sqls = "Insert Into catalog_product_entity_int (attribute_id,store_id,entity_id,value) Values ('231','0','".$productId."','".$level."')";
						$connection->query($sqls);	
						
						 
						
					}
					
					
				    
				} 
			}	  
		
		}
		
		
		
		  
		
	}  
	
	
	public function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}  
	
	
}