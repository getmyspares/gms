<?php
 
namespace Panasonic\CustomUser\Controller\Index;
   
use Magento\Framework\App\Action\Context;   
use Panasonic\CustomUser\Helper\Data as CustomHelper; 

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;


class Forgetpasswordresendotp extends \Magento\Framework\App\Action\Action
{   
    protected $_resultPageFactory;   
	protected $resultFactory;
    protected $url;
	
	 
	
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, CustomHelper $helper,  
		UrlInterface $url,
        ResultFactory $resultFactory) 
    {
        $this->_resultPageFactory = $resultPageFactory;
		$this->helper = $helper;    
		$this->resultFactory = $resultFactory;
		$this->url = $url;
        parent::__construct($context);
    }
 
    public function execute()
    {   
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$today=date('Y-m-d H:i:s');
		 
		$pin = mt_rand(1000, 9999);				  
		$today=date('Y-m-d H:i:s');
		$forgetotp=$customerSession->getForgetPinJson(); 
		
		
		$userForgetPinJson=$forgetotp;
		$userForgetPinJsonnn=json_decode($userForgetPinJson);
		
		$resendcount=$customerSession->getForgetresendcount();   
		if($resendcount=='') 
		{
			$customerSession->setForgetresendcount(1);  	
		}
		else
		{
			$resendcount=$resendcount+1;
			$customerSession->setForgetresendcount($resendcount);  	
		} 	 
		  
		
		if($resendcount > 2) 
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customuser/index/forgetsession'));
			
			return $resultRedirect;    
			die; 
		}  

		

		$completeUser=array(
			'pin'=>$pin,	
			'phone'=>$userForgetPinJsonnn->phone,	
			'userId'=>$userForgetPinJsonnn->userId,	  
			'email'=>$userForgetPinJsonnn->email,	  
			'cr_time'=>$today	    
		);    
		
		 
		
		$pin=$completeUser['pin'];
		$phone=$completeUser['phone'];
		$email=$completeUser['email'];
		
			
		$userForgetPinJsonn=json_encode($completeUser);
		$customerSession->setForgetPinJson($userForgetPinJsonn); //set value in customer session
		$customerSession->getForgetPinJson(); //Get value from customer session 
				
		$customerSession->setForgetcountdown(1); 	
		$customerSession->setResendbuttondba(1); 	
		    
		$customerSession->unsForgetResendbutton();	    
		
		$customerSession->setForgetcountdown(1);  
		
		$message="Spare Parts - Please complete your Forget Password process by entering this OTP - ".$pin; 
		$this->helper->ForgetPasswordHelperOTP($pin,$phone,$message,$email);  
		 
		
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
		$resultRedirect->setUrl($this->url->getUrl('customer/account/forgotpassword/asdd'));
		 
		$this->messageManager->addSuccess(__('Otp sent'));
		return $resultRedirect;     
		
		     
    }
	
	
	
	
}