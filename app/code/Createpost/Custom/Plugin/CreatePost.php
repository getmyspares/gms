<?php
namespace Createpost\Custom\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory; 
use Magento\Framework\App\RequestInterface; 
class CreatePost
{ 
    protected $resultFactory;
    protected $url;
    protected $_request;
    protected $_response; 
	protected $customerFactory;
	protected $subscriberFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        UrlInterface $url,
        ResultFactory $resultFactory,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		StoreManagerInterface $storeManager,
		\Magento\Customer\Model\CustomerFactory $customerFactory,
		 SubscriberFactory $subscriberFactory,
		CustomHelper $helper
		
    )
    {
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->url = $url;
        $this->resultFactory = $resultFactory;
		$this->helper = $helper;    
		$this->messageManager = $messageManager;
		$this->storeManager = $storeManager;
		$this->customerFactory  = $customerFactory;
		 $this->subscriberFactory = $subscriberFactory;
    }

    public function aroundExecute(\Magento\Customer\Controller\Account\CreatePost $subject, \Closure $proceed) {
        
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$group_id=$this->_request->getPost('group_id');    
		$is_seller=$this->_request->getPost('is_seller');    
		$mobile=$this->_request->getPost('mobile');    
		$email=$this->_request->getPost('email');    
		
		
		$check_mobile=$custom_helpers->check_customer_mobile($mobile);
		
		$userId='';
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1);
		$CustomerModel->loadByEmail($email);
		$userId = $CustomerModel->getId();
		
		
		$CustomerRegData=json_encode($_POST);
		$customerSession->setCustomerRegData($CustomerRegData); 
		$customer_date = $customerSession->getCustomerRegisterOtpDetails();
		$customer_date_array=json_decode($customer_date,true);
		$isotpconfirmed = $customer_date_array['is_otp_confirmed'];
		if($isotpconfirmed=='no')
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account/create'));
			$this->messageManager->addError(__('Mobile Otp  Not Confirmed'));  
			return $resultRedirect;     		
			die;
		}
		

		if($userId!='')
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			
			$resultRedirect->setUrl($this->url->getUrl('customer/account/create'));
			
			$this->messageManager->addError(
				__(
					'Enter Email ID already Exists' 
				)      
			);  
			return $resultRedirect;     		
			die;
		} 
		
		
		if($check_mobile > 0)
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account/create'));
			 
			$this->messageManager->addError(
				__(
					'Enter Mobile number already exists'
				)      
			); 
			return $resultRedirect;     		
			die;
		}	 
		
		if( ($group_id==1 && $is_seller==1) || ($group_id==4)) 
		{
			 
			
			$resultProceed = $proceed(); 
			return $resultProceed;
		}
		else
		{
			$userId=0;
			
			$errorcode=0;
			$pincodeSession=$customerSession->getPinValue(); 
			$completeUser=array();
			
			if($pincodeSession=='')
			{
				$pincodeSession=0;		
			} 
			$post = $this->_request->getPostValue();
			
			if($pincodeSession==0 && false )
			{    
							
				$CustomerRegData=json_encode($post);
				$pin = mt_rand(1000, 9999);				  
				$today=date('Y-m-d H:i:s');			
				$phone=$post['mobile']; 
				$group_id=$post['group_id'];
				  
					
				
				$completeUser=array(
					'pin'=>$pin,	
					'phone'=>$phone,	
					'group_id'=>$group_id,	
					'cr_time'=>$today
				);  
				
				$userPinPhoneJson=json_encode($completeUser);
				  
				 
				$customerSession->setUserPinPhoneJson($userPinPhoneJson); //set value in customer session
				$customerSession->getUserPinPhoneJson(); //Get value from customer session 
						 
				$customerSession->setCustomerRegData($CustomerRegData); //set value in customer session
				$customerSession->getCustomerRegData(); //Get value from customer session 
				
				   
				
				$customerSession->setCustomerRegData($CustomerRegData); //set value in customer session
				$customerSession->getCustomerRegData(); //Get value from customer session 
				
				  
				
				$customerSession->setPinValue($pin); //set value in customer session
				$customerSession->getPinValue(); //Get value from customer session
				
				
				   
				
								
				$email = $post['email'];	  
					   
				$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
				$CustomerModel->setWebsiteId(1);
				$CustomerModel->loadByEmail($email);
				$userId = $CustomerModel->getId();
				
				if($userId=='')
				{
					$userId=0;
				}
				if($userId==0)   
				{
					
					$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
					$resultRedirect->setUrl($this->url->getUrl('customer/account/create'));
					 
					$this->messageManager->addSuccess(
						__(
							'OTP has been sent to your mobile number'
						)      
					);
					
					$customerSession->setOTPSent(1);  
					$customerSession->setOTPFormShow(1);  
					$customerSession->setcountdown(1);       
					$this->helper->CustomResendOTP($pin,$phone,$email);    
					return $resultRedirect;               	 
				}            		   
				else    
				{ 
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
					$site_urls=$storeManager->getStore()->getBaseUrl();
					
					$site_url=$site_urls.'customer/account/forgotpassword';  
					  
					$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
					$resultRedirect->setUrl($this->url->getUrl('customer/account/create'));
					$this->messageManager->addError(
						__(
							'There is already an account with this email address. If you are sure that it is your email address, <a href="'.$site_url.'">click here</a> to get your password and access your account.'
						)  
					);   
					  
					$customerSession->setMobileError(1); 
					$customerSession->unsPinValue();
					return $resultRedirect; 	   	
					die; 
			 
					
				}        
				 
					 
			}
			else  
			{  	  	
				
				
				
				$firstname = $post['firstname'];  	   		
				$lastname = $post['lastname'];  	
				$phone = $post['mobile']; 			
				
				  
				$custphone=$phone;
				$group_id=$post['group_id'];
				  
				if($post['group_id']==1 || $post['group_id']==5)
				{   
					/* $customer = $this->accountManagement
						->createAccount($customer, $password, $redirectUrl); */
					
					
					 
					
					$websiteId  = $this->storeManager->getWebsite()->getWebsiteId();

					$customer   = $this->customerFactory->create();
					$customer->setWebsiteId($websiteId);

				   
					$customer->setEmail($post['email']);   
					$customer->setFirstname($post['firstname']);
					$customer->setLastname($post['lastname']);
					$customer->setPassword($post['password']);
					$customer->setMobile($post['mobile']); 

				   
					$customer->save();
					
					$custID=$customer->getId();  
					 
										
					$is_subscribed=$this->_request->getPost('is_subscribed');
					if($is_subscribed!='')
					{
						$this->subscriberFactory->create()->subscribeCustomerById($customer->getId());	
					} 

					
					
				
					
					$objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
					$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($custID);
					$customerSession = $objectManager->create('Magento\Customer\Model\Session');
					$customerSession->setCustomerAsLoggedIn($customer);	  

					  
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					
					
					$sql = "update customer_grid_flat set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
					$connection->query($sql);
					
					$sql = "update customer_entity set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
					$connection->query($sql);      
					
					  
					
					$sql = "update customer_entity set confirmation = NULL WHERE entity_id='".$custID."' ";
					$connection->query($sql);  
					
					
					$sql = "update customer_grid_flat set confirmation = NULL WHERE entity_id='".$custID."' ";
					$connection->query($sql);   
					   
					 	   
					
					$this->helper->individual_welcome($post['firstname'],$post['lastname'],$post['email']) ;                  
										 
					$customerSession->unsOTPSent();  	
					$customerSession->unsCustomerRegData();
					$customerSession->unsPinValue(); 
					$customerSession->unsOTPFormShow(); 
					$customerSession->setCustomerRegisterOtpDetails();

					$objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
					$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($custID);
					$customerSession = $objectManager->create('Magento\Customer\Model\Session');
					$customerSession->setCustomerAsLoggedIn($customer);	  
					
					$customerSession->setbuyerSession($custID); 		
   
					
					$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
					$resultRedirect->setUrl($this->url->getUrl('customuser/index/buyersession/'));
					
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
					$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
					
					//$helpers->create_user_array($custID);     
					  
					    	
					return $resultRedirect;                
				} 
				 
				
						 
				  
			}	
			
			
			
		}
		
		 
		
		
    }
}