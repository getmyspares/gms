<?php

namespace Customm\Apii\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;


/* remove static directory path and make file path dynamic @ritesh23july2020*/
$doc_root = BP;
require_once $doc_root.'/app/code/Razorpay/razorpay/Razorpay.php'; 


class Data extends AbstractHelper
{
    
	
	public function get_path()
	{
		return BP;     	
	}	       
	
	
	public function custom_number_format($value) 
	{
		return $value=bcadd(sprintf('%F', $value), '0', 2);
	}	
	
	public function get_razorpay_mode() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();

		if($site_url=="http://demo16.mobdigi.com/" || $site_url=="https://demo16.mobdigi.com/" || $site_url=="https://getmyspares.in/")
		{
			$mode='dev';    
		} else 
		{
			$mode='live';
		}
		
		if($mode=='dev')  {
			$username='panasonicindia90573_temp';	
			$password='G8SMV7suncywG53w';
			$url='https://clbeta.ecomexpress.in/';
		} else {		    
			$username='panasonicpvtltd402639_pro';
			$password='6bk2LrZpC7UXhuZe';  
			$url='https://api.ecomexpress.in/';
		}  
		
		$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		
		$keyId     = $scopeConfig->getValue('payment/razorpay/key_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
		$keySecret = $scopeConfig->getValue('payment/razorpay/key_secret',\Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
		$array=array('keyId'=>$keyId,'keySecret'=>$keySecret,'mode'=>$mode,'username'=>$username,'password'=>$password,'url'=>$url);  
		return json_encode($array);   
	}	 
	
    
	public function get_token()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		// $userData = array("username" => "orangemantra", "password" => "ritesh@123");               
		// $userData = array("username" => "orange", "password" => "mantra@12");
		$userData = array("username" => "orange", "password" => "mantra@12345");               
		// $userData = array("username" => "gmsgms", "password" => "gmsq@1234");               
		// $userData = array("username" => "akhilesh", "password" => "Akhilesh#31");               
		
		$ch = curl_init($site_url."index.php/rest/V1/integration/admin/token"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);    
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );	
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
		$token = curl_exec($ch);
       
		return json_decode($token);
		 
	}

	public function get_quote_id($token,$user_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$token=$this->get_token();
		//print_r($token);
		//die;
		$customerData = [
			'customer_id' => $user_id
		];
		$ch = curl_init($site_url."index.php/rest/V1/carts/mine");
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);	
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData,true));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
		
		$result = curl_exec($ch); 

		return $quote_id = json_decode($result, 1);
	}	
	 

	public function get_quote_id_Om($token,$user_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$quote_id=null;
		$quote_om  = $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id);
		if(!empty($quote_om))
		{
			return $quote_id = $quote_om->getId();
		}	else 
		{
			$site_url=$storeManager->getStore()->getBaseUrl();
			$customerData = [
				'customer_id' => $user_id
			];
			$ch = curl_init($site_url."index.php/rest/V1/carts/mine");
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData,true));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));
			$result = curl_exec($ch); 
			return $quote_id = json_decode($result, 1);
		}

		
	
	}
	
	public function addtocart_test($user_id,$product_id,$qty)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
		$product_sku=$product->getSku();
		$product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
		$in_stock=$product->getExtensionAttributes()->getStockItem()->getIsInStock();
		
		
		$arrayss=$api_helpers->get_user_token_details($user_id);
		//print_r($arrayss);
		//die; 
		$token=$arrayss['token'];
		$quote_id=$arrayss['quote_id'];  
		
		
		//$token=$api_helpers->get_token(); 
		//$quote_id=json_decode($api_helpers->get_quote_id($token,$user_id));
		
		if($in_stock==0)
		{
			//return 'out';
			
			$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
			return $array_result=array('status'=>'out','cart_array'=>$cartArray);
			die;
		}
		else if($product_qty<=$qty && $in_stock==1)
		{
			//return 'qty'; 
			//die; 	  
			
			$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
			return $array_result=array('status'=>'qty','cart_array'=>$cartArray);
			die;
			
		}  
		else		
		{
			$today = date('Y-m-d H:i:s');
			$quote_id;
			
			$select = $connection->select()
                  ->from('quote_item') 
                  ->where('quote_id = ?', $quote_id)
                  ->where('product_id = ?', $product_id);
			$result_curr = $connection->fetchAll($select);
			$cart_count=count($result_curr);
			if($cart_count==0)
			{
				//insert
				$productData = [
					'cart_item' => [
						'quote_id' => $quote_id,
						'sku' => $product_sku,
						'qty' => $qty   
					]
				];  
				$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
	 
				$result = curl_exec($ch);

				$result = json_decode($result, 1);
				
				$sql = "Insert Into mobile_user_cart (user_id,quote_id, product_id, qty, created_at) Values ('".$user_id."','".$quote_id."','".$product_id."','".$qty."','".$today."')"; 
				$connection->query($sql);  
				
				
				//$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
				$cartArray=array();
				return $array_result=array('status'=>'insert','cart_array'=>$cartArray);
				die;	   

				
			}	
			else
			{
				foreach($result_curr as $item)
				{ 
					$cart_product_id=$item['product_id'];		 
					$cart_sku=$item['sku'];		 
					$cart_price=$item['price'];		 
					$cart_base_price=$item['base_price'];		
				
					
					$cart_item_id=$item['item_id'];	 
					$row_total=$qty*$cart_price;
					$row_base_total=$qty*$cart_base_price;
					
					$quote_qty=number_format($qty, 4, '.', '');
					$quote_row_total=number_format($row_total, 4, '.', '');
					$quote_base_total=number_format($row_base_total, 4, '.', ''); 
					
					
					$cartData = [
						'cart_item' => [
							'item_id' => $cart_item_id,
							'qty' => $qty, 
							'quote_id' => $quote_id   
						]
					]; 
					
					
					$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($cartData));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,1);
					
					
					$result = curl_exec($ch);

					$result = json_decode($result, 1); 
					
					
					/*
					$sql = "update quote_item set qty='".$quote_qty."',row_total='".$quote_row_total."',base_row_total='".$quote_base_total."' where item_id='".$cart_item_id."'"; 
					$connection->query($sql); 
					
					*/
					
					
					$sql = "update mobile_user_cart set qty='".$qty."',created_at='".$today."' where  user_id='".$user_id."' and  quote_id='".$quote_id."' and product_id='".$product_id."'";
							
							
					//$connection->query($sql);    
					 
					//$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
					$cartArray=array();
					return $array_result=array('status'=>'update','cart_array'=>$cartArray);
					die;
					
					
				
				}
				
			}
			
		
		}	
		
	}
	
	
	public function addtocart_test_old($user_id,$product_id,$qty)
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
		$product_sku=$product->getSku();
		$StockState = $objectManager->get('\Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
		$stock_data = $StockState->execute($product_sku);
		/* removed product quantity and replaced it with product salable  quantity to resolve problems at checkout @ritesh180920202*/
		// $product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
		$product_qty = $stock_data[0]['qty'];
		
		
		$in_stock=$product->getExtensionAttributes()->getStockItem()->getIsInStock();
		$arrayss=$api_helpers->get_user_token_details($user_id);
		//print_r($arrayss);
		//die; 
		$token=$arrayss['token'];
		$quote_id=$api_helpers->get_quote_id_Om($token,$user_id);
		
		/* minimum salable  quantity fix , also make  product out  of  stock if  quantity left is  less than  the minimum salable quantity */
		$min_sale_quantity = (int)$product->getExtensionAttributes()->getStockItem()->getData('min_sale_qty');
		if($min_sale_quantity > $qty)
		{
			$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
			return $array_result=array('status'=>'minsalable','cart_array'=>$cartArray,"error"=>"Minimum Quantity You can order is $min_sale_quantity");
			die;
			
			// $qty=$min_sale_quantity;
		}
		if($min_sale_quantity > (int)$product_qty)
		{
			$in_stock=0;
		}
		
		
		if($in_stock==0)
		{
			//return 'out';
			
			$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
			return $array_result=array('status'=>'out','cart_array'=>$cartArray);
			die;
		}
		else if($product_qty<$qty && $in_stock==1)
		{
			//return 'qty'; 
			//die; 	  
			
			$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
			return $array_result=array('status'=>'qty','cart_array'=>$cartArray);
			die;
			
		} 
		
		else		
		{
			
			
			/* $quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
			$quoteItems=$quote->getAllVisibleItems();
			$items = $quote->getAllItems(); */ 
			
			
			
			
			$today = date('Y-m-d H:i:s');
			$select = $connection->select()
			->from('quote_item') 
			->where('quote_id = ?', $quote_id);
			$result_curr = $connection->fetchAll($select);
			$cart_count=count($result_curr);
			
			
			
			
			if($cart_count!=0)
			{  
				//$cartArray=$result['items'];
				foreach($result_curr as $row)
				{
					$skuArray[]=$row['sku'];	
				} 	
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
				$product_sku=$product->getSku();
				
				if (!in_array($product_sku, $skuArray)) 
				{                        
					$productData = [
						'cart_item' => [
							'quote_id' => $quote_id,
							'sku' => $product_sku,
							'qty' => $qty   
							]
						];  
						$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
						curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );	
						curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
						curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
						
						$result = curl_exec($ch);
						$result = json_decode($result, 1);
						
						$sql = "Insert Into mobile_user_cart (user_id,quote_id, product_id, qty, created_at) Values ('".$user_id."','".$quote_id."','".$product_id."','".$qty."','".$today."')"; 
						$connection->query($sql);  
						
						
						
						//return 'insert'; 
						
						//$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
						$cartArray=array();
						return $array_result=array('status'=>'insert','cart_array'=>$cartArray);
						die;
						
					} 	
					else
					{
						
						
						//$cart_items=$result['items'];
						$same_product=0;
						
						$tier_price = $product->getTierPrice();
						if($tier_price)
						{
							foreach ($tier_price as $key => $value) {
									$tqty = (int)$value['price_qty'];
									$t_price = $value['price'];
									$tier_price_info[$tqty]=$t_price;
							}
							ksort($tier_price_info);
						}
						$cart_subtotal = 0;
						foreach($result_curr as $item)
						{
							
							$cart_product_id=$item['product_id'];		 
							$cart_sku=$item['sku'];		 
							$cart_price=$item['price'];		 
							$cart_base_price=$item['base_price'];		 
							
							
							if($cart_product_id==$product_id)
							{
								$cart_item_id=$item['item_id'];	 
								$row_total=$qty*$cart_price;
								
								if($tier_price_info)
								{
									$ias=0;
									foreach ($tier_price_info as $tkqty => $tprice) {
										if($ias==0 && $qty<$tkqty)
										{
											$price=$product->getPrice();		
											$orgprice = $price;
											$specialprice = $product->getSpecialPrice();
											$specialfromdate = $product->getSpecialFromDate();
											$specialtodate =  $product->getSpecialToDate();
											
											$today = time();
											if (!$specialprice)
											{	
												$specialprice = $orgprice;
											} else
											{
												if(!is_null($specialfromdate) && !is_null($specialtodate))
												{
													if($today >= strtotime($specialfromdate) &&  $today <= strtotime($specialtodate))
													{
														$is_special=1;			
													}	else 
													{
														$is_special=0;
													}
												} else{
													$is_special=1;
												}
											}
											if($is_special==1)
											{
												$cart_base_price = $specialprice;  
											} else 
											{
												$cart_base_price = $price;  
											}
										}
										if($qty>=$tkqty)
										{
											$cart_base_price = $tprice;
										}
										$ias++;}
								}
								
								$row_base_total=$qty*$cart_base_price;
								$cart_subtotal+=$row_base_total;
								$quote_qty=number_format($qty, 4, '.', '');
								$quote_row_total=number_format($row_total, 4, '.', '');
								$quote_base_total=number_format($row_base_total, 4, '.', ''); 
								
								$sql = "update quote_item set qty='".$quote_qty."',row_total='".$quote_row_total."',base_row_total='".$quote_base_total."' where item_id='".$cart_item_id."'"; 
								$connection->query($sql);     
								
								/*
								$quoteItem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($cart_item_id);
								$quoteItem->delete(); 
								
								$productData = [
									'cart_item' => [
										'quote_id' => $quote_id,
										'sku' => $product_sku,
										'qty' => $qty   
										]
									];  
									
									
									
									
									
									$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
									curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
									
									$result = curl_exec($ch);
									
									$result = json_decode($result, 1);
									*/
									//$sql = "Insert Into mobile_user_cart (user_id,quote_id, product_id, qty, created_at) Values ('".$user_id."','".$quote_id."','".$product_id."','".$qty."','".$today."')"; 
									
									/*
									$cartData = [
										'cart_item' => [
											'item_id' => $cart_item_id,
											'qty' => $qty, 
											'quote_id' => $quote_id   
											]
										]; 
										
										
										$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
										curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
										curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
										curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($cartData));
										curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
										curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,1);
										
										
										$result = curl_exec($ch);
										
										$result = json_decode($result, 1); 
										
										*/
										
										$sql = "update mobile_user_cart set qty='".$qty."',created_at='".$today."' where  user_id='".$user_id."' and  quote_id='".$quote_id."' and product_id='".$product_id."'";
										$connection->query($sql);    
										//$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
										$cartArray=array();
									} else 
									{
										$cart_subtotal+=$cart_base_price;
									}
								}
								/* in update block  */
								if($cart_subtotal)
								{
									
									$sql = "update quote set subtotal='".$cart_subtotal."'where entity_id='".$quote_id."'"; 
									$connection->query($sql); 
								}
								return $array_result=array('status'=>'update','cart_array'=>$cartArray);
								die;
				} 
			}	
			else
			{
				$productData = [
						'cart_item' => [
							'quote_id' => $quote_id,
							'sku' => $product_sku,
							'qty' => $qty   
						]
					];  
					$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
		 
					$result = curl_exec($ch);

					$result = json_decode($result, 1);
					
					$sql = "Insert Into mobile_user_cart (user_id,quote_id, product_id, qty, created_at) Values ('".$user_id."','".$quote_id."','".$product_id."','".$qty."','".$today."')"; 
					$connection->query($sql);  

					
					//$cartArray=$api_helpers->get_current_cart($user_id,$token,$quote_id); 
					$cartArray=array();
					return $array_result=array('status'=>'insert','cart_array'=>$cartArray);
					die;	    
			}
			
		}
	}
	
	
	public function buy_now_adjusted($user_id,$product_id,$qty,$quote_id,$token)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	

		$product_sku=$product->getSku();
		
		$product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
		$in_stock=$product->getExtensionAttributes()->getStockItem()->getIsInStock();
		
		
		
		
		if($in_stock==0)
		{
			return 'out';
		}
		else if($product_qty<=$qty && $in_stock==1)
		{
			return 'qty'; 
			die; 	  
		}  
		else		
		{
			
			
			
			
			$token=$token;
			$today = date('Y-m-d H:i:s');
			/*
			$ch = curl_init($site_url."index.php/rest/V1/carts/".$quote_id);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . $token));

			$results = curl_exec($ch);

			$result = json_decode($results, 1);
			$cartArray=array();
			$cartArray=$result['items'];
			*/
			
			$select = $connection->select()
                  ->from('quote_item') 
                  ->where('quote_id = ?', $quote_id);
			$result_curr = $connection->fetchAll($select);
			$cart_count=count($result_curr);
			
			if(empty($result_curr))
			{
					$productData = [
						'cart_item' => [
							'quote_id' => $quote_id,
							'sku' => $product_sku,
							'qty' => $qty   
						]
					];  
					$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));

					$result = curl_exec($ch);

					$result = json_decode($result, 1);
					
					$sql = "Insert Into mobile_user_cart (user_id,quote_id, product_id, qty, created_at,buy_now) Values ('".$user_id."','".$quote_id."','".$product_id."','".$qty."','".$today."','1')"; 
					$connection->query($sql);   
					
					 
					return 'insert'; 		 	
			}	
			else
			{
				//$cart_items=$result['items'];
				$same_product=0;
				foreach($result_curr as $row)
				{
					$cart_sku=$row['sku'];		 
					$cart_product=$row['product_id'];		 
					$cart_item_id=$row['item_id'];		
					
					if($cart_product==$product_id)  
					{
						
						/*
						$quoteItem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($cart_item_id);
						$quoteItem->delete(); 
						
						 
						$sql="delete from mobile_user_cart where quote_id='".$quote_id."' and product_id='".$product_id."' and user_id='".$user_id."'";
						$connection->query($sql);	      
						 
						
						$same_product=0;
						*/
						
						$same_product=1;
						break; 
					}
				}	
				
				if($same_product==0)  
				{
					
				
					$productData = [
						'cart_item' => [
							'quote_id' => $quote_id,
							'sku' => $product_sku,
							'qty' => $qty   
						]
					];  
					$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));

					$result = curl_exec($ch);

					$result = json_decode($result, 1);
					
					  
					$sql="update mobile_user_cart set buy_now=NULL where quote_id=".$quote_id;
					$connection->query($sql);	
					
					
					$sql = "Insert Into mobile_user_cart (user_id,quote_id, product_id, qty, created_at,buy_now) Values ('".$user_id."','".$quote_id."','".$product_id."','".$qty."','".$today."','1')"; 
					$connection->query($sql);   
					
					return 'insert';  	
				
				}	
				else 
				{
					$sql="update mobile_user_cart set buy_now=NULL where quote_id=".$quote_id;
					$connection->query($sql);	
					
					
					$sql="update mobile_user_cart set buy_now=1 where quote_id='".$quote_id."' and product_id='".$product_id."'";
					$connection->query($sql);	
					 
					
					return 'same'; 
				} 	 	 
			 
			
			}
			
		}
	}
	
	public function buy_now($user_id,$product_id,$qty,$quote_id,$token)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	

		$product_sku=$product->getSku();
		
		$product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
		$in_stock=$product->getExtensionAttributes()->getStockItem()->getIsInStock();
		
		
		
		
		if($in_stock==0)
		{
			return 'out';
		}
		else if($product_qty<=$qty && $in_stock==1)
		{
			return 'qty'; 
			die; 	  
		}  
		else		
		{
			
			$sql = "delete from mobile_user_buy_now where user_id='".$user_id."'";
			$connection->query($sql);    
			
			$sql = "insert into mobile_user_buy_now (user_id,product_id) values ('".$user_id."','".$product_id."')";
			$connection->query($sql);    
			
			 
			$token=$token;
			$today = date('Y-m-d H:i:s');
						$select = $connection->select()
                  ->from('quote_item') 
                  ->where('quote_id = ?', $quote_id)
                  ->where('product_id = ?', $product_id);
			$result_curr = $connection->fetchAll($select);
			$cart_count=count($result_curr);
			if($cart_count==0)
			{
				//insert
				$productData = [
					'cart_item' => [
						'quote_id' => $quote_id,
						'sku' => $product_sku,
						'qty' => $qty   
					]
				];  
				$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
	 
				$result = curl_exec($ch);

				$result = json_decode($result, 1);
				
				
				return 'insert';
				die;	   

				
			}	
			else
			{
				foreach($result_curr as $item)
				{ 
					$cart_product_id=$item['product_id'];		 
					$cart_sku=$item['sku'];		 
					$cart_price=$item['price'];		 
					$cart_base_price=$item['base_price'];		
				
					
					$cart_item_id=$item['item_id'];	 
					$row_total=$qty*$cart_price;
					$row_base_total=$qty*$cart_base_price;
					
					$quote_qty=number_format($qty, 4, '.', '');
					$quote_row_total=number_format($row_total, 4, '.', '');
					$quote_base_total=number_format($row_base_total, 4, '.', ''); 
					
					
					$cartData = [
						'cart_item' => [
							'item_id' => $cart_item_id,
							'qty' => $qty, 
							'quote_id' => $quote_id   
						]
					]; 
					
					
					$ch = curl_init($site_url."index.php/rest/V1/carts/mine/items"); 
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($cartData));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " .$token));
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,1);
					
					
					$result = curl_exec($ch);

					$result = json_decode($result, 1); 
					
					 
					
					return 'same';  
					die;
					
					
				
				}
				
			}
			
		}
	}
	
	
	
	public function check_mobile_exists($mobile)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('attribute_id = ?', '178')
                  ->where('value = ?', $mobile);
        $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	public function check_mobile_temp_exists($mobile)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('otp_temp_user') 
                  ->where('mobile = ?', $mobile);
        $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
	}	
	
	public function registerToken($user_id=null,$device_id,$token)
	{
		$id=null;
		$tableName = "mobile_token_auth";
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$deviceexistid = $this->deviceIdGuestExist($device_id);
		
		if(!empty($user_id))
		{
			$select = $connection->select('id')
                  ->from($tableName) 
                  ->where('user_id = ?', $user_id);
       		$id = $connection->fetchOne($select);
			
			if(!empty($id))
			{
				$sql = "Update " . $tableName . " Set device_id = '$device_id',token = '$token' where id = $id";
				$connection->query($sql);
				return true;
			}
			else
			{
				if(!empty($deviceexistid))
				{
					$sql = "Update " . $tableName ." Set token = '$token' ,user_id = '$user_id' where id = '$deviceexistid'";
					$connection->query($sql);
					return true;
				} else  
				{
					$sql = "INSERT  INTO  " . $tableName . " Set device_id = '$device_id',token = '$token' ,user_id = '$user_id'";
					$connection->query($sql);
					return true;
				}	
			}
		} else
		{
			if(!empty($deviceexistid))
			{
				$sql = "Update " . $tableName ." Set token = '$token'  where id = '$deviceexistid'";
				$connection->query($sql);
				return true;
			}
			else{
				$sql = "INSERT  INTO  " . $tableName . " Set device_id = '$device_id',token = '$token' ,user_id = '$user_id'";
				$connection->query($sql);
				return true;
			}
		}
		return false;

	}

	public function deviceIdGuestExist($device_id)
	{
		$id=false;
		$tableName = "mobile_token_auth";
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		if(!empty($device_id))
		{
			$select = $connection->select('id')
                  ->from($tableName) 
                  ->where('device_id = ?', $device_id)
                  ->where('user_id = ?', 0);
			   $id = $connection->fetchOne($select);
		}
		return $id;
	}
	
	public function deviceIdExist($device_id)
	{
		$id=false;
		$tableName = "mobile_token_auth";
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		if(!empty($device_id))
		{
			$select = $connection->select('id')
                  ->from($tableName) 
                  ->where('device_id = ?', $device_id);
			   $id = $connection->fetchOne($select);
		}
		return $id;
	}

	public function getUserIdByToken($token)
	{
		$user_id=false;
		$tableName = "mobile_token_auth";
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		if(!empty($token))
		{
			$select = $connection->select()
			->from($tableName) 
                  ->where('token = ?', $token);
			$mobile_token_auth_collection = $connection->fetchAll($select);
			if(!empty($mobile_token_auth_collection))
			{
				$user_id = $mobile_token_auth_collection[0]['user_id'];
			}
		}
		return $user_id;
	}

	public function check_email_exists($email)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1); 
		$CustomerModel->loadByEmail($email);
		$userId = $CustomerModel->getId();
		
		
		if(!empty($userId))
		{
			return 1;	
		}
		else
		{
			return 0;
		} 
		
		
	}	
	
	
	public function check_user_exists($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1); 
		$CustomerModel->load($user_id);
		$userEmail = $CustomerModel->getEmail();
		if(!empty($userEmail))
		{
			return 1;		
		}	
		else
		{
			return 0;
		}
	}

	/* created 
	to  remove secutity (vunrability idor  issue resolving) @ritesh02122020  
	* object manager  dependencies cannot be removed as  that  can break existing functnality
	*/
	public function authenticateUser($user_id,$token)
	{
		$userIdByToken = $this->getUserIdByToken($token);
		if($userIdByToken==$user_id)
		{
			return array('success' => 1, 'result' => "", 'error' => 'User Authenticated');
		}
		return array('success' => 0, 'result' => "", 'error' => 'User Authentication Failed.');
	}

	public function authenticateOrder($user_id,$order_id)
	{
		$om = \Magento\Framework\App\ObjectManager::getInstance();  
		$resource = $om->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = $connection->select()
			->from(
				['ce' => 'sales_order'],
				['customer_id']
			)->where('entity_id=?',$order_id);
		$customer_id = $connection->fetchOne($select);
		if($user_id==$customer_id)
		{
			return array('success' => 1, 'result' => "", 'error' => 'Order Authenticated');
		}
		return array('success' => 0, 'result' => "", 'error' => 'Order Authentication Failed.');

	}

	public function check_order_exists($order_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		$state->setAreaCode('global');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  
		if(!empty($order->getData()))
		{
			return 1;		
		}	
		else
		{
			return 0;
		}
	} 

	
	
	
	public function check_email_temp_exists($email)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('otp_temp_user') 
                  ->where('email = ?', $email);
        $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		} 
		
		
	}	
	
	
	public function create_otp($array)
	{
		 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
		
		$email=$array['email'];
		$password=base64_encode($array['password']); 
		$mobile=$array['mobile']; 
		$groupid=$array['groupid']; 
		
		$pin = mt_rand(1000, 9999);
		// $pin = 1111;		
		$today=date('Y-m-d H:i:s'); 
		
		
		$sql="insert into otp_temp_user (email,mobile,password,group_id,pin_code,cr_date,no_times) values ('".$email."','".$mobile."','".$password."','".$groupid."','".$pin."','".$today."','1')"; 
		$connection->query($sql);	   
		  
		$helpers->CustomResendOTPMobile($pin,$mobile,$email); 
		//$smsHelper->send_sms($mobile,$pin); 
		
		return 1;  
		
		
		
	}	
	
	
	public function update_otp($mobile)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		
		
		$pin = mt_rand(1000, 9999);		
		$today=date('Y-m-d H:i:s');
		
		
		$sql="update otp_temp_user set pin_code='".$pin."',cr_date='".$today."' where mobile='".$mobile."'";
		$connection->query($sql);	  
		

		$select = $connection->select()
		->from('otp_temp_user') 
		->where('mobile = ?', $mobile);
		 
		$results = $connection->fetchAll($select);
		if(!empty($results)) 
		{
			$email=$results[0]['email'];
			$helpers->CustomResendOTPMobile($pin,$mobile,$email);  
			
			$user_array=array('email'=>$email,'mobile'=>$mobile); 
			return json_encode($user_array);
			
		}

		
		
	}	
	
	
	public function address_type($address_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
		

		$select = $connection->select()
		->from('customer_address_type') 
		->where('address_id = ?', $address_id);
		 
		$results = $connection->fetchAll($select);
		if(!empty($results)) 
		{
			return $results[0]['address_type'];
			
		}
		else
		{
			return 0;
		}

	}


	public function delete_address($address_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
		$select = $connection->select()
		->from('customer_address_entity') 
		->where('entity_id = ?', $address_id);
		 
		$results = $connection->fetchAll($select);
		if(!empty($results))    
		{
			$sql="delete from customer_address_entity where entity_id='".$address_id."'"; 
			$connection->query($sql);

			$sql="delete from customer_address_type where address_id='".$address_id."'";  
			$connection->query($sql);	
			
			return 1;   
			  
		}   
		else 
		{
			return 0;
		}
		
		
		
	}	
	
	
	public function get_product_price($product_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$now = date('Y-m-d H:i:s');
		$collectionfirst= $objectManager->create('Magento\Catalog\Model\Product')->getCollection();            
		$collectionfirst->addAttributeToSelect('special_from_date')
		->addAttributeToSelect('special_to_date')
		->addAttributeToFilter('special_price', ['neq' => ''])
		->addAttributeToFilter(
				'special_from_date',
				['lteq' => date('Y-m-d H:i:s', strtotime($now))]
			)

		 ->addAttributeToFilter(
				'special_to_date',
				['gteq' => date('Y-m-d H:i:s', strtotime($now))]

		);
		$excitingDeals = $collectionfirst->getData();
		
		/* echo "<pre>";
			print_r($excitingDeals);
		echo "</pre>";  */
		
		$product_price=0;
		
		if(!empty($excitingDeals)) 
		{
			foreach ($excitingDeals as $product){
				$ex_product_id=$product['entity_id'];
				$ex_product_special_price=$product['special_price'];
				
				if($product_id==$ex_product_id)
				{
					$product_price=$ex_product_special_price;
				}
				
			}	
		} 
		
		return $product_price;  
		
		
	}	
	 
	
	public function check_product_exist($product_id) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
		$productname='';
		$productname=$product->getName();
		if($productname=='')
		{
			return 0;
		}
		else      
		{
			return 1;
		}
	}
	
	
	public function check_customer_address_exists($address_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('customer_address_entity') 
                  ->where('entity_id = ?', $address_id);
                 
        $result = $connection->fetchAll($select); 
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	
	public function check_customer_addresssss_exists($address_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('customer_address_entity') 
                  ->where('entity_id = ?', $address_id);
                 
        $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{ 
			return 0;
		}
		
		
	}	
	
	
	
	public function check_customer_exists($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$state = $objectManager->get('\Magento\Framework\App\State'); 
		$state->setAreaCode('frontend');
		$websiteId = $storeManager->getWebsite()->getWebsiteId();
		
		$store = $storeManager->getStore();
		$storeId = $store->getStoreId();
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory'); 
		$customer=$customerFactory->create();
		$customer->setWebsiteId($websiteId);
		$customer->load($user_id);
		$data= $customer->getData();
		if(!empty($data))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
	
		
	}	
	
	public function set_default_shipping_address($address_id,$user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$sql="update customer_entity set default_shipping='".$address_id."' where entity_id='".$user_id."'";
		$connection->query($sql);	  
		
	}	
	
	public function set_default_billing_address($address_id,$user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$sql="update customer_entity set default_billing='".$address_id."' where entity_id='".$user_id."'";
		$connection->query($sql);	   
		
	}	
	
	public function check_default_address($address_id,$user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$select = $connection->select()
                  ->from('customer_entity') 
                  ->where('entity_id = ?', $user_id)
                  ->where('default_shipping = ?', $address_id);
                 
        $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;
		}
		
		
	}	
	
	public function check_user_locked($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		/* $select = $connection->select()
                  ->from('customer_entity') 
                  ->where('entity_id = ?', $user_id)
                  ->where('lock_expires is null');
                 
        $result = $connection->fetchAll($select); */
		
		$sql = "Select * FROM customer_entity where entity_id='".$user_id."' and lock_expires is null";
		$result = $connection->fetchAll($sql);
		
		if(!empty($result)) 
		{
			return 1;	
		}
		else
		{
			return 0; 
		}
		
		
	}	
	
	
	public function get_address_field($address_id,$field)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$select = $connection->select()
                ->from('customer_address_entity') 
				->where('entity_id = ?', $address_id);
                 
        $result = $connection->fetchAll($select);
		if(!empty($result))
		{	
			return $result[0][$field]; 
		}
		else
		{
			return 0;
		}		
	}	
	
	
	public function addtocart($user_id,$product_id,$qty) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	

		$product_sku=$product->getSku();
		
		$product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
		$in_stock=$product->getExtensionAttributes()->getStockItem()->getIsInStock();
		
		if($in_stock==0)
		{
			return 'out';
		}
		else if($product_qty<=$qty && $in_stock==1)
		{
			return 'qty'; 
			die; 	  
		}  
		else		
		{
			
			$select = $connection->select() 
					->from('mobile_user_cart') 
					->where('user_id = ?', $user_id)
					->where('product_id = ?', $product_id);
					 
			$result = $connection->fetchAll($select);
			$today = date('Y-m-d H:i:s');
			if(empty($result))
			{
				
				$sql = "Insert Into mobile_user_cart (user_id, product_id, qty, created_at) Values ('".$user_id."','".$product_id."','".$qty."','".$today."')";
				$connection->query($sql);		 	
			}
			else  
			{ 
				$id=$result[0]['id'];  
				//$qty=$result[0]['qty'];
				//$qty=$qty+1;
				 
				$sql = "update mobile_user_cart set qty='".$qty."',created_at='".$today."' where id='".$id."'"; 
				$connection->query($sql);	  
			}
			
			return 'yes'; 
		}
		
	}


	
	
	
	
	public function removecart($user_id,$product_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$sql="delete from mobile_user_cart where user_id='".$user_id."' and product_id='".$product_id."'";
		$connection->query($sql);	 
		
	}	
	
	
	public function get_user_cart($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$select = $connection->select()
                ->from('mobile_user_cart') 
				->where('user_id = ?', $user_id);
                 
        $result = $connection->fetchAll($select);
		$mainArray=array();
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$mainArray[]=array(
					'product_id'=>$row['product_id'],
					'qty'=>$row['qty']
				);		
			}	
		
			return $mainArray;
		}
		else
		{
			return 0;
		}
		
	}	
	
	public function get_user_cart_test($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$token=$this->get_token();
		$quote_id=$this->get_quote_id($token,$user_id);
		$quote_id=json_decode($quote_id);
		
		
		$quoteRepository= $objectManager->get('\Magento\Quote\Model\QuoteRepository'); 
		$quote = $quoteRepository->get($quote_id);
		$items = $quote->getAllItems();
		  
		//print_r($items); 
		
		/* $quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$allItems=$quote->getAllItems();
		
		echo "<pre>";
		print_r($allItems);
		echo "</pre>"; */
		
		
		
		
		
	}	
	
	public function check_product_in_whishlist($user_id,$product_id)
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		$wishlist = $objectManager->get('\Magento\Wishlist\Model\Wishlist');
		
		$wishlistCollection = $wishlist->loadByCustomerId($user_id)->getItemCollection();
		
		if(count($wishlistCollection)) {
			foreach ($wishlistCollection as $_item) {
				
				$productArray[]=$_item->getProduct()->getId();
				
			}
		} 
		
		
		
		if(empty($productArray))
		{
			return 0;	
		}
		else
		{
			if (in_array($product_id, $productArray)) {
				return 1;	
			}
			else
			{
				return 0;	
			}
		}
		
		
	}	
	
	public function wishlist($user_id,$product_id,$status)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
			if($status==1)
			{ 
				$productArray=array();
				//$wishlist = $objectManager->get('\Magento\Wishlist\Model\Wishlist');
				//$wishlistCollection = $wishlist->loadByCustomerId($user_id)->getItemCollection();
				
				$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
				$wishlistAdd = $wishList->create()->loadByCustomerId($user_id, true);
				$items = $wishlistAdd->getItemCollection();
				if(!empty($items))
				{	
					foreach ($items as $item) 
					{
						if ($item->getProductId() == $product_id) 
						{
							return 'already';		
							die;
						}
					}
				}
				
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
				$wishlistAdd->addNewItem($product);
				$wishlistAdd->save(); 
				
									
				$sql="delete from mobile_user_cart where user_id='".$user_id."' and product_id='".$product_id."'";
				$connection->query($sql);	
				
				return 'added'; 
				die;	
			}	
			if($status==0)
			{  
				$delete=0;
				$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
				$wishlistAdd = $wishList->create()->loadByCustomerId($user_id, true);
				$items = $wishlistAdd->getItemCollection();
				foreach ($items as $item) 
				{
					if ($item->getProductId() == $product_id) 
					{
						$item->delete();
						$wishlistAdd->save();   
						$delete=1;					
						break;
					}
				}
				
				
				if($delete==1)
				{
					return 'delete';  
					die;
				}	
				else 
				{
					return 'notinlist';	
					die;
				}			
					
			} 		
		
	}	
	
	public function get_user_details($user_id) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$customer = $objectManager->create(
			'Magento\Customer\Model\Customer'
		)->load($user_id);
		
		$array=$customer->getData();
		
		//print_r($array);
		
		$group_id=$array['group_id'];
		
		$type="";
		if($group_id==1)
		{	
			$type='Individual';
		}
		else if($group_id==5)
		{	
			$type='Service Engineer';
		}
		else if($group_id==4)
		{	
			$type='Business';
		}else if($group_id==2)
		{	
			$type='Wholesale';
		}else if($group_id==3)
		{	
			$type='Retailer';
		}
		
		$note='You are register as '.$type;
		
		$mobile='';
		
		if(!empty($array['mobile']))
		{
			$mobile=$array['mobile'];	
		}	
		
		$firstname='';	
		$lastname='';	
		
		if(isset($array['firstname']))
		{
			if($array['firstname']!='firstname')
			{
				$firstname=$array['firstname'];
			}
		}	
		if(isset($array['lastname']))
		{
			if($array['lastname']!='lastname')
			{
				$lastname=$array['lastname'];
			} 
		}
		$mainarray[]=array(
			'group_id'=>$group_id,
			'firstname'=>$firstname,
			'lastname'=>$lastname,
			'email'=>$array['email'],
			'mobile'=>$mobile,
			'note'=>$note, 
		);	 
		
		if($group_id==4)
		{
			
			$mainarray[]=array(
				'gst'=>$customer->getGstNumber(),
				'company_name'=>$customer->getCompanyNam(),
				'company_address'=>$customer->getcompanyAdd(),
				'pan'=>$customer->getPanNumber(),
				'approve'=>$customer->getAttributeApproved(),
			);	 			 
		} 
		
		return $mainarray;
	}	
	
	public function get_product_detail($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
		
		if(!empty($product->getImage()))
		{
			$image = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
		} 
		else
		{
			$site_url = $storeManager->getStore()->getBaseUrl();
			$imageUrl = $site_url.'Customimages/product-thumb.jpg';
			$image = $imageUrl;
		}
		
		
		$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product->getId());
		if(!empty($RatingOb->getCount())){
			$ratings = $RatingOb->getSum()/$RatingOb->getCount();
		}
		
		$productdata['rating_counts'] = $RatingOb->getCount();
		
		if(!empty($ratings)){
			$product_rating = $ratings;
		}else{
			$product_rating = null;
		}
		
		
		$product_array=array(
			'id'=>$product->getId(), 	 
			'name'=>$product->getName(),	
			'sku'=>$product->getSku(),
			'image'=>$image,	
			'rating'=>$product_rating    
		);
		
		//print_r($product_array);
		return $product_array;
		
	}	
	 
	
	public function get_order_summary($order_id,$user_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
  
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  
		$shippingAmount=0;
		$total_shippingAmount=0;
		$discountAmount=0; 
		$total_gst=0; 
		$total_price=0;   
		
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			$qty=$item->getQtyOrdered();
			$product = $productRepository->getById($product_id); 
			$check_price=$api_helpers->get_product_price($product_id);
	
			if($check_price==0)
			{
				$price=$product->getPrice() * $qty;	
			}
			else
			{
				$price=$check_price * $qty;			
			}	 	

 
			$shippingGstRate = $product->getGstRate(); 

			$gstPercent = 100 + $shippingGstRate;
			$rowTotal = $price;	 
			
			$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
				
			$gstAmount = $productPrice * $shippingGstRate;
			$total_gst = $total_gst + $gstAmount;
			$total_price=$total_price + $rowTotal;
			
			  
			$pincode=$custom_helpers->get_customer_shipping_address($user_id,'postcode');
			if($pincode!=0)
			{	
				$shippingAmount=$data_helpers->ecom_price_by_product($pincode,$product_id,$qty);
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
					$couponCode = $cart->getQuote()->getCouponCode();
					$couponCode = $cart->getQuote()->getCouponCode();
			        $ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
			        $rule = $saleRule->load($ruleId);
			        $freeShippingCoupon = $rule->getSimpleFreeShipping();
					if($freeShippingCoupon){
		               $shippingAmount = 0 ;
		            } 
				$total_shippingAmount=$total_shippingAmount+$shippingAmount;
			} 
			
			
			 
		
		} 
		
		
		$total_base_amount=$total_price - $total_gst;
			
		

		$total_gst=number_format($total_gst, 2, '.', '');
		$total_base_amount=number_format($total_base_amount, 2, '.', '');
		  
		$subtotal=$total_gst + $total_base_amount;
		$total_shippingAmount=number_format($total_shippingAmount, 2, '.', '');

		$order_total=$subtotal + $total_shippingAmount;

		$total_gst.' '.$total_base_amount.' '.$subtotal.' '.$order_total;	

		$subtotal=number_format($subtotal, 2, '.', '');
		$order_total=number_format($order_total, 2, '.', '');
		 
		$summary_array=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total);
		
		return $summary_array;
		
		
		
		
	}	
	
	
	public function get_order_status($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
  
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  		
		
		$status=$order->getStatus(); 
		if($status=='processing')
		{
			$order_status=1;	
		}
		else if($status=='complete')
		{
			$order_status=2;	
		} 	
		
		$order_status=1;	
		
		return $order_status;
	
	}	
	
	
	public function get_address_details($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','shipping');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','shipping');
		$delivery_name=$delivery_firstname.' '.$delivery_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		
		
		$line_1='';
		$line_2=''; 
		$line_3='';
		
		$streetarray=explode(PHP_EOL,$delivery_street);
		if(isset($streetarray[0]))
		{
			$line_1=$streetarray[0];	
		}
		if(isset($streetarray[1]))
		{
			$line_2=$streetarray[1];	
		}	
		if(isset($streetarray[2]))
		{
			$line_3=$streetarray[2];	
		}	
		
		
		
		//$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode.".";  
		

		$mainarray=array(
			'name'=>$delivery_name,		
			'line_1'=>$line_1,		
			'line_2'=>$line_2,		
			'line_3'=>$line_3,		
			'city'=>$delivery_city,		
			'pincode'=>$delivery_pincode,		
			'region'=>$delivery_state,		
			'mobile'=>$delivery_telephone	
		);		 
 
		
		return $mainarray;
		
	}	

	public function get_billing_address_details($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$delivery_firstname.' '.$delivery_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','billing');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','billing');
		
		
		$line_1='';
		$line_2=''; 
		$line_3='';
		
		$streetarray=explode(PHP_EOL,$delivery_street);
		if(isset($streetarray[0]))
		{
			$line_1=$streetarray[0];	
		}
		if(isset($streetarray[1]))
		{
			$line_2=$streetarray[1];	
		}	
		if(isset($streetarray[2]))
		{
			$line_3=$streetarray[2];	
		}	
		
		
		
		//$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode.".";  
		

		$mainarray=array(
			'name'=>$delivery_name,		
			'line_1'=>$line_1,		
			'line_2'=>$line_2,		
			'line_3'=>$line_3,		
			'city'=>$delivery_city,		
			'pincode'=>$delivery_pincode,		
			'region'=>$delivery_state,		
			'mobile'=>$delivery_telephone	
		);		 
 
		
		return $mainarray;
		
	}	
	
	public function get_category_icon($cat_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$select = $connection->select()
					->from('catalog_category_entity_varchar') 
					->where('attribute_id = ?', '263')
					->where('entity_id = ?', $cat_id)
					->where('value is not null'); 
					 
		$result = $connection->fetchAll($select);
		  
		$link='';
		if(!empty($result))
		{   
			 if(substr_compare($result[0]['value'],"pub",0))
			 {
				$link=$site_url.''.$result[0]['value'];
			 } else  
			 {
				 $link=$site_url.'pub/media/catalog/category/'.$result[0]['value'];
			 }
		}
		else
		{
			$link=$site_url.'Customimages/cat-icon.svg';
		}
		
		return $link;
		
			
	}	
	
	
	public function get_mobile_order_manual() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		//$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');	
		     
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		
		$sql="select distinct payment_details from mobile_sales_order where order_increment_id is null and order_summary is null group by payment_details";
		$result = $connection->fetchAll($sql); 
		if(!empty($result)) 
		{ 
			foreach($result as $pay_details)
			{
				$payment_details=$pay_details['payment_details'];	
				//$api_helpers->create_order($payment_details);
				$api_helpers->split_order($payment_details);
			} 	    	
		}    
	}	
	
	
	public function get_mobile_order($user_id)  
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$state = $objectManager->get('Magento\Framework\App\State');
		$state->setAreaCode('global');	
		     
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		
		//$sql="select distinct payment_details from mobile_sales_order where order_increment_id is null and order_summary is null group by payment_details";    
		
		$sql="select distinct payment_details from mobile_sales_order where user_id='".$user_id."' and order_increment_id is null and order_summary is null and seller_payment is null group by payment_details";      
		$result = $connection->fetchAll($sql); 
		if(!empty($result)) 
		{ 
			foreach($result as $pay_details)
			{
				$payment_details=$pay_details['payment_details'];	
				//$api_helpers->create_order($payment_details);
				$api_helpers->split_order($payment_details,$user_id);
			} 	    	
		} 
	}	
	
	
	public function get_product_seller_id_mobile($product_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 

		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$id=0;
		$mpProductFactory = $objectManager->get('\Webkul\Marketplace\Model\ProductFactory');	
		
		
		$marketplaceCollection = $mpProductFactory->create()
				->getCollection()
				->addFieldToFilter(
					'mageproduct_id',
					$product_id
				);
		
		
		foreach ($marketplaceCollection as $vendor) {
			$id = $vendor->getSellerId();
		}
		
		return $id; 
		
		
	}	

	public function split_order($payment_details,$user_id)
	{
		/* refactored to  remove unsed object  @ritesh*/
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection(); 
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		/* added user  id parameter  to select only  item from the specific order  even if  the paymentid  is  same  */
		$sql="select * from mobile_sales_order where payment_details='".$payment_details."' and user_id=$user_id";
		$result = $connection->fetchAll($sql);
		$date_r=date('Ymdhis');   
		foreach($result as $row)
		{
			$product_id=$row['product_id'];	 
			$seller_id=$api_helpers->get_product_seller_id_mobile($product_id);
			$seller_payment=$payment_details.'_'.$seller_id.'_'.$date_r.'_'.$user_id; 
			$update="update mobile_sales_order set seller_id='".$seller_id."',seller_payment='".$seller_payment."' where payment_details='".$payment_details."' and product_id='".$product_id."'"; 
			$connection->query($update);	
			$sellerArray[]=$seller_id;
		}	
		$array = array_unique($sellerArray);
		foreach($array as $row)
		{
			$seller_id=$row;	
			$api_helpers->create_split_order($payment_details,$seller_id,$user_id);
		} 	 
	}	
	
	public function create_split_order($payment_details,$seller_id,$user_id)
	{
		/* refactored to  remove unsed object  @ritesh*/
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$sql="select seller_payment from mobile_sales_order where payment_details='".$payment_details."' and seller_id='".$seller_id."' group by seller_payment"; 
		$result = $connection->fetchAll($sql); 
		if(!empty($result)) 
		{ 
			foreach($result as $pay_details)
			{
				$seller_payment=$pay_details['seller_payment'];	
				$api_helpers->create_order_new($seller_payment,$user_id);
			} 		
		} 
	}		
	
	
	public function create_order_new($seller_payment,$user_id)
	{ 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		
		$sql="select * from mobile_sales_order where seller_payment='".$seller_payment."' and user_id=$user_id";
		$result = $connection->fetchAll($sql);
		
		$shippingAmount=0;
		$total_shippingAmount=0;
		$discountAmount=0; 
		$total_gst=0; 
		$total_price=0; 
		$total_base_amount=0; 
		
		foreach($result as $row)
		{
			$user_id=$row['user_id'];	
			$address_id=$row['address_id'];	
			$product_id[]=$row['product_id'];	
			$product_idd=$row['product_id'];	
			$qty[]=$row['qty'];	
			$qtyy=$row['qty'];	
			$payment_method=$row['payment_method'];	
			$payment_details=$row['payment_details'];	
			$cr_date=$row['cr_date'];	
			$product = $productRepository->getById($product_idd); 
			$check_price=$api_helpers->get_product_price($product_idd);
			if($check_price==0)
			{
				$price=$product->getPrice() * $qtyy;	
			}
			else
			{
				$price=$check_price * $qtyy;			
			}	 	
			$shippingGstRate = $product->getGstRate(); 
			$gstPercent = 100 + $shippingGstRate;
			$rowTotal = $price;	 
			$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
			$gstAmount = $productPrice * $shippingGstRate;
			$total_gst = $total_gst + $gstAmount;
			$total_price=$total_price + $rowTotal;
			$pincode=$custom_helpers->get_customer_shipping_address($user_id,'postcode');

			if($pincode!=0)
			{	
				
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$helper_ecom_expreess = $objectManager->create('OM\ExpressShipping\Model\Carrier\Expressshipping');
				$shippingAmount=$helper_ecom_expreess->ecom_price_by_product($pincode,$product_idd,$qtyy); 
				$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
				$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
				$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
				$couponCode = $cart->getQuote()->getCouponCode();
				$couponCode = $cart->getQuote()->getCouponCode();
				$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
				$rule = $saleRule->load($ruleId);
				$freeShippingCoupon = $rule->getSimpleFreeShipping();
				if($freeShippingCoupon){
							$shippingAmount = 0 ;
				}
				$total_shippingAmount=$total_shippingAmount+$shippingAmount;
			}   
		}

		$total_base_amount=$total_price - $total_gst;
		$total_gst=number_format($total_gst, 2, '.', '');
		$total_base_amount=number_format($total_base_amount, 2, '.', '');
		  
		$subtotal=$total_gst + $total_base_amount;
		$total_shippingAmount=number_format($total_shippingAmount, 2, '.', '');
		
		/* change  shippng amount based  on order subtotal @ritesh20022021*/
		$helper_td = $objectManager->create('Baljit\Buynow\Helper\Data');
		$free_active = $helper_td->getConfig('carriers/express_shipping/free_active');
		$minAmount = $helper_td->getConfig('carriers/express_shipping/free_price');
		if($free_active && $subtotal >= $minAmount)
		{
			$total_shippingAmount=0;
		}
		$order_total=$subtotal + $total_shippingAmount;
		
		$total_gst.' '.$total_base_amount.' '.$subtotal.' '.$order_total;	
		$subtotal=number_format($subtotal, 2, '.', '');
		$order_total=number_format($order_total, 2, '.', '');
		$summary_array=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total);
		$order_summary=json_encode($summary_array); 
		
		$firstname=$api_helpers->get_address_field($address_id,'firstname');
		$lastname=$api_helpers->get_address_field($address_id,'lastname');
		$city=$api_helpers->get_address_field($address_id,'city');
		$country_id=$api_helpers->get_address_field($address_id,'country_id');
		$region=$api_helpers->get_address_field($address_id,'region');
		$region_id=$api_helpers->get_address_field($address_id,'region_id');
		$postcode=$api_helpers->get_address_field($address_id,'postcode');
		$telephone=$api_helpers->get_address_field($address_id,'telephone');
		$street=$api_helpers->get_address_field($address_id,'street');
		$streetarray=explode(PHP_EOL,$street);
		$street_1='';
		$street_2='';
		$street_3='';
		if(isset($streetarray[0]))
		{
			$street_1=$streetarray[0];	
		}	
		if(isset($streetarray[1]))
		{
			$street_2=$streetarray[1];	
		}	
		if(isset($streetarray[2]))
		{
			$street_3=$streetarray[2];	
		}	
		
		$address = array(
			'customer_address_id' => '',
			'prefix' => '',
			'firstname' => $firstname,
			'lastname' => $lastname,
			'suffix' => '',
			'company' => '', 
			'street' => array(
				'0' => $street_1, // this is mandatory
				'1' => $street_2, // this is optional
				'2' => $street_3 // this is optional
			),
			'city' => $city,
			'country_id' => $country_id, // two letters country code
			'region' => $region, // can be empty '' if no region
			'region_id' => $region_id, // can be empty '' if no region_id
			'postcode' => $postcode,
			'telephone' => $telephone,
			'fax' => '',
			'currency_id'  => 'INR',
			'save_in_address_book' => 0 
		);  
		$prod_string='';
		$i=0;
		foreach($product_id as $id)
		{
			$itemarray[]=array(
				'product_id'=>$id,
				'qty'=>$qty[$i],
			);	 
		$i++;
		}	
		foreach($itemarray as $k=>$v) {
			$mainArray[$v['product_id']] = $v['qty'];
		}
		$productIds = $mainArray;
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$store = $storeManager->getStore();
		$websiteId = $storeManager->getStore()->getWebsiteId();
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1); 
		$CustomerModel->load($user_id);
		$email = $CustomerModel->getEmail();  
		if($email!='')
		{
			$customer = $customerFactory->setWebsiteId($websiteId)->loadByEmail($email);
			$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customer->getId());
			try {  
			 
				$quoteFactory = $objectManager->get('\Magento\Quote\Model\QuoteFactory')->create();
				$quoteFactory->setStore($store);
				$quoteFactory->setCurrency();
				$quoteFactory->assignCustomer($customer);
				foreach($productIds as $productId => $qty) {
					$product = $objectManager->get('\Magento\Catalog\Model\ProductRepository')->getById($productId);// get product by product id 
					$quoteFactory->addProduct($product, $qty);  // add products to quote
				} 
				
				/*
				 * Set Address to quote 
				 */
				$quoteFactory->getBillingAddress()->addData($address);
				$quoteFactory->getShippingAddress()->addData($address);
				
				/*
				 * Collect Rates and Set Shipping & Payment Method
				 */
				$shippingAddress = $quoteFactory->getShippingAddress();
				
				/* use actual shipping  instead  of  floatreate  @ritesh11022021*/
				$shippingRate = $objectManager->create('\Magento\Quote\Model\Quote\Address\Rate');// get product by product id 
				$shippingRate->setCode('express_shipping_express_shipping')->getPrice(1);
				$shippingAddress->setCollectShippingRates(true)
		    ->collectShippingRates()
    		->setShippingMethod('express_shipping_express_shipping'); //shipping method
				$quoteFactory->getShippingAddress()->addShippingRate($shippingRate);

				// $shippingAddress->setCollectShippingRates(true)
				// 				->collectShippingRates()
				// 				->setShippingMethod('flatrate_flatrate');  
			 
				$quoteFactory->setPaymentMethod('cashondelivery'); //payment method
				$quoteFactory->setInventoryProcessed(false);
				$quoteFactory->save();    
				/*
				 * Set Sales Order Payment
				 */
				$quoteFactory->getPayment()->importData(['method' => 'cashondelivery']);
				/*
				 * Collect Totals & Save Quote
				 */
				$quoteFactory->setBaseShippingAmount($total_shippingAmount);    
				$quoteFactory->setShippingAmount($total_shippingAmount);    
				$quoteFactory->setBaseGrandTotal($order_total);    
				$quoteFactory->setGrandTotal($order_total);  
				$quoteFactory->collectTotals()->save();
				$quoteArray=$quoteFactory->getData();
				$quote_id=$quoteArray['entity_id']; 
				//$sql = "update quote_address set shipping_amount='".$total_shippingAmount."',base_shipping_amount='".$total_shippingAmount."' where quote_id='".$quote_id."' and address_type='shipping'";  
				//$connection->query($sql);	  
				//print_r($quoteArray);
				//die; 
				$model = $objectManager->get('\Magento\Quote\Model\QuoteManagement');
				$order_id = $model->placeOrder($quote_id);   
				$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
				//print_r($orderArray); 
				//echo "aaaaaa";    
				//die; 
				/* 
				 * Create Order From Quote 
				 */
				//$order = $objectManager->get('\Magento\Quote\Model\QuoteManagement')->submit($quoteFactory);
				$order->setEmailSent(1);
				
				
				//echo 'Order Id:' . $order->getRealOrderId();
				$increment_id=$order->getRealOrderId();
				if($telephone)
				{
					$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper'); 	
					$msg = "Dear User, your GetMySpares Order ".$increment_id ."has been successfully placed. Track your order on GetMySpares order details page";
					$smsHelper->send_custom_sms($telephone,$msg);
				}
				$sql="update mobile_sales_order set order_increment_id='".$increment_id."',order_summary='".$order_summary."' where seller_payment='".$seller_payment."' ";    
				$connection->query($sql);	     
				 
				$api_helpers->is_order_online($order->getId(),$total_shippingAmount,$payment_method,$payment_details);		
			   	//$api_helpers->is_order_online_new($order->getId(),$total_shippingAmount,$payment_method,$seller_payment,$payment_details);		
				//$order = $objectManager->create('\Magento\Sales\Model\Order')->load($order->getId());
				//$api_helpers->assign_order_to_seller($order->getId());          
				//$emailSender = $objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
				//$emailSender->send($order); 
				     
				$message_text='
				<p>Order Created on mobile app</p>
				<p>User Id : '.$user_id.'</p>
				<p>User Email : '.$email.'</p>
				<p>Order Inc ID : '.$increment_id.'</p>
				'; 
				$subject='Mobile Order - '.$increment_id;   
				$data_helpers->test_email($message_text,'101',$subject);
				
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
		else
		{ 
			$delete="delete from  mobile_sales_order where user_id='".$user_id."'"; 
			$connection->query($delete);	
		}  
	}	
	
	
	 
	
	
	
	public function create_order($payment_details)
	{ 
		 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();

		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		
		$sql="select * from mobile_sales_order where payment_details='".$payment_details."'";
		
		$result = $connection->fetchAll($sql);
		
		$shippingAmount=0;
		$total_shippingAmount=0;
		$discountAmount=0; 
		$total_gst=0; 
		$total_price=0; 
		$total_base_amount=0; 
		foreach($result as $row)
		{
			$user_id=$row['user_id'];	
			$address_id=$row['address_id'];	
			$product_id[]=$row['product_id'];	
			$product_idd=$row['product_id'];	
			$qty[]=$row['qty'];	
			$qtyy=$row['qty'];	
			$payment_method=$row['payment_method'];	
			$cr_date=$row['cr_date'];	
				
			
			
			$product = $productRepository->getById($product_idd); 
			
			$check_price=$api_helpers->get_product_price($product_idd);
			if($check_price==0)
			{
				$price=$product->getPrice() * $qtyy;	
			}
			else
			{
				$price=$check_price * $qtyy;			
			}	 	


			$shippingGstRate = $product->getGstRate(); 

			$gstPercent = 100 + $shippingGstRate;
			
			
			$rowTotal = $price;	 

			$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
					
			$gstAmount = $productPrice * $shippingGstRate;
			$total_gst = $total_gst + $gstAmount;
			$total_price=$total_price + $rowTotal;
			
			
			$pincode=$custom_helpers->get_customer_shipping_address($user_id,'postcode');
			if($pincode!=0)
			{	
				$shippingAmount=$data_helpers->ecom_price_by_product($pincode,$product_idd,$qtyy); 
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
				$couponCode = $cart->getQuote()->getCouponCode();
				$couponCode = $cart->getQuote()->getCouponCode();
		        $ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
		        $rule = $saleRule->load($ruleId);
		        $freeShippingCoupon = $rule->getSimpleFreeShipping();
				if($freeShippingCoupon){
	               $shippingAmount = 0 ;
	            }
				$total_shippingAmount=$total_shippingAmount+$shippingAmount;
			}   
			 
			  
			
		}	
		$total_base_amount=$total_price - $total_gst;
		$total_gst=number_format($total_gst, 2, '.', '');
		$total_base_amount=number_format($total_base_amount, 2, '.', '');
		  
		$subtotal=$total_gst + $total_base_amount;
		$total_shippingAmount=number_format($total_shippingAmount, 2, '.', '');

		$order_total=$subtotal + $total_shippingAmount;

		$total_gst.' '.$total_base_amount.' '.$subtotal.' '.$order_total;	

		$subtotal=number_format($subtotal, 2, '.', '');
		$order_total=number_format($order_total, 2, '.', '');
		 
		$summary_array=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total);
		
		
		$order_summary=json_encode($summary_array); 
		 
		
		/* echo "<pre>";
			print_r($summary_array); 
		echo "</pre>"; 
		die; */
		
		$firstname=$api_helpers->get_address_field($address_id,'firstname');
		$lastname=$api_helpers->get_address_field($address_id,'lastname');
		$city=$api_helpers->get_address_field($address_id,'city');
		$country_id=$api_helpers->get_address_field($address_id,'country_id');
		$region=$api_helpers->get_address_field($address_id,'region');
		$region_id=$api_helpers->get_address_field($address_id,'region_id');
		$postcode=$api_helpers->get_address_field($address_id,'postcode');
		$telephone=$api_helpers->get_address_field($address_id,'telephone');
		$street=$api_helpers->get_address_field($address_id,'street');
		
		
		$streetarray=explode(PHP_EOL,$street);
		
		$street_1='';
		$street_2='';
		$street_3='';
		
		if(isset($streetarray[0]))
		{
			$street_1=$streetarray[0];	
		}	
		
		if(isset($streetarray[1]))
		{
			$street_2=$streetarray[1];	
		}	
		
		if(isset($streetarray[2]))
		{
			$street_3=$streetarray[2];	
		}	
			 
		 
		
		
		$address = array(
			'customer_address_id' => '',
			'prefix' => '',
			'firstname' => $firstname,
			'lastname' => $lastname,
			'suffix' => '',
			'company' => '', 
			'street' => array(
				'0' => $street_1, // this is mandatory
				'1' => $street_2, // this is optional
				'2' => $street_3 // this is optional
			),
			'city' => $city,
			'country_id' => $country_id, // two letters country code
			'region' => $region, // can be empty '' if no region
			'region_id' => $region_id, // can be empty '' if no region_id
			'postcode' => $postcode,
			'telephone' => $telephone,
			'fax' => '',
			'currency_id'  => 'INR',
			'save_in_address_book' => 1 
		);
		
		
		/* echo "<pre>";
			print_r($address); 
		echo "</pre>";  */
		
		$prod_string='';
		$i=0;
		foreach($product_id as $id)
		{
			$itemarray[]=array(
				'product_id'=>$id,
				'qty'=>$qty[$i],
			);	 
			
		$i++;
		}	
		
		foreach($itemarray as $k=>$v) {
			$mainArray[$v['product_id']] = $v['qty'];
		}
		
		 
		//$productIds = array(1258 => 1, 1259 => 1);     
		$productIds = $mainArray;
		
		
		/* echo "<pre>";
			print_r($productIds);
		echo "</pre>"; */
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
  
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$store = $storeManager->getStore();
		$websiteId = $storeManager->getStore()->getWebsiteId();
		
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
		
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1); 
		$CustomerModel->load($user_id);
		$email = $CustomerModel->getEmail();
		
		
		if($email!='')
		{
			$customer = $customerFactory->setWebsiteId($websiteId)->loadByEmail($email);
			$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customer->getId());
			try {  
			 
				$quoteFactory = $objectManager->get('\Magento\Quote\Model\QuoteFactory')->create();
				$quoteFactory->setStore($store);
				$quoteFactory->setCurrency();
				$quoteFactory->assignCustomer($customer);
			  
				//$productIds = array(1258 => 1, 1259 => 1);        
				foreach($productIds as $productId => $qty) {
					$product = $objectManager->get('\Magento\Catalog\Model\ProductRepository')->getById($productId);// get product by product id 
					$quoteFactory->addProduct($product, $qty);  // add products to quote
				} 
				
				/*
				 * Set Address to quote 
				 */
				$quoteFactory->getBillingAddress()->addData($address);
				$quoteFactory->getShippingAddress()->addData($address);
			 
				/*
				 * Collect Rates and Set Shipping & Payment Method
				 */
				$shippingAddress = $quoteFactory->getShippingAddress();
				$shippingAddress->setCollectShippingRates(true)
								->collectShippingRates()
								->setShippingMethod('flatrate_flatrate'); //shipping method
			 
				$quoteFactory->setPaymentMethod('cashondelivery'); //payment method
				$quoteFactory->setInventoryProcessed(false);
				$quoteFactory->save();
			 
				/*
				 * Set Sales Order Payment
				 */
				$quoteFactory->getPayment()->importData(['method' => 'cashondelivery']);
			 
				/*
				 * Collect Totals & Save Quote
				 */
				$quoteFactory->collectTotals()->save();
			 
				/*
				 * Create Order From Quote 
				 */
				$order = $objectManager->get('\Magento\Quote\Model\QuoteManagement')->submit($quoteFactory);
				$order->setEmailSent(1); 
				//echo 'Order Id:' . $order->getRealOrderId();
				
				$increment_id=$order->getRealOrderId();
				$sql="update mobile_sales_order set order_increment_id='".$increment_id."',order_summary='".$order_summary."' where payment_details='".$payment_details."' ";    
				$connection->query($sql);	    
				
				
				$api_helpers->is_order_online($order->getId(),$total_shippingAmount,$payment_method,$payment_details);		
				
				$order = $objectManager->create('\Magento\Sales\Model\Order')->load($order->getId());
				$api_helpers->assign_order_to_seller($order->getId());        
				
				$emailSender = $objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
				$emailSender->send($order);	      
				 
				 
				$message_text='
				<p>Order Created on mobile app</p>
				<p>User Id : '.$user_id.'</p>
				<p>User Email : '.$email.'</p>
				<p>Order Inc ID : '.$increment_id.'</p>
				'; 
				
				$subject='Mobile Order - '.$increment_id;   
				$data_helpers->test_email($message_text,'101',$subject);
				   
				 
				
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
		else
		{ 
			$delete="delete from  mobile_sales_order where user_id='".$user_id."'"; 
			$connection->query($delete);	
		}  
		  
	}
	
	public function get_order_gst_total($order_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
		$select = $connection->select()
			  ->from('sales_order_item') 
			  ->where('order_id = ?', $order_id);
			 
		$result = $connection->fetchAll($select);
		
		
		$total_cgst=0;
		$total_sgst=0;
		$total_igst=0;
		$total_utgst=0;
		
		foreach($result as $row)
		{
			$cgst_amount=$row['cgst_amount'];	
			//$base_cgst_amount=$row['base_cgst_amount'];	
			
			$sgst_amount=$row['sgst_amount'];	
			//$base_sgst_amount=$row['base_sgst_amount'];

			$igst_amount=$row['igst_amount'];	
			//$base_igst_amount=$row['base_igst_amount'];		
			
			$utgst_amount=$row['utgst_amount'];	
			//$base_utgst_amount=$row['base_utgst_amount'];	
		
			$total_cgst=$total_cgst + $cgst_amount;
			$total_sgst=$total_sgst + $sgst_amount;
			$total_igst=$total_igst + $igst_amount;
			$total_utgst=$total_utgst + $utgst_amount;
		
		}
		
		$total_cgst=number_format($total_cgst, 2, '.', '');
		$total_sgst=number_format($total_sgst, 2, '.', '');
		$total_igst=number_format($total_igst, 2, '.', '');
		$total_utgst=number_format($total_utgst, 2, '.', '');
		
		 
		$total_gst=$total_cgst + $total_sgst + $total_igst + $total_utgst;
		$total_gst=number_format($total_gst, 2, '.', '');

		$gst_array=array('total_gst'=>$total_gst,'total_cgst'=>$total_cgst,'total_sgst'=>$total_sgst,'total_igst'=>$total_igst,'total_utgst'=>$total_utgst);

		
		return json_encode($gst_array);
		
					
	}	 
			
		
	public function is_order_online($order_id,$shippingprice,$payment_method,$payment_details) 
	{    
		    
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
		//$shippingprice='100';  
	
		/* removed changes as of  now 11feb2021 */
		$order->setShippingMethod('ecomexpress_Shipping & Handling Fee');   
		$order->setShippingDescription('E-Com Express - Shipping & Handling Fee'); 
		
		$payment_array=array('method_title'=>'Pay Online');
		$payment_info=json_encode($payment_array);
		 
		$order_id=$order->getId();
		$gst_array=json_decode($api_helpers->get_order_gst_total($order->getId()));
		$total_gst=$gst_array->total_gst;
		$total_cgst=$gst_array->total_cgst;
		$total_sgst=$gst_array->total_sgst;
		$total_igst=$gst_array->total_igst;
		$total_utgst=$gst_array->total_utgst;
		  
		$order->setCgstAmount($total_cgst);
		$order->setBaseCgstAmount($total_cgst);
		$order->setSgstAmount($total_sgst);
		$order->setBaseSgstAmount($total_sgst);
		$order->setIgstAmount($total_igst);
		$order->setBaseIgstAmount($total_igst);  
		$order->setUtgstAmount($total_utgst); 
		$order->setBaseUtgstAmount($total_utgst);    
		
		if($payment_method=='online')
		{	
			$sql="update sales_order_payment set method='razorpay',last_trans_id='".$payment_details."',additional_information='".$payment_info."' where parent_id='".$order_id."'";  
			$connection->query($sql);   	    
		}   
		$order->setShippingAmount($shippingprice);
		$order->setBaseShippingAmount($shippingprice);
		$order->setShippingInclTax($shippingprice);
		$order->setBaseShippingInclTax($shippingprice);
		$order->setBaseShippingInclTax($shippingprice);
		/* also  need to  set  that order came from mobile  appliation @ritesh26feb2021*/
		$order->setData('o_created_by',"mobileapp");
		$subtotal=$order->getSubtotal();
		$order->setBaseTotalPaid($subtotal + $shippingprice);  
		$order->setTotalPaid($subtotal + $shippingprice);   
		$order->setSubtotalInvoiced($subtotal);       
		$order->setTotalInvoiced($subtotal + $shippingprice);         
		$order->setBaseGrandTotal($subtotal + $shippingprice);          
		$order->setGrandTotal($subtotal + $shippingprice);     
		$order->setBaseShippingInvoiced($shippingprice); 	  
		$order->setShippingInvoiced($shippingprice);    
		$order->setBaseTotalInvoiced($subtotal + $shippingprice);  
		$order->save();	    
		
		$emailSender = $objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
		//$emailSender->send($order);  
		$api_helpers->create_mobile_order_invoice($order_id,$shippingprice,$subtotal);    
	}  
	
	
	public function create_mobile_order_invoice($order_id,$shippingprice,$subtotal) 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		if ($order->canInvoice()) {
			$invoice = $objectManager->create('Magento\Sales\Model\Service\InvoiceService')->prepareInvoice($order);
			if (!$invoice->getTotalQty()) {
				throw new \Magento\Framework\Exception\LocalizedException(
							__('You can\'t create an invoice without products.')
				);
			}
			$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
			$invoice->setBaseGrandTotal($shippingprice + $subtotal);
			$invoice->setGrandTotal($shippingprice + $subtotal);  
			$invoice->setShippingAmount($shippingprice);   
			$invoice->setBaseShippingAmount($shippingprice); 
			$invoice->register();    
			$invoice->save(); 
			
			$transaction = $objectManager->create('Magento\Framework\DB\Transaction')
				->addObject($invoice)
				->addObject($invoice->getOrder());
			$transaction->save();
			 			 
			$order->setIsCustomerNotified(true);
			$order->save();
		} 		
	}	
	
	
	public function check_order_from_mobile($order_inc_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('mobile_sales_order') 
				->where('order_increment_id = ?', $order_inc_id);
							
		$result = $connection->fetchAll($select);
		if(!empty($result))	
		{
			return 1;		
		}	 
		else
		{
			return 0; 
		}
	}	
	   
	public function check_order_tracking_exist($order_inc_id)   
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('order_tracking_status') 
				->where('order_inc_id = ?', $order_inc_id); 
							
		$result = $connection->fetchAll($select);
		if(!empty($result))	
		{
			return 1;		 
		}	
		else
		{
			return 0;
		}
	}	 
	
	public function check_rev_order_tracking_exist($order_inc_id,$product_id)   
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('rev_order_tracking_status')   
				->where('order_inc_id = ?', $order_inc_id) 
				->where('product_id = ?', $product_id); 
							
		$result = $connection->fetchAll($select);
		if(!empty($result))	
		{
			return 1;	 	
		}	
		else
		{
			return 0;
		} 
	}	


	public function get_order_tracking($order_id)   
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 
		
		
		   
		
		$select="select * from order_tracking_status where order_inc_id='".$order_inc_id."'";					
		$result = $connection->fetchAll($select);
		
		// echo "<pre>";
		// print_r($result);
		// echo "</pre>";
		
		if(!empty($result))
		{
			return $result[0]['result_status'];	
		}	
		else
		{ 
			return 0;
		}
		
	}	

	public function get_rev_order_tracking($order_id,$product_id)   
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 
		
		
		  
		$select = $connection->select()
				->from('rev_order_tracking_status') 
				->where('order_inc_id = ?', $order_inc_id)
				->where('product_id = ?', $product_id);  
							
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return $result[0]['result_status'];	
		}	
		else 
		{ 
			return 0;
		}
		
	}

	
	public function get_rev_order_tracking_single($order_id)    
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 
		
		
		  
		$select = $connection->select()
				->from('rev_order_tracking_status') 
				->where('order_inc_id = ?', $order_inc_id);  
							
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return $result[0]['result_status'];	
		}	
		else 
		{ 
			return 0;
		}
		
	}
	
		public function manifest_order_awb_with_error($awb,$orderid,$awb_type="PPD")
	{
		 
			
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order_id=$orderid;
		
		 
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 


		  
		$user_id = $order->getCustomerId();

		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		/*
		echo "<pre>";
				print_r($orderArray);
		echo "</pre>";  
		*/
  
		$order_cr_date=$order->getCreatedAt();
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			$product_title=$item->getName();
			$product_qty=$item->getQtyOrdered();
			/* echo "<pre>";
				print_r($item->getData());
			echo "</pre>";   */
			
			$itemarray=$item->getData();
			 
			$row_total_incl_tax=$itemarray['row_total_incl_tax'];
			$weight=$itemarray['weight'];
			
			
			$product_length=$custom_helpers->get_product_length($product_id);
			$product_breath=$custom_helpers->get_product_breath($product_id);
			$product_height=$custom_helpers->get_product_height($product_id);
			
			$item_ids=$item->getId();
            $seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
			//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
			
			
			$sellerarray=$custom_helpers->get_seller_details($seller_id);
			
			//print_r($sellerarray);
			
			
			$seller_name=$sellerarray['seller_name'];
			$seller_comp_nam=$sellerarray['seller_comp_nam'];
			$seller_comp_address=$sellerarray['seller_comp_address'];
			$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
			$zipcode=$sellerarray['zipcode'];
			$seller_gst=$sellerarray['seller_gst'];
			
			
			$cgst_amount=$itemarray['cgst_amount'];
			$sgst_amount=$itemarray['sgst_amount'];
			$igst_amount=$itemarray['igst_amount'];
			
			$cgst_percent=$itemarray['cgst_percent'];
			$sgst_percent=$itemarray['sgst_percent'];
			$igst_percent=$itemarray['igst_percent'];
			
			$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
			
			
			$additional_info=array(
				'INVOICE_NUMBER'=>$invoice_id,
				'INVOICE_DATE'=>$order_cr_date,
				'SELLER_GSTIN'=>$seller_gst, 
				'GST_HSN'=>$order_cr_date,
				'GST_TAX_NAME'=>'DELHI GST',
				'GST_TAX_BASE'=>$total_gst,
				'DISCOUNT'=>0,
				'GST_TAX_RATE_CGSTN'=>$cgst_percent,
				'GST_TAX_RATE_SGSTN'=>$sgst_percent,
				'GST_TAX_RATE_IGSTN'=>$igst_percent,
				'GST_TAX_TOTAL'=>$total_gst, 
				'GST_TAX_CGSTN'=>$cgst_amount,
				'GST_TAX_SGSTN'=>$sgst_amount,
				'GST_TAX_IGSTN'=>$igst_amount
			);
			
			/* echo "<pre>";
				print_r($additional_info);
			echo "</pre>"; */
			
			$collect_amount=0;
			if($awb_type!='PPD')
			{
				$collect_amount=$row_total_incl_tax;
			}
				
			
			$awbarray[]=array( 
				'AWB_NUMBER'=>$awb,
				'ORDER_NUMBER'=>$order_inc_id,
				'PRODUCT'=>$awb_type,
				'CONSIGNEE'=>$delivery_name,
				'CONSIGNEE_ADDRESS1'=>$street_1,
				'CONSIGNEE_ADDRESS2'=>$street_2,
				'CONSIGNEE_ADDRESS3'=>$street_3,
				'DESTINATION_CITY'=>$delivery_city,
				'PINCODE'=>$delivery_pincode,
				'STATE'=>$delivery_state,
				'MOBILE'=>$delivery_telephone,
				'TELEPHONE'=>$delivery_telephone,
				'ITEM_DESCRIPTION'=>$product_title, 
				'PIECES'=>round($product_qty), 
				'COLLECTABLE_VALUE'=>$collect_amount,
				'DECLARED_VALUE'=>$row_total_incl_tax,
				'ACTUAL_WEIGHT'=>$weight,
				'VOLUMETRIC_WEIGHT'=>0,
				'LENGTH'=>$product_length,
				'BREADTH'=>$product_breath,
				'HEIGHT'=>$product_height,
				'PICKUP_NAME'=>$seller_name,
				'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
				'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
				'PICKUP_PINCODE'=>$zipcode,
				'PICKUP_PHONE'=>$seller_comp_mobile,
				'PICKUP_MOBILE'=>$seller_comp_mobile,
				'RETURN_NAME'=>$seller_name,
				'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
				'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
				'RETURN_PINCODE'=>$zipcode,
				'RETURN_PHONE'=>$seller_comp_mobile,
				'RETURN_MOBILE'=>$seller_comp_mobile,
				'ADDONSERVICE'=>[""],
				'DG_SHIPMENT'=>false,
				'ADDITIONAL_INFORMATION'=>$additional_info
			); 
			
			
		}
		
		
		if( $seller_id!=29)
		{
			/* echo "<pre>";
				print_r($awbarray); 
			echo "</pre>";   */  

			
			$modearray=json_decode($api_helpers->get_razorpay_mode());
			//print_r($modearray); 
			$mode=$modearray->mode;
			$username=$modearray->username;
			$password=$modearray->password;
			$ecom_url=$modearray->url;
	 
			$url=$ecom_url.'apiv2/manifest_awb/';    	 	
			   
						
			
			$params = array(
						'username' => $username,
						  'password' => $password, 
						'json_input' => json_encode($awbarray)
					);   
			 
			 
			/*
			$params = array(
						'username' => "panasonicpvtltd402639_pro",
						  'password' => '6bk2LrZpC7UXhuZe', 
						'json_input' => json_encode($awbarray)
					);    	
			
			if($order_id=="92"){
				echo "<pre>";
				var_dump($awbarray);
				
			}*/	
	 
			$fields_string = http_build_query($params);
			// echo "<pre>";
			// echo json_encode($awbarray);
			// die();
			//open connection
			$ch = curl_init();  
			 
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			$result=json_decode($output);  
			curl_close($ch); 
			
			// var_dump($result);
			// die(); 
			if($result->shipments[0]->success==true )
			{
				$awb = @$result->shipments[0]->awb;
				if(!empty($awb))
				{
					$sql="update ecomexpress_awb set manifest='1' where awb='".$awb."'";  
					$connection->query($sql);
				} else
				{
					$sql="update ecomexpress_awb set manifest='1' where orderid='".$order_id."'";  
					$connection->query($sql);
				}
				return array("status"=>true,"response"=>"success");
			}  else 
			{
				$erorr = @$result->shipments[0]->reason;
				if($erorr=="AIRWAYBILL_IN_USE")
				{
					$usedawb = @$result->shipments[0]->awb;
					if(!empty($usedawb))
					{
						$blockmanifestedawb="update ecomexpress_awb set manifest='1' ,status='Assigned',state='1',shipment_to='Already Assignedd' where awb='$usedawb'";
						$connection->query($blockmanifestedawb);
					}
				}
				// 				echo "<pre>";
				// print_r($awbarray);
				// var_dump($result);
				// die();
				return array("status"=>false,"response"=>$erorr);
			}
			// return false;
		}
	}
	
	
	
	
	public function awb_to_manifest($awb,$orderid,$awb_type)
	{
		 
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order_id=$orderid;
		
		 
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 


		  
		$user_id = $order->getCustomerId();

		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		/*
		echo "<pre>";
				print_r($orderArray);
		echo "</pre>";  
		*/
  
		$order_cr_date=$order->getCreatedAt();
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			$product_title=$item->getName();
			$product_qty=$item->getQtyOrdered();
			/* echo "<pre>";
				print_r($item->getData());
			echo "</pre>";   */
			
			$itemarray=$item->getData();
			 
			$row_total_incl_tax=$itemarray['row_total_incl_tax'];
			$weight=$itemarray['weight'];
			
			
			$product_length=$custom_helpers->get_product_length($product_id);
			$product_breath=$custom_helpers->get_product_breath($product_id);
			$product_height=$custom_helpers->get_product_height($product_id);
			
			$item_ids=$item->getId();
            $seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
			//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
			
			
			$sellerarray=$custom_helpers->get_seller_details($seller_id);
			
			//print_r($sellerarray);
			
			
			$seller_name=$sellerarray['seller_name'];
			$seller_comp_nam=$sellerarray['seller_comp_nam'];
			$seller_comp_address=$sellerarray['seller_comp_address'];
			$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
			$zipcode=$sellerarray['zipcode'];
			$seller_gst=$sellerarray['seller_gst'];
			
			
			$cgst_amount=$itemarray['cgst_amount'];
			$sgst_amount=$itemarray['sgst_amount'];
			$igst_amount=$itemarray['igst_amount'];
			
			$cgst_percent=$itemarray['cgst_percent'];
			$sgst_percent=$itemarray['sgst_percent'];
			$igst_percent=$itemarray['igst_percent'];
			
			$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
			
			
			$additional_info[]=array(
				'INVOICE_NUMBER'=>$invoice_id,
				'INVOICE_DATE'=>$order_cr_date,
				'SELLER_GSTIN'=>$seller_gst, 
				'GST_HSN'=>$order_cr_date,
				'GST_TAX_NAME'=>'DELHI GST',
				'GST_TAX_BASE'=>$total_gst,
				'DISCOUNT'=>0,
				'GST_TAX_RATE_CGSTN'=>$cgst_percent,
				'GST_TAX_RATE_SGSTN'=>$sgst_percent,
				'GST_TAX_RATE_IGSTN'=>$igst_percent,
				'GST_TAX_TOTAL'=>$total_gst, 
				'GST_TAX_CGSTN'=>$cgst_amount,
				'GST_TAX_SGSTN'=>$sgst_amount,
				'GST_TAX_IGSTN'=>$igst_amount
			);
			
			/* echo "<pre>";
				print_r($additional_info);
			echo "</pre>"; */
			
			$collect_amount=0;
			if($awb_type!='PPD')
			{
				$collect_amount=$row_total_incl_tax;
			}
				
			
			$awbarray[]=array( 
				'AWB_NUMBER'=>$awb,
				'ORDER_NUMBER'=>$order_inc_id,
				'PRODUCT'=>$awb_type,
				'CONSIGNEE'=>$delivery_name,
				'CONSIGNEE_ADDRESS1'=>$street_1,
				'CONSIGNEE_ADDRESS2'=>$street_2,
				'CONSIGNEE_ADDRESS3'=>$street_3,
				'DESTINATION_CITY'=>$delivery_city,
				'PINCODE'=>$delivery_pincode,
				'STATE'=>$delivery_state,
				'MOBILE'=>$delivery_telephone,
				'TELEPHONE'=>$delivery_telephone,
				'ITEM_DESCRIPTION'=>$product_title, 
				'PIECES'=>round($product_qty), 
				'COLLECTABLE_VALUE'=>$collect_amount,
				'DECLARED_VALUE'=>$row_total_incl_tax,
				'ACTUAL_WEIGHT'=>$weight,
				'VOLUMETRIC_WEIGHT'=>0,
				'LENGTH'=>$product_length,
				'BREADTH'=>$product_breath,
				'HEIGHT'=>$product_height,
				'PICKUP_NAME'=>$seller_name,
				'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
				'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
				'PICKUP_PINCODE'=>$zipcode,
				'PICKUP_PHONE'=>$seller_comp_mobile,
				'PICKUP_MOBILE'=>$seller_comp_mobile,
				'RETURN_NAME'=>$seller_name,
				'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
				'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
				'RETURN_PINCODE'=>$zipcode,
				'RETURN_PHONE'=>$seller_comp_mobile,
				'RETURN_MOBILE'=>$seller_comp_mobile,
				'ADDONSERVICE'=>[""],
				'DG_SHIPMENT'=>false,
				'ADDITIONAL_INFORMATION'=>$additional_info
			); 
			
			
		}
		
		
		if( $seller_id!=29)
		{
			/* echo "<pre>";
				print_r($awbarray); 
			echo "</pre>";   */  

			
			$modearray=json_decode($api_helpers->get_razorpay_mode());
			//print_r($modearray); 
			$mode=$modearray->mode;
			$username=$modearray->username;
			$password=$modearray->password;
			$ecom_url=$modearray->url;
	 
			$url=$ecom_url.'apiv2/manifest_awb/';    	 	
			   
						
			
			$params = array(
						'username' => $username,
						  'password' => $password, 
						'json_input' => json_encode($awbarray)
					);   
			 
			 
			/*
			$params = array(
						'username' => "panasonicpvtltd402639_pro",
						  'password' => '6bk2LrZpC7UXhuZe', 
						'json_input' => json_encode($awbarray)
					);    	
			
			if($order_id=="92"){
				echo "<pre>";
				var_dump($awbarray);
				
			}*/	
	 
			$fields_string = http_build_query($params);

			//open connection
			$ch = curl_init();  
			 
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			
			$result=json_decode($output);  
			/* echo "<pre>";
				print_r($result);
			echo "</pre>";   */
			
			if($result->shipments[0]->success==1)
			{
				$sql="update ecomexpress_awb set manifest='1' where orderid='".$order_id."'";  
				$connection->query($sql);   
			}	 	
				
		
					
			curl_close($ch); 
		}
		
		
	}
	
	public function manifest_order_awb($awb,$orderid,$awb_type="PPD")
	{
		 
			
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order_id=$orderid;
		
		 
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 


		  
		$user_id = $order->getCustomerId();

		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		/*
		echo "<pre>";
				print_r($orderArray);
		echo "</pre>";  
		*/
  
		$order_cr_date=$order->getCreatedAt();
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			$product_title=$item->getName();
			$product_qty=$item->getQtyOrdered();
			/* echo "<pre>";
				print_r($item->getData());
			echo "</pre>";   */
			
			$itemarray=$item->getData();
			 
			$row_total_incl_tax=$itemarray['row_total_incl_tax'];
			$weight=$itemarray['weight'];
			
			
			$product_length=$custom_helpers->get_product_length($product_id);
			$product_breath=$custom_helpers->get_product_breath($product_id);
			$product_height=$custom_helpers->get_product_height($product_id);
			
			$item_ids=$item->getId();
            $seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
			//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
			
			
			$sellerarray=$custom_helpers->get_seller_details($seller_id);
			
			//print_r($sellerarray);
			
			
			$seller_name=$sellerarray['seller_name'];
			$seller_comp_nam=$sellerarray['seller_comp_nam'];
			$seller_comp_address=$sellerarray['seller_comp_address'];
			$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
			$zipcode=$sellerarray['zipcode'];
			$seller_gst=$sellerarray['seller_gst'];
			
			
			$cgst_amount=$itemarray['cgst_amount'];
			$sgst_amount=$itemarray['sgst_amount'];
			$igst_amount=$itemarray['igst_amount'];
			
			$cgst_percent=$itemarray['cgst_percent'];
			$sgst_percent=$itemarray['sgst_percent'];
			$igst_percent=$itemarray['igst_percent'];
			
			$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
			
			
			$additional_info[]=array(
				'INVOICE_NUMBER'=>$invoice_id,
				'INVOICE_DATE'=>$order_cr_date,
				'SELLER_GSTIN'=>$seller_gst, 
				'GST_HSN'=>$order_cr_date,
				'GST_TAX_NAME'=>'DELHI GST',
				'GST_TAX_BASE'=>$total_gst,
				'DISCOUNT'=>0,
				'GST_TAX_RATE_CGSTN'=>$cgst_percent,
				'GST_TAX_RATE_SGSTN'=>$sgst_percent,
				'GST_TAX_RATE_IGSTN'=>$igst_percent,
				'GST_TAX_TOTAL'=>$total_gst, 
				'GST_TAX_CGSTN'=>$cgst_amount,
				'GST_TAX_SGSTN'=>$sgst_amount,
				'GST_TAX_IGSTN'=>$igst_amount
			);
			
			/* echo "<pre>";
				print_r($additional_info);
			echo "</pre>"; */
			
			$collect_amount=0;
			if($awb_type!='PPD')
			{
				$collect_amount=$row_total_incl_tax;
			}
				
			
			$awbarray[]=array( 
				'AWB_NUMBER'=>$awb,
				'ORDER_NUMBER'=>$order_inc_id,
				'PRODUCT'=>$awb_type,
				'CONSIGNEE'=>$delivery_name,
				'CONSIGNEE_ADDRESS1'=>$street_1,
				'CONSIGNEE_ADDRESS2'=>$street_2,
				'CONSIGNEE_ADDRESS3'=>$street_3,
				'DESTINATION_CITY'=>$delivery_city,
				'PINCODE'=>$delivery_pincode,
				'STATE'=>$delivery_state,
				'MOBILE'=>$delivery_telephone,
				'TELEPHONE'=>$delivery_telephone,
				'ITEM_DESCRIPTION'=>$product_title, 
				'PIECES'=>round($product_qty), 
				'COLLECTABLE_VALUE'=>$collect_amount,
				'DECLARED_VALUE'=>$row_total_incl_tax,
				'ACTUAL_WEIGHT'=>$weight,
				'VOLUMETRIC_WEIGHT'=>0,
				'LENGTH'=>$product_length,
				'BREADTH'=>$product_breath,
				'HEIGHT'=>$product_height,
				'PICKUP_NAME'=>$seller_name,
				'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
				'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
				'PICKUP_PINCODE'=>$zipcode,
				'PICKUP_PHONE'=>$seller_comp_mobile,
				'PICKUP_MOBILE'=>$seller_comp_mobile,
				'RETURN_NAME'=>$seller_name,
				'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
				'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
				'RETURN_PINCODE'=>$zipcode,
				'RETURN_PHONE'=>$seller_comp_mobile,
				'RETURN_MOBILE'=>$seller_comp_mobile,
				'ADDONSERVICE'=>[""],
				'DG_SHIPMENT'=>false,
				'ADDITIONAL_INFORMATION'=>$additional_info
			); 
			
			
		}
		
		
		if( $seller_id!=29)
		{
			/* echo "<pre>";
				print_r($awbarray); 
			echo "</pre>";   */  

			
			$modearray=json_decode($api_helpers->get_razorpay_mode());
			//print_r($modearray); 
			$mode=$modearray->mode;
			$username=$modearray->username;
			$password=$modearray->password;
			$ecom_url=$modearray->url;
	 
			$url=$ecom_url.'apiv2/manifest_awb/';    	 	
			   
						
			
			$params = array(
						'username' => $username,
						  'password' => $password, 
						'json_input' => json_encode($awbarray)
					);   
			 
			 
			/*
			$params = array(
						'username' => "panasonicpvtltd402639_pro",
						  'password' => '6bk2LrZpC7UXhuZe', 
						'json_input' => json_encode($awbarray)
					);    	
			
			if($order_id=="92"){
				echo "<pre>";
				var_dump($awbarray);
				
			}*/	
	 
			$fields_string = http_build_query($params);

			//open connection
			$ch = curl_init();  
			 
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			$result=json_decode($output);  
			curl_close($ch); 
			
			// var_dump($result);
			// die();
			if($result->shipments[0]->success==true )
			{
				$sql="update ecomexpress_awb set manifest='1' where orderid='".$order_id."'";  
				$connection->query($sql);
				return true;   
			} 
			return false;
		}
	}
	
	
	public function awb_to_manifest_check($orderid,$awb_type)
	{
		 
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');

echo $sql="select awb from ecomexpress_awb where `orderid`='' and `status`='' and manifest is null limit 1";		  
		$new_awb = $connection->fetchOne($sql); 
		$awb=$new_awb;


		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order_id=$orderid;
		
		 
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 


		  
		$user_id = $order->getCustomerId();

		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		/*
		echo "<pre>";
				print_r($orderArray);
		echo "</pre>";  
		*/
  
		$order_cr_date=$order->getCreatedAt();
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			$product_title=$item->getName();
			$product_qty=$item->getQtyOrdered();
			/* echo "<pre>";
				print_r($item->getData());
			echo "</pre>";   */
			
			$itemarray=$item->getData();
			 
			$row_total_incl_tax=$itemarray['row_total_incl_tax'];
			$weight=$itemarray['weight'];
			
			
			$product_length=$custom_helpers->get_product_length($product_id);
			$product_breath=$custom_helpers->get_product_breath($product_id);
			$product_height=$custom_helpers->get_product_height($product_id);
			
			$item_ids=$item->getId();
            $seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
			//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
			
			
			$sellerarray=$custom_helpers->get_seller_details($seller_id);
			
			//print_r($sellerarray);
			
			
			$seller_name=$sellerarray['seller_name'];
			$seller_comp_nam=$sellerarray['seller_comp_nam'];
			$seller_comp_address=$sellerarray['seller_comp_address'];
			$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
			$zipcode=$sellerarray['zipcode'];
			$seller_gst=$sellerarray['seller_gst'];
			
			
			$cgst_amount=$itemarray['cgst_amount'];
			$sgst_amount=$itemarray['sgst_amount'];
			$igst_amount=$itemarray['igst_amount'];
			
			$cgst_percent=$itemarray['cgst_percent'];
			$sgst_percent=$itemarray['sgst_percent'];
			$igst_percent=$itemarray['igst_percent'];
			
			$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
			
			
			$additional_info=array(
				'INVOICE_NUMBER'=>$invoice_id,
				'INVOICE_DATE'=>$order_cr_date,
				'SELLER_GSTIN'=>$seller_gst, 
				'GST_HSN'=>$order_cr_date,
				'GST_TAX_NAME'=>'DELHI GST',
				'GST_TAX_BASE'=>$total_gst,
				'DISCOUNT'=>0,
				'GST_TAX_RATE_CGSTN'=>$cgst_percent,
				'GST_TAX_RATE_SGSTN'=>$sgst_percent,
				'GST_TAX_RATE_IGSTN'=>$igst_percent,
				'GST_TAX_TOTAL'=>$total_gst, 
				'GST_TAX_CGSTN'=>$cgst_amount,
				'GST_TAX_SGSTN'=>$sgst_amount,
				'GST_TAX_IGSTN'=>$igst_amount
			);
			
			echo "<pre>";
				print_r($additional_info);
			echo "</pre>"; 
			
			$collect_amount=0;
			if($awb_type!='PPD')
			{
				$collect_amount=$row_total_incl_tax;
			}
				
			
			$awbarray[]=array( 
				'AWB_NUMBER'=>$awb,
				'ORDER_NUMBER'=>$order_inc_id,
				'PRODUCT'=>$awb_type,
				'CONSIGNEE'=>$delivery_name,
				'CONSIGNEE_ADDRESS1'=>$street_1,
				'CONSIGNEE_ADDRESS2'=>$street_2,
				'CONSIGNEE_ADDRESS3'=>$street_3,
				'DESTINATION_CITY'=>$delivery_city,
				'PINCODE'=>$delivery_pincode,
				'STATE'=>$delivery_state,
				'MOBILE'=>$delivery_telephone,
				'TELEPHONE'=>$delivery_telephone,
				'ITEM_DESCRIPTION'=>$product_title, 
				'PIECES'=>round($product_qty), 
				'COLLECTABLE_VALUE'=>$collect_amount,
				'DECLARED_VALUE'=>$row_total_incl_tax,
				'ACTUAL_WEIGHT'=>$weight,
				'VOLUMETRIC_WEIGHT'=>0,
				'LENGTH'=>$product_length,
				'BREADTH'=>$product_breath,
				'HEIGHT'=>$product_height,
				'PICKUP_NAME'=>$seller_name,
				'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
				'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
				'PICKUP_PINCODE'=>$zipcode,
				'PICKUP_PHONE'=>$seller_comp_mobile,
				'PICKUP_MOBILE'=>$seller_comp_mobile,
				'RETURN_NAME'=>$seller_name,
				'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
				'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
				'RETURN_PINCODE'=>$zipcode,
				'RETURN_PHONE'=>$seller_comp_mobile,
				'RETURN_MOBILE'=>$seller_comp_mobile,
				'ADDONSERVICE'=>[""],
				'DG_SHIPMENT'=>false,
				'ADDITIONAL_INFORMATION'=>$additional_info
			); 
			
			
		}
		
		
		if( $seller_id!=29)
		{
			echo "<pre>";
				print_r($awbarray); 
			echo "</pre>";
			

			
			$modearray=json_decode($api_helpers->get_razorpay_mode());
			//print_r($modearray); 
			$mode=$modearray->mode;
			$username=$modearray->username;
			$password=$modearray->password;
			$ecom_url=$modearray->url;
	 
			$url=$ecom_url.'apiv2/manifest_awb/';    	 	
			   
						
			
			$params = array(
						'username' => $username,
						  'password' => $password, 
						'json_input' => json_encode($awbarray)
					);   
			 
			 
			/*
			$params = array(
						'username' => "panasonicpvtltd402639_pro",
						  'password' => '6bk2LrZpC7UXhuZe', 
						'json_input' => json_encode($awbarray)
					);    	
			
			if($order_id=="92"){
				echo "<pre>";
				var_dump($awbarray);
				
			}*/	
	 
			$fields_string = http_build_query($params);

			//open connection
			$ch = curl_init();  
			 
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			
			$result=json_decode($output);  
			 echo "<pre>";
				print_r($result);
			echo "</pre>";   
			
			if($result->shipments[0]->success==1)
			{

				echo $sql="select * from ecomexpress_awb where orderid='".$order_id."'";  
				$old_date = $connection->fetchAll($sql);   
                $shipment_id =$old_date[0]['shipment_id'];       		
                       		
                $old_awb = $old_date[0]['awb'];
                $shipment_to = $old_date[0]['shipment_to']."oldawb-".$old_awb;       		

				echo $update_awb="update ecomexpress_awb set manifest='1' ,shipment_id='$shipment_id',shipment_to='$shipment_to',status='Assigned',orderid='$order_id',state='1' where awb='$new_awb'";
				$connection->query($update_awb);

				echo $sql="delete from ecomexpress_awb where awb='".$old_awb."'";  
				$old_date = $connection->query($sql);  			


			}	 	
				
		
					
			curl_close($ch); 
		}
		
		
	}

	public function get_pincode_route($pincode)   
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		 
		
		  
		$select = $connection->select()
				->from('pincode_api') 
				->where('pincode = ?', $pincode); 
							
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			return $result[0]['route'];	
		}	
		else
		{
			return 0;
		}		
		
	}	
	
	
	public function check_product_in_return($order_id,$item_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
		foreach ($order->getAllItems() as $item)
		{
			$item_ids=$item->getId();
			if($item_ids==$item_id) 
			{	
				$item_ordered=$item->getQtyOrdered();
			} 
		} 
		
		
		$select = $connection->select()
				->from('wk_rma_items') 
				->where('item_id = ?', $item_id) 
				->where('order_id = ?', $order_id); 
							
		$result = $connection->fetchAll($select);
		
		$total_item_qty=0;
		if(!empty($result))	
		{
			foreach($result as $row)
			{
				$item_qty=$row['qty'];	
				$total_item_qty=$total_item_qty+$item_qty;
			
			}	
			
			
			//echo $item_ordered.' '.$total_item_qty;
			
			if($total_item_qty==$item_ordered)
			{
				return 0; 	 		
			} 
			else  
			{	
				return 1;	  	 
			}
		}	
		else 
		{
			return 1;  
		}
	}	
	
	
	public function get_product_return_status($order_id,$item_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$rma_helper = $objectManager->create('Webkul\Rmasystem\Helper\Data');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('wk_rma') 
				->where('order_id = ?', $order_id); 
							
		$result = $connection->fetchAll($select);
		$final_status=0; 
		if(!empty($result))
		{
		
			$status=$result[0]['status'];
			$status = $rma_helper->getRmaStatusTitle($status);
			return $status;
		}
	}	
	
	public function get_num_product_sold($product_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('sales_order_item') 
				->where('product_id = ?', $product_id); 
							
		$result = $connection->fetchAll($select);
	
		return count($result);
	
	}	
	
			
	public function check_current_user_like_answer($answer_id,$user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
			
		$select = $connection->select()
				->from('wk_qarespondreview') 
				->where('answer_id = ?', $answer_id)
				->where('review_from = ?', $user_id); 
							
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['like_dislike'];
		}
		else
		{
			return null;
		}
		
	}	
	
	public function ans_like_dislike_count($answer_id,$like_dislike)	
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
			
		$sql="select * from wk_qarespondreview where answer_id='".$answer_id."' and like_dislike='".$like_dislike."'";
		$result = $connection->fetchAll($sql); 
		if(!empty($result))
		{
			return count($result);
		}
		else
		{
			return 0;
		}
	}	
	
	
	public function sync_cart_update_site_app() 
	{
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$user_id=$customerSession->getCustomer()->getId(); 
		
		
		 
		
		
		
		
		
		// get quote items collection
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		 
		// get array of all items what can be display directly
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		 
		// get quote items array
		$items = $cart->getQuote()->getAllItems();
		
		$quote_id=$cart->getQuote()->getId();	
 
		$today = date('Y-m-d H:i:s');
		if(!empty($items))
		{	
			foreach($items as $item) 
			{
				
				$product_id=$item->getProductId();
				
				$product_id=$item->getProductId();
				$item_qty=$item->getQty();
				
				
				$select = $connection->select()
				->from('mobile_user_cart') 
				->where('quote_id = ?', $quote_id)
				->where('product_id = ?', $product_id);
				 
				$result_res = $connection->fetchAll($select);
				if(empty($result_res))
				{
					$sql = "INSERT INTO `mobile_user_cart`(`user_id`, `quote_id`, `product_id`, `qty`, `created_at`) VALUES ('".$user_id."','".$quote_id."','".$product_id."','".$item_qty."','".$today."')";  
					$connection->query($sql);    
				}	 
				else
				{		
					$sql = "update mobile_user_cart set qty='".$item_qty."',created_at='".$today."' where quote_id='".$quote_id."' and product_id='".$product_id."'";  
					$connection->query($sql);	
				}
				
			}
		}


		$sql = "delete from mobile_user_cart where quote_id!='".$quote_id."' and user_id='".$user_id."'";  
		$connection->query($sql);	  
		
			
	}  	
	 
	
	public function clear_cart($user_id)    
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		//$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');   
		
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		 
		
		$token=$api_helpers->get_token(); 
		$quote_id=$api_helpers->get_quote_id($token,$user_id);
		$quote_id=json_decode($quote_id);
		
		
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$allItems=$quote->getAllVisibleItems();
		
		 
		if(!empty($allItems))   
		{	
			foreach ($allItems as $item) {
					
				$itemId = $item->getItemId();
				$product_id = $item->getProductId();
				
				$delete="delete from mobile_user_cart where user_id='".$user_id."' and product_id='".$product_id."'";
				$connection->query($delete);       

				
				$quoteItem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
				$quoteItem->delete();
			}	
			return 1;
		} 
		else
		{
			return 0;
		}
 
	}	
	
	
	public function sync_item_remove($item_idd,$user_id) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		 
		
		$token=$api_helpers->get_token(); 
		$quote_id=$api_helpers->get_quote_id($token,$user_id);
		$quote_id=json_decode($quote_id);
		
		
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$allItems=$quote->getAllVisibleItems();
		
		

		
		if(!empty($allItems))   
		{	  
			foreach ($allItems as $item) {
					
				$itemId = $item->getItemId();
				$productId = $item->getProductId();
				
				if($item_idd==$itemId)
				{
					$sql="delete from mobile_user_cart where quote_id='".$quote_id."' and product_id='".$productId."' and user_id='".$user_id."'";
					$connection->query($sql);		
				}    	 
				
			}	
		}   
		 
		
	}	
	
	
	function check_product_special_price($product_idd)
	{
		$product_id = $product_idd;    
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$price_return=0; 
         
		
		$product_iddd=$product_id;
		
		$price=$api_helpers->get_product_prices($product_iddd);		
		$orgprice = $api_helpers->get_product_prices($product_iddd);
		$specialprice = $api_helpers->get_product_special_prices($product_iddd);
		$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
		$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
		$today = time(); 
		 
		
		if(!is_null($specialfromdate) && !is_null($specialtodate))
		{
			$specialfromdate=strtotime($specialfromdate);
			$specialtodate=strtotime($specialtodate);
			
			if($today >= $specialfromdate &&  $today <= $specialtodate)
			{
				$price_return=1;			
			}	
		}
		
		  
		return $price_return;
	 
	}	
	
	 /*new function*/
 
	function get_product_name($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_varchar') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '73');
				 
		$result = $connection->fetchAll($select);
		
		return $result[0]['value']; 
		
	}	


	function get_product_sku($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity') 
				->where('entity_id = ?', $product_id);
				 
		$result = $connection->fetchAll($select);
		
		return $result[0]['sku']; 
		
	}

	function get_product_prices($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_decimal') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '77');
				 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{	
			return $result[0]['value'];  
		}
		else
		{
			return 0;
		}
	}	






	function get_product_brands($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_varchar') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '213'); 
				  
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			return 0;	 	
		}	 
		else
		{	
			return $result[0]['value']; 
		}
	}	
	
	
	function get_product_model($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_varchar') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '199');    
				  
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			return 'na';		 
		}	 
		else
		{	
			return $result[0]['value']; 
		}
	}	
	
	
	function get_product_model_id_new($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_int') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '167');     
				  
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			return 'na';		 
		}	 
		else 
		{	
			return $result[0]['value']; 
		}
	}	
	
	
	function get_product_model_name_new($model_id) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('eav_attribute_option_value') 
				->where('option_id = ?', $model_id)
				->where('store_id = ?', '0');     
				  
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			return 'na';		 
		}	 
		else 
		{	
			return $result[0]['value']; 
		}
	}	
	
	
	
	


	function get_product_supported_models($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_varchar') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '214');  
				  
		$result = $connection->fetchAll($select);
		if(empty($result))
		{
			return null;		
		}	 
		else
		{	
			return $result[0]['value']; 
		}
	}	


	function get_product_special_from_date($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_datetime') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '79')
				->where('store_id = ?', '0'); 
				 
		$result = $connection->fetchAll($select);
		if(!empty($result))  
		{	
			return $result[0]['value'];  
		}
		
	}	


	function get_product_special_to_date($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_datetime') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '80')
				->where('store_id = ?', '0');
				
		$result = $connection->fetchAll($select);
		if(!empty($result)) 
		{	
			return $result[0]['value'];  
		}
		
	}	

	function get_product_special_prices($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_entity_decimal') 
				->where('entity_id = ?', $product_id)
				->where('attribute_id = ?', '78')
				->where('store_id = ?', '0');
 				 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{	
			return $result[0]['value'];  
		}
		
	}
	
	
	function check_product_brand_id($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('catalog_product_index_eav') 
				->where('entity_id = ?', $product_id) 
				->where('attribute_id = ?', '165');   
				 
		$result = $connection->fetchAll($select);
		if(!empty($result)) 
		{	
			return $result[0]['value'];    
		}
		else
		{
			return 0;
		} 
		
	}
	
	function check_product_attribute_name($attribute_id) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('eav_attribute_option_value') 
				->where('option_id = ?', $attribute_id);   
				 
		$result = $connection->fetchAll($select);
		if(!empty($result)) 
		{	
			return $result[0]['value'];    
		} 
		
		
	}
	
	function count_brand($value) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select="SELECT count(*) 'total_count' from catalog_product_index_eav where `attribute_id`= '165' and value='".$value."' group by value";
				 
		$result = $connection->fetchAll($select);
		//print_r($result);
		
		if(!empty($result)) 
		{	
			return $result[0]['total_count'];     
		}  
		
		
	}
	
	function check_model_attribute_name($attribute_id) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
				->from('eav_attribute_option_value') 
				->where('option_id = ?', $attribute_id);   
				 
		$result = $connection->fetchAll($select);
		if(!empty($result)) 
		{	
			return $result[0]['value'];    
		} 
		
		
	}
	
	
	function count_model($value) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select="SELECT count(*) 'total_count' from catalog_product_index_eav where `attribute_id`= 167 and value='".$value."' group by value";
				 
		$result = $connection->fetchAll($select);
		//print_r($result);
		 
		if(!empty($result)) 
		{	
			return $result[0]['total_count'];     
		}  
		
		
	}
	
	/* product function*/
	
	function get_product_thumb($product_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
		 
		if(!empty($product->getImage())){
			$imageUrl = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('500','500')->getUrl();
		}else{ 
			$site_url = $storeManager->getStore()->getBaseUrl();
			$imageUrl = $site_url.'Customimages/product-thumb.jpg';
		}  	
		
		return $imageUrl; 
		
		
	}	
	
	
	function check_pincode_address_exists($user_id) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select="SELECT * from customer_entity where `entity_id`= '".$user_id."' and default_shipping is not null";
				 
		$result = $connection->fetchAll($select);
		$default_shipping=0;
		$result_data=0;
		   
		if(!empty($result)) 
		{	
			$default_shipping=$result[0]['default_shipping'];     
			$select = $connection->select()
					->from('customer_address_entity') 
					->where('entity_id = ?', $default_shipping);
					 
			$result = $connection->fetchAll($select);
			if(!empty($result))
			{
				$pincode=$result[0]['postcode'];   		
				
				$select = $connection->select()
					->from('pincode_details') 
					->where('pincode = ?', $pincode);
					 
				$result = $connection->fetchAll($select);
				if(!empty($result))
				{
					$result_data=1;		
				}	
			
			} 	   
		
			return $result_data; 
		}   
		
		
		
		
		
	}
	
	
	function shipping_allowed_on_pincode($pincode) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$result_data=0;		
		$select = $connection->select()
			->from('pincode_details') 
			->where('pincode = ?', $pincode);
			 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			$result_data=1;		
		}	
		 
		return $result_data; 
		
	}
	
	function get_brand_by_id($brand_id) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$result_data=0;		
		$select = $connection->select()
			->from('eav_attribute_option_value') 
			->where('option_id = ?', $brand_id);
			 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['value'];
		}	
		 
	
		
	}
	
	function get_model_by_id($model) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$result_data=0;		
		$select = $connection->select()
			->from('eav_attribute_option_value') 
			->where('option_id = ?', $model);
			 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['value'];
		}	
		 
	
		
	}
	
	
	
	function check_order_shipped($order_id) 
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
			->from('ecomexpress_awb') 
			->where('orderid = ?', $order_id);
			 
		$result = $connection->fetchAll($select);
		
		if(empty($result))
		{
			$select = $connection->select()
			->from('sales_shipment') 
			->where('order_id = ?', $order_id);
			$result = $connection->fetchAll($select);
			if(empty($result)){
				return 0;	
			} else 
			{
				return 1;
			}	
		}
		else
		{
			return 1;	
		}		
	}
	
	
	function api_get_order_tracking($order_inc_id)  
	{ 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id);
		$order_id=$order->getId();
		$shipped=$api_helpers->check_order_shipped($order_id); 
		
		if($shipped==0)	
		{
			$status='New'; 
		} 
		else
		{
			
			$return=0;
			$select = $connection->select()
				->from('wk_rma') 
				->where('order_id = ?', $order_id); 
							
			$result = $connection->fetchAll($select);
			if(!empty($result))
			{
				$return=1;
			}	
			
			if($return==1)
			{
				$total_item_ordered=0;
				$total_item_qty=0;
				
				$admin_status=$result[0]['admin_status'];
				$final_status=$result[0]['final_status']; 
				
				
				foreach ($order->getAllItems() as $item)
				{
					$qty=$item->getQtyOrdered();	
					$total_item_ordered=$total_item_ordered+$qty;
				}		
				
				
				foreach ($order->getAllItems() as $item)
				{
					$item_id=$item->getId();
					
					$select = $connection->select()
						->from('wk_rma_items') 
						->where('item_id = ?', $item_id) 
						->where('order_id = ?', $order_id); 
								
					$results = $connection->fetchAll($select);
					if(!empty($results))
					{
						foreach($results as $row)
						{
							$item_qty=$row['qty'];	
							$total_item_qty=$total_item_qty+$item_qty;
						
						}
					}		
				
				}
				
				if($total_item_ordered==$total_item_qty)
				{
					$status='Full Return Initiated';		
					if($admin_status!=6 && $final_status==3)
					{
						$status='Full Return';
					}	
				}	
				else
				{
					$status='Partial Return Initiated';
 					if($admin_status!=6 && $final_status==3)
					{
						$status='Partial Return';
					}
				}	
				
				if($admin_status==5)
				{
					$status='Return Declined'; 
				}	
				
				if($admin_status==6 && $final_status==3)
				{
					$status='Return Accepted';		
				}  	
			}	
			else
			{
				
				// $array=$sales_helpers->get_forward_order_tracking($order_id);
				$array=$sales_helpers->getOrderTrackingOM($order_id,'Forward');
				$status=$array['status']; 
				if($status=='0')
				{
					$status="Processing";	
				} 	 
			} 		
		}	
		return $status;
	} 
	
	
	public function create_forward_manifest() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		$sql="select * from ecomexpress_awb where `orderid`!='' and `status`!='' and manifest is null";		  
			  
		$results = $connection->fetchAll($sql); 
		if(!empty($results)) 
		{	
			$today=date('Y-m-d');    
			foreach($results as $row)
			{ 
				$order_id=$row['orderid'];
				$awb=$row['awb'];
				$awb_type=$row['awb_type']; 
				
				$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
				$order_inc_id=$order->getIncrementId();
				$api_helpers->awb_to_manifest($awb,$order_id,$awb_type);
			}	
		} 				
		else 
		{
			echo "No Order for manifest today";
		}		
		
		
	
	}
	public function forward_tracking() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		$sql="select * from ecomexpress_awb where `orderid`!='' and `status`!='' and manifest='1' and track_status is null";		  
		//$sql="select * from ecomexpress_awb where orderid='25'";		  
		$results = $connection->fetchAll($sql);
				

		$modearray=json_decode($api_helpers->get_razorpay_mode());
		//print_r($modearray);  
		$mode=$modearray->mode;
		$username=$modearray->username;
		$password=$modearray->password;
		$ecom_url=$modearray->url;	



		if(!empty($results))
		{	
			
			foreach($results as $row)
			{  

				
				$order_id=$row['orderid'];
				$query="select entity_id from sales_creditmemo where order_id = $order_id";
				$is_credit_memo_generated = $connection->fetchOne($query);
				if(!empty($is_credit_memo_generated))
				{
					continue;
				}
				
				
				$awb_id=$row['awb_id']; 
				$awb=$row['awb'];
				$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
				
				$order_inc_id=$order->getIncrementId();
				$check=$api_helpers->check_order_tracking_exist($order_inc_id);
				
				$result_status=0; 
				
				$approved=1;	 
				$processed=1;	
				$status="Processing";
				if($mode=="dev")  
				{
					$url='https://clbeta.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
				}	
				else 
				{
					$url='https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
				}	 
												
				$approved=0;	
				$processed=0;	
				$shipped=0;	 
				$delivered=0;	
				$status=0;	
				$pickup_date=0;	
				$delivered_dates=0;	
				
				$xmlobj = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
				
				
				//$xmlstr = $this->get_xml_from_url($url); 
				//$xmlobj = new SimpleXMLElement($xmlstr);
				$xmlobj = (array)$xmlobj;//optional	
				$json=(array)$xmlobj['object']->field;
				
				$pickup=$json[9];  
				
				if(!is_object($pickup))
				{
					$shipped=1;	
					$status="Shipped";
					$pickup_date=$pickup;    
				}		
				
				$delivered_date=$json[21];
				if(!is_object($delivered_date))
				{
					$delivered=1;	
					$status="Delivered";
					$delivered_dates=$delivered_date;
				}
				
				if($delivered_dates!='0') 
				{
					$sql="update ecomexpress_awb set track_status='dl' where awb_id='".$awb_id."'";  
					$connection->query($sql);	
				}	
				  
				$array=array(
					'pickup_date'=>$pickup_date,	
					'delivered_date'=>$delivered_dates,	
					'status'=>$status,	
					'order_id'=>$order_id,	
				);
				
				/*  echo "<pre>";
					print_r($array);
				echo "</pre>";  */
				
			}
		}
		
		
		
	}	
	
	
	
	
	
	public function get_reverse_awb()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		
		$modearray=json_decode($api_helpers->get_razorpay_mode());
		//print_r($modearray);  
		$mode=$modearray->mode;
		$username=$modearray->username;
		$password=$modearray->password;
		$ecom_url=$modearray->url;	
		
		
		$url=$ecom_url.'apiv2/fetch_awb/';  

		$params = array(
			'username' => $username,
			'password' => $password,
			'count' => '5',
			'type' => 'rev'   
		);

		$fields_string = http_build_query($params);
		
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$output = curl_exec($ch);
		curl_close($ch);
		 
		$result=json_decode($output);
		
		 echo "<pre>"; 
		print_r($result);
		echo "</pre>"; 
		
		$awbarray=$result->awb;
		$today=date('Y-m-d H:i:s');
		if(!empty($awbarray))
		{	
			foreach($awbarray as $row)
			{
				$awb=$row;	
				
				$select = $connection->select()
						  ->from('rev_awb') 
						  ->where('awb = ?', $awb);
						 
				$results = $connection->fetchAll($select);
				
				//print_r($results);
				
				if(empty($results))
				{
					$sql="INSERT INTO `rev_awb`(`awb`, `order_id`, `product_id`, `status`, `cr_date`, `update_date`) VALUES ('".$awb."','0','0','0','".$today."','".$today."')";
					//echo "<br>"; 
					$connection->query($sql);
				}  
					
				
			}	
		}
		
	}
	
	 
	public function get_forward_awb() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		
		$modearray=json_decode($api_helpers->get_razorpay_mode());
		//print_r($modearray);  
		$mode=$modearray->mode;
		$username=$modearray->username;
		$password=$modearray->password;
		$ecom_url=$modearray->url;	
		
		
		$url=$ecom_url.'apiv2/fetch_awb/';  

		$params = array(
			'username' => $username, 
			'password' => $password,
			'count' => '5',
			'type' => 'PPD'    
		);

		$fields_string = http_build_query($params);
		
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$output = curl_exec($ch);
		curl_close($ch);
		 
		$result=json_decode($output);
		
		/* echo "<pre>"; 
			print_r($result);
		echo "</pre>"; 
		 */
		
		
	}
	

	public function track_reverse_orders()
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		
		$modearray=json_decode($api_helpers->get_razorpay_mode());
		//print_r($modearray);  
		$mode=$modearray->mode;
		$username=$modearray->username;
		$password=$modearray->password;
		$ecom_url=$modearray->url;	
		
		
		$select="select * from wk_rma where admin_status='6' and final_status='3'";
		 
		$result = $connection->fetchAll($select); 
		
		
		if(!empty($result))
		{

			foreach($result as $row)
			{
				$rma_id=$row['rma_id'];	
				$increment_id=$row['increment_id'];	
				
				$admin_status=$row['admin_status'];	
				$final_status=$row['final_status'];	
				
				$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($increment_id); 
				$order_id=$order->getId();
						
						 
				
				
				
				
				$select="select * from wk_rma_items where rma_id='".$rma_id."' and manifest is null"; 
				$results = $connection->fetchAll($select); 
				if(!empty($results))	
				{
					foreach($results as $rows)
					{
						$item_idd=$rows['item_id'];
						$item_qty=$rows['qty']; 
						
						foreach ($order->getAllItems() as $item)
						{
							$item_id=$item->getId();
							if($item_idd==$item_id)
							{
								$product_id=$item->getProductId();
							}		
						} 
						
						
						
						//echo $rma_id.' '.$item_idd.' '.$admin_status.' '.$final_status.' '.$item_qty.' '.$product_id;
						//echo "<br>";
						
						$custom_helpers->rev_ecom_api($increment_id,$product_id,$item_qty,$rma_id,$item_idd); 	  
						
					}		
				}	
			
			
			
			}
		}


		$sql="select * from rev_awb where `order_id`!='0' and `awb_status` is null";		  
		//$sql="select * from rev_awb where `order_id`!='0' and `awb_status`='pick'";		  
		$results = $connection->fetchAll($sql);  
		 
		//print_r($results); 
		
		if(!empty($results))
		{
			$modearray=json_decode($api_helpers->get_razorpay_mode());
			//print_r($modearray);  
			$mode=$modearray->mode;
			$username=$modearray->username;
			$password=$modearray->password;
			$ecom_url=$modearray->url;	
			
			foreach($results as $row)
			{  
				$order_idd=$row['order_id'];
				$awb=$row['awb'];
				$product_id=$row['product_id'];  
			
				$order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($order_idd); 
				$order_inc_id=$order_idd;
				  
				$order_id=$order->getId();
			
				if($mode=="dev")  
				{
					$url='https://clbeta.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
				}	
				else 
				{
					$url='https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
				}	
				
				$homepage = @file_get_contents($url); 
				
				//$homepage = file_get_contents('https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username=panasonicpvtltd402639_pro&password=6bk2LrZpC7UXhuZe'); 
				
				$result_status=0; 
				
				if($homepage!='')    
				{
					$delivered=0;   
					$tracking_array=$api_helpers->get_rev_order_tracking($order_id,$product_id);
					
					$xml=simplexml_load_string($homepage) or die("Error: Cannot create object");
					$json  = json_encode($xml);
					$configData = json_decode($json, true);
					if(!empty($configData['object']['field']))
					{
						$resultt=$configData['object']['field'];
						$result_status=json_encode($resultt);  
					}
					
					if($tracking_array!='0')
					{
						$tracking_arrayy=json_decode($tracking_array);    
						
						$trackArray=$tracking_arrayy[36]->object;
						if(!empty($trackArray)) 
						{
							$trackArray=array($trackArray);
							$reversed = array_reverse($trackArray);
							
							$array=array(419);
							foreach($reversed as $track_array)
							{
								$trackfield=$track_array->field;	
								if(!is_object($trackfield))
								{
									$status=$trackfield[3]; 
									if($status=='002')				
									{
										$sql="update rev_awb set awb_status='pick' where awb='".$awb."'";  	  	 	
										$connection->query($sql);	
									}	
								} 
							}	 
						
						}
					}
					
					$check=$api_helpers->check_rev_order_tracking_exist($order_inc_id,$product_id);
					$today=date('Y-m-d H:i:s'); 
					if($check==0)  
					{
						$sql = "Insert Into rev_order_tracking_status (order_inc_id,awb,product_id,result_status,cr_date) values ('".$order_inc_id."','".$awb."','".$product_id."','".$result_status."','".$today."')"; 
					}	
					else   
					{
						$sql="update rev_order_tracking_status set result_status='".$result_status."',cr_date='".$today."' where awb='".$awb."' and order_inc_id='".$order_inc_id."'";  
					} 
					
					$connection->query($sql);  
						
				}
			
			}
			
			
			
			
		}	
		
		
		
		
	}	

	
	public function get_pincode_details($zipcode)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$select = $connection->select()
			  ->from('pincode_api') 
			  ->where('pincode= ?',$zipcode);
		$result = $connection->fetchAll($select);
		
		if(!empty($result))
		{	
			$array=array(
				'city'=>$result[0]['city'],
				'state'=>$result[0]['state'],
			);
			
			return json_encode($array);
		}
		else
		{
			return '0';
		}		
		
	}	
	
	
	public function assign_order_to_seller_new($order_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
		$order_inc_id=$order->getIncrementId();
		$created_at=$order->getCreatedAt();
		$buyer_id=$order->getCustomerId();
		
		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
		
		$invoice_details = $order->getInvoiceCollection();
		foreach ($invoice_details as $_invoice) {
			$invoiceArray=$_invoice->getData();
			$invoice_entity_id=$invoiceArray['entity_id'];
			
		} 
		
		
		$orderArray=json_decode($tax_helpers->order_amount_details($order_inc_id));
		$orderItemArray=json_decode($tax_helpers->order_item_details($order_inc_id));
		
		/* echo "<pre>";
			print_r($orderItemArray);
		echo "</pre>"; */
		
		
		$seller_id=$orderArray->seller_id;
		$product_ids='';
		foreach ($order->getAllItems() as $item)
		{
			$item_id=$item->getId();
			$product_ids .=$item->getProductId().',';	
			$seller_id=$api_helpers->get_product_seller_id_mobile($item->getProductId());	
			$sellerArray[]=$seller_id;
		} 
		
		$array = array_unique($sellerArray);
		
		$check_split=count($array);
		
		foreach($array as $row)
		{
			$seller_id=$row;	
		} 	 
		
		
		$product_idss=substr($product_ids,0,-1);
		
		$order_id=$order_id;
		$product_iddd=$product_idss;
		$seller_id=$seller_id;
		$shipment_id=0;
		$invoice_id=$invoice_entity_id; 
		$creditmemo_id=0; 
		$is_canceled=0;   
		$shipping_charges=0.0000;   
		$created_at=$created_at;    
		$updated_at=$created_at;    
		$tax_to_seller=0;  
		$total_tax=0.0000;  
		$coupon_amount=0.0000;   
		$refunded_coupon_amount=0.0000; 
		$refunded_shipping_charges=0.0000;    
		$seller_pending_notification=1; 
		$order_status='pending'; 
		
		
		echo $sql="INSERT INTO `marketplace_orders` (`order_id`, `product_ids`, `seller_id`, `shipment_id`, `invoice_id`, `creditmemo_id`, `is_canceled`, `shipping_charges`, `carrier_name`, `tracking_number`, `created_at`, `updated_at`, `tax_to_seller`, `total_tax`, `coupon_amount`, `refunded_coupon_amount`, `refunded_shipping_charges`, `seller_pending_notification`, `order_status`) VALUES
('".$order_id."', '".$product_iddd."', '".$seller_id."', '0', '".$invoice_id."', '0', 0, 0.0000, NULL, NULL, '".$created_at."', '".$updated_at."', 0, 0.0000, 0.0000, 0.0000, 0.0000, 1, '".$order_status."')";
		
		
		
	foreach($orderItemArray as $value)
	{
		
		$mageproduct_id=$value->product_id;
		
		
		$sql = "Select * FROM sales_order_item where order_id='".$order_id."' and product_id='".$mageproduct_id."'";
		$results = $connection->fetchAll($sql); 
		$item_id=$results[0]['quote_item_id'];
		
		$order_item_id=$item_id;   
		$magerealorder_id=$order_inc_id;
		$magequantity=(int)$value->product_qty;
		$seller_id=$seller_id;
		$trans_id=0;
		$cpprostatus=0;
		$paid_status=0;
		$magebuyer_id=$buyer_id;
		$magepro_name=$value->product_name;
		$magepro_price=$value->product_price;
		$total_amount=$value->row_total;
		$total_tax='0.0000';
		
		$total_commission='0.0000'; 
		$actual_seller_amount=$value->pay_to_seller;
		$created_at=$created_at;
		$updated_at=$created_at;
		$is_shipping=1;
		$is_coupon=0;
		$is_paid=0;
		$commission_rate='0.0000';
		$currency_rate=$value->product_qty;
		$applied_coupon_amount='0.0000';
		$is_withdrawal_requested=0;        
		
		echo "<br>";
		if( (int)$magebuyer_id > 0 and (int)$seller_id > 0 ){
		 $sql="INSERT INTO `marketplace_saleslist` (`mageproduct_id`, `order_id`, `order_item_id`, `magerealorder_id`, `magequantity`, `seller_id`, `trans_id`, `cpprostatus`, `paid_status`, `magebuyer_id`, `magepro_name`, `magepro_price`, `total_amount`, `total_tax`, `total_commission`, `actual_seller_amount`, `created_at`, `updated_at`, `is_shipping`, `is_coupon`, `is_paid`, `commission_rate`, `currency_rate`, `applied_coupon_amount`, `is_withdrawal_requested`) VALUES
('".$mageproduct_id."', '".$order_id."', '".$order_item_id."','".$magerealorder_id."', '".$magequantity."', '".$seller_id."', 0, 0, 0, '".$magebuyer_id."', '".$magepro_name."', '".$magepro_price."', '".$total_amount."', 0.0000, 0.0000, '".$actual_seller_amount."', '".$created_at."', '".$created_at."', 1, 0, 0, 0.0000, '".$currency_rate."', 0.0000, 0);"; 
		}
		
		
		
		 
	}	
		
		
		
		
		
		
		
		
		
		
		
		
	}	
	
	
	
	public function assign_order_to_seller($order_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
		$order_inc_id=$order->getIncrementId();
		$created_at=$order->getCreatedAt();
		$buyer_id=$order->getCustomerId();
		
		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
		
		$invoice_details = $order->getInvoiceCollection();
		foreach ($invoice_details as $_invoice) {
			$invoiceArray=$_invoice->getData();
			$invoice_entity_id=$invoiceArray['entity_id'];
			
		} 
		
		
		$orderArray=json_decode($tax_helpers->order_amount_details($order_inc_id));
		
		 
		
		
		
		$orderItemArray=json_decode($tax_helpers->order_item_details($order_inc_id));
		/* echo "<pre>";
			print_r($orderItemArray);
		echo "</pre>";
		
		die; */    
		
		$seller_id=$orderArray->seller_id;
		
		
		foreach($orderItemArray as $value)
		{
			
			$mageproduct_id=$value->product_id;
			$order_id=$order_id;
			
			$sql = "Select * FROM sales_order_item where order_id='".$order_id."' and product_id='".$mageproduct_id."'";
			$results = $connection->fetchAll($sql); 
			$item_id=$results[0]['quote_item_id'];
			
			$order_item_id=$item_id;   
			$magerealorder_id=$order_inc_id;
			$magequantity=(int)$value->product_qty;
			$seller_id=$seller_id;
			$trans_id=0;
			$cpprostatus=0;
			$paid_status=0;
			$magebuyer_id=$buyer_id;
			$magepro_name=$value->product_name;
			$magepro_price=$value->product_price;
			$total_amount=$value->row_total;
			$total_tax='0.0000';
			
			$total_commission='0.0000'; 
			$actual_seller_amount=$value->pay_to_seller;
			$created_at=$created_at;
			$updated_at=$created_at;
			$is_shipping=1;
			$is_coupon=0;
			$is_paid=0;
			$commission_rate='0.0000';
			$currency_rate=$value->product_qty;
			$applied_coupon_amount='0.0000';
			$is_withdrawal_requested=0;        
			
			if( (int)$seller_id > 0 and (int)$magebuyer_id > 0 ){
				$sql="INSERT INTO `marketplace_saleslist`(`mageproduct_id`, `order_id`, `order_item_id`,`magerealorder_id`, `magequantity`, `seller_id`, `trans_id`, `cpprostatus`, `paid_status`, `magebuyer_id`, `magepro_name`, `magepro_price`, `total_amount`, `total_tax`, `total_commission`, `actual_seller_amount`, `created_at`, `updated_at`, `is_shipping`, `is_coupon`, `is_paid`, `commission_rate`, `currency_rate`, `applied_coupon_amount`, `is_withdrawal_requested`) VALUES ('".$mageproduct_id."','".$order_id."','".$order_item_id."','".$magerealorder_id."','".$magequantity."','".$seller_id."','".$trans_id."','".$cpprostatus."','".$paid_status."','".$magebuyer_id."','".$magepro_name."','".$magepro_price."','".$total_amount."','".$total_tax."','".$total_commission."','".$actual_seller_amount."','".$created_at."','".$updated_at."','".$is_shipping."','".$is_coupon."','".$is_paid."','".$commission_rate."','".$currency_rate."','".$applied_coupon_amount."','".$is_withdrawal_requested."')"; 
					
				$connection->query($sql); 
			}     
		}	
		
		
		
		
		$product_ids='';
		foreach ($order->getAllItems() as $item)
		{
			$item_id=$item->getId();
			$product_ids .=$item->getProductId().',';	
			$seller_id=$api_helpers->get_product_seller_id_mobile($item->getProductId());	
			$sellerArray[]=$seller_id;
		} 
		
		
		
		$array = array_unique($sellerArray);
		
		$check_split=count($array);
		
		foreach($array as $row)
		{
			$seller_id=$row;	
		} 	 
		
		
		
		
		$product_idss=substr($product_ids,0,-1);
		
		$mainarray=array(
			'order_id'=>$order_id,
			'product_ids'=>$product_idss,
			'seller_id'=>$seller_id, 
			'shipment_id'=>0, 
			'invoice_id'=>$invoice_entity_id,    
			'creditmemo_id'=>0,  
			'is_canceled'=>0,   
			'shipping_charges'=>'0.0000',    
			'created_at'=>$created_at,    
			'updated_at'=>$created_at,    
			'tax_to_seller'=>0,    
			'total_tax'=>'0.0000',    
			'coupon_amount'=>'0.0000',    
			'refunded_coupon_amount'=>'0.0000',    
			'refunded_shipping_charges'=>'0.0000',    
			'seller_pending_notification'=>'1',    
			'order_status'=>'pending',     
		); 
		  
		
		 
		$sql="INSERT INTO `marketplace_orders`(`order_id`, `product_ids`, `seller_id`, `shipment_id`, `invoice_id`, `creditmemo_id`, `is_canceled`, `shipping_charges`, `carrier_name`, `tracking_number`, `created_at`, `updated_at`, `tax_to_seller`, `total_tax`, `coupon_amount`, `refunded_coupon_amount`, `refunded_shipping_charges`, `seller_pending_notification`, `order_status`) VALUES ('".$order_id."','0','0','0','0','0','0','0','0','0','".$created_at."','".$created_at."','0','0','0','0','0','1','0')";
		$connection->query($sql);    
		
		
		
		foreach($mainarray as $key=>$value)
		{
			
			$sql="update marketplace_orders set ".$key."='".$value."' where order_id='".$order_id."'";
			$connection->query($sql);  
		} 	 
		
		
		$sql="update sales_order set order_approval_status='1' where entity_id='".$order_id."'";
		$connection->query($sql);  
		$sqls="update sales_order_grid set order_approval_status='1' where entity_id='".$order_id."'"; 
		$sqlq="update marketplace_saleslist set cpprostatus='1' where order_id='".$order_id."'";
		$connection->query($sqls);     
		$connection->query($sqlq);      
		  	
	 
	}
	
	
	
	function get_brand_id_by_name($brand) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$sql="SELECT * FROM `eav_attribute_option_value` WHERE `value`='".$brand."' order by option_id asc limit 0,1 ";
		
		$select = $sql;
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['option_id'];	
		}
		else
		{
			return 0;
		}		
		
		
	}	
	
	function get_model_id_by_name($model) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$sql="SELECT * FROM `eav_attribute_option_value` WHERE `value`='".$model."' order by option_id asc limit 0,1 ";
		 
		$select = $sql;
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['option_id'];	
		}
		else
		{
			return 0;
		}		
		
		
	}	
	
	
	function get_model_name_by_id($model_id) 
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$sql="SELECT value FROM `eav_attribute_option_value` where option_id='".$model_id."'";
		 
		$select = $sql;
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['value'];  	
		}
		else
		{
			return 0;
		}		
		
		
	}	
	
	
	
	
	public function check_api() 
	{
		$header = apache_request_headers(); 
		$header['User-Agent'];
		
		$user_agent=$header['User-Agent'];
		
		$show=0;
		
		if (strpos($user_agent, 'Mozilla') !== false) {
			$show=1;
		}
		else if (strpos($user_agent, 'PostmanRuntime') !== false) {
			$show=1;
		}
		//return $show=1;	    
		return $show;  
			 
	}	 
	
	public function check_user_seller($cust_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		$sql = "Select * FROM marketplace_userdata where seller_id='".$cust_id."'  limit 0,1";
		$result = $connection->fetchAll($sql);
		
		if(!empty($result))
		{
			return 1;	
		}
		else
		{
			return 0;   
		}	
		
	}	
	
	
	public function count_answer_like_dislike($ans_id,$type)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		
		if($type==1)
		{	
			$sql = "SELECT count(*) as 'total' FROM `wk_qarespondreview` where answer_id='".$ans_id."' and like_dislike='1' group by answer_id";
			$result = $connection->fetchAll($sql);
			if(!empty($result))
			{
				return $result[0]['total'];
			}
			else
			{
				return 0;
			}		
		}
		
		if($type==0)
		{	
			$sql = "SELECT count(*) as 'total' FROM `wk_qarespondreview` where answer_id='".$ans_id."' and like_dislike='0' group by answer_id";
			$result = $connection->fetchAll($sql);
			if(!empty($result))
			{
				return $result[0]['total'];
			}
			else
			{
				return 0;
			}		
		}
		
		
		
	}	
	
	
	
	
	public function get_star_count($product_id,$star)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();	
		
		
		$count=0;
		$sql="select * from rating_option_vote  where entity_pk_value='".$product_id."' and value='".$star."'";
		$result = $connection->fetchAll($sql);	
		if(!empty($result))
		{
			
			foreach($result as $row)
			{
			
				$review_id=$row['review_id'];	
				$sql_1="select * from review  where review_id='".$review_id."' and status_id='1'"; 
				$result_1 = $connection->fetchAll($sql_1);	 
				
				//print_r($result_1);
				
				if(!empty($result_1))
				{
					$count=$count+1; 
				}
			
			}
			
		
		}		
		
		return $count;
		
		/*
		$sql = "SELECT count(*) as 'rating_count' FROM `rating_option_vote` where entity_pk_value='".$product_id."' and value='".$star."'";
		$result = $connection->fetchAll($sql);	
		if(!empty($result))
		{
			return $result[0]['rating_count'];	
		}
		else
		{
			return 0;
		}		
		*/
	}	
	
	public function get_current_cart($user_id,$token,$quote_id) 
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	 
		$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 


		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
		$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
		
		$address=$api_helpers->check_pincode_address_exists($user_id);

		$cart_total=0;
		
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$allItems=$quote->getAllVisibleItems();
		
		$mainArrayy=array();
		
		foreach ($allItems as $item) 
		{
		//item id of particular item
		$itemId = $item->getItemId();
		$productId = $item->getProductId();
		$qty=$item->getQty();
		
		
			$mainArrayy[]=array(
				'item_id'=>$itemId,
				'product_id'=>$productId,
				'qty'=>$qty
			);		
			
		}
		
		$shippingAmount=0; 
		$total_shippingAmount=0;
		$special_price=0;
		$sale=0;
		$discountAmount=0;
		$total_gst=0;
		$total_price=0;
		if(!empty($mainArrayy))
		{
			foreach($mainArrayy as $row) 
			{
				$id = $row['product_id'];
				$qty= $row['qty'];
				$item_id= $row['item_id']; 
				
				$priduct_check=$api_helpers->check_product_exist($id);
				if($priduct_check=='')
				{
					$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product of '.$id.' not found','quote_id'=>$quote_id);	
					echo json_encode($result_array);	  
					die;  
				}	 
				
				$product = $productRepository->getById($id); 
				$check_price=$api_helpers->get_product_price($id);
				$orignal_price=$product->getPrice();	 
				
				/*
				if($check_price!=0)
				{
					$special_price=$check_price;	
					$sale = round((($orignal_price - $special_price)/$orignal_price) * 100);
				}
				$orignal_price=number_format($orignal_price, 2, '.', '');
				$special_price=number_format($special_price, 2, '.', '');
				*/
				
				$orignal_price=$product->getPrice();	 
				$special_price=$product->getSpecialPrice();	 
				
				$product_iddd=$product->getId();
				$is_special=0; 
				$price=$api_helpers->get_product_prices($product_iddd);		
				$orgprice = $api_helpers->get_product_prices($product_iddd);
				$specialprice = $api_helpers->get_product_special_prices($product_iddd);
				$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
				$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
				$today = time(); 
				
				if (!$specialprice)
				{	
					$specialprice = $orgprice;
				}
				else
				{
					if(!is_null($specialfromdate) && !is_null($specialtodate))
					{
						$specialfromdate=strtotime($specialfromdate);
						$specialtodate=strtotime($specialtodate);
						
						if($today >= $specialfromdate &&  $today <= $specialtodate)
						{
							$is_special=1;			
						}	
					}
				}
				
				$sale=0;
				
				if($is_special==1)
				{
					
					$orignal_price=number_format($product->getPrice(), 2, '.', '');
					$special_price=number_format($product->getSpecialPrice(), 2, '.', '');
					$sale = round((($orignal_price - $special_price)/$orignal_price) * 100);
					$final_price=$special_price;
				}   	
				else
				{
					$special_price=0;
					$orignal_price= number_format($product->getPrice(), 2, '.', '');
					$special_price = number_format($product->getSpecialPrice(), 2, '.', '');
					$final_price=$orignal_price;
					
					
				}	

				 
				
				$mainArray[]=array(
					'id'=>$id,		
					'qty'=>$qty,		 
					'item_id'=>$item_id,		 
					'product_name'=>$product->getName(),		
					'product_image'=>$_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl(),	
					'orignal_price'=>$orignal_price,	
					'special_price'=>$special_price,	
					'final_price'=>$final_price,	
					'sale'=>$sale,	
					'reward_point'=>$earnOutput->getProductPoints($product)
				);  
				
				//Order Summary
				
				$check_price=$api_helpers->get_product_price($id);
				
				if($check_price==0)
				{
					$price=$product->getPrice() * $qty;	
				}
				else
				{
					$price=$check_price * $qty;			
				}	 	
				
				

				$shippingGstRate = $product->getGstRate();

				$gstPercent = 100 + $shippingGstRate;
				
				
				
				
				
				$rowTotal = $price;	 

				$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
						
				$gstAmount = $productPrice * $shippingGstRate;
				$total_gst = $total_gst + $gstAmount;
				$total_price=$total_price + $rowTotal;
				
				$pincode=$custom_helpers->get_customer_shipping_address($user_id,'postcode');
				if($pincode!=0)
				{	
			
					$shippingAmount=$data_helpers->ecom_price_by_product($pincode,$id,$qty);

					$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
					$couponCode = $cart->getQuote()->getCouponCode();
					$couponCode = $cart->getQuote()->getCouponCode();
			        $ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
			        $rule = $saleRule->load($ruleId);
			        $freeShippingCoupon = $rule->getSimpleFreeShipping();
					if($freeShippingCoupon){
		               $shippingAmount = 0 ;
		            }
					$total_shippingAmount=$total_shippingAmount+$shippingAmount;
					 
				}  
				
				
				
			}
			

		  

			


		 $total_shippingAmount;
			  


			
		$user_array=$mainArray; 
		$total_base_amount=$total_price - $total_gst;

		// echo $total_gst.' '.$total_price.' '.$total_base_amount;
		// die();



		 

		$total_gst=number_format($total_gst, 2, '.', '');
		$total_base_amount=number_format($total_base_amount, 2, '.', '');
		  
		$subtotal=$total_gst + $total_base_amount;
		$total_shippingAmount=number_format($total_shippingAmount, 2, '.', '');

		$order_total=$subtotal + $total_shippingAmount;

		$total_gst.' '.$total_base_amount.' '.$subtotal.' '.$order_total;	

		$subtotal=number_format($subtotal, 2, '.', '');
		$order_total=number_format($order_total, 2, '.', '');
		 
		$summary_array=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total);


		$cart_total=0;
				
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$quoteItems=$quote->getAllVisibleItems();
		$items = $quote->getAllItems();
		if(!empty($items))
		{	
			foreach($items as $item)   
			{
				$product_id=$item->getProductId();
				$qty=$item->getQty(); 
				$cart_total=$cart_total+$qty;
			}		
		} 

		$user_address=0;
		if($address!=0) 
		{
			$user_address=1;
		}	

		$result_array=array('cart' => $user_array,'summary'=> $summary_array,'cart_total'=>$cart_total,'quote_id'=>$quote_id,'address_exist'=>$user_address,'error' => 'Cart Page Found'); 	

		

		}
		else
		{
			$user_array=array(); 
			$result_array=array('cart' => $user_array,'quote_id'=>$quote_id,'error' => 'Cart is empty'); 	
			
		}	
	
		return $result_array; 
	
	}	
	
	
	public function get_user_wishlist($user_id)
	{
		$wishlist_count=0;
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
		$StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
		$stockRegistry = $objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface'); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
		
		$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
		//$wishlist_collection = $wishlist->loadByCustomerId($user_id, true)->getItemCollection();	
		//$wishlistArray=$wishlist_collection->getData();
		
		$wishlistAdd = $wishList->create()->loadByCustomerId($user_id, true);
		$wishlistArray=$wishlistAdd->getItemCollection();
		
		
		
		
		$productArray=array();
		$data=array();
		
		if(!empty($wishlistArray))
		{
			foreach($wishlistArray as $row)	
			{
				$productArray[]=array(
					'id'=>$row['product_id'],
					'qty'=>$row['qty']
				);	
				
				$id=$row['product_id'];
				$product = $productRepository->getById($id);
				$productdata['id']= $product->getId();
				$product_id=$product->getId(); 
				$productdata['name']= strip_tags($product->getName());
				$productdata['sku']= strip_tags($product->getSku());
				$productdata['price']= strip_tags($product->getPrice());
				$productdata['specialprice'] = $product->getSpecialPrice();
				$productdata['specialfromdate'] = $product->getSpecialFromDate();
				$productdata['specialtodate'] = $product->getSpecialToDate();
				
				$price = $product->getPrice();
				$productdata['price'] = number_format($price, 2);
				$specialprice = $product->getSpecialPrice();
				$specialPriceFromDate = $product->getSpecialFromDate();
				$specialPriceToDate = $product->getSpecialToDate(); 
				$today = time();
				
				if($price){
					$sale = round((($price-$specialprice)/$price)*100);
				}
				if ($specialprice) {
					if(!empty($specialPriceFromDate)){
						if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
							
							
							$productdata['specialprice'] = number_format($specialprice, 2);
							$productdata['specialfromdate'] = $specialPriceFromDate;
							$productdata['specialtodate'] = $specialPriceToDate;
							$productdata['sale_percentage'] = $sale.'%'.' Off';
							
							
						}
					}
				}
				
				$productdata['brand']= null;
				if($product->getBrands()!='')
				{
					
					$brand_id=$product->getBrands();
					$sql_brand="select value from eav_attribute_option_value where option_id='".$brand_id."'"; 
					$result_brand = $connection->fetchAll($sql_brand);	
					$brand_name=$result_brand[0]['value']; 	 	
					
					$productdata['brand']=$brand_name;	
				}	
				
				
				
				/* if($product->getAttributeText('supported_models')){
					$productdata['supported_models']= $product->getAttributeText('supported_models');
				}else{
					$productdata['supported_models']= null;	
				} */
				 
				// From Rma extension
				$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
				if($product->getAttributeText('return_period')=="Flat return period"){
					$productdata['return_days']= $return_date.' days';
				}else{
					$productdata['return_days']= "No Return";
				}
				
				//product image resize and setting the default image.
				if( !empty($product->getImage()) ){
					$productdata['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product'.$product->getImage();
					$productdata['image'] = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl(); 
				}else{ 
					$site_url = $storeManager->getStore()->getBaseUrl();
					$imageUrl = $site_url.'Customimages/product-thumb.jpg';
					$productdata['image'] = $imageUrl;
				}
				
				//$productdata['earn_reward'] = $earnOutput->getProductPoints($product);
				
				$productdata['stock_Qty']= $StockState->getStockQty($product_id);
			
			
				$productStock = $stockRegistry->getStockItem($product_id);
				$productQty = $productStock->getQty(); 
				
				$in_stock=1;
				if($StockState->getStockQty($product_id)==0)
				{
					$in_stock=0; 
				}

				$productdata['in_stock']= $in_stock; 
				
				
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$productdata['rating_count'] = $ratings;
				}else{
					$productdata['rating_count'] = 0;
				} 
				
				$data[] = $productdata;
				// print_r($data);
				// die();
			
				
				
			}	
		
		$wishlist_count=count($data);
		}	
		
		
			 
			
		if(!empty($data)){
			$result_array=array('wishlist' =>$data,'wishlist_count'=>$wishlist_count);
			
		}else{
			//$result_array=array('success' => 0, 'result' =>$data, 'error' => 'There is no product in wishlist.');
			$result_array=array('wishlist' =>$data,'wishlist_count'=>$wishlist_count);
			
		} 
		
		return $result_array;	
	}	
	
	public function remove_special($string)  
	{
		$string = str_replace(array('[\', \']'), '', $string);
		$string = preg_replace('/\[.*\]/U', '', $string);
		$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '', $string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
		$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '', $string);
		return $string;
	}	
	
	public function store_user_token($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		$select = $connection->select()
                  ->from('custom_user_token')
				  ->where('user_id = ?', $user_id);
        $result = $connection->fetchAll($select);
		
		$token=$api_helpers->get_token(); 
		$quote_id=json_decode($api_helpers->get_quote_id($token,$user_id));
		
		
		if(empty($result))  
		{
			$sql = "insert into custom_user_token (user_id,user_token,user_quote_id) values ('".$user_id."','".$token."','".$quote_id."')"; 
			$connection->query($sql);  
		}		
		else
		{
			$sql = "update custom_user_token set user_token='".$token."',user_quote_id='".$quote_id."' where user_id='".$user_id."'"; 
			$connection->query($sql);  		
		}	 
	}	
	
	public function get_user_token_details($user_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		$select = $connection->select()
		->from('custom_user_token')
		->where('user_id = ?', $user_id);  
    $result = $connection->fetchAll($select);
		
		if(!empty($result))
		{
			$array=array(
				'token'=>$result[0]['user_token'],
				'quote_id'=>$result[0]['user_quote_id'],
			);
			return $array;
		} else 
		{
			$token=$api_helpers->get_token(); 
			$quote_id=json_decode($api_helpers->get_quote_id_Om($token,$user_id));
			$sql = "insert into custom_user_token (user_id,user_token,user_quote_id) values ('".$user_id."','".$token."','".$quote_id."')"; 
			$connection->query($sql);  
			$select = $connection->select()
                  ->from('custom_user_token')
				  ->where('user_id = ?', $user_id);  
    	$result = $connection->fetchAll($select);
			$array=array(
				'token'=>$result[0]['user_token'],
				'quote_id'=>$result[0]['user_quote_id'],
			);
			return $array;
		}
		
			// $sql = "update custom_user_token set user_token='".$token."',user_quote_id='".$quote_id."' where user_id='".$user_id."'"; 
			// $connection->query($sql);  		
		
		
	}
	
	
	public function sync_address_mobile($user_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();   
		
		$select = $connection->select()
		->from('customer_address_entity') 
		->where('parent_id = ?', $user_id);    
		 
		$results = $connection->fetchAll($select); 

				 
		$mainArray=array();
		foreach($results as $row1)
		{
			$address_id=$row1['entity_id'];
			
			$select_address = $connection->select()
			->from('customer_address_type') 
			->where('address_id = ?', $address_id);   
			 
			$results_address = $connection->fetchAll($select_address);
			if(empty($results_address))
			{
				$sql = "insert into customer_address_type (address_id,address_type) values ('".$address_id."','1')"; 
				$connection->query($sql);  
			}
		}		
		 
	}
	
	
	public function get_product_return_status_rma($order_id,$rma_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		$select = $connection->select()
				->from('wk_rma') 
				->where('rma_id = ?', $rma_id)
				->where('order_id = ?', $order_id); 
							
		$result = $connection->fetchAll($select);
		$final_status=0; 
		if(!empty($result))
		{
			
			/* echo "<pre>";
			print_r($result);
			echo "</pre>"; */
			$created_at=$result[0]['created_at'];
			$admin_status=$result[0]['admin_status'];
			$final_status=$result[0]['final_status']; 
			$status=1;	
			if($admin_status==5)
			{
				$status=0;	  
			}	
			else if($admin_status==6 && $final_status==3)
			{
				$status=2;			
			}
			
			$final_status=$status;
			
		}
		
		
		$array=array(
			'created_at'=>$created_at,	
			'status'=>$final_status,	
		);
		 
		return $array;
		
		
	}
	
	
	public function refund_payment() 
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		

		$payment_id='pay_Erk9bTY8xEQVYs';
		$amount='482.26';
		
		 

		echo $url=$site_url.'qbonline/connection/razorpaytax/payment_id/'.$payment_id.'/amount/'.$amount; 
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$response  = curl_exec($ch); 
		if($response!='')
		{	
			$result=json_decode($response);
			echo "<pre>";
				print_r($result); 
			echo "</pre>";
		} 
		else
		{
			return 0; 
		}
		
		
		
	}	
	
	
	
	 
	public function api_order_create_krde()
	{
		$user_id=24;
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();   
		$state = $objectManager->get('Magento\Framework\App\State');
		$state->setAreaCode('global');	
		     
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$model = $objectManager->get('\Magento\Quote\Model\QuoteManagement');
		echo $model->placeOrder_mobile(101,$user_id);   
		 
	}	
	
	
	
	  
}
