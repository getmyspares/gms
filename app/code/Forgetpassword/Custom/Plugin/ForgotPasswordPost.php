<?php
namespace Forgetpassword\Custom\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory; 
use Magento\Framework\App\RequestInterface; 
use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator;
class ForgotPasswordPost 
{ 
    protected $resultFactory;
    protected $url;
    protected $_request;
    protected $_response; 
	protected $customerFactory;
	protected $subscriberFactory;
	 protected $session;
	  protected $formKeyValidator; 
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        UrlInterface $url,
		 Session $customerSession,
        ResultFactory $resultFactory,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		StoreManagerInterface $storeManager,
		\Magento\Customer\Model\CustomerFactory $customerFactory,
		 SubscriberFactory $subscriberFactory,
		Validator $formKeyValidator, 
		CustomHelper $helper
		
    )
    {
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->url = $url;
        $this->resultFactory = $resultFactory;
		$this->helper = $helper;    
		$this->messageManager = $messageManager;
		$this->storeManager = $storeManager;
		$this->customerFactory  = $customerFactory;
		 $this->subscriberFactory = $subscriberFactory;
		 $this->session = $customerSession; 
		$this->formKeyValidator = $formKeyValidator; 
    }

    public function aroundExecute(\Magento\Customer\Controller\Account\ForgotPasswordPost $subject, \Closure $proceed) 
	{ 
        
		/* echo "<pre>";
			print_r($_POST);    
		echo "</pre>";  
		die; */
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
		$customers = $objectManager->create('\Magento\Customer\Model\Customer')->getCollection();
		$groupRepository  = $objectManager->create('\Magento\Customer\Api\GroupRepositoryInterface');
		
		 
		$email=$this->_request->getPost('email');
		 
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1);
		$CustomerModel->loadByEmail($email);
		
		//print_r($CustomerModel->getData());
		
		
		
		
		$userId = $CustomerModel->getId();
		if($userId=='')
		{
						
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('*/*/forgotpassword'));
			
			$this->messageManager->addError(__('If there is email registered with this account, a reset email would be sent on this. '));
			return $resultRedirect;   
		}  

		$sql = "Select * FROM  customer_entity where entity_id = '".$userId."'";
		$result = $connection->fetchAll($sql);
		
		$confirm=$result[0]['confirmation'];
		
		if($confirm!='') 
		{
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('*/*/forgotpassword'));
			
			$this->messageManager->addError(__('This email is not confirmed yet.'));
			return $resultRedirect;   
		}   
		 

		
		$phone='';  
		$customerRepository = $objectManager
					   ->get('Magento\Customer\Api\CustomerRepositoryInterface');
		$customer = $customerRepository->getById($userId);
		
		if($myCustomAttribute = $customer->getCustomAttribute('mobile')) {
       $cattrValue = $myCustomAttribute->getValue();
           }

		
		if(!empty($cattrValue))
		{
			$phone=$cattrValue;  
		}	
		
		 
		
		
		foreach ($customers as $customer) {
			$customerId = $customer->getEntityId(); 
			if($userId==$customerId)
			{	
				$groupId = $customer->getGroupId(); 
				
				$group = $groupRepository->getById($groupId); 
				$groupCode = $group->getCode();  
			}    
		}
		
		
		$sql = "Select * FROM marketplace_userdata where seller_id='".$userId."' limit 0,1";
		$result = $connection->fetchAll($sql);
		
		
		$is_seller=0;
		
		if(!empty($result)) {
			$is_seller=$result[0]['is_seller'];
		}
		
		if ($email)  
		{
			if (!\Zend_Validate::is($email, \Magento\Framework\Validator\EmailAddress::class)) {
                $this->session->setForgottenEmail($email);
                
				$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
				$resultRedirect->setUrl($this->url->getUrl('*/*/forgotpassword'));
				
				$this->messageManager->addError(__('We cannot find an account. Verify the email address and password  try again.'));
				return $resultRedirect;   
			}
			
			if($groupId!=1 || $is_seller==1)
			{   
				$resultProceed = $proceed(); 
				return $resultProceed;
			} 			
			else  
			{
				$pin = mt_rand(1000, 9999);				  
				$today=date('Y-m-d H:i:s');
				$forgetotp=$customerSession->getForgetPinJson(); 
				if($forgetotp=='')
				{
					$forgetotp=0;	
				}  	
				 
				if($forgetotp==0)
				{   
					$completeUser=array(
						'pin'=>$pin,	
						'phone'=>$phone,	
						'userId'=>$userId,	  
						'email'=>$email,	  
						'cr_time'=>$today	  
					);    
  
					$userForgetPinJson=json_encode($completeUser);
				
					$customerSession->setForgetPinJson($userForgetPinJson); 
					$customerSession->getForgetPinJson();  
					
					$customerSession->setForgetcountdown(1); 		

					$smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
					$smsHelper->send_sms($phone,$pin);	  
					// $message="Please complete your Forget Password process by entering this OTP - ".$pin; 
					// $this->helper->ForgetPasswordHelperOTP($pin,$phone,$message,$email);  
					
					 
					$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
					$resultRedirect->setUrl($this->url->getUrl('*/*/forgotpassword'));
					return $resultRedirect;   
				}  	
			} 
			
		}
		
    }
}