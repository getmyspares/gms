<?php
namespace OM\Rma\Controller\DamagedItem;
use Magento\Framework\Controller\ResultFactory; 

class SaveCertificate extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
        \OM\Rma\Helper\DamagedItem\Data $damagedItemHelper,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Message\ManagerInterface $ManagerInterface,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploader,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
        $this->damagedItemHelper = $damagedItemHelper;
        $this->damagedFactory = $damagedFactory;
		$this->_pageFactory = $pageFactory;
        $this->fileUploader = $fileUploader;
        $this->messageManager = $ManagerInterface;
        $this->customerSession = $customerSession;
        $this->filesystem = $filesystem;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
		return parent::__construct($context);
	}

	public function execute()
	{
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if(!empty($_FILES) && isset($_POST['rma_id'])){
            $rma_id = $_POST['rma_id'];
            $damagedData = $this->damagedFactory->create();
            $collection = $damagedData->getCollection()->addFieldToFilter('rma_id',$rma_id);

            if(empty($collection->getFirstItem()->getDamageCertificate())){
                $fileUpload = $this->uploadFile('damage_certificate', 'om/rma/damaged_item_docs/'.$rma_id.'/');
                if($fileUpload['code'] == 'error'){
                    $data = [
                        'code' => 'error',
                        'msg'   => $fileUpload['msg'],
                    ];
                }else{
                    $this->damagedItemHelper->sendMailToInsurencePartner($fileUpload['msg'], $rma_id);
                    $this->customerSession->setCertUploaded($rma_id);

                    $data = [
                        'code' => 'success',
                        'msg'   => 'Certificate uploaded successfully.',
                    ];

                }
            }else{
                $data = [
                    'code' => 'error',
                    'msg'   => "Certificate already exist for rma id : $rma_id",
                ];
            }
        }else{
            $data = [
                'code' => 'error',
                'msg'   => 'Invalid Request , Required data is missing for form submittion',
            ];
        }


        if($data['code']=='error')
        {
            $this->messageManager->addNotice($data['msg']);
        }

        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

        // echo json_encode($data);
	}

    public function uploadFile($input_fileName, $folderName)
    {
        try{
            $file = $this->getRequest()->getFiles($input_fileName);

            $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;
            
            if ($file && $fileName) {
                $target = $this->mediaDirectory->getAbsolutePath($folderName); 
                
                /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                $uploader = $this->fileUploader->create(['fileId' => $input_fileName]);
                
                // set allowed file extensions
                $uploader->setAllowedExtensions(['pdf']);
                
                // allow folder creation
                $uploader->setAllowCreateFolders(true);
                
                // rename file name if already exists 
                $uploader->setAllowRenameFiles(true);
                
                // upload file in the specified folder
                $result = $uploader->save($target);
                
                //echo '<pre>'; print_r($result); exit;
                
                $data = array(
                    'code' => 'success',
                    'msg' => $uploader->getUploadedFileName()
                );
            }
        } catch (\Exception $e) {
            $data = array(
                'code' => 'error',
                'msg' => $e->getMessage()
            );
        }
        return $data;
    }

}