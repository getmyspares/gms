<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rma\Controller\DamagedItem;

class InitiateClaim extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_FOR_ADMIN_NAME    = 'dmgpolicy/admin/admin_name';
    const XML_PATH_FOR_ADMIN_EMAIL   = 'dmgpolicy/admin/admin_email';

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \OM\Rma\Helper\DamagedItem\Data $damageHelper,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Webkul\Rmasystem\Api\AllRmaRepositoryInterface $rmaRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->damageHelper = $damageHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->rmaRepository = $rmaRepository;
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // return $this->resultPageFactory->create();
        $post = $this->getRequest()->getParams();
        $connection = $this->resourceConnection->getConnection();
        $rmaData = $this->rmaRepository->getById($post['id']);
        $order_id = $rmaData->getOrderId();
        $table_seller = $connection->getTableName('marketplace_orders');
        $query_seller = "SELECT `seller_id` FROM `" . $table_seller . "` WHERE order_id =  $order_id";
        $result_seller = $connection->fetchRow($query_seller);
        $sellerData = $this->customerFactory->create()->load($result_seller['seller_id']);

        $seller_name = $sellerData->getFirstname().' '.$sellerData->getMiddlename().' '.$sellerData->getLastname();
        $seller_email = $sellerData->getEmail();

        $receiverInfo_seller = [
            'name' => $seller_name,
            'email' => $seller_email,
        ];
        $order_inc_id = $rmaData->getIncrementId();
        $templateVars_seller = [
            'greeting_name' => $seller_name,
            'subject'       => 'Damaged Item Claim Alert',
            'msg'           =>  "Your order - $order_inc_id claim has been initiated."
        ];


        $admin_name = $this->scopeConfig->getValue(self::XML_PATH_FOR_ADMIN_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $admin_email = $this->scopeConfig->getValue(self::XML_PATH_FOR_ADMIN_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $receiverInfo_admin = [
            'name' => $admin_name,
            'email' => $admin_email,
        ];
        $rma_id = $rmaData->getRmaId();
        $templateVars_admin = [
            'greeting_name' => $admin_name,
            'subject'   => 'Damaged Item Claim Alert',
            'msg'       =>  "Damaged item [RMA ID - $rma_id, ORDER ID - $order_inc_id] claim has been initiated. Please login to dashbord and complete process."
        ];

        $store = $this->storeManager->getStore();

        try{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
            $transport_admin = $transportBuilder->setTemplateIdentifier('admin_alert_for_damageditem')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                ->addTo($receiverInfo_admin['email'], $receiverInfo_admin['name'])
                ->setTemplateVars($templateVars_admin)
                ->setFrom('general')
                ->getTransport();
            $transport_admin->sendMessage();

            $transport_seller = $transportBuilder->setTemplateIdentifier('admin_alert_for_damageditem')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                ->addTo($receiverInfo_seller['email'], $receiverInfo_seller['name'])
                ->setTemplateVars($templateVars_seller)
                ->setFrom('general')
                ->getTransport();
            $transport_seller->sendMessage();            

            $this->damageHelper->setClaimStatus($rma_id,'1');
            // $this->messageManager->addSuccess(__('Claim initiated successfully'));
            // $resultRedirect = $this->resultRedirectFactory->create();
            // $resultRedirect->setPath('marketplace/account/rmalisting');
            // return $resultRedirect;
            $data = array(
                'code'  => 'success',
                'msg'   => 'Claim initiated successfully',
            );

        }catch(\Exception $e){
            // $this->messageManager->addError(__($e->getMessage()));
            // $resultRedirect = $this->resultRedirectFactory->create();
            // $resultRedirect->setPath('marketplace/account/rmalisting');
            // return $resultRedirect;
            $data = array(
                'code'  => 'error',
                'msg'   => $e->getMessage(),
            );

        }

        echo json_encode($data);

    }
}

