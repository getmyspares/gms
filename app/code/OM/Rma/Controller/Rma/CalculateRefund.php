<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rma\Controller\Rma;

use OM\Rma\Helper\CreateCreditnote;

class CalculateRefund extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OM\Rma\Helper\CreateCreditnote $createCreditnote,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->creditnoteHelper=$createCreditnote;
        $this->connection = $resourceConnection->getConnection();
    }

    public function execute()
    {
        $payload = json_decode($this->getRequest()->getContent(),true);
        $total=0;
        foreach ($payload as $item)
        {
            $item_id = $item['item_id'];
            $rma_id = $item['rma_id'];
            $shipping_option = ($item['shipping_option']=='yes')?'yes':'no';
            $total+=$this->getItemTotal($rma_id,$item_id,$shipping_option);
        }
        if($total>0)
        {
            echo $total;
        }
    }

    public function getItemTotal($rma_id,$item_id,$shipping_option)
    {
        $query = "select price,order_id from sales_order_item where item_id='$item_id'";
        $item_total = $this->connection->fetchAll($query);
        if(!empty($item_total))
        {
           $price = $item_total[0]['price'];
           $order_id = $item_total[0]['order_id'];
           $query = "select qty from wk_rma_items where item_id='$item_id' and rma_id='$rma_id'";
           $rma_requested_qty = $this->connection->fetchOne($query);

           $item_total = (float)($price *$rma_requested_qty) ;

            if($shipping_option=="yes")
            {
                $item_total += $this->calculateShipping($order_id,$item_id,$rma_requested_qty);
            }

            return $item_total;
        }
        return 0;


    }

    public function calculateShipping($order_id,$item_id,$rma_requested_qty)
    {
        $sql = "Select base_shipping_amount FROM sales_order where entity_id='$order_id'";
        $base_shipping_amount = $this->connection->fetchOne($sql);
        /* typecasting to int  as float  0.00 is  treated as not empty in php , it  will give logical error  whihc would be difficult  to debug
        do not  use $base_shipping_amount in calculation as it has rounded int value
        @ritesh5april2021
        */
        $base_shipping_amount=(int)$base_shipping_amount;
        if(!empty($base_shipping_amount))
        {

            return (float)$this->creditnoteHelper->calculateShipping($order_id,$item_id,$rma_requested_qty);
        }
        return 0;
    }
}

