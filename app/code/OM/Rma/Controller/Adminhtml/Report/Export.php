<?php
namespace OM\Rma\Controller\Adminhtml\Report;
class Export extends  \Magento\Backend\App\Action {
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \OM\Rma\Helper\RmaReportHelper $RmaReportHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \OM\Rma\Helper\RmaReportHelper $RmaReportHelper
    )
    {
        parent::__construct($context);
        $this->rmaReportHelper = $RmaReportHelper;
    }

    public  function execute()
    {
        $this->rmaReportHelper->downloadRmaReport();
    }

    protected function _isAllowed()
    {
        return true;
    }
}







?>