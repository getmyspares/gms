<?php
    namespace OM\Rma\Controller\Adminhtml\RegenrevPickups;

    use OM\Rma\Helper\RegenrevPickup;
    use \Magento\Backend\App\Action\Context;
    use \Magento\Framework\View\Result\PageFactory;

    class Index extends \Magento\Backend\App\Action{

        public function __construct(
            Context $context,
            RegenrevPickup $rmaHelper
        ) {
            parent::__construct($context);
            $this->_rmaHelper = $rmaHelper;
        }

        public function execute(){
            $rma_id = $this->getRequest()->getParams();
            $status = $this->_rmaHelper->flushAdminConsignmentNo($rma_id['id']);
            if($status){
                $this->messageManager->addSuccessMessage("Reverse Awb Regenerated");
            } else{
                $this->messageManager->addErrorMessage("Reverse Awb Regeneration Failed");
            }

            $resultRedirect = $this->resultRedirectFactory->create();
            $url = $this->_redirect->getRefererUrl();

            $resultRedirect->setUrl($url);
            return $resultRedirect;
        }
        protected function _isAllowed(){
            /*read about acl and purpose of acl.xml , this should not be true always */
            return true;
        }
    }
?>