<?php
namespace OM\Rma\Controller\Adminhtml\DamagedItem;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class ResendLogisticEmail extends Action
{
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        \Webkul\Rmasystem\Api\Data\RmaitemInterfaceFactory $rmaItemDataFactory,
        \OM\Rma\Helper\DamagedItem\Data $damagedItemHelper,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        Context $context,
        PageFactory $resultPageFactory
    ) {    
        parent::__construct($context);
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->damagedItemHelper = $damagedItemHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->rmaItemDataFactory = $rmaItemDataFactory;
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Rmasystem::damageditem');
    }

    /**
     * View Rma page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $rma_id = $this->getRequest()->getParam('rma_id');
        $id = $this->getRequest()->getParam('id');
        if(!empty($rma_id)){
            $resultRedirect = $this->resultRedirectFactory->create();
            $orderItems = $this->rmaItemDataFactory->create()
                            ->getCollection()
                            ->addFieldToFilter('rma_id', $rma_id);

            $item = $orderItems->getFirstItem();

            // foreach($orderItems as ){
                $this->damagedItemHelper->sendMailToLogisticPartner($item->getRmaId(), '', $item->getItemId());
            // }
            $this->messageManager->addSuccess(__("Email has been sent"));
            $resultRedirect->setPath('rmsystem/damageditem/edit',['id'=>$id]);
            return $resultRedirect;
        }
    }
}
