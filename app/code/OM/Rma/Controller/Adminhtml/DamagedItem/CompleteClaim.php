<?php
namespace OM\Rma\Controller\Adminhtml\DamagedItem;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class CompleteClaim extends Action
{
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        PageFactory $resultPageFactory
    ) {
    
        parent::__construct($context);
        $this->damagedFactory = $damagedFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Rmasystem::damageditem');
    }

    /**
     * Product list page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute(){
        $post = $this->getRequest()->getParams();
        try{
            $collection = $this->damagedFactory->create();
            $singleRow = $collection->getCollection()->addFieldToFilter('rma_id', $post['id'])->getFirstItem();
            $updStatus = $collection->load($singleRow->getId());
            $updStatus->setClaimStatus('4');
            $updStatus->save();

            $this->messageManager->addSuccess(__('Claim completed successfully'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('rmsystem/damageditem/index');
            return $resultRedirect;

        }catch(\Exception $e){
            $this->messageManager->addError(__('Somthing went wrong! while submitting claim.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('rmsystem/damageditem/index');
            return $resultRedirect;
        }

    }
}
