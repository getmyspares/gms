<?php
namespace OM\Rma\Controller\Adminhtml\DamagedItem;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Update extends Action
{
    const XML_PATH_FOR_POLICY_NO        = 'dmgpolicy/desktop/dmgpolicy_no';
    const XML_PATH_FOR_INSURENCE_NAME   = 'dmgpolicy/insurence/insurence_name';
    const XML_PATH_FOR_INSURENCE_EMAIL  = 'dmgpolicy/insurence/insurence_email';
    const XML_PATH_FOR_LOGISTIC_NAME    = 'dmgpolicy/logistic/logistic_name';
    const XML_PATH_FOR_LOGISTIC_EMAIL   = 'dmgpolicy/logistic/logistic_email';
    const XML_PATH_FOR_CONDITION_NUMBER = 'dmgpolicy/condition/condition_number';
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \OM\Rma\Helper\DamagedItem\Data $damagedItemHelper,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \OM\GenerateReport\Helper\Mail $mailHelper,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Panasonic\CustomUser\Helper\Data $panasonicHelper,
        \Webkul\Rmasystem\Api\AllRmaRepositoryInterface $rmaRepository,
        \Magento\Sales\Model\OrderRepository $orderRepo,
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploader,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
        \Webkul\Rmasystem\Helper\Data $webkulHelper,
        PageFactory $resultPageFactory
    ) {    
        parent::__construct($context);
        $this->damagedItemHelper = $damagedItemHelper;
        $this->customerFactory = $customerFactory;
        $this->resourceConnection = $resourceConnection;
        $this->damagedFactory = $damagedFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->rmaRepository = $rmaRepository;
		$this->panasonicHelper=$panasonicHelper;
        $this->mailHelper = $mailHelper;
        $this->orderRepo = $orderRepo;
        $this->_transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->filesystem = $filesystem;
        $this->fileUploader = $fileUploader;
        $this->webkulHelper = $webkulHelper;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Rmasystem::damageditem');
    }

    /**
     * View Rma page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $rmaData = $this->rmaRepository->getById($post['rma_id']);
        $rma_id = $post['rma_id'];

        $damagedData = $this->damagedFactory->create();
        $collection = $damagedData->getCollection()->addFieldToFilter('rma_id',$rmaData->getRmaId());
        $updDamagedData = $damagedData->load($collection->getFirstItem()->getId());

        if(empty($collection->getFirstItem()->getDamageCertificate())){
            $fileUpload = $this->uploadFile('damage_certificate', 'om/rma/damaged_item_docs/'.$rma_id.'/');

            if($fileUpload['code'] == 'error'){
                $this->messageManager->addError(__($fileUpload['msg']));
                $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
                return $resultRedirect;
            } else{
                $updDamagedData->setDamageCertificate($fileUpload['msg']);
                $updDamagedData->save();
                
                $this->damagedItemHelper->sendMailToInsurencePartner($fileUpload['msg'],$rma_id);

                $this->messageManager->addSuccess(__('Claim submitted!, Email has been sent successfully'));
                $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
                return $resultRedirect;
            }
        }else{
            $this->messageManager->addError(__("Certificate already exist for rma id : $rma_id"));
            $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
            return $resultRedirect;
        }
    }

    public function uploadFile($input_fileName, $folderName)
    {
        try{
            $file = $this->getRequest()->getFiles($input_fileName);

            $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;
            
            if ($file && $fileName) {
                $target = $this->mediaDirectory->getAbsolutePath($folderName); 
                
                /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                $uploader = $this->fileUploader->create(['fileId' => $input_fileName]);
                
                // set allowed file extensions
                $uploader->setAllowedExtensions(['pdf']);
                
                // allow folder creation
                $uploader->setAllowCreateFolders(true);
                
                // rename file name if already exists 
                $uploader->setAllowRenameFiles(true);
                
                // upload file in the specified folder
                $result = $uploader->save($target);
                
                //echo '<pre>'; print_r($result); exit;
                
                $data = array(
                    'code' => 'success',
                    'msg' => $uploader->getUploadedFileName()
                );
            }
        } catch (\Exception $e) {
            $data = array(
                'code' => 'error',
                'msg' => $e->getMessage()
            );
        }
        return $data;
    }
}
