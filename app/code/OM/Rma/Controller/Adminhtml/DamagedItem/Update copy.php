<?php
namespace OM\Rma\Controller\Adminhtml\DamagedItem;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Update extends Action
{
    const XML_PATH_FOR_POLICY_NO        = 'dmgpolicy/desktop/dmgpolicy_no';
    const XML_PATH_FOR_INSURENCE_NAME   = 'dmgpolicy/insurence/insurence_name';
    const XML_PATH_FOR_INSURENCE_EMAIL  = 'dmgpolicy/insurence/insurence_email';
    const XML_PATH_FOR_LOGISTIC_NAME    = 'dmgpolicy/logistic/logistic_name';
    const XML_PATH_FOR_LOGISTIC_EMAIL   = 'dmgpolicy/logistic/logistic_email';
    const XML_PATH_FOR_CONDITION_NUMBER = 'dmgpolicy/condition/condition_number';
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \OM\GenerateReport\Helper\Mail $mailHelper,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Panasonic\CustomUser\Helper\Data $panasonicHelper,
        \Webkul\Rmasystem\Api\AllRmaRepositoryInterface $rmaRepository,
        \Magento\Sales\Model\OrderRepository $orderRepo,
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploader,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
        \Webkul\Rmasystem\Helper\Data $webkulHelper,
        PageFactory $resultPageFactory
    ) {    
        parent::__construct($context);
        $this->customerFactory = $customerFactory;
        $this->resourceConnection = $resourceConnection;
        $this->damagedFactory = $damagedFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->rmaRepository = $rmaRepository;
		$this->panasonicHelper=$panasonicHelper;
        $this->mailHelper = $mailHelper;
        $this->orderRepo = $orderRepo;
        $this->_transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->filesystem = $filesystem;
        $this->fileUploader = $fileUploader;
        $this->webkulHelper = $webkulHelper;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Rmasystem::damageditem');
    }

    /**
     * View Rma page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $rmaData = $this->rmaRepository->getById($post['rma_id']);
        $damagedData = $this->damagedFactory->create();
        $collection = $damagedData->getCollection()->addFieldToFilter('rma_id',$rmaData->getRmaId());
        $updDamagedData = $damagedData->load($collection->getFirstItem()->getId());

        $orderData = $this->orderRepo->get($post['order_id']);
        if ($orderData->hasInvoices()){
            foreach($orderData->getInvoiceCollection() as $invoice){
                $invoice_no = $invoice->getIncrementId();
                $invoice_date = date('d-m-Y',strtotime($invoice->getCreatedAt()));
            }
        }
        $connection = $this->resourceConnection->getConnection();
        $table_awb = $connection->getTableName('ecomexpress_awb');
        $oid = $post['order_id'];
        $query_awb = "SELECT `awb` FROM `" . $table_awb . "` WHERE orderid =  $oid";
        $result_awb = $connection->fetchRow($query_awb);

        $table_seller = $connection->getTableName('marketplace_orders');
        $query_seller = "SELECT `seller_id` FROM `" . $table_seller . "` WHERE order_id =  $oid";
        $result_seller = $connection->fetchRow($query_seller);
        $sellerData = $this->customerFactory->create()->load($result_seller['seller_id']);
        // echo "<pre>";
        // print_r($sellerData->getData());
        // die();
        if(!empty($rmaData->getCreditMemoId())){
            $creditMemoData = $this->creditmemoRepository->get($rmaData->getCreditMemoId());
        }else{
            $creditMemoData = '';
        }

        $store = $this->storeManager->getStore();

        $folderName = $this->webkulHelper->getBaseDirRead().$rmaData->getRmaId().'/image/';
        $images = glob($folderName.'*.{jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG,bmp,BMP}', GLOB_BRACE);
        $additionalImages = [];
        foreach ($images as $filename) {
            $getfilename = explode("/",$filename);
            $justfname = $this->webkulHelper->getBaseUrl().$rmaData->getRmaId().'/image'."/".end($getfilename);
            $additionalImages[] = "<a href=".$justfname." download><img class='wk_rma_add_images' alt='".end($getfilename)."' height='50px' width='100px' src='".$justfname."'/></a>";
        }

        $certificateName = $certificatePath = $loss_estimate = '';
        $insurence_name = $this->_scopeConfig->getValue(self::XML_PATH_FOR_INSURENCE_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $insurence_email= $this->_scopeConfig->getValue(self::XML_PATH_FOR_INSURENCE_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $logistic_name  = $this->_scopeConfig->getValue(self::XML_PATH_FOR_LOGISTIC_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $logistic_email = $this->_scopeConfig->getValue(self::XML_PATH_FOR_LOGISTIC_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $condition_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_CONDITION_NUMBER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if(!empty($creditMemoData)){
            if($creditMemoData->getGrandTotal()>$condition_number){
                //Push to insurance Co.
                if(!empty($_FILES)){
                    $fileUpload = $this->uploadFile('damage_certificate', 'om/rma/damaged_item_docs/');
                    if($fileUpload['code'] == 'error'){
                        $this->messageManager->addError(__($fileUpload['msg']));
                        $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
                        return $resultRedirect;
                    }else{
                        $updDamagedData->setDamageCertificate($fileUpload['msg']);
                        $updDamagedData->setClaimStatus('2');
                        $updDamagedData->save();
                        $getCertificate = $damagedData->getCollection()->addFieldToFilter('rma_id',$post['rma_id']);
                        $certificatePath = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'om/rma/damaged_item_docs/';
                        $certificateName = $getCertificate->getFirstItem()->getDamageCertificate();
                        $loss_estimate = $creditMemoData->getGrandTotal();
                        $policy_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_POLICY_NO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

                        $receiverInfo = [
                            'name' => $insurence_name,
                            'email' => $insurence_email,
                        ];
                    }
                }else{
                    $this->messageManager->addError(__('Please upload required certificate first!'));
                    $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
                    return $resultRedirect;
                }
            }else{
                //Push to logistics
                $updDamagedData->setClaimStatus('3');
                $updDamagedData->save();
                $loss_estimate = $creditMemoData->getGrandTotal();
                $policy_number = $orderData->getIncrementId();
                $receiverInfo = [
                    'name' => $logistic_name,
                    'email' => $logistic_email,
                ];
            }
        }else{
            if($rmaData->getStatus() == '12'){
                $_order = $this->orderRepo->get($rmaData->getOrderId());
                // echo "<pre>";
                // print_r($_order->getSubtotal());
                // die();

                if($_order->getSubtotal()>'5000'){
                    //Push to insurance Co.
                    if(!empty($_FILES)){
                        $fileUpload = $this->uploadFile('damage_certificate', 'om/rma/damaged_item_docs/');
                        if($fileUpload['code'] == 'error'){
                            $this->messageManager->addError(__($fileUpload['msg']));
                            $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
                            $resultRedirect = $this->resultRedirectFactory->create();
                            $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
                            return $resultRedirect;
                        }else{
                            $updDamagedData->setDamageCertificate($fileUpload['msg']);
                            $updDamagedData->setClaimStatus('2');
                            $updDamagedData->save();
                            $getCertificate = $damagedData->getCollection()->addFieldToFilter('rma_id',$post['rma_id']);
                            $certificatePath = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'om/rma/damaged_item_docs/';
                            $certificateName = $getCertificate->getFirstItem()->getDamageCertificate();
                            $loss_estimate = $_order->getSubtotal();
                            $policy_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_POLICY_NO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $receiverInfo = [
                                'name' => $insurence_name,
                                'email' => $insurence_email,
                            ];
                        }
                    }else{
                        $this->messageManager->addError(__('Please upload required certificate first!'));
                        $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
                        return $resultRedirect;
                    }
                }else{
                    //Push to logistics
                    $updDamagedData->setClaimStatus('3');
                    $loss_estimate = $_order->getSubtotal();
                    $policy_number = $orderData->getIncrementId();

                    $receiverInfo = [
                        'name' => $logistic_name,
                        'email' => $logistic_email,
                    ];
                    $updDamagedData->save();
                }

            }
        }

        $templateVars = [
            'greetingName'              => $receiverInfo['name'],
            'subject'                   => 'test email',
            'policy_no'                 => $policy_number,
            'policy_copy'               => 'policy_copy',
            'invoice_no'                => $invoice_no,
            'invoice_date'              => $invoice_date,
            'awb_no'                    => $result_awb['awb'],
            'loss_estimate'             => number_format($loss_estimate,2,'.',','),
            'rsrv_amt_crm'              => '',
            'cust_return_date'          => date('d/m/Y'),
            'seller_address'            => $sellerData->getCompanyAdd(),
            'coverage_details'          => '',
            'seller_firstname_name'     => $sellerData->getFirstname(),
            'seller_middlename_name'    => $sellerData->getMiddlename(),
            'seller_lastname_name'      => $sellerData->getLastname(),
            'seller_mobile'             => $sellerData->getMobile(),
            'dmg_certificate'           =>  'Please find attached damaged certificate.',
            'dmg_product_images'        => implode(' ',$additionalImages),
        ];
        
        try{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
            if(!empty($certificateName) && !empty($certificatePath)){
                $fileContent = file_get_contents($certificatePath.$certificateName); 
                $transport = $transportBuilder->setTemplateIdentifier('damaged_item_email_template')
                    ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                    ->addTo($receiverInfo['email'], $receiverInfo['name'])
                    ->setTemplateVars($templateVars)
                    ->setFrom('general')
                    ->addAttachment($fileContent, $certificateName, 'application/pdf')
                    ->getTransport();

            }else{
                $transport = $transportBuilder->setTemplateIdentifier('damaged_item_email_template')
                    ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                    ->addTo($receiverInfo['email'], $receiverInfo['name'])
                    ->setTemplateVars($templateVars)
                    ->setFrom('general')
                    ->getTransport();
            }
            // ->addCc($ccEmail)
            $transport->sendMessage();

            $this->messageManager->addSuccess(__('Claim submitted!, Email has been sent successfully'));
            $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
            return $resultRedirect;

        }catch(\Exception $e){
            $this->messageManager->addError(__($e->getMessage()));
            $argument = ['id' => $collection->getFirstItem()->getId(), '_current' => true];
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('rmsystem/damageditem/edit', $argument);
            return $resultRedirect;
        }
    }

    public function uploadFile($input_fileName, $folderName)
    {
        try{
            $file = $this->getRequest()->getFiles($input_fileName);

            $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;
            
            if ($file && $fileName) {
                $target = $this->mediaDirectory->getAbsolutePath($folderName); 
                
                /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                $uploader = $this->fileUploader->create(['fileId' => $input_fileName]);
                
                // set allowed file extensions
                $uploader->setAllowedExtensions(['pdf']);
                
                // allow folder creation
                $uploader->setAllowCreateFolders(true);
                
                // rename file name if already exists 
                $uploader->setAllowRenameFiles(true);
                
                // upload file in the specified folder
                $result = $uploader->save($target);
                
                //echo '<pre>'; print_r($result); exit;
                
                $data = array(
                    'code' => 'success',
                    'msg' => $uploader->getUploadedFileName()
                );
            }
        } catch (\Exception $e) {
            $data = array(
                'code' => 'error',
                'msg' => $e->getMessage()
            );
        }
        return $data;
    }
}
