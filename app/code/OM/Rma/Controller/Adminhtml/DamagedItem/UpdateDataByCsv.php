<?php

namespace OM\Rma\Controller\Adminhtml\DamagedItem;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class UpdateDataByCsv extends Action
{
     /**
      * @var \Magento\Framework\View\Result\PageFactory
      */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->damagedFactory = $damagedFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager = $messageManager;
        $this->csvProcessor = $csvProcessor;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }


    /**
     * Product list page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {   
        $file = $this->getRequest()->getFiles('csv_file');
        $resultRedirect = $this->resultRedirectFactory->create();

        if(!empty($file)){
            $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            if($ext == 'csv'){
                try{
                    $tempFile  = $file['tmp_name'];
                    $importCsvData = $this->csvProcessor->getData($tempFile);
                    $csvHeader = ['rma_id','status','comment'];
                    $i = 0;
                    $rma_id = '';
                    foreach ($importCsvData as $rowIndex => $dataRow) {
                        if($i == 0){
                            if($dataRow != $csvHeader){
                                $this->messageManager->addError(__('Please upload correct csv file!'));
                                $resultRedirect->setPath('rmsystem/damageditem/uploadcsv');
                                return $resultRedirect;
                            }
                        }else{
                            $rma_id = $dataRow['0'];
                            if(!empty($dataRow['0'] && !empty($dataRow['1'] && !empty($dataRow['2'])))){
                                $collection = $this->damagedFactory->create();
                                $singleRow = $collection->getCollection()->addFieldToFilter('rma_id', $dataRow['0'])->getFirstItem();
                                if(!empty($singleRow->getData())){
                                    $updStatus = $collection->load($singleRow->getId());
                                    $updStatus->setClaimStatus($dataRow['1']);
                                    $updStatus->setApprovalComment($dataRow['2']);
                                    $updStatus->save();
                                    $this->messageManager->addSuccess(__("Data Successfully Updated. RMA id - $rma_id"));
                                }else{
                                    $this->messageManager->addError(__("Data not updated with, RMA id - $rma_id"));
                                }
                            }else{
                                $this->messageManager->addError(__("Data not updated because some fields are missing with, RMA id - $rma_id"));
                            }
                        }
                        $i++;
                    }
                    $resultRedirect->setPath('rmsystem/damageditem/index');
                    return $resultRedirect;
                } catch (\Exception $e) {
                    $this->messageManager->addError(__($e->getMessage()));
                    $resultRedirect->setPath('rmsystem/damageditem/uploadcsv');
                    return $resultRedirect;
                }
            }else{
                $this->messageManager->addError(__('Please upload csv file!'));
                $resultRedirect->setPath('rmsystem/damageditem/uploadcsv');
                return $resultRedirect;
            }
        }else{
            $this->messageManager->addError(__('Please upload csv file!'));
            $resultRedirect->setPath('rmsystem/damageditem/uploadcsv');
            return $resultRedirect;
        }
    }
}
