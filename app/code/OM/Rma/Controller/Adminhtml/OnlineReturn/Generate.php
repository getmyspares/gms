<?php

namespace OM\Rma\Controller\Adminhtml\OnlineReturn;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResourceConnection;

class Generate extends Action
{

    public function __construct(
        Context $context,
        ResourceConnection $ResourceConnection,
        \OM\Rma\Helper\OnlineRefund $onlineRefund,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        ResultFactory $resultFactory
    )
    {
        parent::__construct($context);

        $this->_resultFactory = $resultFactory;
        $this->_messageManager = $messageManager;
        $this->onlineRefundHelper = $onlineRefund;
        $this->_connection = $ResourceConnection->getConnection();
    }

    public  function execute()
    {
        $creditmemo_id = $this->getRequest()->getParam('creditmemo_id');
        $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if(empty($creditmemo_id))
        {
            $msg="Requested Credit Memo not found";
            $this->_messageManager->addError($msg);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        $qry = "SELECT order_id FROM `sales_creditmemo` where entity_id='$creditmemo_id'";
        $order_id = $this->_connection->fetchOne($qry);
        if(empty($order_id))
        {
            $msg="Requested Credit Memo not found";
            $this->_messageManager->addError($msg);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        $qry = "SELECT txn_id FROM `sales_payment_transaction` where order_id='$order_id'";
        $txn_id = $this->_connection->fetchOne($qry);
        if(empty($txn_id))
        {
            $msg="Online Transaction Razorpay Id  not  found";
            $this->_messageManager->addError($msg);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        
        $qry = "SELECT base_grand_total FROM `sales_creditmemo` where entity_id='$creditmemo_id'";
        $base_grand_total = $this->_connection->fetchOne($qry);
        /* need to  convert amount in razorpay  format so  that  it  can be accepted , convert  to  paise @ritesh191120202*/
        $roundedvalue =  round($base_grand_total,2,PHP_ROUND_HALF_DOWN);
        $amount_to_be_refunded = $roundedvalue*100;
        $refund_status = $this->onlineRefundHelper->razorpayPartialrefundOnline($txn_id,$amount_to_be_refunded);
        
        if($refund_status['status']=='Success')
        {
            $msg="Online Refund of $base_grand_total Has been processed for  the razor pay  id  $txn_id";
            $this->_messageManager->addSuccess($msg);
            $refund_status = $this->onlineRefundHelper->SaveRefundResponse($refund_status['refund_data'],$creditmemo_id);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        } else 
        {
            $msg =  $refund_status['message'];
            $this->_messageManager->addError($msg);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }

    }

    protected function _isAllowed()
    {       
        return true;
        // return $this->_authorization->isAllowed('OM_Rma::onlinerefund');
    }
}



















?>