<?php

namespace OM\Rma\Controller\Adminhtml\OnlineReturn;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;


class Razorpay extends Action
{

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }

    public  function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->addBreadcrumb('Razorpay', 'Online Refund');
        $resultPage->getConfig()->getTitle()->prepend(__('Razorpay Refund Data'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('OM_Rma::onlinerefund');
    }
}



















?>