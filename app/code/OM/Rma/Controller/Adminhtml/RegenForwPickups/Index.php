<?php
    namespace OM\Rma\Controller\Adminhtml\RegenForwPickups;

    use OM\Rma\Helper\RegenForwPickup;
    use \Magento\Backend\App\Action\Context;
    use \Magento\Framework\View\Result\PageFactory;

    class Index extends \Magento\Backend\App\Action{
        protected $resultPageFactory;

        public function __construct(
            Context $context,
            RegenForwPickup $rmaHelper,
            PageFactory $resultPageFactory
        ) {
            parent::__construct($context);
            $this->_rmaHelper = $rmaHelper;
            /* copied from pankaj , these 2 classes are not even being used ,
            inface the whole functionality is just copy and pasted without even seeing what is written @ritesh9sept2021
            */
            $this->resultPageFactory = $resultPageFactory;
            $this->resultFactory = $context->getResultFactory();
        }

        public function execute(){
            $rma_id = $this->getRequest()->getParams();
            $status = $this->_rmaHelper->flushCustomerConsignmentNo($rma_id['id']);
            if($status){
                $this->messageManager->addSuccessMessage("Status has been changed to reverse pickup complete.");
            } else{
                $this->messageManager->addErrorMessage("Could not change status , some error occured.");
            }

            $resultRedirect = $this->resultRedirectFactory->create();
            $url = $this->_redirect->getRefererUrl();

            $resultRedirect->setUrl($url);
            return $resultRedirect;
        }
        protected function _isAllowed(){
            /*why is it true , when it will be false ? another example of copy paste @ritesh9sept2021*/
            return true;
        }
    }
?>