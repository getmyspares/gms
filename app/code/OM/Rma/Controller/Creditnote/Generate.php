<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rma\Controller\Creditnote;
use Magento\Framework\Controller\ResultFactory;

class Generate extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $_inlineTranslation;
    protected $_transportBuilder;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        ResultFactory $resultFactory,
        /* for approve */
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \OM\MobileOtp\Helper\SmsHelper $SmsHelper,
        \Panasonic\CustomUser\Helper\Data $mailHelper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \OM\Rma\Helper\CreateCreditnote $CreateCreditnote,       
        /* for  full  refund */
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory,
        \Magento\Sales\Model\Order\Invoice $invoice,
        \Magento\Sales\Model\Service\CreditmemoService $creditmemoService,
        /* for deline   */

        // for Damaged item refund
        \OM\Rma\Helper\DamagedItem\Data $damagedItemHelper

    ) {
        $this->resultFactory = $resultFactory;
        $this->_smsHelper = $SmsHelper;
        $this->_mailHelper = $mailHelper;
        $this->creditnoteHelper = $CreateCreditnote;
        
        $this->connection = $resource->getConnection();
        $this->_messageManager = $messageManager;
        $this->customerSession = $customerSession;
        
        /* for  full refund  */
        $this->order = $order;
        $this->creditmemoFactory = $creditmemoFactory;
        $this->creditmemoService = $creditmemoService;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->invoice = $invoice; 

        // for Damaged item refund
        $this->damagedItemHelper = $damagedItemHelper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $post = $this->getRequest()->getPostValue();
        $rma_action=$post["rma_action"];
        $rma_id=$post["rma_id"];
        $remark = $post["seller_remark"];
        $remark_for_customer = @$post["remark_for_customer"];
        $message="";
        
        
        $query =  "SELECT * FROM `wk_rma` where rma_id =  $rma_id";
        $wk_rma_collection = $this->connection->fetchAll($query);

        $query_rma_item = "SELECT * FROM `wk_rma_items` where rma_id =  $rma_id";
        $wk_rma_item_collection = $this->connection->fetchAll($query_rma_item);

        if(empty($wk_rma_collection)) { 
            $message="Rma with this id do not  exist in system";
            $this->_messageManager->addNotice($message);
            return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            die(); 
        }
        $wk_rma = $wk_rma_collection[0];
        if($wk_rma['status']=="2")
        {
            $message="Rma is already approved. Cannot Generate Again";
            $this->_messageManager->addError($message);
            return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        }


        if($rma_action=="acknowledge")
        {
            // these @ are  !important ,please  do not  remove @ritesh
            $rma_action_second=@$post["rma_action_second"];
            $rma_action_third=@$post["rma_action_third"];
            $rma_action_fourth = @$post['rma_action_fourth'];
            
            if($rma_action_second=='no_reverse_pickup')
            {
                /* accept  rma generate refund */
                $reverse_pickup="no";
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                /* generate creditmemo */
                $creditmemo_id = $this->creditnoteHelper->processReturn($rma_id,$post);
                // $refundstatus = $this->fullRefund($order_id);
                $this->customerSession->setRmaCreditMemoCreated("yes");
                //if(true)
                if($creditmemo_id)
                {
                    $message .="Credit  Memo  has  been created successfullly.";
                    $message .="Rma is approved.";
                    $this->setApproved($rma_id,$remark,$reverse_pickup);
                    $this->_messageManager->addNotice($message);
                    $userstatus="accept";
                    $this->informUser($wk_rma,$userstatus);

                    foreach($wk_rma_item_collection as $wk_item){
                        $key = "rma_action_fourth_".$wk_item['item_id'];
                        if(isset($post[$key])){
                            if($post[$key] == 'yes'){
                                // For Damaged Item
                                $item_type = 'refund';
                                $applicable_items[]=$wk_item['item_id'];
                            }
                        }
                    }
                    if(!empty($applicable_items))
                    {
                        $this->damagedItemHelper->saveDamagedItem($rma_id, $item_type,$applicable_items);
                    }
                } else
                {
                   $message .="Failed To Generate Credit Memo !";
                }
                $this->_messageManager->addNotice($message);
            }

            if($rma_action_second=='schedule_reverse_pickup')
            {
                /* accept  rma schedule reverse pickup */
                // $pickup_status = $this->creditnoteHelper->arrangeReversePickup($rma_id);
                $pickup_status=[];
                $pickup_status['status']='true';
                $pickup_status['task_message']="Reverse pickup Generated with awb id : 2342423";
                $message_status=$pickup_status['task_message'];
                $message.=$message_status;
                if($pickup_status['status']=='False')
                {
                    $this->_messageManager->addNotice($message_status);
                    return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                    die();
                } else 
                {
                    $status=10;
                    $final_status=0;
                    $userstatus="reverseschedule";
                    $this->setStatus($rma_id,$remark,$status,$final_status);
                    $this->_messageManager->addNotice($message);
                    $this->informUser($wk_rma,$userstatus);    
                }
                
                /* means reverse pickup in progress */
                
            }

            if($rma_action_third=='no_replacement')
            {
                /* accept  rma generate refund */
                $reverse_pickup="no";
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                /* generate creditmemo */
                $creditmemo_id = $this->creditnoteHelper->processReturn($rma_id,$post);
                
                if($creditmemo_id)
                {
                    $this->customerSession->setRmaCreditMemoCreated("yes");
                    $message .="Credit  Memo  has  been created successfullly.";
                    $message .="Rma is approved.";
                    $this->setApproved($rma_id,$remark,$reverse_pickup);
                    $userstatus="accept";
                    $this->informUser($wk_rma,$userstatus);
                    foreach($wk_rma_item_collection as $wk_item){
                        $key = "rma_action_fourth_".$wk_item['item_id'];
                        if(isset($post[$key])){
                            if($post[$key] == 'yes'){
                                // For Damaged Item
                                $item_type = 'refund';
                                $applicable_items[]=$wk_item['item_id'];
                            }
                        }
                    }
                    if(!empty($applicable_items))
                    {
                        $this->damagedItemHelper->saveDamagedItem($rma_id, $item_type,$applicable_items);
                    }

                } else
                {
                    $message .="Failed To Create Credit Memo";

                }
                $this->_messageManager->addNotice($message);
            }
            
            if($rma_action_third=='send_replacement')
            {
                /* accept  rma schedule exchange pickup */
                $reverse_pickup="yes";
                
                // $pickup_status = $this->creditnoteHelper->arrangeExchangePickup($rma_id);
                $pickup_status=[];
                $pickup_status['status']='true';
                $pickup_status['task_message']="Exchange Generated with awb id : 2342423";

                
                $message_status=$pickup_status['task_message'];
                $message.=$message_status;
               
                if($pickup_status['status']==false)
                {
                    $this->_messageManager->addNotice($message_status);
                    return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                    die();
                } else
                {
                	$final_status=12;
                 	$status=12;
                 	$userstatus="sellerexchangedone";
                	$this->setStatus($rma_id,$remark,$status,$final_status);
                    foreach($wk_rma_item_collection as $wk_item){
                        $key = "rma_action_fourth_".$wk_item['item_id'];
                        if(isset($post[$key])){
                            if($post[$key] == 'yes'){
                                // For Damaged Item
                                $item_type = 'exchange';
                                $applicable_items[]=$wk_item['item_id'];
                            }
                        }
                    }
                    if(!empty($applicable_items))
                    {
                        $this->damagedItemHelper->saveDamagedItem($rma_id, $item_type,$applicable_items);
                    }
                	$this->_messageManager->addNotice($message);
                	$this->informUser($wk_rma,$userstatus);
                }
                
                /* status 13  means  exhanged */
                
            } 
        }
        /* for decline */
        if($rma_action==="reject")
        {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            $query =  "SELECT * FROM `wk_rma` where rma_id =  $rma_id";
            $wk_rma_collection = $this->connection->fetchAll($query);
            if(empty($wk_rma_collection)) { echo "empty"; die(); }
            $wk_rma = $wk_rma_collection[0];
            $order_id = $wk_rma['order_id'];
            $this->declineRma($rma_id,$remark); 
            $this->informUserDecline($wk_rma);
            $message="Rma is declined .";
            $this->_messageManager->addNotice($message);
        }

        $this->insertAcknowledgeDate($rma_id);
        if(!empty($remark_for_customer))
        {
            $this->updateRemarkForCustomer($rma_id,$remark_for_customer);
        }

        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }
    public function updateRemarkForCustomer($rma_id,$remark_for_customer)
    { 
        $query="update wk_rma set remark_for_customer='$remark_for_customer' where rma_id='$rma_id'";
        $this->connection->query($query); 
    }

    public function insertAcknowledgeDate($rma_id)
    { 
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $date = $objDate->gmtDate();
        $query = "Select acknowledge_date from `wk_rma` where rma_id=$rma_id";
        $acknowledge_date = $this->connection->fetchOne($query);
 
        if(empty($acknowledge_date))
        {
            $query="Update `wk_rma` set acknowledge_date = '$date' where rma_id=$rma_id";
            $this->connection->query($query);
        }

        $query="Update `wk_rma` set status_modified_date = '$date' where rma_id=$rma_id";
        $this->connection->query($query);
    }
    

    public function setApproved($rma_id,$seller_remark=null,$reverse_pickup='no')
    {   
        $status = $reverse_pickup=='no'?'2':'10';
        $final_status = $reverse_pickup=='no'?'3':'0';
        $seller_remark = filter_var($seller_remark, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $date = $objDate->gmtDate();
        $query="Update `wk_rma` set status = $status, admin_status='6',seller_remark='$seller_remark',final_status='$final_status',approved_date='$date' where rma_id=$rma_id";
        $this->connection->query($query);
    }

    public function setStatus($rma_id,$seller_remark=null,$status,$final_status,$admin_status=6)
    {   
        $seller_remark = filter_var($seller_remark, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $query="Update `wk_rma` set status = $status, admin_status='$admin_status',seller_remark='$seller_remark',final_status='$final_status' where rma_id=$rma_id";
        $this->connection->query($query);
    }
    
    public function informUser($wk_rma,$rma_action){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer_id = $wk_rma['customer_id'];
        $increment_id = $wk_rma['increment_id'];
        $name = $wk_rma['name'];
        $query =  "SELECT `value` FROM `customer_entity_varchar` where attribute_id='178' and entity_id='$customer_id'";
        $mobile_number = $this->connection->fetchOne($query);
        
        if($rma_action=='accept')
        {
            $msg = "Dear $name, your return request for GetMySpares order $increment_id has been approved. Refund will be credited to your acount in 5 to 7 days";    
            $msg_for_panasonic_team = "<p>Dear Team</p>
            <p>Seller Has Accepted Refund For the Order no $increment_id for the customer named $name, Credit Memo has been generated in magento </p>
            <p>Please Process Refund for GetMySpares order $increment_id at your end </p>
            <p>Thank You</p>
            ";
            $to=['ombugtest123456@gmail.com','pankaj.kumar06@in.panasonic.com','subhankar.lg@gmail.com','sasmitanupurthapa@infinityeservices.com'];
            $from=$objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_custom2/email');;
            $subject="Refund Processed For Order $increment_id";
            $this->_mailHelper->sendEmailThroughCurl($to,$from,$msg_for_panasonic_team,$subject);
        }
        
        if($rma_action=='reverseschedule')  
        {
            $msg = "Dear $name, your return request for GetMySpares order $increment_id has been acknowledged. Seller Has scheduled Reverse pickup to collect the  item.";    
            
        }
        
        if($rma_action=='sellerexchangedone')
        {
            $customer_consignment_no = $wk_rma['customer_consignment_no'];
            $msg = " Dear $name, your return request for GetMySpares order $increment_id has been approved. Seller Has shipped your item for exchange.";    
            $msg_for_panasonic_team = "<p>Dear Team</p>
            <p>Seller Has Exchanged the Order $increment_id for the customer named $name </p>
            <p>System has generated a forward shipment with the awb no : $customer_consignment_no .</p>
            <p>Customer named $name has also been informed the same </p>
            <p>Thank You</p>
            ";
            $to=['ombugtest123456@gmail.com','pankaj.kumar06@in.panasonic.com','subhankar.lg@gmail.com','sasmitanupurthapa@infinityeservices.com'];
            $from=$objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_custom2/email');;
            $subject="Refund Processed For Order $increment_id";
            $this->_mailHelper->sendEmailThroughCurl($to,$from,$msg_for_panasonic_team,$subject);
        }




        $this->_smsHelper->send_custom_sms($mobile_number,$msg);
        $query1 =  "SELECT `email` FROM `customer_entity` where entity_id='$customer_id'";
        $email = $this->connection->fetchOne($query1);
        $to = $email;
        $message="<p> $msg </p>";
        $subject="Return Request Accepted";
        //$this->_mailHelper->send_email($to,$message,$subject,$email);
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/return_request_accepted');
        $templateId = $selectedTemplateId;
        $fromEmail = $email; 
        $fromName = 'GetMySpares'; 
        $toEmail = $email;
 
        try {
            // template variables pass here
            $templateVars = [
                'msg'=>$msg
            ];
 
            $storeId = $storeManager->getStore()->getId();
 
            $from = ['email' => $fromEmail, 'name' => $fromName];
            $this->_inlineTranslation->suspend();
 
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];
            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($toEmail)
                ->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            echo 'error'.$e->getMessage();
            //$this->logger->info($e->getMessage());
        }
    }

    public function fullRefund($order_id)
    {   
        $order = $this->order->load($order_id);
        $creditmemo = $this->creditmemoFactory->createByOrder($order);
        $staus = $this->creditmemoService->refund($creditmemo);
        return  $staus;  
    }


    public function ArrangeExchange($rma_id,$remark=null)
    {
        $seller_remark = filter_var($remark, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $date = $objDate->gmtDate();
        $query="Update `wk_rma` set  status = '3',seller_remark='$seller_remark',admin_status='5',final_status='2',declined_date='$date' where rma_id=$rma_id";
        $this->connection->query($query);
    }

    /* function for  decline  */


    public function declineRma($rma_id,$remark=null){
        $seller_remark = filter_var($remark, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $date = $objDate->gmtDate();
        $query="Update `wk_rma` set  status = '3',seller_remark='$seller_remark',admin_status='5',final_status='2',declined_date='$date' where rma_id=$rma_id";
        $this->connection->query($query);
    }
    
    public function informUserDecline($wk_rma){
        $customer_id = $wk_rma['customer_id'];
        $increment_id = $wk_rma['increment_id'];
        $name = $wk_rma['name'];
        $query =  "SELECT `value` FROM `customer_entity_varchar` where attribute_id='178' and entity_id='$customer_id'";
        $mobile_number = $this->connection->fetchOne($query);
        // $mobile_number="9555581418";
        // $msg = "Hello $name , Your return request for  the order number $increment_id has been declined , Please  check your profile for  sellers remark";
        $msg = "Dear $name, return request for GetMySpares order $increment_id has been declined. Check order details for remarks. For queries call 01244603167";
        
        $this->_smsHelper->send_custom_sms($mobile_number,$msg);
        $query1 =  "SELECT `email` FROM `customer_entity` where entity_id='$customer_id'";
        $email = $this->connection->fetchOne($query1);
        $to = $email;
        $message="<p> $msg </p>";
        $subject="Return Request Declined";
        //$this->_mailHelper->send_email($to,$message,$subject,$email);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/return_request_declined');
        $templateId = $selectedTemplateId;
        $fromEmail = $email; 
        $fromName = 'GetMySpares'; 
        $toEmail = $email;
 
        try {
            // template variables pass here
            $templateVars = [
                'msg'=>$msg,
                'name' =>$name,
                'incrementId' => $increment_id,
            ];
 
            $storeId = $storeManager->getStore()->getId();
 
            $from = ['email' => $fromEmail, 'name' => $fromName];
            $this->_inlineTranslation->suspend();
 
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];
            $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($toEmail)
                ->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            echo 'error'.$e->getMessage();
            //$this->logger->info($e->getMessage());
        }
    }



}

