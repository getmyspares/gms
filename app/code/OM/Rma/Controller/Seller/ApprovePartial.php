<?php
namespace OM\Rma\Controller\Seller;

class ApprovePartial extends \Magento\Framework\App\Action\Action 
{
    
    public function __construct(
	    \Magento\Backend\App\Action\Context $context,
        \OM\RMA\Helper\CreateCreditnote $CreateCreditnote 
	) {
        $this->createCreditnote = $CreateCreditnote;
	    parent::__construct($context);
	}

	public function execute()
	{
        $this->createCreditnote->processReturn();
    }
}