<?php
namespace OM\Rma\Controller\Seller;

class RvpStatus extends \Magento\Framework\App\Action\Action 
{
    
    public function __construct(
	    \Magento\Backend\App\Action\Context $context
	) {
	    parent::__construct($context);
	}

	public function execute()
	{
		$awb = '537974150';

        $configvalue = $this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        if($configvalue->getValue('carriers/ecomexpress/sanbox')==1) {
            $url = 'https://clbeta.ecomexpress.in/apiv2/manifest_awb/';
        }else {
        }
            $url = 'https://api.ecomexpress.in/apiv2/manifest_awb/';

        $params = array(
                    'username' => $configvalue->getValue('carriers/ecomexpress/username'),
                    'password' => $configvalue->getValue('carriers/ecomexpress/password'), 
                    'json_input' => json_encode($awb)
                );   
        
        $ch = curl_init();  
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
var_dump($output);
        return json_decode($output);
    }
}