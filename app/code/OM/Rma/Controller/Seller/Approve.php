<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rma\Controller\Seller;
use Magento\Framework\Controller\ResultFactory;

class Approve extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \OM\MobileOtp\Helper\SmsHelper $SmsHelper,
        \Panasonic\CustomUser\Helper\Data $mailHelper,
        \OM\Rma\Helper\CreateCreditnote $CreateCreditnote,       
        /* for  full  refund */
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory,
        \Magento\Sales\Model\Order\Invoice $invoice,
        \Magento\Sales\Model\Service\CreditmemoService $creditmemoService,
        
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_smsHelper = $SmsHelper;
        $this->_mailHelper = $mailHelper;
        $this->creditnoteHelper = $CreateCreditnote;
        
        $this->connection = $resource->getConnection();
        $this->logger = $logger;
        $this->_messageManager = $messageManager;
        $this->customerSession = $customerSession;
        
        /* for  full refund  */
        $this->order = $order;
        $this->creditmemoFactory = $creditmemoFactory;
        $this->creditmemoService = $creditmemoService;
        $this->invoice = $invoice; 

        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            $get = $this->getRequest()->getParams();
            $rma_id = $get['id'];
            $remark = $get['remark'];
            $needToReturnShippingAmount = $get['return_shipping']; 
            $query =  "SELECT * FROM `wk_rma` where rma_id =  $rma_id";
            $wk_rma_collection = $this->connection->fetchAll($query);
            if(empty($wk_rma_collection)) { 
                echo "Rma with this id do not  exist in system "; 
                die(); 
            }
            $wk_rma = $wk_rma_collection[0];

            if($wk_rma['status']=="2")
            {
                echo $message="Rma is already approved. Cannot Generate Again";
                $this->_messageManager->addError($message);
            }
            $this->creditnoteHelper->processReturn($rma_id,$needToReturnShippingAmount);
            $this->setApproved($rma_id,$remark);
            // $refundstatus = $this->fullRefund($order_id);

            $this->customerSession->setRmaCreditMemoCreated("yes");
            echo $message="Credit  Memo  has  been created successfullly. Rma is approved .";
            $this->_messageManager->addNotice($message);
            $this->informUser($wk_rma);
        } catch (\Exception $e) {
            echo "Rma not  generated , some problem occured";
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    public function setApproved($rma_id,$seller_remark=null)
    {   
        $seller_remark = filter_var($seller_remark, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $date = $objDate->gmtDate();
        $query="Update `wk_rma` set status = '2',admin_status='6',seller_remark='$seller_remark',final_status='3',approved_date='$date' where rma_id=$rma_id";
        $this->connection->query($query);
    }

    public function informUser($wk_rma){
        $customer_id = $wk_rma['customer_id'];
        $increment_id = $wk_rma['increment_id'];
        $name = $wk_rma['name'];
        $query =  "SELECT `value` FROM `customer_entity_varchar` where attribute_id='178' and entity_id='$customer_id'";
        $mobile_number = $this->connection->fetchOne($query);
        // $msg = "Hello $name , Your return request for  the order number $increment_id has been approved , Sorry for the inconvenience";
        $msg = " Dear $name, your return request for GetMySpares order $increment_id has been approved. We will schedule reverse pickup of the order shortly.";
        $this->_smsHelper->send_custom_sms($mobile_number,$msg);
        
        $query1 =  "SELECT `email` FROM `customer_entity` where entity_id='$customer_id'";
        $email = $this->connection->fetchOne($query1);
        $to = $email;
        $message="<p> $msg </p>";
        $subject="Return Request Accepted";
        $this->_mailHelper->send_email($to,$message,$subject,$email);
    }

    public function fullRefund($order_id)
    {   
        $order = $this->order->load($order_id);
        $creditmemo = $this->creditmemoFactory->createByOrder($order);
        $staus = $this->creditmemoService->refund($creditmemo);
        return  $staus;  
    }
}

