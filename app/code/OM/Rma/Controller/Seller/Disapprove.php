<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rma\Controller\Seller;
use Magento\Framework\Controller\ResultFactory;

class Disapprove extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \OM\MobileOtp\Helper\SmsHelper $SmsHelper,
        \Panasonic\CustomUser\Helper\Data $mailHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        
        
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->connection = $resource->getConnection();
        $this->_messageManager = $messageManager;
        $this->_smsHelper = $SmsHelper;
        $this->_mailHelper = $mailHelper;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            $get = $this->getRequest()->getParams();
            $remark = $get['remark'];
            echo $rma_id = $get['id'];
            $query =  "SELECT * FROM `wk_rma` where rma_id =  $rma_id";
            $wk_rma_collection = $this->connection->fetchAll($query);
            if(empty($wk_rma_collection)) { echo "empty"; die(); }
            $wk_rma = $wk_rma_collection[0];
            $order_id = $wk_rma['order_id'];
            $this->declineRma($rma_id,$remark); 
            $this->informUser($wk_rma);
            $message="Rma is declined .";
            $this->_messageManager->addNotice($message);
            // return $resultRedirect;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    public function declineRma($rma_id,$remark=null){
        $seller_remark = filter_var($remark, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $date = $objDate->gmtDate();
        $query="Update `wk_rma` set  status = '3',seller_remark='$seller_remark',admin_status='5',final_status='2',declined_date='$date' where rma_id=$rma_id";
        $this->connection->query($query);
    }
    
    public function informUser($wk_rma){
        $customer_id = $wk_rma['customer_id'];
        $increment_id = $wk_rma['increment_id'];
        $name = $wk_rma['name'];
        $query =  "SELECT `value` FROM `customer_entity_varchar` where attribute_id='178' and entity_id='$customer_id'";
        $mobile_number = $this->connection->fetchOne($query);
        // $mobile_number="9555581418";
        // $msg = "Hello $name , Your return request for  the order number $increment_id has been declined , Please  check your profile for  sellers remark";
        $msg = "Dear $name, return request for GetMySpares order $increment_id has been declined. Check order details for remarks. For queries call 01244603167";
        
        $this->_smsHelper->send_custom_sms($mobile_number,$msg);
        $query1 =  "SELECT `email` FROM `customer_entity` where entity_id='$customer_id'";
        $email = $this->connection->fetchOne($query1);
        $to = $email;
        $message="<p> $msg </p>";
        $subject="Return Request Declined";
        $this->_mailHelper->send_email($to,$message,$subject,$email);
    
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}

