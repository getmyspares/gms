<?php

namespace OM\Rma\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ResourceConnection;

class RmaReportHelper extends AbstractHelper

{
	protected $connection;
  public function __construct(
    \Webkul\Rmasystem\Helper\Data $rmaHelper,
    ResourceConnection $abcresourceConnection
    )
  {
    $this->connection = $abcresourceConnection->getConnection();
    $this->rmaHelper = $rmaHelper;
  }


  public function downloadRmaReport()
	{
		$query = "SELECT a.*,b.*,c.delivery_date as forward_awb_delivery_date FROM `wk_rma_items`  as a left join wk_rma  as b  on a.rma_id=b.rma_id left  join ecomexpress_awb_exchange as c  on a.rma_id=c.rma_id order by b.rma_id desc";

		$datas = $this->connection->fetchAll($query);
		$columns = $this->getReturnColumns();
		$csvHeader = array_values($columns);
		$fp = fopen('php://memory', 'w');
		$filename = 'Return_Exchange_Requests_Report-'.date('Y-m-d-H-i-s').'.csv';
		fputcsv( $fp, $csvHeader,",");
		if(empty($datas))
		{
			return false;
		}
		foreach ($datas as $data) {
			$insertArr=$this->generateReturnReportRow($data);
			fputcsv($fp, $insertArr, ",");
		}

		fseek($fp, 0);
		header('Content-Type: text/csv');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Disposition: attachment; filename="'.$filename.'";');
		fpassthru($fp);
		exit;   

	}

    public function downloadRmaReportBySellerId($seller_id)
	{

		$columns = $this->getReturnColumns();
		$csvHeader = array_values($columns);
		$fp = fopen('php://memory', 'w');
		$filename = 'Return_Exchange_Requests_Report-'.date('Y-m-d-H-i-s').'.csv';
		fputcsv( $fp, $csvHeader,",");

		 $sql = "SELECT * FROM `wk_rma`  as a left  join `marketplace_orders` as  b on a.`order_id`=b.`order_id` where b.`seller_id`='$seller_id' order by rma_id desc";



		 $result = $this->connection->fetchAll($sql);
         if(empty($result))
		{
			return false;
		}

         foreach ($result as $wk_rma_data)
         {
            $rma_id = $wk_rma_data['rma_id'];
            $query = "SELECT a.*,b.*,c.delivery_date as forward_awb_delivery_date FROM `wk_rma_items`  as a left join wk_rma  as b  on a.rma_id=b.rma_id  left  join ecomexpress_awb_exchange as c  on a.rma_id=c.rma_id  where a.rma_id=$rma_id";


    		    $datas = $this->connection->fetchAll($query);
            foreach ($datas as $data) {
			      $insertArr=$this->generateReturnReportRow($data);
			      fputcsv($fp, $insertArr, ",");
		    }
         }
				 
		fseek($fp, 0);
		header('Content-Type: text/csv');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Disposition: attachment; filename="'.$filename.'";');
		fpassthru($fp);
		exit;
	}

 	public function getReturnColumns()
	{
		$columns = array(
			'name'=>'Customer Name',
			'increment_id'=>'Order ID',
			'final_status'=>'Return Status',
			'resolution_type'=>'Resolution Type',
			'rma_reason'=>'Reason',
			'acknowledge_date'=>'Seller Acknowledgement Date',
			'seller_resolution'=>'Seller Resolution',
			'tracking_status'=>'Tracking Status',
			'tracking_status_date'=>'Tracking Status Date',
			'reverse_awb'=>'Reverse AWB Number',
			'forward_awb_number'=>'Forward AWB Number',
			'forward_awb_delivery_date'=>'Forward AWB DeliveryDate',
			'additional_info'=>'Additional info',
			'product_name'=>'Product  Name',
			'product_price'=>'Product Price',
			'product_qty'=>'Product  Qty',
			'grand_total'=>'Grand Total (excluding shipping)',
			'customer_mobile'=>'Customer Mobile',
			'created_at'=>'Created At',
			'product_seller'=>'Product Seller',
			'seller_remark'=>'Seller Remark',
			'approval_date'=>'Aprroved Date',
			'decline_date'=>'Declined Date',
		);
		return $columns;
	}

	public function generateReturnReportRow($data)
	{
		$insertArr = array();
		$Customer_Name = $data['name'];
		$Order_ID = $data['increment_id'];
		$status = $data['status'];
		$forward_awb_delivery_date = $data['forward_awb_delivery_date'];
		$Return_Status = $this->rmaHelper->getRmaStatusTitle($status);
		$Resolution_Type = ($data['resolution_type'] ==1 )? 'Exchange' : "Refund";
		$Reason = $data['rma_reason'];
		$Additional_info = $data['additional_info'];
		$Product_quantity = $data['qty'];
		$Product_Name = "";
		$Product_Price = "";
		$Product_Seller="";
		$grand_total="";


		$item_id = $data['item_id'];
		$query = "Select name,price,product_id  from  sales_order_item where item_id='$item_id'";
		$p_data = $this->connection->fetchAll($query);
		if(!empty($p_data) && is_array($p_data))
		{
			$Product_Name = $p_data[0]['name'];
			$Product_Price = $p_data[0]['price'];
			$product_id = $p_data[0]['product_id'];

			$grand_total =  $Product_quantity*$Product_Price;

			$query = "SELECT `seller_id` FROM `marketplace_product` where  mageproduct_id='$product_id'";
			$seller_id = $this->connection->fetchOne($query);

			$query = "SELECT shop_url FROM `marketplace_userdata` where seller_id='$seller_id'";
			$Product_Seller = $this->connection->fetchOne($query);
		}

		$customer_id = $data['customer_id'];
		$query = "Select `value` from customer_entity_varchar where  entity_id='$customer_id' and attribute_id='178'";
		$Customer_Mobile = $this->connection->fetchOne($query);

		$Created_At = $data['created_at'];

		$Seller_Remark = $data['seller_remark'];
		$Aprroved_Date = $data['approved_date'];
		$Declined_Date = $data['declined_date'];


		$acknowledge_date = $data['acknowledge_date'];
		$seller_resolution = $this->rmaHelper->getRemarkForCustomerById($data['remark_for_customer']);
		$tracking_status = $this->rmaHelper->getRmaStatusTitle($data['status']);
		$tracking_status_date = $data['status_modified_date'];

		$forward_awb_number = $data['customer_consignment_no'];
		$reverse_awb = $data['admin_consignment_no'];

		$insertArr[] = $Customer_Name;
		$insertArr[] = $Order_ID;
		$insertArr[] = $Return_Status;
		$insertArr[] = $Resolution_Type;
		$insertArr[] = $Reason;

		$insertArr[] = $acknowledge_date;
		$insertArr[] = $seller_resolution;
		$insertArr[] = $tracking_status;
		$insertArr[] = $tracking_status_date;
		$insertArr[] = $reverse_awb;
		$insertArr[] = $forward_awb_number;
		$insertArr[] = $forward_awb_delivery_date;

		$insertArr[] = $Additional_info;
		$insertArr[] = $Product_Name;
		$insertArr[] = $Product_Price;
		$insertArr[] = $Product_quantity;
		$insertArr[] = $grand_total;
		$insertArr[] = $Customer_Mobile;
		$insertArr[] = $Created_At;
		$insertArr[] = $Product_Seller;
		$insertArr[] = $Seller_Remark;
		$insertArr[] = $Aprroved_Date;
		$insertArr[] = $Declined_Date;

		return $insertArr;
	}

}
?>