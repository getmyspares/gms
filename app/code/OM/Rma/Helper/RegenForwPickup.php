<?php
    namespace OM\Rma\Helper;
    use Magento\Framework\App\ResourceConnection;
    use Webkul\Rmasystem\Api\AllRmaRepositoryInterface;

    Class RegenForwPickup  extends \Magento\Framework\App\Helper\AbstractHelper{

        protected $rmaCollectionFactory;
        protected $connection;
        protected $_rmaModel;
        protected $rmaRepository;
        public function __construct(
            AllRmaRepositoryInterface $rmaRepository,
            ResourceConnection $resourceConnection
        ) {
            $this->rmaRepository = $rmaRepository;
            $this->connection = $resourceConnection->getConnection();
        }

        public function flushCustomerConsignmentNo($rma_id){
            $rmaData = $this->rmaRepository->getById($rma_id);
            $rmaData->addData(['customer_consignment_no' => '', 'status'=>'13']);
            $this->rmaRepository->save($rmaData);
            $sql = "DELETE from `ecomexpress_awb_exchange` where rma_id='$rma_id'";
            $this->connection->query($sql);
            return true;
        }
    }
?>