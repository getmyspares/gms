<?php
namespace OM\Rma\Helper;

use Razorpay\Api\Api;
$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$path=$api_helpers->get_path();			
require_once $path.'/app/code/Razorpay/razorpay/Razorpay.php';

class OnlineRefund  extends \Magento\Framework\App\Helper\AbstractHelper
{

    public  $check_data;

    public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Panasonic\CustomUser\Helper\Data $PanasonicData,
		\Magento\Framework\App\ResourceConnection $resource
	) {
		$this->panasonicHelper = $PanasonicData;
		$this->connection = $resource->getConnection();
		$this->objectManager = $objectmanager;
    }
    
    public function fecthRefund($refund_id)
    {

        
    }

	public function razorpayOnlineRefund($id=null)
	{   
        $refund_response=array();
        if(empty($id))
        {
            $status = "Error";
            $message = "Empty Razorpay id provided";
            $return_data = array(
                "status"=>$status,
                "message"=>$message,
                "refund_data"=>$refund_response
            );
            return $return_data;
        } 
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$array=json_decode($api_helpers->get_razorpay_mode());
        
        $keyId=$array->keyId;
        $keySecret=$array->keySecret;

        // $keyId='rzp_test_zcJHsdMUmmTvfi';
        // $keySecret='DZCAKnzdRGt7GA7cleYg7Zdr';

        $api = new Api($keyId, $keySecret);   
        try{
            
            $refund_response_obj = $api->refund->create(array('payment_id' => $id));
            if(!empty($refund_response_obj))
            {
                $status="Success";
                $message="Online Refund Has been processed";
                $refund_response['id'] = $refund_response_obj->id;
                $refund_response['amount'] = $refund_response_obj->amount;
                $refund_response['speed_processed'] = $refund_response_obj->speed_processed;
                $refund_response['payment_id'] = $refund_response_obj->payment_id;
            }
        } catch(\Razorpay\Api\Errors\BadRequestError $e)
        {
            $status="Error";
            $message = $e->getMessage();
        }
        $return_data = array(
            "status"=>$status,
            "message"=>$message,
            "refund_data"=>$refund_response
        );
        return $return_data;
    }

    public function razorpayPartialrefundOnline($id,$refundAmount)
	{
        $refund_response=array();
        if(empty($id))
        {
            $status = "Error";
            $message = "Empty Razorpay id provided";
            $return_data = array(
                "status"=>$status,
                "message"=>$message,
                "refund_data"=>$refund_response
            );
            return $return_data;
        } 
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$array=json_decode($api_helpers->get_razorpay_mode());
        
        $keyId=$array->keyId;
        $keySecret=$array->keySecret;

        $api = new Api($keyId, $keySecret);   
        try{
            
            $refund_response_obj = $api->refund->create(array('payment_id' => $id, 'amount'=>$refundAmount)); // Creates partial refund for a payment
            if(!empty($refund_response_obj))
            {
                $status="Success";
                $message="Online Refund of $refundAmount Has been processed";
                $refund_response['id'] = $refund_response_obj->id;
                $refund_response['amount'] = $refund_response_obj->amount;
                $refund_response['speed_processed'] = $refund_response_obj->speed_processed;
                $refund_response['payment_id'] = $refund_response_obj->payment_id;
            }
        } catch(\Razorpay\Api\Errors\BadRequestError $e)
        {
            $status="Error";
            $message = $e->getMessage();
        }
        $return_data = array(
            "status"=>$status,
            "message"=>$message,
            "refund_data"=>$refund_response
        );
        return $return_data;
    }
    
    public function fetchRefund($refundId)
    {
        $refund = $api->refund->fetch($refundId); // Returns a particular refund
    }

    public function SaveRefundResponse($refund_response,$credit_memo_id=null)
    {
        $refund_id = $refund_response['id'];
        $refund_amount = $refund_response['amount'];
        $speed_processed = $refund_response['speed_processed'];
        $payment_id = $refund_response['payment_id'];
        $refund_amount = ($refund_amount/100);

        $query = "Insert into `om_online_refund` (razorpay_refund_id,razorpay_payment_id,refund_amount,speed_processed,credit_memo_id) values('$refund_id','$payment_id','$refund_amount','$speed_processed','$credit_memo_id')";
        $this->connection->query($query);

        $query = "Update `sales_creditmemo_grid` set online_refund_complete='Yes',refund_id='$refund_id' where entity_id='$credit_memo_id'";
        $this->connection->query($query);
    }

    public  function fetchUrn()
    {

        // $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        // $api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		// $array=json_decode($api_helpers->get_razorpay_mode());
        // $keyId=$array->keyId;
        // $keySecret=$array->keySecret;
        // echo $auth = "$keyId:$keySecret"; 
        
        $auth = "rzp_live_CSaIgeby8ykIRL:ZW12SxMbtRlGfVtUpRPOyFXd";
        $data = $this->connection->fetchAll("SELECT * FROM om_online_refund WHERE arn_urn IS NULL");
        // $data = $this->connection->fetchAll("SELECT * FROM om_online_refund WHERE razorpay_refund_id='rfnd_HbUQM92rqxSVw6'");
        foreach ($data as $_data) {
            $id = $_data['id'];
            $payId = $_data['razorpay_payment_id'];
            $refundId = $_data['razorpay_refund_id'];
            $credit_memo_id = $_data['credit_memo_id'];
            $endpoint = 'https://api.razorpay.com/v1/payments/'.$payId.'/refunds/'.$refundId;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $endpoint);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, $auth);
            $ce = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($ce,true);
            
            if(array_key_exists('acquirer_data', $result)) {
                $acquirer_data = $result['acquirer_data'];
                if(array_key_exists('arn', $acquirer_data)) {
                    $arn = $acquirer_data['arn'];
                    $quyer="UPDATE om_online_refund SET arn_urn = '$arn' WHERE id = $id";
                    $this->connection->query($quyer);
                    
                    $credit_memo_increment_id = $this->connection->fetchOne("SELECT increment_id FROM `sales_creditmemo` where  entity_id='$credit_memo_id'");
                    if(!empty($credit_memo_increment_id))
                    {
                        echo $update_sales_repoort = "UPDATE `om_sales_report` SET refund_rrn = '$arn' WHERE creditmemo_increment_id = '$credit_memo_increment_id' and report_type=7";
                        $this->connection->query($update_sales_repoort);
                    }


                }
                if(array_key_exists('rrn', $acquirer_data)) {
                    $arn = $acquirer_data['rrn'];
                    $quyer="UPDATE om_online_refund SET rrn = '$arn' WHERE id = $id";
                    $this->connection->query($quyer);
                    
                    $credit_memo_increment_id = $this->connection->fetchOne("SELECT increment_id FROM `sales_creditmemo` where  entity_id='$credit_memo_id'");
                    if(!empty($credit_memo_increment_id))
                    {
                        echo $update_sales_repoort = "UPDATE `om_sales_report` SET refund_rrn = '$arn' WHERE creditmemo_increment_id = '$credit_memo_increment_id' and report_type=7";
                        $this->connection->query($update_sales_repoort);
                    }
                }
                echo "<br/>";
            }
        }
    }
}















?>