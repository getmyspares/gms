<?php
namespace OM\Rma\Helper;

Class Data  extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function __construct(
		\Magento\Framework\App\ResourceConnection $resource
	) {
		$this->connection = $resource->getConnection();
	}

    public function getRmaStatusList()
    {
        $status_array=[
            '0'=>"Pending",
            '1'=>"Processing",
            '2'=>"Solved",
            '3'=>"Declined",
            '4'=>"Cancelled",
            '9'=>"Rma solved without refund",
            '10'=>"Reverse Pickup In Progress",
            '11'=>"Exchange Arranged With Reverse Pickup",
            '12'=>"Exchange In Progress",
            '13'=>"Reverse Pickup Complete",
            '14'=>"Solved (Exchange Completed)",
        ];
        return $status_array;
    }

    /**
     * Get RMA Status Title
     *
     * @param int $status
     * @param int $finalStatus
     *
     * @return string
     */
    public function getRmaStatusTitle($status, $finalStatus = 0)
    {
        
        if ($finalStatus == 0) {
            /* we only have to use this if block else block ois being phased out @ritesh7may2021 */
            $rma_status = $this->getRmaStatusList();
            
            if(isset($rma_status[$status]))
            {
                $rmaStatus = $rma_status[$status];
            } else
            {
                $rmaStatus="Pending";
            }
        } else {
            if ($finalStatus == 1) {
                $rmaStatus =  __("Cancelled");
            } elseif ($finalStatus == 2) {
                $rmaStatus =  __("Declined");
            } elseif ($finalStatus == 3 ) {
                $rmaStatus =  __("Solved");
            } elseif ($finalStatus == 10) {
                $rmaStatus =  __("Pickup In Progress");
            }  elseif ($status == 12) {
                $rmaStatus =  __("Exchange In Progress");
            }  elseif ($status == 14) {
                $rmaStatus =  __("Solved (Exchange Completed)");
            } else {
                $rmaStatus =  __("Pending");
            }
        }
        return $rmaStatus;
    }

    public function isReturnAvailable($order_inc_id)
    {
        $needtoshow = false; 
        $rma_valid=false;
        $allowedDays=7;
        $todaySecond = time();
        $allowedSeconds = $allowedDays * 86400;
        $pastSecondFromToday = $todaySecond - $allowedSeconds;
        $validFrom = date('Y-m-d H:i:s', $pastSecondFromToday);
        $order_del_date = $this->connection->fetchOne("select delivery_date from sales_order  where increment_id='$order_inc_id'");
        
        if($order_del_date > $validFrom)
        {
            $QIWER = "SELECT order_id FROM `wk_rma` where increment_id=$order_inc_id";
            $rma_order_id = $this->connection->fetchOne($QIWER);
            
            if(empty($rma_order_id))
            {
                $rma_valid=true;
            } else 
            {
                $QIWER = "SELECT count(item_id) FROM `wk_rma_items` where order_id=$rma_order_id";
                $rma_item_count = $this->connection->fetchOne($QIWER);
            
		        $itemCount = $this->connection->fetchOne("SELECT count(item_id) FROM `sales_order_item` where order_id = $rma_order_id AND parent_item_id IS NULL");
                if($rma_item_count < $itemCount)
                {
                    $rma_valid=true;
                }
            }

            if($rma_valid) {
                $sales_itemm_query = "SELECT product_id FROM `sales_order_item` where order_id=$order_inc_id";
                $sales_item_obj = $this->connection->fetchAll($sales_itemm_query);
                $needtoshow = true;
                if(!empty($sales_item_obj))
                {
                    foreach ($sales_item_obj as $product_data)
                    {
                        $product_id = $product_data['product_id'];
                        $return_period_type = "SELECT `value` FROM `catalog_product_entity_int` where attribute_id=179 and entity_id='$product_id' and store_id=0";
                        $return_period_type_id = $this->connection->fetchOne($return_period_type);
                        if($return_period_type_id==33)
                        {
                            $needtoshow = false;
                            break;
                        }
                    }

                }
            }
        } 
        return $needtoshow;       
    }




}

















?>