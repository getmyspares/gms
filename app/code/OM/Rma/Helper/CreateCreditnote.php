<?php

namespace OM\Rma\Helper;

use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;

class CreateCreditnote  extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(
	    CreditmemoSender $creditmemoSender,
		CreditmemoLoader $creditmemoLoader,
		ObjectManagerInterface $objectmanager,
		\Panasonic\CustomUser\Helper\Data $PanasonicData,
		\Magento\Framework\App\ResourceConnection $resource
	) {
	    $this->creditmemoSender = $creditmemoSender;
		$this->creditmemoLoader = $creditmemoLoader;
		$this->panasonicHelper = $PanasonicData;
		$this->connection = $resource->getConnection();
		$this->objectManager = $objectmanager;
	}

	public function processReturn($rma_id,$post)
	{
		$query =  "SELECT * FROM `wk_rma_items` where rma_id =  $rma_id";
		$wk_rma_collection = $this->connection->fetchAll($query);
		if(!empty($wk_rma_collection)) { 

			$creditMemoData = [];
			$creditMemoData['do_offline'] = 1;
			$creditMemoData['shipping_amount'] = 0;
			foreach ($wk_rma_collection as  $wk_rma_item) {
				$orderId = $wk_rma_item['order_id'];
				$qty = $wk_rma_item['qty'];
				$orderItemId = $wk_rma_item['item_id'];
				$return_shipuing_key ="return_shipping_$orderItemId"; 
				$itemToCredit[$orderItemId] = ['qty'=>$qty];
				$creditMemoData['items'] = $itemToCredit;
				if(isset($post[$return_shipuing_key]) && $post[$return_shipuing_key]=="yes")
				{
					$sql = "Select base_shipping_amount FROM sales_order where entity_id='$orderId'";
					$base_shipping_amount = $this->connection->fetchOne($sql);
					/* typecasting to int  as float  0.00 is  treated as not empty in php , it  will give logical error  whihc would be difficult  to debug
					do not  use $base_shipping_amount in calculation as it has rounded int value
					@ritesh5april2021
					*/
					$base_shipping_amount=(int)$base_shipping_amount;
					if(!empty($base_shipping_amount))
					{
						
						$creditMemoData['shipping_amount'] += $this->calculateShipping($orderId,$orderItemId,$qty);
					}
				}
			}
			$refund_type="partial";
			$returnItemCount=count($wk_rma_collection);
			$itemCount = $this->connection->fetchOne("SELECT count(item_id) FROM `sales_order_item` where order_id = $orderId AND parent_item_id IS NULL");
		  if($itemCount == $returnItemCount)
		  {
		  	$refund_type="full";
		  }
			$creditMemoData['adjustment_positive'] = 0;
			$creditMemoData['adjustment_negative'] = 0;
			$creditMemoData['comment_text'] = 'Credit Note Generated As Seller Accepted Rma';
			$creditMemoData['send_email'] = 1;
			$credit_memo_id = $this->partialRefund($orderId,$creditMemoData,$refund_type);
            if($credit_memo_id)
            {
                $query =  "update `wk_rma` set credit_memo_id='$credit_memo_id' where rma_id =  '$rma_id'";
		        $this->connection->query($query);
            }
            return $credit_memo_id;
        } else
		{
            return 0;
		}
	}

	public function calculateShipping($orderId,$orderItemId,$qty)
	{

		$sql = "Select postcode FROM sales_order_address where parent_id='$orderId' and address_type='shipping'";
		$pincode = $this->connection->fetchOne($sql);
		$sql = "Select product_id FROM sales_order_item where item_id='$orderItemId'";
		$product_id = $this->connection->fetchOne($sql);
		
		
		/* last minutes changes so  need object manager  @ritesh5april2021
		*/
		$ecom_helper = $this->objectManager->create('OM\ExpressShipping\Helper\Data');
		$order = $this->objectManager->create('Magento\Sales\Api\Data\OrderInterface');
		$orderobj = $order->load($orderId);
		return $ecom_helper->ecom_price_by_product($pincode,$product_id,$qty,$orderobj,"mobile");

	}

    public function partialRefund($orderId,$creditMemoData,$refund_type="partial")
	{
	    try {
	        $this->creditmemoLoader->setOrderId($orderId); //pass order id	
	        $this->creditmemoLoader->setCreditmemo($creditMemoData);
	        $creditmemo = $this->creditmemoLoader->load();
	        if ($creditmemo) {
	            if (!$creditmemo->isValidGrandTotal()) {
	                throw new \Magento\Framework\Exception\LocalizedException(
	                    __('The credit memo\'s total must be positive.')
									);
	            }

	            if (!empty($creditMemoData['comment_text'])) {
	                $creditmemo->addComment(
	                    $creditMemoData['comment_text'],
	                    isset($creditMemoData['comment_customer_notify']),
	                    isset($creditMemoData['is_visible_on_front'])
	                );

	                $creditmemo->setCustomerNote($creditMemoData['comment_text']);
	                $creditmemo->setCustomerNoteNotify(isset($creditMemoData['comment_customer_notify']));
	            }

	            $creditmemoManagement = $this->objectManager->create(
	                \Magento\Sales\Api\CreditmemoManagementInterface::class
	            );
	            $creditmemo->getOrder()->setCustomerNoteNotify(!empty($creditMemoData['send_email']));
	            $creditmemstatus = $creditmemoManagement->refund($creditmemo, (bool)$creditMemoData['do_offline']);
                if($refund_type="full")
                {
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $order = $objectManager->create('\Magento\Sales\Model\Order') ->load($orderId);
                    $order->setState('closed')->setStatus('closed');
                    $order->save();
                }
	            if (!empty($creditMemoData['send_email'])) {
	                $this->creditmemoSender->send($creditmemo);
	            }
                $creditmemid = $creditmemstatus->getEntityId();
                return $creditmemid;
            }
            return 0;
	    } catch (\Magento\Framework\Exception\LocalizedException $e) {
				echo $e->getMessage();
	    } catch (\Exception $e) {
				echo $e->getMessage();
        }   
	}

	public function arrangeReversePickup($rma_id)
	{
		$status = array('status'=>"false",'task_message'=>"Cannot Get fresh awb for reverse pickup",'awb'=>"");
		$sql = "SELECT order_id FROM `wk_rma` where rma_id='$rma_id'";
		$order_id = $this->connection->fetchOne($sql);
		$awb = $this->fetchFreshAwb();
		if(!empty($awb))
		{
			$result  = $this->awb_to_manifest_check($rma_id,$awb,$order_id,"REV");
			return $result;
		}
		return $status;
	}

	public function arrangeExchangePickup($rma_id)
	{
		$status = array('status'=>"false",'task_message'=>"Cannot Get fresh awb for exchange pickup",'awb'=>"");
		$sql = "SELECT order_id FROM `wk_rma` where rma_id='$rma_id'";
		$order_id = $this->connection->fetchOne($sql);
		$awb = $this->fetchExchangeAwb();
		if(!empty($awb))
		{
			$result  = $this->awb_to_manifest_exchange($rma_id,$awb,$order_id,"PPD");
			return $result;
		}
		return $status;
	}

	public function fetchExchangeAwb()
	{
		$sql = "SELECT awb FROM `ecomexpress_awb` where manifest is null and state=0 order by awb_id  asc";
		$awb = $this->connection->fetchOne($sql);
		return $awb; 
	}

	public function fetchFreshAwb()
	{
		$sql = "SELECT awb FROM `ecomexpress_awb_reverse` where manifest is null and state=0 order by awb_id  asc";
		$awb = $this->connection->fetchOne($sql);
		return $awb; 
	}

	public function awb_to_manifest_exchange($rma_id,$awb,$orderid,$awb_type)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order_id=$orderid;
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 
		$user_id = $order->getCustomerId();
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		/*
		echo "<pre>";
				print_r($orderArray);
		echo "</pre>";  
		*/
  
		$order_cr_date=$order->getCreatedAt();
		
		
		foreach ($order->getAllItems() as $item)
		{
			$item_ids=$item->getId();
			$rma_item_array  = $this->getRmaItemArray($rma_id);
			if(in_array($item_ids,$rma_item_array))
			{
				$product_id=$item->getProductId();
			$product_title=$item->getName();
			$product_qty=$item->getQtyOrdered();
			/* echo "<pre>";
				print_r($item->getData());
			echo "</pre>";   */
			
			$itemarray=$item->getData();
			 
			$row_total_incl_tax=$itemarray['row_total_incl_tax'];
			$weight=$itemarray['weight'];
			
			
			$product_length=$custom_helpers->get_product_length($product_id);
			$product_breath=$custom_helpers->get_product_breath($product_id);
			$product_height=$custom_helpers->get_product_height($product_id);
			
			$item_ids=$item->getId();
            $seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
			//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
			
			
			$sellerarray=$custom_helpers->get_seller_details($seller_id);
			
			//print_r($sellerarray);
			
			
			$seller_name=$sellerarray['seller_name'];
			$seller_comp_nam=$sellerarray['seller_comp_nam'];
			$seller_comp_address=$sellerarray['seller_comp_address'];
			$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
			$zipcode=$sellerarray['zipcode'];
			$seller_gst=$sellerarray['seller_gst'];
			
			
			$cgst_amount=$itemarray['cgst_amount'];
			$sgst_amount=$itemarray['sgst_amount'];
			$igst_amount=$itemarray['igst_amount'];
			
			$cgst_percent=$itemarray['cgst_percent'];
			$sgst_percent=$itemarray['sgst_percent'];
			$igst_percent=$itemarray['igst_percent'];
			
			$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
			
			
			$additional_info=array(
				'INVOICE_NUMBER'=>$invoice_id,
				'INVOICE_DATE'=>$order_cr_date,
				'SELLER_GSTIN'=>$seller_gst, 
				'GST_HSN'=>$order_cr_date,
				'GST_TAX_NAME'=>'DELHI GST',
				'GST_TAX_BASE'=>$total_gst,
				'DISCOUNT'=>0,
				'GST_TAX_RATE_CGSTN'=>$cgst_percent,
				'GST_TAX_RATE_SGSTN'=>$sgst_percent,
				'GST_TAX_RATE_IGSTN'=>$igst_percent,
				'GST_TAX_TOTAL'=>$total_gst, 
				'GST_TAX_CGSTN'=>$cgst_amount,
				'GST_TAX_SGSTN'=>$sgst_amount,
				'GST_TAX_IGSTN'=>$igst_amount
			);
			
			/* echo "<pre>";
				print_r($additional_info);
			echo "</pre>"; */
			
			$collect_amount=0;
			if($awb_type!='PPD')
			{
				$collect_amount=$row_total_incl_tax;
			}
				
			
			$awbarray[]=array( 
				'AWB_NUMBER'=>$awb,
				'ORDER_NUMBER'=>$order_inc_id,
				'PRODUCT'=>$awb_type,
				'CONSIGNEE'=>$delivery_name,
				'CONSIGNEE_ADDRESS1'=>$street_1,
				'CONSIGNEE_ADDRESS2'=>$street_2,
				'CONSIGNEE_ADDRESS3'=>$street_3,
				'DESTINATION_CITY'=>$delivery_city,
				'PINCODE'=>$delivery_pincode,
				'STATE'=>$delivery_state,
				'MOBILE'=>$delivery_telephone,
				'TELEPHONE'=>$delivery_telephone,
				'ITEM_DESCRIPTION'=>$product_title, 
				'PIECES'=>round($product_qty), 
				'COLLECTABLE_VALUE'=>$collect_amount,
				'DECLARED_VALUE'=>$row_total_incl_tax,
				'ACTUAL_WEIGHT'=>$weight,
				'VOLUMETRIC_WEIGHT'=>0,
				'LENGTH'=>$product_length,
				'BREADTH'=>$product_breath,
				'HEIGHT'=>$product_height,
				'PICKUP_NAME'=>$seller_name,
				'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
				'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
				'PICKUP_PINCODE'=>$zipcode,
				'PICKUP_PHONE'=>$seller_comp_mobile,
				'PICKUP_MOBILE'=>$seller_comp_mobile,
				'RETURN_NAME'=>$seller_name,
				'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
				'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
				'RETURN_PINCODE'=>$zipcode,
				'RETURN_PHONE'=>$seller_comp_mobile,
				'RETURN_MOBILE'=>$seller_comp_mobile,
				'ADDONSERVICE'=>[""],
				'DG_SHIPMENT'=>false,
				'ADDITIONAL_INFORMATION'=>$additional_info
			);
			}
			
		}
		
		
		if( $seller_id!=29)
		{

			$modearray=json_decode($api_helpers->get_razorpay_mode());
			//print_r($modearray); 
			$mode=$modearray->mode;
			
			$username=$modearray->username;
			$password=$modearray->password;
			$ecom_url=$modearray->url;


			$url=$ecom_url.'apiv2/manifest_awb/';    	 	
			
			$params = array(
						'username' => $username,
						  'password' => $password, 
						'json_input' => json_encode($awbarray)
					);   
			 
			$fields_string = http_build_query($params);
			$ch = curl_init();  
			 
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			$result=json_decode($output); 
			curl_close($ch); 

			if($result->shipments[0]->success==true )
			{
					$sql="update ecomexpress_awb set manifest='1',status='Assigned',state=1,shipment_to='Exchange Rmaid- $rma_id' where awb='".$awb."'";  
					$connection->query($sql);
					
					$sql="update wk_rma set customer_consignment_no='$awb' where rma_id='".$rma_id."'";  
					$connection->query($sql);

					$sql="insert into ecomexpress_awb_exchange set awb='$awb',manifest='1',status='Assigned',state=1,orderid='$orderid' , rma_id='$rma_id'";
					$connection->query($sql);

					$sql="update `om_sales_report` set exchange_awb='$awb' where order_id='$orderid'
					";
					$connection->query($sql);

					return array("status"=>true,"task_message"=>"Exchange Completed . Awb $awb Reserved For Shipment.",'awb'=>$awb);
			}  else 
			{
				$erorr = @$result->shipments[0]->reason;
				if($erorr=="AIRWAYBILL_IN_USE")
				{
					$usedawb = @$result->shipments[0]->awb;
					if(!empty($usedawb))
					{
						$blockmanifestedawb="update ecomexpress_awb set manifest='1' ,status='Assigned',state='1',shipment_to='Already Assigned' where awb='$usedawb'";
						$connection->query($blockmanifestedawb);
					}
				}
				return array("status"=>false,"task_message"=>$erorr,'awb'=>$awb);
			}
			return false;
		}

	}
/**
 * has been used to manifest reverse awb
 *
 * @param [type] $rma_id
 * @param [type] $awb
 * @param [type] $orderid
 * @param [type] $awb_type
 * @return void
 */
	public function awb_to_manifest_check($rma_id,$awb,$orderid,$awb_type)
	{
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$order_id=$orderid;
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 
		$user_id = $order->getCustomerId();
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		
  		$order_created_date_timestamp = strtotime($order->getCreatedAt());
		$order_cr_date=date('d-m-Y',$order_created_date_timestamp);

		foreach ($order->getAllItems() as $item)
		{
			$item_ids=$item->getId();
			$rma_item_array  = $this->getRmaItemArray($rma_id);
			if(in_array($item_ids,$rma_item_array))
			{
				
				$product_id=$item->getProductId();
				$product_title=$item->getName();
				$return_qty=$this->getRmaItemQty($rma_id,$item_ids);
				$itemarray=$item->getData();
				$total = ($return_qty * $itemarray['base_price']);
				$total_shipping_amount = $order->getData('base_shipping_amount'); 
				$row_total_incl_tax = $total+$total_shipping_amount;
				
				$weight=$itemarray['weight'];
				$product_length=$custom_helpers->get_product_length($product_id);
				$product_breath=$custom_helpers->get_product_breath($product_id);
				$product_height=$custom_helpers->get_product_height($product_id);
				
				$seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
				//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
				
				$sellerarray=$custom_helpers->get_seller_details($seller_id);
				
				$seller_obj = $objectManager->get('Magento\Customer\Model\Customer')->load($seller_id);
				$seller_city = $seller_obj->getCity(); 
				$seller_state =$seller_obj->getResource()->getAttribute('state')->getFrontend()->getValue($seller_obj);

				
				$seller_name=$sellerarray['seller_name'];
				$seller_comp_nam=$sellerarray['seller_comp_nam'];
				$seller_comp_address=$sellerarray['seller_comp_address'];
				$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
				$zipcode=$sellerarray['zipcode'];
				$seller_gst=$sellerarray['seller_gst'];
				
				$qty_ordered = $itemarray['qty_ordered'];

				$cgst_amount= ($itemarray['cgst_amount']/$qty_ordered)*$return_qty ;
				$sgst_amount= ($itemarray['sgst_amount']/$qty_ordered)*$return_qty ;
				$igst_amount= ($itemarray['igst_amount']/$qty_ordered)*$return_qty ;

				
				$cgst_percent=$itemarray['cgst_percent'];
				$sgst_percent=$itemarray['sgst_percent'];
				$igst_percent=$itemarray['igst_percent'];
				$hsn_code=$itemarray['hsn_code'];
				
				$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
				
				$additional_info=array(
					'INVOICE_NUMBER'=>$invoice_id,
					'INVOICE_DATE'=>$order_cr_date,
					'SELLER_GSTIN'=>$seller_gst, 
					'GST_HSN'=>$hsn_code,
					'GST_TAX_NAME'=>'DELHI GST',
					'GST_TAX_BASE'=>$total_gst,
					'DISCOUNT'=>0,
					'GST_TAX_RATE_CGSTN'=>$cgst_percent,
					'GST_TAX_RATE_SGSTN'=>$sgst_percent,
					'GST_TAX_RATE_IGSTN'=>$igst_percent,
					'GST_TAX_TOTAL'=>$total_gst, 
					'GST_TAX_CGSTN'=>$cgst_amount,
					'GST_TAX_SGSTN'=>$sgst_amount,
					'GST_TAX_IGSTN'=>$igst_amount
			);
			

			$collect_amount=0;
			if($awb_type!='PPD')
			{
				$collect_amount=$row_total_incl_tax;
			} 
			// $delivery_pincode="111111";
			$awbarray=array( 
				'AWB_NUMBER'=>$awb,
				'ORDER_NUMBER'=>$order_inc_id,
				'PRODUCT'=>$awb_type,
				"REVPICKUP_NAME"=> $delivery_name,
				"REVPICKUP_ADDRESS1"=> $street_1,
				"REVPICKUP_ADDRESS2"=> $street_2,
				"REVPICKUP_ADDRESS3"=> "",
				"REVPICKUP_CITY"=> $delivery_city,
				"REVPICKUP_PINCODE"=> $delivery_pincode,
				"REVPICKUP_STATE"=> $delivery_state,
				"REVPICKUP_MOBILE"=> $delivery_telephone,
				"REVPICKUP_TELEPHONE"=> $delivery_telephone,
	
				"PIECES"=> round($return_qty),
				"COLLECTABLE_VALUE"=> $collect_amount,
				"DECLARED_VALUE"=> $row_total_incl_tax,
				"ACTUAL_WEIGHT"=> $weight,
				"VOLUMETRIC_WEIGHT"=> 0,
				'LENGTH'=>$product_length,
				'BREADTH'=>$product_breath,
				'HEIGHT'=>$product_height,
				"VENDOR_ID"=> "",

				"DROP_NAME"=> $seller_name,
				"DROP_ADDRESS_LINE1"=> $seller_comp_nam,
				"DROP_ADDRESS_LINE2"=> $seller_comp_address,
				"DROP_PINCODE"=> $zipcode,
				"DROP_MOBILE"=> $seller_comp_mobile,
				"DROP_PHONE"=> $seller_comp_mobile,
				"ITEM_DESCRIPTION"=> $product_title,
				"EXTRA_INFORMATION"=> "test info",
				"DG_SHIPMENT "=> "False",
				'ADDITIONAL_INFORMATION'=>$additional_info
			);
			}
		}

		if($seller_id!=29)
		{
			
			$configvalue=$objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
			$username = $configvalue->getValue('carriers/ecomexpress/username');
			$password = $configvalue->getValue('carriers/ecomexpress/password');
			if($configvalue->getValue('carriers/ecomexpress/sanbox')==1){
				$url = 'https://clbeta.ecomexpress.in/apiv2/manifest_awb_rev_v2/';
			}
			else {
				$url = 'https://api.ecomexpress.in/apiv2/manifest_awb_rev_v2/';
			}
			$json_array['ECOMEXPRESS-OBJECTS']['SHIPMENT'][]=$awbarray;
			$json_input = json_encode($json_array);

			$curl = curl_init(); 
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => array('username' => $username,'password' => $password,'json_input' => $json_input),
			  CURLOPT_HTTPHEADER => array(
			    'Cookie: AWSALB=viDltCIh3BXOsPCmpsyZ6MeWmhA0PftLkHGc85MThuXtJ4wXAjj0mqRjdO0cCumLSVvkYDFCB/dBCpWZkTjPz8wM3mnFshdLwe0+UQN+JoNX0ob1jJTgIqW7omkj; AWSALBCORS=viDltCIh3BXOsPCmpsyZ6MeWmhA0PftLkHGc85MThuXtJ4wXAjj0mqRjdO0cCumLSVvkYDFCB/dBCpWZkTjPz8wM3mnFshdLwe0+UQN+JoNX0ob1jJTgIqW7omkj'
			  ),
			));
			$result = curl_exec($curl);
			curl_close($curl);
			
			$status="False";
			$task_message="Unable to generate reverse shipment from ecom";
			if(!empty($result))
			{
				$manifest_output = json_decode($result,1);
				
				if(isset($manifest_output['RESPONSE-OBJECTS']['AIRWAYBILL-OBJECTS']))
				{	
					$airbill=$manifest_output['RESPONSE-OBJECTS']['AIRWAYBILL-OBJECTS']['AIRWAYBILL'];
					$status = $airbill['success'];
					$airwaybill_number = $airbill['airwaybill_number'];
					if($status=='True')
					{
						$sql="update ecomexpress_awb_reverse set manifest='1',status='Assigned',state=1,orderid='$order_id',rma_id='$rma_id' where awb='$awb' ";
						$connection->query($sql);
					
						$sql="update wk_rma set admin_consignment_no='$awb' where order_id='".$order_id."'";
						$connection->query($sql);

						$sql="update `om_sales_report` set reverse_awb='$awb' where order_id='$order_id'
						";
						$connection->query($sql);

						
						$task_message="Reverse Pickup has been initiated, Rma will be processed after item delivered to you";
						$status="true";
					}
					if($status=='False')
					{
						$task_message = "AWB cannot be  generated because of erros " .$airbill['error_list']['reason_comment'];
						if(isset($airbill['error_list']['awb_used']))
						{
							$sql="update ecomexpress_awb_reverse set manifest='1',status='Already Used',state=1 where awb='$awb'";
							$connection->query($sql);
						}
					}
				}  else 
				{	

					$erorr = $manifest_output['shipments'][0]['reason'];
					$task_message .=" ". $erorr;
				}
			}
		} 
		return array('status'=>$status,'task_message'=>$task_message,'awb'=>$awb);	
	}

	public function manifest_forward_awb_with_error($rma_id,$awb,$orderid,$awb_type="PPD")
	{
		 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$state = $objectManager->get('Magento\Framework\App\State');
		//$state->setAreaCode('global');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		
		$order_id=$orderid;
		
		 
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
		$order_inc_id=$order->getIncrementId(); 


		  
		$user_id = $order->getCustomerId();

		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');

		$delivery_name=$delivery_firstname.' '.$delivery_lastname; 
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		$delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
		$delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
			
		$street=explode(PHP_EOL,$delivery_street);

		$street_1='';
		$street_2='';
		$street_3='';

		if(isset($street[0]))
		{
			$street_1=$street[0];
		}
		if(isset($street[1]))
		{
			$street_2=$street[1];
		}
		if(isset($street[2]))
		{
			$street_3=$street[2];
		}	


		$invoice_id=$helpers->get_order_invoice_id($order_inc_id); 

		$orderArray=$order->getData();
		/*
		echo "<pre>";
				print_r($orderArray);
		echo "</pre>";  
		*/
		$order_cr_date=$order->getCreatedAt();
		$rma_item_array  = $this->getRmaItemArray($rma_id);
		
		foreach ($order->getAllItems() as $item)
		{
				$item_ids=$item->getId();
				if(in_array($item_ids,$rma_item_array))
				{
				$product_id=$item->getProductId();
				$product_title=$item->getName();
				$product_qty=$item->getQtyOrdered();
				/* echo "<pre>";
					print_r($item->getData());
				echo "</pre>";   */
				
				$itemarray=$item->getData();
				
				$row_total_incl_tax=$itemarray['row_total_incl_tax'];
				$weight=$itemarray['weight'];
				
				
				$product_length=$custom_helpers->get_product_length($product_id);
				$product_breath=$custom_helpers->get_product_breath($product_id);
				$product_height=$custom_helpers->get_product_height($product_id);
				
				$item_ids=$item->getId();
							$seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
				//$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
				
				
				$sellerarray=$custom_helpers->get_seller_details($seller_id);
				
				//print_r($sellerarray);
				
				
				$seller_name=$sellerarray['seller_name'];
				$seller_comp_nam=$sellerarray['seller_comp_nam'];
				$seller_comp_address=$sellerarray['seller_comp_address'];
				$seller_comp_mobile=$sellerarray['seller_comp_mobile'];
				$zipcode=$sellerarray['zipcode'];
				$seller_gst=$sellerarray['seller_gst'];
				
				
				$cgst_amount=$itemarray['cgst_amount'];
				$sgst_amount=$itemarray['sgst_amount'];
				$igst_amount=$itemarray['igst_amount'];
				
				$cgst_percent=$itemarray['cgst_percent'];
				$sgst_percent=$itemarray['sgst_percent'];
				$igst_percent=$itemarray['igst_percent'];
				
				$total_gst=$cgst_amount+$sgst_amount+$igst_amount;
				
				
				$additional_info[]=array(
					'INVOICE_NUMBER'=>$invoice_id,
					'INVOICE_DATE'=>$order_cr_date,
					'SELLER_GSTIN'=>$seller_gst, 
					'GST_HSN'=>$order_cr_date,
					'GST_TAX_NAME'=>'DELHI GST',
					'GST_TAX_BASE'=>$total_gst,
					'DISCOUNT'=>0,
					'GST_TAX_RATE_CGSTN'=>$cgst_percent,
					'GST_TAX_RATE_SGSTN'=>$sgst_percent,
					'GST_TAX_RATE_IGSTN'=>$igst_percent,
					'GST_TAX_TOTAL'=>$total_gst, 
					'GST_TAX_CGSTN'=>$cgst_amount,
					'GST_TAX_SGSTN'=>$sgst_amount,
					'GST_TAX_IGSTN'=>$igst_amount
				);
				
				/* echo "<pre>";
					print_r($additional_info);
				echo "</pre>"; */
				
				$collect_amount=0;
				if($awb_type!='PPD')
				{
					$collect_amount=$row_total_incl_tax;
				}
					
				
				$awbarray[]=array( 
					'AWB_NUMBER'=>$awb,
					'ORDER_NUMBER'=>$order_inc_id,
					'PRODUCT'=>$awb_type,
					'CONSIGNEE'=>$delivery_name,
					'CONSIGNEE_ADDRESS1'=>$street_1,
					'CONSIGNEE_ADDRESS2'=>$street_2,
					'CONSIGNEE_ADDRESS3'=>$street_3,
					'DESTINATION_CITY'=>$delivery_city,
					'PINCODE'=>$delivery_pincode,
					'STATE'=>$delivery_state,
					'MOBILE'=>$delivery_telephone,
					'TELEPHONE'=>$delivery_telephone,
					'ITEM_DESCRIPTION'=>$product_title, 
					'PIECES'=>round($product_qty), 
					'COLLECTABLE_VALUE'=>$collect_amount,
					'DECLARED_VALUE'=>$row_total_incl_tax,
					'ACTUAL_WEIGHT'=>$weight,
					'VOLUMETRIC_WEIGHT'=>0,
					'LENGTH'=>$product_length,
					'BREADTH'=>$product_breath,
					'HEIGHT'=>$product_height,
					'PICKUP_NAME'=>$seller_name,
					'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
					'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
					'PICKUP_PINCODE'=>$zipcode,
					'PICKUP_PHONE'=>$seller_comp_mobile,
					'PICKUP_MOBILE'=>$seller_comp_mobile,
					'RETURN_NAME'=>$seller_name,
					'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
					'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
					'RETURN_PINCODE'=>$zipcode,
					'RETURN_PHONE'=>$seller_comp_mobile,
					'RETURN_MOBILE'=>$seller_comp_mobile,
					'ADDONSERVICE'=>[""],
					'DG_SHIPMENT'=>false,
					'ADDITIONAL_INFORMATION'=>$additional_info
				); 
			}
		}	
			
			
			if( $seller_id!=29)
			{
				/* echo "<pre>";
					print_r($awbarray); 
				echo "</pre>";   */  

				
				$modearray=json_decode($api_helpers->get_razorpay_mode());
				//print_r($modearray); 
				$mode=$modearray->mode;
				$username=$modearray->username;
				$password=$modearray->password;
				$ecom_url=$modearray->url;
		
				$url=$ecom_url.'apiv2/manifest_awb/';    	 	
					
							
				
				$params = array(
							'username' => $username,
								'password' => $password, 
							'json_input' => json_encode($awbarray)
						);   
				
				
				/*
				$params = array(
							'username' => "panasonicpvtltd402639_pro",
								'password' => '6bk2LrZpC7UXhuZe', 
							'json_input' => json_encode($awbarray)
						);    	
				
				if($order_id=="92"){
					echo "<pre>";
					var_dump($awbarray);
					
				}*/	
		
				$fields_string = http_build_query($params);

				//open connection
				$ch = curl_init();  
				
				
				//set the url, number of POST vars, POST data
				curl_setopt($ch,CURLOPT_URL, $url);
				curl_setopt($ch,CURLOPT_POST, 1);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = curl_exec($ch);
				$result=json_decode($output);  
				curl_close($ch); 
				
				if($result->shipments[0]->success==true )
				{
					if(!empty($awb))
					{
						$sql="update ecomexpress_awb set manifest='1' where awb='".$awb."'";  
						$connection->query($sql);
					
						$sql="update wk_rma set admin_consignment_no='$awb' where order_id='".$order_id."'";
						$connection->query($sql);


					}
					return array("status"=>true,"task_message"=>"success");
				}  else 
				{
					$erorr = @$result->shipments[0]->reason;
					if($erorr=="AIRWAYBILL_IN_USE")
					{
						$usedawb = @$result->shipments[0]->awb;
						if(!empty($usedawb))
						{
							$blockmanifestedawb="update ecomexpress_awb set manifest='1' ,status='Assigned',state='1',shipment_to='Already Assigned' where awb='$usedawb'";
							$connection->query($blockmanifestedawb);
						}
					}
					return array('status'=>false,'task_message'=>$erorr,'awb'=>"");
				}
			}
		
	}

	public function getRmaItemArray($rma_id)
	{
		$rma_item_array=array();
		$query = "SELECT item_id FROM `wk_rma_items` where rma_id = '$rma_id'"; 
 		$wk_rma_items = $this->connection->fetchAll($query);
		foreach ($wk_rma_items as $value) {
			$rma_item_array[]=$value['item_id'];
		}
		return $rma_item_array;
	}

	public function getRmaItemQty($rma_id,$item_id)
	{
		$query = "SELECT qty FROM `wk_rma_items` where rma_id = '$rma_id' and item_id='$item_id'"; 
 		$qty = $this->connection->fetchOne($query);
		return $qty;
	}
}















?>