<?php
    namespace OM\Rma\Helper;
    use \Magento\Framework\App\ResourceConnection;
    use Webkul\Rmasystem\Api\AllRmaRepositoryInterface;

    Class RegenrevPickup  extends \Magento\Framework\App\Helper\AbstractHelper{

        protected $connection;
        protected $rmaRepository;

        public function __construct(
            AllRmaRepositoryInterface $rmaRepository,
            ResourceConnection $resourceConnection
        ) {
            $this->rmaRepository = $rmaRepository;
            $this->connection = $resourceConnection->getConnection();
        }

        public function flushAdminConsignmentNo($rma_id){
            $rma_data = $this->rmaRepository->getById($rma_id);
            $rma_data->addData(['admin_consignment_no' => '','status' => '1','final_status' => '0']);
            $this->rmaRepository->save($rma_data);
            $sql = "DELETE from `ecomexpress_awb_reverse` where rma_id='$rma_id'";
            $this->connection->query($sql);
            return true;
        }
    }
?>