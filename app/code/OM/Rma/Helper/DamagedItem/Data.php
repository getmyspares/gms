<?php
namespace OM\Rma\Helper\DamagedItem;

Class Data  extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_PATH_FOR_CONDITION_NUMBER = 'dmgpolicy/condition/condition_number';
    const XML_PATH_FOR_ADMIN_NAME       = 'dmgpolicy/admin/admin_name';
    const XML_PATH_FOR_ADMIN_EMAIL      = 'dmgpolicy/admin/admin_email';
    const XML_PATH_FOR_POLICY_NO        = 'dmgpolicy/desktop/dmgpolicy_no';
    const XML_PATH_FOR_INSURENCE_NAME   = 'dmgpolicy/insurence/insurence_name';
    const XML_PATH_FOR_INSURENCE_EMAIL  = 'dmgpolicy/insurence/insurence_email';
    const XML_PATH_FOR_LOGISTIC_NAME    = 'dmgpolicy/logistic/logistic_name';
    const XML_PATH_FOR_LOGISTIC_EMAIL   = 'dmgpolicy/logistic/logistic_email';

    public function __construct(
        \OM\GenerateReport\Helper\Mail $mailHelper,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Panasonic\CustomUser\Helper\Data $panasonicHelper,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploader,
        \Webkul\Rmasystem\Helper\Data $webkulHelper,


        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Model\OrderRepository $orderRepo,
        \Webkul\Rmasystem\Api\AllRmaRepositoryInterface $rmaRepository,
		\Magento\Framework\App\ResourceConnection $resource,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerModel
	) {
		$this->panasonicHelper=$panasonicHelper;
        $this->mailHelper = $mailHelper;
        $this->_transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->filesystem = $filesystem;
        $this->fileUploader = $fileUploader;
        $this->webkulHelper = $webkulHelper;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);

        $this->orderItemRepository = $orderItemRepository;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->customerFactory = $customerFactory;
        $this->orderRepo = $orderRepo;
        $this->rmaRepository = $rmaRepository;
		$this->connection = $resource->getConnection();
        $this->damagedFactory = $damagedFactory;
        $this->customerModel = $customerModel;
        $this->_scopeConfig = $scopeConfig;
	}

    private $applicableItems;

    /**
     * send mail to logistic and insurance partner ands also save data in the damaged item table
     *
     * @param string $rma_id
     * @param string $item_type
     * @return void
     */
    public function saveDamagedItem($rma_id,$item_type,$applicable_items){
        $this->setApplicableItems($applicable_items);

        if(!empty($rma_id)){
            // @pankaj use model for wk_rma 
            $table = $this->connection->getTableName('wk_rma');
            $query = "SELECT * FROM `" . $table . "` WHERE rma_id = $rma_id ";
            $result = $this->connection->fetchRow($query);
            
            // $applicable_items = $this->getApplicableItemIds($rma_id);
            
            if(is_array($applicable_items) && !empty($applicable_items)){
                $item_ids_string = implode(',',$applicable_items); 
                $customerData = $this->customerModel->load($result['customer_id']);
                $damagedItemModel = $this->damagedFactory->create();
                $damagedItemModel->setRmaId($rma_id);
                $damagedItemModel->setOrderId($result['order_id']);
                $damagedItemModel->setOrderItemId($item_ids_string);
                $damagedItemModel->setCustomerId($result['customer_id']);
                $damagedItemModel->setCustomerEmail($customerData->getEmail());
                $damagedItemModel->setCustomerMobile($customerData->getMobile());
                $damagedItemModel->setIncrementId($result['increment_id']);
                $damagedItemModel->setResolutionType($item_type);
                $damagedItemModel->setAdditionalInfo($result['additional_info']);
                $damagedItemModel->setCustomerConsignmentNo($result['customer_consignment_no']);
                $damagedItemModel->setImage($result['image']);
                $damagedItemModel->setRefundStatus($result['status']);
                $damagedItemModel->setSellerRemark($result['seller_remark']);
                $damagedItemModel->setCreditMemoId($result['credit_memo_id']);
                $damagedItemModel->setFinalStatus($result['final_status']);
                $damagedItemModel->setAdminStatus($result['admin_status']);
                $damagedItemModel->setApprovedDate($result['approved_date']);
                $damagedItemModel->setClaimStatus('0');
                $damagedItemModel->save();
                $this->sendMailToLogisticPartner($rma_id);
                $this->sendMailToSellerAndAdmin($rma_id);
            }
        }
    }

    public function getApplicableItemIds($rma_id,$used_database_value=true)
    {
        if($used_database_value)
        {
            $collection = $this->damagedFactory->create();
            $singleRow = $collection->getCollection()->addFieldToFilter('rma_id', $rma_id)->getFirstItem();
            $order_item_string = $singleRow->getOrderItemId();
            return explode(",",$order_item_string);
        } else if(isset($this->applicableItems) && !empty($this->applicableItems))
        {
            return $this->applicableItems;
        } else 
        {
/*             $table_wk_rma_items = $this->connection->getTableName('wk_rma_items');
            $query_wk_rma_items = "SELECT * FROM `" . $table_wk_rma_items . "` WHERE rma_id = $rma_id ";
            $result_wk_rma_items = $this->connection->fetchAll($query_wk_rma_items);
            $item_ids=[];
            foreach($result_wk_rma_items as $wk_items) {
                $reason_id =  $wk_items['reason_id'];
                if($this->isApplicableForInsurance($reason_id)){
                    $item_ids = $wk_items['item_id'];
                }
            }
            return $item_ids; */
        }
        
    }


    public function setApplicableItems($applicableItems)
    {
        $this->applicableItems = $applicableItems;
    }


    public function setClaimStatus($rma_id, $status){        
        $collection = $this->damagedFactory->create();
        $singleRow = $collection->getCollection()->addFieldToFilter('rma_id', $rma_id)->getFirstItem();
        $updStatus = $collection->load($singleRow->getId());
        $updStatus->setClaimStatus($status);
        $updStatus->save();

        return true;
    }

    public function getClaimStatusTitle($rma_id){
        $collection = $this->damagedFactory->create();
        $singleRow = $collection->getCollection()->addFieldToFilter('rma_id', $rma_id)->getFirstItem();
        $statusNumber = $singleRow->getClaimStatus();
        
        $status_array = [
            '0' => 'Pending',
            '1' => 'Claim Initiated',
            '2' => 'Pending with insurance partner',
            '3' => 'Pending with logistic partner',
            '4' => 'Claim Completed'
        ];

        if(array_key_exists($statusNumber, $status_array)){
            return $status_array[$statusNumber];
        }else{
            return $statusNumber;
        }
    }

    public function getConditionNumber(){
        return $this->_scopeConfig->getValue(self::XML_PATH_FOR_CONDITION_NUMBER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function sendMailToSellerAndAdmin($rma_id){

        $rmaData = $this->rmaRepository->getById($rma_id);
        $order_id = $rmaData->getOrderId();
        // @pankaj use model for marketplace_orders
        $table_seller = $this->connection->getTableName('marketplace_orders');
        $query_seller = "SELECT `seller_id` FROM `" . $table_seller . "` WHERE order_id =  $order_id";
        $result_seller = $this->connection->fetchRow($query_seller);
        $sellerData = $this->customerFactory->create()->load($result_seller['seller_id']);

        $seller_name = $sellerData->getFirstname().' '.$sellerData->getMiddlename().' '.$sellerData->getLastname();
        $seller_email = $sellerData->getEmail();

        $receiverInfo_seller = [
            'name' => $seller_name,
            'email' => $seller_email,
        ];
        $order_inc_id = $rmaData->getIncrementId();
        $templateVars_seller = [
            'greeting_name' => $seller_name,
            'subject'       => 'Damaged Item Claim Alert',
            'msg'           =>  "Your order - $order_inc_id claim has been initiated."
        ];

        $admin_name = $this->_scopeConfig->getValue(self::XML_PATH_FOR_ADMIN_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $admin_email = $this->_scopeConfig->getValue(self::XML_PATH_FOR_ADMIN_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $receiverInfo_admin = [
            'name' => $admin_name,
            'email' => $admin_email,
        ];
        $rma_id = $rmaData->getRmaId();
        $templateVars_admin = [
            'greeting_name' => $admin_name,
            'subject'   => 'Damaged Item Claim Alert',
            'msg'       =>  "Damaged item [RMA ID - $rma_id, ORDER ID - $order_inc_id] claim has been initiated."
        ];

        $store = $this->storeManager->getStore();

        try{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            // @pankaj use class in constructor instead from object manager 
            $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
            $transport_admin = $transportBuilder->setTemplateIdentifier('admin_alert_for_damageditem')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                ->addTo($receiverInfo_admin['email'], $receiverInfo_admin['name'])
                ->setTemplateVars($templateVars_admin)
                ->setFrom('general')
                ->getTransport();
            $transport_admin->sendMessage();

            /* $transport_seller = $transportBuilder->setTemplateIdentifier('admin_alert_for_damageditem')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                ->addTo($receiverInfo_seller['email'], $receiverInfo_seller['name'])
                ->setTemplateVars($templateVars_seller)
                ->setFrom('general')
                ->getTransport();
            $transport_seller->sendMessage(); */            

            $data = array(
                'code'  => 'success',
                'msg'   => 'Claim initiated successfully',
            );

        } catch(\Exception $e){
            $data = array(
                'code'  => 'error',
                'msg'   => $e->getMessage(),
            );

        }

        //echo json_encode($data);
    }

    public function sendMailToLogisticPartner($rma_id){
        $invoice_no="";
        $invoice_date="";
        $applicable_for_insurance=false;
        $rmaData = $this->rmaRepository->getById($rma_id);
        $damagedData = $this->damagedFactory->create();
        $collection = $damagedData->getCollection()->addFieldToFilter('rma_id',$rmaData->getRmaId());
        $updDamagedData = $damagedData->load($collection->getFirstItem()->getId());
        $order_id = $rmaData->getOrderId();
        $orderData = $this->orderRepo->get($order_id);
        if ($orderData->hasInvoices()){
            foreach($orderData->getInvoiceCollection() as $invoice){
                $invoice_no = $invoice->getIncrementId();
                $invoice_date = date('d-m-Y',strtotime($invoice->getCreatedAt()));
            }
        }
        
        $table_awb = $this->connection->getTableName('ecomexpress_awb');
        $query_awb = "SELECT `awb` FROM `" . $table_awb . "` WHERE orderid =  $order_id";
        $result_awb = $this->connection->fetchRow($query_awb);

        // @pankaj use model 
        $table_seller = $this->connection->getTableName('marketplace_orders');
        $query_seller = "SELECT `seller_id` FROM `" . $table_seller . "` WHERE order_id =  $order_id";
        $result_seller = $this->connection->fetchRow($query_seller);
        $sellerData = $this->customerFactory->create()->load($result_seller['seller_id']);

        if(!empty($rmaData->getCreditMemoId())){
            $creditMemoData = $this->creditmemoRepository->get($rmaData->getCreditMemoId());
        }else{
            $creditMemoData = '';
        }

        $store = $this->storeManager->getStore();

        $folderName = $this->webkulHelper->getBaseDirRead().$rmaData->getRmaId().'/image/';
        $images = glob($folderName.'*.{jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG,bmp,BMP}', GLOB_BRACE);
        $additionalImages = [];
        foreach ($images as $filename) {
            $getfilename = explode("/",$filename);
            $justfname = $this->webkulHelper->getBaseUrl().$rmaData->getRmaId().'/image'."/".end($getfilename);
            $additionalImages[] = "<a href=".$justfname." download><img class='wk_rma_add_images' alt='".end($getfilename)."' height='50px' width='100px' src='".$justfname."'/></a>";
        }

        $certificateName = $certificatePath = $loss_estimate = $policy_number = $url = '';
        $logistic_name  = $this->_scopeConfig->getValue(self::XML_PATH_FOR_LOGISTIC_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $logistic_email = $this->_scopeConfig->getValue(self::XML_PATH_FOR_LOGISTIC_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $condition_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_CONDITION_NUMBER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $send_rma_id = base64_encode($rma_id);
        $send_id = base64_encode('rma_id');
        $s_item_id = base64_encode('item_id');
        //$send_item_id = base64_encode($item_id);
        
        /* case of refund */
        if(!empty($creditMemoData)) {
            if($this->getBrokenItemAmount($rma_id)>$condition_number){
                $applicable_for_insurance=true;

                //Push to insurance Co.
                $updDamagedData->setClaimStatus('2');
                $updDamagedData->save();
                $loss_estimate = $this->getBrokenItemAmount($rma_id);
                $policy_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_POLICY_NO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $url = '<a href="'.$this->storeManager->getStore()->getBaseUrl().'rms/damageditem/uploadcertificate?'.$send_id.'='.$send_rma_id.'" > Click Here</a>&nbsp; To Upload Damaged Certificate';
            }else{
                //Push to logistics
                $updDamagedData->setClaimStatus('3');
                $updDamagedData->save();
                $loss_estimate = $this->getBrokenItemAmount($rma_id);
                $policy_number = $orderData->getIncrementId();
            }
        }
        /* case of exchange */ 
        else {
            if(!empty($this->getBrokenItemAmount($rma_id))){
                if($this->getBrokenItemAmount($rma_id)>$condition_number){
                    $applicable_for_insurance=true;
                    //Push to insurance Co.
                    $updDamagedData->setClaimStatus('2');
                    $loss_estimate = $this->getBrokenItemAmount($rma_id);
                    $policy_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_POLICY_NO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $url = '<a href="'.$this->storeManager->getStore()->getBaseUrl().'rms/damageditem/uploadcertificate?'.$send_id.'='.$send_rma_id.'" > Click Here</a>&nbsp; To Upload Damaged Certificate';
                }else{
                    //Push to logistics
                    $updDamagedData->setClaimStatus('3');
                    $loss_estimate = $this->getBrokenItemAmount($rma_id);
                    $policy_number = $orderData->getIncrementId();
                    $updDamagedData->save();
                }
            }
        }

        $receiverInfo = [
            'name' => $logistic_name,
            'email' => $logistic_email,
        ];

        $part_number=[];
        $part_name=[];
        $part_quantity=[];

        foreach ($this->getApplicableItemIds($rma_id) as $item_id) {
            $order_item = $this->orderItemRepository->get($item_id);
            $part_number[]=$order_item->getSku();
            $part_name[]=$order_item->getName();
            $part_quantity[]=(int)$order_item->getQtyOrdered();
        }

        $part_number_string = implode(',',$part_number);
        $part_name_string = implode(',',$part_name);
        $part_quantity_string = implode(',',$part_quantity);

        $templateVars = [
            'greetingName'              => $receiverInfo['name'],
            'subject'                   => 'Broken Item Logistic Partner',
            'policy_no'                 => $policy_number,
            'policy_copy'               => 'policy_copy',
            'rma_id'                    => $rma_id,
            'part_number'               => $part_number_string,
            'part_name'                 => $part_name_string, 
            'quantity'                  => $part_quantity_string,
            'invoice_no'                => $invoice_no,
            'invoice_date'              => $invoice_date,
            'awb_no'                    => $result_awb['awb'],
            'loss_estimate'             => number_format((float)$loss_estimate,2,'.',','),
            'rsrv_amt_crm'              => '',
            'cust_return_date'          => date('d/m/Y'),
            'seller_address'            => $sellerData->getCompanyAdd(),
            'coverage_details'          => '',
            'seller_firstname_name'     => $sellerData->getFirstname(),
            'seller_middlename_name'    => $sellerData->getMiddlename(),
            'seller_lastname_name'      => $sellerData->getLastname(),
            'seller_mobile'             => $sellerData->getMobile(),
            'dmg_product_images'        => implode(' ',$additionalImages),
            'click_here'                => $url,
        ];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // @pankaj place class in constrcutor 
        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
        $transport = $transportBuilder->setTemplateIdentifier('damaged_item_email_template')
            ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->setTemplateVars($templateVars)
            ->setFrom('general')
            ->getTransport();
        $transport->sendMessage();

        if($applicable_for_insurance)
        {
            $templateVars_ins = [
                'greetingName'              => $receiverInfo['name'],
                'subject'                   => 'Broken Item Insurance Partner',
                'policy_no'                 => $policy_number,
                'policy_copy'               => 'policy_copy',
                'rma_id'                    => $rma_id,
                'part_number'               => $part_number_string,
                'part_name'                 => $part_name_string, 
                'quantity'                  => $part_quantity_string,
                'invoice_no'                => $invoice_no,
                'invoice_date'              => $invoice_date,
                'awb_no'                    => $result_awb['awb'],
                'loss_estimate'             => number_format((float)$loss_estimate,2,'.',','),
                'rsrv_amt_crm'              => '',
                'cust_return_date'          => date('d/m/Y'),
                'seller_address'            => $sellerData->getCompanyAdd(),
                'coverage_details'          => '',
                'seller_firstname_name'     => $sellerData->getFirstname(),
                'seller_middlename_name'    => $sellerData->getMiddlename(),
                'seller_lastname_name'      => $sellerData->getLastname(),
                'seller_mobile'             => $sellerData->getMobile(),
                'dmg_product_images'        => implode(' ',$additionalImages),
                'click_here'                => '',
                'dmg_certificate'           =>  'Damaged certificate will be uploaded soon.',
            ];
    
            $insurence_name = $this->_scopeConfig->getValue(self::XML_PATH_FOR_INSURENCE_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $insurence_email= $this->_scopeConfig->getValue(self::XML_PATH_FOR_INSURENCE_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    
            $receiverInfo_ins = [
                'name' => $insurence_name,
                'email' => $insurence_email,
            ];
            $transport = $transportBuilder->setTemplateIdentifier('damaged_item_email_template')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                ->addTo($receiverInfo_ins['email'], $receiverInfo_ins['name'])
                ->setTemplateVars($templateVars_ins)
                ->setFrom('general')
                ->getTransport();
            $transport->sendMessage();
        }
    }

    public function sendMailToInsuancePartner()
    {


        
    }

    /**
     * is applicable for generating insurance
     *
     * @param string $reason_id
     * @return boolean
     */
    public function isApplicableForInsurance($reason_id)
    {
        $query_wk_rma_reason = "SELECT * FROM `wk_rma_reason` WHERE id = ".$reason_id;
        $result_wk_rma_reason = $this->connection->fetchRow($query_wk_rma_reason);
        if(!empty($result_wk_rma_reason)){
            if($result_wk_rma_reason['assign_for_insurence'] == '1' && $result_wk_rma_reason['status'] == '1'){
                return true;
            }else{
                return false;
            }
        }                   
    }

    /** 
     * check item is broken or applicable for insurance
     * @param string 
     * @return string
     */
    public function isApplicableForInsuranceByRmaId($rma_id){
        $data = [];
        $table_wk_rma_items = $this->connection->getTableName('wk_rma_items');
        $query_wk_rma_items = "SELECT `reason_id`,`item_id` FROM `" . $table_wk_rma_items . "` WHERE rma_id = $rma_id ";
        $result_wk_rma_items = $this->connection->fetchAll($query_wk_rma_items);
        foreach($result_wk_rma_items as $wk_items){
            $query_wk_rma_reason = "SELECT * FROM `wk_rma_reason` WHERE id = ".$wk_items['reason_id'];
            $result_wk_rma_reason = $this->connection->fetchRow($query_wk_rma_reason);
            if(!empty($result_wk_rma_reason)){
                if($result_wk_rma_reason['assign_for_insurence'] == '1' && $result_wk_rma_reason['status'] == '1'){
                    $data[] = [
                        'item_id'   => $wk_items['item_id'],
                        'reason_id' => $result_wk_rma_reason['id'],
                        'reason'    => $result_wk_rma_reason['reason'],
                    ];
                }else{
                    $data = null;
                }
            }
        }
        return $data;
    }

    /**
     * only process amount for broken item instead of total amount
     *
     * @param string $rma_id
     * @return string
     */
    public function getBrokenItemAmount($rma_id)
    {
        $broken_amount=0;
        $table_wk_rma = $this->connection->getTableName('wk_rma');
        $table_wk_rma_items = $this->connection->getTableName('wk_rma_items');
        
        $ids = $this->getApplicableItemIds($rma_id);
        $ids_string = implode(',',$ids);
        $query_wk_rma_items = "SELECT * FROM `" . $table_wk_rma_items . "` WHERE rma_id = '$rma_id' and item_id in ($ids_string) ";
        $result_wk_rma_items = $this->connection->fetchAll($query_wk_rma_items);
        foreach ($result_wk_rma_items as $wk_item_info) {
            $reason_id =  $wk_item_info['reason_id'];
            if($this->isApplicableForInsurance($reason_id)){
                $order_item = $this->orderItemRepository->get($wk_item_info['item_id']);
                $broken_amount += $order_item->getRowTotal();
            }                   
        }

        return $broken_amount;
    }

    public function sendMailToInsurencePartner($certificate_name, $rma_id){

        $invoice_no="";
        $invoice_date="";
        $rmaData = $this->rmaRepository->getById($rma_id);
        $order_id = $rmaData->getOrderId();
        $damagedData = $this->damagedFactory->create();
        $collection = $damagedData->getCollection()->addFieldToFilter('rma_id',$rmaData->getRmaId());
        $updDamagedData = $damagedData->load($collection->getFirstItem()->getId());

        $orderData = $this->orderRepo->get($order_id);
        if ($orderData->hasInvoices()){
            foreach($orderData->getInvoiceCollection() as $invoice){
                $invoice_no = $invoice->getIncrementId();
                $invoice_date = date('d-m-Y',strtotime($invoice->getCreatedAt()));
            }
        }
        
        $table_awb = $this->connection->getTableName('ecomexpress_awb');
        $query_awb = "SELECT `awb` FROM `" . $table_awb . "` WHERE orderid =  $order_id";
        $result_awb = $this->connection->fetchRow($query_awb);

        $table_seller = $this->connection->getTableName('marketplace_orders');
        $query_seller = "SELECT `seller_id` FROM `" . $table_seller . "` WHERE order_id =  $order_id";
        $result_seller = $this->connection->fetchRow($query_seller);
        $sellerData = $this->customerFactory->create()->load($result_seller['seller_id']);

        if(!empty($rmaData->getCreditMemoId())){
            $creditMemoData = $this->creditmemoRepository->get($rmaData->getCreditMemoId());
        }else{
            $creditMemoData = '';
        }

        $store = $this->storeManager->getStore();

        $folderName = $this->webkulHelper->getBaseDirRead().$rmaData->getRmaId().'/image/';
        $images = glob($folderName.'*.{jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG,bmp,BMP}', GLOB_BRACE);
        $additionalImages = [];
        foreach ($images as $filename) {
            $getfilename = explode("/",$filename);
            $justfname = $this->webkulHelper->getBaseUrl().$rmaData->getRmaId().'/image'."/".end($getfilename);
            $additionalImages[] = "<a href=".$justfname." download><img class='wk_rma_add_images' alt='".end($getfilename)."' height='50px' width='100px' src='".$justfname."'/></a>";
        }

        $certificateName = $certificatePath = $loss_estimate = '';
        $insurence_name = $this->_scopeConfig->getValue(self::XML_PATH_FOR_INSURENCE_NAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $insurence_email= $this->_scopeConfig->getValue(self::XML_PATH_FOR_INSURENCE_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        


        $updDamagedData->setDamageCertificate($certificate_name);
        $updDamagedData->setClaimStatus('2');
        $updDamagedData->save();
        $getCertificate = $damagedData->getCollection()->addFieldToFilter('rma_id',$rma_id);
        $certificatePath = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'om/rma/damaged_item_docs/'.$rma_id.'/';
        $certificateName = $getCertificate->getFirstItem()->getDamageCertificate();
        
        $loss_estimate = $this->getBrokenItemAmount($rma_id);


        $policy_number = $this->_scopeConfig->getValue(self::XML_PATH_FOR_POLICY_NO, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $receiverInfo = [
            'name' => $insurence_name,
            'email' => $insurence_email,
        ];
        $forinsurance=true;
        foreach ($this->getApplicableItemIds($rma_id,$forinsurance) as $item_id) {
            $order_item = $this->orderItemRepository->get($item_id);
            $part_number[]=$order_item->getSku();
            $part_name[]=$order_item->getName();
            $part_quantity[]=(int)$order_item->getQtyOrdered();
        }

        $part_number_string = implode(',',$part_number);
        $part_name_string = implode(',',$part_name);
        $part_quantity_string = implode(',',$part_quantity);

        
        $templateVars = [
            'greetingName'              => $receiverInfo['name'],
            'subject'                   => 'Broken Item Insurance Partner',
            'policy_no'                 => $policy_number,
            'policy_copy'               => '',
            'rma_id'                    => $rma_id,
            'part_number'               => $part_number_string,
            'part_name'                 => $part_name_string, 
            'quantity'                  => $part_quantity_string,
            'invoice_no'                => $invoice_no,
            'invoice_date'              => $invoice_date,
            'awb_no'                    => $result_awb['awb'],
            'loss_estimate'             => number_format($loss_estimate,2,'.',','),
            'rsrv_amt_crm'              => '',
            'cust_return_date'          => date('d/m/Y'),
            'seller_address'            => $sellerData->getCompanyAdd(),
            'coverage_details'          => '',
            'seller_firstname_name'     => $sellerData->getFirstname(),
            'seller_middlename_name'    => $sellerData->getMiddlename(),
            'seller_lastname_name'      => $sellerData->getLastname(),
            'seller_mobile'             => $sellerData->getMobile(),
            'dmg_certificate'           =>  'Please find attached damaged certificate.',
            'dmg_product_images'        => implode(' ',$additionalImages),
        ];
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // @pankaj place class in constrcutor 
        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
        $fileContent = file_get_contents($certificatePath.$certificateName); 
        $transport = $transportBuilder->setTemplateIdentifier('damaged_item_email_template')
            ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->setTemplateVars($templateVars)
            ->setFrom('general')
            ->addAttachment($fileContent, $certificateName, 'application/pdf')
            ->getTransport();
        $transport->sendMessage();
    }


}
