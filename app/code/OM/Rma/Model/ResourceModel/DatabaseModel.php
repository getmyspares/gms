<?php

namespace OM\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class DatabaseModel  extends  AbstractDb
{

    protected function _construct()
    {
        $this->_init('om_online_refund','id');
    }

}