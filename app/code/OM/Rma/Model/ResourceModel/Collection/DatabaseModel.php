<?php

namespace OM\Rma\Model\ResourceModel\Collection;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
/** 
 * Resource Model  Class
 */
class DatabaseModel  extends  AbstractCollection
{

    protected function _construct()
    {
        $this->_init('OM\Rma\Model\DatabaseModel','OM\Rma\Model\ResourceModel\DatabaseModel');
    }

    protected function _initSelect()
    {
        $this->getSelect()->from(['main_table' => 'sales_order_payment']);
        // $this->getSelect()->joinLeft(['secondTable' => $this->getTable('om_online_refund')],'secondTable.id = main_table.entity_id');
        return $this;
    }

}