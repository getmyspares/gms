<?php
    namespace OM\Rma\Model\ResourceModel\DamagedItem;
    class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
        protected $_idFieldName = 'id';
        protected $_eventPrefix = 'om_rma_damageditem_collection';
        protected $_eventObject = 'damageditem_collection';
            
        /**
             * Define resource model
            * @return void
            
        */
        protected function _construct(){
            $this->_init('OM\Rma\Model\DamagedItem', 'OM\Rma\Model\ResourceModel\DamagedItem');
        }
    }