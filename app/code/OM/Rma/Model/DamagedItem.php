<?php
    namespace OM\Rma\Model;
    class DamagedItem extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {
        const CACHE_TAG = 'om_rma_damageditem';
        protected $_cacheTag = 'om_rma_damageditem';
        protected $_eventPrefix = 'om_rma_damageditem';
        protected function _construct() {
            $this->_init('OM\Rma\Model\ResourceModel\DamagedItem');
        }
        public function getIdentities() {
            return [self::CACHE_TAG . '_' . $this->getId() ];
        }
        public function getDefaultValues() {
            $values = [];
            return $values;
        }
    }
?>