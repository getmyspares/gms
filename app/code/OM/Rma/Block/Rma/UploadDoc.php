<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rma\Block\Seller;

class UploadDoc extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $sessioneManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        $this->sessioneManager= $sessioneManager;
        $this->messageManager= $messageManager;
        parent::__construct($context, $data);
    }

    public function FunctionName(Type $var = null)
    {
        # code...
    }


}

