<?php
namespace OM\Rma\Block\Adminhtml\DamagedItem;

class UploadCsv extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $_coreRegistry = null;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'rma_id';
        $this->_blockGroup = 'Webkul_Rmasystem';
        $this->_controller = 'adminhtml_allrma';

        parent::_construct();

        if ($this->_isAllowedAction('Webkul_Rmasystem::damageditem')) {
            $this->buttonList->update('save', 'label', __('Import'));
        } else {
            $this->buttonList->remove('save');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }


    public function getCsvUploadUrl(){
        return $this->getUrl('rmsystem/damageditem/updatedatabycsv');
    }

    public function getSampleCsv(){
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'om/rma/damaged_item_docs/sample_csv/insurance.csv';
    }
}