<?php
namespace OM\Rma\Block\Adminhtml\DamagedItem;

use Webkul\Rmasystem\Model\ResourceModel\Rmaitem\CollectionFactory as ItemCollectionFactory;
use Magento\Sales\Model\OrderRepository;
use Webkul\Rmasystem\Api\AllRmaRepositoryInterface;
use Webkul\Rmasystem\Api\Data\AllrmaInterfaceFactory;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    /**
     * @var ItemCollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Webkul\Rmasystem\Api\Data\RmaitemInterfaceFactory
     */
    protected $rmaItemDataFactory;

    /**
     * @var \Webkul\Rmasystem\Api\RmaitemRepositoryInterface
     */
    protected $rmaItemRepository;

    /**
     * @var \Webkul\Rmasystem\Api\Data\ShippinglabelInterface
     */
    protected $labelCollectionDataFactory;

    /**
     * @var \Webkul\Rmasystem\Api\Data\ConversationInterfaceFactory
     */
    protected $conversationDataFactory;

    /**
     * @var \Webkul\Rmasystem\Api\ConversationRepositoryInterface
     */
    protected $conversationRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var AllRmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var AllrmaInterfaceFactory
     */
    protected $rmaFactory;

    /**
     * @var \Webkul\Rmasystem\Api\Data\ReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Sales\Model\Order\ItemRepository
     */
    protected $orderItemRepository;

    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    protected $customerRepository;

    /**
     * @var \Webkul\Rmasystem\Helper\Data
     */
    protected $helper;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\View\Element\FormKey $formKey
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Webkul\Rmasystem\Model\ResourceModel\Allrma\CollectionFactory $rmaCollectionFactory
     * @param \Webkul\Rmasystem\Model\ResourceModel\Reason\CollectionFactory $regionCollectionFactory
     * @param \Webkul\Rmasystem\Model\ResourceModel\Rmaitem\CollectionFactory $rmaItemCollectionFactory
     * @param \Webkul\Rmasystem\Model\ResourceModel\Shippinglabel\CollectionFactory $labelCollectionFactory
     * @param \Webkul\Rmasystem\Model\ResourceModel\Conversation\CollectionFactory $conversationCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository,
        \Magento\Backend\Block\Widget\Context $context,
        \OM\Rma\Model\DamagedItemFactory $damagedFactory,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\View\Element\FormKey $formKey,
        ItemCollectionFactory $itemCollectionFactory,
        \Webkul\Rmasystem\Api\Data\RmaitemInterfaceFactory $rmaItemDataFactory,
        \Webkul\Rmasystem\Model\ResourceModel\Shippinglabel\CollectionFactory $labelCollectionDataFactory,
        \Webkul\Rmasystem\Api\RmaitemRepositoryInterface $rmaItemRepository,
        \Webkul\Rmasystem\Api\Data\ConversationInterfaceFactory $conversationDataFactory,
        \Webkul\Rmasystem\Api\ConversationRepositoryInterface $conversationRepository,
        \Webkul\Rmasystem\Api\ReasonRepositoryInterface $reasonRepository,
        OrderRepository $orderRepository,
        AllRmaRepositoryInterface $rmaRepository,
        AllrmaInterfaceFactory $rmaFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Model\Order\ItemRepository $orderItemRepository,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Webkul\Rmasystem\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \OM\Rma\Helper\DamagedItem\Data $damagedItemHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        $this->damagedItemHelper = $damagedItemHelper;
        $this->damagedFactory = $damagedFactory;
        $this->storeManager = $storeManager;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->rmaItemDataFactory = $rmaItemDataFactory;
        $this->rmaItemRepository = $rmaItemRepository;
        $this->conversationDataFactory = $conversationDataFactory;
        $this->conversationRepository = $conversationRepository;
        $this->reasonRepository = $reasonRepository;
        $this->orderRepository = $orderRepository;
        $this->rmaRepository = $rmaRepository;
        $this->rmaFactory = $rmaFactory;
        $this->productRepository = $productRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->customerRepository = $customerRepository;
        $this->labelCollectionDataFactory = $labelCollectionDataFactory;
        $this->helper = $helper;
        $this->_coreRegistry = $registry;
        $this->_currency = $currency;
        $this->_date = $date;
        $this->_formKey = $formKey;
        $this->_coreRegistry = $registry;
        $this->messageManager = $messageManager;
        parent::__construct($context, $data);
    }

    /**
     * Initialize blog post edit block.
     */
    protected function _construct()
    {
        $this->_objectId = 'rma_id';
        $this->_blockGroup = 'Webkul_Rmasystem';
        $this->_controller = 'adminhtml_allrma';

        parent::_construct();
        $id = $this->getRequest()->getParam('id');

        $condition_number = $this->damagedItemHelper->getConditionNumber();
        if(empty($this->getDamagedCertificate($this->getRmaId()))){
            if((float)$this->getCreditMemoAmount()>$condition_number){
                $this->buttonList->update('save', 'label', __('Submit Certificate'));
            }else{
                $this->buttonList->remove('save');
            }
        }else{
            $this->buttonList->remove('save');
        }
        
        // if($damageCollection->getClaimStatus() == '2' || $damageCollection->getClaimStatus() == '3'){
        //     $this->addButton(
        //         'approveClaim',
        //         [
        //             'label' => __('Approve Claim'),
        //             'onclick'   =>  'deleteConfirm(\'' . __(
        //                 'Are you sure! want to conferm claim?'
        //             ) . '\', \'' . $this->getCompleteClaimUrl() . '\')',
        //             'class' => 'primary',
        //             'level' => -1
        //         ]
        //     );
        // }

        $this->addButton(
            'resend_logistic_email',
            [
                'label' => __('Re-send Email to Logistics partner'),
                'onclick'   =>  'deleteConfirm(\'' . __(
                    'Are you sure! You want to Re-Send Email to logistics partner?'
                ) . '\', \'' . $this->reSendLogisticEmailUrl() . '\')',
                'class' => 'secondry',
                'level' => -1
            ]
        );

        $this->buttonList->remove('delete');
    }

    /**
     * Check permission for passed action.
     *
     * @param string $resourceId
     *
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getOrder($orderId)
    {
        return $this->orderRepository->get($orderId);
    }

    
    /**
     * Retrieve url for form submiting.
     *
     * @return string
     */
    public function getCreditMemoAmount()
    {
        return $this->creditmemoRepository->get($this->getRmaDetail()->getCreditMemoId())->getGrandTotal();
    }
     
    /**
     * Retrieve url for form submiting.
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->getUrl('rmsystem/damageditem/update');
    }

    // public function getCompleteClaimUrl()
    // {
    //     $rmaId = $this->getRmaId();
    //     return $this->getUrl('rmsystem/damageditem/completeclaim', ['id'=>$rmaId]);
    // }


    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later.
     *
     * @return string
     */
    // protected function _getSaveAndContinueUrl()
    // {
    //     return $this->getUrl('allrma/*/update', ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']);
    // }

    public function getAllStatus($resolutionType)
    {
        return $this->helper->getAllStatus($resolutionType);
    }

    /**
     * @return int
     */
    public function getRmaId()
    {
        $id = $this->getRequest()->getParam('id');
        $damageCollection = $this->damagedFactory->create()->load($id);
        return $damageCollection->getRmaId();
    }

    public function getDamagedCertificate($rma_id){
        $collection = $this->damagedFactory->create();
        $singleRow = $collection->getCollection()->addFieldToFilter('rma_id', $rma_id)->getFirstItem();
        if(!empty($singleRow->getDamageCertificate())){
            return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'om/rma/damaged_item_docs/'.$rma_id.'/'.$singleRow->getDamageCertificate();
        }else{
            return null;
        }
    }

    /**
     * @return int
     */
    public function getCustomerDetail($id)
    {
        return $this->customerRepository->getById($id);
    }
    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->helper->getBaseUrl().$this->getRmaId().'/image';
    }
    /**
     * @return string
     */
    public function getBaseDirRead()
    {
        return $this->helper->getBaseDirRead();
    }

    public function getImages()
    {
        $folderName = $this->getBaseDirRead().$this->getRmaId().'/image/';

        $images = glob($folderName.'*.{jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG,bmp,BMP}', GLOB_BRACE);
        return $images;
    }

    /**
     * @return Mixed \Webkul\Rmasystem\Model\Allrma
     */
    public function getRmaDetail()
    {
        return $this->rmaRepository->getById($this->getRmaId());
    }

    /**
     * @return Mixed
     */
    public function getSalesOrderItemDetail($itemId)
    {
        return $this->orderItemRepository->get($itemId);
    }
    /**
     * @return Mixed \Magento\Sales\Model\Order\Item
     */
    public function getProductDetail($productId)
    {
        return $this->productRepository->getById($productId);
    }
    /**
     * @return Mixed \Webkul\Rmasystem\Model\Reason
     */
    public function getReason($reasonId)
    {
        return $this->reasonRepository->getById($reasonId);
    }

    /**
     * @return array
     */
    public function getItemCollection($rmaId)
    {
        $collection = $this->rmaItemDataFactory->create()
          ->getCollection()
          ->addFieldToFilter('rma_id', $rmaId);
        return $collection;
    }
    /**
     * @return array
     */
    public function getShippingLabelCollection()
    {
        $collection = $this->labelCollectionDataFactory->create()
          ->addFieldToFilter('status', 1);

        return $collection;
    }
    /**
     * @return string
     */
    public function getLabelBaseUrl()
    {
        return $this->helper->getLabelBaseUrl();
    }
    /**
     * @param Decimal $price
     *
     * @return formated
     */
    public function getCurrency($price)
    {
        return $currency = $this->_currency->format($price);
    }
    /**
     * @param  String Date
     *
     * @return String Timestamp
     */
    public function getTimestamp($date)
    {
        return $date = $this->_date->timestamp($date);
    }

    /**
     * Get form key.
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->_formKey->getFormKey();
    }

    public function reSendLogisticEmailUrl()
    {           
        $id = $this->getRequest()->getParam('id');
        return $this->getUrl('rmsystem/damageditem/resendlogisticemail',['rma_id'=>$this->getRmaId(), 'id'=>$id]);
    }
}
