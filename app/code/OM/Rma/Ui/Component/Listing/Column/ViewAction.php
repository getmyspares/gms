<?php
namespace OM\Rma\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class ViewAction extends Column
{
    protected $urlBuilder;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['entity_id'])) {
                    $viewUrlPath = $this->getData('config/viewUrlPath') ?: '#';
                    $urlEntityParamName = $this->getData('config/urlEntityParamName') ?: 'entity_id';
                    
                    if ($item['online_refund_complete']=="Yes") {
                        $item[$this->getData('name')] = [
                            'view' => [
                                'href' => $this->urlBuilder->getUrl(
                                    $viewUrlPath,
                                    [
                                        $urlEntityParamName => $item['entity_id']
                                    ]
                                ),
                                'label' => __('View')
                            ]
                        ];
                    } else {
                        $item[$this->getData('name')] = [
                            'view' => [
                                'href' => $this->urlBuilder->getUrl(
                                    $viewUrlPath,
                                    [
                                        $urlEntityParamName => $item['entity_id']
                                    ]
                                ),
                                'label' => __('View')
                            ],
                            'razorpayrefund' => [
                                'href' => $this->urlBuilder->getUrl(
                                    "rmsystem/onlinereturn/generate",
                                    [
                                        $urlEntityParamName => $item['entity_id']
                                    ]
                                ),
                                'label' => __('Refund on Razorpay'),
                                'confirm' => [
                                    'title' => __('Generate Online Refund'),
                                    'message' => __('Are you sure?')
                                ]
                            ]
                        ];
                    }
                }
            }
        }
        return $dataSource;
    }
}