<?php
namespace OM\Rma\Ui\Component\Listing\Column\DamagedItem;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class ResolutionType extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $resolution = '';
                if($item['resolution_type'] == 0) {
                    $resolution = 'Refund';
                } elseif ($item['resolution_type'] == 1) {
                    $resolution = 'Exchange';
                } else {
                    $resolution = 'Cancel Items';
                }
                $item[$this->getData('name')] = $resolution;
            }
        }
        return $dataSource;
    }
}
