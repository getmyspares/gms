<?php

namespace OM\Rma\ViewModel\Rmasystem\Block\Adminhtml\Allrma;

use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * using this class for howwing rma detail on the seller panel in the frontend  @ritesh10january2021
 */
class ViewRma implements ArgumentInterface
{

  public function __construct(
    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface, 
    \Magento\Sales\Api\Data\OrderInterface $orderInterface,
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    \Magento\Store\Model\StoreManagerInterface $storeManagerInterface 
    )
  {
    $this->customerRepositoryInterface = $customerRepositoryInterface;
    $this->storeManagerInterface = $storeManagerInterface;
    $this->orderInterface = $orderInterface;
    $this->connection = $resourceConnection->getConnection();
  }

  public function getCustomer($customer_id)
  {
    return $this->customerRepositoryInterface->getById($customer_id);
  }

  public function getCreditMemoAmount($credit_memo_id)
  {
    $query = "SELECT grand_total FROM `sales_creditmemo`  where entity_id='$credit_memo_id'";
    return $this->connection->fetchAll($query);
  }

  public function getBaseUrl()
  {
    return $this->storeManagerInterface->getStore()->getBaseUrl();
  }

  public function getOrderShippingAddress($orderid) {
    $order = $this->orderInterface->load($orderid);  // pass your order id here       
    return $order->getShippingAddress();
  }

}

?>