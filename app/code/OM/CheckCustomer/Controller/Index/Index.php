<?php
namespace OM\CheckCustomer\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
   		\Magento\Framework\App\ResourceConnection $resource
    ) {
   		$this->_resource = $resource;
   		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
    {
        $connection = $this->_resource->getConnection();
        $mobNumber = $_POST['mobNumber'];
        $query = "SELECT entity_id FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND value = $mobNumber";
        $cusEmail = $connection->fetchOne($query);
        echo $cusEmail;
    }
}
