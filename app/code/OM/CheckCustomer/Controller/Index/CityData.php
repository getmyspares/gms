<?php
namespace OM\CheckCustomer\Controller\Index;

class CityData extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
   		\Magento\Framework\App\ResourceConnection $resource
    ) {
   		$this->_resource = $resource;
   		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
    {
        $connection = $this->_resource->getConnection();
        $regions  = $connection->fetchAll("SELECT city FROM `om_city_state` order by city asc");
        echo json_encode($regions);
    }
}
