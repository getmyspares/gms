<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);


namespace OM\ExpressShipping\Controller\Ewaybill;

class Credentials extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory2
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\ResourceConnection $ResourceConnection,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->customerSession = $customerSession;
        $this->connection = $ResourceConnection->getConnection();
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
 
    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() 
    {
      
      $username = $this->getRequest()->getParam('username');
      $password = $this->getRequest()->getParam('password');
      
      $this->customerSession->setEwayBackpathOrderId($eway_backpath_order_id);
      $seller_credential_check = $this->areCredentialPresent();
      if($seller_credential_check['status'])
      {

      } 
      else 
      { ?>
2
        <form action="" method="post">
          Username : <input type="text"> <br>
          Password : <input type="text"> <br>
          <input type="submit"/>  
        </</form>




      <?php }     
      
      $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
      $resultRedirect->setUrl($this->_redirect->getRefererUrl());
      return $resultRedirect;
    
    }

    public function areCredentialPresent()
    {
      $status = array("status"=>false,"id"=>0);
      $seller_id = $this->customerSession->getCustomer()->getId();
      $query = "Select id from the eway_bill_data where seller_id='seller_id'";
      $id = $this->connection->fetchOne($query);
      if(!empty($id))
      {
        $status = array("status"=>false,"id"=>$id);
      }
      return $status;
    }

    public function areCredentialValid()
    {

    }
}

