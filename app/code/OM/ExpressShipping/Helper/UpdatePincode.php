<?php

namespace OM\ExpressShipping\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class UpdatePincode extends AbstractHelper
{
  protected $_objectManager;
  
  protected $_rateResultErrorFactory;

  private $pincodeTable;

  public function __construct(
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Framework\App\ResourceConnection $resourceConnection,
      array $data = []
  ) {
      $this->connection = $resourceConnection->getConnection();
      $this->logger = $logger;
      $this->pincodeTable = "pincode_details";
      $this->scopeConfig = $scopeConfig;
  }
    
  public function markAllAsOld(){
    $tablename = $this->pincodeTable;
    $query = "update $tablename set cron_status='old'";
    $this->connection->query($query);
    echo "all pincode marked as old"; 
  }

  public function setPincodeEnabled($pincode){
    $tablename = $this->pincodeTable;
    $query = "update $tablename set is_active = '1', cron_status='new' where pincode='$pincode'";
    $this->connection->query($query);
  }

  public function disableOldPincode(){
    $tablename = $this->pincodeTable;
    $query = "update $tablename set is_active = '0' where cron_status='old'";
    $this->connection->query($query);
    echo "old pincode deactivated"; 
  }
	
}
