<?php

namespace OM\ExpressShipping\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class Data extends AbstractHelper
{
    protected $_objectManager;
    
    protected $_rateResultErrorFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
				\Psr\Log\LoggerInterface $logger,
				\Magento\Customer\Api\CustomerRepositoryInterface $CustomerRepositoryInterface,
				\Magento\Customer\Api\AddressRepositoryInterface $AddressRepositoryInterface,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        array $data = []
    ) {
		$this->_objectManager = $objectManager;
        $this->connection = $resourceConnection->getConnection();
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
				/* if  customer pincode  not  found in quoote */
				$this->customerRepository = $CustomerRepositoryInterface;
        $this->addressRepository = $AddressRepositoryInterface;
				
				$this->scopeConfig = $scopeConfig;
        $this->_rateResultErrorFactory= $rateErrorFactory;
    }
    
    public function getExpressShippingPrice($quote,$mobile=false){
			/* in case of mobile $quote represent order  object */
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
		$is_mobile = ($mobile=="mobile") ? true:false;
		$shippingPrice = 0;		
		$coupon   = $objectManager->create('Magento\SalesRule\Model\Coupon');
		$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
		$couponCode = $quote->getCouponCode();
		$ruleId     = $coupon->loadByCode($couponCode)->getRuleId();
		$rule       = $saleRule->load($ruleId);
		$freeShippingCoupon = $rule->getSimpleFreeShipping();
		
		$sutotal     = $quote->getSubtotal();
		$minAmount   = (int)$this->scopeConfig->getValue('carriers/express_shipping/free_price', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$free_active = $this->scopeConfig->getValue('carriers/express_shipping/free_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
		$shippingAddress = $quote->getShippingAddress();
		if( $shippingAddress->getData('parent_subtotal') && $shippingAddress->getData('parent_subtotal') > 0){
			$sutotal = $shippingAddress->getData('parent_subtotal');
		}	         
        
		$allowed = true;
		$pincode_allowed = $this->checkRestrictedPincode($quote);
		if($pincode_allowed)
		{
			if($freeShippingCoupon){   //Check If free Shipping Coupon Applied		
				$shippingPrice = 0;	
				$allowed = true;	
			} elseif ( $free_active && $sutotal >= $minAmount) {  //Check if free shipping enable and condition apply
							$shippingPrice = 0;  
							$allowed = true;	     
			} else {	
					$shippingAddress = $quote->getShippingAddress();	
					$customer_zip    = $shippingAddress->getPostcode();
					
					/* special  case for  mobile  if  zipcode  is not  found @ritesh15march2021 */
					if(empty($customer_zip))
					{
						$customer_id =$quote->getCustomerId();
						if(!empty($customer_id))
						{
							$customer = $this->customerRepository->getById($customer_id);
							$shippingAddressId = $customer->getDefaultShipping();
						 
						 //get default billing address
							try {
								 $shippingAddress = $this->addressRepository->getById($shippingAddressId);
								 $customer_zip = $shippingAddress->getPostcode();
								} catch (\Exception $e) {
							 }
						}
					}
				$model = $objectManager->get('Ecom\Ecomexpress\Model\Pincode')->load($customer_zip,'pincode');
				if(!empty($model)){ 				
					$buyer_city=$this->get_city_pincode($customer_zip);		
					if($buyer_city=='0'){
						$allowed = false;	
					}	
							
					if($customer_zip!='0') {	
						$shippingPrice = $this->calculateEcomPrice($customer_zip,$quote,$is_mobile);
					}
				}else{
					$allowed = false;	
				}
			} 
		} else 
		{
			$allowed = false;
		}	
		return array('allowed'=>$allowed,'price'=> $shippingPrice);
	}

	public function checkRestrictedPincode($quote)
	{
		$allowed=true;
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
		$items           =$quote->getAllItems();
		$shippingAddress = $quote->getShippingAddress();	
		$customer_zip    = $shippingAddress->getPostcode();	
		   
		$query = "Select city_name from  pincode_details where pincode='$customer_zip'"; 
		$customer_city = $connection->fetchOne($query); 
		if(!$customer_city){
			return false;
		}
		$customer_city = strtolower($customer_city); 
						
		foreach ($items as  $item) {
			$product_id = $item->getData('product_id');
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			$pincode_restrictions = $product->getData('pincode_restriction');
			if(!empty($pincode_restrictions))
			{
					$pincode_restricted_city_array = explode(",",$pincode_restrictions);

	
					if(!in_array($customer_city,$pincode_restricted_city_array))
					{
							$allowed = false;
							return $allowed;
					}
			}
		}
		return $allowed;
	}
	
    public function calculateEcomPrice($customer_zip,$quote,$mobile=false){	
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
			
		$total_shippingAmount = 0;							
		/* Ecom pincode pricing */ 
		$items = $quote->getAllItems();  
		foreach ($items as $item) {
			$itemId    = $item->getItemId();
			$productId = $item->getProductId();
			$qty       = ($mobile == true) ? $item->getData('qty_ordered') : $item->getQty() ;
			$shippingAmount = $this->ecom_price_by_product($customer_zip,$productId,$qty,$quote,$mobile); 
			$connection->query("UPDATE quote_item SET item_ship_price=$shippingAmount WHERE item_id='".$itemId."'");
			$total_shippingAmount = $total_shippingAmount + $shippingAmount;
		} 
		return $total_shippingAmount;
	}
    		
    
	public function ecom_price_by_product($buyer_zipcode,$product_id,$qty,$quote,$is_mobile=false) 
	{
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;

		$catQuery = $connection->fetchAll("SELECT category_id  FROM `catalog_category_product` WHERE `product_id` = $product_id");
		foreach($catQuery as $cats){
			$catId = $cats['category_id'];
			$catVal = $connection->fetchOne("SELECT value FROM `catalog_category_entity_int` where attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='is_shipping_charge_free') AND entity_id = $catId");
			if($catVal && $catVal=='1'){
				return 0;
			}
		}

		//$quoteData = $objectManager->get('\Magento\Quote\Model\Quote')->load($quote->getId());
		$items = $quote->getAllItems();
		$freeSellers = array();
		foreach ($items as $item) 
		{ 
			if($is_mobile)
			{
					$p_pid = $item->getProductId();
					$p_qty = $item->getData('qty_ordered');
			} else  
			{
				$p_pid = $item->getProduct()->getId();
				$p_qty = $item->getQty();
			}
			$sellerId = $connection->fetchOne("SELECT seller_id FROM `marketplace_product` where mageproduct_id = $p_pid");				
			$noEcomm = $connection->fetchOne("SELECT COUNT(ship_ecom) FROM `catalog_product_entity_tier_price` where entity_id=$p_pid AND ship_ecom=0 AND (qty = $p_qty or qty < $p_qty)");
			if($noEcomm > 0){
				$freeSellers[] = $sellerId;
			}
		}	

		$sellerId_1 = $connection->fetchOne("SELECT seller_id FROM `marketplace_product` where mageproduct_id = $product_id");	
		if( in_array($sellerId_1,$freeSellers) ){
			return 0;
		}

		$qryS = "SELECT COUNT(shipping_free) FROM `catalog_product_entity_tier_price` where entity_id=$product_id AND shipping_free=1 AND (qty = $qty or qty < $qty)";
		$ifFreeSHip = $connection->fetchOne($qryS);
		if($ifFreeSHip > 0){
			return 0;
		}			
				
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url     = $storeManager->getStore()->getBaseUrl();
				
		$ncrarray=array('delhi','faridabad','gurugram','ghaziabad','noida','greater noida','bhiwani','jhajjar','jind','karnal');
		$metroarray=array('delhi','mumbai','bengaluru','chennai','kolkata');
		$jnkarray=array('jnk','jku');  
		$neststatecodearray=array('mn','tr','ml','ar','sk','nl','mz'); 
					
		$buyer_city=$this->get_city_pincode($buyer_zipcode);
		if($buyer_city=='0'){
			return 0;	  
		}	  

		$buyer_region      = $this->get_region_pincode($buyer_zipcode);
		$buyer_region_code = $this->get_region_code_pincode($buyer_zipcode);
		$buyer_ros         = $this->ros_checker($buyer_zipcode);	
		$buyer_up_country  = $this->up_country_checker($buyer_zipcode);	

		/* state for ne calculation */
		$sellerarray=array();		
		$base_rate       = 30;
		$additional_rate = 30;

		$product_total_charge = 0;	
		$product_total_charge = 0;
		$fuel_amount   = 0;
		$sub_total     = 0;
		$other_amount  = 0;
		$net_total     = 0;
		$gst_amount    = 0;
		$grand_total   = 0;
		$zone          = 0;
		$ros           = 0;  
		$up_country    = 0; 

		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);												
		$weight  = $product->getWeight();		
		$vol_weight = ($product->getEcomLength()*$product->getEcomBreadth()*$product->getEcomHeight())/5000;
				 
		if($vol_weight > $weight)  {
			$weight=$vol_weight;   
		}
		$weight;
								
		$prod_weight=$weight*$qty;
		$weight=$prod_weight;
		 
		$actual_prd_id=$product->getProductId();
		
		$select = $connection->select()->from('marketplace_product')->where('mageproduct_id = ?', $product_id); 
		$result = $connection->fetchAll($select);  	
		$count  = count($result);
		
		if($count==0){
			$seller_id='20';		  
		}	
		
		foreach($result as $row){
			$seller_id=$row['seller_id'];			
		}	
		
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');	
		$sellerArray=$custom_helpers->get_seller_details($seller_id);
		if(!empty($sellerArray)){
			$seller_zipcode=$sellerArray['zipcode'];	
		} else {
			$seller_zipcode='122002'; 	
		}
		 	
		$seller_zipcode;  
		$results = 0;
		$seller_city        = $this->get_city_pincode($seller_zipcode); 
		$seller_region      = $this->get_region_pincode($seller_zipcode); 
		$seller_region_code = $this->get_region_code_pincode($seller_zipcode); 
		$seller_ros         = $this->ros_checker($seller_zipcode);	
		$seller_up_country  = $this->up_country_checker($seller_zipcode);
		
		if(in_array($seller_city,$ncrarray) && in_array($buyer_city,$ncrarray)) // NCR
		{
			$zone="A";		
			$results=1;	 
		}  else if($buyer_city==$seller_city) // Inter City
		{
			$zone="A";		 
			$results=1;	 
		}  else if(in_array($seller_city,$metroarray) && in_array($buyer_city,$metroarray)) // Metro
		{
			$zone="C";		 
			$results=1;	  
		}
		
		if($buyer_ros==1 || $seller_ros==1) {
			$ros=1;	 
		}	
		
		if($buyer_up_country==1 || $seller_up_country==1) {
			$up_country=1;
		}	
		 
		// Inter Region
		if($results==0) {
			if($buyer_region==$seller_region)  {
				if((in_array($buyer_region_code,$jnkarray)) && (in_array($seller_region_code,$jnkarray))) {
					$zone="B"; 	 	 
					$results=1;	 	 
				} else if((in_array($buyer_region_code,$jnkarray)) || (in_array($seller_region_code,$jnkarray))) {
					$zone="E";	 	 
					$results=1;	  	 
				} else {		
					$zone="B";	 	 
					$results=1;	
				}  		
			} else {				
				if($buyer_region_code!=$seller_region_code) {
					$zone="D";  	
					$results=1; 
					if($buyer_region_code=="jnk" || $buyer_region=="nthest" || $seller_region=="nthest") {
						$results=0;	 	
					}					
				}
						
				if($results==0) {
					if($buyer_region_code=="jnk") {
						$zone="E";	 	  
						$results=1;	 		
					} else if($buyer_region=="nthest" || $seller_region=="nthest") {
						$zone="F";	  	  
						$results=1;	 		
					} else {		
						if($ros!=0) {
							$zone="H";	   	 
							$results=1;		  
						} else if($up_country!=0) {
							$zone="G"; 	  	 
							$results=1;		
						} 
					}
				}				
			}
		}
			  
		$weight;		
		$seller_city;
		$price='';
		$price=json_decode($this->get_freight_charge($zone));
		$ros_base=0;		
		$ros_additional=0;	
		
		$up_base=0;		
		$up_additional=0;	
		$ex_zone=0;
		 
		if($ros!=0) {
			$ros_base=10; 		
			$ros_additional=10;		
			$ex_zone='H';
		} 

		if($up_country!=0) {
			$up_base=5;		
			$up_additional=5;	 	
			$ex_zone='G';	
		}    
				
		$charge=0;
		$base_price=0;
		$additional_price=0; 
		$product_total_charge=0; 
		
		$base_price=$price->Base;
		$additional_price=$price->Additional;

		$weight=$weight*1000;		
		$findWeight = $weight/500; //total weight divided by your weight
		
		$findWeight = ceil($findWeight); 
		if($findWeight > 1) { 			
			for($i=1; $i <= $findWeight; $i++)   {
				//$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base+$base_price;
				$charge = $charge + $additional_price+$ros_additional+$up_additional+$ros_base+$up_base;
			}      
		} else  { 
			//$charge = $base_price+$ros_base;
			$charge = $base_price+$ros_base+$up_base; 
		}
		
		$buyer_state = $this->get_state_pincode($buyer_zipcode);
		$seller_state = $this->get_state_pincode($seller_zipcode);
  
		/* Zone F  calculation for northeast states  */
		if($zone=="F") {
			$nespecialcharge=0;
			if(in_array($buyer_state,$neststatecodearray) || in_array($seller_state,$neststatecodearray)){
				/* add a surcharge of INR 50 per 500 gram */
				for($i=0; $i < $findWeight; $i++)   
				{
					$nespecialcharge += 50;		
				}
			}
			$charge = $charge + $nespecialcharge;
		}			 
		  
		
		$charge;		
		$product_total_charge=$product_total_charge + $charge;			  
		$product_total_charge;
		$fuel_amount=($product_total_charge * 28)/100; 
		$sub_total=$product_total_charge + $fuel_amount;
		
		$other_amount=($sub_total*2)/100;
		if($other_amount < 35){
			$other_amount=35;	
		}	 
		$other_amount;
		$other_amount=0;
		
		$net_total=$sub_total + $other_amount;	
		$gst=$fuel_amount+$other_amount;
		
		$helper = $objectManager->create('Baljit\Buynow\Helper\Data');
		$shipping_gst = $helper->getConfig('buynows/general/shipping_gst');
		if(empty($shipping_gst)){
			$shipping_gst=0;
		}
		 	 
		$gst_amount=($net_total*$shipping_gst)/100; 
		$grand_total=$net_total+$gst_amount;
		
		if($ex_zone=='0') {
			$show_zone=$zone;
		} else {	 
			$show_zone=$zone.'+'.$ex_zone;
		}		
		 
		$result_array=array('product_total'=>$product_total_charge,'fuel_amount'=>$fuel_amount,'after_fuel_added'=>$sub_total,'other_amount'=>$other_amount,'after_other_total'=>$net_total,'gst_amount'=>$gst_amount,'gst_rate'=>$shipping_gst,'grand_total'=>$grand_total,'zone'=>$show_zone);   
		$array = array('success' => 1, 'result' => $result_array); 
		
		$admin_comm = $helper->getConfig('buynows/general/logistic_pana_tax');
		if(empty($admin_comm))
		{
			$admin_comm=0;
		} 
		
		$grand_total=$grand_total+(($grand_total*$admin_comm)/100);    
		$grand_total=number_format($grand_total, 2, '.', '');
						
		return $grand_total;	     

	}

	

	public function get_city_pincode($pincode)
	{ 
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
					
		$select = $connection->select()->from('pincode_details')->where('pincode = ?', $pincode);
		$result = $connection->fetchAll($select);
				
		if(!empty($result)){
			return strtolower($result[0]['city_name']);
		} else {
			return 0;
		}		 
	}

	public function get_region_pincode($pincode)
	{
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
				
		$select = $connection->select()->from('pincode_details') ->where('pincode = ?', $pincode);
		$result = $connection->fetchAll($select);
				
		if(!empty($result)){	
			return strtolower($result[0]['region']);   
		} else {
			return 0; 
		}		    
	} 

	public function get_region_code_pincode($pincode)
	{
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;

		$sql = "Select region_code FROM pincode_details where pincode='".$pincode."'";
		$result = $connection->fetchAll($sql);		

		if(!empty($result)) {	
			return strtolower($result[0]['region_code']); 
		} else {
			return 0;   
		}
	} 

	public function get_state_pincode($pincode)
	{
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
					
		$select = $connection->select()->from('pincode_details')->where('pincode = ?', $pincode);
		$result = $connection->fetchAll($select);
				
		if(!empty($result)){	
			return strtolower($result[0]['state']);   
		} else {
			return '110001';
		}			   
	} 

	public function ros_checker($pincode)
	{			
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;	

		$select = $connection->select()->from('pincode_details_ros')->where('pincode = ?', $pincode);
		$result = $connection->fetchAll($select);		

		return (!empty($result)) ? 1 : 0;		   
	}

	public function up_country_checker($pincode)
	{
		$objectManager = $this->_objectManager;
		$connection    = $this->connection;
				
		$select = $connection->select()->from('pincode_details_upcountry')->where('pincode = ?', $pincode);
		$result = $connection->fetchAll($select);
				
		return (!empty($result)) ? 1 : 0;			    
	}
    
    public function get_freight_charge($zone) {
		if($zone=="A") // Inter City
		{
			$array=array('Base'=>24,'Additional'=>24);	
		}
		else if($zone=="B") // Inter Region  
		{
			$array=array('Base'=>30,'Additional'=>30);	
		} 
		else if($zone=="C") // Metro  
		{
			$array=array('Base'=>37,'Additional'=>37);	
		}		
		else if($zone=="E") // JNK  
		{
			$array=array('Base'=>45,'Additional'=>45);	
		}
		else if($zone=="F") // NTHEST  
		{
			$array=array('Base'=>45,'Additional'=>45);	 
		}
		else if($zone=="D") // Rest Of India  
		{
			$array=array('Base'=>39,'Additional'=>39);	 
		}	
		else if($zone=="G") // Rest Of India  
		{
			$array=array('Base'=>5,'Additional'=>5);	  
		}	 
						
		return json_encode($array); 		 
	}
	
}
