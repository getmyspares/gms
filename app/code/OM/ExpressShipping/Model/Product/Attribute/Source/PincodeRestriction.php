<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ExpressShipping\Model\Product\Attribute\Source;

class PincodeRestriction extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public  function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->connection = $resource->getConnection();
    }
    public function getAllOptions()
    {
        $query="SELECT DISTINCT city_name FROM `pincode_details` order by city_name asc";
        $deliverable_cities = $this->connection->fetchAll($query); 
        $city_array = [];
        foreach ($deliverable_cities as $value) {
            $city_name =$value['city_name'];
            $city_name_for_db =  trim(strtolower($value['city_name']));
            $city_name_for_db = str_replace("'","", $city_name_for_db);
            $city_array[] = ['label' => $city_name, 'value'=>$city_name_for_db];
        }
        $this->_options = $city_array;
        return $this->_options;
    }
}

