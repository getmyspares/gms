<?php

namespace OM\ExpressShipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class Expressshipping extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'express_shipping';
    
    protected $_objectManager;
    
    protected  $_rateResultErrorFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        array $data = []
    ) {
		$this->_objectManager = $objectManager;
        $this->connection = $resourceConnection->getConnection();
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_rateResultErrorFactory= $rateErrorFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['express_shipping' => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
		
		$objectManager = $this->_objectManager;
		$sHelper = $objectManager->get('\OM\ExpressShipping\Helper\Data');
		
		$quote    = $objectManager->create('Magento\Checkout\Model\Cart')->getQuote();
		$sInfo   = $sHelper->getExpressShippingPrice($quote);
		
		$allowed =       $sInfo['allowed'];
		$shippingPrice = $sInfo['price'];
		
		$result = $this->_rateResultFactory->create();
		$method = $this->_rateMethodFactory->create();
		
		if( $allowed == true ){
			$method->setCarrier('express_shipping');
			$method->setCarrierTitle($this->getConfigData('title'));

			$method->setMethod('express_shipping');
			$method->setMethodTitle($this->getConfigData('name'));

			$method->setPrice($shippingPrice);
			$method->setCost($shippingPrice);        	

			$result->append($method);

			return $result;
		}else{
			$error =$this->_rateResultErrorFactory->create();
			$error->setCarrier('express_shipping');
			$error->setCarrierTitle($this->getConfigData('title'));
			$method->setMethod('express_shipping');
			$method->setMethodTitle($this->getConfigData('name'));
			$error->setErrorMessage('This shipping method is currently unavailable in this zipcode');
			$result->append($error);
			return $result;
		}
    }
    

}
