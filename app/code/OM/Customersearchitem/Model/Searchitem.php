<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Customersearchitem\Model;

use Magento\Framework\Api\DataObjectHelper;
use OM\Customersearchitem\Api\Data\SearchitemInterface;
use OM\Customersearchitem\Api\Data\SearchitemInterfaceFactory;

class Searchitem extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'om_customersearchitem_searchitem';
    protected $searchitemDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param SearchitemInterfaceFactory $searchitemDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \OM\Customersearchitem\Model\ResourceModel\Searchitem $resource
     * @param \OM\Customersearchitem\Model\ResourceModel\Searchitem\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        SearchitemInterfaceFactory $searchitemDataFactory,
        DataObjectHelper $dataObjectHelper,
        \OM\Customersearchitem\Model\ResourceModel\Searchitem $resource,
        \OM\Customersearchitem\Model\ResourceModel\Searchitem\Collection $resourceCollection,
        array $data = []
    ) {
        $this->searchitemDataFactory = $searchitemDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve searchitem model with searchitem data
     * @return SearchitemInterface
     */
    public function getDataModel()
    {
        $searchitemData = $this->getData();
        
        $searchitemDataObject = $this->searchitemDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $searchitemDataObject,
            $searchitemData,
            SearchitemInterface::class
        );
        
        return $searchitemDataObject;
    }
}

