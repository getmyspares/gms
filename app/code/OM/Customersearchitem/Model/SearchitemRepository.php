<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Customersearchitem\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use OM\Customersearchitem\Api\Data\SearchitemInterfaceFactory;
use OM\Customersearchitem\Api\Data\SearchitemSearchResultsInterfaceFactory;
use OM\Customersearchitem\Api\SearchitemRepositoryInterface;
use OM\Customersearchitem\Model\ResourceModel\Searchitem as ResourceSearchitem;
use OM\Customersearchitem\Model\ResourceModel\Searchitem\CollectionFactory as SearchitemCollectionFactory;

class SearchitemRepository implements SearchitemRepositoryInterface
{

    protected $resource;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    protected $searchitemCollectionFactory;

    protected $searchitemFactory;

    private $storeManager;

    protected $dataObjectProcessor;

    protected $dataSearchitemFactory;

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourceSearchitem $resource
     * @param SearchitemFactory $searchitemFactory
     * @param SearchitemInterfaceFactory $dataSearchitemFactory
     * @param SearchitemCollectionFactory $searchitemCollectionFactory
     * @param SearchitemSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceSearchitem $resource,
        SearchitemFactory $searchitemFactory,
        SearchitemInterfaceFactory $dataSearchitemFactory,
        SearchitemCollectionFactory $searchitemCollectionFactory,
        SearchitemSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->searchitemFactory = $searchitemFactory;
        $this->searchitemCollectionFactory = $searchitemCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSearchitemFactory = $dataSearchitemFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \OM\Customersearchitem\Api\Data\SearchitemInterface $searchitem
    ) {
        /* if (empty($searchitem->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $searchitem->setStoreId($storeId);
        } */
        
        $searchitemData = $this->extensibleDataObjectConverter->toNestedArray(
            $searchitem,
            [],
            \OM\Customersearchitem\Api\Data\SearchitemInterface::class
        );
        
        $searchitemModel = $this->searchitemFactory->create()->setData($searchitemData);
        
        try {
            $this->resource->save($searchitemModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the searchitem: %1',
                $exception->getMessage()
            ));
        }
        return $searchitemModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($searchitemId)
    {
        $searchitem = $this->searchitemFactory->create();
        $this->resource->load($searchitem, $searchitemId);
        if (!$searchitem->getId()) {
            throw new NoSuchEntityException(__('searchitem with id "%1" does not exist.', $searchitemId));
        }
        return $searchitem->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->searchitemCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \OM\Customersearchitem\Api\Data\SearchitemInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \OM\Customersearchitem\Api\Data\SearchitemInterface $searchitem
    ) {
        try {
            $searchitemModel = $this->searchitemFactory->create();
            $this->resource->load($searchitemModel, $searchitem->getSearchitemId());
            $this->resource->delete($searchitemModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the searchitem: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($searchitemId)
    {
        return $this->delete($this->get($searchitemId));
    }
}

