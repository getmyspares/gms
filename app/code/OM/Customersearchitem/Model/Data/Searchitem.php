<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Customersearchitem\Model\Data;

use OM\Customersearchitem\Api\Data\SearchitemInterface;

class Searchitem extends \Magento\Framework\Api\AbstractExtensibleObject implements SearchitemInterface
{

    /**
     * Get searchitem_id
     * @return string|null
     */
    public function getSearchitemId()
    {
        return $this->_get(self::SEARCHITEM_ID);
    }

    /**
     * Set searchitem_id
     * @param string $searchitemId
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setSearchitemId($searchitemId)
    {
        return $this->setData(self::SEARCHITEM_ID, $searchitemId);
    }

    /**
     * Get customer_name
     * @return string|null
     */
    public function getCustomerName()
    {
        return $this->_get(self::CUSTOMER_NAME);
    }

    /**
     * Set customer_name
     * @param string $customerName
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCustomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME, $customerName);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OM\Customersearchitem\Api\Data\SearchitemExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \OM\Customersearchitem\Api\Data\SearchitemExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OM\Customersearchitem\Api\Data\SearchitemExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get customer_email
     * @return string|null
     */
    public function getCustomerEmail()
    {
        return $this->_get(self::CUSTOMER_EMAIL);
    }

    /**
     * Set customer_email
     * @param string $customerEmail
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCustomerEmail($customerEmail)
    {
        return $this->setData(self::CUSTOMER_EMAIL, $customerEmail);
    }

    /**
     * Get customer_mobile
     * @return string|null
     */
    public function getCustomerMobile()
    {
        return $this->_get(self::CUSTOMER_MOBILE);
    }

    /**
     * Set customer_mobile
     * @param string $customerMobile
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCustomerMobile($customerMobile)
    {
        return $this->setData(self::CUSTOMER_MOBILE, $customerMobile);
    }

    /**
     * Get search_field
     * @return string|null
     */
    public function getSearchField()
    {
        return $this->_get(self::SEARCH_FIELD);
    }

    /**
     * Set search_field
     * @param string $searchField
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setSearchField($searchField)
    {
        return $this->setData(self::SEARCH_FIELD, $searchField);
    }

    /**
     * Get created_date
     * @return string|null
     */
    public function getCreatedDate()
    {
        return $this->_get(self::CREATED_DATE);
    }

    /**
     * Set created_date
     * @param string $createdDate
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCreatedDate($createdDate)
    {
        return $this->setData(self::CREATED_DATE, $createdDate);
    }

    /**
     * Get is_seller
     * @return string|null
     */
    public function getIsSeller()
    {
        return $this->_get(self::IS_SELLER);
    }

    /**
     * Set is_seller
     * @param string $isSeller
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setIsSeller($isSeller)
    {
        return $this->setData(self::IS_SELLER, $isSeller);
    }
}

