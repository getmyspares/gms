<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Customersearchitem\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface SearchitemRepositoryInterface
{

    /**
     * Save searchitem
     * @param \OM\Customersearchitem\Api\Data\SearchitemInterface $searchitem
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \OM\Customersearchitem\Api\Data\SearchitemInterface $searchitem
    );

    /**
     * Retrieve searchitem
     * @param string $searchitemId
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($searchitemId);

    /**
     * Retrieve searchitem matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \OM\Customersearchitem\Api\Data\SearchitemSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete searchitem
     * @param \OM\Customersearchitem\Api\Data\SearchitemInterface $searchitem
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \OM\Customersearchitem\Api\Data\SearchitemInterface $searchitem
    );

    /**
     * Delete searchitem by ID
     * @param string $searchitemId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($searchitemId);
}

