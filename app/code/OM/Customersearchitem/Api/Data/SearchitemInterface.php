<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Customersearchitem\Api\Data;

interface SearchitemInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMER_NAME = 'customer_name';
    const SEARCHITEM_ID = 'searchitem_id';
    const CUSTOMER_MOBILE = 'customer_mobile';
    const CREATED_DATE = 'created_date';
    const CUSTOMER_EMAIL = 'customer_email';
    const SEARCH_FIELD = 'search_field';
    const IS_SELLER = 'is_seller';

    /**
     * Get searchitem_id
     * @return string|null
     */
    public function getSearchitemId();

    /**
     * Set searchitem_id
     * @param string $searchitemId
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setSearchitemId($searchitemId);

    /**
     * Get customer_name
     * @return string|null
     */
    public function getCustomerName();

    /**
     * Set customer_name
     * @param string $customerName
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCustomerName($customerName);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OM\Customersearchitem\Api\Data\SearchitemExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \OM\Customersearchitem\Api\Data\SearchitemExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OM\Customersearchitem\Api\Data\SearchitemExtensionInterface $extensionAttributes
    );

    /**
     * Get customer_email
     * @return string|null
     */
    public function getCustomerEmail();

    /**
     * Set customer_email
     * @param string $customerEmail
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCustomerEmail($customerEmail);

    /**
     * Get customer_mobile
     * @return string|null
     */
    public function getCustomerMobile();

    /**
     * Set customer_mobile
     * @param string $customerMobile
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCustomerMobile($customerMobile);

    /**
     * Get search_field
     * @return string|null
     */
    public function getSearchField();

    /**
     * Set search_field
     * @param string $searchField
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setSearchField($searchField);

    /**
     * Get created_date
     * @return string|null
     */
    public function getCreatedDate();

    /**
     * Set created_date
     * @param string $createdDate
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setCreatedDate($createdDate);

    /**
     * Get is_seller
     * @return string|null
     */
    public function getIsSeller();

    /**
     * Set is_seller
     * @param string $isSeller
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface
     */
    public function setIsSeller($isSeller);
}

