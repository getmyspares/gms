<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Customersearchitem\Api\Data;

interface SearchitemSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get searchitem list.
     * @return \OM\Customersearchitem\Api\Data\SearchitemInterface[]
     */
    public function getItems();

    /**
     * Set customer_name list.
     * @param \OM\Customersearchitem\Api\Data\SearchitemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

