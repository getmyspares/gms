<?php

namespace OM\OrderRating\Model\ResourceModel;

class OrderRating extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('om_orderrating', 'id');   //here "om_orderrating" is table name and "id" is the primary key of custom table
    }
}