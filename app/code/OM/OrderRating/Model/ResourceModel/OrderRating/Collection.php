<?php

namespace OM\OrderRating\Model\ResourceModel\OrderRating;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'OM\OrderRating\Model\OrderRating',
            'OM\OrderRating\Model\ResourceModel\OrderRating'
        );
    }
}