<?php

namespace OM\OrderRating\Block\Adminhtml;

class Items extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'items';
        $this->_headerText = __('Reviews');
        //$this->_addButtonLabel = __('Add New Item');
        parent::_construct();
    }
}
