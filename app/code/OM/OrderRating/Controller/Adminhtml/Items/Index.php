<?php

namespace OM\OrderRating\Controller\Adminhtml\Items;

class Index extends \OM\OrderRating\Controller\Adminhtml\Items
{
    /**
     * review list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('OM_OrderRating::test');
        $resultPage->getConfig()->getTitle()->prepend(__('Order Reviews & Ratings'));
        $resultPage->addBreadcrumb(__('Order'), __('Order'));
        $resultPage->addBreadcrumb(__('Reviews & Ratings'), __('Reviews & Ratings'));
        return $resultPage;
    }
}