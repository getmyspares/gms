<?php
 
namespace OM\ShowOrderStatusOnHome\Controller\Index;
 
use Magento\Framework\App\Action\Context;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_pageFactory;
 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory,\Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $resultPage = $this->_pageFactory->create();
            $block = $resultPage->getLayout()
                ->createBlock('OM\ShowOrderStatusOnHome\Block\Display')
                ->setTemplate('OM_ShowOrderStatusOnHome::orders.phtml')
                ->toHtml();
               return $this->getResponse()->setBody($block);
    }
}