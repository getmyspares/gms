<?php
namespace OM\ShowOrderStatusOnHome\Block;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
class Display extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
	private $orderCollectionFactory;
	protected $_scopeConfig;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		CollectionFactory $orderCollectionFactory)
	{
		$this->_customerSession = $customerSession;
		$this->orderCollectionFactory = $orderCollectionFactory;
		$this->_scopeConfig = $scopeConfig; 
		parent::__construct($context);
	}

	public function getCustomerId()
	{
	    return $this->_customerSession->getCustomer()->getId();
	    
	}
	public function getOrderCollectionByCustomerId($customerId)
   	{
       $collection = $this->orderCollectionFactory->create($customerId)
         ->addFieldToSelect('*')
         ->addAttributeToFilter('status', array('in' => array('complete','processing','New','Delivered')))
         ->setOrder(
                'created_at',
                'desc'
            )
         ->setPageSize(5);
 
     return $collection;

    }

    public function getOrderBanner(){
    	return $this->_scopeConfig->getValue("alternatebannerfororder/desktop/image");
	}
	public function checkOrderBanner(){
    	return $this->_scopeConfig->getValue("alternatebannerfororder/desktop/enable");
	}
	public function getOrderBannerUrl(){
    	return $this->_scopeConfig->getValue("alternatebannerfororder/desktop/url");
	}
}