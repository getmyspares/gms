<?php
namespace OM\HomepageTopProducts\Block;

use Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory as BestSellersCollectionFactory;


class Topproducts extends \Magento\Framework\View\Element\Template
{    
    protected $_productCollectionFactory;
    protected $_imageBuilder;
    protected $_optionFactory;
    protected $_productRepository;
    protected $_productImageHelper;
    protected $productVisibility;
    protected $_categoryFactory;
    protected $_storeManager;
    protected $_urlInterface;
    protected $_currency;
    protected $_scopeConfig;
        
    public function __construct(
        BestSellersCollectionFactory $bestSellersCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $_imageBuilder,
        \Magento\Catalog\Model\Product\OptionFactory $_optionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Helper\Image $productImageHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\UrlInterface $urlInterface, 
        \OM\HomepageTopProducts\Helper\Data $customHelperData,
        \OM\CheckCustomerSession\Helper\Data $customSessionHelperData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        array $data = []
    )
    {    
        $this->_bestSellersCollectionFactory = $bestSellersCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_imageBuilder = $_imageBuilder;
        $this->_optionFactory = $_optionFactory;
        $this->_productRepository = $productRepository;
        $this->_productImageHelper = $productImageHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig; 
        $this->_currency = $currency; 
        $this->_helperData = $customHelperData;
        $this->_sessionHelperData = $customSessionHelperData;
        $this->_resource = $resource;
        $this->_urlInterface = $urlInterface;
        $this->cookieManager = $cookieManager;
        $this->_productloader = $_productloader;
        parent::__construct($context, $data);
    }


    public function getCategoryId()
    {
        $login_customer =  $this->getLayout()->createBlock('Magento\Customer\Block\Account\Customer');
        $categoryIdsArray = $this->_helperData->getTopSaleCategories();
        $areaOfInterestArray = explode(",",$this->_sessionHelperData->getAreaOfInterest());
        $catId = array();
        if(count($areaOfInterestArray)){
            foreach($areaOfInterestArray as $interest){
                if($interest == 'Air conditioner'){
                   
                    $catId[]['category_id'] = '38';
                }
                if($interest == 'Camera'){
                    $catId[]['category_id'] = '10';
                }
                if($interest == 'Microwave Oven'){
                    $catId[]['category_id'] = '41';
                }
                if($interest == 'Mobile Phones'){
                    $catId[]['category_id'] = '8';
                }
                if($interest == 'Refrigerator'){
                    $catId[]['category_id'] = '50';
                }
                if($interest == 'Tools & Accessories'){
                    $catId[]['category_id'] = '4';
                }
                if($interest == 'Universal Parts'){
                    $catId[]['category_id'] = '7';
                }
                if($interest == 'Washing Machine'){
                    $catId[]['category_id'] = '55';
                }
            }
        }
        if($login_customer->customerLoggedIn()){
            
            if(count($catId)){
                $categoryIdsArray = array_unique(array_merge ($catId, $categoryIdsArray),SORT_REGULAR); 
                $categoryIdsArray = array_slice($categoryIdsArray, 0, 4, true);
            }
            else{
                if(!$this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts()){
                    $categoryIdsArray = array_slice($categoryIdsArray, 0, 4, true);
                } 
            } 
           if($this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts()){
                $recentcategoryIdsArray = $this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts();
                if(count($catId) ){
                    $categoryIdsArray = array_unique(array_merge ($recentcategoryIdsArray, $categoryIdsArray),SORT_REGULAR);
                    $categoryIdsArray = array_unique(array_merge ($categoryIdsArray,$catId),SORT_REGULAR);
                    $categoryIdsArray = array_slice($categoryIdsArray, 0, 4, true);
                }
                else{
                if(count($recentcategoryIdsArray)){
                    $categoryIdsArray = array_unique(array_merge ($recentcategoryIdsArray, $categoryIdsArray),SORT_REGULAR);
                    $categoryIdsArray = array_slice($categoryIdsArray, 0, 4, true);
                }
                }
          
           }
        }
        else{
            if($this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts()){
                $recentcategoryIdsArray = $this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts();
                if(count($recentcategoryIdsArray) < 8){
                    $categoryIdsArray = array_unique(array_merge ($recentcategoryIdsArray, $categoryIdsArray),SORT_REGULAR);
                    $categoryIdsArray = array_slice($categoryIdsArray, 0, 7, true);
                }
               
            }
        }
        
        foreach ($categoryIdsArray as $categoryIds) {
            $categoryid[] =  $categoryIds;
        }
        return $categoryid;
    }


    public function getCategoryNames()
    {
        $categoryIdsArray = $this->getCategoryId();
        
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryName[] =   $category->getName();
        }

        return $categoryName;
    }

    public function getCategoryUrl()
    {
        $categoryIdsArray = $this->getCategoryId();
    
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryUrl[] =   $category->getUrl();
        }
        return $categoryUrl;
    }

    public function getCategoryIcon()
    {
        $categoryIdsArray = $this->getCategoryId();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryIcon[] =   $category->getCategoryIcon();
        }

        return $categoryIcon;
    }

   
     public function getProductCollection($id)
    {
        $childCategoryId = array();
        $productIdsArrays = array();
        $combinedIds = array();
        $randonProductIds = array();
        $combinedProductIdsArrays = array();
        $childIds = $id;
        $categoryId = $id;
        $productIdsOne = array();
        $productIdsTwo = array();
        $childCategoryHelper = $this->_helperData->getChildCategory($id);
        $childrenCategories = $childCategoryHelper->getChildrenCategories();
        foreach($childrenCategories as $child){
            $childCategoryId[] = $child->getId();
        }
        if(count($childCategoryId)){
            $childIds = implode(",",$childCategoryId);
        }
        $connection = $this->_resource->getConnection();
        if($id != '8' && $id != '7'){
            $query ="SELECT sum(sales_order_item.qty_ordered) as qty_ordered,sales_order_item.product_id FROM sales_order_item inner join catalog_category_product on sales_order_item.product_id = catalog_category_product.product_id WHERE catalog_category_product.category_id in($id,$childIds) and catalog_category_product.product_id in(SELECT cataloginventory_stock_item.product_id FROM `cataloginventory_stock_item` where cataloginventory_stock_item.is_in_stock = 1 and cataloginventory_stock_item.product_id in(SELECT catalog_product_entity_int.entity_id FROM `catalog_product_entity_int` WHERE catalog_product_entity_int.attribute_id = (SELECT eav_attribute.attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1)) GROUP BY sales_order_item.product_id ORDER BY qty_ordered DESC limit 10";
        }
        else{
            $query = "SELECT sum(sales_order_item.qty_ordered) as qty_ordered,sales_order_item.product_id FROM sales_order_item inner join catalog_category_product on sales_order_item.product_id = catalog_category_product.product_id WHERE catalog_category_product.category_id = $id and catalog_category_product.product_id in(SELECT cataloginventory_stock_item.product_id FROM `cataloginventory_stock_item` where cataloginventory_stock_item.is_in_stock = 1 and cataloginventory_stock_item.product_id in(SELECT catalog_product_entity_int.entity_id FROM `catalog_product_entity_int` WHERE catalog_product_entity_int.attribute_id = (SELECT eav_attribute.attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1)) GROUP BY sales_order_item.product_id ORDER BY qty_ordered DESC limit 10";
        }
        $productIdArray = $connection->fetchAll($query);
        if(count($productIdArray) <= 10 ){
        foreach($productIdArray as $productId){
            $productIdsOne[] = $productId['product_id'];
        }
        }
        $deviceId=$this->cookieManager->getCookie('device_ids');
        if($deviceId){
        $viewQuery ="SELECT count(product_id) as product_id_count,product_id FROM recently_viewed_data WHERE category_id = $id and device_id = $deviceId GROUP BY product_id ORDER BY product_id_count DESC LIMIT 10";
        $productIdArray = $connection->fetchAll($viewQuery);
        foreach($productIdArray as $productId){
            $productIdsTwo[] = $productId['product_id'];
        }
        }
        $ids = array_unique(array_merge ($productIdsTwo, $productIdsOne),SORT_REGULAR);
        if(count($ids)< 10){
            if($id != '8' && $id != '7'){
                $query ="SELECT product_id FROM `catalog_category_product` WHERE `category_id` in ($id,$childIds) and product_id in(SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1))";
            }
            else{
                $query = "SELECT product_id FROM `catalog_category_product` WHERE `category_id` in ($id) and product_id in(SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1))";
            }
            $productIdsArray = $connection->fetchAll($query);
            foreach($productIdsArray as $productIdsArray){
                $productIdsArrays[] = $productIdsArray['product_id'];
            }
            $ids = array_unique(array_merge ($ids, $productIdsArrays),SORT_REGULAR);
        }
        if(count($ids) < 10){
            $sql = "SELECT parent_id FROM `catalog_category_entity` WHERE `entity_id` = $id";
            $ParentId = $connection->fetchOne($sql);
            $sql = "SELECT entity_id FROM `catalog_category_entity` WHERE `parent_id` = $ParentId limit 1"; 
            $entityId = $connection->fetchOne($sql);  
            $sql = "SELECT entity_id  FROM `catalog_category_entity` WHERE `parent_id` = $entityId";
            $entityId1 = $connection->fetchOne($sql);
            if($entityId1){  
                $sql = "SELECT product_id FROM `cataloginventory_stock_item` where product_id in (SELECT product_id FROM `catalog_category_product` where category_id in($entityId1)) and is_in_stock = 1";
                $randonProductIds = $connection->fetchAll($sql);
            }
            else{
                $sql = "SELECT product_id FROM `cataloginventory_stock_item` where product_id in (SELECT product_id FROM `catalog_category_product` where category_id in($entityId)) and is_in_stock = 1";
                $randonProductIds = $connection->fetchAll($sql);
            }
        }
        foreach($randonProductIds as $randonProductIds){
            $combinedProductIdsArrays[] = $randonProductIds['product_id'];
         }
        $ids1 = array_unique(array_merge ($ids, $combinedProductIdsArrays),SORT_REGULAR);
        $category = $this->_categoryFactory->create()->load($categoryId);
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*')->setFlag('has_stock_status_filter', false)->joinField('stock_item', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id', 'is_in_stock=1');
        //$collection->addCategoryFilter($category);   
        if(count($ids1)){
            $collection->addFieldToFilter('entity_id', ['in' => $ids1]);
            $collection->getSelect()->order(new \Zend_Db_Expr("FIELD(e.entity_id, ".implode(",", $ids1).")"));
        }
        $collection->addAttributeToFilter('visibility',4);
        $collection->setPageSize(10);
        return $collection;

    }

    public function getBrowsingProductsHeading(){
        return $this->_scopeConfig->getValue("hometop/general/suggested_product_heading");

    }

    public function getBrowsingProducts(){
        $productSkus= $this->_scopeConfig->getValue("hometop/general/display_product");
        $productSkuArray = explode(",", $productSkus);

        foreach ($productSkuArray as $productSku) {
            $productCollection[] =$this->_productRepository->get($productSku);
        }
        return $productCollection;

    }

    public function getLatestDealsProducts(){
        $productSkus= $this->_scopeConfig->getValue("hometop/general/latest_deal");
        $productSkuArray1 = explode(",", $productSkus);
        if($this->_helperData->getLatestDealsProductIds()){
            $productSkuArray = $this->_helperData->getLatestDealsProductIds();
            
            foreach ($productSkuArray as $productSku) {
                $productCollection[] =$this->_productRepository->getById($productSku['product_id']);
            }
        }
        else{
            foreach ($productSkuArray1 as $productSku) {
                $productCollection[] = $this->_productRepository->getById($productSku);
            } 
        }
        
        return $productCollection;

    }

    public function getLatestDealsProductsHeading(){
       return $this->_scopeConfig->getValue("hometop/general/latest_deal_heading");

    }

    public function getDailyDealsProducts(){
        $productSkus= $this->_scopeConfig->getValue("daliydeals/productdeal/daily_deal");
        $productSkuArray = explode(",", $productSkus);

        foreach ($productSkuArray as $productSku) {
            $productCollection[] =$this->_productRepository->get($productSku);
        }
        return $productCollection;

    }
    public function getDailyDealsProductsHeading(){
     return $this->_scopeConfig->getValue("daliydeals/productdeal/daily_deal_heading");

    }
    

    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

    public function getAddToCartUrl($product)
    {
        $params = [
            'product' => $product->getId(),
            '_secure' => $this->getRequest()->isSecure()
        ];

        return $this->getUrl('checkout/cart/add', $params);
    }

    public function getOptions($product)
    {
        return $this->_optionFactory->create()->getProductOptionCollection($product);
    }

    public function getSiteUrl()
    {
            return $this->_urlInterface->getBaseUrl();
    }

    public function getProductById($product)
    {
        return $product->getId();
    }

     public function getCurrentCurrencySymbol()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();;
    }

    public function getTrendingCategory(){
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->_helperData->getTopSaleCategories();
        if($this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts()){
            $recentcategoryIdsArray = $this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts();
            if(count($recentcategoryIdsArray) < 8){
                $categoryIdsArray = array_unique(array_merge ($recentcategoryIdsArray, $categoryIdsArray),SORT_REGULAR);
                $categoryIdsArray = array_slice($categoryIdsArray, 0, 7, true);
            }
        }
        foreach($categoryIdsArray as $catArray){
            $categoryIdsArrays[] = $catArray;
        }
        return $categoryIdsArrays;
    }

    public function getTrendingCategoryNames()
    {
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->getTrendingCategory();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryName[] =   $category->getName();
        }

        return $categoryName;
    }

    public function getTrendingCategoryIcon()
    {
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->getTrendingCategory();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryIcon[] =   $category->getCategoryIcon();
        }

        return $categoryIcon;
    }

    public function getTrendingCategoryUrls()
    {
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->getTrendingCategory();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryUrl[] =   $category->getUrl();
        }

        return $categoryUrl;
    }

    public function getMobileTrendingCategory(){
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/mobile_cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->_helperData->getTopSaleCategories();
        if($this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts()){
            $recentcategoryIdsArray = $this->_helperData->getTopSaleCategoriesBasedOnRecentViewdProducts();
            if(count($recentcategoryIdsArray) < 8){
                $categoryIdsArray = array_unique(array_merge ($recentcategoryIdsArray, $categoryIdsArray),SORT_REGULAR);
                $categoryIdsArray = array_slice($categoryIdsArray, 0, 7, true);
            }
        }
        foreach ($categoryIdsArray as $categoryIds) {
            $categoryIdsArray[] =  $categoryIds;
        }            
        return $categoryIdsArray;
    }

    public function getMobileTrendingCategoryNames()
    {
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/mobile_cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->getMobileTrendingCategory();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryName[] =   $category->getName();
        }

        return $categoryName;
    }

    public function getMobileTrendingCategoryIcon()
    {
        //$categoryIds= $this->_scopeConfig->getValue("trendingcategories/category/mobile_cat_id");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->getMobileTrendingCategory();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryIcon[] =   $category->getCategoryIcon();
        }

        return $categoryIcon;
    }

    public function getTrendingCategoryUrl()
    {
        //$categoryIds= $this->_scopeConfig->getValue("hometop/general/display_text");
        //$categoryIdsArray = explode(",", $categoryIds);
        $categoryIdsArray = $this->getMobileTrendingCategory();
        foreach ($categoryIdsArray as $categoryIdsArray) {
            $category = $this->_categoryFactory->create()->load($categoryIdsArray['category_id']);
            $categoryUrl[] =   $category->getUrl();
        }

        return $categoryUrl;
    }

    public function getTopRateProducts($limit = 12)
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        $collection->joinField(
            'rating_summary',
            'review_entity_summary',
            'rating_summary',
            'entity_pk_value = entity_id',
            ['entity_type' => 1, 'store_id' => 1],
            'left'
        );
        $collection->addAttributeToFilter('rating_summary', ['neq' => null]);
        $collection->addAttributeToSort('rating_summary', 'desc');
        $collection->getSelect()->limit($limit);

        return $collection;
    }
    public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
    
    public function getCustomerViewedProductCollection()
    {
        $productIds = array();
        $productIds = $this->_helperData->getCustomerAlsoViewedProducts();
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
        $collection->setPageSize(15);
        return $collection;
    }

}
?>