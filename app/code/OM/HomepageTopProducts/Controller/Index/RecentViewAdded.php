<?php
namespace OM\HomepageTopProducts\Controller\Index;

class RecentViewAdded extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
        $this->_resource = $resource;
		return parent::__construct($context);
	}

	public function execute()
	{
		$connection = $this->_resource->getConnection();
        $post = $this->getRequest()->getPostValue();
        if($post){
        $customerId = $post['customer_id'];
        $productId = $post['product_id'];
        $deviceId = $post['device_id'];
        $categoryId = $post['category_id'];
        $childCategoryId = $post['child_category_id'];
        $tableName = 'recently_viewed_data';
        $sql = "Insert Into " . $tableName . " (customer_id,product_id,device_id,category_id,child_category_id) Values ($customerId,$productId,$deviceId,$categoryId,$childCategoryId)";
		$connection->query($sql);
        }
        else{
            echo 'no data';
        }
	}
}