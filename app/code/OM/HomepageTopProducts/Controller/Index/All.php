<?php
namespace OM\HomepageTopProducts\Controller\Index;

class All extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		
			$resultPage = $this->_pageFactory->create();
			$block = $resultPage->getLayout()
                ->createBlock('OM\HomepageTopProducts\Block\Topproducts')
                ->setTemplate('OM_HomepageTopProducts::All.phtml')
                ->toHtml();
               return $this->getResponse()->setBody($block);
		
	}
}