<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OM\HomepageTopProducts\Helper;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use Magento\Catalog\Model\CategoryFactory;
class Data extends \Magento\Framework\App\Helper\AbstractHelper {
  protected $galleryReadHandler;
    /**
     * Catalog Image Helper
     *
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;
    public function __construct(
    GalleryReadHandler $galleryReadHandler,
    \Magento\Framework\App\Helper\Context $context,
    \Magento\Catalog\Helper\Image $imageHelper,
    \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
    CategoryFactory $categoryFactory,
    \OM\CheckCustomerSession\Helper\Data $customSessionHelperData,
    \Magento\Framework\Registry $registry,
    \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->imageHelper = $imageHelper;
        $this->cookieManager = $cookieManager;
        $this->galleryReadHandler = $galleryReadHandler;
        $this->_resource = $resource;
        $this->categoryFactory = $categoryFactory;
        $this->_registry = $registry;
        $this->_sessionHelperData = $customSessionHelperData;
        parent::__construct($context);
    }
   /** Add image gallery to $product */
    public function addGallery($product) {
        $this->galleryReadHandler->execute($product);
    }
    public function getGalleryImages(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        $images = $product->getMediaGalleryImages();
        $width = 278;
        $height = 208;
        if ($images instanceof \Magento\Framework\Data\Collection) {
            foreach ($images as $image) {
                /** @var $image \Magento\Catalog\Model\Product\Image */
                $image->setData(
                    'small_image_url',
                    $this->imageHelper->init($product, 'product_page_image_small')
                        ->setImageFile($image->getFile())
                        ->resize($width, $height)
                        ->getUrl()
                );
                $image->setData(
                    'medium_image_url',
                    $this->imageHelper->init($product, 'product_page_image_medium')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->resize($width, $height)
                        ->getUrl()
                );
                $image->setData(
                    'large_image_url',
                    $this->imageHelper->init($product, 'product_page_image_large')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->resize($width, $height)
                        ->getUrl()
                );
            }
        }
        return $images;
    }

    public function getTopSaleCategories()
    {
        $connection = $this->_resource->getConnection();
        $sql = "SELECT catalog_category_product.category_id FROM sales_order_item inner join catalog_category_product on sales_order_item.product_id = catalog_category_product.product_id WHERE catalog_category_product.category_id in('7','8','9','10','12','13','14','38','39','40','41','50','53','55','91','58','62','67','71','73','75','77','79','82','84','86','32','33','34','88','100','101','102','103','104','105','106','107','108','109','110','111') and created_at >= now() - interval 3 MONTH GROUP BY catalog_category_product.category_id ORDER BY sum(sales_order_item.qty_ordered) DESC LIMIT 7";
        return $connection->fetchAll($sql);
    }

    public function getTopSaleCategoriesBasedOnRecentViewdProducts()
    {
        $deviceId=$this->cookieManager->getCookie('device_ids');
        //$customerId=$this->cookieManager->getCookie('customer_id');
        $customerId = $this->_sessionHelperData->getCustomerId();
        $connection = $this->_resource->getConnection();
        if($deviceId){
        if($customerId){
            $sql = "SELECT category_id FROM `recently_viewed_data` WHERE `customer_id` = '$customerId' OR `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 7";
        }
        else{   
            $sql = "SELECT category_id FROM `recently_viewed_data` WHERE `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 7";
        }
         return $connection->fetchAll($sql);
        }
    }

    public function getTopSectionProductIds(){
        
        $connection = $this->_resource->getConnection();  
        $recentViewdArray = $this->getTopSaleCategoriesBasedOnRecentViewdProducts();
        $deviceId=$this->cookieManager->getCookie('device_ids');
        //$customerId=$this->cookieManager->getCookie('customer_id');
        $customerId = $this->_sessionHelperData->getCustomerId();
        $childCategoryId = array();
        if($deviceId){
            if($customerId){
                $sql = "SELECT category_id FROM `recently_viewed_data` WHERE `customer_id` = '$customerId' OR `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 1";
            }
            else{    
                $sql = "SELECT category_id FROM `recently_viewed_data` WHERE `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 1";
            }
            $catsId = $connection->fetchOne($sql);
            $childCategory = $this->getChildCategory($catsId);
            $childrenCategories = $childCategory->getChildrenCategories();
            foreach($childrenCategories as $child){
                $childCategoryId[] = $child->getId();
            }
            if(count($childCategoryId)){
                $childIds = implode(",",$childCategoryId);
            }
            if($catsId){
                $childIds = $catsId;
                $sql ="SELECT catalog_category_product.product_id FROM sales_order_item inner join catalog_category_product on sales_order_item.product_id = catalog_category_product.product_id WHERE catalog_category_product.category_id in($catsId) and created_at >= now() - interval 3 MONTH GROUP BY catalog_category_product.product_id ORDER BY sum(sales_order_item.qty_ordered) DESC LIMIT 3";
                $catArraySecond = $connection->fetchAll($sql); 
                if(count($catArraySecond) < 3){
                    if($catsId != '8' && $catsId != '7'){
                        $sql ="SELECT product_id FROM `catalog_category_product` WHERE `category_id` in($catsId,$childIds) limit 3";  
                    }else{
                        $sql ="SELECT product_id FROM `catalog_category_product` WHERE `category_id` = $catsId limit 3";
                    }
                    $catArraySecond = $connection->fetchAll($sql);
                }
                
            }
        }
        if($recentViewdArray){ 
            if($customerId){
                $sql = "SELECT child_category_id FROM `recently_viewed_data` WHERE `customer_id` = '$customerId' OR `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(child_category_id) DESC LIMIT 1";
            }
            else{    
                $sql = "SELECT child_category_id FROM `recently_viewed_data` WHERE `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(child_category_id) DESC LIMIT 1";
            }   
            $catsIdChild = $connection->fetchOne($sql);
            if($catsIdChild){     
                $sql = "SELECT category_id FROM `catalog_category_product` where product_id in(SELECT entity_id FROM `catalog_product_entity_decimal` WHERE `attribute_id` = 78 AND `value` IS NOT NULL) and category_id in($catsIdChild,$catsId)";
            }
            else{
                $sql = "SELECT category_id FROM `catalog_category_product` where product_id in(SELECT entity_id FROM `catalog_product_entity_decimal` WHERE `attribute_id` = 78 AND `value` IS NOT NULL) and category_id in($catsId)";
            }
        $specialProductPriceArray = $connection->fetchAll($sql);
        if(count($specialProductPriceArray)){
            foreach($specialProductPriceArray as $catIds){
                $categoryIdsArray[]= $catIds['category_id'];
            }
            $ids = implode(', ', $categoryIdsArray);
            $sql ="SELECT product_id FROM `catalog_category_product` where category_id in ($ids) and product_id in(SELECT entity_id FROM `catalog_product_entity_decimal` WHERE `attribute_id` = 78 AND `value` IS NOT NULL)";
            $catArrayFirst = $connection->fetchAll($sql);
            if(count($catArrayFirst) < 3){
                $productIdsArray = array_unique(array_merge ($catArrayFirst, $catArraySecond),SORT_REGULAR);
                $productViewdIds = array_slice($productIdsArray, 0, 3, true);
               foreach($productViewdIds as $pViwed){
                   $productViewd[] = $pViwed;
               }
               return $productViewd;
            }
            else{
                return $catArrayFirst;
            }
            
        }
        else{
            return $catArraySecond;
        }
            
    }
    }
    public function getLatestDealsProductIds(){
        $connection = $this->_resource->getConnection();  
        $deviceId=$this->cookieManager->getCookie('device_ids');
        //$customerId=$this->cookieManager->getCookie('customer_id');
        $customerId = $this->_sessionHelperData->getCustomerId();
        $catArraySecond = array();
        $categoryspecialProductPriceArray = array();
        $specialofferMergedDataArray = array();
        if($deviceId){
            if($customerId){
                $sql = "SELECT category_id FROM `recently_viewed_data` WHERE `customer_id` = '$customerId' OR `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 1";
                $sqlChild = "SELECT child_category_id FROM `recently_viewed_data` WHERE `customer_id` = '$customerId' OR `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 1";
            }
            else{  
                $sql = "SELECT category_id FROM `recently_viewed_data` WHERE `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 1";
                $sqlChild = "SELECT child_category_id FROM `recently_viewed_data` WHERE `device_id` = '$deviceId' GROUP BY category_id ORDER BY count(category_id) DESC LIMIT 1";
            }
            $catsId = $connection->fetchOne($sql);
            $ChildCatId = $connection->fetchOne($sqlChild);
            if($ChildCatId){
                if($catsId != '8' && $catsId != '7'){
                $sql = "SELECT cpeid1.entity_id as product_id, cpeid1.value as price, cpeid2.value as special_price, (100-(cpeid2.value/cpeid1.value)*100) AS price_percent FROM catalog_product_entity_decimal cpeid1 LEFT JOIN catalog_product_entity_decimal cpeid2 ON (cpeid1.entity_id = cpeid2.entity_id AND cpeid2.attribute_id = 78) WHERE cpeid1.attribute_id = 77 AND cpeid2.value IS NOT NULL and cpeid1.entity_id in(SELECT product_id FROM `catalog_category_product` WHERE `category_id` in ($catsId,$ChildCatId) and product_id in(SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1))) order by price_percent desc limit 5";
                }
                else{
                    $sql = "SELECT cpeid1.entity_id as product_id, cpeid1.value as price, cpeid2.value as special_price, (100-(cpeid2.value/cpeid1.value)*100) AS price_percent FROM catalog_product_entity_decimal cpeid1 LEFT JOIN catalog_product_entity_decimal cpeid2 ON (cpeid1.entity_id = cpeid2.entity_id AND cpeid2.attribute_id = 78) WHERE cpeid1.attribute_id = 77 AND cpeid2.value IS NOT NULL and cpeid1.entity_id in(SELECT product_id FROM `catalog_category_product` WHERE `category_id` in ($ChildCatId) and product_id in(SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1))) order by price_percent desc limit 5";
                }
                $categoryspecialProductPriceArray = $connection->fetchAll($sql);
            }

            if($catsId){
                $sql ="SELECT catalog_category_product.product_id FROM sales_order_item inner join catalog_category_product on sales_order_item.product_id = catalog_category_product.product_id WHERE catalog_category_product.category_id in($catsId) and created_at >= now() - interval 3 MONTH GROUP BY catalog_category_product.product_id ORDER BY sum(sales_order_item.qty_ordered) DESC LIMIT 5";
                $catArraySecond = $connection->fetchAll($sql);
            }
            else{
                $query = "SELECT catalog_category_product.product_id FROM sales_order_item inner join catalog_category_product on sales_order_item.product_id = catalog_category_product.product_id WHERE created_at >= now() - interval 3 MONTH GROUP BY catalog_category_product.product_id ORDER BY sum(sales_order_item.qty_ordered) DESC LIMIT 5";
                $catArraySecond = $connection->fetchAll($query);
            }
        }
            
            $sql = "SELECT cpeid1.entity_id as product_id, cpeid1.value as price, cpeid2.value as special_price, (100-(cpeid2.value/cpeid1.value)*100) AS price_percent FROM catalog_product_entity_decimal cpeid1 LEFT JOIN catalog_product_entity_decimal cpeid2 ON (cpeid1.entity_id = cpeid2.entity_id AND cpeid2.attribute_id = 78) WHERE cpeid1.attribute_id = 77 AND cpeid2.value IS NOT NULL and cpeid1.entity_id in(SELECT product_id FROM `catalog_category_product` WHERE product_id in(SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1))) order by price_percent desc limit 5";

            $specialProductPriceArray = $connection->fetchAll($sql);

            if(count($categoryspecialProductPriceArray) < 5){
                $specialofferMerged = array_unique(array_merge ($categoryspecialProductPriceArray, $specialProductPriceArray),SORT_REGULAR);
                foreach($specialofferMerged as $specialofferMergedData){
                    $specialofferMergedDataArray[] =  $specialofferMergedData;
                }
                $specialofferMergedDataArray = array_slice($specialofferMergedDataArray,0,5);
                if(count($specialofferMergedDataArray)){
                    if(count($specialofferMergedDataArray) < 5){
                        $productIdsArray = array_unique(array_merge ($specialofferMergedDataArray, $catArraySecond),SORT_REGULAR);        
                        foreach($productIdsArray as $combinedData){
                            $catCombinedIds[] =  $combinedData;
                        }
                        return $catCombinedIds;
                    }
                    else{
                        return $specialofferMergedDataArray;
                    }
                    
                }
                else{
                    return $catArraySecond;
                }
            }
            else{
                return $categoryspecialProductPriceArray;
            }
           
    }

    public function getCategory($categoryId)
    {
        $this->category = $this->categoryFactory->create();
        $this->category->load($categoryId);
        return $this->category;
    }
    public function getChildCategory($categoryId)
    {
        $this->category = $this->categoryFactory->create();
        $this->category->load($categoryId);
        return $this->category;
    }

    public function getCustomerAlsoViewedProducts(){
        $currentProduct = $this->_registry->registry('current_product');
        $viewedProductCollection = array();
        $viewedProductsCustomerArray = array();
        if($currentProduct){
            $productId = $currentProduct->getId();
        }
        $deviceId=$this->cookieManager->getCookie('device_ids');
        $customerId = $this->_sessionHelperData->getCustomerId();
        $connection = $this->_resource->getConnection();  
        if(!$currentProduct){
        if($deviceId){
            if($customerId){
                $sql ="SELECT product_id FROM `recently_viewed_data` where customer_id = '$customerId' OR `device_id` = '$deviceId' ORDER BY `recently_viewed_data`.`id` DESC limit 1";
            }
            else{    
                $sql ="SELECT product_id FROM `recently_viewed_data` where device_id = '$deviceId' ORDER BY `recently_viewed_data`.`id` DESC limit 1";
            }
            $productId = $connection->fetchOne($sql);
        }
        }
        
        $sql = "SELECT DISTINCT product_id FROM `sales_order_item` where order_id in(SELECT order_id FROM `sales_order_item` WHERE `product_id` = $productId) and product_id != $productId  and product_id in(SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1))";
        $viewedProductCollection = $connection->fetchAll($sql);
        if(empty($viewedProductCollection) || count($viewedProductCollection) < 5){
            //$sql ="SELECT product_id FROM `sales_order_item` where product_id in (SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1)) GROUP BY product_id order by sum(qty_ordered) DESC limit 10";
            if($currentProduct){
            $currentProductcatId = $currentProduct->getCategoryIds();
            $currentProductcatIds = implode(",",$currentProductcatId);
            $sql ="SELECT product_id FROM `catalog_category_product` WHERE `category_id` in($currentProductcatIds)  and product_id in (SELECT product_id FROM `cataloginventory_stock_item` where is_in_stock = 1 and product_id in(SELECT entity_id FROM `catalog_product_entity_int`WHERE attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status') AND `catalog_product_entity_int`.value = 1)) limit 10";  
            $randomViewedProductCollection = $connection->fetchAll($sql);
            $viewedProductsCustomer = array_unique(array_merge ($viewedProductCollection, $randomViewedProductCollection),SORT_REGULAR);
                foreach($viewedProductsCustomer as $viewedProductsCustomer){
                    $viewedProductsCustomerArray[] =  $viewedProductsCustomer;
                }
                return $viewedProductsCustomerArray;
            }
        } 
        else{
            return $viewedProductCollection;
        }   

    }

    public function getCurrentCategory(){
        return $this->_registry->registry('current_category');
    }
}
 