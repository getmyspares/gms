<?php
namespace OM\MobilePopup\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $userAgent = $objectManager->create('Magento\Framework\HTTP\Header')->getHttpUserAgent();
        $server = $this->getRequest()->getServer();
        $isMobileDevice = \Zend_Http_UserAgent_Mobile::match($userAgent, $server);
        $conf = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');

        $desktopEnabled = $conf->getValue('homepagepopup/desktop/enable');
        $data = [];
        $device = 'desktop';
        if($isMobileDevice)
            $device = 'mobile';
        $enabled = $conf->getValue('homepagepopup/'.$device.'/enable');
        if($enabled) {
            $url = $conf->getValue('homepagepopup/'.$device.'/url');
            $image = $conf->getValue('homepagepopup/'.$device.'/image');
            $data = [
                    "enabled" => true,
                    "device" => $device,
                    "url" => $url,
                    "imgpath" => "pub/media/homepopup/".$device."/".$image,
                ];
        } else {
            $data = [
                    "enabled" => false
                ];
        }
        echo json_encode($data);
    }
}
