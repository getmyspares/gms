<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Pincodes\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $_httpClientFactory;
	protected $httpClient;
    protected $request;
    protected $_pincode;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory ,
		\Zend\Http\Client $httpClient ,
        \Magento\Framework\App\Request\Http $request,
        \OM\Pincodes\Model\Pincodes $pincode,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->_pincode = $pincode;
        $this->_httpClientFactory = $httpClientFactory;
        $this->request = $request;
		$this->httpClient = $httpClient;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
			$pincode = $this->request->getParam('pincode');
			$pcollection = $this->_pincode->getCollection()
							->addFieldToFilter('pincode',array('eq'=>$pincode))
							->getData();
			$datas = array();
			foreach($pcollection as $pcodes){
				$datas['is_active'] = $pcodes['is_active'];
				$datas['pincode']   = $pcodes['pincode'];
				$datas['msg']       = $pcodes['msg'];
			}	 
			
            return $this->jsonResponse($datas);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }
    
   public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
    
	
}
