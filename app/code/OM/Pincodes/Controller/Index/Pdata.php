<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Pincodes\Controller\Index;

class Pdata extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $_httpClientFactory;
	protected $httpClient;
    protected $request;
    protected $_pincode;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory ,
		\Zend\Http\Client $httpClient ,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Request\Http $request,
        \OM\Pincodes\Model\Pincodes $pincode,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->_pincode = $pincode;
        $this->_httpClientFactory = $httpClientFactory;
        $this->request = $request;
		$this->httpClient = $httpClient;
		$this->connection = $resourceConnection->getConnection();
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
			$arr = array(				
				'BH' => 'BR',
				'PD' => 'PY',
				'OD' => 'OR',
				'TS' => 'TG',
                'CG' => 'CT',  
                'UK' => 'UT',
                'DH' => 'DN'
			);
			$pincode = $this->request->getParam('pincode');
			$pcollection = $this->_pincode->getCollection()
							->addFieldToFilter('pincode',array('eq'=>$pincode))
							->getData();
			$datas = array();
			foreach($pcollection as $pcodes){
				$datas['city_name'] = $pcodes['city_name'];
				$state = $pcodes['state'];
				$scode = array_key_exists($state,$arr) ? $arr[$state] : $state;				
				$st = $this->connection->fetchOne("SELECT region_id FROM `directory_country_region` WHERE country_id='IN' AND code='".$scode."'");				 
				$datas['state']     = $st;				
			}	 
			
            return $this->jsonResponse($datas);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }
    
   public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
    
	
}
