<?php

namespace OM\Pincodes\Controller\Adminhtml\pincodes;

use Magento\Backend\App\Action;

class Import extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    
    protected $csvProcessor;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->csvProcessor = $csvProcessor;
        $this->connection = $resourceConnection->getConnection();
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('OM_Pincodes::Pincodes')
            ->addBreadcrumb(__('OM Pincodes'), __('OM Pincodes'))
            ->addBreadcrumb(__('Manage Item'), __('Manage Item'));
        return $resultPage;
    }

    /**
     * Edit Item
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
		if(isset($_POST['submit'])){
			 $file = $_FILES['stfile'];
			 if (!isset($file['tmp_name']) || $file['tmp_name']=='') {
				 $this->messageManager->addError(__('Please select csv file'));
				 $resultRedirect = $this->resultRedirectFactory->create();
				 return $resultRedirect->setPath('pincodes/*/import');
			 }
			 $importProductRawData = $this->csvProcessor->getData($file['tmp_name']);
		        $no_error = true;
                foreach ($importProductRawData as $rowIndex => $dataRow) {
                 $row_count = $rowIndex+1;
                 $pincode = $dataRow[0];
                 $status = $dataRow[1];

                 if ($this->has_specchar($pincode) || $this->has_specchar($status) )
                 {
                     $no_error = false;
                     $error_msg = "Error at Row $row_count , Please correct Data => pincode:  $pincode  status: $status ";
                     $this->messageManager->addErrorMessage($error_msg);
                 }
                if($no_error)
                {
				 $this->connection->query("UPDATE pincode_details set is_active = '".trim($dataRow[1])."' WHERE pincode = '".trim($dataRow[0])."'");
                }

             }
             if($no_error){
                 $this->messageManager->addSuccess(__('The Pincodes has been updated.'));
             }
			 $resultRedirect = $this->resultRedirectFactory->create();
			 return $resultRedirect->setPath('*/*/');
		 }
		 
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->setActiveMenu('OM_Pincodes::pincodes');
        $resultPage->addBreadcrumb(__('OM'), __('OM'));
        $resultPage->addBreadcrumb(__('Import Status'),__('Import Status') );
        $resultPage->getConfig()->getTitle()->prepend( __('Import Status'));

        return $resultPage;
    }

    public function has_specchar($x,$excludes=array()){
        if (is_array($excludes)&&!empty($excludes)) {
            foreach ($excludes as $exclude) {
                $x=str_replace($exclude,'',$x);
            }
        }
        if (preg_match('/[^a-z0-9 ]+/i',$x)) {
            return true;
        }
        return false;
    }

}
