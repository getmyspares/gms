<?php
namespace OM\Pincodes\Controller\Adminhtml\pincodes;

use Magento\Backend\App\Action;

class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * Update blog post(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $itemIds = $this->getRequest()->getParam('pincodes');
        if (!is_array($itemIds) || empty($itemIds)) {
            $this->messageManager->addError(__('Please select item(s).'));
        } else {
            try {
				
				$params = $this->getRequest()->getParams();
				
				if(array_key_exists('status',$params)) {
					$status = (int) $this->getRequest()->getParam('status');
					foreach ($itemIds as $postId) {
						$post = $this->_objectManager->get('OM\Pincodes\Model\Pincodes')->load($postId);
						$post->setIsActive($status)->save();
					}
				}
                
				if(array_key_exists('codstatus',$params)) {
					$codstatus = (int) $this->getRequest()->getParam('codstatus');
					foreach ($itemIds as $postId) {
						$post = $this->_objectManager->get('OM\Pincodes\Model\Pincodes')->load($postId);
						$post->setCodAvailable($codstatus)->save();
					}
				}
				
				if(array_key_exists('changemsg',$params)) {
					$changemsg = $this->getRequest()->getParam('changemsg');
					foreach ($itemIds as $postId) {
						$post = $this->_objectManager->get('OM\Pincodes\Model\Pincodes')->load($postId);
						$post->setMsg($changemsg)->save();
					}
				}
				
                
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($itemIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('pincodes/*/index');
    }

}
