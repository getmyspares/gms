<?php

namespace OM\Pincodes\Block\Adminhtml\Pincodes\Edit\Tab;

/**
 * Pincodes edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \OM\Pincodes\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \OM\Pincodes\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \OM\Pincodes\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('pincodes');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

		
        $fieldset->addField(
            'pincode',
            'text',
            [
                'name' => 'pincode',
                'label' => __('Pincode'),
                'title' => __('Pincode'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'area',
            'text',
            [
                'name' => 'area',
                'label' => __('Area'),
                'title' => __('Area'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'region',
            'text',
            [
                'name' => 'region',
                'label' => __('Region'),
                'title' => __('Region'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'region_code',
            'text',
            [
                'name' => 'region_code',
                'label' => __('Region Code'),
                'title' => __('Region Code'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'city_name',
            'text',
            [
                'name' => 'city_name',
                'label' => __('City Name'),
                'title' => __('City Name'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'dc_code',
            'text',
            [
                'name' => 'dc_code',
                'label' => __('Dc Code'),
                'title' => __('Dc Code'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'state',
            'text',
            [
                'name' => 'state',
                'label' => __('State'),
                'title' => __('State'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        /*$fieldset->addField(
            'service',
            'text',
            [
                'name' => 'service',
                'label' => __('Service'),
                'title' => __('Service'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );*/
					
        $fieldset->addField(
            'pincode_type',
            'text',
            [
                'name' => 'pincode_type',
                'label' => __('Pincode Type'),
                'title' => __('Pincode Type'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'availability',
            'text',
            [
                'name' => 'availability',
                'label' => __('Availability'),
                'title' => __('Availability'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'is_active',
            'select',
            [
                'name' => 'is_active',
                'label' => __('Status'),
                'title' => __('Status'),
				'required' => true,
				'options' => \OM\Pincodes\Block\Adminhtml\Pincodes\Grid::enableDisable(),
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'msg',
            'text',
            [
                'name' => 'msg',
                'label' => __('Message'),
                'title' => __('Message'),
				'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'cod_available',
            'select',
            [
                'name' => 'cod_available',
                'label' => __('Cod Available'),
                'title' => __('Cod Available'),
				'required' => true,
				'options' => \OM\Pincodes\Block\Adminhtml\Pincodes\Grid::enableDisable(),
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'days_to_deliver',
            'select',
            [
                'name' => 'days_to_deliver',
                'label' => __('Days To Deliver'),
                'title' => __('Days To Deliver'),
				'required' => true,
				'options' => \OM\Pincodes\Block\Adminhtml\Pincodes\Grid::nofDays(),
                'disabled' => $isElementDisabled
            ]
        );
					

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
