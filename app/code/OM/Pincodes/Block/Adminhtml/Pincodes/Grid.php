<?php
namespace OM\Pincodes\Block\Adminhtml\Pincodes;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \OM\Pincodes\Model\pincodesFactory
     */
    protected $_pincodesFactory;

    /**
     * @var \OM\Pincodes\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \OM\Pincodes\Model\pincodesFactory $pincodesFactory
     * @param \OM\Pincodes\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \OM\Pincodes\Model\PincodesFactory $PincodesFactory,
        \OM\Pincodes\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_pincodesFactory = $PincodesFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_pincodesFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
				/*$this->addColumn(
					'id',
					[
						'header' => __('ID'),
						'type' => 'number',
						'index' => 'id',
						'header_css_class' => 'col-id',
						'column_css_class' => 'col-id'
					]
				);*/


		
				$this->addColumn(
					'pincode',
					[
						'header' => __('Pincode'),
						'index' => 'pincode',
					]
				);
				
				$this->addColumn(
					'area',
					[
						'header' => __('Area'),
						'index' => 'area',
					]
				);
				
				$this->addColumn(
					'region',
					[
						'header' => __('Region'),
						'index' => 'region',
					]
				);
				
				$this->addColumn(
					'region_code',
					[
						'header' => __('Region Code'),
						'index' => 'region_code',
					]
				);
				
				$this->addColumn(
					'city_name',
					[
						'header' => __('City Name'),
						'index' => 'city_name',
					]
				);
				
				$this->addColumn(
					'dc_code',
					[
						'header' => __('Dc Code'),
						'index' => 'dc_code',
					]
				);
				
				$this->addColumn(
					'state',
					[
						'header' => __('State'),
						'index' => 'state',
					]
				);
				
				/*$this->addColumn(
					'service',
					[
						'header' => __('Service'),
						'index' => 'service',
					]
				);*/
				
				$this->addColumn(
					'pincode_type',
					[
						'header' => __('Pincode Type'),
						'index' => 'pincode_type',
					]
				);
				
				$this->addColumn(
					'availability',
					[
						'header' => __('Availability'),
						'index' => 'availability',
					]
				);
				
				$this->addColumn(
					'is_active',
					[
						'header' => __('Status'),
						'index' => 'is_active',
						'type' => 'options',
						'options' => \OM\Pincodes\Block\Adminhtml\Pincodes\Grid::enableDisable()
					]
				);
				
				$this->addColumn(
					'msg',
					[
						'header' => __('Message'),
						'index' => 'msg',
					]
				);
				
				$this->addColumn(
					'cod_available',
					[
						'header' => __('Cod Available'),
						'index' => 'cod_available',
						'type' => 'options',
						'options' => \OM\Pincodes\Block\Adminhtml\Pincodes\Grid::enableDisable()
					]
				);
							
				$this->addColumn(
					'days_to_deliver',
					[
						'header' => __('Days To Deliver'),
						'index' => 'days_to_deliver',
						'type' => 'options',
						'options' => \OM\Pincodes\Block\Adminhtml\Pincodes\Grid::nofDays()
					]
				);
				


		
        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit'
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );
		

		
		   $this->addExportType($this->getUrl('pincodes/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('pincodes/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }


	public static function enableDisable(){		
		$retArr = array();
		$retArr[] = 'Please Select';
		$retArr['1'] = 'Enable';
		$retArr['0'] = 'Disable';
		return $retArr;
	}
	
	public static function nofDays(){		
		$retArr = array();
		$retArr[] = 'Please Select';
		for($i=2; $i<13; $i++){
			$retArr[$i] = $i;
		}
		return $retArr;
	}
	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('OM_Pincodes::pincodes/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('pincodes');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('pincodes/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change Status'),
                'url' => $this->getUrl('pincodes/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );
        
		$this->getMassactionBlock()->addItem(
            'codstatus',
            [
                'label' => __('Change COD Availability'),
                'url' => $this->getUrl('pincodes/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'codstatus',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('COD Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );
        
		$this->getMassactionBlock()->addItem(
            'changemsg',
            [
                'label' => __('Change Message'),
                'url' => $this->getUrl('pincodes/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'changemsg',
                        'type' => 'text',
                        'style' => 'min-width:400px;',
                        'class' => 'required-entry',
                        'label' => __('Pincode Message')
                    ]
                ]
            ]
        );
        


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('pincodes/*/index', ['_current' => true]);
    }

    /**
     * @param \OM\Pincodes\Model\pincodes|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'pincodes/*/edit',
            ['id' => $row->getId()]
        );
		
    }

	

}
