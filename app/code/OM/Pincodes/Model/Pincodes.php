<?php
namespace OM\Pincodes\Model;

class Pincodes extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\Pincodes\Model\ResourceModel\Pincodes');
    }
}
?>