<?php
namespace OM\Pincodes\Model\ResourceModel;

class Pincodes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('pincode_details', 'id');
    }
}
?>