<?php
namespace OM\CustomRouter\Controller;
class CustomRouter implements \Magento\Framework\App\RouterInterface{
    protected $actionFactory;
    protected $_response;
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }

    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        if(strpos($identifier, 'login') !== false) {
            $request->setModuleName('customer')->setControllerName('account')->
            setActionName('login');
        } else if(strpos($identifier, 'register') !== false){
            $request->setModuleName('customer')->setControllerName('account')->
            setActionName('create');
        }else {
            return false;
        }
        return $this->actionFactory->create('Magento\Framework\App\Action\Forward',['request' => $request]
        );
    }
}
?>