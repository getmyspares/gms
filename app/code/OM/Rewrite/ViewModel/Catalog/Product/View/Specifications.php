<?php

namespace OM\Rewrite\ViewModel\Catalog\Product\View;

use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * using this class for product detail page specification section to  load product attributes @ritesh7december2021
 */
class Specifications implements ArgumentInterface
{

  public function __construct(\Magento\Catalog\Model\Config $config)
  {
    $this->catalogConfig = $config;
  }

  public function getTechnicalSpecification($_product)
  {
    $attribute_set_id = $_product->getAttributeSetId();
    $tech_specs_group_id = $this->catalogConfig->getAttributeGroupId($attribute_set_id, 'technical specifications');
    return $_product->getAttributes($tech_specs_group_id);
  }

  public function getOtherSpecification($_product)
  {
    $attribute_set_id = $_product->getAttributeSetId();
    $other_specs_group_id = $this->catalogConfig->getAttributeGroupId($attribute_set_id, 'other specification');
    return $_product->getAttributes($other_specs_group_id);
  }

}

?>