<?php

namespace OM\Rewrite\Observer;

use Magento\Framework\Event\ObserverInterface;

class CorrectDecimalPrice implements ObserverInterface
{
    protected $_objectmanager;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        $this->_objectmanager = $objectmanager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /*Convert price from 8.24 rs to  9rs when product is edicted individually from seller panel or admin panel @ritesh31august2021 */
        $product = $observer->getEvent()->getProduct();
        $id = $product->getId(); //Get Product Id

        $product_price = $this->correctPrice($product->getPrice());
        $product_special_price = $this->correctPrice($product->getSpecialPrice());

        if(!empty($product_price))
        {

            $price_decimal_corrected = ceil($product_price);
            $product->setPrice($price_decimal_corrected);
        }
        if(!empty($product_special_price))
        {
            $product_special_price_decimal_corrected =  ceil($product_special_price);
            $product->setSpecialPrice($product_special_price_decimal_corrected);
        }
        return $this;
    }

    public function correctPrice($price)
    {
        $price = str_replace(",","",$price);
        $price = str_replace(" ","",$price);
        return $price;
    }
}
