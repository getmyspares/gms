<?php

namespace OM\Rewrite\Observer;

use Magento\Framework\Event\ObserverInterface;

class SendReviewEmail implements ObserverInterface
{
    protected $_inlineTranslation;

    protected $_transportBuilder;

    protected $_objectmanager;

    protected $temp_id;

    public function __construct(
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_objectmanager = $objectmanager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        if($order->getStatus() && $order->getStatus() == "Delivered" && !$order->getReviewEmailSent())
        {
            $this->sendEmail($order);
            $order->setReviewEmailSent(1)->save();
        }

        return $this;
    }

    public function sendEmail($order)
    {
        $html = '<ul>';
        foreach ($order->getAllVisibleItems() as $item) {
            $html .= '<li>';
            $html .= '<a href="'.$item->getProduct()->getProductUrl().'#reviews">'.$item->getProduct()->getName().'</a>';
            $html .= '</li>';
        }
        $html .= '</ul>';

        $baseUrl = $this->_objectmanager->get('\Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseUrl();

        /* Assign values for your template variables  */
        $emailTempVariables = [];
        $emailTempVariables['order'] = $order;
        $emailTempVariables['orderreview'] = $baseUrl."sales/order/view/order_id/".$order->getId()."#orderreviews";
        $emailTempVariables['items'] = $html;
        $emailTempVariables['firstname'] = ucfirst($order->getCustomerFirstname());

        $storeid = $order->getStoreId();

        $this->temp_id = 16;

        $receiverInfo = [
                'name' => $order->getCustomerFirstname().' '.$order->getCustomerLastname(),
                'email' => $order->getCustomerEmail()
            ];

        $senderInfo = [
                'name' => $this->_objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_sales/name'),
                'email' => $this->_objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_sales/email'),
            ];

        $this->_inlineTranslation->suspend();    
        $this->generateStoreTemplate($emailTempVariables, $senderInfo, $receiverInfo, $storeid);    
        $transport = $this->_transportBuilder->getTransport();
        $transport->sendMessage();        
        $this->_inlineTranslation->resume();

    }

    public function generateStoreTemplate($emailTemplateVariables,$senderInfo,$receiverInfo,$storeid,$bccEmail='')
    {
        $template =  $this->_transportBuilder->setTemplateIdentifier($this->temp_id)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, 
                        'store' => $storeid,
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'],$receiverInfo['name']);

        if($bccEmail!='') {
            $template->addBcc($bccEmail);
        }

        return $this;        
    }
}
