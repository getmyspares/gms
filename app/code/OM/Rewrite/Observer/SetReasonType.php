<?php

namespace OM\Rewrite\Observer;

use Magento\Framework\Event\ObserverInterface;

class SetReasonType implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $input = $observer->getEvent()->getInput();
        if (isset($input['reason_type'])) {
            $creditMemo = $observer->getEvent()->getCreditmemo();
            $rType = $input['reason_type'];
            $creditMemo->setReasonType($rType);
        }

        return $this;
    }
}
