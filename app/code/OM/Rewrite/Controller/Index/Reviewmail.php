<?php

namespace OM\Rewrite\Controller\Index;

use Magento\Framework\App\Action\Context;

class Reviewmail extends \Magento\Framework\App\Action\Action
{
	protected $_session;
	protected $_customer;
	protected $_store;
	
	private $context;
	protected $_inlineTranslation;
	protected $_transportBuilder;
	protected $temp_id;
	protected $_objectmanager;
	
	public function __construct(
		Context $context,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
		\Magento\Customer\Model\Customer $_customer,
		\Magento\Customer\Model\Session $_session,
		\Magento\Store\Model\StoreManagerInterface $_store,
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
		\Magento\Framework\App\ResourceConnection $resourceConnection
	) {
		$this->_customer = $_customer;
		$this->_session = $_session;
		$this->_store = $_store;
		$this->_objectmanager = $objectmanager;
		$this->_transportBuilder = $transportBuilder;
		$this->_inlineTranslation = $inlineTranslation;
		$this->connection = $resourceConnection->getConnection();
		parent::__construct($context);
	}
		
		
    public function execute()
    {
		$token = $this->getRequest()->getParam('token');
		$baseUrl = $this->_store->getStore()->getBaseUrl();
		
		if($token){
			$tokendata = base64_decode($token);
			$tdata = explode('####',$tokendata);
			
			$url         = $tdata[0];
			$customer_id = $tdata[1];
			$customer = $this->_customer->load($customer_id);
			$this->_session->setCustomerAsLoggedIn($customer);				
			header("location:$url");
			
		}else{
			$lTime = date( 'Y-m-d H:i:s', (time()-(86700*6)) );

			$uQu = "UPDATE `sales_order_grid` set `delivery_date`=(select delivery_date from sales_order where sales_order.entity_id=sales_order_grid.entity_id and sales_order.delivery_date!=0) WHERE status='Delivered' AND sales_order_grid.delivery_date=NULL";
			$this->connection->query($uQu);
			
			$orders = $this->connection->fetchAll("SELECT entity_id,customer_id FROM `sales_order_grid` WHERE `status` LIKE 'Delivered' AND `review_mail_sent` = '0' AND delivery_date < '$lTime'");
			foreach($orders as $order){
				$this->sendEmail($order['entity_id']);
				$this->connection->query("UPDATE sales_order_grid set review_mail_sent=1 WHERE `entity_id` = '".$order['entity_id']."'");
			}					
		}
		
		exit;
    }
    
    
    public function sendEmail($orderId)
    {
		$order   = $this->_objectmanager->create('Magento\Sales\Model\Order')->load($orderId);
		$baseUrl = $this->_store->getStore()->getBaseUrl();
		
        $html = '<ul>';
        foreach ($order->getAllVisibleItems() as $item) {
            $html .= '<li>';
            $purl = $baseUrl."omrewrite/index/reviewmail?token=".base64_encode( $item->getProduct()->getProductUrl().'#reviews'."####".$order->getCustomerId() ); 
            $html .= '<a href="'.$purl.'">'.$item->getProduct()->getName().'</a>';
            $html .= '</li>';
        }
        $html .= '</ul>';

        

        /* Assign values for your template variables  */
        $emailTempVariables = [];
        $emailTempVariables['order'] = $order;
        $orderurl = $baseUrl."sales/order/view/order_id/".$orderId."/#orderreviews";
        $emailTempVariables['orderreview'] = $baseUrl."omrewrite/index/reviewmail?token=".base64_encode($orderurl."####".$order->getCustomerId()); 
        $emailTempVariables['items'] = $html;
        $emailTempVariables['firstname'] = ucfirst($order->getCustomerFirstname());

        $storeid = $order->getStoreId();

        $this->temp_id = $this->_objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('custom_template/email/order_review');

        $receiverInfo = [
                'name' => $order->getCustomerFirstname().' '.$order->getCustomerLastname(),
                'email' => $order->getCustomerEmail()
            ];

        $senderInfo = [
                'name' => $this->_objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_sales/name'),
                'email' => $this->_objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_sales/email'),
            ];

        $this->_inlineTranslation->suspend();    
        $this->generateStoreTemplate($emailTempVariables, $senderInfo, $receiverInfo, $storeid);    
        $transport = $this->_transportBuilder->getTransport();
        $transport->sendMessage();        
        $this->_inlineTranslation->resume();

    }

    public function generateStoreTemplate($emailTemplateVariables,$senderInfo,$receiverInfo,$storeid,$bccEmail='')
    {
        $template =  $this->_transportBuilder->setTemplateIdentifier($this->temp_id)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, 
                        'store' => $storeid,
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'],$receiverInfo['name']);

        if($bccEmail!='') {
            $template->addBcc($bccEmail);
        }
        
        //$template->addBcc('kumar.rajesh@orangemantra.in');

        return $this;        
    }
    
}
