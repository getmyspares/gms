<?php

namespace OM\Rewrite\Controller\Index;

class Cartcount extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		 
		$totalItems    = $cart->getQuote()->getItemsCount();
		$totalQuantity = $cart->getQuote()->getItemsQty();
        echo json_encode(["count"=>(int)$totalQuantity]);
    }
}
