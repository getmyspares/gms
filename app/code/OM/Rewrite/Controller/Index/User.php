<?php

namespace OM\Rewrite\Controller\Index;

class User extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $objectManagerlogin = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManagerlogin->create('Magento\Customer\Model\Session');
        $cName = $customerSession->getCustomer()->getName();
        echo json_encode(["cname"=>$cName]);
    }
}
