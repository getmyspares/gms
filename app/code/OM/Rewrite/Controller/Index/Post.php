<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OM\Rewrite\Controller\Index;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Contact\Model\ConfigInterface;
use Magento\Contact\Model\MailInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\ObjectManagerInterface;

class Post extends \Magento\Contact\Controller\Index implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var MailInterface
     */
    private $mail;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectmanager;

    protected $temp_id;

    /**
     * @param Context $context
     * @param ConfigInterface $contactsConfig
     * @param MailInterface $mail
     * @param DataPersistorInterface $dataPersistor
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig,
        MailInterface $mail,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ObjectManagerInterface $objectmanager,
        LoggerInterface $logger = null
    ) {
        parent::__construct($context, $contactsConfig);
        $this->context = $context;
        $this->mail = $mail;
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->objectmanager = $objectmanager;

        $this->logger = $logger ?: ObjectManager::getInstance()->get(LoggerInterface::class);
    }

    /**
     * Post user question
     *
     * @return Redirect
     */
    public function execute()
    {
        return $this->resultRedirectFactory->create()->setPath('helpdesk/ticket');
        die();
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        try {
            $this->sendEmail($this->validatedParams());
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->get('Magento\Customer\Model\Session');
            $customerSession->setContactFormSubmitted(true);
            $this->messageManager->addSuccessMessage(
                __('Thanks for contacting us with your comments and questions. We\'ll respond to you very soon.')
            );

            $this->dataPersistor->clear('contact_us');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('contact_us', $this->getRequest()->getParams());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(
                // __('An error occurred while processing your form. Please try again later.')
                $e->getMessage()
            );
            $this->dataPersistor->set('contact_us', $this->getRequest()->getParams());
        }
        return $this->resultRedirectFactory->create()->setPath('contact/index');
    }

    // /**
    //  * @param array $post Post data from contact form
    //  * @return void
    //  */
    // private function sendEmail($post)
    // {
    //     $this->mail->send(
    //         $post['email'],
    //         ['data' => new DataObject($post)]
    //     );
    // }

    public function sendEmail($post)
    {
        /* Assign values for your template variables  */
        $emailTempVariables = [];
        $emailTempVariables['comment'] = $post['comment'];
        $emailTempVariables['name'] = $post['name'];

        $storeid = $this->storeManager->getStore()->getId();

        $this->temp_id = 17;

        $receiverInfo = [
                'name' => $post['name'],
                'email' => $post['email']
            ];

        $senderInfo = [
                'name' => $this->objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_sales/name'),
                'email' => $this->objectmanager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_sales/email'),
            ];
        $bccEmail  = $senderInfo['email'];    
        $this->inlineTranslation->suspend();    
        $this->generateStoreTemplate($emailTempVariables, $senderInfo, $receiverInfo, $storeid,$bccEmail);    
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();        
        $this->inlineTranslation->resume();

    }

    public function generateStoreTemplate($emailTemplateVariables,$senderInfo,$receiverInfo,$storeid,$bccEmail='')
    {
        $template =  $this->transportBuilder->setTemplateIdentifier($this->temp_id)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, 
                        'store' => $storeid,
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'],$receiverInfo['name']);
        if($bccEmail!='') {
            $template->addBcc($bccEmail);
        }

        return $this;        
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if (trim($request->getParam('name')) === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if (trim($request->getParam('comment')) === '') {
            throw new LocalizedException(__('Enter the comment and try again.'));
        }
        if (false === \strpos($request->getParam('email'), '@')) {
            throw new LocalizedException(__('The email address is invalid. Verify the email address and try again.'));
        }
        if (trim($request->getParam('hideit')) !== '') {
            throw new \Exception();
        }

        return $request->getParams();
    }
}
