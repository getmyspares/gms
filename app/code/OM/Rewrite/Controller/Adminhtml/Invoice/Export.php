<?php

namespace OM\Rewrite\Controller\Adminhtml\Invoice;

class Export extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Export Invoices'));
        $resultPage->addBreadcrumb(__('Export Invoices'), __('Export Invoices'));
        return $resultPage;
    }

    protected function _initAction()
    {
        $this->_view->loadLayout();
        return $this;
    }

    protected function _isAllowed()
    {
        return true;//$this->_authorization->isAllowed('OM_OrderRating::order_reviews');
    }
}