<?php

namespace OM\Rewrite\Controller\Adminhtml\Markordernew;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory; 

class Submit extends \Magento\Backend\App\Action
{   
    protected $_resource;
    protected $_messageManager;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Message\ManagerInterface $messageManager,
		PageFactory $resultPageFactory
    ) {
		$this->resultPageFactory = $resultPageFactory;
        $this->_resource = $resource;
        $this->_messageManager = $messageManager;

        parent::__construct($context);
    }

    public function execute()
    {   
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try
           {
            $connection = $this->_resource->getConnection();
            $req = $this->getRequest()->getParams();
            $incrementId = $req['number'];
            
            $sqlQuery = "SELECT entity_id FROM `sales_order` WHERE increment_id = $incrementId";
            $entityId = $connection->fetchOne($sqlQuery);  
            //print_r($entityId);
    
            $sqlQuery = "UPDATE `sales_order_item` set qty_shipped='0' where order_id = $entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "DELETE from `sales_shipment_grid` WHERE order_id =  $entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "DELETE from `sales_shipment` WHERE order_id= $entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "UPDATE `marketplace_orders` set shipment_id=0,order_status='pending',tracking_number='' WHERE order_id = $entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "UPDATE `sales_order` set state='processing' ,status='New' where entity_id= $entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "UPDATE `sales_order_grid` set status='New' where entity_id=$entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "DELETE from  `ecomexpress_awb` WHERE `orderid` = $entityId";
            $conn = $connection->query($sqlQuery);
            $sqlQuery = "UPDATE `om_ecom_api_status` set awb_number='',pickup_date='',delivery_date='' ,order_status='New' where order_id=$entityId";
            $conn = $connection->query($sqlQuery);

            $message = __("The order #$incrementId is successfully marked as New."); 
             $this->messageManager->addSuccessMessage($message);
        } 
        catch(\Exception $e) {
                $message = __("The order #$incrementId is not marked as New."); 
                $this->messageManager->addErrorMessage($message);
        }

        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}