<?php
/**
 * Copyright © srfsefsd All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rewrite\Controller\Adminhtml\Markordernew;

use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Mark Order As New'));
        $resultPage->addBreadcrumb(__('Mark Order As New'), __('Mark Order As New'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return true;
        // return $this->_authorization->isAllowed('OM_Rewrite::markordernew_index');
    }
}