<?php

namespace OM\Rewrite\Controller\Adminhtml\ProductCommission;

class Export extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $objectManagerlogin = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManagerlogin->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $query = "SELECT a.sku,b.`value` FROM `catalog_product_entity` as a left join (select * from catalog_product_entity_varchar where attribute_id  ='201' and store_id=0) b on a.entity_id=b.entity_id";
        $result = $connection->fetchAll($query);	

        $columns = array("sku","commission_for_product");
        $fp = fopen('php://memory', 'w');
        fputcsv( $fp, $columns,",");
        foreach ($result as $data) {
            $sku=$data["sku"];
            $product_commission_percentage=$data["value"];
            $insertArr=[$sku,$product_commission_percentage];
            fputcsv($fp, $insertArr, ",");
        }
        
        $filename = 'product_commision_report-'.date('Y-m-d-H-i-s').'.csv';
        fseek($fp, 0);
        header('Content-Type: text/csv');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($fp);
        exit;  

    }

    protected function _isAllowed()
    {
        return true;
    }
}