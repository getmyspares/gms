<?php
namespace OM\Rewrite\Controller\Adminhtml\Customer;
use Magento\Framework\Controller\ResultFactory; 

class Accountconfirm extends \Magento\Framework\App\Action\Action
{
  private $_resourceConnection;
  public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->request = $request;
        $this->_resourceConnection = $resourceConnection;
        parent::__construct($context);
    }
  public function execute()
  {
    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
    $postParams = $this->request->getParams();
    if(isset($postParams['customer_id']) && !empty($postParams['customer_id']))
    {
      $customer_id = $postParams['customer_id'];
      $connection = $this->_resourceConnection->getConnection();
      $customer_entity = $this->_resourceConnection->getTableName('customer_entity');
      $sql = "Update $customer_entity SET `confirmation` = NULL WHERE entity_id = '$customer_id'";
      $connection->query($sql);
      $customer_grid_flat = $this->_resourceConnection->getTableName('customer_grid_flat');
      $sql1 = "Update $customer_grid_flat SET `confirmation` = NULL WHERE entity_id = '$customer_id'";
      $connection->query($sql1);

      $data = $connection->fetchRow("SELECT email, firstname, lastname FROM $customer_entity WHERE entity_id = '$customer_id'");
      $mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$customer_id'");
      $this->sendEmailNotification($data['email'], $mobile, $data['firstname'], $data['lastname']);

      $message = 'Account confirmed successfully';
      $this->messageManager->addSuccess(__("$message"));
    } else 
    {
      $this->messageManager->addError(__('Customer Id not found'));
    }
    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    return $resultRedirect;
  }

  public function sendEmailNotification($email, $mobile, $firstName, $lastName)
  {
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
      $helpers->individual_welcome($firstName,$lastName,$email);
      $smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
      $msg = "Your Account with GetMySpares Store is confirmed successfully";
      $smsHelper->send_custom_sms($mobile,$msg);
  }
}
