<?php

namespace OM\Rewrite\Controller\Adminhtml\Paymentlink;
use Magento\Framework\Controller\ResultFactory; 


class Send extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \OM\Rewrite\Helper\PaymentLink $paymentLinkHelper
    ) {
        $this->paymentLinkHelper = $paymentLinkHelper;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
      $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
      $postData = $this->getRequest()->getPost();
      $mobile_number = $postData['mobile'];
      $email = $postData['email'];
      $order_id = $postData['order_id'];

      if($mobile_number)
      {
				$status = $this->paymentLinkHelper->sendPaymentLinkSms($order_id,$mobile_number);
        if($status)
        {
          $message="Payment link has been send to mobile number $mobile_number";
          $this->messageManager->addSuccessMessage($message); 
        }else 
        {
          $message="Payment link could not be send mobile number $mobile_number";
          $this->messageManager->addErrorMessage($message);
        }
      }

      if($email)
      {
				$status = $this->paymentLinkHelper->sendPaymentLinkEmail($order_id,$email);
        if($status)
        {
          $message="Payment link has been send to Email Address $email";
          $this->messageManager->addSuccessMessage($message); 
        } else 
        {
          $message="Payment link could not be send Email Address $email";
          $this->messageManager->addErrorMessage($message);
        }
      }

      $resultRedirect->setUrl($this->_redirect->getRefererUrl());
      return $resultRedirect;
      
    }

    protected function _isAllowed()
    {
        return true;
    }



}