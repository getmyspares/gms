<?php

namespace OM\Rewrite\Block\Adminhtml\Grid\Column\Renderer;

class Mobile extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Currency
{
    protected $customer;
    
    protected $storemanager;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customer,
        \Magento\Store\Model\StoreManagerInterface $storemanager
    )
    {
        $this->customer = $customer;
        $this->storemanager = $storemanager;
    }

    /**
     * Renders grid column
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $email = $row->getCustomerEmail();
        
        $websiteID = $this->storemanager->getStore()->getWebsiteId();
        $customer = $this->customer->create()->setWebsiteId($websiteID)->loadByEmail($email);

        return $customer->getMobile();
    }
}
