<?php
/**
 * Copyright © srfsefsd All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rewrite\Block\Adminhtml\Markordernew;

class Index extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\UrlInterface $urlBuilder,
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

    public function getMarkOrderNewUrl()
    {
        return $this->urlBuilder->getRouteUrl('rewriteadmin/markordernew/submit',[ 'key'=>$this->urlBuilder->getSecretKey('rewriteadmin','markordernew','submit')]);
    }
}

