<?php

namespace OM\Rewrite\Block\Adminhtml\Sales\Order\Creditmemo;

use Magento\Framework\Registry;
use Magento\Framework\App\ResourceConnection;
use Magento\Backend\Block\Template\Context;

class Status extends \Magento\Backend\Block\Template
{
    private $_resourceConnection;

    private $coreRegistry;

    public function __construct(
        Context $context,
        Registry $registry,
        ResourceConnection $resourceConnection,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->_resourceConnection = $resourceConnection;
        parent::__construct($context, $data);
    }

    public function getCreditmemo()
    {
        return $this->coreRegistry->registry('current_creditmemo');
    }

    public function getStatus()
    {
        $q = "SELECT id,reason FROM `wk_rma_reason` WHERE status = 1";
        $result = $this->_resourceConnection->getConnection()->fetchAll($q);
        $result[] = ['id' => 0, 'reason'=> 'Refund'];
        return $result;
    }
}
