<?php

namespace OM\Rewrite\Block\Adminhtml\Invoice;

class Export extends \Magento\Backend\Block\Template
{
    
    protected $resource;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->resource = $resource;
        parent::__construct($context, $data);
    }

    public function getSellers()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $collection = $objectManager->create('Webkul\Marketplace\Model\ResourceModel\Seller\Collection');
        $collection->addFieldToFilter('is_seller', 1);

        return $collection;
    }

}
