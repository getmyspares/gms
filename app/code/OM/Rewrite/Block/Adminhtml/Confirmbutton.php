<?php

namespace OM\Rewrite\Block\Adminhtml;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Confirmbutton  implements ButtonProviderInterface
{
  protected $_coreRegistry;
  protected $_urlInterface;
  protected $_accountManagement;

  public function __construct(
    \Magento\Framework\Registry $registry,
    \Magento\Framework\UrlInterface $urlInterface,
    \Magento\Customer\Api\AccountManagementInterface $accountManagement
  ) {
    $this->_coreRegistry = $registry;
    $this->_urlInterface = $urlInterface;
    $this->_accountManagement = $accountManagement;
  }

  public function getButtonData()
  {
    $customerId = $this->getCustomerId();
    $data=null;
    if($customerId)
    {
      $aStatus = $this->_accountManagement->getConfirmationStatus($customerId);
      if($aStatus=='account_confirmation_required') {
        $url = $this->_urlInterface->getUrl('rewriteadmin/customer/accountconfirm', ['customer_id' => $customerId]);
        $data = [
          'label' => 'Confirm Account',
          'on_click' => sprintf("location.href = '%s';", $url),
          'class' => 'confirm-customer-account',
        ];
      }
    }
    return $data;
  }

  public function getCustomerId()
  {
    $customerId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    return $customerId;
  }
}

?>
