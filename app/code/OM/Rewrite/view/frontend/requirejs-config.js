var config = {
    shim: {
        'bxSlider': {
            deps: ['jquery']
        }
    },
    paths: {
        'bxSlider': "OM_Rewrite/js/bxslider"
    }
};