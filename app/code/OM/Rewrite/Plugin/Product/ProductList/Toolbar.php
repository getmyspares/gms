<?php

namespace OM\Rewrite\Plugin\Product\ProductList;

class Toolbar
{
    public function aroundgetProductCollection(
        \Magento\Catalog\Model\Layer $subject,
        callable $proceed
    ) {
        $collection = $proceed();
        $collection->getSelect()->order('is_salable DESC');
        return $collection;
    }
}