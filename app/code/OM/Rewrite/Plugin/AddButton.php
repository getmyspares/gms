<?php
namespace OM\Rewrite\Plugin;
use Magento\Sales\Block\Adminhtml\Order\View;

class AddButton
{
	protected $_objectManager;
	
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->_objectManager = $objectManager;
        $this->connection = $resourceConnection->getConnection();
    }
      
    public function beforeGetBackUrl(View $subject)
    {
        $orderId = $subject->getOrder()->getId();
        $status = $subject->getOrder()->getStatus();
        
        $rzpoId = $this->connection->fetchOne("SELECT razorpay_order_id FROM `sales_order_grid` WHERE entity_id='".$orderId."' ");    
        
		$storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url     = $storeManager->getStore()->getBaseUrl();
		
        if(in_array($status,array('pending_payment','pending'))) {
			$subject->addButton('refresh_payment_status', [
				'label'    => __('Refresh Payment Status'),
				'class'    => 'refresh_payment_status',
				'on_click' => sprintf(
					"window.open('%s');",
					$site_url.'razorpay/payment/refreshstatus/razorpay_order_id/'.$rzpoId
				)
			], 60);
	    }
	    
		if($status == 'canceled') {
			$subject->addButton('refresh_payment_status', [
				'label'    => __('Refresh Payment Status'),
				'class'    => 'refresh_payment_status',
				'on_click' => sprintf(
					"window.open('%s');",
					$site_url.'razorpay/payment/refreshstatus/razorpay_order_id/'.$rzpoId.'/schange/canceled/'
				)
			], 60);
	    }
	    
    }
}
