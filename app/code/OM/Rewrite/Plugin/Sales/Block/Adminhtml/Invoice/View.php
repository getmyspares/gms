<?php
namespace OM\Rewrite\Plugin\Sales\Block\Adminhtml\Invoice;
use Magento\Sales\Block\Adminhtml\Order\Invoice\View as InvoiceView;
use Magento\Framework\UrlInterface;
use Magento\Framework\AuthorizationInterface;

class View
{
    /** @var \Magento\Framework\UrlInterface */
    protected $_urlBuilder;

    protected $_objectmanager;

    /** @var \Magento\Framework\AuthorizationInterface */
    protected $_authorization;

    public function __construct(
      UrlInterface $url,
      \Magento\Framework\ObjectManagerInterface $objectmanager,
      AuthorizationInterface $authorization
    ) {
      $this->_urlBuilder = $url;
      $this->_authorization = $authorization;
      $this->_objectmanager = $objectmanager;
    }

    public function beforeSetLayout(InvoiceView $view) {

      $invoice = $view->getInvoice();
      $invoiceId = $invoice->getId();
      $order = $invoice->getOrder();
      $orderId = $order->getIncrementId();
      $orderItems = $order->getAllItems();
      $pId = 0;
      foreach ($order->getAllVisibleItems() as $item) {
        $pId = $item->getProductId();
        break;
      }

      $regenerateUrl = $this->_urlBuilder->getUrl('marketplace/order_invoice/regenerate', ['invoice_id' => $invoiceId,'real_order_id' => $orderId,'pid' => $pId]);
      $view->addButton('regenerate', 
              [
                'label' => 'Regenerate', 
                'onclick' => "setLocation('{$regenerateUrl}')", 
                  'class' => 'go',
                  'margin' => '20px',
               ] 
             ); 

      $downloadUrl = $this->_urlBuilder->getUrl('marketplace/order_invoice/download', ['invoice_id' => $invoiceId,'real_order_id' => $orderId]);
      $view->addButton('downloadnew', 
              [
                'label' => 'Download', 
                'onclick' => "setLocation('{$downloadUrl}')", 
                  'class' => 'go',
                  'margin' => '20px',
               ] 
             );

    }
}