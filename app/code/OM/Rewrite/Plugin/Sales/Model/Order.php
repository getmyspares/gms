<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rewrite\Plugin\Sales\Model;

class Order
{

  public function __construct(
    \Magento\Framework\App\ResourceConnection $resourceConnection
  ) {
      $this->connection = $resourceConnection->getConnection();
  }

    public function afterCanCreditmemo(
        \Magento\Sales\Model\Order $subject,
        $result
    ) {
        $order_id = $subject->getId();
        $query = "Select id from om_settlement_report_partial where order_id='$order_id'";
        $settlement_row = $this->connection->fetchOne($query);
        if($settlement_row)
        {
          /* selttlement already generated , restrict the provision to generate credit memo @ritesh*/
          return false;
        }
        return $result;
    }
}

