<?php

namespace OM\Rewrite\Plugin\Wishlist\Model;

class Wishlist
{  

  /**
   * only add one product quantity to wishlist @ritesh16dec2021
   *
   * @param \Magento\Wishlist\Model\Wishlist $subject
   * @param \Closure $proceed
   * @param [type] $product
   * @param [type] $qty
   * @param boolean $forciblySetQty
   * @return void
   */
  public function aroundAddNewItem(\Magento\Wishlist\Model\Wishlist $subject, \Closure $proceed, $product, $qty, $forciblySetQty = true)
  { 
      $qty=1;
      $forciblySetQty=1;
      $result = $proceed($product, $qty, $forciblySetQty);
      return $result;
  }

} 