<?php
namespace OM\Rewrite\Plugin\Checkout\Model;

use Magento\Framework\Exception\LocalizedException;

class Cart
{
    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    /**
     * Plugin constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \OM\Rewrite\Helper\Addtocartrestriction $Addtocartrestriction 
    ) {
        $this->quote = $checkoutSession->getQuote();
        $this->redirect = $redirect;
        $this->messageManager = $messageManager;
        $this->_addtocartrestriction = $Addtocartrestriction;
    }

    /**
     * beforeAddProduct
     *
     * @param      $subject
     * @param      $productInfo
     * @param null $requestInfo
     *
     * @return array
     * @throws LocalizedException
     */
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null)
    {
        $product_id = $productInfo->getId();
        $request_originated_from =  $this->redirect->getRefererUrl();
        $is_seller_enabled = $this->_addtocartrestriction->isSellerActive($product_id);
        if (!$is_seller_enabled) {
          if(stripos($request_originated_from,"searchautocomplete"))
          {/* this mean request comes from ajax , which means redirect url wont work , so need a little hack here @ritesh*/
            echo $request_originated_from;
            $this->messageManager->addNoticeMessage('Seller is unable to ship this item');
            throw new LocalizedException(__('Seller is unable to ship this item'));
            die();
          }
          throw new LocalizedException(__('Seller is unable to ship this item'));
        }
        return [$productInfo, $requestInfo];
    }

}