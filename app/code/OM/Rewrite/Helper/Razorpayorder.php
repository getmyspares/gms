<?php

namespace OM\Rewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

use Razorpay\Api\Api;
use Razorpay\Magento\Model\PaymentMethod;
use Razorpay\Magento\Model\Config;

$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$path=$api_helpers->get_path();			

require_once $path.'/app/code/Razorpay/Razorpay/Razorpay.php';  

class Razorpayorder extends AbstractHelper
{
	
	protected $quote;

    protected $checkoutSession;

    protected $cartManagement;

    protected $cache;

    protected $orderRepository;
    
    protected $order;
    
	protected $orderModel;
	
	protected $orderSender;
	
	protected $logger;
	
	protected $quoteRepository;
	
	protected $_smshepler;
	
	protected $eventmanager;

    public function __construct(
		\Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Razorpay\Magento\Model\Config $config,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Razorpay\Magento\Model\CheckoutFactory $checkoutFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\Data\OrderInterface $order,
		\Magento\Sales\Model\OrderFactory $orderModel,
		\Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,           
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Event\ManagerInterface $eventmanager,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \OM\MobileOtp\Helper\SmsHelper $smshelper,
		\Magento\Framework\App\Request\Http $request
    ) {
		
        $this->catalogSession  = $catalogSession;
        $this->config          = $config;
        $this->checkoutSession = $checkoutSession;
        $this->cartManagement  = $cartManagement;
        $this->customerSession = $customerSession;
        $this->checkoutFactory = $checkoutFactory;
        $this->_smshepler = $smshelper;    
        $this->cache = $cache;
        $this->orderRepository = $orderRepository;
        $this->order           = $order;
        $this->connection = $resourceConnection->getConnection();
        $this->logger          = $logger;
		$this->orderModel = $orderModel;
		$this->eventmanager = $eventmanager;
		$this->orderSender = $orderSender;
        $this->quoteRepository = $quoteRepository;   
        $this->objectManagement   = \Magento\Framework\App\ObjectManager::getInstance();
        
	    $this->request = $request;
	    	  
        $this->key_id = $this->config->getConfigData(Config::KEY_PUBLIC_KEY);
        $this->key_secret = $this->config->getConfigData(Config::KEY_PRIVATE_KEY);
        $this->rzp = new Api($this->key_id, $this->key_secret);
    }
    
	public function create_mobile_order($result)
	{	
		$request = array();		
        /*$file   = BP.'/var/log/rk.log';
        $fp     = fopen($file,"a+");
        fclose($fp);        
		file_put_contents($file, print_r($result, true).PHP_EOL.date('Y-m-d h:i:s'), FILE_APPEND);*/
		
		
		//$rzp_payment_id  = 'pay_GhqVdjSs4vwUE7';
		//$rzp_order_id    = 'order_GhqVHLMTZn6hLe'; 
		//$rzp_signature   = 'TEST';
		
		$rzp_payment_id  = $result['payment']; 
		$rzp_order_id    = $result['razorpay_order_id']; 
		$rzp_signature   = $result['razorpay_signature'];
		
	    if($rzp_payment_id && $rzp_payment_id!='')
        {
            if($rzp_signature && $rzp_signature!='') {
                $request['paymentMethod']['additional_data'] = [
                    'rzp_payment_id' => $rzp_payment_id,
                    'rzp_order_id'   => $rzp_order_id ,
                    'rzp_signature'  => $rzp_signature
                ];
            }	
            
			$this->validateSignature($request);
            
            $orders = $this->connection->fetchAll("SELECT entity_id,razorpay_order_id,grand_total,razorpay_total FROM `sales_order_grid` WHERE razorpay_order_id='".$rzp_order_id."' ");          
            foreach($orders as $orow){            
                $order_id = $orow['entity_id'];
                $amnt     = $orow['razorpay_total'];
                $orderAmnt = $orow['grand_total'];
                $order    = $this->orderModel->create()->load($order_id);
                
                $quote_id = $this->connection->fetchOne("SELECT quote_id FROM `sales_order` WHERE entity_id='".$order_id."' ");
                $customer_id = $this->connection->fetchOne("SELECT customer_id FROM `sales_order` WHERE entity_id='".$order_id."' ");
                
				$this->checkoutSession->setLastQuoteId($quote_id)
                            ->setLastSuccessQuoteId($quote_id);
            
				$this->checkoutSession->setLastOrderId($order_id)             
							->setLastRealOrderId($order->getIncrementId())
							->setLastOrderStatus($order->getStatus()); 
							
				$payment = $order->getPayment();
				$payment ->setStatus(1)
				    ->setTransactionId($rzp_payment_id)
				    ->setPreparedMessage(__('Razorpay transaction has been successful from mobile App .'))
					->setAmountPaid($orderAmnt)
					->setLastTransId($rzp_payment_id)				
					->setIsTransactionClosed(true)
					->setShouldCloseParentTransaction(true)
					->registerCaptureNotification($orderAmnt,true );                    
				
				$invoice = $payment->getCreatedInvoice();
				
				$order->setState("processing");
				$order->setStatus("New");
				
				$this->checkoutSession->setForceOrderMailSentOnSuccess(true);
				$this->orderSender->send($order, true);
				
				$order->save();
				
				$smsHelper = $this->_smshepler;
				$smsHelper->sendOrderConfMsg($order);
								
				//update the Razorpay payment with corresponding created order ID of this quote ID
				$this->updatePaymentNote($rzp_payment_id, $order);
				
				$this->connection->query("update quote set is_active=0 WHERE customer_id='".$customer_id."' ");
				
				//$this->eventmanager->dispatch('checkout_onepage_controller_success_action', ['order_ids' => [$order_id] ]);   
			}	
			return 'success';					                               																
        }
        return 'fail';		
	}
	
	protected function validateSignature($request)
    {
        $attributes = array(
            'razorpay_payment_id' => $request['paymentMethod']['additional_data']['rzp_payment_id'],
            'razorpay_order_id'   => $request['paymentMethod']['additional_data']['rzp_order_id'],
            'razorpay_signature'  => $request['paymentMethod']['additional_data']['rzp_signature'],
        );

        $this->rzp->utility->verifyPaymentSignature($attributes);
    }
    
	protected function updatePaymentNote($paymentId, $order)
    {
        //update the Razorpay payment with corresponding created order ID of this quote ID
        $this->rzp->payment->fetch($paymentId)->edit(
            array(
                'notes' => array(
                    'merchant_order_id' => $order->getIncrementId(),
                    'merchant_quote_id' => $order->getQuoteId()
                )
            )
        );
    }
    
}

