<?php

namespace OM\Rewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;

class PaymentLink extends AbstractHelper
{


  public function __construct(
    \Magento\Framework\App\Helper\Context $context,
    \Panasonic\CustomUser\Helper\Data $panasonicHelperData,
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    \Magento\Framework\Message\ManagerInterface $messageManager,
    StoreManagerInterface $storeManager,
    \OM\MobileOtp\Helper\SmsHelper $smsHelper,
    \Psr\Log\LoggerInterface $logger,
    \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
    \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,    
    \Magento\Framework\ObjectManagerInterface $_objectManager
  ) {
    parent::__construct($context);
    $this->smsHelper = $smsHelper;
    $this->panasonicHelperData = $panasonicHelperData;
    $this->messageManager = $messageManager;
    $this->storeManager = $storeManager;
    $this->logger          = $logger;
    $this->_objectManager = $_objectManager;
    $this->_inlineTranslation = $inlineTranslation;
    $this->_transportBuilder = $transportBuilder;
    $this->connection = $resourceConnection->getConnection();
  }

  public function sendPaymentLinkEmail($order_id,$email)
  {
  
    $customer_support_email = $this->panasonicHelperData->getCustomerSupportEmail();
    $to = array($customer_support_email, $email);
    $selectedTemplateId = $this->scopeConfig->getValue('custom_template/email/payment_link');
    $templateId = $selectedTemplateId;
    $fromEmail = $this->getFromEmail();
    $fromName = 'GetMySpares';
    $toEmail = $email;
    $paymentLInk  = $this->getPaymentLink($order_id);
    $baseUrl = $this->storeManager->getStore()->getBaseUrl();
    $customer_name = $this->getCustomerName($order_id);

    try {
      $templateVars = [
        'siteUrl' => $baseUrl,
        'customerName' => $customer_name,
        'paymentLink' => $paymentLInk,
      ];
      print_r($templateVars);

      $storeId = $this->storeManager->getStore()->getId();

      $from = ['email' => $fromEmail, 'name' => $fromName];
      $this->_inlineTranslation->suspend();

      $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
      $templateOptions = [
        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
        'store' => $storeId
      ];
      $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
        ->setTemplateOptions($templateOptions)
        ->setTemplateVars($templateVars)
        ->setFrom($from)
        ->addTo($toEmail)
        ->getTransport();
      $transport->sendMessage();
      $this->_inlineTranslation->resume();
      return true;
    } catch (\Exception $e) {
      echo $e->getMessage();
      $this->logger->info($e->getMessage());
    }
    return false;
  }

  public function getPaymentLink($order_id)
  {
    $query = "select * from sales_order_grid where entity_id='$order_id'";
    $sales_order_grid_data = $this->connection->fetchAll($query)[0];
    
    $baseUrl = $this->storeManager->getStore()->getBaseUrl();
    
    if($sales_order_grid_data)
    {
        $razorpayOrderId = $sales_order_grid_data['razorpay_order_id'];
        return $baseUrl.'razorpay/payment/vieworder?rzp_order_id='.$razorpayOrderId;
    } else 
    {
      $this->messageManager->addErrorMessage("Payment Link Not Found For This Order !!"); 
    }
    return false;
  }

  public function getCustomerName($order_id)
  {
    $query = "SELECT customer_name FROM `sales_order_grid`  where entity_id='$order_id'";
    $customer_name = $this->connection->fetchOne($query);
    
    if($customer_name)
    {
      return $customer_name;
    } 

    return false;
  }

  public function sendPaymentLinkSms($order_id,$mobile)
  {

    $paymentLInk = $this->getPaymentLink($order_id);
    $name = $this->getCustomerName($order_id);
    $msg = "Dear $name, Your GetMySpares order cart is ready. Please review cart and make payment for your GetMySpares order at " . $paymentLInk;
    return $this->smsHelper->send_custom_sms($mobile, $msg);
  }

  public  function getFromEmail(){
		return $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_custom2/email');
	}
}
