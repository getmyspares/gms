<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\Rewrite\Helper;
use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;

class PdfDataHelper extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $_customerSession,
        CreditmemoRepositoryInterface $creditmemoRepository
    ) {
        parent::__construct($context);
        $this->customerSession = $_customerSession;
        $this->creditmemoRepository = $creditmemoRepository;
    }

    public function getCreditmemoData($creditmemoId): ?CreditmemoInterface
    {   
        try {
            $creditmemoData = $this->creditmemoRepository->get($creditmemoId);
        } catch (Exception $exception)  {
            $this->_logger->critical($exception->getMessage());
            $creditmemoData = null;
        }
 
        return $creditmemoData;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }
}

