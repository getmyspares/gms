<?php

namespace OM\Rewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class ContactUs extends AbstractHelper
{
  public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $ScopeConfigInterface
    ) {
        parent::__construct($context);
        $this->scopeConfigInterface = $ScopeConfigInterface;
	}
	
  public function getContactUsArray()
  {
    $contact_info=[];
    $phone_1 = $this->scopeConfigInterface->getValue('contact_us/telephone/one');
    $phone_2 = $this->scopeConfigInterface->getValue('contact_us/telephone/two');
    
    $mobile_1 = $this->scopeConfigInterface->getValue('contact_us/mobile/one');
    $mobile_2 = $this->scopeConfigInterface->getValue('contact_us/mobile/two');
    
    $email_1 = $this->scopeConfigInterface->getValue('contact_us/email/one');
    $email_2 = $this->scopeConfigInterface->getValue('contact_us/email/two');

    if($phone_1 || $phone_2)
    {
      $contact_info['phone']=array("phone_one"=>$phone_1,"phone_two"=>$phone_2);
    } 

    if($mobile_1 || $mobile_2)
    {
      $contact_info['mobile']=array("mobile_one"=>$mobile_1,"mobile_two"=>$mobile_2);
    }
    
    if($email_1 || $email_2)
    {
      $contact_info['email']=array("email_one"=>$email_1,"email_two"=>$email_2);
    }

    return $contact_info;
  }
   
}















?>