<?php

namespace OM\Rewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Shipment extends AbstractHelper
{
    public function updaeteFromEcom()
    {

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $_resource   =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
        // $q = "SELECT shipping_and_handling, ecom_order, delivery_date, status,entity_id, increment_id FROM `sales_order_grid` WHERE status NOT IN('Delivered','closed','canceled') AND entity_id IN (select order_id from om_ecom_api_status)";
        $q = "SELECT shipping_and_handling, ecom_order, a.delivery_date, a.status,b.entity_id, a.increment_id FROM `sales_order_grid` as a left join `sales_order` as b on a.entity_id = b.entity_id WHERE (a.status NOT IN('closed','canceled','pending','pending_payment') and b.state NOT IN('closed') AND a.entity_id IN (select order_id from om_ecom_api_status WHERE order_status in ('processing','Shipped','New') and  shipping_type='Ecom'))";
        $results = $connection->fetchAll($q);
        
        foreach ($results as $result) 
        {
            $order_id    = $result['entity_id'];
            $ship_charge = (int)$result['shipping_and_handling'];
            $ecom_order = $result['ecom_order'];
          
            //print_r($result);exit;

            if($ecom_order=="Yes")
            { 
                $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
                $api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
                $modearray = json_decode($api_helpers->get_razorpay_mode());
                $mode = $modearray->mode;
                $username = $modearray->username;
                $password = $modearray->password;

                $results = $connection->fetchAll("select * from ecomexpress_awb where orderid='".$order_id."'");
                if(!empty($results))
                {  
                  
                    foreach($results as $row)
                    {  
                        $awb   = $row['awb'];
                        $shipment_to   = $row['shipment_to'];
                        $status= "processing"; 
                        $pickup_date = ''; 
                        $delivered_dates='';
                        if($mode=="dev")  {
                            $url='https://clbeta.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;   
                        }else {
                           $url='https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;    
                        }
                                                
                        $stin = @file_get_contents($url);    
                        $hastag = strpos($stin,"</object"); 
                        $openingtag = strpos($stin,"<object");  
                        if(!$hastag && $openingtag){    
                            $stin = str_ireplace("</ecomexpress-objects>","</object></ecomexpress-objects>",$stin);                             
                        }   
                        $xmlobj = simpleXML_load_string($stin,"SimpleXMLElement",LIBXML_NOCDATA);                
                        $xmlobj = (array)$xmlobj;
                        if(!empty($xmlobj['object']->field))
                        {   
                            $json = (array)$xmlobj['object']->field;      
                            $pickup = $json[9];                   
                            if(!is_object($pickup)){
                                $shipped = 1; 
                                $status = "Shipped";
                                $pickup_date = $pickup;   
                                /* send sms notification */
                                $this->sendSms($order_id, $connection, $status,$shipment_to,$awb);
                            }       
                            
                            $delivered_date = $json[21];
                            if(!is_object($delivered_date)){
                                $delivered = 1;   
                                $status = "Delivered";
                                $delivered_dates = $delivered_date;
                                /* send sms notification */
                                $this->sendSms($order_id, $connection, $status,$shipment_to,$awb);
                            }
                            /* check if awb has become rto @ritesh 26nov2021 */
                            if($json[14]=='777')
                            {
                                $rtoHelper = $objectManager->create('OM\EcomexpressRto\Helper\AwbStatusHelper');
                                $istoindatabase = $rtoHelper->checkAwbRtoInDatabase($awb);
                                if(!$istoindatabase)
                                {
                                    $rtoHelper->registerRtoToDatabase($awb,$order_id);
                                } 
                            } 
                                
                            $updateQuery = "UPDATE om_ecom_api_status SET order_status = '$status',pickup_date = '$pickup_date',delivery_date = '$delivered_dates',awb_number = '$awb' WHERE order_id = '".$order_id."' and movement_type='Forward'";
                            $connection->query($updateQuery);  
                            
                            $quewrrsds = "update om_sales_report set pickup_date='$pickup_date',delivery_date='$delivered_dates',`forward_awb`='$awb',`order_status`='$status' where order_id='$order_id' and report_type='1'";
					        $connection->query($quewrrsds);

                            
                            $iscomplete = $connection->fetchOne("SELECT state FROM `sales_order` WHERE entity_id = '".$order_id."' ");
                            if($iscomplete=='complete'){
                                if($pickup_date){
                                    $pickup_date = date('Y-m-d H:i:s',strtotime($pickup_date));
                                }
                                $connection->query("UPDATE `sales_order_grid` SET status='$status' WHERE entity_id='".$order_id."'");
                                $connection->query("UPDATE `sales_order` SET `status`='$status', pickup_date='$pickup_date',delivery_date='$delivered_dates' WHERE  entity_id='$order_id' ");
                            }
                            
                        }
                    }
                }
            } else {
                $status = $connection->fetchOne("SELECT status FROM sales_order_grid WHERE entity_id='".$order_id."'");
                $pickup_date = $connection->fetchOne("SELECT delivery_date FROM sales_order_grid WHERE entity_id='".$order_id."'");

                $orderStatus = $connection->fetchOne("SELECT order_status FROM om_ecom_api_status WHERE order_id='".$order_id."'");
                
                /* send sms notification */
                if($orderStatus != $status) {
                    $this->sendSmsForNonEcom($order_id, $connection, $status);
                }
                $state = $connection->fetchOne("SELECT `state` FROM sales_order WHERE entity_id='".$order_id."'");
                if($state!="complete")
                {
                    $updateQuery = "UPDATE om_ecom_api_status SET order_status = '$status' WHERE order_id='".$order_id."' and movement_type='Forward' ";
                    $connection->query($updateQuery);

                    $quewrrsds = "update om_sales_report set pickup_date='$pickup_date',`order_status`='$status' where order_id='$order_id' and report_type='1'";
                    $connection->query($quewrrsds);
                }
            }
        }
        $this->updateReverseShipment();
        $this->updateExchangeShipment();
    }

    public function sendSms($orderId, $connection, $status,$shipment_to,$awb)
    {
        if($orderId) {
            $_status = $connection->fetchOne("SELECT status FROM `sales_order_grid` WHERE entity_id='".$orderId."'");
            if($_status != $status) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
                $cId = $order->getCustomerId();
                if($cId) {
                    $mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$cId'");
                    if($mobile) {
                        $smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
                        if($status == 'Shipped') {
                            $msg = "Shipment for your order number ".$order->getIncrementId()." is picked up successfully.";
                            $msg = "Dear $shipment_to, shipment for your GetMySpares order ".$order->getIncrementId()." is dispatched. Track Shipment on https://ecomexpress.in with AWB $awb";
                            $smsHelper->send_custom_sms($mobile, $msg);
                            
                        } elseif ($status == 'Delivered') {
                            
                            // $msg = "Your order ".$order->getIncrementId()." is delivered successfully.";
                            $msg = "Dear $shipment_to, your GetMySpares order ".$order->getIncrementId()." is delivered successfully. Please rate your order experience on GetMySpares";
                            $smsHelper->send_custom_sms($mobile, $msg);
                        }
                    }
                }
            }
        }
    }

    public function sendSmsForNonEcom($orderId, $connection, $status)
    {
        if($orderId) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
            $cId = $order->getCustomerId();
            if($cId) {
                $mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$cId'");
                if($mobile) {
                    $smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
                    if($status == 'Shipped') {
                        $msg = "Shipment for your order number ".$order->getIncrementId()." is picked up successfully.";
                        $smsHelper->send_custom_sms($mobile, $msg);
                        
                    } elseif ($status == 'Delivered') {
                        $msg = "Your order ".$order->getIncrementId()." is delivered successfully.";
                        $smsHelper->send_custom_sms($mobile, $msg);
                    }
                }
            }
        }
    }

    public function awbTracking(int $awb)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
        $modearray = json_decode($api_helpers->get_razorpay_mode());
        $mode = $modearray->mode;
        $username = $modearray->username;
        // $username = "panasonicpvtltd402639_pro";
        $password = $modearray->password;
        // $password = "6bk2LrZpC7UXhuZe";
        if($mode=='dev')
        {
            $url="https://clbeta.ecomexpress.in";
        } else 
        {
          $url="https://plapi.ecomexpress.in";   
        }
        


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url/track_me/api/mawbd/?awb=$awb&username=$username&password=$password",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $xml=@simplexml_load_string($response,"SimpleXMLElement",LIBXML_NOCDATA); 
        if ($xml === false) {
            return  array("status"=>0,"data"=>array());
            
        } else {
            $status_object = @$xml->object->field[36];
            $pickupdate = @$xml->object->field[9];
            $deliverydate = @$xml->object->field[21];
            $expected_date = @$xml->object->field[18];
            $expected_date = (string)$expected_date;
            if(empty($status_object))
            {
            return  array("status"=>0,"data"=>array());
            }
            $count= $status_object->count();
            $awb_track_array = array();
            /* using for loop for a change  , reminds me of C language again @ritesh*/
            for(--$count;$count>=0;$count--)
            { 
                $field = json_decode(json_encode($status_object->object[$count]),true)['field']; 
                $status = is_string($field[1])?$field[1]:"";
                $awb_track_array[]= ['date'=>$field[0],'status'=>$status,'location'=>$field[8]];
            }
            return  array("status"=>1,"data"=>$awb_track_array,"completion_status"=>array('pickup_date'=>$pickupdate,'delivery_date'=>$deliverydate,"expected_date"=>$expected_date));
        }
    }


    public function awbInfo($awb)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
        $modearray = json_decode($api_helpers->get_razorpay_mode());
        $mode = $modearray->mode;
        $username = $modearray->username;
        // $username = "panasonicpvtltd402639_pro";
        $password = $modearray->password;
        // $password = "6bk2LrZpC7UXhuZe";
        if($mode=='dev')
        {
            $url="https://clbeta.ecomexpress.in";
        } else 
        {
          $url="https://plapi.ecomexpress.in";   
        }
        


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url/track_me/api/mawbd/?awb=$awb&username=$username&password=$password",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $xml=@simplexml_load_string($response,"SimpleXMLElement",LIBXML_NOCDATA); 
        
        
        if ($xml === false) {
            return  array("status"=>0,"completion_status"=>array());
            
        } else {

            $pickupdate = @$xml->object->field[9];
            $deliverydate = @$xml->object->field[21];
            $pickupdate= (string)$pickupdate;
            $deliverydate= (string)$deliverydate;
            return  array("status"=>1,"completion_status"=>array('pickup_date'=>$pickupdate,'delivery_date'=>$deliverydate));
        }
    }

    

    public function updateReverseShipment()
    {

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $_resource   =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
        $sql =  "Select * from ecomexpress_awb_reverse where (delivery_date ='' or delivery_date is null)  and (orderid!='' or orderid is not null)";
        $results = $connection->fetchAll($sql);
        $smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
        if(!empty($results))
        {
            foreach ($results as  $value) {
                $awb = (int)$value['awb'];
                $awb_id = $value['awb_id'];
                $rma_id = $value['rma_id'];
                $ecom_status = $this->awbInfo($awb);
                if($ecom_status['status'])
                {
                    $completion_status = $ecom_status['completion_status'];
                    $pickup_date = $completion_status['pickup_date'];
                    $delivery_date = $completion_status['delivery_date'];
                    if(!empty($pickup_date))
                    {
                        $sql = "update ecomexpress_awb_reverse set pickup_date='$pickup_date' where awb_id='$awb_id'";
                        $results = $connection->query($sql);

                    }
                    if(!empty($delivery_date))
                    {
                        $sql = "update ecomexpress_awb_reverse set delivery_date='$delivery_date', track_status='delivered' where awb_id='$awb_id'";
                        $results = $connection->query($sql);

                        $sql = "update wk_rma set status='13' where rma_id='$rma_id'";
                        $results = $connection->query($sql);


                        $sql = "select * from wk_rma where rma_id='$rma_id'";
                        $results = $connection->fetchAll($sql);
                        if(!empty($results))
                        {
                            $order_id = $results[0]['increment_id']; 
                            $customer_name = $results[0]['name']; 
                            $customer_id = $results[0]['customer_id']; 
    
                            $query = "SELECT seller_id FROM `marketplace_orders` where order_id='$order_id'";
                            $seller_id=$connection->fetchOne($query);
    
                            $seller_mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$seller_id'");
    
                            $msg = "Dear Seller,Reverse Pickup of  your GetMySpares order $order_id has been completed ,Item has been delivered back , Please Process the rma";
                            $smsHelper->send_custom_sms($seller_mobile, $msg);
    
                            $mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$customer_id'");
    
                            $msg = "Dear $customer_name, Reverse Pickup of  your GetMySpares order $order_id has been completed ,Item has been delivered back to seller";
                            $smsHelper->send_custom_sms($mobile, $msg);
                        }
                    }
                }
            }
        }
    }

    public function updateExchangeShipment()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $_resource   =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
        // $sql =  "Select * from ecomexpress_awb_exchange where delivery_date !='' or delivery_date is null  and orderid!='' or orderid is not null";
        $sql =  "Select * from ecomexpress_awb_exchange where (delivery_date ='' or delivery_date is null)  and (orderid!='' or orderid is not null)";
        $results = $connection->fetchAll($sql);
        $smsHelper = $objectManager->create('OM\MobileOtp\Helper\SmsHelper');
        $mailHelper = $objectManager->create('Panasonic\CustomUser\Helper\Data');

        if(!empty($results))
        {
            foreach ($results as  $value) {
                $awb = (int)$value['awb'];
                $id = $value['awb_id'];
                $ecom_status = $this->awbInfo($awb);
                $completion_status = $ecom_status['completion_status'];
                $pickup_date = $completion_status['pickup_date'];
                $delivery_date = $completion_status['delivery_date'];
                $rma_id = $value['rma_id'];
                if($ecom_status['status'])
                {
                    if(!empty($pickup_date))
                    {
                        $sql = "update ecomexpress_awb_exchange set pickup_date='$pickup_date' where awb_id='$awb'";
                        $results = $connection->query($sql);
                    }

                    if(!empty($delivery_date))
                    {
                        $sql = "update ecomexpress_awb_exchange set delivery_date='$delivery_date', track_status='delivered' where awb_id='$id'";
                        $results = $connection->query($sql);


                        $sql = "update wk_rma set status='14' where rma_id='$rma_id'";
                        $results = $connection->query($sql);

                        $sql = "select * from wk_rma where rma_id='$rma_id'";
                        $results = $connection->fetchAll($sql);
                        if(!empty($results))
                        {
                            $order_id = $results[0]['increment_id']; 
                            $customer_name = $results[0]['name']; 
                            $customer_id = $results[0]['customer_id']; 
                            $order_entity_id = $results[0]['order_id']; 

                            $sql="update `om_sales_report` set exchange_awb_delivery_date='$delivery_date' where order_id='$order_entity_id'
                            ";
                            $connection->query($sql);

                            $query = "SELECT seller_id FROM `marketplace_orders` where order_id='$order_id'";
                            $seller_id=$connection->fetchOne($query);
                            $seller_mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$seller_id'");
    
    
                            $msg = "Dear Seller,Exchange of  your GetMySpares order $order_id has been completed ,Item has been delivered to the customer";
                            $smsHelper->send_custom_sms($seller_mobile, $msg);
    
                            $mobile = $connection->fetchOne("SELECT value FROM `customer_entity_varchar` WHERE `attribute_id` = 178 AND entity_id = '$customer_id'");

                            $msg = "Dear $customer_name, Reverse Pickup of  your GetMySpares order $order_id has been completed ,Item has been delivered back to seller";
                            $smsHelper->send_custom_sms($mobile,$msg);

                            $msg_for_panasonic_team = "Dear Seller,Exchange of your GetMySpares order $order_id has been completed ,Item has been delivered to the customer
                            $customer_name ";
                            $to=['ombugtest123456@gmail.com','pankaj.kumar06@in.panasonic.com','subhankar.lg@gmail.com','sasmitanupurthapa@infinityeservices.com'];
                            $from=$objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_custom2/email');;
                            $subject="Exchange Complete | Order $order_id";
                            $mailHelper->sendEmailThroughCurl($to,$from,$msg_for_panasonic_team,$subject);

                        }

                    }
                }
            }
        }
    }
}