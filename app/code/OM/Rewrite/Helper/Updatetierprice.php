<?php
namespace OM\Rewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Api\Data\ProductTierPriceInterface;
use Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory;
use Magento\Catalog\Api\ScopedProductTierPriceManagementInterface;

class Updatetierprice extends AbstractHelper
{


	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
    ScopedProductTierPriceManagementInterface $tierPriceManagement,
    ProductTierPriceInterfaceFactory $productTierPriceFactory,
		\Magento\Framework\ObjectManagerInterface $_objectManager
    ) {
        parent::__construct($context);
        $this->tierPriceManagement = $tierPriceManagement;
        $this->productTierPriceFactory = $productTierPriceFactory;
        $this->_objectManager = $_objectManager;
	}
	
    public function updateTierPrice()
    {

      $appState = $this->_objectManager->get('\Magento\Framework\App\State');
      $tierPrices[] = array(
        'website_id'  => 0,
        'cust_group'  => 32000,
        'price_qty'   => 60,
        'price'       => 6,
        'ship_ecom'       => 1,
        'shipping_free' => 0
      );
      $tierPrices[] = array(
        'website_id'  => 0,
        'cust_group'  => 32000,
        'price_qty'   => 40,
        'price'       => 6,
        'ship_ecom'       => 1,
        'shipping_free' => 0
      );

      $sku="TNP4G610DB";
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
      $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
      $product = $productRepository->get($sku);


    //  $product_id = 19794;    //PASS YOUR PRODUCT ID HERE   
    //  $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
     $product->setTierPrice($tierPrices);
     $product->save(); 
    }
}



?>