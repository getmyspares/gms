<?php
namespace OM\Rewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Fetchppdawb extends AbstractHelper
{


	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Framework\ObjectManagerInterface $_objectManager
    ) {
        parent::__construct($context);
        $this->_objectManager = $_objectManager;
        $this->connection = $resourceConnection->getConnection();
	}
	
    public function checkandfecthppdawb()
    {
			$count = $this->getFreeAwbCount();        
			if($count<50)
			{
					$this->fetchawb();
			} else 
			{
					echo "no need to fetch awb";
			}
    }

		public function checkIfAwbUsed($awbIds)
    {
			if(is_array($awbIds) && !empty($awbIds))
			{
					foreach($awbIds as $awb_id)
					{	
        		$sql = "SELECT * from ecomexpress_awb where  awb_id ='$awb_id'";
						$awb_data = $this->connection->fetchAll($sql); 
						$shipment_id = $awb_data[0]['shipment_id'];
						$orderid = $awb_data[0]['orderid'];
						$status = $awb_data[0]['status'];
						$manifest = $awb_data[0]['manifest'];
						$awb = $awb_data[0]['awb'];

						if(!empty($shipment_id) || !empty($orderid) || !empty($status) || $manifest=='1')
						{
							$msg="AWB : $awb is already assigned . Only unsed awb can be reserved for further use";
							return [true,$msg];
						}
					}
			}
			return [false,"success"];
    } 
		

		public function getFreeAwbCount()
    {
			$sql = "SELECT COUNT(awb) from ecomexpress_awb where  manifest  is null and awb_type='PPD' and status=''";
			return  $this->connection->fetchOne($sql);   
    } 
		public function manuallyAssing($awbIds)
    {
			if(is_array($awbIds) && !empty($awbIds))
			{
					foreach($awbIds as $awb_id)
					{	
						$sql = "update `ecomexpress_awb` set `status`='Assigned' ,manifest=1,shipment_to='Manually Shipped',state=1,track_status='dl' where  awb_id ='$awb_id'";
						$awb_data = $this->connection->query($sql); 
					}
			}
    } 

		
    public function fetchawb(){
        $configvalue = $this->_objectManager->get ( '\Magento\Framework\App\Config\ScopeConfigInterface' );
			$params = array();
			$params['type'] = "PPD";
			$model = $this->_objectManager->get('Ecom\Ecomexpress\Model\Awb');
			$count = $model->getCollection()->addFieldToFilter('awb_type',$params['type'])->addFieldToFilter('state',0)->getData();
			$configvalue=$this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
			$fetch_awb = $configvalue->getValue('carriers/ecomexpress/fetch_awb');
			$max_fetch_awb = $configvalue->getValue('carriers/ecomexpress/max_fetch_awb');				
				if ($fetch_awb <= $max_fetch_awb) {		
					$type = 'post';
					$params['username'] = $configvalue->getValue('carriers/ecomexpress/username');
					$params['password'] = $configvalue->getValue('carriers/ecomexpress/password');
					$params['count'] = $fetch_awb;
					if($configvalue->getValue('carriers/ecomexpress/sanbox')==1){
						$url = 'https://clbeta.ecomexpress.in/apiv2/fetch_awb/';
					}
					else {
						$url = 'https://api.ecomexpress.in/apiv2/fetch_awb/';
					}		
							
					if($params)
					{
						$helper = $this->_objectManager->get('Ecom\Ecomexpress\Helper\Data');
						$retValue = $helper->execute_curl($url,$type,$params);
						
						if (!$retValue){
                            echo 'Ecom service is currently Unavilable , please try after sometime';
						}
						$awb_codes = json_decode($retValue);
						if (empty($awb_codes))
						{
                            echo 'Please add valid Username,Password and Count in plugin configuration';
						}		
						foreach ($awb_codes->awb as  $key => $item)
						{
							try {
								$data = array();
								$data['awb'] = $item;
								$data['state'] = 0;
								$data['awb_type'] = $params['type'];
								$datefinder=$this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime');
								$date = $datefinder->date ();
								$data['created_at'] = $date;
								$data['updated_at'] = $date;
								$model->setData($data);
								$model->save();
							}
							catch (\Exception $e)
							{
								echo 'Caught exception: ',  $e->getMessage(), "\n";
							}
                        }	
                        // echo 'AWB Downloaded Successfully';
					}
			}
    	

	}
}



?>