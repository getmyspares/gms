<?php
namespace OM\Rewrite\Ui\Component\Listing\Column\Customer;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\App\ResourceConnection;

class Customergst extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $_resource;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ResourceConnection $resource,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->_resource = $resource;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $connection = $this->_resource->getConnection();
            $attribute_id = $connection->fetchOne("SELECT `attribute_id` FROM `eav_attribute` WHERE attribute_code ='gst_number'");
            foreach ($dataSource['data']['items'] as & $item) {
                $entity_id = $item['entity_id'];
                $gst_number = $connection->fetchOne("SELECT `value` FROM `customer_entity_varchar` where attribute_id='$attribute_id' and entity_id='$entity_id'");
                if(empty($gst_number))
                {
                    $gst_number="";
                }
                $item[$this->getData('name')] = $gst_number;
            }
        }
        return $dataSource;
    }
}
