<?php

namespace OM\ContentParser\Helper;
 
use \Magento\Framework\App\Helper\AbstractHelper;
 
class Data extends AbstractHelper
{
    
    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;
 
 
    /**
     * @param \Magento\Backend\Block\Template\Context    $context
     * @param array                                      $data
     */
    public function __construct(
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Framework\App\Helper\Context $context,
        array $data = []
    ) {
        $this->_filterProvider = $filterProvider;
        parent::__construct($context);
    }
 
    
    /**
     * Prepare HTML content
     *
     * @return string
     */
    public function getCmsFilterContent($content)
    {
        $html = $this->_filterProvider->getBlockFilter()->filter($content);
        return $html;
    }
}