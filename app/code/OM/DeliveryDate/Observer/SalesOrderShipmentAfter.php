<?php

namespace OM\DeliveryDate\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderShipmentAfter implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $shipment->getOrder();
        $order_id = $order->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
        $shipmentcreateddate = $objDate->gmtDate();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

		$isEcom = $connection->fetchOne("SELECT ecom_order FROM `sales_order_grid` where entity_id =". $order_id);

        if($isEcom == 'Yes'){
			$iscomplete = $connection->fetchOne("SELECT state FROM `sales_order` WHERE entity_id = '".$order_id."' ");
			if($iscomplete=='complete'){
				$query1="Update sales_order Set pickup_date ='$shipmentcreateddate' ,delivery_date='$shipmentcreateddate' where entity_id= '$order_id'";
				$connection->query($query1);
				$query2 = "Update sales_order_grid Set delivery_date='$shipmentcreateddate' where entity_id='$order_id'";
				$connection->query($query2);
			}
        }

    }
}
