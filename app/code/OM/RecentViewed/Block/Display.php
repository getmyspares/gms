<?php
namespace OM\RecentViewed\Block;
class Display extends \Magento\Framework\View\Element\Template
{
    protected $recentlyViewed;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Reports\Block\Product\Viewed $recentlyViewed,
        array $data = []
    ) {
        $this->recentlyViewed = $recentlyViewed;
        parent::__construct( $context, $data );
    }

    public function getMostRecentlyViewed(){
       return $this->recentlyViewed->getItemsCollection()->load();
    }
}