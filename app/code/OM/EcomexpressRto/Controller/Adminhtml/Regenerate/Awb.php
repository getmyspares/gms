<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Controller\Adminhtml\Regenerate;

use Ecom\Ecomexpress\Model\AwbFactory;
use \Magento\Framework\App\Request\Http;
use \Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Controller\ResultFactory;
use OM\EcomexpressRto\Helper\AwbStatusHelper AS Data;

class Awb extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $_publicActions = ['awb'];

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        AwbFactory $awbCollection,
        Http $request,
        ManagerInterface $messageManager,
        Data $helperData,
        array $data = []
    ) {
        $this->awbCollection = $awbCollection;
        $this->request = $request;
        $this->messageManager = $messageManager;
        $this->helperData = $helperData;
        parent::__construct($context);
    }
    

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $orderid = $this->request->getParam('order_id');
        $status =$this->helperData->regenrateAwb($orderid);
        if(!empty($status))
        {
            if(isset($status['response']))
            {
                $message = $status['response'];
                if($status['status'])
                {
                    $this->messageManager->addSuccess($message);
                } else 
                {
                    $this->messageManager->addError($message);
                }
            }
        }
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('OM_EcomexpressRto::rto');
    }
}

