<?php
/**
 * Copyright orangemantra ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OM\EcomexpressRto\Helper\AutomatedMails;

use Magento\Framework\App\Helper\AbstractHelper;

class RtoInfoToSeller extends AbstractHelper
{
	public function __construct(
    \Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\App\ResourceConnection $resource,
    \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory $marketplaceOrderCollectionFactory,
		\Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $seller,
    \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
		\Panasonic\CustomUser\Helper\Data $panasonicHelper,
    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
		\OM\EcomexpressRto\Model\ResourceModel\Rto\CollectionFactory $rtoCollectionFactory,
		\OM\GenerateReport\Helper\Mail $mail,
		\Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
		$this->_objectManager = $objectmanager;
		$this->marketplaceOrderCollectionFactory = $marketplaceOrderCollectionFactory;
		$this->rtoCollectionFactory = $rtoCollectionFactory;
		$this->panasonicHelper=$panasonicHelper;
		$this->customerRepository = $customerRepositoryInterface;
		$this->mail = $mail;
    $this->orderRepository = $orderRepository;
		$this->connection = $resource->getConnection();
    parent::__construct($context);
	}

	public function sendRtoMailsToSeller()  
	{
    $seller_rto_array=[];
		$rtoCollection = $this->rtoCollectionFactory->create();
    $rtoCollection->addFieldToSelect(['rto_awb','rto_id', 'order_id']);
		$rtoCollection->addFieldToFilter('is_regenerated', ['eq' => '0']);
    foreach($rtoCollection as $rtoAwbs)
		{
			$order_id = $rtoAwbs->getData('order_id');
			$rto_id = $rtoAwbs->getData('rto_id');
      $orderDataCollection = $this->marketplaceOrderCollectionFactory->create();
      $orderDataCollection->addFieldToSelect(['seller_id']);
      $orderDataCollection->addFieldToFilter('order_id', ['eq' => $order_id]);
      if($orderDataCollection->count())
      {
        $orderData = $orderDataCollection->getFirstItem();
        $seller_id = $orderData->getData('seller_id');
        $seller_rto_array[$seller_id][] = $rto_id; 
      }
    }

    foreach ($seller_rto_array as $seller_id=>$rto_id_array) {
      $sellerData = $this->getSellerData($seller_id);
      $selller_email = $sellerData['email'];
			$columns = $this->getRtoColumns();
			$csvHeader = array_values($columns);
			$filename = 'Rto Awb Report-'.date('Y-m-d-H-i-s').'.csv';
			$final_text_data="";
			$fp = fopen('php://memory', 'w');
			fputcsv( $fp, $csvHeader,",");
			foreach ($rto_id_array as $rto_id) {
				echo "<pre>";
				$insertArr = $this->generateRtoRow($rto_id,$seller_id);
				fputcsv($fp, $insertArr, ",");
			}
			fseek($fp, 0);
			$file = stream_get_contents($fp);
			fclose($fp);
      $from = $this->panasonicHelper->getFromEmail(); 
			$to=$this->rtoReportEmail();
			// $to[]=$selller_email;
			$subject="Awb Rto";
			$msg="Please Regenarate Rto Awb's";
			$this->mail->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
		}
	}

  public function rtoReportEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/rto_report/email'));
	}

  public function generateRtoRow($rto_id,$seller_id)
  {
		$sellerData = $this->getSellerData($seller_id);
		$selller_email = $sellerData['email'];

    $rtoCollection = $this->rtoCollectionFactory->create();
    $rtoCollection->addFieldToSelect('*');
		$rtoCollection->addFieldToFilter('rto_id', ['eq' => $rto_id]);
    $rtoData = $rtoCollection->getFirstItem();
    $order_id = $rtoData->getData('order_id');
    $order = $this->orderRepository->get($order_id);
    $increment_id = $order->getIncrementId();

		$insertArr[] = $rtoData->getData('rto_awb'); 
		$insertArr[] = $increment_id; 
		$insertArr[] = $selller_email; 
		$insertArr[] = $rtoData->getData('created_at'); 
		return $insertArr;
  }

  public function getSellerData($customer_id)
	{
		$sellerData=[];
    $customerData = $this->customerRepository->getById($customer_id);
    $email = $customerData->getEmail();
		$sellerData['email'] = $email; 
		return $sellerData;
	}

  public function getRtoColumns()
	{
		return ["Rto Awb","Order Increment Id","Seller Email","Rto Date"];
	}

}

