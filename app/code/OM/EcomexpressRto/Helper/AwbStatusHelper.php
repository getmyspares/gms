<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class AwbStatusHelper extends AbstractHelper
{
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Customm\Apii\Helper\Data $helperData,
        \Ecom\Ecomexpress\Model\ResourceModel\Awb\CollectionFactory $pincodeCollectionFactory
      )
      {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->pincodeCollectionFactory = $pincodeCollectionFactory;
      }


    public function regenrateAwb($orderid)
    {
      return $this->regenerateRtoAwb($orderid);
    }
  
    /**
     * get rto status by order id
     *
     * @param [string] $order_id
     * @return boolean
     */
    public function isOrderRto($order_id)
    {
      $status=false;
      $awbCollection = $this->pincodeCollectionFactory->create();
      $awbCollection->addFieldToSelect('*');
      $awbCollection->addFieldToFilter('orderid',['eq'=>$order_id]);
      
      if($awbCollection->count())
      {
        $ecomexpress_awb = $awbCollection->getFirstItem();
        $awb = $ecomexpress_awb->getAwb();

        $isPresentInRtOTable = $this->checkAwbRtoInDatabase($awb);
        
        if($isPresentInRtOTable)
        {
          return $isPresentInRtOTable;
        }

        if($awb){
          $status =  $this->isAwbRtoInEcom($awb);     
          if($status)
          {
            $this->registerRtoToDatabase($awb,$order_id);
          }
        }
      
      } 
      return $status;
    }
     
    /**
     * get awb rto status by awb number 
     *
     * @param [string] $awb
     * @return boolean
     */
    public function isAwbRtoInEcom($awb)
    {
      $isRto=false;
      $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
      $api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
      $modearray = json_decode($api_helpers->get_razorpay_mode());
      // $modearray = json_decode($this->helperData->get_razorpay_mode());
      $mode = $modearray->mode;
      $username = $modearray->username;
      $password = $modearray->password;
  
      if($mode=="dev")  {
        $url='https://clbeta.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;   
      }else {
        $url='https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;    
      }
      $stin = @file_get_contents($url);    
      $hastag = strpos($stin,"</object"); 
      $openingtag = strpos($stin,"<object");  
      if(!$hastag && $openingtag){    
          $stin = str_ireplace("</ecomexpress-objects>","</object></ecomexpress-objects>",$stin);                             
      }   
      $xmlobj = simpleXML_load_string($stin,"SimpleXMLElement",LIBXML_NOCDATA);                
      $xmlobj = (array)$xmlobj;
      if(!empty($xmlobj['object']->field))
      {   
        $json = (array)$xmlobj['object']->field;  
        if($json[14]=='777')
        {
          $isRto=true;
          return $isRto; 
        } 
      }
      return $isRto;
    }
    /**
     * Insert  Rto Awb to  database
     *
     * @param string $awb
     * @param int $order_id
     * @return boolean
     */  
    public function registerRtoToDatabase($awb,$order_id)
    {
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
      $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
      $connection = $resource->getConnection();
  
      $query =  "select rto_id from om_ecomexpressrto_rto where rto_awb='$awb'";
      $alreadyRegistered = $connection->fetchOne($query);

      $query =  "select increment_id from sales_order where entity_id='$order_id'";
      $increment_id = $connection->fetchOne($query);

      if(empty($alreadyRegistered))
      {
        $query =  "Insert into om_ecomexpressrto_rto set 
          rto_awb = '$awb',
          order_id = '$order_id',
          is_regenerated = '0',
          increment_id = '$increment_id',
          visible_to_seller = '0',
          new_active_awb = '0'
        ";
        $connection->query($query);
        return true;
      }
    }

    /**
     * check rto in database
     *
     * @param string $awb
     * @return boolean
     */
    public function checkAwbRtoInDatabase($awb)
    {
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
      $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
      $connection = $resource->getConnection();
  
      $query =  "select rto_id from om_ecomexpressrto_rto where rto_awb='$awb'";
      $alreadyRegistered = $connection->fetchOne($query);

      if($alreadyRegistered)
      {
        return true;
      }
      return false;
    }

    public function regenerateRtoAwb($orderid)
    {
       
      $awb_type='PPD';
      $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
      $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
      $connection = $resource->getConnection();
      $state = $objectManager->get('Magento\Framework\App\State');
  
      $sql="select awb from ecomexpress_awb where `orderid`='' and `status`='' and manifest is null limit 1";		  
      $new_awb = $connection->fetchOne($sql); 
      $awb=$new_awb;
  
      //$state->setAreaCode('global');
      $custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
      $api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
      $helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
      
      $order_id=$orderid;
      
       
      $order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id); 
      $order_inc_id=$order->getIncrementId(); 
  
  
        
      $user_id = $order->getCustomerId();
  
      $delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
      $delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
  
      $delivery_name=$delivery_firstname.' '.$delivery_lastname; 
      $delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
      $delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
      $delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
      $delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
      $delivery_telephone = $custom_helpers->get_customer_order_adress($order_id,'telephone','shipping');
      $delivery_street = $custom_helpers->get_customer_order_adress($order_id,'street','shipping');
        
      $street=explode(PHP_EOL,$delivery_street);
  
      $street_1='';
      $street_2='';
      $street_3='';
  
      if(isset($street[0]))
      {
        $street_1=$street[0];
      }
      if(isset($street[1]))
      {
        $street_2=$street[1];
      }
      if(isset($street[2]))
      {
        $street_3=$street[2];
      }	
  
  
      $invoice_id=$helpers->get_order_invoice_id($order_inc_id); 
  
      $orderArray=$order->getData();
      /*
      echo "<pre>";
          print_r($orderArray);
      echo "</pre>";  
      */
    
      $order_cr_date=$order->getCreatedAt();
      foreach ($order->getAllItems() as $item)
      {
        $product_id=$item->getProductId();
        $product_title=$item->getName();
        $product_qty=$item->getQtyOrdered();
        /* echo "<pre>";
          print_r($item->getData());
        echo "</pre>";   */
        
        $itemarray=$item->getData();
         
        $row_total_incl_tax=$itemarray['row_total_incl_tax'];
        $weight=$itemarray['weight'];
        
        
        $product_length=$custom_helpers->get_product_length($product_id);
        $product_breath=$custom_helpers->get_product_breath($product_id);
        $product_height=$custom_helpers->get_product_height($product_id);
        
        $item_ids=$item->getId();
              $seller_id=$custom_helpers->get_product_seller($order_id,$item_ids);
        //$seller_id=$custom_helpers->get_product_seller($order_id,$product_id);
        
        
        $sellerarray=$custom_helpers->get_seller_details($seller_id);
        
        //print_r($sellerarray);
        
        
        $seller_name=$sellerarray['seller_name'];
        $seller_comp_nam=$sellerarray['seller_comp_nam'];
        $seller_comp_address=$sellerarray['seller_comp_address'];
        $seller_comp_mobile=$sellerarray['seller_comp_mobile'];
        $zipcode=$sellerarray['zipcode'];
        $seller_gst=$sellerarray['seller_gst'];
        
        
        $cgst_amount=$itemarray['cgst_amount'];
        $sgst_amount=$itemarray['sgst_amount'];
        $igst_amount=$itemarray['igst_amount'];
        
        $cgst_percent=$itemarray['cgst_percent'];
        $sgst_percent=$itemarray['sgst_percent'];
        $igst_percent=$itemarray['igst_percent'];
        
        $total_gst=$cgst_amount+$sgst_amount+$igst_amount;
        
        
        $additional_info=array(
          'INVOICE_NUMBER'=>$invoice_id,
          'INVOICE_DATE'=>$order_cr_date,
          'SELLER_GSTIN'=>$seller_gst, 
          'GST_HSN'=>$order_cr_date,
          'GST_TAX_NAME'=>'DELHI GST',
          'GST_TAX_BASE'=>$total_gst,
          'DISCOUNT'=>0,
          'GST_TAX_RATE_CGSTN'=>$cgst_percent,
          'GST_TAX_RATE_SGSTN'=>$sgst_percent,
          'GST_TAX_RATE_IGSTN'=>$igst_percent,
          'GST_TAX_TOTAL'=>$total_gst, 
          'GST_TAX_CGSTN'=>$cgst_amount,
          'GST_TAX_SGSTN'=>$sgst_amount,
          'GST_TAX_IGSTN'=>$igst_amount
        );
        $collect_amount=0;
        if($awb_type!='PPD')
        {
          $collect_amount=$row_total_incl_tax;
        }
        $awbarray[]=array( 
          'AWB_NUMBER'=>$awb,
          'ORDER_NUMBER'=>$order_inc_id,
          'PRODUCT'=>$awb_type,
          'CONSIGNEE'=>$delivery_name,
          'CONSIGNEE_ADDRESS1'=>$street_1,
          'CONSIGNEE_ADDRESS2'=>$street_2,
          'CONSIGNEE_ADDRESS3'=>$street_3,
          'DESTINATION_CITY'=>$delivery_city,
          'PINCODE'=>$delivery_pincode,
          'STATE'=>$delivery_state,
          'MOBILE'=>$delivery_telephone,
          'TELEPHONE'=>$delivery_telephone,
          'ITEM_DESCRIPTION'=>$product_title, 
          'PIECES'=>round($product_qty), 
          'COLLECTABLE_VALUE'=>$collect_amount,
          'DECLARED_VALUE'=>$row_total_incl_tax,
          'ACTUAL_WEIGHT'=>$weight,
          'VOLUMETRIC_WEIGHT'=>0,
          'LENGTH'=>$product_length,
          'BREADTH'=>$product_breath,
          'HEIGHT'=>$product_height,
          'PICKUP_NAME'=>$seller_name,
          'PICKUP_ADDRESS_LINE1'=>$seller_comp_nam,
          'PICKUP_ADDRESS_LINE2'=>$seller_comp_address,
          'PICKUP_PINCODE'=>$zipcode,
          'PICKUP_PHONE'=>$seller_comp_mobile,
          'PICKUP_MOBILE'=>$seller_comp_mobile,
          'RETURN_NAME'=>$seller_name,
          'RETURN_ADDRESS_LINE1'=>$seller_comp_nam,
          'RETURN_ADDRESS_LINE2'=>$seller_comp_address,
          'RETURN_PINCODE'=>$zipcode,
          'RETURN_PHONE'=>$seller_comp_mobile,
          'RETURN_MOBILE'=>$seller_comp_mobile,
          'ADDONSERVICE'=>[""],
          'DG_SHIPMENT'=>false,
          'ADDITIONAL_INFORMATION'=>$additional_info
        ); 
      }
      
      if( $seller_id!=29)
      {
        $modearray=json_decode($api_helpers->get_razorpay_mode());
        //print_r($modearray); 
        $mode=$modearray->mode;
        $username=$modearray->username;
        $password=$modearray->password;
        $ecom_url=$modearray->url;
     
        $url=$ecom_url.'apiv2/manifest_awb/';    	 	
        
        $params = array(
          'username' => $username,
            'password' => $password, 
          'json_input' => json_encode($awbarray)
        );   
        	
        $fields_string = http_build_query($params);
  
        //open connection
        $ch = curl_init();  
         
        
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch); 
        $result=json_decode($output);  
        if($result->shipments[0]->success==1)
        {
  
          $sql="select * from ecomexpress_awb where orderid='".$order_id."'";  
          $old_date = $connection->fetchAll($sql);   
          $shipment_id =$old_date[0]['shipment_id'];       		
                             
          $old_awb = $old_date[0]['awb'];
          $shipment_to = $old_date[0]['shipment_to']."oldawb-".$old_awb;       		
  
          $update_awb="update ecomexpress_awb set manifest='1' ,shipment_id='$shipment_id',shipment_to='$shipment_to',status='Assigned',orderid='$order_id',state='1' where awb='$new_awb'";
          $connection->query($update_awb);

          $istoindatabase = $this->checkAwbRtoInDatabase($old_awb);
          if(!$istoindatabase)
          {
            $this>registerRtoToDatabase($old_awb,$order_id);
          } 
          $query =  "update  om_ecomexpressrto_rto set is_regenerated='1',new_active_awb='$new_awb' where rto_awb='$old_awb'";
          $alreadyRegistered = $connection->query($query);
          
          $sql="delete from ecomexpress_awb where awb='".$old_awb."'";  
          $old_date = $connection->query($sql);
  				return array("status"=>true,"response"=>"Awb has been regenerated for the order $order_inc_id . The new awb is $new_awb");
        }	  else 
        {
          $erorr = @$result->shipments[0]->reason;
          if($erorr=="AIRWAYBILL_IN_USE")
          {
            $usedawb = @$result->shipments[0]->awb;
            if(!empty($usedawb))
            {
              $blockmanifestedawb="update ecomexpress_awb set manifest='1' ,status='Assigned',state='1',shipment_to='Already Assignedd' where awb='$usedawb'";
              $connection->query($blockmanifestedawb);
            }
          }	
        }
        return array("status"=>false,"response"=>$erorr);
      }
    }

    public function checkOrderIsOlder()
    {
      $created_date='2011-08-19 17:14:40';
      $someDate = new \DateTime($created_date);
      $now = new \DateTime();

      if($someDate->diff($now)->days > 90) {
        echo 'The date was more than 30 days ago.';
      }
    }
}

