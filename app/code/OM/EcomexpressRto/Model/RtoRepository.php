<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use OM\EcomexpressRto\Api\Data\RtoInterfaceFactory;
use OM\EcomexpressRto\Api\Data\RtoSearchResultsInterfaceFactory;
use OM\EcomexpressRto\Api\RtoRepositoryInterface;
use OM\EcomexpressRto\Model\ResourceModel\Rto as ResourceRto;
use OM\EcomexpressRto\Model\ResourceModel\Rto\CollectionFactory as RtoCollectionFactory;

class RtoRepository implements RtoRepositoryInterface
{

    protected $rtoCollectionFactory;

    protected $dataObjectProcessor;

    protected $rtoFactory;

    protected $extensionAttributesJoinProcessor;

    protected $extensibleDataObjectConverter;
    protected $dataRtoFactory;

    private $storeManager;

    private $collectionProcessor;

    protected $searchResultsFactory;

    protected $resource;

    protected $dataObjectHelper;


    /**
     * @param ResourceRto $resource
     * @param RtoFactory $rtoFactory
     * @param RtoInterfaceFactory $dataRtoFactory
     * @param RtoCollectionFactory $rtoCollectionFactory
     * @param RtoSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceRto $resource,
        RtoFactory $rtoFactory,
        RtoInterfaceFactory $dataRtoFactory,
        RtoCollectionFactory $rtoCollectionFactory,
        RtoSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->rtoFactory = $rtoFactory;
        $this->rtoCollectionFactory = $rtoCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRtoFactory = $dataRtoFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \OM\EcomexpressRto\Api\Data\RtoInterface $rto
    ) {
        /* if (empty($rto->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $rto->setStoreId($storeId);
        } */
        
        $rtoData = $this->extensibleDataObjectConverter->toNestedArray(
            $rto,
            [],
            \OM\EcomexpressRto\Api\Data\RtoInterface::class
        );
        
        $rtoModel = $this->rtoFactory->create()->setData($rtoData);
        
        try {
            $this->resource->save($rtoModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the rto: %1',
                $exception->getMessage()
            ));
        }
        return $rtoModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($rtoId)
    {
        $rto = $this->rtoFactory->create();
        $this->resource->load($rto, $rtoId);
        if (!$rto->getId()) {
            throw new NoSuchEntityException(__('rto with id "%1" does not exist.', $rtoId));
        }
        return $rto->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->rtoCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \OM\EcomexpressRto\Api\Data\RtoInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \OM\EcomexpressRto\Api\Data\RtoInterface $rto
    ) {
        try {
            $rtoModel = $this->rtoFactory->create();
            $this->resource->load($rtoModel, $rto->getRtoId());
            $this->resource->delete($rtoModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the rto: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($rtoId)
    {
        return $this->delete($this->get($rtoId));
    }
}

