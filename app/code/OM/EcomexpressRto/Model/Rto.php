<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Model;

use Magento\Framework\Api\DataObjectHelper;
use OM\EcomexpressRto\Api\Data\RtoInterface;
use OM\EcomexpressRto\Api\Data\RtoInterfaceFactory;

class Rto extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'om_ecomexpressrto_rto';
    protected $rtoDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param RtoInterfaceFactory $rtoDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \OM\EcomexpressRto\Model\ResourceModel\Rto $resource
     * @param \OM\EcomexpressRto\Model\ResourceModel\Rto\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        RtoInterfaceFactory $rtoDataFactory,
        DataObjectHelper $dataObjectHelper,
        \OM\EcomexpressRto\Model\ResourceModel\Rto $resource,
        \OM\EcomexpressRto\Model\ResourceModel\Rto\Collection $resourceCollection,
        array $data = []
    ) {
        $this->rtoDataFactory = $rtoDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve rto model with rto data
     * @return RtoInterface
     */
    public function getDataModel()
    {
        $rtoData = $this->getData();
        
        $rtoDataObject = $this->rtoDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $rtoDataObject,
            $rtoData,
            RtoInterface::class
        );
        
        return $rtoDataObject;
    }
}

