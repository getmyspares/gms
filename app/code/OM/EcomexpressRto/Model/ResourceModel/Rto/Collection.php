<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Model\ResourceModel\Rto;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'rto_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \OM\EcomexpressRto\Model\Rto::class,
            \OM\EcomexpressRto\Model\ResourceModel\Rto::class
        );
    }
}