<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Model\Data;

use OM\EcomexpressRto\Api\Data\RtoInterface;

class Rto extends \Magento\Framework\Api\AbstractExtensibleObject implements RtoInterface
{

    /**
     * Get rto_id
     * @return string|null
     */
    public function getRtoId()
    {
        return $this->_get(self::RTO_ID);
    }

    /**
     * Set rto_id
     * @param string $rtoId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setRtoId($rtoId)
    {
        return $this->setData(self::RTO_ID, $rtoId);
    }

    /**
     * Get rto_awb
     * @return string|null
     */
    public function getRtoAwb()
    {
        return $this->_get(self::RTO_AWB);
    }

    /**
     * Set rto_awb
     * @param string $rtoAwb
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setRtoAwb($rtoAwb)
    {
        return $this->setData(self::RTO_AWB, $rtoAwb);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OM\EcomexpressRto\Api\Data\RtoExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \OM\EcomexpressRto\Api\Data\RtoExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OM\EcomexpressRto\Api\Data\RtoExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * Set order_id
     * @param string $orderId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get is_regenerated
     * @return string|null
     */
    public function getIsRegenerated()
    {
        return $this->_get(self::IS_REGENERATED);
    }

    /**
     * Set is_regenerated
     * @param string $isRegenerated
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setIsRegenerated($isRegenerated)
    {
        return $this->setData(self::IS_REGENERATED, $isRegenerated);
    }

    /**
     * Get visible_to_seller
     * @return string|null
     */
    public function getVisibleToSeller()
    {
        return $this->_get(self::VISIBLE_TO_SELLER);
    }

    /**
     * Set visible_to_seller
     * @param string $visibleToSeller
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setVisibleToSeller($visibleToSeller)
    {
        return $this->setData(self::VISIBLE_TO_SELLER, $visibleToSeller);
    }

    /**
     * Get old_rto_awb
     * @return string|null
     */
    public function getOldRtoAwb()
    {
        return $this->_get(self::OLD_RTO_AWB);
    }

    /**
     * Set old_rto_awb
     * @param string $oldRtoAwb
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setOldRtoAwb($oldRtoAwb)
    {
        return $this->setData(self::OLD_RTO_AWB, $oldRtoAwb);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get Increment_id
     * @return string|null
     */
    public function getIncrementId()
    {
        return $this->_get(self::INCREMENT_ID);
    }

    /**
     * Set Increment_id
     * @param string $incrementId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }
}

