<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Api\Data;

interface RtoSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get rto list.
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface[]
     */
    public function getItems();

    /**
     * Set rto_awb list.
     * @param \OM\EcomexpressRto\Api\Data\RtoInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

