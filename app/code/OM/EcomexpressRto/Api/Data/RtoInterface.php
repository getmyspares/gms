<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Api\Data;

interface RtoInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const VISIBLE_TO_SELLER = 'visible_to_seller';
    const UPDATED_AT = 'updated_at';
    const RTO_AWB = 'rto_awb';
    const OLD_RTO_AWB = 'old_rto_awb';
    const IS_REGENERATED = 'is_regenerated';
    const RTO_ID = 'rto_id';
    const INCREMENT_ID = 'increment_id';
    const CREATED_AT = 'created_at';
    const ORDER_ID = 'order_id';

    /**
     * Get rto_id
     * @return string|null
     */
    public function getRtoId();

    /**
     * Set rto_id
     * @param string $rtoId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setRtoId($rtoId);

    /**
     * Get rto_awb
     * @return string|null
     */
    public function getRtoAwb();

    /**
     * Set rto_awb
     * @param string $rtoAwb
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setRtoAwb($rtoAwb);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OM\EcomexpressRto\Api\Data\RtoExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \OM\EcomexpressRto\Api\Data\RtoExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OM\EcomexpressRto\Api\Data\RtoExtensionInterface $extensionAttributes
    );

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setOrderId($orderId);

    /**
     * Get is_regenerated
     * @return string|null
     */
    public function getIsRegenerated();

    /**
     * Set is_regenerated
     * @param string $isRegenerated
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setIsRegenerated($isRegenerated);

    /**
     * Get visible_to_seller
     * @return string|null
     */
    public function getVisibleToSeller();

    /**
     * Set visible_to_seller
     * @param string $visibleToSeller
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setVisibleToSeller($visibleToSeller);

    /**
     * Get old_rto_awb
     * @return string|null
     */
    public function getOldRtoAwb();

    /**
     * Set old_rto_awb
     * @param string $oldRtoAwb
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setOldRtoAwb($oldRtoAwb);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get Increment_id
     * @return string|null
     */
    public function getIncrementId();

    /**
     * Set Increment_id
     * @param string $incrementId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     */
    public function setIncrementId($incrementId);
}

