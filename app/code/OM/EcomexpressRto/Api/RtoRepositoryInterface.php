<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\EcomexpressRto\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RtoRepositoryInterface
{

    /**
     * Save rto
     * @param \OM\EcomexpressRto\Api\Data\RtoInterface $rto
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \OM\EcomexpressRto\Api\Data\RtoInterface $rto
    );

    /**
     * Retrieve rto
     * @param string $rtoId
     * @return \OM\EcomexpressRto\Api\Data\RtoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($rtoId);

    /**
     * Retrieve rto matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \OM\EcomexpressRto\Api\Data\RtoSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete rto
     * @param \OM\EcomexpressRto\Api\Data\RtoInterface $rto
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \OM\EcomexpressRto\Api\Data\RtoInterface $rto
    );

    /**
     * Delete rto by ID
     * @param string $rtoId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($rtoId);
}

