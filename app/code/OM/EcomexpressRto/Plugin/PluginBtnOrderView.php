<?php
    namespace OM\EcomexpressRto\Plugin;
    
    class PluginBtnOrderView
    {
        public function __construct(
            \OM\EcomexpressRto\Helper\AwbStatusHelper $AwbStatusHelper
        ) {
            $this->AwbStatusHelper = $AwbStatusHelper;
        }

        public function beforeSetLayout(\Magento\Sales\Block\Adminhtml\Order\View $subject )
        {
            $rto=false;
            $order_id =  $subject->getOrderId();
            $rto=$this->AwbStatusHelper->isOrderRto($order_id);
            if($rto)
            {
                $subject->addButton(
                    'regenrateawb',
                    [
                        'label' => __('Regenrate Awb'),
                        'class' => 'secondary',
                        'onclick' => 'setLocation(\'' . $subject->getUrl('ecomexpressrto/regenerate/awb') . '\')'
                    ]
                ); 
            }
               
        }
    }
?>
