<?php
namespace OM\HomeAjax\Block;
 
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
 
class CustomerOrders extends Template
{
    /**
     * @var array
     */
    private $data;
 
    /**
     * @var Context
     */
    private $context;
 
    /**
     * @var CollectionFactory
     */
    private $orderCollectionFactory;

    protected $helper;
 
    public function __construct(
        Context $context,
        CollectionFactory $orderCollectionFactory,
        \OM\CheckCustomerSession\Helper\Data $helper,
        array $data = []
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }
 
    /**
     * @return array
     */
    public function getCustomerOrder()
    {
        $customerId = $this->helper->getCustomerId();
        $customerOrder = $this->orderCollectionFactory->create()
        ->addFieldToFilter('customer_id', $customerId)
        ->addFieldToSelect('*')
        ->addAttributeToFilter('status', array('in' => array('complete','processing','New','Delivered')))
        ->setOrder(
                'created_at',
                'desc'
            )
        ->setPageSize(1);
        return $customerOrder;
    }

    public function getAddToCartUrl($product)
    {
        $params = [
            'product' => $product->getId(),
            '_secure' => $this->getRequest()->isSecure()
        ];

        return $this->getUrl('checkout/cart/add', $params);
    }
}