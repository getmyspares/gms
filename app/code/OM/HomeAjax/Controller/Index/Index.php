<?php
namespace OM\HomeAjax\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action

	{
		protected $_pageFactory;
		public function __construct(
			\Magento\Framework\App\Action\Context $context,
			\Magento\Framework\View\Result\PageFactory $pageFactory)
		{
			$this->_pageFactory = $pageFactory;
			return parent::__construct($context);
		}

		public function execute()
		{
			$resultPage = $this->_pageFactory->create();
			$block = $resultPage->getLayout()
                ->createBlock('OM\HomeAjax\Block\Index')
                ->setTemplate('OM_HomeAjax::index.phtml')
                ->toHtml();
               return $this->getResponse()->setBody($block);
		}
	}
