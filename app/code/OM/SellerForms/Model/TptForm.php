<?php
namespace OM\SellerForms\Model;

class TptForm extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\SellerForms\Model\ResourceModel\TptForm');
    }
}
?>