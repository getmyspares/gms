<?php
namespace OM\SellerForms\Model\ResourceModel;

class RelationshipForm extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('om_seller_relationship_form', 'id');
    }
}
?>