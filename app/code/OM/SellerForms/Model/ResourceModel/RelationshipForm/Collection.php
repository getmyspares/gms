<?php

namespace OM\SellerForms\Model\ResourceModel\RelationshipForm;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\SellerForms\Model\RelationshipForm', 'OM\SellerForms\Model\ResourceModel\RelationshipForm');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>