<?php

namespace OM\SellerForms\Model\ResourceModel\DealerForm;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\SellerForms\Model\DealerForm', 'OM\SellerForms\Model\ResourceModel\DealerForm');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>