<?php
namespace OM\SellerForms\Model\ResourceModel;

class AdministrativeForm extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('om_seller_administrative_form', 'id');
    }
}
?>