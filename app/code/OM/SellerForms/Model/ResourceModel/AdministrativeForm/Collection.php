<?php

namespace OM\SellerForms\Model\ResourceModel\AdministrativeForm;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\SellerForms\Model\AdministrativeForm', 'OM\SellerForms\Model\ResourceModel\AdministrativeForm');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>