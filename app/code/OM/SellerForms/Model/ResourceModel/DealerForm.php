<?php
namespace OM\SellerForms\Model\ResourceModel;

class DealerForm extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('om_seller_dealership_form', 'id');
    }
}
?>