<?php
namespace OM\SellerForms\Block\Adminhtml\Index;
class Index extends \Magento\Backend\Block\Widget\Container
{
    public function __construct(\Magento\Backend\Block\Widget\Context $context,array $data = [])
    {
        parent::__construct($context, $data);
    }
    public function getNewFunctionUrl()
    {
        return $this->getUrl('sellerform/index/assigntoseller');
    }

} 
