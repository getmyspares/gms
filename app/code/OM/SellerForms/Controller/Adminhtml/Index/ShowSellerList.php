<?php    
namespace OM\SellerForms\Controller\Adminhtml\Index;
use Magento\Framework\Controller\ResultFactory;

class ShowSellerList extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\App\ResourceConnection $resource
        )
    {
        $this->_resultFactory = $context->getResultFactory();
        $this->_pageFactory = $pageFactory;
        $this->_resource = $resource;
        return parent::__construct($context);
    }

    public function execute()
    {
        $connection = $this->_resource->getConnection();
        $productIds = $this->getRequest()->getParams();
        if(isset($productIds['filters']['name']) && !isset($productIds['selected'])){
            $pname = $productIds['filters']['name'];
            $sql = "SELECT entity_id FROM `catalog_product_entity_varchar` WHERE `value` ='".$pname."'"; 
            $pIds = $connection->fetchOne($sql);
        }
        elseif(isset($productIds['search']) && !isset($productIds['selected'])){
            $pname = $productIds['search'];
            $sql = "SELECT entity_id FROM `catalog_product_entity_varchar` WHERE `value` ='".$pname."'"; 
            $pIds = $connection->fetchOne($sql);
        }
        else{
            $productIds = $this->getRequest()->getParam('selected');
            $pIds = implode(",",$productIds);
        }
        $page = $this->_resultFactory->create(ResultFactory::TYPE_PAGE);

        /** @var Template $block */
        $block = $page->getLayout()->getBlock('seller.list');
        $block->setData('product_ids', $pIds);
        return $page;
    }

}
