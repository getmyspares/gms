<?php    
namespace OM\SellerForms\Controller\Adminhtml\Index;
use Magento\Framework\Controller\ResultFactory;

class AssignToSeller extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\App\ResourceConnection $resource
        )
    {
        $this->_resultFactory = $context->getResultFactory();
        $this->_pageFactory = $pageFactory;
        $this->_resource = $resource;
        return parent::__construct($context);
    }

    public function execute()
    {
        $connection = $this->_resource->getConnection();
        $tableName= 'marketplace_product';
        $formData = $this->getRequest()->getParams();
        $productIds = explode(',', $formData['productIds']);
        $sellerIds = explode(',', $formData['sellerIds']);
        foreach($productIds as $productId){
         foreach($sellerIds as $sellerId){   
            $sql = "Insert Into " . $tableName . " (entity_id,mageproduct_id,adminassign,seller_id,store_id,status,created_at,	updated_at,seller_pending_notification,admin_pending_notification,is_approved,deny_reason,product_count) Values ('',$productId,1,$sellerId,0,1,'2022-02-22 11:14:02','2022-02-22 11:14:02',0,0,1,'',1)";
            $connection->query($sql);
            $sql = "SELECT count(mageproduct_id) as product_count FROM `marketplace_product` WHERE mageproduct_id = $productId";
            $productCount = $connection->fetchOne($sql);
            $sql = "UPDATE `marketplace_product` SET `product_count` = $productCount WHERE  mageproduct_id = $productId";
            $connection->query($sql);
         }
        }
    }

}
