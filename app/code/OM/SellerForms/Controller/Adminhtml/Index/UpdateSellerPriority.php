<?php    
namespace OM\SellerForms\Controller\Adminhtml\Index;

class UpdateSellerPriority extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\App\ResourceConnection $resource
        )
    {
        $this->_pageFactory = $pageFactory;
        $this->_resource = $resource;
        return parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPost();
        if($post){
        $sellerId = $post['sellerId'];
        $priority = $post['priority'];
        $connection = $this->_resource->getConnection();
        $sql = "UPDATE `marketplace_userdata` SET `seller_priority` = $priority WHERE `marketplace_userdata`.`seller_id` = $sellerId";
        $connection->query($sql);
        echo $sql;
        echo 'success';
        }
    }

}
