<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class AdministrativeForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
         ManagerInterface $messageManager,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
		return parent::__construct($context);
	}

	public function execute()
	{
      
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
        $result =array();
        if($data){    
                $adminFormData = $objectManager->create('OM\SellerForms\Model\AdministrativeForm');
        }
        $InputFileName = array('certificate_of_incorporation_file','registration_certificate_file','gst_certificate_file','gst_certificate_year_file','financial_statement_year_file','directors_details_file','shareholder_pattern_doccument_file','appointment_details_file','credit_report_file','emplyment_record_file','office_proof_file','copies_of_declarations_file','pi_current_asp_declaration_file');
        foreach($InputFileName as $InputFileName){ 
        try{     
        $file = $this->getRequest()->getFiles($InputFileName);
        $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;

        if ($file && $fileName) {   
        $target = $this->_mediaDirectory->getAbsolutePath('sellerDocx/');        
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $InputFileName]); 
        $uploader->setAllowedExtensions(['jpg', 'pdf', 'doc', 'png', 'zip', 'doc','jpeg']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($target);
        if($InputFileName == 'certificate_of_incorporation_file'){
            $adminFormData->setCertificateOfIncorporationFile($result['file']);    
        }
        if($InputFileName == 'registration_certificate_file'){
            $adminFormData->setRegistrationCertificateFile($result['file']);    
        } 
        if($InputFileName == 'gst_certificate_file'){
            $adminFormData->setGstCertificateFile($result['file']);    
        } 
        if($InputFileName == 'gst_certificate_year_file'){
            $adminFormData->setGstCertificateYearFile($result['file']);    
        } 
        if($InputFileName == 'financial_statement_year_file'){
            $adminFormData->setFinancialStatementYearFile($result['file']);    
        } 
        if($InputFileName == 'credit_report_file'){
            $adminFormData->setCreditReportFile($result['file']);    
        } 
        if($InputFileName == 'emplyment_record_file'){
            $adminFormData->setEmplymentRecordFile($result['file']);    
        } 
        if($InputFileName == 'copies_of_declarations_file'){
            $adminFormData->setCopiesOfDeclarationsFile($result['file']);    
        } 
        if($InputFileName == 'pi_current_asp_declaration_file'){
            $adminFormData->setPiCurrentAspDeclarationFile($result['file']);    
        } 
        if($InputFileName == 'directors_details_file'){
            $adminFormData->setDirectorsDetailsFile($result['file']);    
        } 
        if($InputFileName == 'shareholder_pattern_doccument_file'){
            $adminFormData->setShareholderPatternDoccumentFile($result['file']);    
        } 
        if($InputFileName == 'appointment_details_file'){
            $adminFormData->setAppointmentDetailsFile($result['file']);    
        }
        if($InputFileName == 'office_proof_file'){
            $adminFormData->setOfficeProofFile($result['file']);    
        }
        }
        } catch (\Exception $e) {
                echo $e->getMessage();
        }

        }  
        if(isset($data['entity_old'])){
            $adminFormData->setEntityOld($data['entity_old']);
        }
        if(isset($data['certificate_of_incorporation'])){
            $adminFormData->setCertificateOfIncorporation($data['certificate_of_incorporation']);
        }
        if(isset($data['entity_type'])){
            $adminFormData->setEntityType($data['entity_type']);
        }
        if(isset($data['registration_certificate'])){
            $adminFormData->setRegistrationCertificate($data['registration_certificate']);
        }
        if(isset($data['valid_gstin'])){
            $adminFormData->setValidGstin($data['valid_gstin']);
        }
        if(isset($data['gst_certificate'])){
            $adminFormData->setGstCertificate($data['gst_certificate']);
        }
        if(isset($data['gstin_register_year'])){
            $adminFormData->setGstinRegisterYear($data['gstin_register_year']);
        }
        if(isset($data['gst_certificate_year'])){
            $adminFormData->setGstCertificateYear($data['gst_certificate_year']);
        }
        if(isset($data['financial_statement'])){
            $adminFormData->setFinancialStatement($data['financial_statement']);
        }
        if(isset($data['financial_statement_year'])){
            $adminFormData->setFinancialStatementYear($data['financial_statement_year']);
        }
        if(isset($data['entity_borrowings'])){
            $adminFormData->setEntityBorrowings($data['entity_borrowings']);
        }
        if(isset($data['new_profit_firm_consistent'])){
            $adminFormData->setNewProfitFirmConsistent($data['new_profit_firm_consistent']);
        }
        if(isset($data['director_name'])){
            $adminFormData->setDirectorName($data['director_name']);
        }
        if(isset($data['directors_details'])){
            $adminFormData->setDirectorsDetails($data['directors_details']);
        }
        if(isset($data['current_shareholders_service_provider'])){
            $adminFormData->setCurrentShareholdersServiceProvider($data['current_shareholders_service_provider']);
        }
        if(isset($data['shareholder_pattern_doccument'])){
            $adminFormData->setShareholderPatternDoccument($data['shareholder_pattern_doccument']);
        }
        if(isset($data['change_in_admin_service_provider'])){
            $adminFormData->setChangeInAdminServiceProvider($data['change_in_admin_service_provider']);
        }
        if(isset($data['appointment_details'])){
            $adminFormData->setAppointmentDetails($data['appointment_details']);
        }
        if(isset($data['credit_rating_score'])){   
            $adminFormData->setCreditRatingScore($data['credit_rating_score']);
        }
        if(isset($data['credit_report'])){
            $adminFormData->setCreditReport($data['credit_report']);
        }
        if(isset($data['entity_operates'])){
            $adminFormData->setEntityOperates($data['entity_operates']);
        }
        if(isset($data['office_address_details'])){
            $adminFormData->setOfficeAddressDetails($data['office_address_details']);
        }
        if(isset($data['entity_global_presence'])){
            $adminFormData->setEntityGlobalPresence($data['entity_global_presence']);
        }
        if(isset($data['global_office_details'])){
            $adminFormData->setGlobalOfficeDetails($data['global_office_details']);
        }
        if(isset($data['number_of_emplyoee_emplyed'])){
            $adminFormData->setNumberOfEmplyoeeEmplyed($data['number_of_emplyoee_emplyed']);
        }
        if(isset($data['emplyment_record'])){
            $adminFormData->setEmplymentRecord($data['emplyment_record']);
        }
        if(isset($data['avg_turnover'])){
            $adminFormData->setAvgTurnover($data['avg_turnover']);
        }
        if(isset($data['dedicated_office'])){
            $adminFormData->setDedicatedOffice($data['dedicated_office']);
        }
        if(isset($data['office_proof'])){
            $adminFormData->setOfficeProof($data['office_proof']);
        }
        if(isset($data['current_business_activities'])){
            $adminFormData->setCurrentBusinessActivities($data['current_business_activities']);
        }
        if(isset($data['number_of_year_service_providing'])){
            $adminFormData->setNumberOfYearServiceProviding($data['number_of_year_service_providing']);
        }
        if(isset($data['past_project'])){
            $adminFormData->setPastProject($data['past_project']);
        }
        if(isset($data['clients_associated'])){
            $adminFormData->setClientsAssociated($data['clients_associated']);
        }
        if(isset($data['client_reference'])){
            $adminFormData->setClientReference($data['client_reference']);
        }
        if(isset($data['reference_details'])){
            $adminFormData->setReferenceDetails($data['reference_details']);
        }
        if(isset($data['supplier_reference'])){
            $adminFormData->setSupplierReference($data['supplier_reference']);
        }
        if(isset($data['supplier_reference_details'])){
            $adminFormData->setSupplierReferenceDetails($data['supplier_reference_details']);
        }
        if(isset($data['asp_previous_service'])){
            $adminFormData->setAspPreviousService($data['asp_previous_service']);
        }
        if(isset($data['asp_old_customer_code'])){
            $adminFormData->setAspOldCustomerCode($data['asp_old_customer_code']);
        }
        if(isset($data['number_of_projects'])){
            $adminFormData->setNumberOfProjects($data['number_of_projects']);
        }
        if(isset($data['old_customer_code'])){
            $adminFormData->setOldCustomerCode($data['old_customer_code']);
        }
        if(isset($data['approximate_revenue'])){
            $adminFormData->setApproximateRevenue($data['approximate_revenue']);
        }
        if(isset($data['anti_bribery'])){
            $adminFormData->setAntiBribery($data['anti_bribery']);
        }
        if(isset($data['copies_of_declarations'])){
            $adminFormData->setCopiesOfDeclarations($data['copies_of_declarations']);
        }
        if(isset($data['policy_communication'])){
            $adminFormData->setPolicyCommunication($data['policy_communication']);
        }
        if(isset($data['abac_training'])){
            $adminFormData->setAbacTraining($data['abac_training']);
        }
        if(isset($data['specific_gift_policy'])){
            $adminFormData->setSpecificGiftPolicy($data['specific_gift_policy']);
        }
        if(isset($data['internal_financial_control'])){
            $adminFormData->setInternalFinancialControl($data['internal_financial_control']);
        }
        if(isset($data['administrative_payment_from_client'])){
            $adminFormData->setAdministrativePaymentFromClient($data['administrative_payment_from_client']);
        }
        if(isset($data['administrative_payment_to_suppliers'])){
            $adminFormData->setAdministrativePaymentToSuppliers($data['administrative_payment_to_suppliers']);
        }
        if(isset($data['pi_current_asp'])){
            $adminFormData->setPiCurrentAsp($data['pi_current_asp']);
        }
        if(isset($data['pi_current_asp_declaration'])){
            $adminFormData->setPiCurrentAspDeclaration($data['pi_current_asp_declaration']);
        }
        if(isset($data['entity_relation_with_govt_party'])){
            $adminFormData->setEntityRelationWithGovtParty($data['entity_relation_with_govt_party']);
        }
        if(isset($data['entity_relation_with_govt_authorities'])){
            $adminFormData->setEntityRelationWithGovtAuthorities($data['entity_relation_with_govt_authorities']);
        }
        if(isset($data['asp_pi'])){
            $adminFormData->setAspPi($data['asp_pi']);
        }
        if(isset($data['kmp'])){
            $adminFormData->setKmp($data['kmp']);
        }
        if(isset($data['asp_corruption'])){
            $adminFormData->setAspCorruption($data['asp_corruption']);
        }
        if(isset($data['asp_anticorruption_laws'])){
            $adminFormData->setAspAnticorruptionLaws($data['asp_anticorruption_laws']);
        }
        if(isset($data['entity_blacklisted'])){
            $adminFormData->setEntityBlacklisted($data['entity_blacklisted']);
        }
        $adminFormData->setCustomerId($data['customer_id']);

        $adminFormData->save();
		$redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $this->messageManager->addSuccess(__("Form Submitted Successfully")); 
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}