<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class EditDealerForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
        return parent::__construct($context);
    }

	public function execute()
	{
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
    
        if($data){    
                $dealerFormData = $objectManager->create('OM\SellerForms\Model\DealerForm');
                $dealerFormData = $dealerFormData->load($data['edit_id']);
        }
        $InputFileName = array('signature','rubber_stamp');
        foreach($InputFileName as $InputFileName){ 
            try{     
                    $file = $this->getRequest()->getFiles($InputFileName);
                    $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;
                    if ($file && $fileName) {   
                        $target = $this->_mediaDirectory->getAbsolutePath('sellerDocx/');        
                        $uploader = $this->_fileUploaderFactory->create(['fileId' => $InputFileName]); 
                        $uploader->setAllowedExtensions(['jpg', 'pdf', 'doc', 'png', 'zip', 'doc','jpeg']);
                        $uploader->setAllowRenameFiles(true);
                        $result = $uploader->save($target);
                        if($InputFileName == 'signature'){
                            $dealerFormData->setSignature($result['file']);    
                        }
                        if($InputFileName == 'rubber_stamp'){
                            $dealerFormData->setRubberStamp($result['file']);    
                        } 
                    }
                } 
            catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        if(isset($data['dealer_name'])){  
            $dealerFormData->setDealerName($data['dealer_name']);
        }
        if(isset($data['dealer_business_address_1'])){  
            $dealerFormData->setDealerBusinessAddress1($data['dealer_business_address_1']);
        }
        if(isset($data['dealer_business_address_2'])){  
            $dealerFormData->setDealerBusinessAddress2($data['dealer_business_address_2']);
        }
        if(isset($data['dealer_pin'])){  
            $dealerFormData->setDealerPin($data['dealer_pin']);
        }
        if(isset($data['dealer_tell_no'])){  
            $dealerFormData->setDealerTellNo($data['dealer_tell_no']);
        }
        if(isset($data['dealer_mobile_number'])){  
            $dealerFormData->setDealerMobileNumber($data['dealer_mobile_number']);
        }
        if(isset($data['dealer_email_id'])){  
            $dealerFormData->setDealerEmailId($data['dealer_email_id']);
        }
        if(isset($data['dealer_address_proof_name_1'])){  
            $dealerFormData->setDealerAddressProofName1($data['dealer_address_proof_name_1']);
        }
        if(isset($data['dealer_address_proof_address_1'])){  
            $dealerFormData->setDealerAddressProofAddress1($data['dealer_address_proof_address_1']);
        }
        if(isset($data['dealer_address_proof_tel_1'])){  
            $dealerFormData->setDealerAddressProofTel1($data['dealer_address_proof_tel_1']);
        }
        if(isset($data['dealer_address_proof_name_2'])){  
            $dealerFormData->setDealerAddressProofName2($data['dealer_address_proof_name_2']);
        }
        if(isset($data['dealer_address_proof_address_2'])){  
            $dealerFormData->setDealerAddressProofAddress2($data['dealer_address_proof_address_2']);
        }
        if(isset($data['dealer_address_proof_tel_2'])){  
            $dealerFormData->setDealerAddressProofTel2($data['dealer_address_proof_tel_2']);
        }
        if(isset($data['dealer_address_proof_name_3'])){  
            $dealerFormData->setDealerAddressProofName3($data['dealer_address_proof_name_3']);
        }
        if(isset($data['dealer_address_proof_address_3'])){  
            $dealerFormData->setDealerAddressProofAddress3($data['dealer_address_proof_address_3']);
        }
        if(isset($data['dealer_address_proof_tel_3'])){  
            $dealerFormData->setDealerAddressProofTel3($data['dealer_address_proof_tel_3']);
        }
        if(isset($data['year_established'])){  
            $dealerFormData->setYearEstablished($data['year_established']);
        }
        if(isset($data['registration_number'])){  
            $dealerFormData->setRegistrationNumber($data['registration_number']);
        }
        if(isset($data['income_tax_number'])){  
            $dealerFormData->setIncomeTaxNumber($data['income_tax_number']);
        }
        if(isset($data['tin_lst'])){  
            $dealerFormData->setTinLst($data['tin_lst']);
        }
        if(isset($data['cst'])){  
            $dealerFormData->setCst($data['cst']);
        }
        if(isset($data['main_office'])){  
            $dealerFormData->setMainOffice($data['main_office']);
        }
        if(isset($data['main_office_size_h'])){  
            $dealerFormData->setMainOfficeSizeH($data['main_office_size_h']);
        }
        if(isset($data['main_office_size_w'])){  
            $dealerFormData->setMainOfficeSizeW($data['main_office_size_w']);
        }
        if(isset($data['main_office_size_l'])){  
            $dealerFormData->setMainOfficeSizeL($data['main_office_size_l']);
        }
        if(isset($data['main_showroom_size_h'])){  
            $dealerFormData->setMainShowroomSizeH($data['main_showroom_size_h']);
        }
        if(isset($data['main_showroom_size_w'])){  
            $dealerFormData->setMainShowroomSizeW($data['main_showroom_size_w']);
        }
        if(isset($data['main_showroom_size_l'])){  
            $dealerFormData->setMainShowroomSizeL($data['main_showroom_size_l']);
        }
        if(isset($data['main_showroom_address_1'])){  
            $dealerFormData->setMainShowroomAddress1($data['main_showroom_address_1']);
        }
        if(isset($data['main_showroom_address_2'])){  
            $dealerFormData->setMainShowroomAddress2($data['main_showroom_address_2']);
        }
        if(isset($data['main_showroom_pin'])){  
            $dealerFormData->setMainShowroomPin($data['main_showroom_pin']);
        }
        if(isset($data['window_display'])){  
            $dealerFormData->setWindowDisplay($data['window_display']);
        }
        if(isset($data['window_length'])){  
            $dealerFormData->setWindowLength($data['window_length']);
        }
        if(isset($data['window_breadth'])){  
            $dealerFormData->setWindowBreadth($data['window_breadth']);
        }
        if(isset($data['brand'])){  
            $dealerFormData->setBrand($data['brand']);
        }
        if(isset($data['product'])){  
            $dealerFormData->setProduct($data['product']);
        }
        if(isset($data['showroom_location_1'])){  
            $dealerFormData->setShowroomLocation1($data['showroom_location_1']);
        }
        if(isset($data['showroom_location_2'])){  
            $dealerFormData->setShowroomLocation2($data['showroom_location_2']);
        }
        if(isset($data['showroom_location_3'])){  
            $dealerFormData->setShowroomLocation3($data['showroom_location_3']);
        }
        if(isset($data['godown_inside_address_1'])){  
            $dealerFormData->setGodownInsideAddress1($data['godown_inside_address_1']);
        }
        if(isset($data['godown_inside_address_2'])){  
            $dealerFormData->setGodownInsideAddress2($data['godown_inside_address_2']);
        }
        if(isset($data['godown_inside_pin'])){  
            $dealerFormData->setGodownInsidePin($data['godown_inside_pin']);
        }
        if(isset($data['godown_outside_address_1'])){  
            $dealerFormData->setGodownOutsideAddress1($data['godown_outside_address_1']);
        }
        if(isset($data['godown_outside_address_2'])){  
            $dealerFormData->setGodownOutsideAddress2($data['godown_outside_address_2']);
        }
        if(isset($data['godown_outside_pin'])){  
            $dealerFormData->setGodownOutsidePin($data['godown_outside_pin']);
        }
        if(isset($data['manegers_value'])){  
            $dealerFormData->setManegersValue($data['manegers_value']);
        }
        if(isset($data['salesmen_value'])){  
            $dealerFormData->setSalesmenValue($data['salesmen_value']);
        }
        if(isset($data['technicians_value'])){  
            $dealerFormData->setTechniciansValue($data['technicians_value']);
        }
        if(isset($data['others_value'])){  
            $dealerFormData->setOthersValue($data['others_value']);
        }
        if(isset($data['date'])){  
            $dealerFormData->setDate($data['date']);
        }
        if(isset($data['place'])){  
            $dealerFormData->setPlace($data['place']);
        }
        $dealerFormData->setCustomerId($data['customer_id']);

        $dealerFormData->save();
        $this->messageManager->addSuccess(__("Form Submitted Successfully"));
		$redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}