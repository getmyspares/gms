<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class RelationshipForm extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
        return parent::__construct($context);
    }

	public function execute()
	{
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
	if($data){    
	$relationshipFormData = $objectManager->create('OM\SellerForms\Model\RelationshipForm');
        }
        $relationshipFormData->setRelationshipDeclarationName($data['seller_name']);
        $relationshipFormData->setRelationshipDeclarationOffice($data['seller_employees']);
        if(isset($data['govt_relationship'])){
            $relationshipFormData->setGovtRelations($data['govt_relationship']);
        }
        $relationshipFormData->setGovtRelationsNameDesignation($data['Name_of_Government_employee']);
        $relationshipFormData->setGovtRealtionsPlace($data['Place_and_office_of_Posting']);
        $relationshipFormData->setGovtRelationsPartner($data['Relation_with_the_Partner']);
         if(isset($data['company_relationship'])){
            $relationshipFormData->setCompanyRelations($data['company_relationship']);
        }
        $relationshipFormData->setCompanyRelationsNameOfBenificial($data['Beneficial_owner']);
        $relationshipFormData->setCompanyRelationsType($data['Relationship_Type_Owner']);
        $relationshipFormData->setCompanyRelationsNumberOfShares($data['Number_of_shares']);
        $relationshipFormData->setCompanyRelationsHoldings($data['percentage_holding']);
        if(isset($data['govt_transaction'])){
            $relationshipFormData->setGovtTransaction($data['govt_transaction']);
        }
        $relationshipFormData->setGovtTransactionsDate($data['Date_of_Transaction']);
        $relationshipFormData->setGovtTransactionsType($data['Type_of_Transaction']);
        $relationshipFormData->setGovtTransactionsAmount($data['Amount']);
        $relationshipFormData->setGovtTransactionsOfficeDetails($data['Government_Office_Details']);
        if(isset($data['litigation_matters'])){
            $relationshipFormData->setLitigationMatters($data['litigation_matters']);
        }
        $relationshipFormData->setLitigationMattersCaseNumber($data['Case_No']);
        $relationshipFormData->setLitigationMattersCaseName($data['Case_Name']);
        $relationshipFormData->setLitigationMattersCourt($data['Court_/_Forum']);
        $relationshipFormData->setLitigationMattersNature($data['Nature_of_Case']);
        $relationshipFormData->setLitigationMattersHearingDate($data['Next_Hearing_Date']);
        if(isset($data['conflict_of_interest'])){
            $relationshipFormData->setConflictOfInterest($data['conflict_of_interest']);
        }
        $relationshipFormData->setConflictOfInterestPartyName($data['Conflicting_Party_Name']);
        $relationshipFormData->setConflictOfInterestDepartment($data['conflict_of_interest_department']);
        $relationshipFormData->setConflictOfInterestRelationship($data['Relationship']);
        $relationshipFormData->setCustomerId($data['customer_id']);
        $relationshipFormData->save();
        $this->messageManager->addSuccess(__("Form Submitted Successfully"));
        $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}