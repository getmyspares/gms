<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class TptForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $messageManager,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
		return parent::__construct($context);
	}

	public function execute()
	{
      
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
        $result =array();
        if($data){    
                $tptFormData = $objectManager->create('OM\SellerForms\Model\TptForm');
        }
        $InputFileName = array('prevention_bribery_value','offering_of_gift_value','political_contribution_value','employee_signature_1');
        foreach($InputFileName as $InputFileName){ 
        try{     
        $file = $this->getRequest()->getFiles($InputFileName);
        $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;

        if ($file && $fileName) {   
        $target = $this->_mediaDirectory->getAbsolutePath('sellerDocx/');        
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $InputFileName]); 
        $uploader->setAllowedExtensions(['jpg', 'pdf', 'doc', 'png', 'zip', 'doc','jpeg']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($target);
        if($InputFileName == 'prevention_bribery_value'){
           $tptFormData->setPreventionBriberyValue($result['file']);    
        }
        if($InputFileName == 'offering_of_gift_value'){
            $tptFormData->setOfferingOfGiftValue($result['file']);    
        } 
        if($InputFileName == 'political_contribution_value'){
            $tptFormData->setPoliticalContributionValue($result['file']);    
        }
        if($InputFileName == 'employee_signature_1'){
            $tptFormData->setEmployeeSignature1($result['file']);    
        } 
        
        }
        } catch (\Exception $e) {
                echo $e->getMessage();
        }

        } 
        if(isset($data['full_name'])){ 
            $tptFormData->setFullName($data['full_name']);
        }
        if(isset($data['registered_address'])){ 
            $tptFormData->setRegisteredAddress($data['registered_address']);
        }
        if(isset($data['city'])){ 
            $tptFormData->setCity($data['city']);
        }
        if(isset($data['country'])){ 
            $tptFormData->setCountry($data['country']);
        }
        if(isset($data['individual_party'])){ 
            $tptFormData->setIndividualParty($data['individual_party']);
        }
        if(isset($data['panasonic_region'])){ 
            $tptFormData->setPanasonicRegion($data['panasonic_region']);
        }
        if(isset($data['company_domain'])){ 
            $tptFormData->setCompanyDomain($data['company_domain']);
        }
        if(isset($data['panasonic_entity'])){ 
            $tptFormData->setPanasonicEntity($data['panasonic_entity']);
        }
        if(isset($data['business_sponsor_email'])){ 
            $tptFormData->setBusinessSponsorEmail($data['business_sponsor_email']);
        }
        if(isset($data['servie_provide'])){ 
            $tptFormData->setServieProvide($data['servie_provide']);
        }
        if(isset($data['addtional_details'])){ 
            $tptFormData->setAddtionalDetails($data['addtional_details']);
        }
        if(isset($data['govt_dealing'])){ 
            $tptFormData->setGovtDealing($data['govt_dealing']);
        }
        if(isset($data['govt_ownership'])){ 
            $tptFormData->setGovtOwnership($data['govt_ownership']);
        }
        if(isset($data['required_for_govt_official'])){ 
            $tptFormData->setRequiredForGovtOfficial($data['required_for_govt_official']);
        }
        if(isset($data['required_for_commercial_customer'])){ 
            $tptFormData->setRequiredForCommercialCustomer($data['required_for_commercial_customer']);
        }
        if(isset($data['country_list'])){ 
            $tptFormData->setCountryList($data['country_list']);
        }
        if(isset($data['compensation_structure'])){ 
            $tptFormData->setCompensationStructure($data['compensation_structure']);
        }
        if(isset($data['proposed_payment_method'])){ 
            $tptFormData->setProposedPaymentMethod($data['proposed_payment_method']);
        }
        if(isset($data['compensation_value'])){ 
            $tptFormData->setCompensationValue($data['compensation_value']);
        }
        if(isset($data['contract_currency'])){ 
            $tptFormData->setContractCurrency($data['contract_currency']);
        }
        if(isset($data['compensation_to_paid'])){ 
            $tptFormData->setCompensationToPaid($data['compensation_to_paid']);
        }
        if(isset($data['payment_proccessing'])){ 
            $tptFormData->setPaymentProccessing($data['payment_proccessing']);
        }
        if(isset($data['beneficiary_payment'])){ 
            $tptFormData->setBeneficiaryPayment($data['beneficiary_payment']);
        }
        if(isset($data['compensation_paid_other_country'])){ 
            $tptFormData->setCompensationPaidOtherCountry($data['compensation_paid_other_country']);
        }
        if(isset($data['compensation_country_list'])){ 
            $tptFormData->setCompensationCountryList($data['compensation_country_list']);
        }
        if(isset($data['business_justification_for_payment'])){ 
            $tptFormData->setBusinessJustificationForPayment($data['business_justification_for_payment']);
        }
        if(isset($data['intermediary_delegate'])){    
            $tptFormData->setIntermediaryDelegate($data['intermediary_delegate']);
        }
        if(isset($data['intermediary_delegate_name'])){ 
            $tptFormData->setIntermediaryDelegateName($data['intermediary_delegate_name']);
        }
        if(isset($data['intermediary_delegate_country'])){ 
            $tptFormData->setIntermediaryDelegateCountry($data['intermediary_delegate_country']);
        }
        if(isset($data['intermediary_delegate_affiliations'])){ 
            $tptFormData->setIntermediaryDelegateAffiliations($data['intermediary_delegate_affiliations']);
        }
        if(isset($data['intermediary_delegate_business_justification'])){ 
            $tptFormData->setIntermediaryDelegateBusinessJustification($data['intermediary_delegate_business_justification']);
        }
        if(isset($data['intermediary_delegate_other'])){ 
            $tptFormData->setIntermediaryDelegateOther($data['intermediary_delegate_other']);
        }
        if(isset($data['interactions_with_government_officials'])){ 
            $tptFormData->setInteractionsWithGovernmentOfficials($data['interactions_with_government_officials'])
        ;
        }
        if(isset($data['interactions_with_government_officials_justification'])){ 
            $tptFormData->setInteractionsWithGovernmentOfficialsJustification($data['interactions_with_government_officials_justification']);
        }
        if(isset($data['government_ownership'])){ 
            $tptFormData->setGovernmentOwnership($data['government_ownership']);
        }
        if(isset($data['government_ownership_value'])){ 
            $tptFormData->setGovernmentOwnershipValue($data['government_ownership_value']);
        }
        if(isset($data['intermediary_required_govt_official'])){ 
            $tptFormData->setIntermediaryRequiredGovtOfficial($data['intermediary_required_govt_official']);
        }
        if(isset($data['intermediary_required_govt_official_value_enage'])){ 
        $tptFormData->setIntermediaryRequiredGovtOfficialValueEnage($data['intermediary_required_govt_official_value_enage']);
        }
        if(isset($data['intermediary_required_govt_official_value_reason'])){ 
            $tptFormData->setIntermediaryRequiredGovtOfficialValueReason($data['intermediary_required_govt_official_value_reason']);
        }
        if(isset($data['intermediary_required_govt_official_value'])){ 
        $tptFormData->setIntermediaryRequiredGovtOfficialValue($data['intermediary_required_govt_official_value']);
        }
        if(isset($data['intermediary_required_govt_official_value_control'])){ 
        $tptFormData->setIntermediaryRequiredGovtOfficialValueControl($data['intermediary_required_govt_official_value_control']);
        }
        if(isset($data['intermediary_required_govt_official_value_control_value'])){ 
        $tptFormData->setIntermediaryRequiredGovtOfficialValueControlValue($data['intermediary_required_govt_official_value_control_value']);
        }
        if(isset($data['contemplating'])){ 
            $tptFormData->setContemplating($data['contemplating']);
        }
        if(isset($data['contemplating_legal_requirement'])){ 
            $tptFormData->setContemplatingLegalRequirement($data['contemplating_legal_requirement']);
        }
        if(isset($data['contemplating_specific_business'])){ 
            $tptFormData->setContemplatingSpecificBusiness($data['contemplating_specific_business']);
        }
        if(isset($data['contemplating_customer_ownership'])){ 
            $tptFormData->setContemplatingCustomerOwnership($data['contemplating_customer_ownership']);
        }
        if(isset($data['contemplating_customer_otherwise'])){ 
            $tptFormData->setContemplatingCustomerOtherwise($data['contemplating_customer_otherwise']);
        }
        
        if(isset($data['publicly_traded_company'])){ 
            $tptFormData->setPubliclyTradedCompany($data['publicly_traded_company']);
        }
        if(isset($data['govt_official_considered'])){ 
            $tptFormData->setGovtOfficialConsidered($data['govt_official_considered']);
        }
        if(isset($data['govt_official_considered_value'])){ 
            $tptFormData->setGovtOfficialConsideredValue($data['govt_official_considered_value']);
        }
        if(isset($data['under_investigation'])){ 
            $tptFormData->setUnderInvestigation($data['under_investigation']);
        }
        if(isset($data['under_investigation_value'])){ 
            $tptFormData->setUnderInvestigationValue($data['under_investigation_value']);
        }
        if(isset($data['prevention_bribery'])){ 
            $tptFormData->setPreventionBribery($data['prevention_bribery']);
        }
        if(isset($data['offer_entertainment_to_client'])){ 
            $tptFormData->setOfferEntertainmentToClient($data['offer_entertainment_to_client']);
        }
        if(isset($data['offering_of_gift'])){ 
            $tptFormData->setOfferingOfGift($data['offering_of_gift']);
        }
        if(isset($data['political_contribution'])){ 
            $tptFormData->setPoliticalContribution($data['political_contribution']);
        }
        if(isset($data['training_on_anti_corruption'])){ 
            $tptFormData->setTrainingOnAntiCorruption($data['training_on_anti_corruption']);
        }
        if(isset($data['accounting_control'])){ 
            $tptFormData->setAccountingControl($data['accounting_control']);
        }
        if(isset($data['external_audit'])){ 
            $tptFormData->setExternalAudit($data['external_audit']);
        }
        if(isset($data['is_panasonic_emplyoee'])){ 
            $tptFormData->setIsPanasonicEmplyoee($data['is_panasonic_emplyoee']);
        }
        if(isset($data['doing_business_with_panasonic'])){ 
            $tptFormData->setDoingBusinessWithPanasonic($data['doing_business_with_panasonic']);
        }
        $tptFormData->setCustomerId($data['customer_id']);
        $tptFormData->save();
		$redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $this->messageManager->addSuccess(__("Form Submitted Successfully"));
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}