<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class EditMainForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
        return parent::__construct($context);
    }

	public function execute()
	{
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
	   $chanelClassification ='';
    $sourcesOfFunds ='';
    $applicationOfFunds ='';
    $liquidityRatio ='';
    $inventoryRatio='';
    $documentation ='';
    if(isset($data['chanel_classification'])){
       $chanelClassification = implode(",", $data['chanel_classification']);
    }
    if(isset($data['sources_of_funds'])){
       $sourcesOfFunds = implode(",", $data['sources_of_funds']);
    }
    if(isset($data['application_of_funds'])){
       $applicationOfFunds = implode(",", $data['application_of_funds']);
    }
    if(isset($data['liquidity_ratio'])){
       $liquidityRatio = implode(",", $data['liquidity_ratio']);
    }
    if(isset($data['inventory_ratio'])){
       $inventoryRatio = implode(",", $data['inventory_ratio']);
    }
    if(isset($data['documentation'])){
       $documentation =  implode(",", $data['documentation']);
    }
        $result =array();
        if($data){    
                $mainFormData = $objectManager->create('OM\SellerForms\Model\MainForm');
                $editMainFormData = $mainFormData->load($data['edit_id']);

        }
        $InputFileName = array('application-form-file','interest-letter-file','showroom-owner-photo-file','audit-file','incometax-file','residence-proof-file','bank-credit-file','sales-tax-registration-file','pancard-file','owner-signature-file','memorandum-file','others-file');
        foreach($InputFileName as $InputFileName){ 
        try{     
        $file = $this->getRequest()->getFiles($InputFileName);
        $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;

        if ($file && $fileName) {   
        $target = $this->_mediaDirectory->getAbsolutePath('sellerDocx/');        
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $InputFileName]); 
        $uploader->setAllowedExtensions(['jpg', 'pdf', 'doc', 'png', 'zip', 'doc','jpeg']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($target);
        if($InputFileName == 'application-form-file'){
            $editMainFormData->setApplicationFormFile($result['file']);    
        }
        if($InputFileName == 'interest-letter-file'){
            $editMainFormData->setDealerInterestLetterFile($result['file']);    
        } 
        if($InputFileName == 'showroom-owner-photo-file'){
            $editMainFormData->setShowroomOwnerPhotographFile($result['file']);    
        } 
        if($InputFileName == 'audit-file'){
            $editMainFormData->setAuditFile($result['file']);    
        } 
        if($InputFileName == 'incometax-file'){
            $editMainFormData->setIncomeTaxFile($result['file']);    
        } 
        if($InputFileName == 'residence-proof-file'){
            $editMainFormData->setResidenceFile($result['file']);    
        } 
        if($InputFileName == 'bank-credit-file'){
            $editMainFormData->setBankCertifiateFile($result['file']);    
        } 
        if($InputFileName == 'sales-tax-registration-file'){
            $editMainFormData->setSalesTaxFile($result['file']);    
        } 
        if($InputFileName == 'pancard-file'){
            $editMainFormData->setPancardFile($result['file']);    
        } 
        if($InputFileName == 'owner-signature-file'){
            $editMainFormData->setOwnerSignatureFile($result['file']);    
        } 
        if($InputFileName == 'memorandum-file'){
            $editMainFormData->setMemorandumFile($result['file']);    
        } 
        if($InputFileName == 'others-file'){
            $editMainFormData->setOtherFile($result['file']);    
        }        
        }
        } catch (\Exception $e) {
                echo $e->getMessage();
        }

        } 
        if(isset($data['branch_details'])){   
            $editMainFormData->setBranchDetails($data['branch_details']);
        }
        if(isset($data['category'])){  
            $editMainFormData->setCategory($data['category']);
        }
        $editMainFormData->setChannelClassification($chanelClassification);
        if(isset($data['dealer_distributor_name'])){  
            $editMainFormData->setDealerDistributerName($data['dealer_distributor_name']);
        }
        if(isset($data['status_of_dealer_distributor'])){  
            $editMainFormData->setDealerDistributerStatus($data['status_of_dealer_distributor']);
        }
        $editMainFormData->setBrandsDealing($data['Brands_Dealing']);
        $editMainFormData->setYearlySales($data['Yearly_sales']);
        $editMainFormData->setBusinessExpectedPerMonth($data['Expected_per_month']);
        $editMainFormData->setSubDealers($data['sub-dealers']);
        $editMainFormData->setOutlets($data['outlets']);
        $editMainFormData->setProductRange($data['product_range']);
        if(isset($data['party_visited'])){  
            $editMainFormData->setPartyVisited($data['party_visited']);
        }
        $editMainFormData->setSalesPersonMappedWithCustomerCode($data['sales_person']);
        $editMainFormData->setSourceOfFund($sourcesOfFunds);
        $editMainFormData->setApplicationFund($applicationOfFunds);
        $editMainFormData->setLiquidityRatio($liquidityRatio);
        $editMainFormData->setInventoryRatio($inventoryRatio);
        $editMainFormData->setBankerName($data['banker_name']);
        $editMainFormData->setAccountNumber($data['account_number']);
        $editMainFormData->setCcLimit($data['overdraft_cc_value']);
        $editMainFormData->setCreditLimitProposed($data['Credit_limit']);
        if(isset($data['credit_days'])){  
            $editMainFormData->setCreditDaysProposed($data['credit_days']);
        }
        $editMainFormData->setDocumentation($documentation);
        $editMainFormData->setCustomerId($data['customer_id']);

        $editMainFormData->save();
        $this->messageManager->addSuccess(__("Form Submitted Successfully"));
		$redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}