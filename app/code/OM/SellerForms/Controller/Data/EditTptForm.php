<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class EditTptForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $messageManager,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
		return parent::__construct($context);
	}

	public function execute()
	{
      
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
        $result =array();
        if($data){    
                $editTptFormData = $objectManager->create('OM\SellerForms\Model\TptForm');
                $editTptFormData = $editTptFormData->load($data['edit_id']);
        }
        $InputFileName = array('prevention_bribery_value','offering_of_gift_value','political_contribution_value','employee_signature_1');
        foreach($InputFileName as $InputFileName){ 
        try{     
        $file = $this->getRequest()->getFiles($InputFileName);
        $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;

        if ($file && $fileName) {   
        $target = $this->_mediaDirectory->getAbsolutePath('sellerDocx/');        
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $InputFileName]); 
        $uploader->setAllowedExtensions(['jpg', 'pdf', 'doc', 'png', 'zip', 'doc','jpeg']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($target);
        if($InputFileName == 'prevention_bribery_value'){
           $editTptFormData->setPreventionBriberyValue($result['file']);    
        }
        if($InputFileName == 'offering_of_gift_value'){
            $editTptFormData->setOfferingOfGiftValue($result['file']);    
        } 
        if($InputFileName == 'political_contribution_value'){
            $editTptFormData->setPoliticalContributionValue($result['file']);    
        } 
        if($InputFileName == 'employee_signature_1'){
            $tptFormData->setEmployeeSignature1($result['file']);    
        } 
        
        }
        } catch (\Exception $e) {
                echo $e->getMessage();
        }

        }  
        if(isset($data['full_name'])){ 
            $editTptFormData->setFullName($data['full_name']);
        }
        if(isset($data['registered_address'])){ 
            $editTptFormData->setRegisteredAddress($data['registered_address']);
        }
        if(isset($data['city'])){ 
            $editTptFormData->setCity($data['city']);
        }
        if(isset($data['country'])){ 
            $editTptFormData->setCountry($data['country']);
        }
        if(isset($data['individual_party'])){ 
            $editTptFormData->setIndividualParty($data['individual_party']);
        }
        if(isset($data['panasonic_region'])){ 
            $editTptFormData->setPanasonicRegion($data['panasonic_region']);
        }
        if(isset($data['company_domain'])){ 
            $editTptFormData->setCompanyDomain($data['company_domain']);
        }
        if(isset($data['panasonic_entity'])){ 
            $editTptFormData->setPanasonicEntity($data['panasonic_entity']);
        }
        if(isset($data['business_sponsor_email'])){ 
            $editTptFormData->setBusinessSponsorEmail($data['business_sponsor_email']);
        }
        if(isset($data['servie_provide'])){ 
            $editTptFormData->setServieProvide($data['servie_provide']);
        }
        if(isset($data['addtional_details'])){ 
            $editTptFormData->setAddtionalDetails($data['addtional_details']);
        }
        if(isset($data['govt_dealing'])){ 
            $editTptFormData->setGovtDealing($data['govt_dealing']);
        }
        if(isset($data['govt_ownership'])){ 
            $editTptFormData->setGovtOwnership($data['govt_ownership']);
        }
        if(isset($data['required_for_govt_official'])){ 
            $editTptFormData->setRequiredForGovtOfficial($data['required_for_govt_official']);
        }
        if(isset($data['required_for_commercial_customer'])){ 
            $editTptFormData->setRequiredForCommercialCustomer($data['required_for_commercial_customer']);
        }
        if(isset($data['country_list'])){ 
            $editTptFormData->setCountryList($data['country_list']);
        }
        if(isset($data['compensation_structure'])){ 
            $editTptFormData->setCompensationStructure($data['compensation_structure']);
        }
        if(isset($data['proposed_payment_method'])){ 
            $editTptFormData->setProposedPaymentMethod($data['proposed_payment_method']);
        }
        if(isset($data['compensation_value'])){ 
            $editTptFormData->setCompensationValue($data['compensation_value']);
        }
        if(isset($data['contract_currency'])){ 
            $editTptFormData->setContractCurrency($data['contract_currency']);
        }
        if(isset($data['compensation_to_paid'])){ 
            $editTptFormData->setCompensationToPaid($data['compensation_to_paid']);
        }
        if(isset($data['payment_proccessing'])){ 
            $editTptFormData->setPaymentProccessing($data['payment_proccessing']);
        }
        if(isset($data['beneficiary_payment'])){ 
            $editTptFormData->setBeneficiaryPayment($data['beneficiary_payment']);
        }
        if(isset($data['compensation_paid_other_country'])){ 
            $editTptFormData->setCompensationPaidOtherCountry($data['compensation_paid_other_country']);
        }
        if(isset($data['compensation_country_list'])){ 
            $editTptFormData->setCompensationCountryList($data['compensation_country_list']);
        }
        if(isset($data['business_justification_for_payment'])){ 
            $editTptFormData->setBusinessJustificationForPayment($data['business_justification_for_payment']);
        }
        if(isset($data['intermediary_delegate'])){    
            $editTptFormData->setIntermediaryDelegate($data['intermediary_delegate']);
        }
        if(isset($data['intermediary_delegate_name'])){ 
            $editTptFormData->setIntermediaryDelegateName($data['intermediary_delegate_name']);
        }
        if(isset($data['intermediary_delegate_country'])){ 
            $editTptFormData->setIntermediaryDelegateCountry($data['intermediary_delegate_country']);
        }
        if(isset($data['intermediary_delegate_affiliations'])){ 
            $editTptFormData->setIntermediaryDelegateAffiliations($data['intermediary_delegate_affiliations']);
        }
        if(isset($data['intermediary_delegate_business_justification'])){ 
            $editTptFormData->setIntermediaryDelegateBusinessJustification($data['intermediary_delegate_business_justification']);
        }
        if(isset($data['intermediary_delegate_other'])){ 
            $editTptFormData->setIntermediaryDelegateOther($data['intermediary_delegate_other']);
        }
        if(isset($data['interactions_with_government_officials'])){ 
            $editTptFormData->setInteractionsWithGovernmentOfficials($data['interactions_with_government_officials'])
        ;
        }
        if(isset($data['interactions_with_government_officials_justification'])){ 
            $editTptFormData->setInteractionsWithGovernmentOfficialsJustification($data['interactions_with_government_officials_justification']);
        }
        if(isset($data['government_ownership'])){ 
            $editTptFormData->setGovernmentOwnership($data['government_ownership']);
        }
        if(isset($data['government_ownership_value'])){ 
            $editTptFormData->setGovernmentOwnershipValue($data['government_ownership_value']);
        }
        if(isset($data['intermediary_required_govt_official'])){ 
            $editTptFormData->setIntermediaryRequiredGovtOfficial($data['intermediary_required_govt_official']);
        }
        if(isset($data['intermediary_required_govt_official_value_enage'])){ 
        $editTptFormData->setIntermediaryRequiredGovtOfficialValueEnage($data['intermediary_required_govt_official_value_enage']);
        }
        if(isset($data['intermediary_required_govt_official_value_reason'])){ 
            $editTptFormData->setIntermediaryRequiredGovtOfficialValueReason($data['intermediary_required_govt_official_value_reason']);
        }
        if(isset($data['intermediary_required_govt_official_value'])){ 
        $editTptFormData->setIntermediaryRequiredGovtOfficialValue($data['intermediary_required_govt_official_value']);
        }
        if(isset($data['intermediary_required_govt_official_value_control'])){ 
        $editTptFormData->setIntermediaryRequiredGovtOfficialValueControl($data['intermediary_required_govt_official_value_control']);
        }
        if(isset($data['intermediary_required_govt_official_value_control_value'])){ 
        $editTptFormData->setIntermediaryRequiredGovtOfficialValueControlValue($data['intermediary_required_govt_official_value_control_value']);
        }
        if(isset($data['contemplating'])){ 
            $editTptFormData->setContemplating($data['contemplating']);
        }
        if(isset($data['contemplating_legal_requirement'])){ 
            $editTptFormData->setContemplatingLegalRequirement($data['contemplating_legal_requirement']);
        }
        if(isset($data['contemplating_specific_business'])){ 
            $editTptFormData->setContemplatingSpecificBusiness($data['contemplating_specific_business']);
        }
        if(isset($data['contemplating_customer_ownership'])){ 
            $editTptFormData->setContemplatingCustomerOwnership($data['contemplating_customer_ownership']);
        }
        if(isset($data['contemplating_customer_otherwise'])){ 
            $editTptFormData->setContemplatingCustomerOtherwise($data['contemplating_customer_otherwise']);
        }
        
        if(isset($data['publicly_traded_company'])){ 
            $editTptFormData->setPubliclyTradedCompany($data['publicly_traded_company']);
        }
        if(isset($data['govt_official_considered'])){ 
            $editTptFormData->setGovtOfficialConsidered($data['govt_official_considered']);
        }
        if(isset($data['govt_official_considered_value'])){ 
            $editTptFormData->setGovtOfficialConsideredValue($data['govt_official_considered_value']);
        }
        if(isset($data['under_investigation'])){ 
            $editTptFormData->setUnderInvestigation($data['under_investigation']);
        }
        if(isset($data['under_investigation_value'])){ 
            $editTptFormData->setUnderInvestigationValue($data['under_investigation_value']);
        }
        if(isset($data['prevention_bribery'])){ 
            $editTptFormData->setPreventionBribery($data['prevention_bribery']);
        }
        if(isset($data['offer_entertainment_to_client'])){ 
            $editTptFormData->setOfferEntertainmentToClient($data['offer_entertainment_to_client']);
        }
        if(isset($data['offering_of_gift'])){ 
            $editTptFormData->setOfferingOfGift($data['offering_of_gift']);
        }
        if(isset($data['political_contribution'])){ 
            $editTptFormData->setPoliticalContribution($data['political_contribution']);
        }
        if(isset($data['training_on_anti_corruption'])){ 
            $editTptFormData->setTrainingOnAntiCorruption($data['training_on_anti_corruption']);
        }
        if(isset($data['accounting_control'])){ 
            $editTptFormData->setAccountingControl($data['accounting_control']);
        }
        if(isset($data['external_audit'])){ 
            $editTptFormData->setExternalAudit($data['external_audit']);
        }
        if(isset($data['is_panasonic_emplyoee'])){ 
            $editTptFormData->setIsPanasonicEmplyoee($data['is_panasonic_emplyoee']);
        }
        if(isset($data['doing_business_with_panasonic'])){ 
            $editTptFormData->setDoingBusinessWithPanasonic($data['doing_business_with_panasonic']);
        }
        $editTptFormData->setCustomerId($data['customer_id']);
        $editTptFormData->save();
		$redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/sellerform/index/display');
        $this->messageManager->addSuccess(__("Form Submitted Successfully"));
        return $redirect;
	}
}