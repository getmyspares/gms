<?php
namespace OM\SellerForms\Controller\Data;

class RelationshipEditForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
	if($data){    
	$relationshipFormData = $objectManager->create('OM\SellerForms\Model\RelationshipForm');
        $editRelationshipFormData = $relationshipFormData->load($data['edit_id']);
        }
        $editRelationshipFormData->setRelationshipDeclarationName($data['seller_name']);
        $editRelationshipFormData->setRelationshipDeclarationOffice($data['seller_employees']);
        if(isset($data['govt_relationship'])){
        $editRelationshipFormData->setGovtRelations($data['govt_relationship']);
        }
        $editRelationshipFormData->setGovtRelationsNameDesignation($data['Name_of_Government_employee']);
        $editRelationshipFormData->setGovtRealtionsPlace($data['Place_and_office_of_Posting']);
        $editRelationshipFormData->setGovtRelationsPartner($data['Relation_with_the_Partner']);
        if(isset($data['company_relationship'])){
                $editRelationshipFormData->setCompanyRelations($data['company_relationship']);
        }
        $editRelationshipFormData->setCompanyRelationsNameOfBenificial($data['Beneficial_owner']);
        $editRelationshipFormData->setCompanyRelationsType($data['Relationship_Type_Owner']);
        $editRelationshipFormData->setCompanyRelationsNumberOfShares($data['Number_of_shares']);
        $editRelationshipFormData->setCompanyRelationsHoldings($data['percentage_holding']);
        if(isset($data['govt_transaction'])){
                $editRelationshipFormData->setGovtTransaction($data['govt_transaction']);
        }
        $editRelationshipFormData->setGovtTransactionsDate($data['Date_of_Transaction']);
        $editRelationshipFormData->setGovtTransactionsType($data['Type_of_Transaction']);
        $editRelationshipFormData->setGovtTransactionsAmount($data['Amount']);
        $editRelationshipFormData->setGovtTransactionsOfficeDetails($data['Government_Office_Details']);
        if(isset($data['litigation_matters'])){
        $editRelationshipFormData->setLitigationMatters($data['litigation_matters']);
        }
        $editRelationshipFormData->setLitigationMattersCaseNumber($data['Case_No']);
        $editRelationshipFormData->setLitigationMattersCaseName($data['Case_Name']);
        $editRelationshipFormData->setLitigationMattersCourt($data['Court_/_Forum']);
        $editRelationshipFormData->setLitigationMattersNature($data['Nature_of_Case']);
        $editRelationshipFormData->setLitigationMattersHearingDate($data['Next_Hearing_Date']);
        if(isset($data['conflict_of_interest'])){
                $editRelationshipFormData->setConflictOfInterest($data['conflict_of_interest']);
        }
        $editRelationshipFormData->setConflictOfInterestPartyName($data['Conflicting_Party_Name']);
        $editRelationshipFormData->setConflictOfInterestDepartment($data['conflict_of_interest_department']);
        $editRelationshipFormData->setConflictOfInterestRelationship($data['Relationship']);
        $editRelationshipFormData->setCustomerId($data['customer_id']);
        $editRelationshipFormData->save();
        $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}