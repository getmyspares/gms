<?php
namespace OM\SellerForms\Controller\Data;
use Magento\Framework\Message\ManagerInterface;
class MainForm extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->messageManager = $messageManager;
        return parent::__construct($context);
    }

	public function execute()
	{
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	$data = $this->getRequest()->getPostValue();
    $chanelClassification ='';
    $sourcesOfFunds ='';
    $applicationOfFunds ='';
    $liquidityRatio ='';
    $inventoryRatio='';
    $documentation ='';
    if(isset($data['chanel_classification'])){
	   $chanelClassification = implode(",", $data['chanel_classification']);
    }
    if(isset($data['sources_of_funds'])){
	   $sourcesOfFunds = implode(",", $data['sources_of_funds']);
    }
    if(isset($data['application_of_funds'])){
	   $applicationOfFunds = implode(",", $data['application_of_funds']);
    }
    if(isset($data['liquidity_ratio'])){
	   $liquidityRatio = implode(",", $data['liquidity_ratio']);
    }
    if(isset($data['inventory_ratio'])){
	   $inventoryRatio = implode(",", $data['inventory_ratio']);
    }
    if(isset($data['documentation'])){
	   $documentation =  implode(",", $data['documentation']);
    }
        $result =array();
        if($data){    
                $mainFormData = $objectManager->create('OM\SellerForms\Model\MainForm');
        }
        $InputFileName = array('application-form-file','interest-letter-file','showroom-owner-photo-file','audit-file','incometax-file','residence-proof-file','bank-credit-file','sales-tax-registration-file','pancard-file','owner-signature-file','memorandum-file','others-file');
        foreach($InputFileName as $InputFileName){ 
        try{     
        $file = $this->getRequest()->getFiles($InputFileName);
        $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;

        if ($file && $fileName) {   
        $target = $this->_mediaDirectory->getAbsolutePath('sellerDocx/');        
        $uploader = $this->_fileUploaderFactory->create(['fileId' => $InputFileName]); 
        $uploader->setAllowedExtensions(['jpg', 'pdf', 'doc', 'png', 'zip', 'doc','jpeg']);
        $uploader->setAllowRenameFiles(true);
        $result = $uploader->save($target);
        if($InputFileName == 'application-form-file'){
            $mainFormData->setApplicationFormFile($result['file']);    
        }
        if($InputFileName == 'interest-letter-file'){
            $mainFormData->setDealerInterestLetterFile($result['file']);    
        } 
        if($InputFileName == 'showroom-owner-photo-file'){
            $mainFormData->setShowroomOwnerPhotographFile($result['file']);    
        } 
        if($InputFileName == 'audit-file'){
            $mainFormData->setAuditFile($result['file']);    
        } 
        if($InputFileName == 'incometax-file'){
            $mainFormData->setIncomeTaxFile($result['file']);    
        } 
        if($InputFileName == 'residence-proof-file'){
            $mainFormData->setResidenceFile($result['file']);    
        } 
        if($InputFileName == 'bank-credit-file'){
            $mainFormData->setBankCertifiateFile($result['file']);    
        } 
        if($InputFileName == 'sales-tax-registration-file'){
            $mainFormData->setSalesTaxFile($result['file']);    
        } 
        if($InputFileName == 'pancard-file'){
            $mainFormData->setPancardFile($result['file']);    
        } 
        if($InputFileName == 'owner-signature-file'){
            $mainFormData->setOwnerSignatureFile($result['file']);    
        } 
        if($InputFileName == 'memorandum-file'){
            $mainFormData->setMemorandumFile($result['file']);    
        } 
        if($InputFileName == 'others-file'){
            $mainFormData->setOtherFile($result['file']);    
        }        
        }
        } catch (\Exception $e) {
                echo $e->getMessage();
        }

        }
        if(isset($data['branch_details'])){  
            $mainFormData->setBranchDetails($data['branch_details']);
        }
        if(isset($data['category'])){  
            $mainFormData->setCategory($data['category']);
        }
        $mainFormData->setChannelClassification($chanelClassification);
        $mainFormData->setDealerDistributerName($data['dealer_distributor_name']);
        if(isset($data['status_of_dealer_distributor'])){ 
            $mainFormData->setDealerDistributerStatus($data['status_of_dealer_distributor']);
        }
        $mainFormData->setBrandsDealing($data['Brands_Dealing']);
        $mainFormData->setYearlySales($data['Yearly_sales']);
        $mainFormData->setBusinessExpectedPerMonth($data['Expected_per_month']);
        $mainFormData->setSubDealers($data['sub-dealers']);
        $mainFormData->setOutlets($data['outlets']);
        $mainFormData->setProductRange($data['product_range']);
        if(isset($data['party_visited'])){ 
            $mainFormData->setPartyVisited($data['party_visited']);
        }
        $mainFormData->setSalesPersonMappedWithCustomerCode($data['sales_person']);
        $mainFormData->setSourceOfFund($sourcesOfFunds);
        $mainFormData->setApplicationFund($applicationOfFunds);
        $mainFormData->setLiquidityRatio($liquidityRatio);
        $mainFormData->setInventoryRatio($inventoryRatio);
        $mainFormData->setBankerName($data['banker_name']);
        $mainFormData->setAccountNumber($data['account_number']);
        $mainFormData->setCcLimit($data['overdraft_cc_value']);
        $mainFormData->setCreditLimitProposed($data['Credit_limit']);
        if(isset($data['credit_days'])){ 
            $mainFormData->setCreditDaysProposed($data['credit_days']);
        }
        $mainFormData->setDocumentation($documentation);
       /* $mainFormData->setFinancialCondition($data['financial_condition']);
        $mainFormData->setProposedPaymentTerms($data['proposed_payment_terms']);
        $mainFormData->setRecommendation($data['recommendation']);*/
        //$mainFormData->setCustomerEmail($data['customer_email']);
        $mainFormData->setCustomerId($data['customer_id']);

        $mainFormData->save();
        $this->messageManager->addSuccess(__("Form Submitted Successfully"));
		$redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl('/sellerform/index/display');
        return $redirect;
	}
}