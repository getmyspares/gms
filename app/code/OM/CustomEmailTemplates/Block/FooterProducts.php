<?php
namespace OM\CustomEmailTemplates\Block;
class FooterProducts extends \Magento\Framework\View\Element\Template
{    
    protected $_productCollectionFactory;
    protected $_imageBuilder;
    protected $_optionFactory;
    protected $_productRepository;
    protected $_productImageHelper;
    protected $scopeConfig;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $_imageBuilder,
        \Magento\Catalog\Model\Product\OptionFactory $_optionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Helper\Image $productImageHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_imageBuilder = $_imageBuilder;
        $this->_optionFactory = $_optionFactory;
        $this->_productRepository = $productRepository;
        $this->_productImageHelper = $productImageHelper;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }
    

    public function getProductCollection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $array = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('custom_template/footer/products');
        $array = explode (",", $array); 
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addFieldToFilter('sku',['in' => $array]);
        $collection->setPageSize(10);
        return $collection;
    }



    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

    public function getAddToCartUrl($product)
    {
        $params = [
            'product' => $product->getId(),
            '_secure' => $this->getRequest()->isSecure()
        ];

        return $this->getUrl('checkout/cart/add', $params);
    }

    public function getOptions($product)
    {
        return $this->_optionFactory->create()->getProductOptionCollection($product);
    }

    public function getProductHeading(){
       return  $this->scopeConfig->getValue('custom_template/footer/products_heading', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
?>