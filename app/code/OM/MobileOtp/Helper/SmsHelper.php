<?php
/**
 * Copyright © Orange Mantra All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\MobileOtp\Helper;
use Exception;
use Magento\Framework\App\Helper\AbstractHelper;

require_once BP.'/sms/vendor/autoload.php';   
use Twilio\Rest\Client;  


class SmsHelper extends AbstractHelper
{

	protected $scopeConfig;
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }
    
    CONST KARIX_APU_URL    = 'mobile_api/mobile_otp/url';
    CONST KARIX_APU_TOKEN  = 'mobile_api/mobile_otp/token';
    CONST KARIX_APU_SENDER = 'mobile_api/mobile_otp/sender';
    
    public function smsCurlRequest($msg,$phone){		
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$url    = $this->scopeConfig->getValue(self::KARIX_APU_URL, $storeScope);
		$token  = $this->scopeConfig->getValue(self::KARIX_APU_TOKEN, $storeScope);
		$sender = $this->scopeConfig->getValue(self::KARIX_APU_SENDER, $storeScope);
		$msg    = urlencode($msg);
		$request_url = $url.'?ver=1.0&key='.$token.'&encrpt=0&dest='.$phone.'&send='.$sender.'&text='.$msg;

		$response = file_get_contents($request_url);
		if(strpos($response, 'Statuscode=200') !== false){
			return 1;
		}else{
			return 0;
		}
		
	}

    public function send_sms($phone,$pin,$msg=null)
	{
		try {  

			// $msg   = "Spare Parts - Please complete your process by entering OTP - ".$pin;
			if(empty($msg))
			{
				$msg   = "Dear user, please use the OTP $pin to complete the registration/update profile on GetMySpares";
			}
			
			$phone = $phone;
			$message = $this->smsCurlRequest($msg,$phone);
	    }  catch(Exception $e) {
		   $message=$e->getMessage();  
        }	
        return 	$message;	
        	
		// try {
		// 	// $sid    = "ACf65431d42cfec245c5eaad3b48a085fd";
        //     // $token  = "3d5997316017dd03c2f6f47676d516b0"; 
            
        //     $sid    = "AC927db94863fa1840c73ba79f4633fbf3";
        //     $token  = "abe4d84a06369a861edd2c0c2f49206a"; 

            
		// 	$twilio = new Client($sid, $token);
		// 	$message = $twilio->messages
		// 	  ->create("+91".$phone, 
		// 		 array("from" => "+12568575277", "body" => "Spare Parts - Please complete your process by entering OTP - ".$pin)
		// 	);     
		// 	$message=1;
		// }  
		// catch(Exception $e) {
		//   $message=$e->getMessage();  
        // }	
        // return 	$message;
    }
    
    public function send_custom_sms($phone,$msg)
		{
			try {  
				$message = $this->smsCurlRequest($msg,$phone);
			}  catch(Exception $e) {
					$message=$e->getMessage();  
			}	
			return 	$message;
		}
	
    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }
    
    public function sendOrderConfMsg($order){
		$telephone = '';
		if($order){
			if(is_object($order->getShippingAddress())) {
				$telephone = $order->getShippingAddress()->getData('telephone');
			} elseif(is_object($order->getBillingAddress())) {
				$telephone = $order->getBillingAddress()->getData('telephone');
			}
		}
		/*get order Id*/
		$orderiddata = $order->getIncrementId();
		if($telephone) {
			$msg = "Dear User, your GetMySpares Order ".$orderiddata ." has been successfully placed. Track your order on GetMySpares order details page";
			$this->send_custom_sms($telephone,$msg);
		}
	}
}

