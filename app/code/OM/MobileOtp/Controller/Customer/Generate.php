<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\MobileOtp\Controller\Customer;

class Generate extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Customer\Model\Session $customerSession,
        \OM\MobileOtp\Helper\SmsHelper $smsHelper, 
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->smsHelper = $smsHelper;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        try {
            $pin = mt_rand(1000, 9999);
            $post = $this->getRequest()->getPostValue();
            $phone=$post['mobile'];
            $completeUser=array(
                'pin'=>$pin,	
                'phone'=>$phone,
                'is_otp_send'=>'no',
                'is_otp_confirmed'=>'no'	
            );
            $status = $this->smsHelper->send_sms($phone,$pin);
            if($status) {
                $completeUser['is_otp_send']='yes';
            }
            $customerregisterotpdata=json_encode($completeUser);
            $this->customerSession->setCustomerRegisterOtpDetails($customerregisterotpdata);
            return $this->jsonResponse($status);
        
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}

