<?php
namespace OM\MobileOtp\Observer;

class SendOrderSms implements \Magento\Framework\Event\ObserverInterface
{
    protected $_objectManager;
    protected $_orderFactory;    
    protected $_checkoutSession;
    protected $_smshepler;


 	public function __construct(        
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\ObjectManager\ObjectManager $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \OM\MobileOtp\Helper\SmsHelper $smshelper
    ) {
        $this->_objectManager = $objectManager;        
        $this->_storemanager  = $storeManager;
        $this->_orderFactory  = $orderFactory;
        $this->_checkoutSession = $checkoutSession; 
        $this->_smshepler = $smshelper;               

    }

	public function execute(\Magento\Framework\Event\Observer $observer)
	{ 	
		$invoice     = $observer->getEvent()->getInvoice();
		$orderInv    = $invoice->getOrder();
		$orderId     = $orderInv->getId();
		
		$order = $this->_orderFactory->create()->load($orderId);
		$smsHelper = $this->_smshepler;
		$smsHelper->sendOrderConfMsg($order);
		
	}

}	
