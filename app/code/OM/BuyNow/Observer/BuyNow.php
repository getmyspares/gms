<?php 

namespace OM\BuyNow\Observer;

use Magento\Framework\Event\ObserverInterface;

class BuyNow implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
   {
           

    $request = $observer->getEvent()->getRequest();
    $fullActionName = $request->getFullActionName();
    if($fullActionName=="cms_index_index" || $fullActionName=="catalog_product_view")
    {
       //$this->checkBuyNow($fullActionName);     
    }
   }

   public function checkBuyNow($fullActionName)
   {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $quote = $cart->getQuote();

        $current_quote_id = $quote->getId();
        $buy_now = $quote->getData('buy_now');
        $buynow_quote_id = $quote->getData('buy_now_quote_id');
        
        if($buy_now && $buynow_quote_id)
        {
            $quoteFactory =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
            $old_quote = $quoteFactory->create()->load($buynow_quote_id);
            $quote->setData('buy_now',0);
            $quote->merge($old_quote);
            $quote->setTotalsCollectedFlag(false)->collectTotals();
            $quote->save();

            $resources = $objectManager->create('Magento\Framework\App\ResourceConnection');
            $connection= $resources->getConnection();
            $sql = "insert into `dummy` set `text`='$fullActionName',`quote_id`='$current_quote_id'";
            $connection->query($sql);
            return true;
        }
        return false; 
   }

}