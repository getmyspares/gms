<?php
/**
 * Copyright © @ritesh All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\BuyNow\Controller\Check;


class Buynow extends \Magento\Framework\App\Action\Action
{

	protected $_pageFactory;

	public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
      $status=false;

      $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
      $fullActionName = $this->getRequest()->getPost('action');
    
      if($fullActionName=="cms_index_index" || $fullActionName=="catalog_product_view" || $fullActionName=="catalog_category_view" || $fullActionName=="catalogsearch_result_index")
      {
         $status =  $this->checkBuyNow();     
      }
      if($status)
      {
        echo '1';
      } else 
      {
        echo '0';
      }

    }
    public function checkBuyNow()
   {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $quote = $cart->getQuote();
        $current_quote_id = $quote->getId();
        $buy_now = $quote->getData('buy_now');
        $buynow_quote_id = $quote->getData('buy_now_quote_id');
        
        /* normal  case , customer tried to  buynow but did not  buy the product */
        if(($buy_now && $buynow_quote_id))
        {
            $quoteFactory =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
            $old_quote = $quoteFactory->create()->load($buynow_quote_id);
            $quote->setData('buy_now',0);
            $quote->merge($old_quote);
            $quote->setTotalsCollectedFlag(false)->collectTotals();
            $quote->save();
            return true;
        }

        /* customer  successfully  placed  order  and now a completly new quote has been created */
        $customer_email = $quote->getData('customer_email');
        $resources = $objectManager->create('Magento\Framework\App\ResourceConnection');
        $connection= $resources->getConnection();
        $sql = "select buy_now_quote_id from quote where customer_email='$customer_email' and buy_now='1' order by entity_id desc limit 1";
        $old_buy_now_item = $connection->fetchOne($sql);
        if($old_buy_now_item)
        {
          $quoteFactory =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
          $old_quote = $quoteFactory->create()->load($old_buy_now_item);
          $quote->setData('buy_now',0);
          $quote->merge($old_quote);
          $quote->setTotalsCollectedFlag(false)->collectTotals();
          $quote->save();
          $sql = "update quote set buy_now='0' where customer_email='$customer_email'";
          $old_buy_now_item = $connection->query($sql);
          return true; 
        }


        return false; 
   }
  }