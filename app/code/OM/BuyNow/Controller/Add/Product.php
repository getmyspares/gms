<?php
/**
 * Copyright © @ritesh All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\BuyNow\Controller\Add;


class Product extends \Magento\Framework\App\Action\Action
{

	protected $_pageFactory;

	public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $cartRepositoryInterface = $objectManager->get('Magento\Quote\Api\CartRepositoryInterface');
        
        $addproduct = false;
        $mergecart  = true;
        
        if($addproduct) 
        {
            if ($customerSession->isLoggedIn()) {
                $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
                $customer = $customerSession->getCustomer();
                $customer_id = $customer->getId();
                
                /* get current quote  */
                $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
                $quote = $cart->getQuote();
                $current_quote_id = $quote->getId();
                echo '$current_quote_id'.$current_quote_id;
                echo "<br>";
    
                /* deactivate quote  */
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $quoteFactory =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
                $currentQuoteObj = $quoteFactory->create()->load($current_quote_id);
                $currentQuoteObj->setIsActive(false)->save();
    
                /* create new quote */
                
                $_customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterfaceFactory');
                $quoteFactory_2 =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
                $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                $storeId = $storeManager->getStore()->getStoreId();
                $customer   = $_customerRepository->create()->getById($customer_id);
                $quote_new    = $quoteFactory_2->create();
                $quote_new->setStoreId($storeId);
                $quote_new->assignCustomer($customer);  
                $quote_new->setData('buy_now','1');
                $quote_new->assignCustomer($customer);
                // $quote_new->setData('buy_now_quote_id',"$current_quote_id");

                /* add new product to cart */  
    
                $productRepository = $objectManager->get('Magento\Catalog\Model\ProductRepository');
                $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
                $dataObjectFactory = $objectManager->get('Magento\Framework\DataObject\Factory');
                $formKey = $objectManager->get('Magento\Framework\Data\Form\FormKey');
                $cartRepository = $objectManager->create('Magento\Quote\Api\CartRepositoryInterface');
                
                $productId=16147;
                $params = [
                    'form_key' => $formKey->getFormKey(),
                    'product' => $productId,
                    'qty'   =>1
                ];
    
                $product = $productRepository->getById($productId);
                $quote_new->addProduct($product,$dataObjectFactory->create($params));
                $quote_new->setTotalsCollectedFlag(false)->collectTotals();
                $cartRepository->save($quote_new);
                $cart_id=$quote_new->getId();
                echo '$cart_id'.$cart_id;
                
                /* $quote_new->setData('buy_now_quote_id',"$current_quote_id"); not working so this is workaround */
                $resources = $objectManager->create('Magento\Framework\App\ResourceConnection');
                $connection= $resources->getConnection();
                $sql = "UPDATE  `quote` set buy_now_quote_id='$current_quote_id' where entity_id ='$cart_id' ";
                $connection->query($sql);
            }
        }

        if($mergecart)
        {
            $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
            $quote = $cart->getQuote();
            $current_quote_id = $quote->getId();
            $buy_now = $quote->getData('buy_now');
            $buynow_quote_id = $quote->getData('buy_now_quote_id');
            
            if($buy_now && $buynow_quote_id)
            {
                $quoteFactory =$objectManager->create('\Magento\Quote\Model\QuoteFactory');
                $old_quote = $quoteFactory->create()->load($buynow_quote_id);
                $quote->merge($old_quote);
                $quote->setTotalsCollectedFlag(false)->collectTotals();
                $quote->save();
            }
            echo "old quote id : ".$buynow_quote_id;
            echo "<br>";
            echo 'now new quote id : '.$current_quote_id;
        }
    }
}

