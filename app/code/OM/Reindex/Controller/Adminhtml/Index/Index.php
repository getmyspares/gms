<?php

namespace OM\Reindex\Controller\Adminhtml\Index;


class Index extends \Magento\Backend\App\Action
{
	protected $_indexerFactory;

	protected $_indexerCollectionFactory;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Indexer\Model\IndexerFactory $indexerFactory,
		\Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory
	){
		parent::__construct($context);
		$this->_indexerFactory = $indexerFactory;
		$this->_indexerCollectionFactory = $indexerCollectionFactory;
	}
	
    public function execute()
    {
	
		$post = $this->getRequest()->getParams(); 
		
		if($post){
			if(isset($post['reindexall'])){
				//$indexerCollection = $this->_indexerCollectionFactory->create();
				//$allIds = $indexerCollection->getAllIds();
	
				$allIds = array('design_config_grid',
					'customer_grid',
					'catalog_category_product',
					'catalog_product_category',
					'catalogrule_rule',
					'catalog_product_attribute',
					'cataloginventory_stock',
					'inventory',
					'catalogrule_product',
					'catalog_product_price'
				);
				
				foreach ($allIds as $id) {
					$indexer = $this->_indexerFactory->create()->load($id);
					$indexer->reindexAll();
				}				
			}
			
			if(isset($post['reindexsearch'])){				
				$allIds = array('design_config_grid',
					'catalogsearch_fulltext',
					'mst_misspell',
					'mirasvit_search_score_rule_product'
				);
				
				foreach ($allIds as $id) {
					$indexer = $this->_indexerFactory->create()->load($id);
					$indexer->reindexAll();
				}				
			}
		}
		
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
	}
}

?>
