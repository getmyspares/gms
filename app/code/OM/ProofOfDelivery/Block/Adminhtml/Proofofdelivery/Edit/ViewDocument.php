<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Block\Adminhtml\Proofofdelivery\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ViewDocument extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
   public function getButtonData()
    {
        $data = [];
        if ($this->getModelId()) {
            $data = [
                'label' => __('view attach'),
                'class' => 'delete',
                'on_click' => sprintf("window.location.href = '%s';", $this->getViewUrl()),
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * Get URL for delete button
     *
     * @return string
     */
    public function getViewUrl()
    {
        $model_id = $this->getModelId();
        $query = "Select document_path from om_proofofdelivery_proofofdelivery where proofofdelivery_id='$model_id'";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource_conection = $objectManager->create('Magento\Framework\App\ResourceConnection');
        $connection = $resource_conection->getConnection();
        $document_path = $connection->fetchOne($query);
        $url = '/pub/media/proofofdelivery'.$document_path;
        return $url;
    }
}

