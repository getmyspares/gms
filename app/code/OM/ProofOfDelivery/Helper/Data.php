<?php
declare(strict_types=1);

namespace OM\ProofOfDelivery\Helper;
use Exception;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
				\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
				\Panasonic\CustomUser\Helper\Data $panasonicHelper,
				\OM\ProofOfDelivery\Api\ProofofdeliveryRepositoryInterface $ProofofdeliveryRepositoryInterface 
    ) {
        parent::__construct($context);
        $this->_panasonicHelper = $panasonicHelper;
        $this->_proofofdelivery = $ProofofdeliveryRepositoryInterface;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }
    
    public function sendRejectionMail($pod_id){	
			
			$pod = $this->podbyId($pod_id);
			$seller_id = $pod->getSellerId();

			$admin_comment = $pod->getAdminComment();
			$increment_id = $pod->getOrderIncrementId();
			$seller_email = $this->getSellerEmail($seller_id);
			$message="
			<p>Hi,</p>
			<p>Admin has rejected your proof of delivery with the folowing comments!</p>
			<p>$admin_comment</p>
			<p>Please re-submit the docment according to the requirements </p>
			<p>Thanks</p>
			";
			$to=$seller_email;
			$from=$this->getFromEmail();
			$subject="Proof Of Delivery Rejected | order : $increment_id ";
			$this->_panasonicHelper->sendEmailThroughCurl($to,$from,$message,$subject);
		}

		public function sendAcceptanceMail($pod_id){		
			$pod = $this->podbyId($pod_id);
			$seller_id = $pod->getSellerId();
			$admin_comment = $pod->getAdminComment();
			$increment_id = $pod->getOrderIncrementId();
			$seller_email = $this->getSellerEmail($seller_id);
			$message="
			<p>Hi,</p>
			<p>Admin has accepted your proof of delivery for the order $increment_id with the following comments</p>
			<p>$admin_comment</p>
			<p>Shipment has been generated for the same according to the dates provided by you .</p>
			<p>Thanks</p>
			";
			$to=$seller_email;
			$from=$this->getFromEmail();
			$subject="Proof Of Delivery Accepted | order : $increment_id ";
			$this->_panasonicHelper->sendEmailThroughCurl($to,$from,$message,$subject);
		}

		public function podbyId($pod_id){		
			/* using service contract as model load is being depreciated in magento @ritesh5august2021*/
			return $this->_proofofdelivery->get($pod_id);
		}
		

	public function podSubmitted($seller_name,$seller_email,$increment_id){		
		$message="
		<p>Hi,</p>
		<p>Seller $seller_name has submitted proof of delivery for the order $increment_id , please check</p>
		<p>Thanks</p>
		"; 
		$to=$this->getToEmail();
		$from=$this->getFromEmail();
		$subject="Proof Of Delivery Submitted | order : $increment_id ";
		$this->_panasonicHelper->sendEmailThroughCurl($to,$from,$message,$subject);
		$this->informSellerAlso($seller_name,$seller_email,$increment_id);
	}

	public function informSellerAlso($seller_name,$seller_email,$increment_id){		
		$message="
		<p>Hi, $seller_name</p>
		<p>Your proof of delivery for the order $increment_id , has been submitted </p>
		<p>Thanks</p>
		"; 
		$to=$this->getToEmail();
		$from=$this->getFromEmail();
		$subject="Proof Of Delivery Submitted | order : $increment_id ";
		$this->_panasonicHelper->sendEmailThroughCurl($to,$from,$message,$subject);
	}

	public function getSellerEmail($seller_id)
	{
		$customer = $this->_customerRepositoryInterface->getById($seller_id);
		return $customer->getEmail();
	}

	public  function getFromEmail(){
		return  $this->scopeConfig->getValue('trans_email/ident_custom2/email');
	}

	public  function getToEmail(){
		return  explode(',',$this->scopeConfig->getValue('pod/email/admin'));
	}
	
}

