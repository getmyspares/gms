<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OM\ProofOfDelivery\Controller\Adminhtml\Proofofdelivery;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Panasonic\CustomUser\Helper\Data $panasonicHelper,
        \OM\ProofOfDelivery\Helper\Data $helper,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_panasonicHelper = $panasonicHelper;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {

            $resource   = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();

            $id = $this->getRequest()->getParam('proofofdelivery_id');

            $model = $this->_objectManager->create(\OM\ProofOfDelivery\Model\Proofofdelivery::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Proofofdelivery no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            
            $order_id = $data['order_id'];
            $pickup_date = date('Y-m-d H:i:s', strtotime($data['pickup_date']));
            $delivery_date = date('Y-m-d H:i:s', strtotime($data['delivery_date']));

            $delivery_date = $connection->fetchOne("SELECT created_at FROM `sales_order` WHERE entity_id='" . $order_id . "'");
            if(strtotime($data['pickup_date'])<strtotime($delivery_date))
            {
                $this->messageManager->addErrorMessage(__('Pickup Date canot be less than order created date'));
                return $resultRedirect->setPath('*/*/');
            }

            if(strtotime($data['delivery_date'])<strtotime($delivery_date))
            {
                $this->messageManager->addErrorMessage(__('Delivery Date canot be less than order created date'));
                return $resultRedirect->setPath('*/*/');
            }
            
            $approved_status = $data['approved_status'];
            if ($approved_status == '1') {
                

                $connection->query("UPDATE sales_order_grid SET ecom_order='No' WHERE entity_id='$order_id'");
                $order        = $this->_objectManager->create('Magento\Sales\Model\Order')->load($order_id);
                $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
                $shipment     = $convertOrder->toShipment($order);

                foreach ($order->getAllVisibleItems() as $orderItem) {
                    if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                        continue;
                    }
                    $qtyShipped = $orderItem->getQtyToShip();
                    $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
                    $shipment->addItem($shipmentItem);
                }

                // Register shipment
                $shipment->register();
                $shipment->getOrder()->setIsInProcess(true);

                try {
                    // Save created shipment and order
                    $shipment->save();
                    $shipment->getOrder()->save();

                    /* shipment sms */
                    $cId = $order->getCustomerId();
                  
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    $this->messageManager->addError(
                        "Shipment Not Created Because It's already created or products ordered are out of stock"
                    );
                    return false;
                    exit;
                }


                $shipmentcreateddate = date('Y-m-d H:i:s', strtotime($data['pickup_date']));
                $deliveryDate = date('Y-m-d H:i:s', strtotime($data['delivery_date']));

                $iscomplete = $connection->fetchOne("SELECT state FROM `sales_order` WHERE entity_id = '" . $order_id . "' ");
                if ($iscomplete == 'complete') {
                    $connection->query("Update sales_order Set status='Delivered',pickup_date ='$shipmentcreateddate' ,delivery_date='$deliveryDate' where entity_id='$order_id'");
                    $connection->query("Update sales_order_grid Set status='Delivered',delivery_date='$shipmentcreateddate' where entity_id='$order_id'");

                    /* Also needed to update data in sales  and  settlement report @ritesh1april2021 */
                    $connection->query("Update `om_ecom_api_status` Set shipping_type='Manually Delivered',order_status='Delivered',pickup_date ='$shipmentcreateddate' ,delivery_date='$deliveryDate' where order_id='$order_id' and movement_type='Forward'");
                    $quewrrsds = "update om_sales_report set pickup_date='$shipmentcreateddate',delivery_date='$deliveryDate',order_status='Delivered' where order_id='$order_id' and report_type='1'";
					$connection->query($quewrrsds);

                    /* Adding shipment  date and shipment id  also  in sales report  @ritesh1april2021 */
                    $ship_date = $connection->fetchOne("SELECT created_at FROM `sales_shipment_grid` WHERE order_id='" . $order_id . "'");
                    $ship_id = $connection->fetchOne("SELECT entity_id FROM `sales_shipment` WHERE order_id='" . $order_id . "'");
                    $connection->query("Update om_sales_report set shipping_date='" . $ship_date . "',shipment_id='$ship_id' where order_id='$order_id'");
                }

                try {
                    $pickup_date = date('Y-m-d H:i:s', strtotime($data['pickup_date']));
                    $delivery_date = date('Y-m-d H:i:s', strtotime($data['delivery_date']));
                    $approved_status = $data['approved_status'];
                    $model->setData('pickup_date',$pickup_date);
                    $model->setData('delivery_date',$delivery_date);
                    $model->setData('approved_status',$approved_status);
                    $admin_comment = $data['admin_comment'];
                    $model->setData('admin_comment',$admin_comment);
                    $model->save();
                    $this->messageManager->addSuccessMessage(__('You saved the Proof of delivery.'));
                    $this->dataPersistor->clear('om_proofofdelivery_proofofdelivery');

                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', ['proofofdelivery_id' => $model->getId()]);
                    }
                    return $resultRedirect->setPath('*/*/');
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Proof of delivery.'));
                }

                $this->helper->sendAcceptanceMail($id);

                // $this->dataPersistor->set('om_proofofdelivery_proofofdelivery', $data);
                // return $resultRedirect->setPath('*/*/edit', ['proofofdelivery_id' => $this->getRequest()->getParam('proofofdelivery_id')]);
            
            } else if($approved_status == '2' || $approved_status == '0')
            {
                $pickup_date = date('Y-m-d H:i:s', strtotime($data['pickup_date']));
                $delivery_date = date('Y-m-d H:i:s', strtotime($data['delivery_date']));
                $admin_comment = $data['admin_comment'];
                $model->setData('pickup_date',$pickup_date);
                $model->setData('delivery_date',$delivery_date);
                $model->setData('approved_status',$approved_status);
                $model->setData('admin_comment',$admin_comment);
                $model->save();
                if($approved_status == '2'){
                    $this->helper->sendRejectionMail($id);
                }
    
            }
            return $resultRedirect->setPath('*/*/');
        } else{
            echo "data not  found";
        }
    }

    protected function _isAllowed()
    {
        return true;
        return $this->_authorization->isAllowed('OM_ProofOfDelivery::proofofdeliver');
    }

}
