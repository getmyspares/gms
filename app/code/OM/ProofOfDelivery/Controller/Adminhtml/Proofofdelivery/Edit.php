<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Controller\Adminhtml\Proofofdelivery;

class Edit extends \OM\ProofOfDelivery\Controller\Adminhtml\Proofofdelivery
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('proofofdelivery_id');
        $model = $this->_objectManager->create(\OM\ProofOfDelivery\Model\Proofofdelivery::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Proofofdelivery no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('om_proofofdelivery_proofofdelivery', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Proof of delivery') : __('New Proof of delivery'),
            $id ? __('Edit Proof of delivery') : __('New Proof of delivery')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Proofofdeliverys'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Proof of delivery ', $model->getId()) : __('New Proof of delivery'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return true;
        // return $this->_authorization->isAllowed('OM_ProofOfDelivery::proofofdeliver');
    }
}

