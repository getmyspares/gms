<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Controller\Proof;
use Magento\Framework\App\Filesystem\DirectoryList;

class Submit extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \OM\ProofOfDelivery\Helper\Data $helper,
        \Magento\Framework\App\ResourceConnection $ResourceConnection,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_filesystem = $fileSystem;
        $this->_helper = $helper;
        $this->_connection = $ResourceConnection->getConnection();
        $this->logger = $logger;
        parent::__construct($context);
    }
    
    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $post_data = $this->getRequest()->getPost();
            
            $referer_url = $this->_redirect->getRefererUrl();
            $resultRedirect = $this->resultRedirectFactory->create();
            $uploaded_proof="";
            
            
            if(count(explode('/',$post_data['shipping_date']))!=3)
            {
                $this->messageManager->addNoticeMessage("Please enter valid pickup date");
                $resultRedirect->setUrl($referer_url);
                return $resultRedirect;
            }

            if(count(explode('/',$post_data['delivery_date']))!=3)
            {
                $this->messageManager->addNoticeMessage("Please enter valid delivery date");
                $resultRedirect->setUrl($referer_url);
                return $resultRedirect;
            }

            

            if ($_FILES['proof_document']['name']) {
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'proof_document']
                    );
                    $uploader->setAllowedExtensions(['jpg','jpeg','pdf','png']);
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $mediaDirectory = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
                    $imagePath = $uploader->save($mediaDirectory->getAbsolutePath()."proofofdelivery");
                    $uploaded_proof = $imagePath['file'];
                } catch (\Exception $e) {
                    $this->messageManager->addNoticeMessage("Proof Could not be uploaded,Only jpg and pdf are allowed ");
                    $resultRedirect->setUrl($referer_url);
                    return $resultRedirect;
                }
            }

            if(empty($uploaded_proof))
            {
                $this->messageManager->addNoticeMessage("Proof Could not be uploaded , Please try again");
                $resultRedirect->setUrl($referer_url);
                return $resultRedirect;
            }

            
            $order_id = $post_data['order_id'];
            $query = "SELECT seller_id FROM `marketplace_orders` where order_id = '$order_id'";
            $seller_id = $this->_connection->fetchOne($query);
            
            $query = "SELECT increment_id FROM `sales_order` where entity_id = '$order_id'";
            $increment_id = $this->_connection->fetchOne($query);
            

            $query = "Select `firstname`,`lastname`,`email` from customer_entity where entity_id='$seller_id' ";
            $seller_data = $this->_connection->fetchAll($query);
            if(!empty($seller_data))
            {
                if(is_array($seller_data))
                {
                    $seller_name = $seller_data[0]['firstname'] ." ". $seller_data[0]['lastname'];
                    $seller_email = $seller_data[0]['email'];
                }

            }
            $shipping_date =  date('Y-m-d H:i:s', strtotime($post_data['shipping_date']));
            $delivery_date =  date('Y-m-d H:i:s', strtotime($post_data['delivery_date']));

            $wefrf = "SELECT proofofdelivery_id,approved_status,admin_comment FROM `om_proofofdelivery_proofofdelivery` where order_id =". $order_id;
            $proof_submitted_data = $this->_connection->fetchAll($wefrf);	
            
            if(empty($proof_submitted_data))
            {
                $query = "Insert into om_proofofdelivery_proofofdelivery set 
                `pickup_date`='$shipping_date',
                `delivery_date`='$delivery_date',
                `approved_status`='0',
                `document_path`='$uploaded_proof',
                `order_id`='$order_id',
                `order_increment_id`='$increment_id',
                `seller_name`='$seller_name',
                `seller_id`='$seller_id'
                ";

            } else 
            {
                $proof_submitted_id =  $proof_submitted_data[0]['proofofdelivery_id'];
                $query = "update om_proofofdelivery_proofofdelivery set 
                `pickup_date`='$shipping_date',
                `delivery_date`='$delivery_date',
                `approved_status`='0',
                `document_path`='$uploaded_proof' where proofofdelivery_id = '$proof_submitted_id';
                ";
            }
            $status = $this->_connection->query($query);
            $this->messageManager->addNoticeMessage("Thanks ,Proof Has Been Submitted !");
            $this->_helper->podSubmitted($seller_name,$seller_email,$increment_id);
            $resultRedirect->setUrl($referer_url);
            return $resultRedirect;
        }catch (\Exception $e) {
            $this->messageManager->addNoticeMessage("Data could not be saved , please contact admin");
            $resultRedirect->setUrl($referer_url);
            return $resultRedirect;
        }
    }

    protected function _isAllowed(){
        return true;
    }
}

