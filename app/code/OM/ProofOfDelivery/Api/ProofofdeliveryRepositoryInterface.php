<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ProofofdeliveryRepositoryInterface
{

    /**
     * Save proofofdelivery
     * @param \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface $proofofdelivery
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface $proofofdelivery
    );

    /**
     * Retrieve proofofdelivery
     * @param string $proofofdeliveryId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($proofofdeliveryId);

    /**
     * Retrieve proofofdelivery matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliverySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete proofofdelivery
     * @param \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface $proofofdelivery
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface $proofofdelivery
    );

    /**
     * Delete proofofdelivery by ID
     * @param string $proofofdeliveryId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($proofofdeliveryId);
}

