<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Api\Data;

interface ProofofdeliveryInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PROOFOFDELIVERY_ID = 'proofofdelivery_id';
    const APPROVED_STATUS = 'approved_status';
    const PICKUP_DATE = 'pickup_date';
    const ORDER_ID = 'order_id';
    const ORDER_INCREMENT_ID = 'order_increment_id';
    const DELIVERY_DATE = 'delivery_date';
    const UPDATED_AT = 'updated_at';
    const SELLER_ID = 'seller_id';
    const Admin_Comment = 'admin_comment';

    /**
     * Get proofofdelivery_id
     * @return string|null
     */
    public function getProofofdeliveryId();

    /**
     * Set proofofdelivery_id
     * @param string $proofofdeliveryId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setProofofdeliveryId($proofofdeliveryId);

    /**
     * Get pickup_date
     * @return string|null
     */
    public function getPickupDate();

    /**
     * Set pickup_date
     * @param string $pickupDate
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setPickupDate($pickupDate);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \OM\ProofOfDelivery\Api\Data\ProofofdeliveryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OM\ProofOfDelivery\Api\Data\ProofofdeliveryExtensionInterface $extensionAttributes
    );

    /**
     * Get delivery_date
     * @return string|null
     */
    public function getDeliveryDate();

    /**
     * Set delivery_date
     * @param string $deliveryDate
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setDeliveryDate($deliveryDate);

    /**
     * Get approved_status
     * @return string|null
     */
    public function getApprovedStatus();

    /**
     * Set approved_status
     * @param string $approvedStatus
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setApprovedStatus($approvedStatus);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setOrderId($orderId);

    
    /**
     * Get order_increment_id
     * @return string|null
     */
    public function getOrderIncrementId();


    /**
     * Get admin_comment
     * @return string|null
     */
    public function getAdminComment();
    
    /**
     * Get seller_id
     * @return string|null
     */
    public function getSellerId();

    /**
     * Set order_increment_id
     * @param string $orderIncrementId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setOrderIncrementId($orderIncrementId);
}

