<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Api\Data;

interface ProofofdeliverySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get proofofdelivery list.
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface[]
     */
    public function getItems();

    /**
     * Set pickup_date list.
     * @param \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

