<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'proofofdelivery_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \OM\ProofOfDelivery\Model\Proofofdelivery::class,
            \OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery::class
        );
    }
}

