<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Model;

use Magento\Framework\Api\DataObjectHelper;
use OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface;
use OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterfaceFactory;

class Proofofdelivery extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $proofofdeliveryDataFactory;

    protected $_eventPrefix = 'om_proofofdelivery_proofofdelivery';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ProofofdeliveryInterfaceFactory $proofofdeliveryDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery $resource
     * @param \OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ProofofdeliveryInterfaceFactory $proofofdeliveryDataFactory,
        DataObjectHelper $dataObjectHelper,
        \OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery $resource,
        \OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery\Collection $resourceCollection,
        array $data = []
    ) {
        $this->proofofdeliveryDataFactory = $proofofdeliveryDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve proofofdelivery model with proofofdelivery data
     * @return ProofofdeliveryInterface
     */
    public function getDataModel()
    {
        $proofofdeliveryData = $this->getData();
        
        $proofofdeliveryDataObject = $this->proofofdeliveryDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $proofofdeliveryDataObject,
            $proofofdeliveryData,
            ProofofdeliveryInterface::class
        );
        
        return $proofofdeliveryDataObject;
    }
}

