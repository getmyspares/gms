<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterfaceFactory;
use OM\ProofOfDelivery\Api\Data\ProofofdeliverySearchResultsInterfaceFactory;
use OM\ProofOfDelivery\Api\ProofofdeliveryRepositoryInterface;
use OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery as ResourceProofofdelivery;
use OM\ProofOfDelivery\Model\ResourceModel\Proofofdelivery\CollectionFactory as ProofofdeliveryCollectionFactory;

class ProofofdeliveryRepository implements ProofofdeliveryRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $proofofdeliveryCollectionFactory;

    protected $searchResultsFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $proofofdeliveryFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $dataProofofdeliveryFactory;


    /**
     * @param ResourceProofofdelivery $resource
     * @param ProofofdeliveryFactory $proofofdeliveryFactory
     * @param ProofofdeliveryInterfaceFactory $dataProofofdeliveryFactory
     * @param ProofofdeliveryCollectionFactory $proofofdeliveryCollectionFactory
     * @param ProofofdeliverySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceProofofdelivery $resource,
        ProofofdeliveryFactory $proofofdeliveryFactory,
        ProofofdeliveryInterfaceFactory $dataProofofdeliveryFactory,
        ProofofdeliveryCollectionFactory $proofofdeliveryCollectionFactory,
        ProofofdeliverySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->proofofdeliveryFactory = $proofofdeliveryFactory;
        $this->proofofdeliveryCollectionFactory = $proofofdeliveryCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataProofofdeliveryFactory = $dataProofofdeliveryFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface $proofofdelivery
    ) {
        /* if (empty($proofofdelivery->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $proofofdelivery->setStoreId($storeId);
        } */
        
        $proofofdeliveryData = $this->extensibleDataObjectConverter->toNestedArray(
            $proofofdelivery,
            [],
            \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface::class
        );
        
        $proofofdeliveryModel = $this->proofofdeliveryFactory->create()->setData($proofofdeliveryData);
        
        try {
            $this->resource->save($proofofdeliveryModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the proofofdelivery: %1',
                $exception->getMessage()
            ));
        }
        return $proofofdeliveryModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($proofofdeliveryId)
    {
        $proofofdelivery = $this->proofofdeliveryFactory->create();
        $this->resource->load($proofofdelivery, $proofofdeliveryId);
        if (!$proofofdelivery->getId()) {
            throw new NoSuchEntityException(__('proofofdelivery with id "%1" does not exist.', $proofofdeliveryId));
        }
        return $proofofdelivery->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->proofofdeliveryCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface $proofofdelivery
    ) {
        try {
            $proofofdeliveryModel = $this->proofofdeliveryFactory->create();
            $this->resource->load($proofofdeliveryModel, $proofofdelivery->getProofofdeliveryId());
            $this->resource->delete($proofofdeliveryModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the proofofdelivery: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($proofofdeliveryId)
    {
        return $this->delete($this->get($proofofdeliveryId));
    }
}

