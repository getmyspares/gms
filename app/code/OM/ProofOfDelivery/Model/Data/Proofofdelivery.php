<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Model\Data;

use OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface;

class Proofofdelivery extends \Magento\Framework\Api\AbstractExtensibleObject implements ProofofdeliveryInterface
{

    /**
     * Get proofofdelivery_id
     * @return string|null
     */
    public function getProofofdeliveryId()
    {
        return $this->_get(self::PROOFOFDELIVERY_ID);
    }

    /**
     * Set proofofdelivery_id
     * @param string $proofofdeliveryId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setProofofdeliveryId($proofofdeliveryId)
    {
        return $this->setData(self::PROOFOFDELIVERY_ID, $proofofdeliveryId);
    }

    /**
     * Get pickup_date
     * @return string|null
     */
    public function getPickupDate()
    {
        return $this->_get(self::PICKUP_DATE);
    }

    /**
     * Set pickup_date
     * @param string $pickupDate
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setPickupDate($pickupDate)
    {
        return $this->setData(self::PICKUP_DATE, $pickupDate);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \OM\ProofOfDelivery\Api\Data\ProofofdeliveryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OM\ProofOfDelivery\Api\Data\ProofofdeliveryExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get delivery_date
     * @return string|null
     */
    public function getDeliveryDate()
    {
        return $this->_get(self::DELIVERY_DATE);
    }

    /**
     * Set delivery_date
     * @param string $deliveryDate
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setDeliveryDate($deliveryDate)
    {
        return $this->setData(self::DELIVERY_DATE, $deliveryDate);
    }

    /**
     * Get approved_status
     * @return string|null
     */
    public function getApprovedStatus()
    {
        return $this->_get(self::APPROVED_STATUS);
    }

    /**
     * Set approved_status
     * @param string $approvedStatus
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setApprovedStatus($approvedStatus)
    {
        return $this->setData(self::APPROVED_STATUS, $approvedStatus);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * Set order_id
     * @param string $orderId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get order_increment_id
     * @return string|null
     */
    public function getOrderIncrementId()
    {
        return $this->_get(self::ORDER_INCREMENT_ID);
    }

    /**
     * Get admin_comment
     * @return string|null
     */
    public function getAdminComment()
    {
        return $this->_get(self::Admin_Comment);
    }

    /**
     * Get seller_id
     * @return string|null
     */
    public function getSellerId()
    {
        return $this->_get(self::SELLER_ID);
    }
    
    /**
     * Set order_increment_id
     * @param string $orderIncrementId
     * @return \OM\ProofOfDelivery\Api\Data\ProofofdeliveryInterface
     */
    public function setOrderIncrementId($orderIncrementId)
    {
        return $this->setData(self::ORDER_INCREMENT_ID, $orderIncrementId);
    }

   
    public function setSellerId($seller_id)
    {
        return $this->setData(self::SELLER_ID, $seller_id);
    }

    
    public function setAdminComment($admin_comment)
    {
        return $this->setData(self::Admin_Comment, $admin_comment);
    }
}

