<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\ProofOfDelivery\Ui\Component\Listing\Column;

class DeliveryDate extends \Magento\Ui\Component\Listing\Columns\Column
{


    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['proofofdelivery_id'])) {
                  if ($this->getData('name')=='delivery_date') {
                      $delivery_date = $item[$this->getData('name')];
                      $todatedeliivery = date_create($delivery_date);
                      $item[$this->getData('name')]= date_format($todatedeliivery, 'd-m-Y');
                    }
                }
                if (isset($item['proofofdelivery_id'])) {
                  if ($this->getData('name')=='pickup_date') {
                      $delivery_date = $item[$this->getData('name')];
                      $todatedeliivery = date_create($delivery_date);
                      $item[$this->getData('name')]= date_format($todatedeliivery, 'd-m-Y');
                    }
                }
            }
        }
        return $dataSource;
    }
}

