<?php
namespace OM\NotifySellerForLowStock\Controller\Index;
use Magento\Store\Model\StoreManagerInterface;
class NotifySeller extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	protected $_inlineTranslation;
    protected $storeManager;
    protected $_scopeConfig;
    protected $_helper;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Webkul\Marketplace\Helper\Data $helper)
	{
		$this->_pageFactory = $pageFactory;
		$this->_inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_helper = $helper;
		return parent::__construct($context);
	}

	public function execute()
	{
		$isEnable = $this->_scopeConfig->getValue('lowstocknotification/general/enable');
        $ccEmail = $this->_scopeConfig->getValue('lowstocknotification/general/low_notification_cc_email');
        $cc = explode(',',$ccEmail);
        $fromEmail = $this->_scopeConfig->getValue('trans_email/ident_custom2/email');
        $selectedTemplateId = $this->_scopeConfig->getValue('lowstocknotification/general/seller_low_stock');
        $templateId = $selectedTemplateId;
        $fromName = 'GetMySpares';  
        if($isEnable){
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
                $sql = "SELECT DISTINCT seller_id FROM `marketplace_product` WHERE mageproduct_id in(SELECT product_id FROM `cataloginventory_stock_item` WHERE qty < min_sale_qty)";
                $results = $connection->fetchAll($sql);
                foreach ($results as $result) {
                    $sellerId = $result['seller_id'];
                    /*quick fix for skipping the process for deactivated seller @ritesh03Sept2021*/
                    $query = "SELECT is_seller FROM `marketplace_userdata` where seller_id='$sellerId'";
                    $seller_status = $connection->fetchOne($query);
                    if($seller_status!=1)
                    {
                        continue;
                    }
                    $sql = "SELECT mageproduct_id  FROM `marketplace_product` WHERE `seller_id` = $sellerId and mageproduct_id in(SELECT product_id FROM `cataloginventory_stock_item` WHERE qty < min_sale_qty)";
                    $products = $connection->fetchAll($sql);
                    $columns = $this->getProductLowStockColumns();
                    $csvHeader = array_values($columns);
                    $filename = 'product-low-stock-'.date('Y-m-d-H-i-s').'.csv';
                    $final_text_data="";
                    $fp = fopen('php://memory', 'w');
                    fputcsv( $fp, $csvHeader,",");
                    foreach ($products as $product) {
                        $insertArr = $this->getSellerProducts($product);
                        fputcsv($fp, $insertArr, ",");
                    }
                    fseek($fp, 0);
                    $file = stream_get_contents($fp);
                    fclose($fp);
                    $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
                    $sql = "SELECT email FROM `customer_entity` WHERE `entity_id` = $sellerId";
                    $toEmail = $connection->fetchOne($sql);
                    $sql = "SELECT firstname FROM `customer_entity` WHERE `entity_id` = $sellerId";
                    $sellerName = $connection->fetchOne($sql);
                    try {
                            $templateVars = [
                                'sellername' => $sellerName
                            ];

                            $storeId = $this->storeManager->getStore()->getId();

                            $from = ['email' => $fromEmail, 'name' => $fromName];
                            $this->_inlineTranslation->suspend();

                            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                            $templateOptions = [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => $storeId
                            ];
                            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                                ->setTemplateOptions($templateOptions)
                                ->setTemplateVars($templateVars)
                                ->setFrom($from)
                                ->addTo($toEmail)
                                ->addCc($cc)
                                ->addAttachment($file, $filename, 'application/octet-stream')
                                ->getTransport();
                            $transport->sendMessage();
                            $this->_inlineTranslation->resume();

                        } catch (\Exception $e) {
                            echo $e->getMessage();
                        }
                }
                
                
        }
        echo 'success';
        die();
	}
    public function getProductLowStockColumns(){
        return ['Product-Sku','Avialable Qty','Minimum Saleable Qty'];
    }
    public function getSellerProducts($product){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
        $insertArr=[];    
        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($product['mageproduct_id']);
        $marketplaceProduct = $this->_helper->getSellerProductDataByProductId($product['mageproduct_id']);
        $insertArr[] = $_product->getSku();
        $query = "SELECT qty FROM `cataloginventory_stock_item` WHERE `product_id` = ".$product['mageproduct_id'];
        $productQty = $connection->fetchOne($query);
        $insertArr[] = $productQty;
        $query = "SELECT min_sale_qty FROM `cataloginventory_stock_item` WHERE `product_id` = ".$product['mageproduct_id'];
        $minProductQty = $connection->fetchOne($query);
        $insertArr[] = $minProductQty;
        return $insertArr;
    }
}