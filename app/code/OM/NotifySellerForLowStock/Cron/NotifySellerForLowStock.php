<?php
declare(strict_types=1);

namespace OM\NotifySellerForLowStock\Cron;
use Magento\Store\Model\StoreManagerInterface;

/* TODO: instances of dicert use of object manager still need to be removed , Done : removed all instance of direct use of resource connection and centralised it by injecting in constructor ritesh03sept2021*/

class NotifySellerForLowStock
{

    protected $logger;
    protected $_inlineTranslation;
    protected $_scopeConfig;
    protected $_helper;
    protected $storeManager;
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\ObjectManager $objectManager,
        StoreManagerInterface $storeManager,
        \Webkul\Marketplace\Helper\Data $helper)
    {
       
        $this->_inlineTranslation = $inlineTranslation;
        $this->_scopeConfig = $scopeConfig;
        $this->_helper = $helper;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->objectManager = $objectManager;
        $this->productRepository = $productRepository;
        $this->connection = $resourceConnection->getConnection();
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $isEnable = $this->_scopeConfig->getValue('lowstocknotification/general/enable');
        $ccEmail = $this->_scopeConfig->getValue('lowstocknotification/general/low_notification_cc_email');
        $cc = explode(',',$ccEmail);
        $fromEmail = $this->_scopeConfig->getValue('trans_email/ident_custom2/email');
        $selectedTemplateId = $this->_scopeConfig->getValue('lowstocknotification/general/seller_low_stock');
        $templateId = $selectedTemplateId;
        $fromName = 'GetMySpares';  
        if($isEnable){

            /*object manger should not be used , putting object manager and resource connection in constructor for future use @ritesh3sept2021*/
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                /*removing these and fetching values from constructor @ritesh03sept2021*/
                /*$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();*/

                $sql = "SELECT DISTINCT seller_id FROM `marketplace_product` WHERE mageproduct_id in(SELECT product_id FROM `cataloginventory_stock_item` WHERE qty < min_sale_qty)";
                $results = $this->connection->fetchAll($sql);
                foreach ($results as $result) {
                    $sellerId = $result['seller_id'];

                    /*quick fix for skipping the process for deactivated seller @ritesh03Sept2021*/
                    $query = "SELECT is_seller FROM `marketplace_userdata` where seller_id='$sellerId'";
                    $seller_status = $this->connection->fetchOne($sql);

                    $sql = "SELECT email FROM `customer_entity` WHERE `entity_id` = $sellerId";
                    $toEmail = $this->connection->fetchOne($sql);

                    if($seller_status!=1)
                    {
                        echo "Skipped for seller id".$toEmail;
                        continue;
                    }
                    echo "proceseed for seller id ".$toEmail;

                    $sql = "SELECT mageproduct_id  FROM `marketplace_product` WHERE `seller_id` = $sellerId and mageproduct_id in(SELECT product_id FROM `cataloginventory_stock_item` WHERE qty < min_sale_qty)";
                    $products = $this->connection->fetchAll($sql);
                    $columns = $this->getProductLowStockColumns();
                    $csvHeader = array_values($columns);
                    $filename = 'product-low-stock-'.date('Y-m-d-H-i-s').'.csv';
                    $final_text_data="";
                    $fp = fopen('php://memory', 'w');
                    fputcsv( $fp, $csvHeader,",");
                    foreach ($products as $product) {
                        $insertArr = $this->getSellerProducts($product);
                        fputcsv($fp, $insertArr, ",");
                    }
                    fseek($fp, 0);
                    $file = stream_get_contents($fp);
                    fclose($fp);
                    $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');


                    $sql = "SELECT firstname FROM `customer_entity` WHERE `entity_id` = $sellerId";
                    $sellerName = $this->connection->fetchOne($sql);

                    try {
                            $templateVars = [
                                'sellername' => $sellerName
                            ];

                            $storeId = $this->storeManager->getStore()->getId();

                            $from = ['email' => $fromEmail, 'name' => $fromName];
                            $this->_inlineTranslation->suspend();

                            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                            $templateOptions = [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => $storeId
                            ];
                            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                                ->setTemplateOptions($templateOptions)
                                ->setTemplateVars($templateVars)
                                ->setFrom($from)
                                ->addTo($toEmail)
                                ->addCc($cc)
                                ->addAttachment($file, $filename, 'application/octet-stream')
                                ->getTransport();
                            $transport->sendMessage();
                            $this->_inlineTranslation->resume();

                        } catch (\Exception $e) {
                            echo $e->getMessage();
                        }

                }
                
                
        }
    
        $this->logger->addInfo("Cronjob NotifySellerForLowStock is executed.");
    }
    public function getProductLowStockColumns(){
        return ['Product-Sku','Product Name','Avialable Qty','Minimum Saleable Qty'];
    }
    public function getSellerProducts($product){
        $insertArr=[];

        /* removing these and fetching values from constructor @ritesh03sept2021
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $resource->getConnection();
        */

        /*load method is also depreciated removing direct use and calling product repository from constructor @ritesh03sept2021
        $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($product['mageproduct_id']);
        */

        $_product = $this->productRepository->getById($product['mageproduct_id']);
        $marketplaceProduct = $this->_helper->getSellerProductDataByProductId($product['mageproduct_id']);
        $insertArr[] = $_product->getSku();
        $insertArr[] = $_product->getName();
        $query = "SELECT qty FROM `cataloginventory_stock_item` WHERE `product_id` = ".$product['mageproduct_id'];
        $productQty = $this->connection->fetchOne($query);
        $insertArr[] = $productQty;
        $query = "SELECT min_sale_qty FROM `cataloginventory_stock_item` WHERE `product_id` = ".$product['mageproduct_id'];
        $minProductQty = $this->connection->fetchOne($query);
        $insertArr[] = $minProductQty;
        return $insertArr;
    }
}

