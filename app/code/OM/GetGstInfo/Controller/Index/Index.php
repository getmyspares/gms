<?php
namespace OM\GetGstInfo\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
	{
		$this->_pageFactory = $pageFactory;
		$this->_scopeConfig = $scopeConfig;
		$this->resource = $resource;
		return parent::__construct($context);
	}

	public function execute()
	{
		$isEnable = $this->_scopeConfig->getValue('gstinfo/general/enable');
		$gstNumber = $_POST['gstNumber'];
		$url = $this->_scopeConfig->getValue('gstinfo/general/endpoint_url');
		$key = $this->_scopeConfig->getValue('gstinfo/general/api_key');
		$seceret = $this->_scopeConfig->getValue('gstinfo/general/api_secret_key');
		if($isEnable){
			$post_url = $url.'authenticate';
			$curl = curl_init($post_url);
			$headers = array(
			        'x-api-key:'.$key,
			        'x-api-secret:'.$seceret,
			        'x-api-version:1.0',
					'Accept: application/json'
			        );
			curl_setopt($curl, CURLOPT_URL, $post_url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_POST, true);		
			curl_setopt($curl, CURLOPT_POSTFIELDS, array());
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); 
			$post_response = curl_exec($curl);
			
			$authCode = json_decode($post_response,true);
			$token = $authCode['access_token'];
			if($token){
				$endPointUrl = $url.'gsp/public/gstin/'.$gstNumber;
				$curl = curl_init($endPointUrl);
				$headers = array(
			        'Authorization:'.$token,
			        'x-api-key:'.$key,
			        'x-api-version:1.0'
			        );
			curl_setopt($curl, CURLOPT_URL, $endPointUrl);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); 
			$response = curl_exec($curl);
			//$results = json_decode($response,true);
			echo json_encode($response);
			die();
			}

		}
		return $this->_pageFactory->create();
	}
}
