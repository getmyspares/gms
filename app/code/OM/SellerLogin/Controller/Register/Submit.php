<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\SellerLogin\Controller\Register;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;

class Submit extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Model\CustomerFactory $_customerFactory,
        \Magento\Customer\Model\Session $_customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $ScopeConfigInterface,
        \Magento\Framework\App\ResourceConnection $ResourceConnection,
        \Panasonic\CustomUser\Helper\Data $customerUserHelper,
        UrlInterface $url,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellerCollection,
        \Panasonic\CustomUser\Helper\Customfunction $customerUserCustomfunctionHelper,
        \Magento\Store\Model\StoreManagerInterface $StoreManagerInterface
    ) {
        $this->customerFactory=$_customerFactory;
        $this->customerSession=$_customerSession;
        $this->sellerCollectionFactory = $sellerCollection;
        $this->scopeConfigInterface=$ScopeConfigInterface;
        $this->storeManagerInterface=$StoreManagerInterface;
        
        $this->customerUserHelper=$customerUserHelper;
        $this->url = $url;
        $this->customerUserCustomfunctionHelper=$customerUserCustomfunctionHelper;
        $this->connection=$ResourceConnection->getConnection();
        $this->logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $group_id=4;
        $email=$post['email'];
        $firstname=$post['firstname'];
        $lastname=$post['lastname'];
        $password=$post['password'];
        $mobile=$post['mobile'];
        $shopurl=$post['profileurl'];
        $company_nam = $post['company_nam'];
        $company_add = $post['company_add'];
        $gst_number = $post['gst_number'];
        $pan_number= $post['pan_number'];
        $invoice_prefix= $post['invoice_prefix'];
        $domaincheck = substr($email, -4);
        if(strpos($domaincheck, ".ru"))
        {
            echo "Invalid data";
            die();
            $this->messageManager->addNoticeMessage(__('Invalid Data'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('seller/create/index'));
			return $resultRedirect;
            die();
        }
        
        if(strpos($mobile, ".ru"))
        {
            echo "Invalid data";
            die();
            $this->messageManager->addNoticeMessage(__('Invalid Data'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('seller/create/index'));
			return $resultRedirect;
            die();
        }

        $emailexist = $this->checkIfEmailAlreadyExist($email);
        $otpverfied = $this->checkIfOtpVerified();
        $mobileexist = $this->checkIfMobileNumberAlreadyExist($mobile);
        $shopurlexist =  $this->checkIfShopUrlExist($shopurl);
        $invoiceAlready =  $this->checkInvoicePrefix($invoice_prefix);


        if(!$invoice_prefix)
        {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('seller/create/index'));
            $this->messageManager->addNoticeMessage(__('Please Fill Invoice Prefix')); 
			return $resultRedirect;
        }

        if($emailexist==true || $otpverfied==false || $mobileexist==true || $shopurlexist==true || $invoiceAlready==true )
        {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('seller/create/index'));
			return $resultRedirect;
            die();     		
        }

        $websiteId = $this->storeManagerInterface->getWebsite()->getWebsiteId();
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->setEmail($email);
        $customer->setFirstname($firstname);
        $customer->setLastname($lastname);
        $customer->setPassword($password);
        $customer->setMobile($mobile); 
        $customer->setData("company_nam",$company_nam);
        $customer->setData("company_add",$company_add);
        $customer->setData("gst_number",$gst_number);
        $customer->setData("pan_number",$pan_number);
        $customer->save();
        $custID=$customer->getId();
        $sellerId = $custID;
			
        /* Seller Region & ZipCode Save By OM */
        if(isset($_POST['seller_zipcode'])) {
            $seller_zipcode = $_POST['seller_zipcode'];			
            $postcode_exist = $this->connection->fetchOne("SELECT `id` FROM `seller_postcode` WHERE `seller_id` LIKE '$sellerId'");
            if(!$postcode_exist){
                $this->connection->query("INSERT INTO `seller_postcode` (`seller_id`, `postcode`) VALUES ('$sellerId', '$seller_zipcode')");
            }else{
                $this->connection->query("UPDATE `seller_postcode` set `postcode`='$seller_zipcode' WHERE `seller_id` LIKE '$sellerId'");
            }
        }		
        /* dispartching events for other extensions to hook ,
         webkul  marketplace is  using this event for  seller registration
        * @ritesh13012021        
        */	
        $this->_eventManager->dispatch(
            'customer_register_success',['account_controller' => $this, 'customer' => $customer]
        );
        $isconfirmationrequired = $this->scopeConfigInterface->getValue('customer/create_account/confirm');
        if($isconfirmationrequired)
        {
            $customer->sendNewAccountEmail('confirmation', '', $customer->getStoreId());
        } else 
        {
            $sql = "update customer_entity set confirmation = NULL WHERE entity_id='".$custID."' ";
            $this->connection->query($sql);  
        
            $sql = "update customer_grid_flat set confirmation = NULL WHERE entity_id='".$custID."' ";
            $this->connection->query($sql);   
           
            $this->customerUserHelper->individual_welcome($firstname,$lastname,$email) ; 				
        }
        $sql = "update customer_grid_flat set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
        $this->connection->query($sql);
        
        $sql = "update customer_entity set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
        $this->connection->query($sql);
        $this->messageManager->addNoticeMessage(__('Seller Account Has Been Created'));       
        $this->customerSession->setRegistrationSuccusfulNeedConfirmation("yes");
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->url->getUrl('seller/create/index/'));
        return $resultRedirect;
    }



    private function checkIfOtpVerified(){
        // return  true;
        $customer_date = $this->customerSession->getCustomerRegisterOtpDetails();
		$customer_date_array=json_decode($customer_date,true);
		$isotpconfirmed = $customer_date_array['is_otp_confirmed'];
		
		if($isotpconfirmed!='yes')
		{
			// $this->messageManager->addNoticeMessage(__('Otp does not match'));  
            echo "Invalid data";
            die();
			$this->messageManager->addNoticeMessage(__('Invalid Data'));  
            return false;
        }        
        return  true;
    }

    
    private function checkInvoicePrefix($invoice_prefix){
        $filteredData=$this->sellerCollectionFactory->create()->addFieldToFilter('invoice_prefix',$invoice_prefix);
        echo $filteredData->getSelect()->__toString();

        if($filteredData->getSize())
        {
            $this->messageManager->addNoticeMessage(__('Invoice Prefix Already Exist')); 
            return true;
        }
        return false;
    }

    private function checkIfEmailAlreadyExist($email)
    {
        $CustomerModel = $this->customerFactory->create();
        $CustomerModel->setWebsiteId(1);
        $CustomerModel->loadByEmail($email);
        $userIdcheck = $CustomerModel->getId();
        if($userIdcheck!='')
        {
            $this->messageManager->addNoticeMessage(('Enter Email ID already Exists'));  
            return true;
        }
        return false;
    }

    private function checkIfMobileNumberAlreadyExist($mobile){
        $check_mobile=$this->customerUserCustomfunctionHelper->check_customer_mobile($mobile);
        if($check_mobile > 0)
        {
            $this->messageManager->addNoticeMessage(__('Enter Mobile number already exists')); 
            return true;
        }
        return false;
    }

    private function checkIfShopUrlExist($url){
        $profileurlcount=$this->sellerCollectionFactory->create()->addFieldToFilter('shop_url',$url);
        if($profileurlcount->getSize())
        {
            $this->messageManager->addNoticeMessage(__('Shop Url Already Exist')); 
            return true;
        }
        return false;
    }
}

