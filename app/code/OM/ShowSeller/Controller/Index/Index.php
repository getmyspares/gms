<?php
namespace OM\ShowSeller\Controller\Index;
 
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\PageFactory;
 
 
class Index extends Action
{
 
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
 
    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;
 
 
    /**
     * View constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory, JsonFactory $resultJsonFactory,\Magento\Framework\App\ResourceConnection $resource)
    {
 
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_resource = $resource;
 
        parent::__construct($context);
    }
 
 
    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $_connection = $this->_resource->getConnection();
        $sellerId ='';
        $result = $this->_resultJsonFactory->create();
        $resultPage = $this->_resultPageFactory->create();
        $productId = $this->getRequest()->getParam('productId');
        $pincode = $this->getRequest()->getParam('pincode');
        $sellerId = $_connection->fetchOne("SELECT seller_id FROM `marketplace_userdata` mu WHERE mu.seller_id in (SELECT mp.seller_id FROM `marketplace_product` mp where mageproduct_id='".$productId."' and status = 1) and is_seller = 1 and seller_priority = 1");
        if($sellerId){
            $sellerId = $sellerId;
        }
        else{
        $sql = "SELECT lattitude FROM `pincode_checker` where pincode = $pincode";
        $customerLat = $_connection->fetchOne($sql);
        $sql = "SELECT longitude FROM `pincode_checker` where pincode = $pincode";
        $customerLong=$_connection->fetchOne($sql);
        $sql = "SELECT seller_id FROM `marketplace_userdata` mu WHERE mu.seller_id in (SELECT mp.seller_id FROM `marketplace_product` mp where mageproduct_id='".$productId."' and status = 1) and is_seller = 1 ORDER BY mu.`seller_priority` ASC";
        $results = $_connection->fetchAll($sql);
        $distanceToMeasure = (float)100000;
        foreach($results as $sellerIds){
            $sId = $sellerIds['seller_id'];
            $sql ="SELECT postcode FROM `seller_postcode` where seller_id = $sId";
            $sellerPostCode = $_connection->fetchOne($sql);
            $sql = "SELECT lattitude FROM `pincode_checker` where pincode = $sellerPostCode";
            $sellerLat = $_connection->fetchOne($sql);
            $sql = "SELECT longitude FROM `pincode_checker` where pincode = $sellerPostCode";
            $sellerLong=$_connection->fetchOne($sql);
            $point1 = array('lat' => $customerLat, 'long' => $customerLong);
            $point2 = array('lat' => $sellerLat, 'long' => $sellerLong);
            $lat1 = $point1['lat'];
            $lon1 = $point1['long'];
            $lat2 = $point2['lat'];
            $lon2 = $point2['long'];
            $theta = $lon1 - $lon2;
            $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
            $miles = acos($miles);
            $miles = rad2deg($miles);
            $miles = $miles * 60 * 1.1515;
            $feet = $miles * 5280;
            $yards = $feet / 3;
            $kilometers = $miles * 1.609344;
            $meters = $kilometers * 1000;
            $distance = compact('kilometers'); 
            foreach ($distance as $unit => $value) {
                $sellerCustomerDistance = round((float)$value,4);
                if($distanceToMeasure > $sellerCustomerDistance){
                    $distanceToMeasure = $sellerCustomerDistance;
                    $sellerId = $sId;
                }
                
            }
        }
    }
        $data = array('seller_id'=>$sellerId);
        $block = $resultPage->getLayout()
                ->createBlock('Magento\Framework\View\Element\Template')
                ->setTemplate('OM_ShowSeller::seller-data.phtml')
                ->setData('data',$data)
                ->toHtml();
                return $this->getResponse()->setBody($block);
    }
 
}