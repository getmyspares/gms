<?php
namespace OM\Custom\Model;

class Gmscity extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\Custom\Model\ResourceModel\Gmscity');
    }
}
?>