<?php
namespace OM\Custom\Model\ResourceModel;

class Gmscity extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('om_city_state', 'id');
    }
}
?>