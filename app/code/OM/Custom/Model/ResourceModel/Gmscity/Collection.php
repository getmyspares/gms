<?php

namespace OM\Custom\Model\ResourceModel\Gmscity;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\Custom\Model\Gmscity', 'OM\Custom\Model\ResourceModel\Gmscity');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>