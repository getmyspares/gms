<?php

namespace OM\Custom\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){
			$installer->run('create table om_city_state(id int not null auto_increment, city varchar(255), state varchar(255),state_id varchar(255),primary key(id))');		
			$installer->run('create table om_area_interest(id int not null auto_increment, name varchar(100), primary key(id))'); 
		}

        $installer->endSetup();

    }
}
