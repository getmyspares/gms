<?php
namespace OM\Custom\Block\Adminhtml\Gmscity;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \OM\Custom\Model\gmscityFactory
     */
    protected $_gmscityFactory;

	protected $_objectManager;
	
    /**
     * @var \OM\Custom\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \OM\Custom\Model\gmscityFactory $gmscityFactory
     * @param \OM\Custom\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \OM\Custom\Model\GmscityFactory $GmscityFactory,
        \OM\Custom\Model\Status $status,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_gmscityFactory = $GmscityFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_gmscityFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'city',
					[
						'header' => __('City'),
						'index' => 'city',
					]
				);
				
				/*$this->addColumn(
					'state',
					[
						'header' => __('State'),
						'index' => 'state',
					]
				);*/
				
				$this->addColumn(
					'state_id',
					[
						'header' => __('State'),
						'index' => 'state_id',
						'type' => 'options',
						'options' => \OM\Custom\Block\Adminhtml\Gmscity\Grid::indiaRegions()
					]
				);
				


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('custom/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('custom/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('OM_Custom::gmscity/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('gmscity');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('custom/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('custom/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('custom/*/index', ['_current' => true]);
    }

    /**
     * @param \OM\Custom\Model\gmscity|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'custom/*/edit',
            ['id' => $row->getId()]
        );
		
    }

	public static function indiaRegions(){	
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $_resource  =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
		$regions    = $connection->fetchAll("SELECT region_id,default_name  FROM `directory_country_region` WHERE `country_id` LIKE 'IN'");
		
		$retArr = array();
		foreach($regions as $region){
			$retArr[$region['region_id']] = $region['default_name'];
		}
		return $retArr;
	}
	
	/*public static function indiaRegionsCities(){	
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $_resource  =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
		$regions    = $connection->fetchAll("SELECT city FROM `om_city_state` order by city asc");
		
		$retArr = array();
		foreach($regions as $region){
			$retArr[$region['city']] = $region['city'];
		}
		return $retArr;
	}
	
	public static function areaOfInterests(){	
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $_resource  =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
		$interests    = $connection->fetchAll("SELECT name FROM `om_area_interest` ORDER BY `om_area_interest`.`name` ASC");
		
		$retArr = array();
		foreach($interests as $interest){
			$retArr[$interests['name']] = $interests['name'];
		}
		return $retArr;
	}*/


}
