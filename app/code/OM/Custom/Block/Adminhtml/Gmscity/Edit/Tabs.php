<?php
namespace OM\Custom\Block\Adminhtml\Gmscity\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('gmscity_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Gmscity Information'));
    }
}