<?php

namespace OM\Custom\Controller\Adminhtml\Index;


class Index extends \Magento\Backend\App\Action
{
    public function execute()
    {
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$request    = $om->get('Magento\Framework\App\RequestInterface');
		$_resource  =  $om->create('\Magento\Framework\App\ResourceConnection');
        $connection = $_resource->getConnection();
        
		$download = $request->getParam('download');
		$five30 = (5*60*60)+(30*60);
		
		if($download == 'yes'){
			$header1  = array('Aging by order date');
			$header2  = array('Aging by status');
			$agingArr = array('AGING');
			$newArr   = array('New');
			$procArr  = array('Processing');
			$shipArr  = array('Shipped');
			$totalArr = array('Grand Total');
			
			$gap1 =  array('');
			
			// Aging by order date
			for($a=0; $a<26; $a++){
				$agingArr[] = $a;
				$startdate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
				$startdate = date('Y-m-d H:i:s',$startdate);
				
				$enddate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
				$enddate = date('Y-m-d H:i:s',$enddate + 86399);
				
				$baseQuery = "select count(entity_id) from sales_order where created_at BETWEEN  '$startdate' AND '$enddate'";
				$newS  = $connection->fetchOne($baseQuery." AND status like '%new%' ");
				$proS  = $connection->fetchOne($baseQuery." AND status like '%processing%' ");
				$shipS = $connection->fetchOne($baseQuery." AND status like '%shipped%' ");
				$total = $newS + $proS + $shipS;
				$newArr[]   = $newS;
				$procArr[]  = $proS;
				$shipArr[]  = $shipS;
				$totalArr[] = $total;	
			}
			
			$baseQuery = "select count(entity_id) from sales_order where created_at > '$enddate'";
			$newS  = $connection->fetchOne($baseQuery." AND status like '%new%' ");
			$proS  = $connection->fetchOne($baseQuery." AND status like '%processing%' ");
			$shipS = $connection->fetchOne($baseQuery." AND status like '%shipped%' ");
			
			$total = $newS + $proS + $shipS;
			
			$agingArr[] = '>25';
			$newArr[]   = $newS;
			$procArr[]  = $proS;
			$shipArr[]  = $shipS;
			$totalArr[] = $total;
			
			
			$agingArr[] = 'Grand Total';
			$newArr[] = array_sum($newArr);
			$procArr[] = array_sum($procArr);
			$shipArr[] = array_sum($shipArr);
			$totalArr[] = array_sum($totalArr);
			
			// Aging by status
			$agingArr_1 = array('AGING');
			$newArr_1   = array('New');
			$procArr_1  = array('Processing');
			$shipArr_1  = array('Shipped');
			$totalArr_1 = array('Grand Total');
			
			for($a=0; $a<26; $a++){
				$agingArr_1[] = $a;
				$startdate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
				$startdate = date('Y-m-d H:i:s',$startdate);
				
				$enddate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
				$enddate = date('Y-m-d H:i:s',$enddate + 86399);
				
				$baseQuery = "select count(entity_id) from sales_order where created_at BETWEEN  '$startdate' AND '$enddate'";
				$newS  = $connection->fetchOne($baseQuery." AND status like '%new%' ");
				$proS  = $connection->fetchOne($baseQuery." AND status like '%processing%' ");
				$shipS = $connection->fetchOne($baseQuery." AND status like '%shipped%' ");
				
				$newS  = $connection->fetchOne("SELECT count(entity_id) from sales_order where (created_at BETWEEN  '$startdate' AND '$enddate') AND status like '%new%' ");
				$proS  = $connection->fetchOne("SELECT count(a.entity_id) FROM `sales_order` as a, `sales_shipment` as b WHERE a.entity_id=b.entity_id AND a.status like '%processing%' AND (b.created_at BETWEEN  '$startdate' AND '$enddate') ");
				$shipS = $connection->fetchOne("SELECT count(entity_id) from sales_order where (pickup_date BETWEEN  '$startdate' AND '$enddate') AND status like '%Shipped%' ");
			
			
				$total = $newS + $proS + $shipS;
				$newArr_1[]   = $newS;
				$procArr_1[]  = $proS;
				$shipArr_1[]  = $shipS;
				$totalArr_1[] = $total;	
			}
			
			$newS  = $connection->fetchOne("SELECT count(entity_id) from sales_order where created_at > '$enddate' AND status like '%new%' ");
			$proS  = $connection->fetchOne("SELECT count(a.entity_id) FROM `sales_order` as a, `sales_shipment` as b WHERE a.entity_id=b.entity_id AND a.status like '%processing%' AND b.created_at > '$enddate' ");
			$shipS = $connection->fetchOne("SELECT count(entity_id) from sales_order where pickup_date > '$enddate' AND status like '%Shipped%' ");
			
			$total = $newS + $proS + $shipS;
			
			$agingArr_1[] = '>25';
			$newArr_1[]   = $newS;
			$procArr_1[]  = $proS;
			$shipArr_1[]  = $shipS;
			$totalArr_1[] = $total;
			
			
			$agingArr_1[] = 'Grand Total';
			$newArr_1[] = array_sum($newArr_1);
			$procArr_1[] = array_sum($procArr_1);
			$shipArr_1[] = array_sum($shipArr_1);
			$totalArr_1[] = array_sum($totalArr_1);
			
			
			$totalD = $connection->fetchOne("select count(entity_id) from sales_order where status like '%delivered%'");
			$totalR = $connection->fetchOne("select count(entity_id) from sales_order where status like '%Refunded%'");
					
			$fullArr = [
				$header1,
				$agingArr,
				$newArr,
				$procArr,
				$shipArr,
				$totalArr,
				[],
				['Total Delivered',$totalD],
				['Total Refunded',$totalR],
				[],
				$header2,
				$agingArr_1,
				$newArr_1,
				$procArr_1,
				$shipArr_1,
				$totalArr_1
				
			];
			
			$fp = fopen('php://memory', 'w');
			$filename = 'Aging Report-'.date('Y-m-d-H-i-s').'.csv';
			foreach($fullArr as $insertArr){
				fputcsv($fp, $insertArr, ",");
			}
			
			//echo  dirname(__FILE__); exit;
			
			fseek($fp, 0);
			header('Content-Type: text/csv');
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header('Content-Disposition: attachment; filename="'.$filename.'";');
			fpassthru($fp);
			exit;    			
		}
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
	}
}

?>
