<?php
namespace OM\CheckCustomerSession\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		$objectManagerlogin = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManagerlogin->create('Magento\Customer\Model\Session');
        $customerName = $customerSession->getCustomer()->getName();
        echo json_encode(["name"=>$customerName]);
	}
}