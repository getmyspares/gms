<?php

namespace OM\CheckCustomerSession\Helper;
 
use \Magento\Framework\App\Helper\AbstractHelper;
 
class Data extends AbstractHelper
{
    
    /**
* @var \Magento\Customer\Model\Session
*/
    protected $_customerSession;
    protected $objectManager;
    protected $groupRepository;
 
    /**
     * @param \Magento\Backend\Block\Template\Context    $context
     * @param array                                      $data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        array $data = []
    ) {
        $this->_customerSession = $session;
        $this->objectManager = $objectmanager;
        $this->_addressFactory = $addressFactory;
        $this->groupRepository = $groupRepository;
        parent::__construct($context);
    }
 
    
    /**
     * Prepare HTML content
     *
     * @return string
     */
    public function isLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }
    
    public function getCustomerId()
    {
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        $customerId = $customerSession->getCustomer()->getId();
        return $customerId;
    }
    public function getCustomerAreaOfInterest()
    {
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        $interest = $customerSession->getCustomer()->getAreaOfInterest();
        return $interest;
    }
    public function getCustomerMobileNumber()
    {
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        $mobile = $customerSession->getCustomer()->getMobile();
        return $mobile;
    }

    public function getCustomerBillingAddress(){
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        $billingAddressId = $customerSession->getCustomer()->getDefaultBilling();
        $billingAddress =$this->_addressFactory->create()->load($billingAddressId);
        return $billingAddress;
    }
    public function getCustomerShippingAddress(){
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        $shippingAddressId = $customerSession->getCustomer()->getDefaultShipping();
        $shippingAddress =$this->_addressFactory->create()->load($shippingAddressId);
        return $shippingAddress;
    }

    public function getCustomerEmail(){
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
         $cusEmail = $customerSession->getCustomer()->getEmail();
         return $cusEmail;
    }
    public function getCustomerName(){
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
         $cusName = $customerSession->getCustomer()->getFirstname().' '.$customerSession->getCustomer()->getLastname();
         return $cusName;
    }

    public function getCustomerGroup(){
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        $currentGroupId = $customerSession->getCustomer()->getGroupId();
        $group = $this->groupRepository->getById($currentGroupId);
        return $group->getCode();

    }

    public function getAreaOfInterest(){
        $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
        return $customerSession->getCustomer()->getAreaOfInterest();
    }
    
    
}