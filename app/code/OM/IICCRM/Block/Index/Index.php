<?php

namespace OM\IICCRM\Block\Index;


class Index extends \Magento\Framework\View\Element\Template {

	protected $productCategoryList;  
	
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Customer\Model\Session $customer,
        \Magento\Catalog\Model\ProductCategoryList $productCategoryList,
        array $data = []
    ) {
        $this->customer = $customer;
        $this->productCategoryList = $productCategoryList;
        parent::__construct($context, $data);

    }

    public function getCategoryIds($productId)
    {
		$categoryIds = $this->productCategoryList->getCategoryIds($productId);
        $category = [];
        if ($categoryIds) {
            $category = array_unique($categoryIds);
        }
        return $category;
    }
    
    public function onlyUniversal($order)
    {
		$orderItems = $order->getAllItems();
		foreach($orderItems as $item){
			$cids =  $this->getCategoryIds($item['product_id']); 
			if(!in_array(7,$cids)){
				return false;
			}		
		}
		return true;
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
}
