<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OM\IICCRM\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

	protected $resultPageFactory;
	protected $jsonHelper;
	protected $_httpClientFactory;
	protected $httpClient;
	protected $_customer;
	protected $_customerSession;
	protected $request;
	protected $_order;
	protected $_orderitem;
	protected $_productRepo;

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\App\Action\Context  $context
	 * @param \Magento\Framework\Json\Helper\Data $jsonHelper
	 */
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Json\Helper\Data $jsonHelper,
		\Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
		\Zend\Http\Client $httpClient,
		\Magento\Customer\Model\Customer $customer,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Catalog\Model\Product $productRepo,
		\Magento\Sales\Model\Order $order,
		\Magento\Sales\Model\Order\Item $orderitem,
		\Psr\Log\LoggerInterface $logger
	) {
		$this->resultPageFactory = $resultPageFactory;
		$this->jsonHelper = $jsonHelper;
		$this->logger = $logger;
		$this->connection = $resourceConnection->getConnection();
		$this->_httpClientFactory = $httpClientFactory;
		$this->_customer = $customer;
		$this->request = $request;
		$this->httpClient = $httpClient;
		$this->_order = $order;
		$this->_orderitem = $orderitem;
		$this->_productRepo = $productRepo;
		$this->_customerSession = $customerSession;
		parent::__construct($context);
	}

	/**
	 * Execute view action
	 *
	 * @return \Magento\Framework\Controller\ResultInterface
	 */
	public function execute()
	{
		try {
			$orderId = $this->request->getParam('orderid');
			$action  = $this->request->getParam('action');

			$logCustomerId   = $this->_customerSession->getCustomerId();
			$logCustomer     = $this->_customer->load($logCustomerId);
			$crmInfo         = $logCustomer->getData();

			$order = $this->_order->load($orderId);
			$crmLoginInfo = $this->loginOnCRM($crmInfo);
			$tknResponce = json_decode($crmLoginInfo, true);
			if ($tknResponce) {
				$token = $tknResponce['Token'];

				if ($action == 'create') {
					$resp = $this->syncWithCrm($orderId, $crmInfo, $token);
					$responce = json_decode($resp, true);
					//print_r($responce);

					if ($responce['HeaderMessage'] == 'success') {
						$sapOrderNo   = $responce['OrderNoFromSAP'];
						$crmOrderNo   = $responce['OrderNo'];
						$crmOrderGuid = $responce['PurchaseOrderGUId'];

						$order->setSapResId($sapOrderNo);
						$order->setCrmResId($crmOrderNo);
						$order->setCrmOrderGuid($crmOrderGuid);
						$order->save();

						/* save crm_res_id and sap_res_id to sales_report also */
						$quewrrsds = "update om_sales_report set crm_res_id='$crmOrderNo',sap_res_id='$sapOrderNo' where order_id='$orderId'";
						$this->connection->query($quewrrsds);

						$succes = false;
						foreach ($responce['OrderLinesResponse'] as $lineItems) {
							$l_status  = $lineItems['LineStatus'];
							$l_msg     = $lineItems['LineMessage'];
							$l_line_id = $lineItems['OrderLineID'];
							$l_guid    = $lineItems['OrderLinesGUID'];

							$orderitem = $this->_orderitem->load($l_line_id);
							$orderitem->setCrmLineStatus($l_status);
							$orderitem->setCrmLineMessage($l_msg);
							$orderitem->setCrmLineGuid($l_guid);
							$orderitem->save();

							if ($l_status == '1' && $l_msg == 'success') {
								$succes = true;
							} else {
								$succes = false;
								break;
							}
						}

						if ($succes == true) {
							$order->setCrmSynced($responce['HeaderMessage']);
							$order->save();
						}
					}
				}

				if ($action == 'fetch') {
					$resp = $this->fetchSapInfo($orderId, $token);
					$responce = json_decode($resp, true);

					//echo "<pre>";
					//print_r($responce);
					//exit;

					if ($responce['Message'] == 'success') {
						foreach ($responce['Data'] as $respp) {
							$sapOrderNo   = $respp['OrderNoFromSAP'];
							$order->setSapResId($sapOrderNo);
							$order->save();
							/* save crm_res_id and sap_res_id to sales_report also */
							$quewrrsds = "update om_sales_report set sap_res_id='$sapOrderNo' where order_id='$orderId'";
							$this->connection->query($quewrrsds);
						}

					}
				}
				//echo "<pre>";
				//print_r($responce);
				//exit;
			};
			return $this->jsonResponse($resp);
		} catch (\Magento\Framework\Exception\LocalizedException $e) {
			return $this->jsonResponse($e->getMessage());
		} catch (\Exception $e) {
			$this->logger->critical($e);
			return $this->jsonResponse($e->getMessage());
		}
	}

	/**
	 * Create json response
	 *
	 * @return \Magento\Framework\Controller\ResultInterface
	 */
	public function jsonResponse($response = '')
	{
		return $this->getResponse()->representJson(
			$this->jsonHelper->jsonEncode($response)
		);
	}

	public function crmApiUrl()
	{
		$host = $_SERVER['HTTP_HOST'];
		if (strpos($host, 'getmyspares.com') !== false) {
			$apiUrl = 'http://10.85.88.110:3902';
		} else {
			$apiUrl = 'http://10.85.88.110:3802';
		}
		return $apiUrl;
	}

	public function loginOnCRM($customerInfo)
	{
		$username = $customerInfo['iic_username'];
		$clientId = $customerInfo['iic_clientid'];

		$apiUrl = $this->crmApiUrl();

		$client = $this->_httpClientFactory->create();
		$client->setUri($apiUrl . '/api/Login');
		$client->setMethod(\Zend_Http_Client::POST);
		$client->setHeaders(\Zend_Http_Client::CONTENT_TYPE, 'application/json');
		$client->setHeaders('Accept', 'application/json');
		$client->setHeaders('cache-control', 'no-cache');
		//$client->setHeaders("Authorization","Bearer $clientId");
		$client->setParameterPost([
			'UserName' => $username,
			'ClientId' => $clientId,
			'GrantType' => 'ClientId'
		]);
		$response = $client->request();
		return $response->getBody();
	}

	public function syncWithCrm($orderId, $crmInfo, $token)
	{

		$order = $this->_order->load($orderId);

		$request = array();
		$request['CustomerCode']    = $crmInfo['iic_customercode'];
		$request['BillSiteID']      = $crmInfo['iic_billsiteid'];
		$request['ShipSiteID']      = $crmInfo['iic_shipsiteid'];
		$request['TransactionType'] = 'Online';
		$request['ASCRemarks']      = 'Test Remark';
		$request['OrderType']       = $crmInfo['iic_ordertype'];
		$request['Company']         = $crmInfo['iic_company'];
		$request['CreditBalance']   = '100';
		$request['CreditLimit']     = '200';
		$request['DORecieptNumber'] = $order->getData('increment_id');
		$request['Warehouse']       = $crmInfo['iic_warehouse'];

		$orderLines = array();

		$postData = $_POST;

		$orderItems = $order->getAllItems();
		foreach ($orderItems as $key => $item) {
			$itemInfo = $item->getData();
			$productID = $item->getProduct()->getId();
			if (array_key_exists($productID, $postData) && $postData[$productID] > 0) {
				$pInfo = $this->_productRepo->load($productID);
				$itemArr = array();
				$itemArr['OrderLineID'] = $item->getItemId();
				$itemArr['PartNo']      = $pInfo->getItemModelNumber(); //'02-MQ556A-T1630';
				$itemArr['Model']       = $pInfo->getPiModel(); //'018F6251';
				$itemArr['Quantity']    = $postData[$productID]; //round($itemInfo['qty_ordered']);
				$itemArr['Amount']      = round($itemInfo['row_total']);
				$itemArr['UnitPrice']   = round($itemInfo['price']);
				$itemArr['PartDescription'] = $itemInfo['name'];
				$itemArr['MWH']         = $crmInfo['iic_warehouse'];
				$itemArr['BWH']         = $crmInfo['iic_warehouse'];
				$itemArr['OrderLineNo'] = $itemInfo['name'];
				$itemArr['QuantityStatus'] = $itemInfo['qty_ordered'];
				$itemArr['OrderDate']   = $itemInfo['created_at'];
				$orderLines[] = $itemArr;
			}
		}
		$request['OrderLines'] = $orderLines;

		//print_r($request);

		$apiUrl = $this->crmApiUrl();
		$client = $this->_httpClientFactory->create();
		$client->setUri($apiUrl . '/api/PurchaseOrder');
		$client->setMethod(\Zend_Http_Client::POST);
		$client->setHeaders(\Zend_Http_Client::CONTENT_TYPE, 'application/json');
		$client->setHeaders('Accept', 'application/json');
		$client->setHeaders('cache-control', 'no-cache');
		$client->setHeaders("Authorization", "Bearer $token");
		$client->setParameterPost($request);
		$response = $client->request();
		return $response->getBody();
	}

	public function fetchSapInfo($orderId, $token)
	{

		$order = $this->_order->load($orderId);
		$guid  = $order->getData('crm_order_guid');
		$request = json_encode(array($guid));

		$apiUrl = $this->crmApiUrl();

		/*$client = $this->_httpClientFactory->create();
		$client->setUri($apiUrl.'/api/PurchaseOrder');
		$client->setHeaders(\Zend_Http_Client::CONTENT_TYPE, 'application/json');
		$client->setHeaders('Accept','application/json');
		$client->setHeaders('cache-control','no-cache');
		$client->setHeaders("Authorization","Bearer $token");
		$client->setParameterGET( $request );
		$response= $client->request();
		return $response->getBody();*/

		$httpHeaders = new \Zend\Http\Headers();
		$httpHeaders->addHeaders([
			'Authorization' => 'Bearer ' . $token,
			'Accept' => 'application/json',
			'Content-Type' => 'application/json'
		]);

		$req = new \Zend\Http\Request();
		$req->setUri($apiUrl . '/api/PurchaseOrder');
		$req->setMethod(\Zend\Http\Request::METHOD_GET);
		$req->setHeaders($httpHeaders);
		$req->setContent($request);

		$client = new \Zend\Http\Client();
		//$client->setRawBody($request);
		$response = $client->send($req);

		//print_r($response->getContent());
		return $response->getContent();
	}
}
