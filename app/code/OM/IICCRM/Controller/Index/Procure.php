<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\IICCRM\Controller\Index;

class Procure extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $_httpClientFactory;
	protected $httpClient;
    protected $_customer;
    protected $_customerSession;
    protected $request;
    protected $_order;
    protected $_orderitem;
    protected $_productRepo;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory ,
		\Zend\Http\Client $httpClient ,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\Product $productRepo,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Item $orderitem,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_customer = $customer;
        $this->request = $request;
		$this->httpClient = $httpClient;
        $this->_order = $order;
        $this->_orderitem = $orderitem;
        $this->_productRepo = $productRepo;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */

    public function execute()
    {
		$orderId    = $this->request->getParam('orderid');
		$order      = $this->_order->load($orderId);		
        $resultPage = $this->resultPageFactory->create();
        $block = $resultPage->getLayout()
                ->createBlock('OM\IICCRM\Block\Index\Index')
                ->setData('order', $order)
                ->setTemplate('OM_IICCRM::procure_index_index.phtml')
                ->toHtml();
 
		echo $block; exit;
        //$result->setData(['output' => $block]);
        //return $result;
    }
}
