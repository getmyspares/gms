<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\IICCRM\Model\Customer\Attribute\Source;

class AreaOfInterest extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
			
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$_resource  =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
			$connection = $_resource->getConnection();
			$interests    = $connection->fetchAll("SELECT name FROM `om_area_interest` ORDER BY `om_area_interest`.`name` ASC");
			
			$retArr = array();
			foreach($interests as $interest){
				$retArr[] = ['value' => $interest['name'], 'label' => $interest['name'] ];
			}
			$this->_options = $retArr;
			
        }
        return $this->_options;
    }
}

