<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\IICCRM\Model\Customer\Attribute\Source;

class State extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
     
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$_resource  =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
			$connection = $_resource->getConnection();
			$regions    = $connection->fetchAll("SELECT region_id,default_name  FROM `directory_country_region` WHERE `country_id` LIKE 'IN'");
			
			foreach($regions as $region){
				$retArr[] = ['value' => $region['region_id'], 'label' => $region['default_name'] ];
			}
		
			$this->_options = $retArr;
			
        }
        return $this->_options;
    }
}

