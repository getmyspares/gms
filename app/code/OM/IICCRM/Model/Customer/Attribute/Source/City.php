<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\IICCRM\Model\Customer\Attribute\Source;

class City extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
			
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$_resource  =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
			$connection = $_resource->getConnection();
			$regions    = $connection->fetchAll("SELECT city FROM `om_city_state` order by city asc");
			
			$retArr = array();
			foreach($regions as $region){
				$retArr[] = ['value' => $region['city'], 'label' => $region['city'] ];
			}
			$this->_options = $retArr;
			
        }
        return $this->_options;
    }
}

