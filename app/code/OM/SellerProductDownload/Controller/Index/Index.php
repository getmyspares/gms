<?php
/* todo need  to  remove object manager  and put product collection factory in constructor @ritesh29nov2021 */
namespace OM\SellerProductDownload\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory
	) {
		$this->_pageFactory = $pageFactory;
		parent::__construct($context);
	}

	public function execute()
	{
		$data = $this->getRequest()->getParams();
		$selllerId = $this->array_key_first($data);
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();


		/* 
		removed for speed optimisation @ritesh23november2021
		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('marketplace_product');
		$sql = "Select * FROM " . $tableName. " WHERE seller_id = ".$selllerId;
		$result = $connection->fetchAll($sql);
		$StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
		*/

		/* removed product load , and stock Inventory  classes  as taking too much time . product collection with joins have been implemented to optimize performance and speed 
			 @ritesh23november2021 
			*/
		$productCollectionFactory = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
		$collection = $productCollectionFactory->create();
		$collection->addAttributeToSelect(['name', 'sku', 'price']);
		$collection->getSelect()->join(
			['second' => 'marketplace_product'],
			'`second`.mageproduct_id = `e`.entity_id',
			['seller_id' => 'seller_id', 'status' => 'status']
		)->where(
			"`second`.seller_id = $selllerId"
		);
		$collection->joinField(
			'qty',
			'cataloginventory_stock_item',
			'qty',
			'product_id=entity_id',
			'{{table}}.stock_id=1',
			'left'
		);

		$productQty = 0;
		if (count($collection) > 0) {
			foreach ($collection as $data) {
				$pStatus = $data['status'];
				if ($pStatus == 1) {
					$productStatus = 'Approved';
				} else if($pStatus == 0) {
					$productStatus = 'Pending Approval';
				} else {
					$productStatus = 'Disapproved';
				}

			/* 
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($data['mageproduct_id']);
			$productQty =  $StockState->getStockQty($data->getData('entity_id'));
			$productQty =  "0";
			*/

				$productData[] = [
					'Product Sku ' => $data->getSku(),
					'Qty' => $data->getQty(),
					'Product Name' => $data->getName(),
					'Product Price' => $data->getPrice(),
					'Product Status' => $productStatus
				];
			}
		
			$fh = fopen('php://temp', 'rw');
			fputcsv($fh, array_keys(current($productData)));
			foreach ($productData as $row) {
				fputcsv($fh, $row);
			}
			rewind($fh);
			$csv = stream_get_contents($fh);
			fclose($fh);
			$filename = 'sellerproduct.csv';
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename="' . $filename . '";');
			echo $csv;
		} else {
			echo 'No products for download';
		}
		die();
	}
	public  function array_key_first(array $array)
	{
		if (count($array)) {
			reset($array);
			return key($array);
		}
		return null;
	}
}
