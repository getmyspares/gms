<?php

namespace OM\PushNotification\Model;

use Magento\Framework\Model\AbstractModel;

class PushNotification extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('OM\PushNotification\Model\ResourceModel\PushNotification');
    }
}