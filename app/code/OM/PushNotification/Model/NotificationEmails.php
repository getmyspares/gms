<?php

namespace OM\PushNotification\Model;

use Magento\Framework\Model\AbstractModel;

class NotificationEmails extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('OM\PushNotification\Model\ResourceModel\NotificationEmails');
    }
}