<?php

namespace OM\PushNotification\Model\ResourceModel\PushNotification;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'pushnotification_id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'OM\PushNotification\Model\PushNotification',
            'OM\PushNotification\Model\ResourceModel\PushNotification'
        );
    }
}