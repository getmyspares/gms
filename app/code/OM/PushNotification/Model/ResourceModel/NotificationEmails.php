<?php

namespace OM\PushNotification\Model\ResourceModel;

class NotificationEmails extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('om_pushnotification_emails', 'id');   //here "om_pushnotification_emails" is table name and "id" is the primary key of custom table
    }
}