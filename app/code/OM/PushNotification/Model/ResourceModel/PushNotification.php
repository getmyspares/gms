<?php

namespace OM\PushNotification\Model\ResourceModel;

class PushNotification extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('om_pushnotification', 'pushnotification_id');   //here "om_pushnotification" is table name and "pushnotification_id" is the primary key of custom table
    }
}