<?php

namespace OM\PushNotification\Model\ResourceModel\NotificationEmails;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'OM\PushNotification\Model\NotificationEmails',
            'OM\PushNotification\Model\ResourceModel\NotificationEmails'
        );
    }
}