<?php

namespace OM\PushNotification\Block\Adminhtml;

class Items extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'items';
        $this->_headerText = __('Push Notifications');
        $this->_addButtonLabel = __('Add New Notifications');
        parent::_construct();
    }
}
