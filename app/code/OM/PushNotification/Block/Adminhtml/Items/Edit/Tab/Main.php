<?php

namespace OM\PushNotification\Block\Adminhtml\Items\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface
{
    protected $_wysiwygConfig;

    protected $_groups;
 
    public function __construct(
        \Magento\Backend\Block\Template\Context $context, 
        \Magento\Framework\Registry $registry, 
        \Magento\Framework\Data\FormFactory $formFactory,  
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Customer\Ui\Component\Listing\Column\Group\Options $groups,
        array $data = []
    ) 
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_groups = $groups;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Notification Information');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Notifications Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_om_pushnotification_items');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('item_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Notifications Information')]);
        if ($model->getId()) {
            $fieldset->addField('pushnotification_id', 'hidden', ['name' => 'pushnotification_id']);
        }
        $fieldset->addField(
            'title',
            'text',
            ['name' => 'title', 'label' => __('Title'), 'title' => __('Title'), 'required' => true]
        );
        $fieldset->addField(
            'customer_group',
            'select',
            [
                'label'    => __('Customer Group'),
                'name'     => 'customer_group',
                'required' => true,
                'values'   => $this->_groups->toOptionArray(),
            ]
        );
        $fieldset->addField(
            'customer_emails',
            'file',
            [
                'name' => 'customer_emails', 
                'label' => __('Customer Emails'), 
                'title' => __('Customer Emails'), 
                'required' => false, 
                'after_element_html' => '<p style="font-weight:600">' . ' Upload in csv format. ' . '</p>'
            ]
        );
        /*$fieldset->addField(
            'emails',
            'text',
            [
                'label'    => __('Specific Customers'),
                'name'     => 'emails',
                'required' => false,
                'after_element_html' => '<p style="font-weight:600">' . ' Enter comma (,) separated customer emails. ' . '</p>'
            ]
        );*/
        $fieldset->addField(
            'status',
            'select',
            ['name' => 'status', 'label' => __('Status'), 'title' => __('Status'),  'options'   => [0 => 'Disable', 1 => 'Enable'], 'required' => true]
        );
        $fieldset->addField(
            'content',
            'editor',
            [
                'name' => 'content',
                'label' => __('Content'),
                'title' => __('Content'),
                'style' => 'height:15em;',
                'required' => true,
                'config'    => $this->_wysiwygConfig->getConfig(),
                'wysiwyg' => true
            ]
        );
        $fieldset->addField(
            'expired_at',
            'date',
            [
                'name' => 'expired_at',
                'label' => __('End Date'),
                'title' => __('End Date'),
                'date_format' => 'dd-MM-yyyy',
                //'time_format' => 'hh:mm:ss',
                'required' => false            
            ]
        );
        
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
