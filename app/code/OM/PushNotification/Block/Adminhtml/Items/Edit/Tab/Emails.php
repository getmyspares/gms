<?php

namespace OM\PushNotification\Block\Adminhtml\Items\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Emails extends \Magento\Backend\Block\Widget\Grid\Extended implements TabInterface
{
    protected $coreRegistry = null;
    
    protected $collectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
       	\OM\PushNotification\Model\NotificationEmailsFactory $collectionFactory,
        \Magento\Framework\Registry $registry,

        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->coreRegistry = $registry;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Return Tab label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Customer Emails');
    }

    /**
     * Return Tab title
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Customer Emails');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('pushEmailsGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');

        $this->setUseAjax(true);

        $this->setEmptyText(__('No Emails Found'));
    }

    /**
     * @Inheritdoc
     */
    /*public function getRowUrl($item)
    {
        return $this->getUrl('amasty_rma/request/edit', ['id' => $item->getId()]);
    }*/

    /**
     * @Inheritdoc
     */
    // public function getGridUrl()
    // {
    //     return $this->getUrl('om_pushnotification/*/index', ['_current' => true]);
    // }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
    	$notificationId = $this->getRequest()->getParam('id');
        $collection = $this->collectionFactory->create()->getCollection()
            ->addFieldToFilter(
                'pushnotification_id', $notificationId
            );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'align' => 'left',
                'index' => 'id',
                'filter'    => false
            ]
        );

        $this->addColumn(
            'email',
            [
                'header' => __('Customer Email'),
                'align' => 'left',
                'index' => 'email',
                'filter'    => false
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->canShowTab()) {
            return parent::_toHtml();
        } else {
            return '';
        }
    }
}
