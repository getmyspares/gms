<?php

namespace OM\PushNotification\Block;

/**
 * PushNotification content block
 */
class PushNotification extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        //$this->pageConfig->getTitle()->set(__('OM PushNotification Module'));
        
        return parent::_prepareLayout();
    }
}
