<?php

namespace OM\PushNotification\Controller\Adminhtml\Items;

class Index extends \OM\PushNotification\Controller\Adminhtml\Items
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('OM_PushNotification::test');
        $resultPage->getConfig()->getTitle()->prepend(__('Push Notifications'));
        $resultPage->addBreadcrumb(__('Push'), __('Push'));
        $resultPage->addBreadcrumb(__('Notifications'), __('Notifications'));
        return $resultPage;
    }
}