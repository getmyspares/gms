<?php

namespace OM\PushNotification\Controller\Adminhtml\Items;

class NewAction extends \OM\PushNotification\Controller\Adminhtml\Items
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
