<?php

namespace OM\PushNotification\Controller\Adminhtml\Items;

class Save extends \OM\PushNotification\Controller\Adminhtml\Items
{
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            try {
                $model = $this->_objectManager->create('OM\PushNotification\Model\PushNotification');
                $data = $this->getRequest()->getPostValue();
                $isFileSet = false;
                if(isset($_FILES['customer_emails']['name']) && $_FILES['customer_emails']['name'] != '') {
                    $isFileSet = true;
                    try{
                        $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'customer_emails']);
                        $imageAdapter = $this->adapterFactory->create();
                        $uploaderFactory->setAllowRenameFiles(true);
                        $mediaDirectory = $this->filesystem->getDirectoryRead($this->directoryList::MEDIA);
                        $destinationPath = $mediaDirectory->getAbsolutePath('om/pushnotification');
                        $result = $uploaderFactory->save($destinationPath);
                        if (!$result) {
                            throw new LocalizedException(
                                __('File cannot be saved to path: $1', $destinationPath)
                            );
                        }
                        
                        $imagePath = 'om/pushnotification/'.$result['file'];
                        $data['customer_emails'] = $imagePath;
                    } catch (\Exception $e) {
                        $this->messageManager->addError(
                            __('Something went wrong while saving the notification data.')
                        );
                    }
                }
                
                $inputFilter = new \Zend_Filter_Input(
                    [],
                    [],
                    $data
                );
                $data = $inputFilter->getUnescaped();
                $id = $this->getRequest()->getParam('id');
                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        throw new \Magento\Framework\Exception\LocalizedException(__('The wrong notification is specified.'));
                    }
                }

                $connection = $this->_objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection();

                if(array_key_exists('pushnotification_id', $data)) {
                    $pushnotificationId = $data['pushnotification_id'];
                    $query = "SELECT id FROM `om_pushnotification_emails` WHERE `pushnotification_id` = $pushnotificationId";
                    $notifEmailId = $connection->fetchOne($query);
                    if($notifEmailId) {
                        $data['customer_group'] = null;
                    }
                }
                if($isFileSet) {
                    $data['customer_group'] = null;
                }
                
                $model->setData($data);
                $session = $this->_objectManager->get('Magento\Backend\Model\Session');
                $session->setPageData($model->getData());
                $model->save();

                /* save emails from csv file- starts */
                if($isFileSet) {
                    $notificationId = $model->getId();
                    $connection->query("DELETE FROM `om_pushnotification_emails` WHERE `pushnotification_id` = $notificationId");

                    $mediaDirectory = $this->filesystem->getDirectoryRead($this->directoryList::MEDIA)->getAbsolutePath();
                    $filePath = $mediaDirectory . $data['customer_emails'];
                    $file = fopen($filePath, "r");
                    while(! feof($file)){
                        $fData   = fgetcsv($file);
                        $email = $fData[0];
                        $connection->query("INSERT INTO `om_pushnotification_emails` (`pushnotification_id`, `email`) VALUES ($notificationId, '$email')");
                    }
                    fclose($file);
                    if ($this->_file->isExists($filePath))  {
                        $this->_file->deleteFile($filePath);
                    }
                }
                /* save emails from csv file- ends */

                $this->messageManager->addSuccess(__('You saved the notification.'));
                $session->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('om_pushnotification/*/edit', ['id' => $model->getId()]);
                    return;
                }
                $this->_redirect('om_pushnotification/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('id');
                if (!empty($id)) {
                    $this->_redirect('om_pushnotification/*/edit', ['id' => $id]);
                } else {
                    $this->_redirect('om_pushnotification/*/new');
                }
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong while saving the notification data. Please review the error log.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($data);
                $this->_redirect('om_pushnotification/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->_redirect('om_pushnotification/*/');
    }
}
