<?php
namespace OM\PushNotification\Controller\Index;

use Magento\Framework\App\Action\Context;
use OM\PushNotification\Model\PushNotificationFactory;

class Save extends \Magento\Framework\App\Action\Action
{
	/**
     * @var PushNotification
     */
    protected $_pushnotification;

    public function __construct(
		Context $context,
        PushNotificationFactory $pushnotification
    ) {
        $this->_pushnotification = $pushnotification;
        parent::__construct($context);
    }
	public function execute()
    {
        $data = $this->getRequest()->getParams();
    	$pushnotification = $this->_pushnotification->create();
        $pushnotification->setData($data);
        if($pushnotification->save()){
            $this->messageManager->addSuccessMessage(__('You saved the data.'));
        }else{
            $this->messageManager->addErrorMessage(__('Data was not saved.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('pushnotification/index/index');
        return $resultRedirect;
    }
}
