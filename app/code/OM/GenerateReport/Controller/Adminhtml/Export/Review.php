<?php

namespace OM\GenerateReport\Controller\Adminhtml\Export;

use Magento\Framework\App\Filesystem\DirectoryList;


class Review extends \Magento\Backend\App\Action
{
    protected $uploaderFactory;

    protected $_locationFactory; 

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        \OM\GenerateReport\Helper\EmailReport $emailreportHelper, 
        \Magento\Review\Model\Review $locationFactory // This is returns Collaction of Data
    ) {

       parent::__construct($context);
       $this->_fileFactory = $fileFactory;
       $this->_locationFactory = $locationFactory;
       $this->emailreportHelper = $emailreportHelper;
       $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR); // VAR Directory Path
       parent::__construct($context);
    }

    public function execute()
    {   
        $name = date('m-d-Y-H-i-s');
        $filepath = 'export/export-data-' .$name. '.csv'; // at Directory path Create a Folder Export and FIle
        $this->directory->create('export');
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        //column name dispay in your CSV 
        $columns = $this->emailreportHelper->reviewColumns();
        foreach ($columns as $column) 
        {
            $header[] = $column;
        }
        $stream->writeCsv($header);
        $location = $this->_locationFactory;
        $location_collection = $location->getCollection(); // get Collection of Table data 
        foreach($location_collection as $item){
            $itemData = $this->emailreportHelper->generateReviewReportRow($item);
            $stream->writeCsv($itemData);
        }
        $content = [];
        $content['type'] = 'filename';
        $content['value'] = $filepath;
        $content['rm'] = '1';
        $csvfilename = 'review-import-'.$name.'.csv';
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }



}