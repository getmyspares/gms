<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\GenerateReport\Controller\Report;

class Settlement extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \OM\GenerateReport\Helper\SettlementHelper $settlementHelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_settlementHelper = $settlementHelper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // $this->_settlementHelper->generateSettlementReport();     
        /* temporary to  check partial return reports */
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $settlementHelperPartial = $objectManager->create('OM\GenerateReport\Helper\SettlementHelperPartial');
        $settlementHelperPartial->generateSettlementReport();
    }
}

