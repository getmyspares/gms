<?php

namespace OM\GenerateReport\Plugin;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;

class SalesReportPlugin{

    public function __construct(
        \Magento\Framework\App\ObjectManager $objectManager,
        \Magento\Framework\App\ResourceConnection $connection,
        \Panasonic\CustomUser\Helper\Common $common_helpers
    )
    {
        $this->objectManager = $objectManager;
        $this->common_helperps = $common_helpers;
        $this->connection = $connection->getConnection();        
    }
 
    public function afterPlace(
        OrderManagementInterface $subject,
        OrderInterface $result
    ) {
        $order_inc_id = $result->getIncrementId();
        $order_id = $result->getEntityId();
        if($order_inc_id)
        {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
            foreach ($order->getAllVisibleItems() as $_orderItem) {
                $product_id = $_orderItem->getProductId();
                $product_qty = $_orderItem->getQtyOrdered();
                $item_id = $_orderItem->getItemId();
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
                $product_hsn = $product->getHsn();
                $product_gst_rate = $product->getGstRate();
                $row_total_without_tax = $_orderItem->getRowTotal() - ($_orderItem->getData('cgst_amount')+$_orderItem->getData('sgst_amount')+$_orderItem->getData('igst_amount')+$_orderItem->getData('cgst_amount')+$_orderItem->getData('discount_amount')); 
                $categorydate = $this->getParentandBPCategory($product->getCategoryIds());
                $bp_category=$categorydate['bp_category'];
                $product_category=$categorydate['product_category'];
                $product_comission_category = $this->getComissionCategory($product_id);
                $product_comission_percentage = $this->common_helpers->get_single_product_logic($product_id);
                $shippingDetails = $this->shippingDetails($order_id,$order_inc_id,$product_id,$product_qty,$row_total_without_tax,$product_comission_percentage);
                $shipping_gst_rate = $shippingDetails['shipping_gst_rate'];
                $commision = $shippingDetails['commision'];
                $shipping_invoice_total = $shippingDetails['shipping_invoice_total'];
                $shipping_gst = $shippingDetails['shipping_gst'];
                $shipping_base = $shippingDetails['shipping_base'];
                
                $query = "update sales_order_item set 
                bp_category='$bp_category',
                product_category = '$product_category',
                hsn_code = '$product_hsn',
                product_gst_rate = '$product_gst_rate',
                product_comission_category = '$product_comission_category',
                product_comission_percentage = '$product_comission_percentage',
                shipping_gst_rate = '$shipping_gst_rate',
                commision = '$commision',
                shipping_invoice_total =  '$shipping_invoice_total',
                shipping_gst = '$shipping_gst',
                shipping = '$shipping_base'
                where `item_id`=$item_id;
                ";
                $this->connection->query($query);
            }
        }  
        return $result;
    }

    public function getComissionCategory($product_id){
        $product_comission_category="";
        $option_id_query = "Select `value` FROM catalog_product_entity_int where entity_id='".$product_id."' and attribute_id='261'";
        $option_id = $this->connection->fetchOne($option_id_query);  
        if(!empty($option_id))
        {	
            $part_value_query = "SELECT `value` FROM `eav_attribute_option_value` where option_id=$option_id";
            $product_comission_category = $this->connection->fetchOne($part_value_query);
        }
        return $product_comission_category;
    }

    public function getParentandBPCategory($categories){
        $selected_cat_name="";
        $parent_cat_name="";
        if(!empty($categories))
        {	
            foreach($categories as $category_id)
            {
                $cat_name=$this->get_category_name($category_id);
                $selected_cat_name .=$cat_name.',';
                $cat = $this->objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
                $parentCategories = $cat->getParentCategories();
                if(!empty($parentCategories))
                {
                    foreach($parentCategories as $parent)
                    {
                        $parentArray=$parent->getData();
                        if($parentArray['is_active']==1)
                        {	
                            if($parentArray['entity_id']!=$category_id) {		
                                $parent_cat_name .=	$parentArray['name'].',';		
                            } 
                        }	
                    }	 
                }									
            }
            $selected_cat_name=substr($selected_cat_name,0,-1);
            $parent_cat_name=substr($parent_cat_name,0,-1);
            $parent_cat_name=str_replace("Default Category,","",$parent_cat_name);
        }
        return array("product_category"=>$selected_cat_name,"bp_category"=>$parent_cat_name);
    }

    public function get_category_name($cat_id)
    {   
        $select = $this->connection->select()
                    ->from('catalog_category_entity_varchar') 
                    ->where('entity_id = ?', $cat_id)
                    ->where('attribute_id = ?', '45'); 
            
        $result = $this->connection->fetchAll($select);
        if(!empty($result))
        {
            return $result[0]['value'];	
        }
    }
}





?>