<?php 

namespace OM\GenerateReport\Model;

class EcomTempOrder extends \Magento\Framework\Model\AbstractModel
{
	public function _construct()
	{
		$this->_init('OM\GenerateReport\Model\ResourceModel\EcomTempOrder');
	}
	
}