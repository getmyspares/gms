<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\GenerateReport\Model\Config\Source;

class Time implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '12am', 'label' => __('12am')],['value' => '1am', 'label' => __('1am')],['value' => '3am', 'label' => __('3am')],['value' => '5am', 'label' => __('5am')],['value' => '8am', 'label' => __('8am')],['value' => '9am', 'label' => __('9am')],['value' => '7pm', 'label' => __('7pm')],['value' => '9pm', 'label' => __('9pm')],['value' => '10pm', 'label' => __('10pm')],['value' => '11pm', 'label' => __('11pm')]];
    }

    public function toArray()
    {
        return ['12am' => __('12am'),'1am' => __('1am'),'3am' => __('3am'),'5am' => __('5am'),'8am' => __('8am'),'9am' => __('9am'),'7pm' => __('7pm'),'9pm' => __('9pm'),'10pm' => __('10pm'),'11pm' => __('11pm')];
    }
}

