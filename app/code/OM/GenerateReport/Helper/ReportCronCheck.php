<?php
    namespace OM\GenerateReport\Helper;
    use Magento\Framework\App\Helper\AbstractHelper;
    use Panasonic\CustomUser\Helper\Data;
    use Magento\Framework\App\Config\ScopeConfigInterface;

    class ReportCronCheck extends AbstractHelper
    {
        const XML_SETTLEMENT_REPORT = "automated_report_sharing/error_report_generation/settlement_email";
        const XML_SALES_REPORT      = "automated_report_sharing/error_report_generation/sales_email";
        const XML_GSTTCS_REPORT     = "automated_report_sharing/error_report_generation/gsttcs_email";

        public function __construct(
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            ScopeConfigInterface $scopeConfig,
            Data $sendEmail
        ) {
            $this->resourceConnection = $resourceConnection;
            $this->sendEmail = $sendEmail;
            $this->scopeConfig = $scopeConfig;
        }

        public function checkSettelmentReprtEntries()
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $to = $this->scopeConfig->getValue(self::XML_SETTLEMENT_REPORT, $storeScope);
            $from = $this->sendEmail->getFromEmail();

            $yeserdayDate = date('Y-m-d',strtotime("-1 days"));
            $connection = $this->resourceConnection->getConnection();
            $table = $connection->getTableName('om_settlement_report_partial');
            $query = "Select created_at FROM $table WHERE date(created_at) = $yeserdayDate";
            $result = $connection->fetchAll($query);
            if(count($result)<1){
                $msg = "Settlement Report Not Generated on ".$yeserdayDate.". Please Check!";
                $subject = "Settlement Report Not Generated";
                $this->sendEmail->sendEmailThroughCurl($to,$from,$msg,$subject);
            }
        }

        public function checkSalesReportEntries()
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $to = $this->scopeConfig->getValue(self::XML_SALES_REPORT, $storeScope);
            $from = $this->sendEmail->getFromEmail();

            $yeserdayDate = date('Y-m-d',strtotime("-1 days"));
            $connection = $this->resourceConnection->getConnection();
            $table = $connection->getTableName('om_sales_report');
            $query = "Select report_createdd_at FROM $table WHERE date(report_createdd_at) = $yeserdayDate";
            $result = $connection->fetchAll($query);
            if(count($result)<1){
                $msg = "Sales Report Not Generated on $yeserdayDate. Please Check!";
                $subject = "Sales Report Not Generated";
                $this->sendEmail->sendEmailThroughCurl($to,$from,$msg,$subject);
            }
        }

        public function checkGstReportEntries()
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $to = $this->scopeConfig->getValue(self::XML_SETTLEMENT_REPORT, $storeScope);
            $from = $this->sendEmail->getFromEmail();

            $yeserdayDate = date('Y-m-d',strtotime("-1 days"));
            $connection = $this->resourceConnection->getConnection();
            $table = $connection->getTableName('om_gsttcs_report');
            $query = "Select created_at FROM $table WHERE date(created_at) = $yeserdayDate";
            $result = $connection->fetchAll($query);
            if(count($result)<1){
                $msg = "GstTcs Report Not Generated on $yeserdayDate. Please Check!";
                $subject = "GstTcs Report Not Generated";
                $this->sendEmail->sendEmailThroughCurl($to,$from,$msg,$subject);
            }            
        }

    }
?>