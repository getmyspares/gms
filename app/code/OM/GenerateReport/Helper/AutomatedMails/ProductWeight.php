<?php
declare(strict_types=1);

namespace OM\GenerateReport\Helper\AutomatedMails;
use Magento\Framework\App\Helper\AbstractHelper;
class ProductWeight extends AbstractHelper
{
	public function __construct(
		\Magento\Framework\App\ResourceConnection $resource,
		\Panasonic\CustomUser\Helper\Data $panasonicHelper,
		\Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $seller,
		\OM\GenerateReport\Helper\Mail $mail,
		\Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
		$this->_objectManager = $objectmanager;
		$this->panasonicHelper=$panasonicHelper;
		$this->sellerCollectionFactory = $seller;
		$this->mail = $mail;
		$this->connection = $resource->getConnection();
	}

	public function checkWeightNotFoundEmail()  
	{
		$seller_ids = $this->getAllActiveSellersId();
		foreach ($seller_ids as $seller_id) {
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
			$_resource  =  $objectManager->create('Magento\Eav\Model\Entity\Attribute');
			$weight_attribute_id = $_resource->load('weight', 'attribute_code')->getAttributeId();
			$datas = $this->getSellersProductData($seller_id);
			$sellerData = $this->getSellerData($seller_id);
			$selller_email = $sellerData['email'];
			if(!$datas)
			{
				continue;
			}
			$columns = $this->getProductWeightColumns();
			$csvHeader = array_values($columns);
			$filename = 'Product Weight Not Found Report-'.date('Y-m-d-H-i-s').'.csv';
			$final_text_data="";
			$fp = fopen('php://memory', 'w');
			fputcsv( $fp, $csvHeader,",");
			foreach ($datas as $data) {
				$insertArr = $this->generateProductWeightRow($data,$seller_id);
				fputcsv($fp, $insertArr, ",");
			}
			fseek($fp, 0);
			$file = stream_get_contents($fp);
			fclose($fp);

			$to=$this->productWeightEmail();
      $from = $this->panasonicHelper->getFromEmail(); 
			// $to[]=$selller_email;
			$subject="Product Weight Not Filled";
			$msg="Please Fill Weight in the attached products";
			$this->mail->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
		}
	}

	public function getProductWeightColumns()
	{
		return ["Product Id","Seller Email","Product Name","Sku","Weight"];
	}

  public function productWeightEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/product_weight/email'));
	}

  public function getSellerEmail()  
	{
	}

	public function generateProductWeightRow($data,$seller_id)  
	{
		$product_id = $data;
		$sellerData = $this->getSellerData($seller_id);
		$selller_email = $sellerData['email'];

		$productData = $this->getProductData($product_id);
		$sku = $productData['sku'];
		$product_name = $productData['name'];
		$weight = $productData['weight'];

		$insertArr[] = $product_id; 
		$insertArr[] = $selller_email; 
		$insertArr[] = $product_name; 
		$insertArr[] = $sku; 
		$insertArr[] = $weight; 
		return $insertArr;
	}

	public function getSellerData($entity_id)
	{
		$sellerData=[];
		$query = "SELECT email FROM `customer_entity` where entity_id = '$entity_id'";
		$sellerEmail = $this->connection->fetchOne($query);
		$sellerData['email'] = $sellerEmail; 
		return $sellerData;
	}

	public function getProductData($entity_id)
	{
		$sellerData=[];

		$query = "SELECT sku FROM `catalog_product_entity` where entity_id = '$entity_id'";
		$sku = $this->connection->fetchOne($query);
		
		$query = "SELECT *  FROM `eav_attribute` WHERE `attribute_code`='name' and entity_type_id='4'";
		$name_attribute_id = $this->connection->fetchOne($query);
	
		$query = "SELECT *  FROM `eav_attribute` WHERE `attribute_code`='weight' and entity_type_id='4'";
		$weight_attribute_id = $this->connection->fetchOne($query);

		$query = "SELECT `value` FROM `catalog_product_entity_varchar` where attribute_id = '$name_attribute_id' and store_id='0' and entity_id='$entity_id' ";
		$name = $this->connection->fetchOne($query);
		

		$query = "SELECT `value` FROM `catalog_product_entity_decimal` where attribute_id = '$weight_attribute_id' and store_id='0' and entity_id='$entity_id' ";
		$weight = $this->connection->fetchOne($query);
		
		$sellerData['sku'] = $sku; 
		$sellerData['name'] = $name; 
		$sellerData['weight'] = $weight; 
		
		return $sellerData;
	}

	public function getSellersProductData($seller_id)
	{
		$data = [];

		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$_resource  =  $objectManager->create('Magento\Eav\Model\Entity\Attribute');
		$weight_attribute_id = $_resource->load('weight', 'attribute_code')->getAttributeId();

		/* using direct query as this is resource intensive operation , direct query will means less load on server @ritesh21oct021*/
		$query = "SELECT * FROM `marketplace_product` as a left JOIN catalog_product_entity_decimal as b on a.mageproduct_id=b.entity_id  where a.status=1 and b.attribute_id='$weight_attribute_id' and b.store_id=0 and b.value=0 and a.seller_id='$seller_id'";
		$weightless_product_collection = $this->connection->fetchAll($query);		

		foreach($weightless_product_collection as $weightless_product)
		{
			$product_id = $weightless_product['mageproduct_id'];
			$salabilityChecker  =  $objectManager->create('\Magento\Catalog\Model\Product\SalabilityChecker');
			$is_salable = $salabilityChecker->isSalable($product_id);
			if($is_salable)
			{
				$data[] = $product_id;
			}
		}
		return $data;
	}

  public function getAllActiveSellersId()
	{
		$seller_id = [];
		$sellerdata = $this->sellerCollectionFactory->create();
		$sellerdata->addFieldToSelect(['seller_id', 'is_seller']);
		$sellerdata->addFieldToFilter('is_seller', ['eq' => '1']);

		foreach($sellerdata as $seller)
		{
			$seller_id[] = $seller->getSellerId();
		}

		return $seller_id;
	}

}