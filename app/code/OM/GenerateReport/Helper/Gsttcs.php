<?php
declare(strict_types=1);

namespace OM\GenerateReport\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
class Gsttcs extends AbstractHelper
{
	public function __construct(
		\Magento\Framework\App\ResourceConnection $resource,
		\Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
		$this->_resource = $resource;
		$this->_objectManager = $objectmanager;
		$this->connection = $resource->getConnection();
	}

	public function generateGstTcsReport()  
	{
		$movement_type = "forward";
		$select   = "SELECT * FROM `sales_order` WHERE increment_id NOT IN (SELECT distinct order_inc_id from om_gsttcs_report where movement_type='forward') and state not in ('closed','pending_payment','canceled')";

		// $select   = "SELECT * FROM `sales_order` WHERE increment_id IN ('000006965')";

		$connection = $this->_resource->getConnection();
		$results = $connection->fetchAll($select);
		$today=strtotime(date('Y-m-d'));
		foreach($results as $result){
			$incremment_id = $result['increment_id'];
			$order_id = $result['entity_id'];

			$queryasda =  "select transaction_completion_date from om_settlement_report_partial where order_num='$incremment_id'";
			$transaction_completion_date = $connection->fetchOne($queryasda);
			if(empty($transaction_completion_date))
			{
				continue;
			}
			$transaction_completion_date_str = strtotime($transaction_completion_date);

			$wk_rma_query = "SELECT * FROM `wk_rma` where increment_id = '$incremment_id' and final_status in ('1','0') and settled_without_return='0'";
			$wk_rma = $this->connection->fetchAll($wk_rma_query);
			
			$sales_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
			$array=$sales_helpers->getOrderTrackingOM($order_id,"normal");
			$delivery_date=$array['delivered_date'];
			$bypassedshippingdeldate = $this->checkifshippingbypassed($order_id);
			if($bypassedshippingdeldate)
			{
				$delivery_date = $bypassedshippingdeldate;
			};

			if(empty($wk_rma) && !empty($delivery_date) && !empty($transaction_completion_date) && $transaction_completion_date_str<$today)
			{
				$this->generateGstAndShippingReportRow($result['increment_id'],$movement_type,$transaction_completion_date);
			}   
		}

		/* for  backward entry */
		// $movement_type = "backward";
		// $select="SELECT * FROM `sales_creditmemo` where order_id not  in (SELECT order_id from om_settlement_report_partial where report_type='refund')";
		// $results = $this->connection->fetchAll($select);
		// foreach($results as $result){
		// 	$order_id = $result['order_id'];
		// 	$query="select increment_id  from sales_order  where  entity_id='$order_id'";
		// 	$incremment_id = $this->connection->fetchOne($query);
		// 	if($transaction_completion_date>$today)
		// 	{
		// 		$this->generateGstAndShippingReportRow($result['increment_id'],$movement_type,$transaction_completion_date);
		// 	}
		// }
	}

	public function generateGstAndShippingReportRow($order_inc_id,$movement_type,$transaction_completion_date)    
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		
		$this->insert_shipping_report($order_inc_id,$movement_type,$transaction_completion_date); 
		
		foreach ($order->getAllItems() as $item)
		{
			$item_id=$item->getId();
			$product_id=$item->getProductId();
			$product_qty=$item->getQtyOrdered();
			$isrefunded =  $this->checkIfItemRefunded($product_id,$item_id);
			if($movement_type=='backward' && $isrefunded) {
				
					$this->insert_sales_report($order_inc_id,$product_id,$item_id,$product_qty,$movement_type,$transaction_completion_date);
			}
			if($movement_type=='forward' && !$isrefunded) {
					
					$this->insert_sales_report($order_inc_id,$product_id,$item_id,$product_qty,$movement_type,$transaction_completion_date);
			}
		}      
	}
	public function checkifshippingbypassed($order_id) 
	{
		$coupon =$this->_objectManager->create('Magento\SalesRule\Model\Coupon');
		$saleRule = $this->_objectManager->create('Magento\SalesRule\Model\Rule');
		$order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($order_id);
		$couponCode = $order->getCouponCode();
		$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
		$rule = $saleRule->load($ruleId);
		$freeShippingCoupon = $rule->getSimpleFreeShipping();
		if($freeShippingCoupon){
			$sql = "Select * FROM sales_shipment where order_id='$order_id'";  
			$results = $this->connection->fetchAll($sql);
			$status = !empty($results) ? $results[0]['created_at']:0; 
			return $status;
		}
	}	
	public function checkIfItemRefunded($product_id,$item_id)
	{	/* code  cleaned @ritesh */
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$select = "select entity_id from sales_creditmemo_item where product_id=$product_id and order_item_id=$item_id";
		$result = $connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0]['entity_id'];
		return $status;
	}

	public function insert_shipping_report($order_inc_id,$movement_type,$transaction_completion_date) 
	{  
		$type='shipping';
		$query = "select id from `om_gsttcs_report` where order_inc_id='$order_inc_id' and movement_type='$movement_type' and type='$type'";
		$alreadyExist = $this->connection->fetchOne($query);
		if($alreadyExist)
		{
			return false;
		}

		/*code cleaned    @ritesh*/
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		$shippingAddressObj = $order->getShippingAddress();
		$shippingAddressArray = $shippingAddressObj->getData(); 
		$customer_shipping_state = $shippingAddressArray['region'];
		$customerID = $orderArray['customer_id'];
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerID);
		$customer_gstin = $customerObj->getData('gst_number');
		
		$items = $order->getAllVisibleItems();
		$product_qty=1;
		
		$buyer_id=$orderArray['customer_id']; 
		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$grand_total=$orderDetailArray->grand_total;
		$total_discount=$orderDetailArray->total_discount;
		$total_gst=$orderDetailArray->total_gst;
		
		
		$shipping_base=$orderDetailArray->shipping_base;
		
		$shipping_gst=$orderDetailArray->shipping_gst;
		
		$shipping_expense=$orderDetailArray->shipping_expense;
		
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		
		$payment_type='COD';	 
		
		if($payment_id!='COD') 
		{
			$payment_type='Online';	 
		}	
		
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		//print_r($seller_details);
		
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		

		$seller_state=$seller_details->seller_state;
		$seller_gst=$seller_details->seller_gst;
		$pana_state=0;
		$pana_gst=0; 
		$supply_type='Intra-State';
		$seller_state=strtolower($seller_state);
		if($seller_state=="new delhi") { 
			$seller_state='Delhi';	
		}  	
		
			
		if(strtolower($seller_state)==strtolower($customer_shipping_state))
		{
			$supply_type='Intra-State';	
		} 
		/* elseif( $customer_shipping_pincode==$seller_zipcode){
			loophole  exist in the current setup , condition to check loophole ,need  to  fix @ritesh
			$supply_type='Intra-State';
		} */ else  {
			$supply_type='Inter-State';	
		}

		$sql_pana="select * from gst_tcs_panasonic where state_code='".$seller_state."'";
		$results_pana = $connection->fetchAll($sql_pana);    
		if(!empty($results_pana))
		{	
			$pana_state=$results_pana[0]['state_code'];
			$pana_gst=$results_pana[0]['gst']; 
		}
		
		$document_date=date('Y-m-d',strtotime($order_cr_date));
		$posting_date=date("Y-m-t", strtotime($order_cr_date));
		
		$product_name='Shipping & Handling';
		$product_hsn='996812';
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$igst_amount_int=round($igst_amount);
		$cgst_amount_int=round($cgst_amount);
		$sgst_amount_int=round($sgst_amount);
		$utgst_amount_int=round($ugst_amount);

		if($igst_amount<1 && $igst_amount>0)
		{
			$igst_amount_int=1;
		}		
		if($cgst_amount<1 && $cgst_amount>0)
		{
			$cgst_amount_int=1;
		}
		if($sgst_amount<1 && $sgst_amount>0)
		{
			$sgst_amount_int=1;
		}
		if($ugst_amount<1 && $ugst_amount>0)
		{
			$utgst_amount_int=1;
		}
		
		$igst_rate=0;
		$cgst_rate=0;
		$sgst_rate=0;
		$utgst_rate=0;
		
		$igst_gst_amount=0;
		$cgst_gst_amount=0;
		$sgst_gst_amount=0;
		$utgst_gst_amount=0;
		
		$igst_gst_amount_tcs=0;
		$cgst_gst_amount_tcs=0;
		$sgst_gst_amount_tcs=0;
		$utgst_gst_amount_tcs=0;
		$gst_tcs_rate=$orderDetailArray->gst_tcs_rate; 
		$value_supplier_return=0;
		$shipping_base=$orderDetailArray->shipping_base;
		$shipping_expense=$orderDetailArray->shipping_expense;
		$gross_price=$shipping_base;
		$net_amount_lible_tcs=$gross_price-$value_supplier_return;
		
		if($igst_amount_int!=0) {
			$igst_rate = $gst_tcs_rate;
			$igst_gst_amount = $total_gst;
			$igst_gst_amount_tcs = ($net_amount_lible_tcs*$gst_tcs_rate)/100;
		}	
		
		if($cgst_amount_int!=0 && $sgst_amount_int!=0) {
			$cgst_rate=$gst_tcs_rate/2;
			$sgst_rate=$gst_tcs_rate/2;
			$cgst_gst_amount=$total_gst/2;
			$sgst_gst_amount=$total_gst/2;
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$sgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}	
		
		if($cgst_amount_int!=0 && $utgst_amount_int!=0) {
			$cgst_rate=$gst_tcs_rate/2;
			$utgst_rate=$gst_tcs_rate/2;
			$cgst_gst_amount=$total_gst/2;
			$utgst_gst_amount=$total_gst/2;
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$utgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}

		if($supply_type=='Inter-State') {
			$total_gst_tcs=$igst_gst_amount_tcs;
		} else {
			$total_gst_tcs= $cgst_gst_amount_tcs+$sgst_gst_amount_tcs+$utgst_gst_amount_tcs;
		}

		$transection_type='Regular Supply';
		$document_type="Tax Invoice";
		$orignal_invoice_date='';
		$orignal_invoice='';
		if($movement_type=='refund')
		{
			$transection_type="Sales returns / Deficiency in service (Credit Note)";
			$creditmemo_details = $this->getCreditMemo($order_id);
			$document_type = "Credit Note";
			$orignal_invoice_date = $document_date;
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			$orignal_invoice = $invoice_id;
			$invoice_id = $credit_memo_increment_id;
			$document_date = $credit_memo_created_date;
			$net_amount_lible_tcs= - $net_amount_lible_tcs;
			$igst_gst_amount= -$igst_gst_amount;
			$cgst_gst_amount= -$cgst_gst_amount;
			$sgst_gst_amount= -$sgst_gst_amount;
			$utgst_gst_amount= -$utgst_gst_amount;
			$total_gst_tcs = - $total_gst_tcs;
			$igst_gst_amount_tcs = -$igst_gst_amount_tcs; 
			$cgst_gst_amount_tcs = -$cgst_gst_amount_tcs;
			$sgst_gst_amount_tcs = -$sgst_gst_amount_tcs;
			$posting_date = date("Y-m-t", strtotime($credit_memo_created_date));
			$value_supplier_return = -$gross_price;
			$gross_price="";
		}
		if(!empty($customer_gstin)) {
			$is_customer_registered = "Registered";
		} else {
			$is_customer_registered = "";
		}


		$posting_date = date('Y-m-t',strtotime($transaction_completion_date));
		$seller_state = ucfirst($seller_state);
		$orignal_invoice_date = $this->corectDatewithouttime($orignal_invoice_date);
		$document_date = $this->corectDatewithouttime($document_date);


		$sql = "INSERT INTO `om_gsttcs_report` set		
		`invoice_no`='$invoice_id',
		`suppliers_gstin`='$seller_gst',
		`register_unregister_supplier`='Registered',
		`state_of_supplier`= '$seller_state',
		`gms_ecommerce_gstin`='$pana_gst',
		`state_of_e_commerce_gstin`='$pana_state',
		`type_of_transaction`='$transection_type',
		`type_of_document`='$document_type',
		`state_of_customer`='$customer_shipping_state',
		`customer_gstin`='$customer_gstin',
		`is_customer_registered`='$is_customer_registered',
		`original_gstin_of_the_supplier`='',
		`original_invoice_no`='$orignal_invoice',
		`original_invoice_date`='$orignal_invoice_date',
		`document_number_internal_invoice_no`='',
		`document_date`='$document_date',
		`posting_date`='$posting_date',
		`place_of_supply`='$customer_shipping_state',
		`type_of_supply`='$supply_type',
		`description_of_goods_services_supplied`='$product_name',
		`hsn_sac_code`='$product_hsn',
		`unit_of_measurement`='number',
		`number_of_units`='$product_qty',
		`gross_value_of_supplies_made`='$gross_price',
		`value_of_supplies_returned`='$value_supplier_return',
		`net_amount_liable_for_tcs`='$net_amount_lible_tcs',
		`igst_tcs_rate`='$igst_rate',
		`igst_amount_tcs`='$igst_gst_amount_tcs',
		`cgst_tcs_rate`='$cgst_rate',
		`cgst_amount_tcs`='$cgst_gst_amount_tcs',
		`sgst_tcs_rate`='$sgst_rate',
		`sgst_amount_tcs`='$sgst_gst_amount_tcs',
		`utgst_tcs_rate`='$utgst_rate',
		`utgst_amount_tcs`='$utgst_gst_amount_tcs',
		`total_gst_tcs_amount`='$total_gst_tcs',
		`type`='$type',
		`item_id`='',
		`movement_type`='$movement_type',
		`order_inc_id`='$order_inc_id'
		";
		$connection = $resource->getConnection();
		$connection->query($sql); 	      
	
	}	
	
	public function getCreditMemo($order_id)
	{	/* code  cleaned @ritesh */
		$connection = $this->_resource->getConnection();
		$select = "select * from sales_creditmemo where order_id = $order_id";
		$result = $connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
	}

	public function insert_sales_report($order_inc_id,$product_id,$item_id,$product_qty,$movement_type,$transaction_completion_date) 
	{
		$type='gsttcs';
		$query = "select id from `om_gsttcs_report` where order_inc_id='$order_inc_id' and movement_type='$movement_type' and type='$type' and item_id='$item_id'";
		$alreadyExist = $this->connection->fetchOne($query);
		
		if($alreadyExist)
		{
			return false;
		}

		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();
		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		// print_r($gstArray);

		$customerID = $orderArray['customer_id'];
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerID);
		$customer_gstin = $customerObj->getData('gst_number');
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		$check_return=$api_helpers->check_product_in_return($order_id,$item_id);	
		
		$return_req_date='';
		$return_req_reason='';
		$pickup_date='';	 
		
		if($check_return==1)
		{	
			$movement_type='forward';
			$array=$sales_helpers->get_forward_order_tracking($order_id);
			$order_status=$array['status'];
			$pickup_date=$array['pickup_date'];
		}
		else
		{
			$movement_type='Return';
			$array=$sales_helpers->get_return_order_tracking($order_id,$product_id);
			
			$order_status=$array['status'];
			$pickup_date=$array['pickup_date'];
			
			$returnArray=$sales_helpers->order_return_details($order_id,$item_id);
			$return_req_date=$returnArray['rma_created_at'];
			$return_req_reason=$returnArray['rma_reason'];  
		}		
			
		$awb=$tax_helpers->tax_get_order_awb($order_id);
		
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$grand_total=$orderDetailArray->grand_total;
		$total_discount=$orderDetailArray->total_discount;
		$total_gst=$orderDetailArray->total_gst;
		$shipping_expense=$orderDetailArray->shipping_expense;
		$shipping_base=$orderDetailArray->shipping_base;
		$shipping_gst=$orderDetailArray->shipping_gst;
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		
		$payment_type='COD';	 
		
		if($payment_id!='COD') 
		{
			$payment_type='Online';	
		}	
		
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		//print_r($seller_details);
		
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		
		$seller_zipcode=$seller_details->seller_zipcode;
		$seller_state=$seller_details->seller_state;
		
		$seller_gst=$seller_details->seller_gst;
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		
		
		
		$buyer_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $custom_helpers->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $custom_helpers->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','billing');
		
		
		$buyer_street = '';
		foreach($order->getBillingAddress()->getStreet() as $value){
			$buyer_street .= $value.', ' ;
		}
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
		
		
		$delivery_firstname=$custom_helpers->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$custom_helpers->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $custom_helpers->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $custom_helpers->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $custom_helpers->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $custom_helpers->get_customer_order_adress($order_id,'country_id','shipping');
		
		$street = '';
		foreach($order->getShippingAddress()->getStreet() as $value){
			$street .= $value.', ' ;
		}
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	
		
		if($seller_state=="new delhi")
		{ 
			$seller_state='Delhi';	
			//$seller_statee='delhi';	
		}

		if(strtolower($seller_state)==strtolower($delivery_state))
		{
			$supply_type='Intra-State';	
		} 
		/* elseif($delivery_pincode == $seller_zipcode){
			loophole exist in the  current system condition to check loophole  which passed
			$supply_type='Intra-State';
		}  */
		else  {
			$supply_type='Inter-State';	
		}

		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
		$categories = $product->getCategoryIds();
		$product_name = $product->getName();
		$product_hsn = $product->getHsn();
		$product_gst_rate = $product->getGstRate();
		
		$product_name = preg_replace('/[^A-Za-z0-9. -]/', '', $product_name);
			
		$parent_cat_nam='';
		$parent_cat_name='';
		
		$child_cat_nam='';
		$child_cat_name='';
		
		$selected_cat_nam='';	
		$selected_cat_name='';	 
		$product_invoice_value='';	 
		
		$array=array();
		
		
		//$seller_city=$helpers->get_city_pincode($seller_zipcode);
		//$seller_state=$helpers->get_region_pincode($seller_zipcode); 
		
		
		$discount_per='';  
		$discount_type=''; 
		$discount_amount=''; 
		$coupon_number='';
		$shipping_sac_code='';
		
		
		$shipping_date='';			
		$delivery_date='';	  			
		$cancel_date='';	
		
		
		$productArray=$tax_helpers->order_item_details_product($order_inc_id,$product_id);
		/* echo "<pre>";
			print_r($productArray);
		echo "</pre>";   */   
		
		
		$grand_total=$productArray[0]['row_total'];
		$total_gst=$productArray[0]['total_gst'];
		$gross_price=$productArray[0]['product_base'];
		$total_discount=$productArray[0]['product_discount'];  
		$net_sale_value=$productArray[0]['net_sale_value'];
		$product_invoice_value=$productArray[0]['product_invoice_value'];
		$comission=$productArray[0]['admin_comm'];
		
		
		
		$shipping_base=$productArray[0]['shipping_base'];
		$shipping_gst=$productArray[0]['shipping_gst'];
		$shipping_expense=$productArray[0]['product_shipping'];
		$product_admin_comm=$productArray[0]['product_admin_comm_rate'];
		
		$total_invoice_value=$product_invoice_value+$shipping_expense;  
		 
		
		
		$value_supplier_return=0;
		$net_amount_lible_tcs=$gross_price-$value_supplier_return;
		
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		
		
		$igst_amount_int=round($igst_amount);
		$cgst_amount_int=round($cgst_amount);
		$sgst_amount_int=round($sgst_amount);
		$utgst_amount_int=round($ugst_amount);
		
		//making adjustment because of  previous developer's SINS  and crimes  commited in code @ritesh , 
		//round function return 0 in case of value is 0.6 which is not taken care of  so  taking care  of  that
		if($igst_amount<1 && $igst_amount>0)
		{
			$igst_amount_int=1;
		}		
		if($cgst_amount<1 && $cgst_amount>0)
		{
			$cgst_amount_int=1;
		}
		if($sgst_amount<1 && $sgst_amount>0)
		{
			$sgst_amount_int=1;
		}
		if($ugst_amount<1 && $ugst_amount>0)
		{
			$utgst_amount_int=1;
		}
		
		$igst_rate=0;
		$cgst_rate=0;
		$sgst_rate=0;
		$utgst_rate=0;
		
		
		$igst_gst_amount=0;
		$cgst_gst_amount=0;
		$sgst_gst_amount=0;
		$utgst_gst_amount=0;
		
		$igst_gst_amount_tcs=0;
		$cgst_gst_amount_tcs=0;
		$sgst_gst_amount_tcs=0;
		$utgst_gst_amount_tcs=0;
		
		$gst_tcs_rate=$orderDetailArray->gst_tcs_rate; 
		
		
		if($igst_amount_int!=0)
		{
			
			$igst_rate = $gst_tcs_rate;
			$igst_gst_amount = $total_gst;
			$igst_gst_amount_tcs = ($net_amount_lible_tcs*$gst_tcs_rate)/100;
		}
		
		if($cgst_amount_int!=0 && $sgst_amount_int!=0)
		{
			$cgst_rate=$gst_tcs_rate/2;
			$sgst_rate=$gst_tcs_rate/2;
			
			$cgst_gst_amount=$total_gst/2;
			$sgst_gst_amount=$total_gst/2;
			
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$sgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}	
		
		if($cgst_amount_int!=0 && $utgst_amount_int!=0)
		{
			$cgst_rate=$gst_tcs_rate/2;
			$utgst_rate=$gst_tcs_rate/2;
			
			$cgst_gst_amount=$total_gst/2;
			$utgst_gst_amount=$total_gst/2;
			
			$cgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
			$utgst_gst_amount_tcs=(($net_amount_lible_tcs*$gst_tcs_rate)/100)/2;
		}	
		
		if($supply_type=='Inter-State')
		{
			$total_gst_tcs=$igst_gst_amount_tcs;
		} else 
		{
			$total_gst_tcs=$cgst_gst_amount_tcs+$sgst_gst_amount_tcs+$utgst_gst_amount_tcs;
			
		}
		
		$seller_state=strtolower($seller_state);
		if($seller_state=="new delhi") { 
			$seller_state='Delhi';	
		}  	
		
		$pana_state=0;
		$pana_gst=0;
		
		$sql_pana="select * from gst_tcs_panasonic where state_code='".$seller_state."'";
		$results_pana = $connection->fetchAll($sql_pana);    
		if(!empty($results_pana))
		{	
			$pana_state=$results_pana[0]['state_code'];
			$pana_gst=$results_pana[0]['gst']; 
		}
		
		$document_date=date('Y-m-d',strtotime($order_cr_date));
		$posting_date=date("Y-m-t", strtotime($order_cr_date));
		
		$transection_type='Regular Supply';
		$document_type="Tax Invoice";
		$orignal_invoice_date='';
		$orignal_invoice='';
		if($type =='refund')
		{
			$transection_type="Sales returns / Deficiency in service (Credit Note)";
			$creditmemo_details = $this->getCreditMemo($order_id);
			$document_type = "Credit Note";
			$orignal_invoice_date = $document_date;
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			$orignal_invoice = $invoice_id; 
			$invoice_id = $credit_memo_increment_id;
			$document_date = $credit_memo_created_date;
			$net_amount_lible_tcs= - $net_amount_lible_tcs;
			$igst_gst_amount= -$igst_gst_amount;
			$cgst_gst_amount= -$cgst_gst_amount;
			$sgst_gst_amount= -$sgst_gst_amount;
			$utgst_gst_amount= -$utgst_gst_amount;
			$total_gst_tcs = - $total_gst_tcs;
			$igst_gst_amount_tcs = -$igst_gst_amount_tcs; 
			$cgst_gst_amount_tcs = -$cgst_gst_amount_tcs;
			$sgst_gst_amount_tcs = -$sgst_gst_amount_tcs;
			$value_supplier_return = -$gross_price;
			$gross_price="";
			$posting_date = date("Y-m-t", strtotime($credit_memo_created_date));
		}

		if(!empty($customer_gstin)) {
			$is_customer_registered = "Registered";
		} else {
			$is_customer_registered = "";
		}

		$posting_date = date('Y-m-t',strtotime($transaction_completion_date));
		$seller_state = ucfirst($seller_state);
		$orignal_invoice_date = $this->corectDatewithouttime($orignal_invoice_date);
		$document_date = $this->corectDatewithouttime($document_date);
		
		$sql = "INSERT INTO `om_gsttcs_report` set		
		`invoice_no`='$invoice_id',
		`suppliers_gstin`='$seller_gst',
		`register_unregister_supplier`='Registered',
		`state_of_supplier`= '$seller_state',
		`gms_ecommerce_gstin`='$pana_gst',
		`state_of_e_commerce_gstin`='$pana_state',
		`type_of_transaction`='$transection_type',
		`type_of_document`='$document_type',
		`state_of_customer`='$delivery_state',
		`customer_gstin`='$customer_gstin',
		`is_customer_registered`='$is_customer_registered',
		`original_gstin_of_the_supplier`='',
		`original_invoice_no`='$orignal_invoice',
		`original_invoice_date`='$orignal_invoice_date',
		`document_number_internal_invoice_no`='',
		`document_date`='$document_date',
		`posting_date`='$posting_date',
		`place_of_supply`='$delivery_state',
		`type_of_supply`='$supply_type',
		`description_of_goods_services_supplied`='$product_name',
		`hsn_sac_code`='$product_hsn',
		`unit_of_measurement`='number',
		`number_of_units`='$product_qty',
		`gross_value_of_supplies_made`='$gross_price',
		`value_of_supplies_returned`='$value_supplier_return',
		`net_amount_liable_for_tcs`='$net_amount_lible_tcs',
		`igst_tcs_rate`='$igst_rate',
		`igst_amount_tcs`='$igst_gst_amount_tcs',
		`cgst_tcs_rate`='$cgst_rate',
		`cgst_amount_tcs`='$cgst_gst_amount_tcs',
		`sgst_tcs_rate`='$sgst_rate',
		`sgst_amount_tcs`='$sgst_gst_amount_tcs',
		`utgst_tcs_rate`='$utgst_rate',
		`utgst_amount_tcs`='$utgst_gst_amount_tcs',
		`total_gst_tcs_amount`='$total_gst_tcs',
		`type`='$type',
		`movement_type`='$movement_type',
		`item_id`='$item_id',
		`order_inc_id`='$order_inc_id'
		";
		$connection = $resource->getConnection();
		$connection->query($sql); 	     
	}
	
	public function get_forward_order_tracking($order_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		
		$approved=0;	
		$processed=0;	
		$shipped=0;	 
		$delivered=0;	
		$status=0;	
		$pickup_date=0;	
		$delivered_dates=0;	
		
		 
		
		$tracking_array=$api_helpers->get_order_tracking($order_id);   
		
		
		if($tracking_array=='0')  
		{
			$approved=1;	 
			$processed=1;	
			$status="Processing";
		}
		else
		{
			$approved=1;	
			$processed=1;	
			$tracking_arrayy=json_decode($tracking_array);	
			
			/* echo "<pre>";
			print_r($tracking_arrayy);
			echo "</pre>"; */
			
			
			$trackArray=$tracking_arrayy[36]->object;
				
			
			$pickup=$tracking_arrayy[9];
			
			
			if(!is_object($pickup))
			{
				$shipped=1;	
				$status="Shipped";
				$pickup_date=$pickup;
			}	
			

			$delivered_date=$tracking_arrayy[21];
			if(!is_object($delivered_date))
			{
				$delivered=1;	
				$status="Delivered";
				$delivered_dates=$delivered_date;
			}

		
		}		
	 
		
		$array=array(
			'pickup_date'=>$pickup_date,	
			'delivered_date'=>$delivered_dates,	
			'status'=>$status,	
		);
		
		return $array;  
	
	}


	public function get_return_order_tracking($order_id)
	{ 
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
		$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
		$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$sales_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		
		
		$approved=0;	
		$processed=0;	
		$shipped=0;	 
		$delivered=0;	
		$status=0;	
		
		$pickup_date=0;
		$delivered_dates=0;
		 
		$tracking_array=$api_helpers->get_order_tracking($order_id);   
		
		if($tracking_array=='0')
		{
			$approved=1;	 
			$processed=1;	
			$status="Approved";
		}
		else
		{
			$approved=1;	
			$processed=1;	
			$tracking_arrayy=json_decode($tracking_array);	
			
			$trackArray=$tracking_arrayy[36]->object;
				
			
			$pickup=$tracking_arrayy[9];
			
			if(!is_object($pickup))
			{
				$shipped=1;	
				$status="Shipped";
				$pickup_date=$pickup;
			}	
			

			$delivered_date=$tracking_arrayy[21];
			if(!is_object($delivered_date))
			{
				$delivered=1;	
				$status="Delivered";
				$delivered_dates=$delivered_date;
			}

		
		}		
	
		
		$array=array(
			'pickup_date'=>$pickup_date,	
			'delivered_date'=>$delivered_dates,	
			'status'=>$status,	
		);
		
		return $array;  
	
	}

	
	
	public function order_return_details($order_id,$item_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		
		$select = $connection->select()
                  ->from('wk_rma') 
                  ->where('order_id = ?', $order_id);
		
		$results = $connection->fetchAll($select);
		$cr_date='';
		$rma_reason='';
		
		if(!empty($results))
		{
			$cr_date=$results[0]['created_at'];	
		}	
		
		
		$select = $connection->select()
                  ->from('wk_rma_items') 
                  ->where('order_id = ?', $order_id)
                  ->where('item_id = ?', $item_id);
		
		$results = $connection->fetchAll($select);
		if(!empty($results))
		{
			$rma_reason=$results[0]['rma_reason'];	
		}	
		
		
		$array=array(
			'rma_created_at'=>$cr_date,
			'rma_reason'=>$rma_reason,
		);
		return $array;
		
	} 

	public function check_gst_tcs_report($increment_id)
	{
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = "Select * from om_gsttcs_report where order_id='$increment_id' and movement_type='forward' ";
		$results = $connection->fetchAll($select);
		if(!empty($results))
		{
			return 1;
		}	
		else
		{
			return 0;
		}		
	}	
	
	public function checkiforderrefunded($increment_id){
		$connection = $this->_resource->getConnection();
		$refunded_payment_query = "SELECT * FROM `sales_order` where `state`='closed' and increment_id='$increment_id'";
		$results = $connection->fetchAll($refunded_payment_query);
		$status =  !empty($results) ? 1:0;
		return $status;
	}

	public function check_return_gst_tcs_report($increment_id)
	{
		$isorderrefunded =  $this->checkiforderrefunded($increment_id);
		if(empty($isorderrefunded)){ /* here it means opposite as  0  will generate report */ return 1;  }
		$connection = $this->_resource->getConnection();
		$select = "select * from `om_gsttcs_report` where order_id= $increment_id and type='refund'";
		$results = $connection->fetchAll($select);
		$status =  !empty($results) ? 1:0;
		return $status;
	}
	public function corectDatewithouttime($date){
		if(empty($date)){return $date;}
		$date=date_create($date);
		return date_format($date,'d-m-Y');
	}
		
}