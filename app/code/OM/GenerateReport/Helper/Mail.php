<?php
/**
 * Copyright Â©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\GenerateReport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Mail extends AbstractHelper
{

	public function __construct(
		\Magento\Framework\App\Helper\Context $context
	) {
		parent::__construct($context);
	}

	public  function sendEmailWithAttachment($to,$from,$msg,$subject,$attachment,$attachment_name)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$attachment=base64_encode($attachment);
			$to_actual_array = array();
			if(is_array($to))
			{
				/* need array unique as sendgrid do not accept request if emails are duplicated for  any reason */
				$to=array_unique($to);
				foreach ($to as $to_emails)
				{
					$to_actual_array[]=array('email' => $to_emails);
				}
			} else 
			{
				$to_actual_array[]=array('email' => $to);
			}
			/* cannot get this from configuration as i am using the latest v3 version  @ritesh10march2021*/
			$sendgridurl = 'https://api.sendgrid.com/v3/mail/send';
			$token_encrypted = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('smtp/configuration_option/password');
			$_encryptor = $objectManager->create('\Magento\Framework\Encryption\EncryptorInterface');
			$token = $_encryptor->decrypt($token_encrypted);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $sendgridurl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$fied_array =array (
				'personalizations' => array (0 => array(
						'to' =>$to_actual_array
						),
					),
				'from' => 
					array (
						'email' => $from,
					),
				'subject' => $subject,
				'content' => array (0 => 
					array (
						'type' => 'text/html',
						'value' => "<html>$msg</html>",
					),
				),
				'attachments' => array (0 => 
					array (
						'type' => 'text/csv',
						"content"=> "$attachment",
						"filename"=>"$attachment_name",
					),
				)
			);
			$fied_array_json = json_encode($fied_array);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fied_array_json);
			$headers = array();
			$headers[] = "Authorization: Bearer $token";
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result = curl_exec($ch);
			if(!empty($result))
			{
				/* error  in api reports */
        
			} 
			curl_close($ch);
		}

}