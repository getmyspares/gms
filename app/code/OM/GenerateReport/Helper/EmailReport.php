<?php
/**
 * Copyright Â©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\GenerateReport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

class EmailReport extends AbstractHelper
{
	protected $_objectManager;

	private $date ;
	protected $_inlineTranslation;
    protected $_transportBuilder;
	
	public function __construct(
		\Magento\Framework\App\Helper\Context $context,

		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Framework\Filesystem $filesystem,
		\Webkul\Rmasystem\Helper\Data $rmasystemhelperdata,
		\Magento\Catalog\Model\Product $product,
		\OM\GenerateReport\Helper\AutomatedMails\ProductWeight $productWeight, 
		\Magento\Customer\Api\CustomerRepositoryInterface $customer,
		\Magento\Customer\Api\AddressRepositoryInterface $addressInterface,
		\Webkul\Rmasystem\Helper\Data $rmaHelper,
		\Magento\Review\Model\Review $locationFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
		\Panasonic\CustomUser\Helper\Data $panasonicHelper
	) {
		parent::__construct($context);
		$this->_rmasystemhelperdata = $rmasystemhelperdata;
		$this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
		$this->_storeManager = $storeManager;
		$this->rmaHelper = $rmaHelper;
		$this->productWeight = $productWeight;
		$this->connection = $resourceConnection->getConnection();
		$this->panasonicHelper=$panasonicHelper;
		$this->_locationFactory = $locationFactory;
		$this->_product = $product;
		$this->_customer = $customer;
		$this->_inlineTranslation = $inlineTranslation;
		$this->addressInterface = $addressInterface;
		$this->date = date('Y-m-d',strtotime("-1 days")); 
	}

	public function formatPrice($price)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
		$priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper
		$formattedPrice = $priceHelper->currency($price, true, true);
		return $formattedPrice;
	}

	public  function sendReportsToAdmin()
	{
		$this->sendRmaReport();
		$this->sendSalesReport();
		$this->sendReviewEmail();
		$this->sendSettlementReport();
		$this->sendNewSellerEmail();
		$this->sendGstTcsReport();
		$this->sendProductApprovalEmail();
		$this->sendGetBestPriceEmail();
		$this->sendQuestionEmail();
		$this->productWeight->checkWeightNotFoundEmail();
	}	

  public function salesReportEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/sales_report/email'));
	}

	public function settlementReportEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/settlement_report/email'));
	}
	
	public function gstReportEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/gst_tcs_report/email'));
	}
		
	public function getRmaEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/rma/email'));
	}

	public function newSellerEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/new_seller/email'));
	}
	
	public function getQuestionEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/question/email'));
	}
	
	public function getReviewEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/review/email'));
	}
	
	public function getNewProductApprovalEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/product_approval/email'));
	
	}	

	public function getBestPriceDailyReportEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/best_price_daily/email'));
	}

	public  function roundOff($amount)
	{
		return round($amount,2);
	}

	public function agingData(){
		return $this->AgingReportData();
	}
	
	public function sendSalesReport()
	{
		$query = 'SELECT `sor`.`o_created_by`,osr.*, oeas.`order_status`, oeas.`pickup_date`, oeas.`awb_number`, oeas.`delivery_date`
		FROM `om_sales_report` osr 
		JOIN om_ecom_api_status oeas ON osr.order_increment_id=oeas.increment_id 
		JOIN sales_order sor ON sor.increment_id=osr.order_increment_id';
		$datas = $this->connection->fetchAll($query.' ORDER BY order_increment_id DESC');
		$columns = $this->getSalesColumns();
		$csvHeader = array_values($columns);
		$filename = 'Sales_Report-'.date('Y-m-d-H-i-s').'.csv';
		$final_text_data="";
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
		foreach ($datas as $data) {
			$insertArr=$this->generateSalesReportRow($data);
			fputcsv($fp, $insertArr, ",");
		}
		fseek($fp, 0);
		$file = stream_get_contents($fp);
		fclose($fp);
		
		if($file)
		{
			$today = date("l (d-m-Y)", strtotime($this->date));
			$to = $this->salesReportEmail();
			$from = $this->panasonicHelper->getFromEmail();
			$subject="Sales Report For $today";
			
			$yesterday_sale_data = $this->getDailySale();
			$monthly_sale_data = $this->getMonthlySale();
			$yearly_sale_data = $this->getYearlySale();



			$yesterday_sale_amount = strip_tags($this->formatPrice($yesterday_sale_data['total']));
			$monthly_sale_amount = strip_tags($this->formatPrice($monthly_sale_data['total']));
			$yearly_sale_amount = strip_tags($this->formatPrice($yearly_sale_data['total']));

			$yesterday_sale_count = $yesterday_sale_data['order_count'];
			$monthly_sale_count = $monthly_sale_data['order_count'];
			$yearly_sale_count = $yearly_sale_data['order_count'];
			
			$query= "SELECT count(DISTINCT order_id) FROM `om_sales_report` where order_id in (SELECT order_id FROM `om_ecom_api_status` where order_status in ('New'))";
			$open_order_count = $this->connection->fetchOne($query);
			

			$query= "SELECT sum(`total_invoice_value`) FROM `om_sales_report` where order_id in (SELECT order_id FROM `om_ecom_api_status` where order_status in ('New'))";
			$open_order_amount = $this->connection->fetchOne($query);
			$open_order_amount = $this->roundOff($open_order_amount);
			$open_order_amount = strip_tags($this->formatPrice($open_order_amount));

			$msg= "<html>";
			$msg	.="
			<p>Hi , Please download Sales Report of date : $today <p>
			<table border='1'  style='border-collapse:collapse'>
				<tr>
					<th>Sales</th>
					<th>Order Count</th>
					<th>Amount (IN INR)</th>
				</tr>
				<tr>
					<td>FYTD </td>
					<td>$yearly_sale_count</td>
					<td>$yearly_sale_amount</td>
				</tr>
				<tr>
					<td>MTD sales </td>
					<td>$monthly_sale_count</td>
					<td>$monthly_sale_amount</td>
				</tr>	
				<tr>
					<td>Yesterday’s sales</td>
					<td>$yesterday_sale_count</td>
					<td>$yesterday_sale_amount</td>
				</tr>	
				<tr>
					<td>Open Orders</td>
					<td>$open_order_count</td>
					<td>$open_order_amount</td>
				</tr>
				<tr>
					<td>Aging</td>
					<td></td>
				</tr>	
			</table>
			";
			$fullArr = $this->AgingReportData();
			$msg.= '<table border="1"  style="border-collapse:collapse">';
			
			foreach($fullArr as $mainkey=>$row ){
				$msg.="<tr>";
					foreach ($row as $key =>$value){
							if($key==0 || $key==28 || $mainkey==1 || $mainkey==11):
								$msg.="<th>$value</th>";
							else :
								$msg.="<td>$value</td>";
							endif;
						}
				$msg.="</tr>";
			}
			$msg.="</table>";
			$msg.="</html>";

			//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
					$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
					$selectedTemplateId = $scopeConfig->getValue('custom_template/email/sales_report');
					$templateId = $selectedTemplateId;
					$fromEmail = $this->panasonicHelper->getFromEmail(); 
					$fromName = 'GetMySpares'; 
					$toEmail = $this->salesReportEmail();
					$transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		
					try {
							// template variables pass here
							$templateVars = [
									'today' => $today,
									'yearly_sale_count' => $yearly_sale_count,
									'yearly_sale_amount' => $yearly_sale_amount,
									'monthly_sale_count' => $monthly_sale_count,
									'monthly_sale_amount' => $monthly_sale_amount,
									'yesterday_sale_count' => $yesterday_sale_count,
									'yesterday_sale_amount' => $yesterday_sale_amount,
									'open_order_count' => $open_order_count,
									'open_order_amount' => $open_order_amount,
							];
		
							$storeId = $storeManager->getStore()->getId();
		
							$from = ['email' => $fromEmail, 'name' => $fromName];
							$this->_inlineTranslation->suspend();
		
							$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
							$templateOptions = [
									'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
									'store' => $storeId
							];
							$transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
									->setTemplateOptions($templateOptions)
									->setTemplateVars($templateVars)
									->setFrom($from)
									->addTo($toEmail)
									->addAttachment($file, $filename, 'application/octet-stream')
									->getTransport();
							$transport->sendMessage();
							$this->_inlineTranslation->resume();
					} catch (\Exception $e) {
							echo 'error'.$e->getMessage();
							//$this->logger->info($e->getMessage());
					}
		}
	}

	public function getDailySale()
	{
		$type="yesterday";
		$yesterday = "SELECT * FROM `om_sales_report` where date(order_date) = DATE(NOW() - INTERVAL 1 DAY) and report_type=1";
		$datas = $this->connection->fetchAll($yesterday);
		return $this->calculateTotal($datas,$type);
		
	}
	
	public function getMonthlySale()
	{
		$type="month";
		$current_month = "SELECT * from om_sales_report where year(curdate()) = year(order_date) and month(curdate()) = month(order_date) and report_type=1";
		$datas = $this->connection->fetchAll($current_month);
		return $this->calculateTotal($datas,$type);
		
	}

	public function getYearlySale()
	{
		$type="year";
		$current_month = "SELECT * FROM om_sales_report WHERE YEAR(order_date) = YEAR(CURDATE()) and report_type=1";
		$datas = $this->connection->fetchAll($current_month);
		return $this->calculateTotal($datas,$type);
	}

	public  function calculateTotal($data,$type)
	{
		$total=0;
		$order_count_array=array();
		if(!empty($data))
		{
			foreach($data  as $row)
			{
				$include_amount=true;
				$total_invoice_value = $row['total_invoice_value'];
				$order_date = $row['order_date'];
				$item_id = $row['item_id'];
				$order_id = $row['order_id'];
				$sql  = "SELECT * from om_sales_report  where item_id='$item_id' and report_type=7";
				$return_data = $this->connection->fetchAll($sql);
				if($return_data)
				{
					$retun_row = $return_data[0];
					$sql = "SELECT created_at from sales_creditmemo where order_id='$order_id'";
					$credit_memo_date = $this->connection->fetchOne($sql);
					
					if(!empty($credit_memo_date) && !empty($order_date))
					{
						$credit_memo_date_timestamp = strtotime($credit_memo_date);
						$order_date_timestamp = strtotime($order_date);

						$date1 = new \DateTime();
						$date1->setTimestamp($credit_memo_date_timestamp);
	
						$date2 = new \DateTime();
						$date2->setTimestamp($order_date_timestamp);
				
				
						if($type=="month")
						{
							if ($date1->format('Y-m') === $date2->format('Y-m')) {
								$include_amount=false;	
							}
						}
						
						if($type=="yesterday")
						{
							if ($date1->format('Y-m-d') === $date2->format('Y-m-d')) {
								$include_amount=false;	
							}
						}
						
						if($type=="year")
						{
							if ($date1->format('Y') === $date2->format('Y')) {
								$include_amount=false;	
							}
						}
					}
				}

				if($include_amount)
				{
					$order_count_array[] = $order_id;
					$total=$total+$total_invoice_value;		
				}
			
			}
			$order_count = count($order_count_array);
			$total=$this->roundOff($total);
			return array("total"=>$total,"order_count"=>$order_count);
		}
	}
	
	public function sendProductApprovalEmail()
	{
		$date = $this->date;
		$query = "SELECT * FROM `marketplace_product` where `status`=0";
		$datas = $this->connection->fetchAll($query);
		$columns = $this->getProductApprovalColumns();
		$csvHeader = array_values($columns);
		$filename = 'Product Approval Report-'.date('Y-m-d-H-i-s').'.csv';
		$final_text_data="";
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
		if(empty($datas))
		{
			return false;
		}
		foreach ($datas as $data) {
			$insertArr = $this->generateProductApprovalRow($data);
			fputcsv($fp, $insertArr, ",");
		}
		fseek($fp, 0);
		$file = stream_get_contents($fp);
		fclose($fp);
		
		if($file)
		{
			$today = date("l (d-m-Y)", strtotime($this->date));
			$to = $this->getNewProductApprovalEmail();
			$from = $this->panasonicHelper->getFromEmail();
			$subject="Product_Approval For $today";
			$msg="<html>
			<p>Hi , Please download Product Approval Report of date : $today <p>
			<html>";
			//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/product_approval');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail(); 
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->getNewProductApprovalEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
		}
	}

	public function sendGetBestPriceEmail()
	{
		$date = $this->date;
		$query = "SELECT * FROM `om_get_best_price`  ORDER BY `om_get_best_price`.`id`  DESC";
		$datas = $this->connection->fetchAll($query);
		$columns = $this->getGetBestPriceColumns();
		$csvHeader = array_values($columns);
		$filename = 'Best Price Request-'.date('Y-m-d-H-i-s').'.csv';
		$final_text_data="";
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
		if(empty($datas))
		{
			return false;
		}
		foreach ($datas as $data) {
			$insertArr = $this->generateGetBestPriceRow($data);
			print_r($insertArr);
			fputcsv($fp, $insertArr, ",");
		}
		fseek($fp, 0);
		$file = stream_get_contents($fp);
		fclose($fp);
		
		if($file)
		{
			$today = date("l (d-m-Y)", strtotime($this->date));
			$to = $this->getBestPriceDailyReportEmail();
			$from = $this->panasonicHelper->getFromEmail();
			$subject="Best Price Request Report For $today";
			$msg="<html>
			<p>Hi , Please download Best Price Request Report of date : $today <p>
			<html>";
			//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/best_price_request');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail(); 
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->getBestPriceDailyReportEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
		}
	}
	
	public function getGetBestPriceColumns()
	{
		return ["Product Id","Product Name","Qty","Customer Email","Customer Name","Mobile","Requested At"];
	}

	public function generateGetBestPriceRow($data)
	{
		$insertArr=[];
		$product_id = $data['product_id'];
		$qty = $data['qty'];
		$product_name = $data['product_name'];
		$customer_email = $data['customer_email'];
		$customer_name = $data['customer_name'];
		$mobile_number  = $data['pincode'];
		$created_at  = $data['created_at'];
        $requested_at="";
		if(!empty($created_at))
        {
		 $requested_at = date ('d F Y (l) ',strtotime($created_at));
        }
        if(strlen($mobile_number)>9 && strlen($mobile_number)<12)
        {
            $customer_mobile = $mobile_number;
        } else
        {
            $query = "select entity_id from customer_entity where email='$customer_email'";
		    $customer_id = $this->connection->fetchOne($query);
           $query = "Select `value` from customer_entity_varchar where  entity_id='$customer_id' and attribute_id='178'";
		   $customer_mobile = $this->connection->fetchOne($query);
        }

		$insertArr[] = $product_id; 
		$insertArr[] = $product_name; 
		$insertArr[] = $qty; 
		$insertArr[] = $customer_email; 
		$insertArr[] = $customer_name; 
		$insertArr[] = $customer_mobile;
		$insertArr[] = $requested_at;
		return $insertArr;
	}
	

	public function generateProductApprovalRow($data)
	{
		$insertArr=[];
		$product_id = $data['mageproduct_id'];
		$seller_id = $data['seller_id'];
		$status = $data['status'];
		$is_approved = $data['is_approved'];
		
		$query = "SELECT firstname,lastname FROM `customer_entity` where entity_id='$seller_id'";
		$customer_data = $this->connection->fetchAll($query);
				
		if(!empty($customer_data) && is_array($customer_data))
		{
			$firstname = $customer_data[0]['firstname'];
			$lastname = $customer_data[0]['lastname'];
			$selller_name = $firstname." ".$lastname;
		}

		$product_data = $this->getProduct($product_id);
		$product_name = $product_data['name'];
		$product_price = $product_data['price'];
		$quantity = $product_data['qty'];
		
		$status="";
		if($data['status']==0)
		{
			$status="pending approval";
		}
		if($data['status']==1)
		{
			$status="approved";
		}
		if($data['status']==2)
		{
			$status="disapproved";
		}
		$insertArr[] = $product_id; 
		$insertArr[] = $selller_name; 
		$insertArr[] = $product_name; 
		$insertArr[] = $product_price; 
		$insertArr[] = $quantity; 
		$insertArr[] = $status; 
		return $insertArr;
	}

	public function getProductApprovalColumns()
	{
		return ["Product Id","Seller Name","Product Name","Price","Quantity","Status"];
	}

	public function sendReviewEmail()
	{
			$csvHeader = $this->reviewColumns();
			$filename = 'Review Report-'.date('Y-m-d-H-i-s').'.csv';
			$final_text_data="";
			$fp = fopen('php://memory', 'w');
			fputcsv( $fp, $csvHeader,",");
			$location = $this->_locationFactory;
			$location_collection = $location->getCollection(); // get Collection of Table data 
			$location_collection->addFieldToFilter('status_id', ['eq' => 2]);
			
			if(!$location_collection->count())
			{
				return 0;
			}
			
			$onestar=0;
			$twostar=0;
			$threestar=0;
			$fourstar=0;
			$fivestar=0;
			foreach($location_collection as $item){
				
				$created_at = $item->getData('created_at');
				$insertArr = $this->generateReviewReportRow($item);
				
				if(!empty($created_at))
				{
					
					$date1 = new \DateTime("now");
					$created_at_timestamp = strtotime($created_at);
					$date2 = new \DateTime();
					$date2->setTimestamp($created_at_timestamp);
					
					if ($date1->format('Y-m') === $date2->format('Y-m')) {
						if($insertArr[4]==1)
						{
							$onestar++;
						}
						if($insertArr[4]==2)
						{
							$twostar++;
						}
						if($insertArr[4]==3)
						{
							$threestar++;
						}
						if($insertArr[4]==4)
						{
							$fourstar++;
						}
						if($insertArr[4]==5)
						{
							$fivestar++;
						}
					}
				}

				
				fputcsv($fp, $insertArr, ",");
			}
			fseek($fp, 0);
			$file = stream_get_contents($fp);
			fclose($fp);
			
			if($file)
			{
				$today = date("l (d-m-Y)", strtotime($this->date));
				$to = $this->getReviewEmail();
				$from = $this->panasonicHelper->getFromEmail();
				$subject="Review Report For $today";
				$msg="<html>
				<p>Hi , Please download Review Report of date : $today <p>
				<table border='1'  style='border-collapse:collapse'>
					<tr>
						<th> Star Rating (MTD) </th>
						<th> Total Count </th>
					</tr>
					<tr>
						<td>1 star</td>
						<td>$onestar</td>
					</tr>	
					<tr>
						<td>2 star</td>
						<td>$twostar</td>
					</tr>	
					<tr>
						<td>3 star</td>
						<td>$threestar</td>
					</tr>	
					<tr>
						<td>4 star</td>
						<td>$fourstar</td>
					</tr>	
					<tr>
						<td>5 star</td>
						<td>$fivestar</td>
					</tr>
				</table>		
				<html>";
				//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);

				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/send_review_report');
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail(); 
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->getReviewEmail();
		 
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today,
		                'onestar' => $onestar,
		                'twostar' => $twostar,
		                'threestar' => $threestar,
		                'fourstar' => $fourstar,
		                'fivestar' => $fivestar,
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
			}
	}
	
	public function sendSettlementReport()
	{
		$query = 'SELECT * FROM om_settlement_report_partial ORDER BY order_num DESC';
		$datas = $this->connection->fetchAll($query);
		$columns = $this->getSettlementColumns();
		$csvHeader = array_values($columns);
		$filename = 'Settlement Report-'.date('Y-m-d-H-i-s').'.csv';
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
		foreach ($datas as $data) {
			$insertArr=$this->generateSettlementReportRow($data);
			fputcsv($fp, $insertArr, ",");
		}
		fseek($fp, 0);
		$file = stream_get_contents($fp);
		fclose($fp);
		
		if($file)
		{
			$today = date("l (d-m-Y)", strtotime($this->date));
			$to = $this->settlementReportEmail();
			$from = $this->panasonicHelper->getFromEmail();
			$subject="Settlement  Report For $today";	$msg="<html>
			<p>Hi , Please download Settlement  Report of date : $today <p>
			<html>";
			//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/settlement_report');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail();
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->settlementReportEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today,
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
		}
	}

	public function sendNewSellerEmail()
	{
		/* fecth todays created seller if any */
		$date = $this->date;
		$query = "SELECT * FROM `marketplace_userdata` where is_seller = '2'";
		$datas = $this->connection->fetchAll($query);
		$columns = $this->getNewSellerColumns();
		$csvHeader = array_values($columns);
		$filename = 'New Seller Report-'.date('Y-m-d-H-i-s').'.csv';
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
	
		/* no point of sending mail if no new user is  present  */
		if(!empty($datas) && is_array($datas))
		{
			foreach ($datas as $data) {
				$insertArr = $this->generateNewSellerReportRow($data);
				fputcsv($fp, $insertArr, ",");
			}
			
			fseek($fp, 0);
			$file = stream_get_contents($fp);
			fclose($fp);
			
			if($file)
			{
				$today = date("l (d-m-Y)", strtotime($this->date));
				$to = $this->newSellerEmail();
				$from = $this->panasonicHelper->getFromEmail();
				$subject="New Seller Report For $today";	$msg="<html>
				<p>Hi , Please download New Seller  Report of date : $today , Their  approvals has been pending .<p>
				<html>";
				//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/new_seller_email');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail();
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->newSellerEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today,
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
			}
		}
	}

	public function sendQuestionEmail()
	{
		$date = $this->date;
		/* fetch todays created seller if any */
		$query = "SELECT * FROM `wk_qaquestion` where date(created_at) = '$date'";
		$datas = $this->connection->fetchAll($query);
		$columns = ["Customer Name","Product Name","Subject Name","Question","Status"];
		$csvHeader = array_values($columns);
		$filename = 'Product Question Report-'.date('Y-m-d-H-i-s').'.csv';
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
		/* no point of sending mail if no data is  present  */
		if(!empty($datas) && is_array($datas))
		{
			foreach ($datas as $data) {
				$insertArr=array();
				$customer_id =  $data['buyer_id'];
				$query = "SELECT firstname,lastname FROM `customer_entity` where entity_id='$customer_id'";
				$customer_data = $this->connection->fetchAll($query);
				
				if(!empty($customer_data) && is_array($customer_data))
				{
					$firstname = $customer_data[0]['firstname'];
					$lastname = $customer_data[0]['lastname'];
					$customer_name = $firstname." ".$lastname;
				}
				$product_id =  $data['product_id'];

				$query = "SELECT * FROM `catalog_product_entity_varchar` where attribute_id=73 and entity_id='$product_id'";
				$product_name = $this->connection->fetchOne($query);

				$subject_name=$data['subject'];
				$question=$data['content'];
				$status=$data['status']==1?"Approved":"Disapproved";
				
				$insertArr[] = $customer_name;
				$insertArr[] = $product_name;
				$insertArr[] = $subject_name;
				$insertArr[] = $question;
				$insertArr[] = $status;

				fputcsv($fp, $insertArr, ",");
			}
			
			fseek($fp, 0);
			$file = stream_get_contents($fp);
			fclose($fp);
			
			if($file)
			{
				$today = date("l (d-m-Y)", strtotime($this->date));
				$to = $this->getQuestionEmail();
				$from = $this->panasonicHelper->getFromEmail();
				$subject="Product Question Report For $today";	$msg="<html>
				<p>Hi , Please download Product Question Report of date : $today <p>
				<html>";
				//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/question_email');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail();
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->getQuestionEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today,
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
			}
		}
	}

	public function AgingReportData()
	{
		$five30 = (5*60*60)+(30*60);
		$connection = $this->connection;
		$header1  = array('Aging by order date');
		$header2  = array('Aging by status');
		$agingArr = array('AGING');
		$newArr   = array('New');
		$procArr  = array('Processing');
		$shipArr  = array('Shipped');
		$totalArr = array('Grand Total');
		
		$gap1 =  array('');
		
		// Aging by order date
		for($a=0; $a<26; $a++){
			$agingArr[] = $a;
			$startdate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
			$startdate = date('Y-m-d H:i:s',$startdate);
			
			$enddate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
			$enddate = date('Y-m-d H:i:s',$enddate + 86399);
			
			$baseQuery = "select count(entity_id) from sales_order where created_at BETWEEN  '$startdate' AND '$enddate'";
			$newS  = $connection->fetchOne($baseQuery." AND status like '%new%' ");
			$proS  = $connection->fetchOne($baseQuery." AND status like '%processing%' ");
			$shipS = $connection->fetchOne($baseQuery." AND status like '%shipped%' ");
			$total = $newS + $proS + $shipS;
			$newArr[]   = $newS;
			$procArr[]  = $proS;
			$shipArr[]  = $shipS;
			$totalArr[] = $total;	
		}
		
		$baseQuery = "select count(entity_id) from sales_order where created_at > '$enddate'";
		$newS  = $connection->fetchOne($baseQuery." AND status like '%new%' ");
		$proS  = $connection->fetchOne($baseQuery." AND status like '%processing%' ");
		$shipS = $connection->fetchOne($baseQuery." AND status like '%shipped%' ");
		
		$total = $newS + $proS + $shipS;
		
		$agingArr[] = '>25';
		$newArr[]   = $newS;
		$procArr[]  = $proS;
		$shipArr[]  = $shipS;
		$totalArr[] = $total;
		
		
		$agingArr[] = 'Grand Total';
		$newArr[] = array_sum($newArr);
		$procArr[] = array_sum($procArr);
		$shipArr[] = array_sum($shipArr);
		$totalArr[] = array_sum($totalArr);
		
		// Aging by status
		$agingArr_1 = array('AGING');
		$newArr_1   = array('New');
		$procArr_1  = array('Processing');
		$shipArr_1  = array('Shipped');
		$totalArr_1 = array('Grand Total');
		
		for($a=0; $a<26; $a++){
			$agingArr_1[] = $a;
			$startdate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
			$startdate = date('Y-m-d H:i:s',$startdate);
			
			$enddate = strtotime( date('Y-m-d',( time()-($a*86400) ) + $five30) ) - $five30;
			$enddate = date('Y-m-d H:i:s',$enddate + 86399);
			
			$baseQuery = "select count(entity_id) from sales_order where created_at BETWEEN  '$startdate' AND '$enddate'";
			$newS  = $connection->fetchOne($baseQuery." AND status like '%new%' ");
			$proS  = $connection->fetchOne($baseQuery." AND status like '%processing%' ");
			$shipS = $connection->fetchOne($baseQuery." AND status like '%shipped%' ");
			
			$newS  = $connection->fetchOne("SELECT count(entity_id) from sales_order where (created_at BETWEEN  '$startdate' AND '$enddate') AND status like '%new%' ");
			$proS  = $connection->fetchOne("SELECT count(a.entity_id) FROM `sales_order` as a, `sales_shipment` as b WHERE a.entity_id=b.entity_id AND a.status like '%processing%' AND (b.created_at BETWEEN  '$startdate' AND '$enddate') ");
			$shipS = $connection->fetchOne("SELECT count(entity_id) from sales_order where (pickup_date BETWEEN  '$startdate' AND '$enddate') AND status like '%Shipped%' ");
		
		
			$total = $newS + $proS + $shipS;
			$newArr_1[]   = $newS;
			$procArr_1[]  = $proS;
			$shipArr_1[]  = $shipS;
			$totalArr_1[] = $total;	
		}
		
		$newS  = $connection->fetchOne("SELECT count(entity_id) from sales_order where created_at > '$enddate' AND status like '%new%' ");
		$proS  = $connection->fetchOne("SELECT count(a.entity_id) FROM `sales_order` as a, `sales_shipment` as b WHERE a.entity_id=b.entity_id AND a.status like '%processing%' AND b.created_at > '$enddate' ");
		$shipS = $connection->fetchOne("SELECT count(entity_id) from sales_order where pickup_date > '$enddate' AND status like '%Shipped%' ");
		
		$total = $newS + $proS + $shipS;
		
		$agingArr_1[] = '>25';
		$newArr_1[]   = $newS;
		$procArr_1[]  = $proS;
		$shipArr_1[]  = $shipS;
		$totalArr_1[] = $total;
		
		
		$agingArr_1[] = 'Grand Total';
		$newArr_1[] = array_sum($newArr_1);
		$procArr_1[] = array_sum($procArr_1);
		$shipArr_1[] = array_sum($shipArr_1);
		$totalArr_1[] = array_sum($totalArr_1);
		
		
		$totalD = $connection->fetchOne("select count(entity_id) from sales_order where status like '%delivered%'");
		$totalR = $connection->fetchOne("select count(entity_id) from sales_order where status like '%Refunded%'");
				
		$fullArr = [
			$header1,
			$agingArr,
			$newArr,
			$procArr,
			$shipArr,
			$totalArr,
			[],
			['Total Delivered',$totalD],
			['Total Refunded',$totalR],
			[],
			$header2,
			$agingArr_1,
			$newArr_1,
			$procArr_1,
			$shipArr_1,
			$totalArr_1
			
		];
		return $fullArr;
	}



	public function sendRmaReport()
	{
		$query = "SELECT * FROM `wk_rma_items`  as a left join wk_rma  as b  on a.rma_id=b.rma_id where `status` not in ('2','3','14')";
		$datas = $this->connection->fetchAll($query);

		$query = "SELECT SUM(row_total) FROM sales_order_item where item_id  in (select distinct item_id from `wk_rma_items` where rma_id in (SELECT rma_id FROM `wk_rma` where `status` in (0)))";
		$pending_amount = $this->connection->fetchOne($query);
		
		$query = 'SELECT count(*) FROM `wk_rma` where `status` in (0)  and year(curdate()) = year(created_at) and month(curdate()) = month(created_at) ';
		$pending_count = $this->connection->fetchOne($query);

		$columns = $this->getReturnColumns();
		$csvHeader = array_values($columns);
		$fp = fopen('php://memory', 'w');
		$filename = 'Return_Exchange_Requests_Report-'.date('Y-m-d-H-i-s').'.csv';
		fputcsv( $fp, $csvHeader,",");
		if(empty($datas))
		{
			return false;
		}
		foreach ($datas as $data) {
			$insertArr=$this->generateReturnReportRow($data);
			fputcsv($fp, $insertArr, ",");
		}
		fseek($fp, 0);
		$file = stream_get_contents($fp);
		fclose($fp);
		
		if($file)
		{
			$today = date("l (d-m-Y)", strtotime($this->date));
			$to = $this->getRmaEmail();
			$from = $this->panasonicHelper->getFromEmail();
			/*  */
			$query = 'SELECT count(*) FROM `wk_rma` where year(curdate()) = year(created_at) and month(curdate()) = month(created_at)';
			$total_rma = $this->connection->fetchOne($query);

			$query_accepted = 'SELECT count(*) FROM `wk_rma` where final_status in (3) and year(curdate()) = year(created_at) and month(curdate()) = month(created_at)';
			$rma_approved = $this->connection->fetchOne($query_accepted);

			$query_processing = 'SELECT count(*) FROM `wk_rma` where `status` not in (0,2,3,4,14) and year(curdate()) = year(created_at) and month(curdate()) = month(created_at)';
			$rma_processing = $this->connection->fetchOne($query_processing);


			$query_rejected = 'SELECT count(*) FROM `wk_rma` where final_status in (1,2)  and year(curdate()) = year(created_at) and month(curdate()) = month(created_at)';
			$rma_declined = $this->connection->fetchOne($query_rejected);
	

			$query = 'SELECT SUM(row_total) FROM sales_order_item where item_id  in (select distinct item_id from `wk_rma_items` where rma_id in (select rma_id  from wk_rma where year(curdate()) = year(created_at) and month(curdate()) = month(created_at)))';
			$total_rma_amount = $this->connection->fetchOne($query);

			$query = "SELECT SUM(row_total) FROM sales_order_item where item_id  in (select distinct item_id from `wk_rma_items` where rma_id  in (SELECT rma_id FROM `wk_rma` where final_status in (3) and year(curdate()) = year(created_at) and month(curdate()) = month(created_at) ))";
			$total_rma_accepted = $this->connection->fetchOne($query);
		
			$query = "SELECT SUM(row_total) FROM sales_order_item where item_id  in (select distinct item_id from `wk_rma_items` where rma_id  in (SELECT rma_id FROM `wk_rma` where `status` not in (2,3,4,14) and year(curdate()) = year(created_at) and month(curdate()) = month(created_at) ))";
			$total_rma_proessing = $this->connection->fetchOne($query);
		
			
			$query = "SELECT SUM(row_total) FROM sales_order_item where item_id  in (select distinct item_id from `wk_rma_items` where rma_id  in (SELECT rma_id FROM `wk_rma` where `status` in (0) and year(curdate()) = year(created_at) and month(curdate()) = month(created_at) ))";
			$total_rma_pending = $this->connection->fetchOne($query);
		
			$query = "SELECT SUM(row_total) FROM sales_order_item where item_id  in (select distinct item_id from `wk_rma_items` where rma_id  in (SELECT rma_id FROM `wk_rma` where final_status in (1,2) and year(curdate()) = year(created_at) and month(curdate()) = month(created_at)))";
			$total_rma_rejected = $this->connection->fetchOne($query);

			$total_amount_jabardasti_wala = $total_rma_accepted+$total_rma_rejected+$total_rma_pending+$total_rma_proessing;
			
			$total_rma_amount = $this->roundOff($total_rma_amount);
			$total_rma_accepted = $this->roundOff($total_rma_accepted);
			$total_rma_rejected = $this->roundOff($total_rma_rejected);
			$total_rma_pending = $this->roundOff($total_rma_pending);
			$total_amount_jabardasti_wala = $this->roundOff($total_amount_jabardasti_wala);
			

			$total_rma_amount = strip_tags($this->formatPrice($total_rma_amount));
			$total_rma_accepted = strip_tags($this->formatPrice($total_rma_accepted));
			$total_rma_rejected = strip_tags($this->formatPrice($total_rma_rejected));
			$total_rma_proessing = strip_tags($this->formatPrice($total_rma_proessing));
			$total_rma_pending = strip_tags($this->formatPrice($total_rma_pending));
			$total_amount_jabardasti_wala = strip_tags($this->formatPrice($total_amount_jabardasti_wala));

			$subject="Return/Exchange Requests Reports For $today";	
			$msg="<html>
			<p>Hi , Please download Return/Exchange Requests Report of date : $today .<p> 
			<h4> There are $pending_count pending Return/Exchange of  amount $total_rma_pending</h4> 
				<table border='1'  style='border-collapse:collapse'>
					<tr>
						<th>   </th>
						<th>Number</th>
						<th>Amount(IN INR)</th>
					</tr>
					<tr>
						<td><h4>Total Return/Exchange Requests MTD</h4></td>
						<td>$total_rma</td>
						<td>$total_amount_jabardasti_wala</td>
					</tr>
					<tr>
						<td><h4>Return/Exchange approved MTD</h4></td>
						<td>$rma_approved</td>
						<td>$total_rma_accepted</td>
					</tr>
					<tr>
						<td><h4>Prossing Return/Exchange</h4></td>
						<td>$rma_processing</td>
						<td>$total_rma_proessing</td>
					</tr>
					<tr>
					<td><h4>Pending Rmas</h4></td>
					<td>$pending_count</td>
					<td>$total_rma_pending</td>
				</tr>
					<tr>
						<td><h4>Return/Exchange Rejected MTD</h4></td>
						<td>$rma_declined</td>
						<td>$total_rma_rejected</td>
					</tr>
				</table>
			<html>";
			
			//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/send_rma_report');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail();
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->getRmaEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today,
		                'total_rma' =>$total_rma,
		                'total_rma_amount' =>$total_amount_jabardasti_wala,
		                'rma_approved' =>$rma_approved,
		                'total_rma_accepted' =>$total_rma_accepted,
		                'rma_declined' =>$rma_declined,
		                'total_rma_rejected' =>$total_rma_rejected,
		                'pending_amount' =>$total_rma_pending,
		                'pending_count'=>$pending_count,
		                'procssing_count'=>$rma_processing,
		                'processing_amount'=>$total_rma_proessing
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
		}
	}

	public function sendGstTcsReport()
	{
		$query = 'select * from om_gsttcs_report';
		$datas = $this->connection->fetchAll($query);
		$columns = $this->getGstTcsReportColumns();
		$filename = 'Gst_Tcs_Report-'.date('Y-m-d-H-i-s').'.csv';
		$csvHeader = array_values($columns);
		$fp = fopen('php://memory', 'w');
		fputcsv( $fp, $csvHeader,",");
		foreach ($datas as $data) {
			$insertArr=$this->generatGstTcsReportRow($data);
			fputcsv($fp, $insertArr, ",");
		}
		fseek($fp, 0);
		$file = stream_get_contents($fp);
		fclose($fp);
		
		if($file)
		{
			$today = date("l (d-m-Y)", strtotime($this->date));
			$to = $this->gstReportEmail();
			$from = $this->panasonicHelper->getFromEmail();
			$subject="Gst Tcs Report For $today";	$msg="<html>
			<p>Hi , Please download Gst Tcs  Report of date : $today <p>
			<html>";
			//$this->sendEmailWithAttachment($to,$from,$msg,$subject,$file,$filename);
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		        $selectedTemplateId = $scopeConfig->getValue('custom_template/email/gst_tcs_report');
		        $templateId = $selectedTemplateId;
		        $fromEmail = $this->panasonicHelper->getFromEmail();
		        $fromName = 'GetMySpares'; 
		        $toEmail = $this->gstReportEmail();
		        $transportBuilder = $objectManager->create('OM\CustomEmailTemplates\Model\Mail\Template\TransportBuilder');
		        try {
		            // template variables pass here
		            $templateVars = [
		                'today' => $today,
		            ];
		 
		            $storeId = $storeManager->getStore()->getId();
		 
		            $from = ['email' => $fromEmail, 'name' => $fromName];
		            $this->_inlineTranslation->suspend();
		 
		            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		            $templateOptions = [
		                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
		                'store' => $storeId
		            ];
		            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($toEmail)
		                ->addAttachment($file, $filename, 'application/octet-stream')
		                ->getTransport();
		            $transport->sendMessage();
		            $this->_inlineTranslation->resume();
		        } catch (\Exception $e) {
		            echo 'error'.$e->getMessage();
		            //$this->logger->info($e->getMessage());
		        }
		}
	
	}

	public function generatGstTcsReportRow($data)
	{
		$columns	 = $this->getGstTcsReportColumns();
		$insertArr = array();
		foreach($columns as $key => $column){
			if($key=='order_inc_id' or $key=='invoice_no'){
				$insertArr[] = "\t".$data[$key];
			} else {
				$insertArr[] = $data[$key];
			}
		}
		return $insertArr;
	}

	public function generateNewSellerReportRow($data)
	{
		$columns	 = $this->getNewSellerColumns();
		$insertArr = array();
		$seller_id = $data['seller_id'];
		$selller_name="";
		$seller_email="";
		$query = "SELECT firstname,lastname,email FROM `customer_entity` where entity_id='$seller_id'";
		$customer_data = $this->connection->fetchAll($query);
		$phone="";
		$postcode="";
		$region="";
		$country="";
		$customer = $this->_customer->getById($seller_id);
		$firstname = $customer->getFirstname();
		$lastname = $customer->getLastname();
		$created_at = $customer->getCreatedAt();
		$seller_email = $customer->getEmail();
		$selller_name = $firstname." ".$lastname;
		$billling_address_id = $customer->getDefaultBilling();
		if(!empty($billling_address_id))
		{
			$billling_address = $this->addressInterface->getById($billling_address_id);
			$phone  = $billling_address->getTelephone();
			$postcode  = $billling_address->getPostcode();
			$region  = $billling_address->getRegion()->getRegion();
			$country  = $billling_address->getCountryId();
		}
		
		$insertArr[] = $selller_name;
		$insertArr[] = $seller_email;
		$insertArr[] = $phone;
		$insertArr[] = $postcode;
		$insertArr[] = $country;
		$insertArr[] = $region;
		$insertArr[] = $created_at;
		return $insertArr;
	}
	

	public function generateSettlementReportRow($data)
	{
		$columns	 = $this->getSettlementColumns();
		$insertArr = array();
		foreach($columns as $key => $column){
			if($key=='order_num' or $key=='invoice_ref_num'){
				$insertArr[] = "\t".$data[$key];
			} elseif($key=='order_date'){
				$insertArr[] = date("Y-m-d", (strtotime($data[$key])+19800) ); 	
			} else {
				$insertArr[] = $data[$key];
			}
			
		}
		return $insertArr;
	}

		/*---------need to move to seperate class */
	public function getReturnColumns()
	{
		$columns = array(
			'name'=>'Customer Name',
			'increment_id'=>'Order ID',
			'final_status'=>'Return Status',
			'resolution_type'=>'Resolution Type',
			'rma_reason'=>'Reason',

			'acknowledge_date'=>'Seller Acknowledgement Date',
			'seller_resolution'=>'Seller Resolution',
			'tracking_status'=>'Tracking Status',
			'tracking_status_date'=>'Tracking Status Date',
			'reverse_awb'=>'Reverse AWB Number',
			'forward_awb_number'=>'Forward AWB Number',

			'additional_info'=>'Additional info',
			'product_name'=>'Product  Name',
			'product_price'=>'Product Price',
			'product_qty'=>'Product  Qty',
			'grand_total'=>'Grand Total (excluding shipping)',
			'customer_mobile'=>'Customer Mobile',
			'created_at'=>'Created At',
			'product_seller'=>'Product Seller',
			'seller_remark'=>'Seller Remark',
			'approval_date'=>'Aprroved Date',
			'decline_date'=>'Declined Date',
		);
		return $columns;
	}

	public function generateReturnReportRow($data)
	{
		$insertArr = array();
		$Customer_Name = $data['name'];
		$Order_ID = $data['increment_id'];
		$status = $data['status'];
		$Return_Status = $this->rmaHelper->getRmaStatusTitle($status);
		$Resolution_Type = ($data['resolution_type'] ==1 )? 'Exchange' : "Refund";
		$Reason = $data['rma_reason'];
		$Additional_info = $data['additional_info'];
		$Product_quantity = $data['qty'];
		$Product_Name = "";
		$Product_Price = "";
		$Product_Seller="";
		$grand_total="";


		$item_id = $data['item_id'];
		$query = "Select name,price,product_id  from  sales_order_item where item_id='$item_id'";
		$p_data = $this->connection->fetchAll($query);
		if(!empty($p_data) && is_array($p_data))
		{
			$Product_Name = $p_data[0]['name']; 
			$Product_Price = $p_data[0]['price']; 
			$product_id = $p_data[0]['product_id'];

			$grand_total =  $Product_quantity*$Product_Price;
			
			$query = "SELECT `seller_id` FROM `marketplace_product` where  mageproduct_id='$product_id'";
			$seller_id = $this->connection->fetchOne($query);
			
			$query = "SELECT shop_url FROM `marketplace_userdata` where seller_id='$seller_id'";
			$Product_Seller = $this->connection->fetchOne($query);
		}

		$customer_id = $data['customer_id'];
		$query = "Select `value` from customer_entity_varchar where  entity_id='$customer_id' and attribute_id='178'";
		$Customer_Mobile = $this->connection->fetchOne($query);
		
		$Created_At = $data['created_at'];
		
		$Seller_Remark = $data['seller_remark'];
		$Aprroved_Date = $data['approved_date'];
		$Declined_Date = $data['declined_date'];


		$acknowledge_date = $data['acknowledge_date'];
		$seller_resolution = $this->rmaHelper->getRemarkForCustomerById($data['remark_for_customer']);
		$tracking_status = $this->rmaHelper->getRmaStatusTitle($data['status']);
		$tracking_status_date = $data['status_modified_date'];
		
		$forward_awb_number = $data['customer_consignment_no'];
		$reverse_awb = $data['admin_consignment_no'];

		$insertArr[] = $Customer_Name;
		$insertArr[] = $Order_ID;
		$insertArr[] = $Return_Status;
		$insertArr[] = $Resolution_Type;
		$insertArr[] = $Reason;
		
		$insertArr[] = $acknowledge_date;
		$insertArr[] = $seller_resolution;
		$insertArr[] = $tracking_status;
		$insertArr[] = $tracking_status_date;
		$insertArr[] = $reverse_awb;
		$insertArr[] = $forward_awb_number;


		$insertArr[] = $Additional_info;
		$insertArr[] = $Product_Name;
		$insertArr[] = $Product_Price;
		$insertArr[] = $Product_quantity;
		$insertArr[] = $grand_total;
		$insertArr[] = $Customer_Mobile;
		$insertArr[] = $Created_At;
		$insertArr[] = $Product_Seller;
		$insertArr[] = $Seller_Remark;
		$insertArr[] = $Aprroved_Date;
		$insertArr[] = $Declined_Date;

		return $insertArr;
	}

/*---------need to move to seperate class ends  */

	public function generateSalesReportRow($data)
	{
		$connection = $this->connection;
		$columns = $this->getSalesColumns();
		$insertArr = array();
		foreach($columns as $key => $column){
			if($key=='order_increment_id' or $key=='invoice_increment_id'){
				$insertArr[] = "\t".$data[$key];
				
			} elseif($key=='delivery_date' || $key=='pickup_date')
			{	if(!empty($data[$key]))
				{
					$insertArr[] = date("Y-m-d", (strtotime($data[$key])+19800)); 
				} else 
				{
					$insertArr[] ="";
				}
			}
			elseif($key=='order_status') {
				$type = $data['report_type'];
				$order_status=$data['order_status'];
				if($type==1)
				{
					$order_id = $data['order_id'];
					$query1="SELECT `order_id`  FROM `wk_rma` WHERE `order_id` = '$order_id'";
					$order_rma = $connection->fetchOne($query1);
					if($order_rma)
					{
						$item_id = $data['item_id']; 
						$query2="SELECT `id`  FROM `om_sales_report` WHERE `report_type` = '7' and item_id='$item_id'";
						$entity_id_adadasd = $connection->fetchOne($query2);
						if(!empty($entity_id_adadasd))
						{
							$order_status="Refund";
						} else 
						{
							$order_status =  "Delivered";
						}	
					}
				}
				$insertArr[]=$order_status;
			}
			else {
				$insertArr[] = $data[$key];
			}
		}
		return $insertArr;
	}


	public function generateSalesSellerSettleReportRow($data)
	{
		$connection = $this->connection;
		$columns = $this->getSellerSettlementColumns();
		$insertArr = array();
		foreach($columns as $key => $column){
			if($key=='order_increment_id' or $key=='invoice_increment_id'){
				$insertArr[] = "\t".$data[$key];
			} elseif($key=='order_date' || $key=='invoice_date' || $key=='shipping_date' || $key=='delivery_date' || $key=='cancellation_date' || $key=='return_request_date' || $key=='pickup_date' )
			{	if(!empty($data[$key]))
				{
					$insertArr[] = date("Y-m-d", (strtotime($data[$key])+19800) ); 
				} else 
				{
					$insertArr[] ="";
				}
			} elseif($key=='part_code')
			{
				$item_id = $data['item_id'];
				$query1="SELECT `product_id`  FROM `sales_order_item` WHERE `item_id` = '$item_id'";
				$entity_id = $connection->fetchOne($query1);
				$query="SELECT `value` FROM `catalog_product_entity_varchar` where attribute_id='197' and entity_id='$entity_id'";
				$item_model_number = $connection->fetchOne($query);
				$insertArr[]=$item_model_number;
			}
			elseif($key=='mobile_number') {
				$order_increment_id = $data['order_increment_id'];
				$query1="SELECT `entity_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
				$entity_id = $connection->fetchOne($query1);
				$query="SELECT `telephone` FROM `sales_order_address` where address_type='shipping' and parent_id='$entity_id'";
				$telephone = $connection->fetchOne($query);
				$insertArr[]=$telephone;
			}	elseif($key=='order_comment') {
				$order_increment_id = $data['order_increment_id'];
				$query1="SELECT `entity_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
				$entity_id = $connection->fetchOne($query1);
				$query =  "SELECT `comment` FROM `sales_order_status_history` where parent_id='$entity_id' and entity_name='order' and comment is not  null";
				$comments = $connection->fetchAll($query);
				$comment_array = array();
				if(!empty($comments))
				{
					foreach ($comments as $comment) {
						$comment_array[] = $comment['comment'];
					}
				}
				$comments_string = implode(" | | ",$comment_array);
				$insertArr[]=$comments_string;
			}

			elseif($key=="sub_order_num"){
							
								
								$order_increment_id = $data['order_increment_id'];
								$query1="SELECT `increment_id`  FROM `sales_order` WHERE `increment_id` = '$order_increment_id'";
								$entity_id = $connection->fetchAll($query1);
								if($orderIncrementId == $entity_id[0]['increment_id']){
									$count++;
								}
								else{
									$orderIncrementId = $entity_id[0]['increment_id'];
									$count = 1;
								}
								$insertArr[] = $order_increment_id.'-'.$count;
							}

							elseif($key=="sales_return_confirmation_date"){
							
								$dateVal ='';
								$creditmemo_increment_id = $data['creditmemo_increment_id'];
								$order_increment_id = $data['order_increment_id'];
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
								$orderCollection = $objectManager->create('Magento\Sales\Model\Order'); 
								$order = $orderCollection->loadByIncrementId($order_increment_id);
								$orderId = $order->getId();
								if(count($creditmemo_increment_id)){
									$query1="SELECT DATE_FORMAT(`created_at`, '%Y-%m-%d') as created_at  FROM `sales_creditmemo` WHERE `order_id` = '$orderId'";
									$dateData = $connection->fetchAll($query1);
									
									if($dateData){
									$strip = $dateData[0]['created_at']; 
								}	
							}
								else{
								$query1="SELECT DATE_FORMAT(`created_at`, '%Y-%m-%d') as created_at  FROM `sales_invoice` WHERE `order_id` = '$orderId'";
								$dateData = $connection->fetchAll($query1);	
								if($dateData){
									$strip = $dateData[0]['created_at']; 
									
								}
								}
							$insertArr[] = $strip;
							}

							elseif($key=="transaction_completion_date"){
							
								$transactionDate = '';
								$order_increment_id = $data['order_increment_id'];
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
								$orderCollection = $objectManager->create('Magento\Sales\Model\Order'); 
								$order = $orderCollection->loadByIncrementId($order_increment_id);
								$orderId = $order->getId();

								$query="SELECT `transaction_completion_date`  FROM `om_settlement_report` WHERE `order_id` = '$orderId'";
								
								$trasactionDateData = $connection->fetchAll($query);
								
								if(count($trasactionDateData) > 0){
									$transactionDate = $trasactionDateData[0]['transaction_completion_date'];
								}
								else{
									$transactionDate = '';
							}
							$insertArr[] = $transactionDate;
							}

							elseif($key=="expected_payment_date"){							
								$paymentDate = '';
								$order_increment_id = $data['order_increment_id'];
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
								$orderCollection = $objectManager->create('Magento\Sales\Model\Order'); 
								$order = $orderCollection->loadByIncrementId($order_increment_id);
								$orderId = $order->getId();
								$query="SELECT DATE_FORMAT(`transaction_completion_date`, '%Y-%m-%d') as transaction_completion_date  FROM `om_settlement_report` WHERE `order_id` = '$orderId'";
								$trasactionDateData = $connection->fetchAll($query);
								if(count($trasactionDateData) > 0){
									$paymentDate = $trasactionDateData[0]['transaction_completion_date'];
									$paymentDate = date('Y-m-d', strtotime($paymentDate. ' + 3 days'));
								}
								else{
									$paymentDate = '';
								}

								$insertArr[] = $paymentDate;
							}

							elseif($key=="gst_on_comm_18_percent"){
							 
								$comission = $data['comission'];
								$comissionGst = $comission*0.18;
							$insertArr[] = $comissionGst;
							}
							elseif($key=="net_commission"){
							
								$comission = $data['comission'];
								$comissionGst = $comission*0.18;
								$netCommsion = $comission + $comissionGst;
								$insertArr[] = $netCommsion;
							}

							elseif($key=="tds_on_pi_comission"){
								$comission = $data['comission'];
								$tdsComission = $comission*0.0375;
								$insertArr[] = $tdsComission;
							}	

							elseif($key=="gst_tcs_1_on_basic_sale_amt"){
								$netSalevalue = $data['net_sales_value'];
								$netValue = $netSalevalue*0.01;
								$insertArr[] = $netValue;
							}	

							elseif($key=="due_from_seller_to_pi"){
							
								$comission = $data['comission'];
								$comissionGst = $comission*0.18;
								$netCommsion = $comission + $comissionGst;
								$netSalevalue = $data['net_sales_value'];
								$netValue = $netSalevalue*0.01;
								$tdsComission = $comission*0.0375;
								$due = $netCommsion + $netValue - $tdsComission;
								$insertArr[] = $due;
							}
								elseif($key=="amount_due_to_seller"){
								 
								$comission = $data['comission'];
								$comissionGst = $comission*0.18;
								$netCommsion = $comission + $comissionGst;
								$netSalevalue = $data['net_sales_value'];
								$netValue = $netSalevalue*0.01;
								$tdsComission = $comission*0.0375;
								$gst = $data['gst'];
								$itemval = $netSalevalue + $gst;
								$amountDue = $itemval - $netCommsion - $netValue + $tdsComission;
								$insertArr[] = $amountDue;
							}
							
							elseif($key=="refunded_amount"){
							
								$itemRefunded = '';
								$creditId = '';
								$creditmemo_increment_id = $data['creditmemo_increment_id'];
								if($creditmemo_increment_id){
								$query1="SELECT `entity_id`  FROM `sales_creditmemo` WHERE `increment_id` = '$creditmemo_increment_id'";
								$entity_id = $connection->fetchAll($query1);

								$creditId = $entity_id[0]['entity_id'];
								if($creditId){
								$creditQuery = "SELECT `base_price` FROM `sales_creditmemo_item` WHERE `parent_id` = $creditId";
								$creditQueryResults = $connection->fetchAll($creditQuery);
								$itemRefunded = $creditQueryResults[$this->counter]['base_price'];
								$this->counter++;
							}
							}else{
								$this->counter = 0;
								$orderCreditId = $creditmemo_increment_id;
							}
							$insertArr[] = $itemRefunded;
							}
			else {
				$insertArr[] = $data[$key];
			}
		}
		return $insertArr;
	}



	public function getNewSellerColumns()
	{
		$columns = array(
			'name'=>'Seller Name',
			'email'=>'Seller Email',
			'phone'=>'Phone',
			'zip'=>'Zip',
			'country'=>'Country',
			'state'=>'State/Province',
			'seller_since'=>'Seller Since',
		); 
		return $columns;
	}
	
	public function getGstTcsReportColumns(){
		$columns = array(
		'invoice_no'=>'Invoice No',
		'suppliers_gstin'=>"Supplier's GSTIN",
		'register_unregister_supplier'=>'Register/Unregister Supplier',
		'state_of_supplier'=>'State of supplier',
		'gms_ecommerce_gstin'=>'GMS E-Commerce GSTIN',
		'state_of_e_commerce_gstin'=>'State of E-Commerce GSTIN',
		'type_of_transaction'=>'Type of transaction',
		'type_of_document'=>'Type of Document',
		'state_of_customer'=>'State Of Customer',
		'customer_gstin'=>'Customer  Gstin',
		'is_customer_registered'=>'Is Customer  Registered',
		'original_gstin_of_the_supplier'=>'Original GSTIN of the Supplier',
		'original_invoice_no'=>'Original Invoice No',
		'original_invoice_date'=>'Original Invoice Date',
		'document_number_internal_invoice_no'=>'Document Number(Internal Invoice No.)',
		'document_date'=>'Document Date',
		'posting_date'=>'Posting Date',
		'place_of_supply'=>'Place of Supply',
		'type_of_supply'=>'Type of Supply',
		'description_of_goods_services_supplied'=>'Description of goods / services supplied',
		'hsn_sac_code'=>'HSN  SAC code',
		'unit_of_measurement'=>'Unit of measurement',
		'number_of_units'=>'Number of units',
		'gross_value_of_supplies_made'=>'Gross Value of Supplies Made',
		'value_of_supplies_returned'=>'Value of Supplies Returned',
		'net_amount_liable_for_tcs'=>'Net Amount liable for TCS',
		'igst_tcs_rate'=>'IGST TCS Rate',
		'igst_amount_tcs'=>'IGST Amount(TCS)',
		'cgst_tcs_rate'=>'CGST TCS Rate',
		'cgst_amount_tcs'=>'CGST Amount(TCS)',
		'sgst_tcs_rate'=>'SGST TCS Rate',
		'sgst_amount_tcs'=>'SGST Amount(TCS)',
		'utgst_tcs_rate'=>'UTGST TCS Rate',
		'utgst_amount_tcs'=>'UTGST Amount(TCS)',
		'total_gst_tcs_amount'=>'Total GST (TCS) Amount'
		); 
		return $columns;
	}

	public function getSettlementColumns()
	{
		$columns = array(
			'order_num'=>'Order Num',
			'order_date'=>'Order Date',
			'buyer_id'=>'Buyer Id',
			'order_type'=>'Order Type',
			'payment_id'=>'Payment Id',
			'invoice_ref_num'=>'Invoice Ref Num',
			'invoice_date'=>'Invoice Date',
			'seller_code'=>'Seller Code',
			'seller_name'=>'Seller Name',
			'delivery_date'=>'Delivery Date',
			'sales_return_confirmation_date'=>'Sales/Return confirmation Date',
			'transaction_completion_date'=>'Transaction Completion Date',
			'basic_sale_amount'=>'Basic Sale Amount(ex-GST)',
			'nature_of_gst'=>'Nature of GST',
			'igst_sgst_ugst_amount'=>'IGST/SGST/ UGST Amount',
			'listed_price'=>'Listed price',
			'shipping_charges_basic'=>'Shipping charges(basic)',
			'shipping_charges_gst'=>'Shipping charges(GST)',
			'rate_percentage_shipping'=>'Rate%',
			'igst_shipping'=>'IGST',
			'cgst_shipping'=>'CGST',
			'sgst_shipping'=>'SGST',
			'utgst_shipping'=>'UTGST',
			'shipping_charges_total'=>'Shipping charges(Total)',
			'total_sale_invoice_value'=>'Total sale (Invoice value)',
			'amount_received_in_payment_gateway'=>'Amount received in Payment Gateway',
			'payment_method'=>'Payment Method',
			'card_type'=>'Card Type',
			'card_network'=>'Card Network',
			'payment_gateway_pg_charges'=>'Payment Gateway PG charges',
			'gst_on_pg_charges'=>'GST on PG charges',
			'rate_percent_razor'=>'Rate%',
			'igst_razor'=>'IGST',
			'cgst_razor'=>'CGST',
			'sgst_razor'=>'SGST',
			'utgst_razor'=>'UTGST',
			'payment_gateway_charges_invoice_value'=>'Payment Gateway charges invoice value',
			'razorpay_tds'=>'TDS',
			'payment_gateway_charges_net_of_tds'=>'Payment Gateway Charges net of TDS',
			'net_amount_received_in_nodal_account'=>'Net Amount received in nodal account',
			'nodal_charges_monthly'=>'Nodal charges(monthly)',
			'balance_in_nodal_first'=>'Balance in nodal',
			'logistic_tds'=>'Logistic TDS',
			'gst_tcs_1_on_supplies_to_deposited_by_pi'=>'GST TCS @1% on Supplies <br>by Logistic Co and <br>collected & to be <br>deposited by PI' ,
			'amount_due_to_logistics'=>'Amount due to logistics'  ,
			'balance_in_nodal_second'=>'Balance in nodal',
			'gross_comm_due'=>'Gross comm due',
			'gst_on_comm_18_percent'=>'GST on comm @ 18%',
			'net_commission'=>'Net commission',
			'gst_tcs_1_on_basic_sale_amt'  => 'GST TCS @1% on basic sale amt',
			'tds_on_pi_comission'=>'TDS on PI Comission',
			'due_from_seller_to_pi'=>'Due from Seller to PI',
			'amount_due_to_pi'=>'Amount due to PI',
			'balance_to_be_in_nodal_if_paid_to_pi'=>'Balance to be in nodal if paid to PI',
			'amount_due_to_seller'=>'Amount due to seller',
			'refunded_amount'=>'Refunded Amount',
			'tds_for_seller'=>'TDS for seller ',
			'due_to_seller_after_1tds'=>'Amount due to seller after 1% TDS'
		); 
		return $columns;
	}

	public function getSellerSettlementColumns()
	{
		$columns = array(
			'order_increment_id'=>'Order Num',
			'sub_order_num' => 'Sub Order Num',
			'order_status' => 'Order Status',
			'order_date' => 'Order Date',
			'order_type' => 'Order Type',
			'order_comment'=>'Order Comment',
			'awb_number' => 'Awb Number',
			'payment_ref_num' => 'Payment Ref Num',
			'invoice_increment_id' => 'Invoice Ref Num',
			'invoice_date' => 'Invoice Date',
			'shipping_date' => 'Shipping Date',
			'pickup_date' => 'Pickup Date',
			'delivery_date' => 'Delivery Date',
			'cancellation_date' => 'Cancellation Date',
			'return_request_date' => 'Return request  Date',
			'return_request_reason' => 'Return request reason',
			'sales_return_confirmation_date'=>'Sales/Return confirmation Date',
			'insurance_claimed'=>'Insurance claimed',
			'insurance_ref_num '=>'Insurance Ref Num',
			'seller_code' => 'Seller Code', 
			'seller_name' => 'Seller Name',
			'seller_address' => 'Seller address',
			'seller_city' => 'Seller city',
			'seller_state' => 'Seller state',
			'seller_pincode' => 'Seller pincode',
			'buyer_id' => 'Buyer ID',
			'billing_name' => 'Billing name',
			'mobile_number'=>'Mobile  Number',
			'billing_address' => 'Billing address',
			'billing_city' => 'Billing city',
			'billing_state' => 'Billing state',
			'billing_pincode' => 'Billing pincode',
			'shipping_name' => 'Shipping name',
			'shipping_address' => 'Shipping address',
			'shipping_city' => 'Shipping city',
			'shipping_state' => 'Shipping state',
			'shipping_pincode' => 'Shipping pincode',
			'product_code' => 'Product Code',
			'product_category' => 'Product Category ',
			'bp_category' => 'BP Category ',
			'item' => 'Item',
			'item_description' => 'Item Description',
			'hsn_code' => 'HSN code',
			'product_gst_rate' => 'Product GST rate',
			'product_comission_category' => 'Product Comission Category', 
			'product_comission_percentage' => 'Product Comission(%)',
			'quantity' => 'Quantity', 
			'unit_of_measurement' => 'Unit of measurement', 
			'currency' => 'Currency',
			'gross_price_amt_inr' => 'Gross Price Amt(INR)',
			'discount_percentage' => 'Discount%',
			'type_of_discount' => 'Type of Discount',
			'coupon_number' => 'Coupon Number',
			'coupon_amount' => 'Coupon Amount',
			'total_discount' => 'Total Discount',
			'net_sales_value' => 'Net Sales value',
			'gst' => 'GST', 
			'product_invoice_value' => 'Product Invoice value',
			'shipping_gst_rate' => 'Shipping GST rate', 
			'shipping' => 'Shipping Amount',
			'shipping_gst' => 'Shipping GST Amount',
			'shipping_invoice_total' => 'Shipping Invoice Total',
			'total_invoice_value' => 'Total Invoice Value',
			'o_created_by' => 'Order Created By',
			'comission' => 'Comission',
			'gst_on_comm_18_percent'=>'GST on comm',
			'net_commission'=>'Net commission',
			'gst_tcs_1_on_basic_sale_amt'  => 'GST TCS @1% on basic sale amt',
			'tds_on_pi_comission'=>'TDS on PI Comission',
			'due_from_seller_to_pi'=>'Total Due from Seller to PI',
			'amount_due_to_seller'=>'Seller settlement amount',
			'refunded_amount'=>'Refunded To Customer',
			'transaction_completion_date'=>'Transaction Completion Date',
			'expected_payment_date' => 'Expected Payment Date'

					); 
		return $columns;
	}

	public function getSalesColumns(){
		return [
			'order_increment_id' => 'Order Num',
			'order_status' => 'Order Status',
			'movement_type' => 'Movement type',
			'order_date' => 'Order Date',
			'order_type' => 'Order Type',
			'awb_number' => 'Awb Number',
			'payment_ref_num' => 'Payment Ref Num',
			'invoice_increment_id' => 'Invoice Ref Num',
			'invoice_date' => 'Invoice Date', 
			'return_request_date' => 'Return request  Date', 
			'return_request_reason' => 'Return request reason', 
			'seller_code' => 'Seller Code', 
			'pickup_date' => 'Pickup Date', 
			'seller_code' => 'Seller Code',
			'seller_name' => 'Seller Name',
			'seller_address' => 'Seller address',
			'seller_city' => 'Seller city',
			'seller_state' => 'Seller state',
			'seller_pincode' => 'Seller pincode',
			'buyer_id' => 'Buyer ID',
			'billing_name' => 'Billing name',
			'billing_address' => 'Billing address',
			'billing_city' => 'Billing city',
			'billing_state' => 'Billing state',
			'billing_pincode' => 'Billing pincode',
			'shipping_name' => 'Shipping name',
			'shipping_address' => 'Shipping address',
			'shipping_city' => 'Shipping city',
			'shipping_state' => 'Shipping state',
			'shipping_pincode' => 'Shipping pincode',
			'product_code' => 'Product Code',
			'product_category' => 'Product Category ',
			'bp_category' => 'BP Category ',
			'item' => 'Item',
			'item_description' => 'Item Description',
			'hsn_code' => 'HSN code',
			'product_gst_rate' => 'Product GST rate',
			'product_comission_category' => 'Product Comission Category', 
			'product_comission_percentage' => 'Product Comission(%)',
			'quantity' => 'Quantity', 
			'unit_of_measurement' => 'Unit of measurement', 
			'currency' => 'Currency',
			'gross_price_amt_inr' => 'Gross Price Amt(INR)',
			'discount_percentage' => 'Discount%',
			'type_of_discount' => 'Type of Discount',
			'coupon_number' => 'Coupon Number',
			'coupon_amount' => 'Coupon Amount',
			'total_discount' => 'Total Discount',
			'net_sales_value' => 'Net Sales value',
			'gst' => 'GST', 
			'product_invoice_value' => 'Product Invoice value', 		
			'shipping_gst_rate' => 'Shipping GST rate', 
			'shipping' => 'Shipping',
			'shipping_gst' => 'Shipping GST',
			'shipping_invoice_total' => 'Shipping Invoice Total',
			'total_invoice_value' => 'Total Invoice Value',
			'comission' => 'Comission',
			'shipping_date' => 'Shipping Date',
			'delivery_date' => 'Delivery Date',
			'cancellation_date' => 'Cancellation Date',
			'part_code'=>'Part Code',
			'o_created_by' => 'Order Created By',
			'mobile_number'=>'Mobile  Number',
			'order_comment'=>'Order Comment',
			'reverse_awb'=>'Reverse Awb',
			'exchange_awb'=>'Exchaneg Awb',
			'exchange_awb_delivery_date'=>'Exchange Order Delivery Date',
			'customer_gst_num'=>'Customer Gst Number',
			'crm_res_id' => 'CRM Order ID',	
			'sap_res_id' => 'SAP Order ID',
			'refund_rrn' => 'Refund Rrn/Arn'
		];
	}

	public  function sendEmailWithAttachment($to,$from,$msg,$subject,$attachment,$attachment_name)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$attachment=base64_encode($attachment);
			$to_actual_array = array();
			if(is_array($to))
			{
				/* need array unique as sendgrid do not accept request if emails are duplicated for  any reason */
				$to=array_unique($to);
				foreach ($to as $to_emails)
				{
					$to_actual_array[]=array('email' => $to_emails);
				}
			} else 
			{
				$to_actual_array[]=array('email' => $to);
			}
			/* cannot get this from configuration as i am using the latest v3 version  @ritesh10march2021*/
			$sendgridurl = 'https://api.sendgrid.com/v3/mail/send';
			$token_encrypted = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('smtp/configuration_option/password');
			$_encryptor = $objectManager->create('\Magento\Framework\Encryption\EncryptorInterface');
			$token = $_encryptor->decrypt($token_encrypted);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $sendgridurl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$fied_array =array (
				'personalizations' => array (0 => array(
						'to' =>$to_actual_array
						),
					),
				'from' => 
					array (
						'email' => $from,
					),
				'subject' => $subject,
				'content' => array (0 => 
					array (
						'type' => 'text/html',
						'value' => "<html>$msg</html>",
					),
				),
				'attachments' => array (0 => 
					array (
						'type' => 'text/csv',
						"content"=> "$attachment",
						"filename"=>"$attachment_name",
					),
				)
			);
			$fied_array_json = json_encode($fied_array);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fied_array_json);
			$headers = array();
			$headers[] = "Authorization: Bearer $token";
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$result = curl_exec($ch);
			if(!empty($result))
			{
				/* error  in api reports */
			}
			curl_close($ch);
		}
	
		public function getTypeLabel($customer_id)
    {
        if (!empty($customer_id)) {
            return __('Customer');
        }

        if (isset($item['store_id']) && $item['store_id'] == \Magento\Store\Model\Store::DEFAULT_STORE_ID) {
            return __('Administrator');
        }

        return __('Guest');
    }

    public function getStatus($status_id)
    {
        if($status_id==1)
        {
            return "Approved";
        }
        if($status_id==2)
        {
            return "Pending";
        }
        if($status_id==3)
        {
            return "Not Approved";
        }
        return "Not Known";
    }

    public function getProduct($product_id)
    {
        $prod=array();       
        $product = $this->_product->load($product_id);       
        $prod['sku'] = $product->getSku();
        $prod['name'] = $product->getName();
        $prod['price'] = $product->getPrice();
				$stockItem = $product->getExtensionAttributes()->getStockItem();
        $prod['qty'] = $stockItem->getQty();
        return $prod;
		}

		public function reviewColumns()
    {
        return ["Id",'Title','Nickname','Review','Rating','Status','Type','Product','Sku','Created At']; 
		}

		public function generateReviewReportRow($item)
    {
			
			$review_id = $item->getData('review_id');
			$entity_pk_value = $item->getData('entity_pk_value');
			$itemData = [];
			$status_id = $item->getData('status_id');
			$itemData[] = $review_id;
			$itemData[] = $item->getData('title');
			$itemData[] = $item->getData('nickname');
			$itemData[] = $item->getData('detail');
			/* rating calculation  */
			$query = "SELECT `value` FROM `rating_option_vote` where review_id='$review_id' and entity_pk_value='$entity_pk_value' limit 1";
			$rating_star = $this->connection->fetchOne($query);
			
			$itemData[] = $rating_star;
			$itemData[] = $this->getStatus($status_id);
			$itemData[] = $this->getTypeLabel($item->getData('customer_id'));
			$itemData[] = $this->getProduct($item->getData('entity_pk_value'))['name'];
			$itemData[] = $this->getProduct($item->getData('entity_pk_value'))['sku'];
			$itemData[] = $item->getData('created_at');
			return $itemData;
		}
}
