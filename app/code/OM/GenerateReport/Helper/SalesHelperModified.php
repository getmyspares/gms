<?php

declare(strict_types=1);

namespace OM\GenerateReport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class SalesHelperModified extends AbstractHelper
{
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface
    ) {
        parent::__construct($context);
        $this->connection = $resourceConnection->getConnection();
        $this->orderInterface = $orderInterface;
    }

    public function generateSaleReport()  
	{   
        try {
			$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
				// Update Existing Report
			$select = "SELECT shipping_and_handling,ecom_order,delivery_date,status,entity_id,increment_id FROM `sales_order_grid` WHERE status!='Delivered' AND status NOT IN('closed','canceled','pending','pending_payment') AND entity_id IN (select order_id from om_ecom_api_status)";
			
			//$select = "SELECT * FROM `sales_order_grid`";
			$results = $this->connection->fetchAll($select);
			foreach($results as $result){
				//$this->updateEcommOrderStatus($result);
				$order_id    = $result['entity_id'];
				$ship_charge = (int)$result['shipping_and_handling'];
				$discount_per=''; 
				$discount_type=''; 
				$discount_amount=''; 
				$coupon_number='';
				$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
				$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
				$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
				$couponCode = $order->getCouponCode();
				$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
				$rule = $saleRule->load($ruleId);
				$freeShippingCoupon = $rule->getSimpleFreeShipping();
				if($freeShippingCoupon){
					$discount_per=$rule->getDiscountAmount(); 
					$discount_type='Shipping Bypass'; 
					$discount_amount=$rule->getDiscountAmount(); 
					$coupon_number=$couponCode;	
				}
				
				$ship_date = $this->connection->fetchOne("SELECT created_at FROM `sales_shipment_grid` WHERE order_id='".$order_id."'");
				$this->connection->query("update om_sales_report_generated set shipping_date='".$ship_date."' , total_discount='".$discount_per."', type_of_discount='".$discount_type."',coupon_number='".$coupon_number."',coupon_amount='".$discount_amount."' WHERE order_id='".$order_id."'");
			}

			$quewrr = "SELECT `status`,`entity_id`,`updated_at` FROM `sales_order` where state='closed'";
			$results = $this->connection->fetchAll($quewrr);
			foreach($results as $result){
				$_status='Refunded';
				$entity_id = $result['entity_id'];
				$asd = "update om_ecom_api_status set order_status ='$_status' where order_id='$entity_id' and movement_type='Refund'";
				$this->connection->query($asd); 
			}

			$quewrrasdds = "SELECT a.`entity_id`,a.`state`,a.`status`,a.`increment_id` ,b.`created_at` FROM `sales_order` as  a  left join sales_shipment as  b on a.`entity_id`=`b`.`order_id` where a.`coupon_code` is not null and a.`state` ='complete'";
			// $quewrrasdds = "SELECT a.* ,b.`created_at` FROM `sales_order` as  a  left join sales_shipment as  b on a.`entity_id`=`b`.`order_id` where a.`base_shipping_amount`  is NULL OR a.`base_shipping_amount`=0 and a.`state` ='complete'";
			$resultsaa = $this->connection->fetchAll($quewrrasdds);
			
			foreach($resultsaa as $result){
				$pickup_date = $result['created_at'];
				$delivery_date = $result['created_at'];
				$entity_id = $result['entity_id'];
				$quewrrsds = "update om_ecom_api_status set order_status ='Delivered',pickup_date='$pickup_date',delivery_date='$delivery_date' where order_id='$entity_id'";
				$this->connection->query($quewrrsds);
			}

			// Insert New Record
			$today   = date('Y-m-d');
			$yesday  = date('Y-m-d',strtotime("-1 days"));		
			$select  = "select * from sales_order where status NOT IN('closed','pending','canceled','pending_payment') AND ( DATE(created_at) in ('".$today."','".$yesday."') OR DATE(updated_at) in ('".$today."','".$yesday."') )";
			
			//$select   = "SELECT * FROM `sales_order`";
			// $select   = "SELECT * FROM `sales_order` WHERE entity_id NOT IN (SELECT order_id from om_sales_report_generated)";
			
			$results = $this->connection->fetchAll($select);
			$this->processReport($results);
        }catch (Exception $e) {
			echo $e->getMessage();
		}
    }
    
    
    public function processReport($results){
		if(!empty($results)) {
			foreach($results as $row) 
			{
                $order_id=$row['entity_id'];
                $order_increment_id=$row['increment_id'];
				$type=($row['state']=='closed') ?7:1;
				
				if($type==7)
				{
					$select_1 = "select * from om_sales_report_generated where order_id=$order_id and report_type=1";
					$om_sales_report_forward = $this->connection->fetchAll($select_1);	
					if(empty($om_sales_report_forward))
					{
						$sales_order_item_query = "SELECT * FROM `sales_order_item` where  order_id = $order_id";
						$sales_order_item = $this->connection->fetchAll($sales_order_item_query);
						if(!empty($sales_order_item))
						{
							foreach ($sales_order_item as $item )
							{
								$item_id=$item['item_id'];
								$product_id=$item['product_id'];
								$product_qty=$item['qty_ordered'];
								$product_gst_rate=$item['product_gst_rate'];
								if(!empty($product_gst_rate))
								{
									$this->generateSalesReportItemStatic($order_increment_id,$product_id,$item_id,$product_qty,'1');
								}
							}
						}
					}
				}
				
				$select = "select * from om_sales_report_generated where order_id=$order_id and report_type=$type";
				$om_sales_report_results = $this->connection->fetchAll($select);
                if(empty($om_sales_report_results))
                {
					$sales_order_item_query = "SELECT * FROM `sales_order_item` where  order_id = $order_id";
                    $sales_order_item = $this->connection->fetchAll($sales_order_item_query);
                    if(!empty($sales_order_item))
                    {
                        foreach ($sales_order_item as $item )
                        {
                            $item_id=$item['item_id'];
                            $product_id=$item['product_id'];
							$product_qty=$item['qty_ordered'];
							$product_gst_rate=$item['product_gst_rate'];
							if(!empty($product_gst_rate)){
								$this->generateSalesReportItemStatic($order_increment_id,$product_id,$item_id,$product_qty,$type);
							}
                        }
                    }
				}
            }
        }		
	}

	public function generateSalesReportItemStatic($order_inc_id,$product_id,$item_id,$product_qty,$type)
	{

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $select = "select * from sales_order where increment_id=$order_inc_id";
        $sales_order = $this->connection->fetchAll($select)[0];
        $buyer_id = $sales_order['customer_id'];
        $order_id = $sales_order['entity_id'];
        $status = $sales_order['status'];
        /* check  if  item in return */
        $check_return = $this->check_product_in_return($order_id,$item_id);
        $return_req_date='';
		$return_req_reason='';
        $pickup_date='';

        if($check_return==1 || $type==1)
		{	

			$shippingbypassed_delivery_date = $this->checkifshippingbypassed($order_inc_id);
			if($shippingbypassed_delivery_date){
				if($shippingbypassed_delivery_date=="not_present")
				{
					$shippingbypassed_delivery_date=NULL;
				}
				$array=array(
					'pickup_date'=>$shippingbypassed_delivery_date,	
					'delivered_date'=>$shippingbypassed_delivery_date,	
					'status'=>$status,	
					'movement_type'=>"Forward",
					'awb_number'=>'N/A',
					'shipping_type'=>"Free Shipping",
					'order_id'=>$order_id,
		
				);
				
				$this->insert_into_ecom_apistatus($array);
			}
			$movement_type='Forward';
            $select = "select order_status,pickup_date,delivery_date from om_ecom_api_status where order_id=$order_id";
            $om_ecom_api_statuses = $this->connection->fetchAll($select);
            if(!empty($om_ecom_api_statuses[0]))
            {   
				
            } else {

				if($type!='7')
				{
					$array= $this->get_forward_order_tracking($order_id);
					$order_status= $array['status'];
					$pickup_date= $array['pickup_date'];
				}
			}
			
		}

		else
		{
			$movement_type='Return';
			
			$array=$this->get_return_order_tracking($order_id,$product_id,$movement_type);
			$order_status=$array['status'];
			$pickup_date=$array['pickup_date'];
			$returnArray = $this->order_return_details($order_id,$item_id);
			$return_req_date=$returnArray['rma_created_at'];
			$return_req_reason=$returnArray['rma_reason'];  
		}        
        $awb=$this->tax_get_order_awb($order_id);
		
		$order_inc_id=$sales_order['increment_id'];
		$order_cr_date=$sales_order['created_at'];
		$sales_invoice  = $this->getInvoiceDetails($order_id);
		$invoice_increment_id=$sales_invoice['increment_id'];
		$transaction_id= $sales_invoice['transaction_id'];
		$sales_order_item = $this->getSalesOrderItemDetails($item_id);
		$seller_id=	$this->get_product_seller_id($product_id);
		$total_discount = $sales_order_item['discount_amount'];
		$payment_type = $transaction_id=='COD' ? $transaction_id:"Online";
		
		$sellerdetails=$this->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		
		$seller_state=$this->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		$seller_address=$seller_details->seller_comp_address;
		$seller_zipcode=$seller_details->seller_zipcode;
		$seller_state=$seller_details->seller_state;
		$seller_city=$seller_details->seller_city;
		$seller_comp_nam=$seller_details->seller_comp_nam;
		
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		$seller_comp_nam=str_replace(array('\'', '"'), '', $seller_comp_nam); 
        
        $buyer_firstname=$this->get_customer_order_adress($order_id,'firstname','billing');
		$buyer_lastname=$this->get_customer_order_adress($order_id,'lastname','billing');
		$buyer_name=$buyer_firstname.' '.$buyer_lastname;
		$buyer_city = $this->get_customer_order_adress($order_id,'city','billing');
		$buyer_state = $this->get_customer_order_adress($order_id,'region','billing');
		$buyer_pincode = $this->get_customer_order_adress($order_id,'postcode','billing');
		$buyer_country_id = $this->get_customer_order_adress($order_id,'country_id','billing');
        $buyer_street = $this->get_customer_order_adress($order_id,'street','billing');
		
		$buyer_address = $buyer_street.$buyer_city.', '.$buyer_state.', '.$buyer_country_id.', '.$buyer_pincode;
		
		$delivery_firstname=$this->get_customer_order_adress($order_id,'firstname','billing');
		$delivery_lastname=$this->get_customer_order_adress($order_id,'lastname','billing');
		$delivery_name=$buyer_firstname.' '.$buyer_lastname;
		$delivery_city = $this->get_customer_order_adress($order_id,'city','shipping');
		$delivery_state = $this->get_customer_order_adress($order_id,'region','shipping');
		$delivery_pincode = $this->get_customer_order_adress($order_id,'postcode','shipping');
		$delivery_country_id = $this->get_customer_order_adress($order_id,'country_id','shipping');
		$street = $this->get_customer_order_adress($order_id,'street','shipping');
		
		$delivery_adress = $street.$delivery_city.', '.$delivery_state.', '.$delivery_country_id.', '.$delivery_pincode."."; 	
		
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
		$categories = $product->getCategoryIds();
		$product_name = $product->getName();
		$product_gst_rate = $product->getGstRate();		
		$product_name = preg_replace('/[^A-Za-z0-9. -]/', '', $product_name);
			
		$product_invoice_value='';	 
        $array=array();
		
		$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
		$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
		$couponCode = $order->getCouponCode();
		$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
		$rule = $saleRule->load($ruleId);
		$freeShippingCoupon = $rule->getSimpleFreeShipping();
		if($freeShippingCoupon){
			$discount_per=$rule->getDiscountAmount(); 
			$discount_type='Shipping Bypass'; 
			$discount_amount=$rule->getDiscountAmount(); 
			$coupon_number=$couponCode;	
			$awb="N/A";
		}
		else{
			$discount_per='';  
			$discount_type=''; 
			$discount_amount=''; 
			$coupon_number='';
		}
		$shipping_sac_code='';
		$shipping_date='';			
		$delivery_date='';	  			
		$cancel_date='';	
		
		$shipping_date = $this->connection->fetchOne("SELECT created_at FROM `sales_shipment_grid` WHERE order_id='".$order_id."'");
		
		$total_gst=$sales_order_item['cgst_amount']+$sales_order_item['sgst_amount']+$sales_order_item['igst_amount']+$sales_order_item['utgst_amount'];

		$gross_price = $sales_order_item['row_total'] - $total_gst;
		$total_discount=$sales_order_item['discount_amount'];  
		$net_sale_value=$gross_price-$total_discount;
		$product_invoice_value=$sales_order_item['row_total'];
		$comission=$sales_order_item['commision'];
		$shipping_base=$sales_order_item['shipping'];
		$shipping_gst=$sales_order_item['shipping_gst'];
		$shipping_expense=$sales_order_item['shipping_invoice_total'];
		$product_category=$sales_order_item['product_category'];
		$bp_category=$sales_order_item['bp_category'];
		$product_comission_category=$sales_order_item['product_comission_category'];
		$hsn_code=$sales_order_item['hsn_code'];
		$shipping_gst_rate=$sales_order_item['shipping_gst_rate'];
		$product_comission_percentage=$sales_order_item['product_comission_percentage'];
		$total_invoice_value=$product_invoice_value+$shipping_expense;  
        $credit_memo_increment_id="";
		$credit_memo_created_date="";
		if($type=='7') {
			$movement_type='Return';
			$order_status = ($check_return!=1)? "Return":"Refund";
			$movement_type = ($check_return!=1)? "Return":"Refund";
			$payment_type='offline';
			$transaction_id="Offline Refund";
			$creditmemo_details = $this->getCreditMemo($order_id);
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			
			if($check_return==1){
				$array=array(
					'pickup_date'=>"",	
					'delivered_date'=>"",	
					'status'=>$order_status,	
					'movement_type'=>"Refund",
					'awb_number'=>'',
					'shipping_type'=>"none",
					'order_id'=>$order_id,
				);
				$this->insert_into_ecom_apistatus($array);
			}

			$gross_price = -$gross_price;
			$net_sale_value = -$net_sale_value; 
			$total_gst = -$total_gst;
			$product_invoice_value = -$product_invoice_value; 
			$shipping_base = -$shipping_base;
			$shipping_gst = -$shipping_gst;
			$shipping_expense = -$shipping_expense;
			$total_invoice_value = -$total_invoice_value;
			$comission = -$comission;
			$cancel_date = $credit_memo_created_date;
        }

        $inoice_details = "Select entity_id,order_currency_code,created_at FROM sales_invoice where order_id=$order_id";
        $invoice_results_part = $this->connection->fetchAll($inoice_details);
		$invoice_entity_id="";
		$invoice_created_date="";
		if(!empty($invoice_results_part))
        {   
            $invoice_entity_id = $invoice_results_part[0]['entity_id'];
            $invoice_created_date = $invoice_results_part[0]['created_at']; 
        }
        $buyer_address = str_replace("'", " ",$buyer_address);
		$delivery_adress = str_replace("'", " ",$delivery_adress);
		if($freeShippingCoupon)
		{
			$shipping_base='0';
			$shipping_gst='0';
			$shipping_expense='0';
			$total_invoice_value=$product_invoice_value; 
		}
		
		echo $sql="insert into om_sales_report_generated set 
		order_increment_id = '$order_inc_id',
		invoice_increment_id = '$invoice_increment_id',
		creditmemo_increment_id = '$credit_memo_increment_id',
		order_date = '$order_cr_date',
		order_type = '$payment_type',
		payment_ref_num = '$transaction_id',
		invoice_date = '$invoice_created_date',
		credit_memo_date = '$credit_memo_created_date',
		return_request_date = '$return_req_date',
		return_request_reason = '$return_req_reason',
		awb = '$awb',
		seller_code = '$seller_id',
		seller_name = '$seller_comp_nam',
		seller_address = '$seller_address',
		seller_city = '$seller_city',
		seller_state = '$seller_state',
		seller_pincode = '$seller_zipcode',
		buyer_id = '$buyer_id',
		billing_name = '$buyer_name',
		billing_address = '$buyer_address',
		billing_city = '$buyer_city',
		billing_state = '$buyer_state',
		billing_pincode = '$buyer_pincode',
		shipping_name = '$delivery_name',
		shipping_address ='$delivery_adress',
		shipping_city ='$delivery_city',
		shipping_state = '$delivery_state',
		shipping_pincode ='$delivery_pincode',
		product_code = '$product_id',
		product_category = '$product_category',
		bp_category = '$bp_category',
		item = '$product_name',
 		item_description ='$product_name',
		hsn_code = '$hsn_code',
		product_gst_rate = '$product_gst_rate',
		product_comission_category = '$product_comission_category',
		product_comission_percentage = '$product_comission_percentage',
		quantity = '$product_qty',
		unit_of_measurement ='number',
		currency ='INR',
		gross_price_amt_inr = '$gross_price',
		discount_percentage = '$discount_per',
		type_of_discount = '$discount_type',
		coupon_number ='$coupon_number',
		coupon_amount ='',
		total_discount ='$total_discount',
		net_sales_value = '$net_sale_value',
		gst = '$total_gst',
		product_invoice_value = '$product_invoice_value',
		shipping_gst_rate = '$shipping_gst_rate',
		shipping = '$shipping_base',
		shipping_gst = '$shipping_gst',
		shipping_invoice_total = '$shipping_expense',
		total_invoice_value = '$total_invoice_value',
		comission = '$comission',
		shipping_date = '$shipping_date',
		cancellation_date = '$cancel_date',
		order_id ='$order_id',
		shipment_id ='',
		invoice_id ='$invoice_entity_id',
		credit_memo_id ='',
		return_id ='',
		return_increment_id ='',
		report_type ='$type',
		movement_type ='$movement_type',
		item_id = '$item_id'";
		
		$checkrecordalready = "select  `order_increment_id` from om_sales_report_generated where order_id=$order_id and report_type=$type and item_id = $item_id" ;
		$om_sales_report_alreadyhasentry = $this->connection->fetchOne($checkrecordalready);
		if(!$om_sales_report_alreadyhasentry)
		{
			$this->connection->query($sql); 
		}
	}






	function checkifshippingbypassed($incorder_idd){
		$sql = "Select * FROM sales_order_grid where increment_id='$incorder_idd' and ecom_order='No'";  
		$results = $this->connection->fetchAll($sql);
		if(empty($results))
		{
			return 0;
		}
		$deliverydate = $results[0]['delivery_date']; 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$timezone = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface'); 
		if(empty($deliverydate))
		{
			return $deliverydate="not_present";
		}
		$deliverydate = $timezone->date($deliverydate)->format('d-m-Y h:i:s a');
		return $deliverydate;
	}


    public function getCreditMemo($order_id)
	{	
		$select = "select * from sales_creditmemo where order_id = $order_id";
		$result = $this->connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
	}
	
	public function getInvoiceDetails($order_id)
	{	
		$select = "select * from sales_invoice where order_id = $order_id";
		$result = $this->connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
	}
	
	public function getSalesOrderItemDetails($item_id)
	{	
		$select = "select * from sales_order_item where `item_id` = $item_id";
		$result = $this->connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
    }
    
    public function get_category_name($cat_id)
	{
		$select = $this->connection->select()
				  ->from('catalog_category_entity_varchar') 
				  ->where('entity_id = ?', $cat_id)
				  ->where('attribute_id = ?', '45'); 
		 
		$result = $this->connection->fetchAll($select);
		if(!empty($result))
		{
			return $result[0]['value'];	
		}
    }
    
    public function get_customer_order_adress($order_id,$field,$address_type)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$sql = "Select ".$field." FROM sales_order_address where parent_id='".$order_id."' and address_type='".$address_type."'";
		$results = $connection->fetchAll($sql);
		if(!empty($results)) {
				return $results[0][$field];
		}   	  
	}

    public function get_state_pincode($pincode)
    {
        $select = $this->connection->select()
            ->from('pincode_details') 
            ->where('pincode = ?', $pincode);
        $result = $this->connection->fetchAll($select);
        
        if(!empty($result)){	
            return strtolower($result[0]['state']);   
        } else {
            return '110001';
        }         
    } 

    public function get_seller_zipcode($seller_id)
	{ 
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();		
			
		$select = $connection->select()
                  ->from('seller_postcode') 
                  ->where('seller_id = ?', $seller_id);
       
		$result = $connection->fetchAll($select);
				
		if(!empty($result)) {
			return $result[0]['postcode'];
		} else  {
			return 0;
		}	
	}

    public function tax_get_seller_array($seller_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$customer = $objectManager->get( 'Magento\Customer\Model\Customer')->load($seller_id);
		if ($customer) {		
			$returnArray = [];
			$returnArray['seller_name'] = $customer->getName();
			$returnArray['seller_gst'] = $customer->getGstNumber();
			$returnArray['seller_comp_nam'] = $customer->getCompanyNam();
			$returnArray['seller_comp_address'] = $customer->getcompanyAdd();
			$returnArray['seller_comp_mobile'] = $customer->getmobile();
			$returnArray['id'] = $customer->getId();
			
			$seller_zipcode=$this->get_seller_zipcode($customer->getId());
			if($seller_zipcode!='0'){  
				$returnArray['seller_zipcode']=$seller_zipcode; 	
			} else {
				$returnArray['seller_zipcode']='110001';	
			}  
			
			$select = $this->connection->select()
				->from('pincode_api') 
				->where('pincode = ?', $seller_zipcode);
			$result = $this->connection->fetchAll($select);
					
			if(!empty($result)){
				$returnArray['seller_state']=strtolower($result[0]['state']); 
				$returnArray['seller_city']=strtolower($result[0]['city']);
			}else{
				$returnArray['seller_state']='';
				$returnArray['seller_city']='';
			}		
		}
		return json_encode($returnArray);
	}

    public function tax_get_order_awb($order_id)
	{
		$select = $this->connection->select()
                  ->from('ecomexpress_awb') 
                  ->where('orderid = ?', $order_id);	
		$results = $this->connection->fetchAll($select);
		if(!empty($results)) {
			return $results[0]['awb'];
		}else {
			return 0;
		} 
	}

	public function get_product_seller_id($product_id)
	{ 
		$select = "select * from marketplace_product where mageproduct_id = '$product_id'";
		$results = $this->connection->fetchAll($select);
		if(!empty($results))
		{
			return $results[0]['seller_id'];
		} else {
			return '29';
		}   
	}

    public function get_return_order_tracking($order_id,$product_id,$movement_type)
	{ 
        $select = "select * from om_ecom_api_status where order_id=$order_id and movement_type='$movement_type'";
		$om_ecom_api_statuses = $this->connection->fetchAll($select);
        $status='closed';	
		$pickup_date=0;	
		$delivered_dates=0;
        if(!empty($om_ecom_api_statuses[0]))
        {   
            $om_ecom_api_statuse = $om_ecom_api_statuses[0];
            $array=array(
                'pickup_date'=>$om_ecom_api_statuse['pickup_date'],	
                'delivered_date'=>$om_ecom_api_statuse['delivery_date'],	
                'status'=>$om_ecom_api_statuse['order_status'],	
                'order_id'=>$order_id,	
            );
            return $array; 
        }
    
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$status=0;	
		$pickup_date=0;
		$delivered_dates=0;
		 
		$tracking_array=$api_helpers->get_rev_order_tracking($order_id,$product_id);   
		$tracking_array;
		if($tracking_array=='0') {
			$status="Approved";
		} else {
			$tracking_arrayy=json_decode($tracking_array);	
			$pickup=$tracking_arrayy[9];
			
			if(!is_object($pickup))
			{
				$status="Shipped";
				$pickup_date=$pickup;
			}	
			$delivered_date=$tracking_arrayy[21];
			if(!is_object($delivered_date))
			{
				$status="Delivered";
				$delivered_dates=$delivered_date;
			}
		}		
	
		$array=array(
			'pickup_date'=>$pickup_date,	
			'delivered_date'=>$delivered_dates,	
			'status'=>$status,	
			'movement_type'=>"Return",
			'awb_number'=>'',
			'shipping_type'=>"Return",
			'order_id'=>$order_id,
		);
		$this->insert_into_ecom_apistatus($array);
		return $array;  
	
    }

    public function check_product_in_return($order_id,$item_id)
	{
		$order = $this->orderInterface->load($order_id);
		foreach ($order->getAllItems() as $item)
		{
			$item_ids=$item->getId();
			if($item_ids==$item_id) {	
				$item_ordered=$item->getQtyOrdered();
			}
		} 
		
		$select = $this->connection->select()
				->from('wk_rma_items') 
				->where('item_id = ?', $item_id) 
				->where('order_id = ?', $order_id); 
							
		$result = $this->connection->fetchAll($select);
		
		$total_item_qty=0;
		if(!empty($result))	{
			foreach($result as $row){
				$item_qty=$row['qty'];	
				$total_item_qty=$total_item_qty+$item_qty;
			}	
			if($total_item_qty==$item_ordered){
				return 0; 	 		
			} else  {	
				return 1;	  	 
			}
		}	
		else {
			return 1;  
		}
	}
	
    public function isEnabled(){
        return true;
    }

    public  function generateApiTracking(){
        $select = "select * from sales_order ";
        $sales_order = $this->connection->fetchAll($select);
        foreach($sales_order as $order){
            $order_id = $order['entity_id'];
            $this->get_forward_order_tracking($order_id);
        }
    }


    public function get_forward_order_tracking($order_id)
	{
        $select = "select order_status,pickup_date,delivery_date from om_ecom_api_status where order_id=$order_id";
        $om_ecom_api_statuses = $this->connection->fetchAll($select);
        $status='New';	
		$pickup_date=0;	
		$delivered_dates=0;
        if(!empty($om_ecom_api_statuses[0]))
        {   
            $om_ecom_api_statuse = $om_ecom_api_statuses[0];
            $array=array(
                'pickup_date'=>$om_ecom_api_statuse['pickup_date'],	
                'delivered_date'=>$om_ecom_api_statuse['delivery_date'],	
                'status'=>$om_ecom_api_statuse['order_status'],	
                'order_id'=>$order_id,	
            );
            return $array; 
		}

		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
        $sql="select * from ecomexpress_awb where orderid='".$order_id."'";		  
		$results = $this->connection->fetchAll($sql);
		$modearray=json_decode($api_helpers->get_razorpay_mode());
		$mode=$modearray->mode;
		$username=$modearray->username;
		$password=$modearray->password;
		$awb="";
		if(!empty($results))
		{	
			foreach($results as $row)
			{  
				$order_id=$row['orderid'];
				$awb=$row['awb'];
				$status="processing";
				if($mode=="dev")  {
					$url='https://clbeta.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
				}else {
					$url='https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
                }
                
                
                $stin = @file_get_contents($url);	
                $hastag = strpos($stin,"</object");	
                $openingtag = strpos($stin,"<object");	
                if(!$hastag && $openingtag){	
                    $stin = str_ireplace("</ecomexpress-objects>","</object>	
                    </ecomexpress-objects>	
                    ",$stin);	
                    
                }	
                
               $xmlobj = simpleXML_load_string($stin,"SimpleXMLElement",LIBXML_NOCDATA);                
				//$xmlstr = $this->get_xml_from_url($url); 
				//$xmlobj = new SimpleXMLElement($xmlstr);
				$xmlobj = (array)$xmlobj;
				if(!empty($xmlobj['object']->field))
				{	
					$json=(array)$xmlobj['object']->field;		
					$pickup=$json[9];  					
					if(!is_object($pickup)){
						$shipped=1;	
						$status="Shipped";
						$pickup_date=$pickup;    
					}		
					
					$delivered_date=$json[21];
					if(!is_object($delivered_date)){
						$delivered=1;	
						$status="Delivered";
						$delivered_dates=$delivered_date;
					}
				
				}
			}
		}
		$array=array(
			'pickup_date'=>$pickup_date,	
			'delivered_date'=>$delivered_dates,	
			'status'=>$status,	
			'order_id'=>$order_id,
			'movement_type'=>"Forward",
			'shipping_type'=>"Ecom",	
			'awb_number'=>$awb
		);
        $this->insert_into_ecom_apistatus($array);
		return $array;  
    }
    
    /* not in use here from 08 sep 2020*/
    public function updateEcommOrderStatus($result)
	{
		if( $ship_charge > 0 ){ 
				$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
				$api_helpers   =  $objectManager->create('Customm\Apii\Helper\Data'); 
				$sql           =  "select * from ecomexpress_awb where orderid='".$order_id."'";		  
				$results = $this->connection->fetchAll($sql);
				$modearray=json_decode($api_helpers->get_razorpay_mode());
				$mode=$modearray->mode;
				$username=$modearray->username;
				$password=$modearray->password;
				if(!empty($results))
				{	
					foreach($results as $row)
					{  
						$awb   = $row['awb'];
						$status= "processing"; $pickup_date = ''; $delivered_dates='';
						if($mode=="dev")  {
							$url='https://clbeta.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
						}else {
							$url='https://plapi.ecomexpress.in/track_me/api/mawbd/?awb='.$awb.'&username='.$username.'&password='.$password;	
						}
												
						$stin = file_get_contents($url);	
						$hastag = strpos($stin,"</object");	
						$openingtag = strpos($stin,"<object");	
						if(!$hastag && $openingtag){	
							$stin = str_ireplace("</ecomexpress-objects>","</object></ecomexpress-objects>",$stin);								
						}	
					   $xmlobj = simpleXML_load_string($stin,"SimpleXMLElement",LIBXML_NOCDATA);                
						//$xmlstr = $this->get_xml_from_url($url); 
						//$xmlobj = new SimpleXMLElement($xmlstr);
						$xmlobj = (array)$xmlobj;
						if(!empty($xmlobj['object']->field))
						{	
							$json=(array)$xmlobj['object']->field;		
							$pickup=$json[9];  					
							if(!is_object($pickup)){
								$shipped=1;	
								$status="Shipped";
								$pickup_date=$pickup;    
							}		
							
							$delivered_date=$json[21];
							if(!is_object($delivered_date)){
								$delivered=1;	
								$status="Delivered";
								$delivered_dates=$delivered_date;
							}
								
						    $updateQuery = "update om_ecom_api_status set order_status='$status',pickup_date='$pickup_date',delivery_date='$delivered_dates',awb_number='$awb' where order_id='".$order_id."'";
							$this->connection->query($updateQuery);	 
							
							
						}
					}
				} 
			}else {
				$coupon =$objectManager->create('Magento\SalesRule\Model\Coupon');
				$saleRule = $objectManager->create('Magento\SalesRule\Model\Rule');
				$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
				$couponCode = $order->getCouponCode();
				$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
				$rule = $saleRule->load($ruleId);
				$freeShippingCoupon = $rule->getSimpleFreeShipping();
				if($freeShippingCoupon){
					$discount_per=$rule->getDiscountAmount(); 
					$discount_type='Shipping Bypass'; 
					$discount_amount=$rule->getDiscountAmount(); 
					$coupon_number=$couponCode;	
				}
				else{
						
				}
				$status = $this->connection->fetchOne("select status from sales_order_grid where entity_id='".$order_id."'");
				$pickup_date = $this->connection->fetchOne("select delivery_date from sales_order_grid where entity_id='".$order_id."'");
				$updateQuery = "update om_ecom_api_status set order_status='$status',pickup_date='$pickup_date',delivery_date='$pickup_date' where order_id='".$order_id."'";
				$this->connection->query($updateQuery);	 				
			}
		}

		public function insert_into_ecom_apistatus($array)
		{
			$pickup_date      = $array['pickup_date'];	
			$delivered_dates  = $array['delivered_date'];	
			$status           = $array['status'];	
			$order_id         = $array['order_id'];
			$movement_type    = $array['movement_type'];
			$shipping_type    = $array['shipping_type'];
			$awb              = $array['awb_number'];
			
/* 			$isEcom = $this->connection->fetchOne("SELECT ecom_order FROM `sales_order_grid` where entity_id =". $order_id);
			if($isEcom == 'Yes') {
				$shipping_type    = 'Ecom';
			} */

			$select = "select increment_id,status from sales_order where entity_id = '$order_id'";
			$order_data = $this->connection->fetchAll($select);
			$order_increment_id="";
			$order_status_original ="";
			if(!empty($order_data))
			{
				$order_increment_id = $order_data[0]['increment_id']; 
				$order_status_original = $order_data[0]['status']; 
			}  
			if(!in_array($order_status_original,array('pending_payment','pending','canceled'))){ 
				$seelct = "select id from om_ecom_api_status where order_id='$order_id' and shipping_type='$shipping_type' and movement_type='$movement_type'";
				$checkifentry_exist = $this->connection->fetchAll($seelct);
				if($checkifentry_exist){
					$eom_ecom_api_id = $checkifentry_exist[0]['id'];
					$updateQuery = "update om_ecom_api_status set order_status='$status',pickup_date='$pickup_date',delivery_date='$delivered_dates' where order_id='".$order_id."'";
					$this->connection->query($updateQuery);	   
				} else {
					$insert_ecom_query = "insert into om_ecom_api_status(order_id,increment_id,order_status,pickup_date,delivery_date,movement_type,shipping_type,awb_number) values('$order_id','$order_increment_id','$status','$pickup_date','$delivered_dates','$movement_type','$shipping_type','$awb')";
					$this->connection->query($insert_ecom_query);
				}
			}
		}



    public function order_return_details($order_id,$item_id)
	{
		$select = $this->connection->select()
                  ->from('wk_rma') 
                  ->where('order_id = ?', $order_id);
		
		$results = $this->connection->fetchAll($select);
		$cr_date='';
		$rma_reason='';
		
		if(!empty($results))
		{
			$cr_date=$results[0]['created_at'];	
		}			
		
		$select = $this->connection->select()
                  ->from('wk_rma_items') 
                  ->where('order_id = ?', $order_id)
                  ->where('item_id = ?', $item_id);
		
		$results = $this->connection->fetchAll($select);
		if(!empty($results))
		{
			$rma_reason=$results[0]['rma_reason'];	
		}	
		$array=array(
			'rma_created_at'=>$cr_date,
			'rma_reason'=>$rma_reason,
		);
		return $array;
	}

}
