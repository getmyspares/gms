<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\GenerateReport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class SettlementHelperPartial extends AbstractHelper
{
	protected $_objectManager;
	
	public function __construct(
			\Magento\Framework\App\Helper\Context $context,
			\Magento\Framework\App\ResourceConnection $resourceConnection,
			\Magento\Framework\ObjectManagerInterface $objectmanager,
			\Magento\Sales\Api\Data\OrderInterface $orderInterface
	) {
			parent::__construct($context);
			$this->_objectManager = $objectmanager;
			$this->connection = $resourceConnection->getConnection();
			$this->orderInterface = $orderInterface;
	}

  public function generateSettlementReport()  
	{   
		try {		
			// $select   = "SELECT * FROM `sales_order` WHERE entity_id NOT IN (SELECT order_id from om_settlement_report_partial where report_type='normal') and state!='closed' limit 10";
			// $results = $this->connection->fetchAll($select);
			// /* for  forward  entry */
			// foreach($results as $result){
			// 	$incremment_id = $result['increment_id'];
			// 	$order_id = $result['entity_id'];
			// 	$type = "normal";
			// 	$this->create_settlement_report($result['increment_id'],$type);
			// }

			/* for backward entry */
			$today=date('Y-m-d');
			$yesterday=date('Y-m-d',strtotime("-1 days"));
			$select="SELECT * FROM `sales_creditmemo` where order_id not  in (SELECT order_id from om_settlement_report_partial where report_type='refund') and order_id  ='1035'  ";
			$results = $this->connection->fetchAll($select);
			foreach($results as $result){
				$order_id = $result['order_id'];
				$query="select increment_id  from sales_order  where  entity_id='$order_id'";
				// $refund_status  = $this->orderRefundStatus($order_id);
				$incremment_id = $this->connection->fetchOne($query);
				$refund_status=2;
				if($refund_status==2)
				{
					$type = "refund"; 
					$this->create_settlement_report($incremment_id,$type);
				}
				if($refund_status==3)
				{
					$type = "partial_return"; 
					$this->create_partial_settlement_report($incremment_id,$type);
					
				}
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	
	public function create_partial_settlement_report($order_inc_id,$type='normal')
	{

		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$custom_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Customfunction'); 
		$helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $this->_objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$buyer_id=$order->getCustomerId();  
 		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$cgst_amount_n=round($cgst_amount); 
		$sgst_amount_n=round($sgst_amount); 
		$igst_amount_n=round($igst_amount); 
		$ugst_amount_n=round($ugst_amount); 
		$gst_nature='CGST/SGST/UTGST';	
		
		if($igst_amount_n!=0){
			$gst_nature='IGST';	
		}	
		
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		$nodal_charges=0;
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_increment_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$baseprice=$orderDetailArray->baseprice;
		
		$grand_total = $orderDetailArray->grand_total;
		$base_total_refunded = $order->getData('base_total_refunded');
		
		$grand_total = $grand_total- $base_total_refunded;


		$total_gst=$orderDetailArray->total_gst;
		$subtotal=$orderDetailArray->subtotal;
		
		
		$shipping_gst=$orderDetailArray->shipping_gst;
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		
		$order_shipping_amount = ($order->getData('shipping_incl_tax') - $order->getData('base_shipping_refunded')); 
		$logistic_tds = $tax_helpers->tax_logistic_tds($order_shipping_amount,$order_inc_id);
		
		$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id);
		$shipping_base = $order_shipping_amount-$logistic_tax;

		$shipping_expense =  $order_shipping_amount;

		$total_admin_comm=0;
		
		$sqasdal = "select * from sales_order_item where order_id='$order_id' and `qty_refunded`=0";
		$only_purchased_item = $connection->query($sqasdal);
		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getproductId(); 
			$seller=$custom_helpers->get_product_seller($order_id,$product_id);
			$sellerarray[]=$custom_helpers->get_product_seller($order_id,$product_id);
			$productArray=$tax_helpers->order_item_details_product($order_inc_id,$product_id);
			$comission=$productArray[0]['admin_comm'];  
			$total_admin_comm=$total_admin_comm+$comission;
		
		} 
		$gross_comm_due = $total_admin_comm;
		$admin_comm = $total_admin_comm;
		/*  */

		$gst_tcs=$orderDetailArray->gst_tcs; 
		
		$nodal_charges=$orderDetailArray->nodle_charges; 

		$gross_comm_due=$orderDetailArray->gross_comm_due; 
		
		$tax_rates_array=json_decode($tax_helpers->tax_order_parameter($order_inc_id));
		$admin_comm_tax_rate=$tax_rates_array->admin_comm_tax;
		$admin_comm_tax=($admin_comm*$admin_comm_tax_rate)/100;
		$gst_gross_comm_due=$admin_comm_tax; 
		
		$razorpay_tds=$orderDetailArray->razorpay_tds; 
		$razor_method=$orderDetailArray->razor_payment; 
		$razor_card_type=$orderDetailArray->razor_card; 
		$razor_network=$orderDetailArray->razor_network;   
		$razor_pay_fee_amount=$orderDetailArray->razor_pay_fee_amount; 
		$razor_pay_gst_amount=$orderDetailArray->razor_pay_gst_amount; 
		$admin_comm_tds=$orderDetailArray->admin_comm_tds; 
		
		$admin_comm_tds_rate=$tax_rates_array->admin_comm_tds;
		$gst_tcs_rate=$tax_rates_array->gst_tcs;
		$net_comm=$admin_comm+$admin_comm_tax;

		$admin_comm_tds=($admin_comm*$admin_comm_tds_rate)/100;
		$gstArray=$this->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst'];  
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$total_gst=$gstArray['total_gst']; 

		$order_subtotal = ($order->getData('subtotal') - $order->getData('subtotal_refunded'))-$total_gst;
		$gst_tcs_on_total_amount=($order_subtotal*$gst_tcs_rate)/100;	   
		$net_admin_comm=$net_comm+$gst_tcs_on_total_amount-$admin_comm_tds;

		$razor_pay_total_charge_amount=$razor_pay_fee_amount+$razor_pay_gst_amount;
		$net_amount_rec_nodal=$grand_total-$razor_pay_total_charge_amount;
		$net_remittance_logistic=$shipping_expense-$logistic_tds;
		$shipping_gst_tcs=($shipping_base*1)/100; 

		$amount_due_to_logistic=$shipping_expense-$logistic_tds-$shipping_gst_tcs;    
		
		
		$tds_pi_comm=$admin_comm_tds;	  
		$gross_remittance_pi = $net_admin_comm;

		$payment_gateway_charges_net_tds=$razor_pay_fee_amount-$razorpay_tds; 
		
		$net_remittance_pi=$gross_remittance_pi-$payment_gateway_charges_net_tds-$nodal_charges+$logistic_tds+$shipping_gst_tcs-$razor_pay_gst_amount; 
		
		$nodal_balance=$net_amount_rec_nodal-$nodal_charges;
		$balance_in_nodal=$nodal_balance-$amount_due_to_logistic;
		$balance_in_nodal_pi=$balance_in_nodal-$net_remittance_pi;  
		$remittance_to_seller=$grand_total-$shipping_expense-$gross_remittance_pi;
		$payment_type='COD';	 
		if($payment_id!='COD') {
			$payment_type='Online';	
		}	
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		
		$seller_comp_nam = $seller_details->seller_comp_nam;
		$seller_comp_nam = str_replace(array('\'', '"'), '', $seller_comp_nam); 
		
		$seller_address=$seller_details->seller_comp_address;
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		$return_date = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($order_cr_date)); 
		$settlement_due_date = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
		$cgst_amount_n=round($cgst_amount); 
		$sgst_amount_n=round($sgst_amount); 
		$igst_amount_n=round($igst_amount); 
		$ugst_amount_n=round($ugst_amount); 
		$shipping_igst=0;
		$shipping_cgst=0;
		$shipping_sgst=0;
		$shipping_ugst=0;
		$shipping_gst_t=0;
		if($igst_amount_n!=0){
			$shipping_igst=$shipping_gst;
		}else if($cgst_amount_n!=0 && $sgst_amount_n!=0) {			
			$shipping_gst_t=$shipping_gst/2;
			$shipping_cgst=$shipping_gst_t;
			$shipping_sgst=$shipping_gst_t;
		}else if($cgst_amount_n!=0 && $ugst_amount!=0) {			
			$shipping_gst_t=$shipping_gst/2;
			$shipping_cgst=$shipping_gst_t;
			$shipping_ugst=$shipping_gst_t; 
		}	
		$razor_pay_gst_igst=0;
		$razor_pay_gst_cgst=0;
		$razor_pay_gst_sgst=0;
		$razor_pay_gst_ugst=0;
		$razor_pay_gst_gst_t=0; 
		$razor_pay_gst_gst_rate=0;  
		if($igst_amount_n!=0) {
			$razor_pay_gst_igst=$razor_pay_gst_amount;
		}else if($cgst_amount_n!=0 && $sgst_amount_n!=0) {			
			$razor_pay_gst_gst_t=$razor_pay_gst_amount/2;
			$razor_pay_gst_cgst=$razor_pay_gst_gst_t;
			$razor_pay_gst_sgst=$razor_pay_gst_gst_t; 
		}else if($cgst_amount_n!=0 && $ugst_amount!=0){			
			$razor_pay_gst_gst_t=$razor_pay_gst_amount/2;
			$razor_pay_gst_cgst=$razor_pay_gst_gst_t;
			$razor_pay_gst_ugst=$razor_pay_gst_gst_t; 
		}	
		
		if($razor_pay_fee_amount=='0'){
			$razor_pay_gst_gst_rate=0;
		}else{	
			$razor_pay_gst_gst_rate=($razor_pay_gst_amount/$razor_pay_fee_amount)*100; 
		}
		$razor_pay_gst_amount.' '.$razor_pay_fee_amount.' '.$razor_pay_gst_gst_rate;
			
		if($razor_pay_gst_gst_rate >=5 && $razor_pay_gst_gst_rate< 12)	{
			$razor_pay_gst_gst_rate=5;	
		} else if($razor_pay_gst_gst_rate >=12 && $razor_pay_gst_gst_rate< 18) {
			$razor_pay_gst_gst_rate=12;	
		} else if($razor_pay_gst_gst_rate >=18 && $razor_pay_gst_gst_rate< 28) {
			$razor_pay_gst_gst_rate=18;	
		} else if($razor_pay_gst_gst_rate >=28 && $razor_pay_gst_gst_rate< 30)	{
			$razor_pay_gst_gst_rate=28;	
		}	
		
		if($razor_pay_gst_gst_rate!=0) 
		{
			$razor_pay_gst_gst_rate=18;	
		}

		$gst_gross_comm_due_igst=0;
		$gst_gross_comm_due_cgst=0;
		$gst_gross_comm_due_sgst=0;
		$gst_gross_comm_due_ugst=0;
		$gst_gross_comm_due_t=0; 
		if($igst_amount_n!=0) {
			$gst_gross_comm_due_igst=$gst_gross_comm_due;
		}else if($cgst_amount_n!=0 && $sgst_amount_n!=0) {			
			$gst_gross_comm_due_t=$gst_gross_comm_due/2;
			$gst_gross_comm_due_cgst=$gst_gross_comm_due_t;
			$gst_gross_comm_due_sgst=$gst_gross_comm_due_t; 
		}	else if($cgst_amount_n!=0 && $ugst_amount!=0) {			
			$gst_gross_comm_due_t=$gst_gross_comm_due/2;
			$gst_gross_comm_due_sgst=$gst_gross_comm_due_t;
			$gst_gross_comm_due_ugst=$gst_gross_comm_due_t; 
		}	
		$buyer_id=$orderDetailArray->customer_id;
		$payment_id=$orderDetailArray->payment_id;
		$delivery_date=0;
		$return=0;
		/* 
			delivery date logic 
		*/
		$sales_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$array=$sales_helpers->getOrderTrackingOM($order_id,$type);
		$pickup_date=$array['pickup_date'];   
		$delivery_date=$array['delivered_date'];
		$bypassedshippingdeldate = $this->checkifshippingbypassed($order_inc_id);
		if($bypassedshippingdeldate)
		{
			$delivery_date = $bypassedshippingdeldate;
		};

		/* 
			RealizationDays logic 
		*/
		$RealizationDays=0; 
		if($delivery_date!=0) 
		{	
			$return_days = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
			$today = date("Y-m-d",strtotime($delivery_date)); 
			$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
			$settlement_due_date=date('Y-m-d', strtotime('next monday', strtotime($RealizationDays)));
			$todayy=date('Y-m-d'); 
			$today_time = strtotime($todayy);
			$expire_time = strtotime($RealizationDays);	
			if ($expire_time < $today_time)
			{
				$recognition_status='Recognised';
			}		
		}
		$settlement_due_date=0;
		if($return==0)
		{
			if($delivery_date!=0)
			{	
				$return_days = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
				$today = date("Y-m-d",strtotime($delivery_date)); 
				$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
				$settlement_due_date=date('Y-m-d', strtotime('next monday', strtotime($RealizationDays)));	
			}
		}	
		$not_to_show_old_format=true;
		$datecheck = "2020-09-01";
		$datecheck = strtotime($datecheck);
		$order_Created_date = $this->getOrderCreatedDate($order_id);
		$order_Created_date = strtotime($order_Created_date);
		if($RealizationDays!=0)
		{
			$transaction_complete_date=date('Y-m-d',strtotime($RealizationDays));
			$sales_confirmation_date =$transaction_complete_date; 
			$todayStamp = strtotime($transaction_complete_date);
			$numOfDays = date('t', $todayStamp);
			$base = strtotime('+'.$numOfDays.' days', strtotime(date('Y-m-01', $todayStamp)));
			$transaction_complete_modified = date('Y-m-25', $base);
			$datetocheck = strtotime("2020-10-01");
			if(strtotime($RealizationDays) >= $datetocheck)
			{
				$not_to_show_old_format=false;
				$day = date("d", strtotime($sales_confirmation_date));
				if($day<=15)
				{
					$transaction_complete_modified = date('Y-m-30',strtotime($sales_confirmation_date));
				}
				if($day>=16)
				{
					$day = date("d", strtotime($sales_confirmation_date));
					$transaction_complete_modified = date('Y-m-15', strtotime("first day of +1 month", strtotime($sales_confirmation_date)));	
				}
			}
		}			
		$creditmemo_grand_amount='';
		$refundsettlementbalance='';
		
		$raz_cgst=0;
		$raz_sgst=0;
		$raz_igst=0;
		$raz_utgst=0;
		$raz_igst = $razor_pay_gst_igst+$razor_pay_gst_cgst+$razor_pay_gst_sgst+$razor_pay_gst_ugst;
		
		$settlement_due_date=0;

		/* calculate  gst nature */
		$cgst=$order->getCgstAmount();
		$sgst=$order->getSgstAmount();
		$igst=$order->getIgstAmount();
		$utgst=$order->getUtgstAmount();
		if($igst!='0.0000')
		{
			$gst_nature='IGST';	
		}
		else if($cgst!='0.0000' && $sgst!='0.0000')
		{
			$gst_nature='CGST/SGST';	
		}	
		else if($cgst!='0.0000' && $utgst!='0.0000')
		{
			$gst_nature='CGST/UTGST';	
		}

		if($datecheck>$order_Created_date && $type!='refund' && $not_to_show_old_format)
		{
			$order_date_type="old_date_format";
			$transaction_complete_modified = $settlement_due_date;
		}
		$payment_type='partial refund';
		$creditmemo_details = $this->getCreditMemo($order_id);
		$return_days = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
		$credit_memo_increment_id = $creditmemo_details['increment_id'];
		$credit_memo_created_date = $creditmemo_details['created_at'];
		$creditmemo_grand_amount = $creditmemo_details['grand_total'];
		$invoice_increment_id = $credit_memo_increment_id;
		$order_cr_date = $credit_memo_created_date;
		$sales_confirmation_date = date("Y-m-d",strtotime($credit_memo_created_date));
		$transaction_complete_modified = date ("Y-m-d", strtotime ($sales_confirmation_date ."+".$return_days." days"));
		$payment_id='Partial  Refund';

		
		if(!empty($refund_settlement_balance))
		{
			$amount_due_to_pi = $refund_settlement_balance; 
		}else{
			$amount_due_to_pi = $net_remittance_pi;
		} 

		$razorpay_charges_net_tds=$razor_pay_total_charge_amount-$razorpay_tds;
		
		
		if($delivery_date!=0 && $RealizationDays!=0 || true)
		{
			$eligibale_for_settlemet=false;	
			if($type!='refund' && $type!='partial_return')
			{
				$date1 = date('Y-m-d');
				$date2 = date("Y-m-d", strtotime($RealizationDays));
				$eligibale_for_settlemet =  $date1 >= $date2 ? true : false;
			}
			$eligibale_for_settlemet=true;	

			if($eligibale_for_settlemet || true )
			{
				$sql = "INSERT INTO `om_settlement_report_partial` set
				`order_num` =  '$order_inc_id',
				`order_date`='$order_cr_date',
				`buyer_id`='$buyer_id',
				`order_type`='$payment_type',
				`payment_id`='$payment_id',
				`invoice_ref_num`='$invoice_increment_id',
				`invoice_date`='$order_cr_date',
				`seller_code`='$seller_id',
				`seller_name`='$seller_comp_nam',
				`delivery_date`='$delivery_date',
				`sales_return_confirmation_date`='$sales_confirmation_date',
				`transaction_completion_date`='$transaction_complete_modified',
				`basic_sale_amount`='$baseprice',
				`nature_of_gst`='$gst_nature',
				`igst_sgst_ugst_amount`='$total_gst',
				`listed_price`='$subtotal',
				`shipping_charges_basic`='$shipping_base',
				`shipping_charges_gst`='$shipping_gst',
				`rate_percentage_shipping`='$shipping_gst_rate',
				`igst_shipping`='$shipping_igst',
				`cgst_shipping`='$shipping_cgst',
				`sgst_shipping`='$shipping_sgst',
				`utgst_shipping`='$shipping_ugst',
				`shipping_charges_total`='$shipping_expense',
				`total_sale_invoice_value`='$grand_total',
				`amount_received_in_payment_gateway`='$grand_total',
				`payment_method`='$razor_method',
				`card_type`='$razor_card_type',
				`card_network`='$razor_network',
				`payment_gateway_pg_charges`='$razor_pay_fee_amount',
				`gst_on_pg_charges`='$razor_pay_gst_amount',
				`rate_percent_razor`='$razor_pay_gst_gst_rate',
				`igst_razor`='$raz_igst',
				`cgst_razor`='$raz_cgst',
				`sgst_razor`='$raz_sgst',
				`utgst_razor`='$raz_utgst',
				`payment_gateway_charges_invoice_value`='$razor_pay_total_charge_amount',
				`razorpay_tds`='$razorpay_tds',
				`payment_gateway_charges_net_of_tds`='$razorpay_charges_net_tds;',
				`net_amount_received_in_nodal_account`='$net_amount_rec_nodal',
				`nodal_charges_monthly`='$nodal_charges',
				`balance_in_nodal_first`='$nodal_balance',
				`logistic_tds`='$logistic_tds',
				`gst_tcs_1_on_supplies_to_deposited_by_pi`='$shipping_gst_tcs',
				`amount_due_to_logistics`='$amount_due_to_logistic',
				`balance_in_nodal_second`='$balance_in_nodal',
				`gross_comm_due`='$gross_comm_due',
				`gst_on_comm_18_percent`='$gst_gross_comm_due',
				`net_commission`='$net_comm',
				`gst_tcs_1_on_basic_sale_amt`='$gst_tcs',
				`tds_on_pi_comission`='$tds_pi_comm',
				`due_from_seller_to_pi`='$gross_remittance_pi',
				`amount_due_to_pi`='$amount_due_to_pi',
				`balance_to_be_in_nodal_if_paid_to_pi`='$balance_in_nodal_pi',
				`amount_due_to_seller`='$remittance_to_seller',
				`order_id`='$order_id',
				`report_type`='$type',
				`refunded_amount`='$creditmemo_grand_amount'
			";
			$connection->query($sql); 
			}
	       
		}
		
	}	
	
	public function create_settlement_report($order_inc_id,$type='normal')
	{
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		$helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Data');
		$tax_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $this->_objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_inc_id); 
		$order_id=$order->getId();
		$buyer_id=$order->getCustomerId();  
 		$gstArray=$tax_helpers->get_order_gst_details($order_inc_id);
		
		$cgst_amount=$gstArray['cgst']; 
		$sgst_amount=$gstArray['sgst']; 
		$igst_amount=$gstArray['igst']; 
		$ugst_amount=$gstArray['utgst'];  
		$cgst_amount_n=round($cgst_amount); 
		$sgst_amount_n=round($sgst_amount); 
		$igst_amount_n=round($igst_amount); 
		$ugst_amount_n=round($ugst_amount); 
		$gst_nature='CGST/SGST/UTGST';	
		
		if($igst_amount_n!=0){
			$gst_nature='IGST';	
		}	
		
		$orderDetailArray=$tax_helpers->order_amount_details($order_inc_id);
		$orderDetailArray=json_decode($orderDetailArray);
		
		$nodal_charges=0;
		$order_inc_id=$orderDetailArray->order_increment_id;
		$order_cr_date=$orderDetailArray->order_created_at;
		$invoice_increment_id=$orderDetailArray->invoice_id;
		$seller_id=$orderDetailArray->seller_id;
		$payment_id=$orderDetailArray->payment_id;
		$baseprice=$orderDetailArray->baseprice;
		$grand_total=$orderDetailArray->grand_total;
		$total_discount=$orderDetailArray->total_discount;
		$total_gst=$orderDetailArray->total_gst;
		$subtotal=$orderDetailArray->subtotal;
		$shipping_expense=$orderDetailArray->shipping_expense;
		$shipping_base=$orderDetailArray->shipping_base;
		$shipping_gst=$orderDetailArray->shipping_gst;
		$shipping_gst_rate=$orderDetailArray->shipping_gst_rate; 
		$logistic_tds=$orderDetailArray->logistic_tds; 
		$gst_tcs=$orderDetailArray->gst_tcs; 
		$nodal_charges=$orderDetailArray->nodle_charges; 
		$gross_comm_due=$orderDetailArray->gross_comm_due; 
		$gst_gross_comm_due=$orderDetailArray->gst_gross_comm_due; 
		$razorpay_tds=$orderDetailArray->razorpay_tds; 
		$razor_method=$orderDetailArray->razor_payment; 
		$razor_card_type=$orderDetailArray->razor_card; 
		$razor_network=$orderDetailArray->razor_network;   
		$razor_pay_fee_amount=$orderDetailArray->razor_pay_fee_amount; 
		$razor_pay_gst_amount=$orderDetailArray->razor_pay_gst_amount; 
		$admin_comm_rate=$orderDetailArray->admin_comm_rate; 
		$admin_comm_tds_rate=$orderDetailArray->admin_comm_tds_rate; 
		$admin_comm_tds=$orderDetailArray->admin_comm_tds; 
		$gst_tcs_rate=$orderDetailArray->gst_tcs_rate; 
		$net_comm=$orderDetailArray->net_comm; 
		$net_admin_comm=$orderDetailArray->net_admin_comm; 
		$razor_pay_total_charge_amount=$razor_pay_fee_amount+$razor_pay_gst_amount;
		$net_amount_rec_nodal=$grand_total-$razor_pay_total_charge_amount;
		$net_remittance_logistic=$shipping_expense-$logistic_tds;
		$shipping_gst_tcs=($shipping_base*1)/100; 
		$amount_due_to_logistic=$shipping_expense-$logistic_tds-$shipping_gst_tcs;    
		$tds_pi_comm=$admin_comm_tds;	  
		$gross_remittance_pi=$net_admin_comm; 
		$payment_gateway_charges_net_tds=$razor_pay_fee_amount-$razorpay_tds; 
		$net_remittance_pi=$gross_remittance_pi-$payment_gateway_charges_net_tds-$nodal_charges+$logistic_tds+$shipping_gst_tcs-$razor_pay_gst_amount; 
		$nodal_balance=$net_amount_rec_nodal-$nodal_charges;
		$balance_in_nodal=$nodal_balance-$amount_due_to_logistic;
		$balance_in_nodal_pi=$balance_in_nodal-$net_remittance_pi;  
		$remittance_to_seller=$grand_total-$shipping_expense-$gross_remittance_pi;
		$payment_type='COD';	 
		if($payment_id!='COD') {
			$payment_type='Online';	
		}	
		$sellerdetails=$tax_helpers->tax_get_seller_array($seller_id);
		$seller_details=json_decode($sellerdetails);
		$seller_state=$helpers->get_state_pincode($seller_details->seller_zipcode); 
		$seller_name=$seller_details->seller_name;
		
		$seller_comp_nam = $seller_details->seller_comp_nam;
		$seller_comp_nam = str_replace(array('\'', '"'), '', $seller_comp_nam); 
		
		$seller_address=$seller_details->seller_comp_address;
		$seller_zipcode=$seller_details->seller_zipcode;
		$seller_state=$seller_details->seller_state;
		$seller_city=$seller_details->seller_city;
		$seller_name=str_replace(array('\'', '"'), '', $seller_name); 
		$seller_address=str_replace(array('\'', '"'), '', $seller_address); 
		$return_date = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
		$today = date("Y-m-d",strtotime($order_cr_date)); 
		$settlement_due_date = date ("Y-m-d", strtotime ($today ."+".$return_date." days"));
		$cgst_amount_n=round($cgst_amount); 
		$sgst_amount_n=round($sgst_amount); 
		$igst_amount_n=round($igst_amount); 
		$ugst_amount_n=round($ugst_amount); 
		$shipping_igst=0;
		$shipping_cgst=0;
		$shipping_sgst=0;
		$shipping_ugst=0;
		$shipping_gst_t=0;
		if($igst_amount_n!=0){
			$shipping_igst=$shipping_gst;
		}else if($cgst_amount_n!=0 && $sgst_amount_n!=0) {			
			$shipping_gst_t=$shipping_gst/2;
			$shipping_cgst=$shipping_gst_t;
			$shipping_sgst=$shipping_gst_t;
		}else if($cgst_amount_n!=0 && $ugst_amount!=0) {			
			$shipping_gst_t=$shipping_gst/2;
			$shipping_cgst=$shipping_gst_t;
			$shipping_ugst=$shipping_gst_t; 
		}	
		$razor_pay_gst_igst=0;
		$razor_pay_gst_cgst=0;
		$razor_pay_gst_sgst=0;
		$razor_pay_gst_ugst=0;
		$razor_pay_gst_gst_t=0; 
		$razor_pay_gst_gst_rate=0;  
		if($igst_amount_n!=0) {
			$razor_pay_gst_igst=$razor_pay_gst_amount;
		}else if($cgst_amount_n!=0 && $sgst_amount_n!=0) {			
			$razor_pay_gst_gst_t=$razor_pay_gst_amount/2;
			$razor_pay_gst_cgst=$razor_pay_gst_gst_t;
			$razor_pay_gst_sgst=$razor_pay_gst_gst_t; 
		}else if($cgst_amount_n!=0 && $ugst_amount!=0){			
			$razor_pay_gst_gst_t=$razor_pay_gst_amount/2;
			$razor_pay_gst_cgst=$razor_pay_gst_gst_t;
			$razor_pay_gst_ugst=$razor_pay_gst_gst_t; 
		}	
		
		if($razor_pay_fee_amount=='0'){
			$razor_pay_gst_gst_rate=0;
		}else{	
			$razor_pay_gst_gst_rate=($razor_pay_gst_amount/$razor_pay_fee_amount)*100; 
		}
		$razor_pay_gst_amount.' '.$razor_pay_fee_amount.' '.$razor_pay_gst_gst_rate;
			
		if($razor_pay_gst_gst_rate >=5 && $razor_pay_gst_gst_rate< 12)	{
			$razor_pay_gst_gst_rate=5;	
		} else if($razor_pay_gst_gst_rate >=12 && $razor_pay_gst_gst_rate< 18) {
			$razor_pay_gst_gst_rate=12;	
		} else if($razor_pay_gst_gst_rate >=18 && $razor_pay_gst_gst_rate< 28) {
			$razor_pay_gst_gst_rate=18;	
		} else if($razor_pay_gst_gst_rate >=28 && $razor_pay_gst_gst_rate< 30)	{
			$razor_pay_gst_gst_rate=28;	
		}	
		
		if($razor_pay_gst_gst_rate!=0) 
		{
			$razor_pay_gst_gst_rate=18;	
		}

		$gst_gross_comm_due_igst=0;
		$gst_gross_comm_due_cgst=0;
		$gst_gross_comm_due_sgst=0;
		$gst_gross_comm_due_ugst=0;
		$gst_gross_comm_due_t=0; 
		if($igst_amount_n!=0) {
			$gst_gross_comm_due_igst=$gst_gross_comm_due;
		}else if($cgst_amount_n!=0 && $sgst_amount_n!=0) {			
			$gst_gross_comm_due_t=$gst_gross_comm_due/2;
			$gst_gross_comm_due_cgst=$gst_gross_comm_due_t;
			$gst_gross_comm_due_sgst=$gst_gross_comm_due_t; 
		}	else if($cgst_amount_n!=0 && $ugst_amount!=0) {			
			$gst_gross_comm_due_t=$gst_gross_comm_due/2;
			$gst_gross_comm_due_sgst=$gst_gross_comm_due_t;
			$gst_gross_comm_due_ugst=$gst_gross_comm_due_t; 
		}	
		$buyer_id=$orderDetailArray->customer_id;
		$payment_id=$orderDetailArray->payment_id;
		$delivery_date=0;
		$return=0;
		/* 
			delivery date logic 
		*/
		$sales_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$array=$sales_helpers->getOrderTrackingOM($order_id,$type);
		$pickup_date=$array['pickup_date'];   
		$delivery_date=$array['delivered_date'];
		$bypassedshippingdeldate = $this->checkifshippingbypassed($order_inc_id);
		if($bypassedshippingdeldate)
		{
			$delivery_date = $bypassedshippingdeldate;
		};

		/* 
			RealizationDays logic 
		*/
		$RealizationDays=0; 
		if($delivery_date!=0) 
		{	
			$return_days = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
			$today = date("Y-m-d",strtotime($delivery_date)); 
			$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
			$settlement_due_date=date('Y-m-d', strtotime('next monday', strtotime($RealizationDays)));
			$todayy=date('Y-m-d'); 
			$today_time = strtotime($todayy);
			$expire_time = strtotime($RealizationDays);	
			if ($expire_time < $today_time)
			{
				$recognition_status='Recognised';
			}		
		}
		$settlement_due_date=0;
		if($return==0)
		{
			if($delivery_date!=0)
			{	
				$return_days = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');	
				$today = date("Y-m-d",strtotime($delivery_date)); 
				$RealizationDays = date ("Y-m-d", strtotime ($today ."+".$return_days." days"));
				$settlement_due_date=date('Y-m-d', strtotime('next monday', strtotime($RealizationDays)));	
			}
		}	
		$not_to_show_old_format=true;
		$datecheck = "2020-09-01";
		$datecheck = strtotime($datecheck);
		$order_Created_date = $this->getOrderCreatedDate($order_id);
		$order_Created_date = strtotime($order_Created_date);
		if($RealizationDays!=0)
		{
			$transaction_complete_date=date('Y-m-d',strtotime($RealizationDays));
			$sales_confirmation_date =$transaction_complete_date; 
			$todayStamp = strtotime($transaction_complete_date);
			$numOfDays = date('t', $todayStamp);
			$base = strtotime('+'.$numOfDays.' days', strtotime(date('Y-m-01', $todayStamp)));
			$transaction_complete_modified = date('Y-m-25', $base);
			$datetocheck = strtotime("2020-10-01");
			if(strtotime($RealizationDays) >= $datetocheck)
			{
				$not_to_show_old_format=false;
				$day = date("d", strtotime($sales_confirmation_date));
				if($day<=15)
				{
					$transaction_complete_modified = date('Y-m-30',strtotime($sales_confirmation_date));
				}
				if($day>=16)
				{
					$day = date("d", strtotime($sales_confirmation_date));
					$transaction_complete_modified = date('Y-m-15', strtotime("first day of +1 month", strtotime($sales_confirmation_date)));	
				}
			}
		}			
		$creditmemo_grand_amount='';
		$refundsettlementbalance='';
		
		$raz_cgst=0;
		$raz_sgst=0;
		$raz_igst=0;
		$raz_utgst=0;
		$raz_igst = $razor_pay_gst_igst+$razor_pay_gst_cgst+$razor_pay_gst_sgst+$razor_pay_gst_ugst;
		
		$settlement_due_date=0;

		/* calculate  gst nature */
		$cgst=$order->getCgstAmount();
		$sgst=$order->getSgstAmount();
		$igst=$order->getIgstAmount();
		$utgst=$order->getUtgstAmount();
		if($igst!='0.0000')
		{
			$gst_nature='IGST';	
		}
		else if($cgst!='0.0000' && $sgst!='0.0000')
		{
			$gst_nature='CGST/SGST';	
		}	
		else if($cgst!='0.0000' && $utgst!='0.0000')
		{
			$gst_nature='CGST/UTGST';	
		}

		if($datecheck>$order_Created_date && $type!='refund' && $not_to_show_old_format)
		{
			$order_date_type="old_date_format";
			$transaction_complete_modified = $settlement_due_date;
		}

		if($type=='refund')
		{
			$payment_type='refund';
			$payment_id='Offline Refund';
			$creditmemo_details = $this->getCreditMemo($order_id);
			$return_days = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
			$credit_memo_increment_id = $creditmemo_details['increment_id'];
			$credit_memo_created_date = $creditmemo_details['created_at'];
			$creditmemo_grand_amount = $creditmemo_details['grand_total'];
			$invoice_increment_id = $credit_memo_increment_id;
			$order_cr_date = $credit_memo_created_date;
			$sales_confirmation_date = date("Y-m-d",strtotime($credit_memo_created_date));
			$transaction_complete_modified = date ("Y-m-d", strtotime ($sales_confirmation_date ."+".$return_days." days"));
			$net_amount_rec_nodal='0';
			$nodal_balance='0';
			$logistic_tds='0';
			$shipping_gst_tcs='0';
			$amount_due_to_logistic='0';
			$balance_in_nodal='0';
			$gross_comm_due='0';
			$gst_gross_comm_due='0';
			$net_comm='0';
			$gst_tcs='0';
			$tds_pi_comm='0';
			$net_remittance_pi='0';
			$balance_in_nodal_pi='0';
			$remittance_to_seller='0';
			$refundsettlementbalance = ($grand_total-$creditmemo_grand_amount-$razor_pay_total_charge_amount) ;
		}
		
		if(!empty($refund_settlement_balance))
		{
			$amount_due_to_pi = $refund_settlement_balance; 
		}else{
			$amount_due_to_pi = $net_remittance_pi;
		} 

		$razorpay_charges_net_tds=$razor_pay_total_charge_amount-$razorpay_tds;
		
		
		if($delivery_date!=0 && $RealizationDays!=0 || $type=='refund' )
		{
			$eligibale_for_settlemet=false;	
			if($type!='refund')
			{
				$date1 = date('Y-m-d');
				$date2 = date("Y-m-d", strtotime($RealizationDays));
				$eligibale_for_settlemet =  $date1 >= $date2 ? true : false;
			}
			if($eligibale_for_settlemet || $type=='refund')
			{
				$sql = "INSERT INTO `om_settlement_report_partial` set
				`order_num` =  '$order_inc_id',
				`order_date`='$order_cr_date',
				`buyer_id`='$buyer_id',
				`order_type`='$payment_type',
				`payment_id`='$payment_id',
				`invoice_ref_num`='$invoice_increment_id',
				`invoice_date`='$order_cr_date',
				`seller_code`='$seller_id',
				`seller_name`='$seller_comp_nam',
				`delivery_date`='$delivery_date',
				`sales_return_confirmation_date`='$sales_confirmation_date',
				`transaction_completion_date`='$transaction_complete_modified',
				`basic_sale_amount`='$baseprice',
				`nature_of_gst`='$gst_nature',
				`igst_sgst_ugst_amount`='$total_gst',
				`listed_price`='$subtotal',
				`shipping_charges_basic`='$shipping_base',
				`shipping_charges_gst`='$shipping_gst',
				`rate_percentage_shipping`='$shipping_gst_rate',
				`igst_shipping`='$shipping_igst',
				`cgst_shipping`='$shipping_cgst',
				`sgst_shipping`='$shipping_sgst',
				`utgst_shipping`='$shipping_ugst',
				`shipping_charges_total`='$shipping_expense',
				`total_sale_invoice_value`='$grand_total',
				`amount_received_in_payment_gateway`='$grand_total',
				`payment_method`='$razor_method',
				`card_type`='$razor_card_type',
				`card_network`='$razor_network',
				`payment_gateway_pg_charges`='$razor_pay_fee_amount',
				`gst_on_pg_charges`='$razor_pay_gst_amount',
				`rate_percent_razor`='$razor_pay_gst_gst_rate',
				`igst_razor`='$raz_igst',
				`cgst_razor`='$raz_cgst',
				`sgst_razor`='$raz_sgst',
				`utgst_razor`='$raz_utgst',
				`payment_gateway_charges_invoice_value`='$razor_pay_total_charge_amount',
				`razorpay_tds`='$razorpay_tds',
				`payment_gateway_charges_net_of_tds`='$razorpay_charges_net_tds;',
				`net_amount_received_in_nodal_account`='$net_amount_rec_nodal',
				`nodal_charges_monthly`='$nodal_charges',
				`balance_in_nodal_first`='$nodal_balance',
				`logistic_tds`='$logistic_tds',
				`gst_tcs_1_on_supplies_to_deposited_by_pi`='$shipping_gst_tcs',
				`amount_due_to_logistics`='$amount_due_to_logistic',
				`balance_in_nodal_second`='$balance_in_nodal',
				`gross_comm_due`='$gross_comm_due',
				`gst_on_comm_18_percent`='$gst_gross_comm_due',
				`net_commission`='$net_comm',
				`gst_tcs_1_on_basic_sale_amt`='$gst_tcs',
				`tds_on_pi_comission`='$tds_pi_comm',
				`due_from_seller_to_pi`='$gross_remittance_pi',
				`amount_due_to_pi`='$amount_due_to_pi',
				`balance_to_be_in_nodal_if_paid_to_pi`='$balance_in_nodal_pi',
				`amount_due_to_seller`='$remittance_to_seller',
				`order_id`='$order_id',
				`report_type`='$type',
				`refunded_amount`='$creditmemo_grand_amount'
			";
			$connection->query($sql); 
			}
	       
		}
		
	}	
	
	public function check_settlement_report($order_inc_id)
	{
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = $connection->select()->from('settlement_report')->where('order_id = ?', $order_inc_id);
		$result = $connection->fetchAll($select);	
		if(!empty($result)) { return 1;}
		else 
		{
			return 0;
		}		
	}
	
	public function checkiforderrefunded($increment_id)
	{  
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$refunded_payment_query = "SELECT `entity_id` FROM `sales_order` where `state`='closed' and increment_id='$increment_id'";
		$results = $connection->fetchAll($refunded_payment_query);
		$status =  !empty($results) ? $results[0]['entity_id']:0;
		return $status;
	}

	public function getCreditMemo($order_id)
	{
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = "select * from sales_creditmemo where order_id = $order_id";
		$result = $connection->fetchAll($select);
		$status = empty($result) ?  0: $result[0];
		return $status;
	}

	public function check_refund_settlement_report($order_inc_id){
		$isorderrefunded =  $this->checkiforderrefunded($order_inc_id);
		if(empty($isorderrefunded)){ /* here it means opposite as  0  will generate report */ return 1;  }
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = "Select  * from settlement_report where order_id='$order_inc_id' and type='refund'";
		$result = $connection->fetchAll($select);	
		$status =!empty($result) ? 1:0;   
		return $status;
	}

/* integrating real time customisations  ritesh@01/02/2021 */
public function getCreditNoteCreatedDate($order_idd)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$sqleee = "SELECT `created_at` FROM `sales_creditmemo` where order_id=$order_idd";  
		$created_at = $connection->fetchOne($sqleee);
		if(!empty($created_at)) {
			return $created_at;
		}
		return 0;
	}

	public function checkifshippingbypassed($order_id) 
	{
		$coupon =$this->_objectManager->create('Magento\SalesRule\Model\Coupon');
		$saleRule = $this->_objectManager->create('Magento\SalesRule\Model\Rule');
		$order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($order_id);
		$couponCode = $order->getCouponCode();
		$ruleId =   $coupon->loadByCode($couponCode)->getRuleId();
		$rule = $saleRule->load($ruleId);
		$freeShippingCoupon = $rule->getSimpleFreeShipping();
		if($freeShippingCoupon){
			$sql = "Select * FROM sales_shipment where order_id='$order_id'";  
			$results = $this->connection->fetchAll($sql);
			$status = !empty($results) ? $results[0]['created_at']:0; 
			return $status;
		}
	}

	public function getOrderCreatedDate($order_idd){
		$resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$sqleee = "SELECT `created_at` FROM `sales_order` where entity_id=$order_idd";  
		$created_at = $connection->fetchOne($sqleee);
		if(!empty($created_at)){ return $created_at;}
		return 0;
	}
	public function getOrderTrackingDeliveryAndPickkupDate($order_id,$type)
	{
		$tracking_dates = array("pickup_date"=>"","delivery_date"=>"");
		$sales_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\Reports\sales');
		$array=$sales_helpers->getOrderTrackingOM($order_id,$type);
		if(!empty($array) && is_array($array)) {
			$tracking_dates['pickup_date']=$array['pickup_date'];   
			$tracking_dates['delivery_date']=$array['delivered_date'];
		}
		return $tracking_dates;
	}

	public function checkReportType($order_id,$order_inc_id)
	{
		$eligibility = array('is_eligible'=>true,"report_type"=>"normal");
		$checkRmaStatus = $this->checkRmaStatus($order_inc_id);	
		if($checkRmaStatus["rma_requested"])
		{
			if($checkRmaStatus["rma_status"]=="pending")
			{
				/* this means rma is in between */
				$eligibility['is_eligible']=false;
				$eligibility['report_type']="rmainbetween";
				return $eligibility;
			}
			if($checkRmaStatus["rma_status"]=="solved")
			{
				/* this means rma is in between */
				$eligibility['is_eligible']=true;
				$eligibility['report_type']="refund";
			}
		}

	}

	public function checkReportShowingLogic($order_id,$pickup_date,$deliveryDate,$report_type)
	{


	}

	public function orderedDelivered()
	{

	}

	

	public function checkRmaStatus($order_inc_id)
	{
		$checkrmastatus=array("rma_requested"=>false,"rma_status"=>"none");
		$sql = "SELECT * FROM `wk_rma` where increment_id = '$order_inc_id'";
		$results = $this->connection->fetchAll($sql);
		if(!empty($results))
		{
			$checkrmastatus['rma_requested']=true;
			$final_status = $results['final_status'];
			if($final_status==0)
			{
				/* this means rma is in between and seller has not taken any action @ritesh05022021*/
				$checkrmastatus['rma_status']="pending";
			}
			if($final_status==2)
			{
				/* this means rma is declined @ritesh05022021*/
				$checkrmastatus['rma_status']="declined";
			}
			if($final_status == 3)
			{
				/* this means rma is approved @ritesh05022021*/
				$checkrmastatus['rma_status']="solved";
			}
		}
		return $checkrmastatus;
	}
	public function orderRefundStatus($order_id)
	{
		/* 1  => refund not generated , 2=>full refund generated , 3=>partial refund generated */
		echo $itemCount = $this->connection->fetchOne("SELECT count(item_id) FROM `sales_order_item` where order_id = $order_id AND parent_item_id IS NULL");
		echo $returnItemCount = $this->connection->fetchOne("SELECT count(entity_id) FROM `sales_creditmemo_item` where parent_id = $order_id");
		if(empty($returnItemCount))
		{
			return 1;
		}
		else  if($itemCount == $returnItemCount)
		{
			return 2;
		}
		else if($itemCount > $returnItemCount)
		{
			return 3;
		}
	}

	public function get_order_gst_details($order_increment_id)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();	
		$order_inc_id=$order_increment_id;
		$tax_helpers = $this->_objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId($order_increment_id); 
		$order_id=$order->getId();
		$orderArray=$order->getData();	
		$order_shipping_amount = ($order->getData('shipping_incl_tax') - $order->getData('base_shipping_refunded'));
		
		$logistic_tax=$tax_helpers->tax_logistic_tax($order_shipping_amount,$order_inc_id); 
		
		$cgst_amount=0;
		$sgst_amount=0;
		$igst_amount=0;
		$utgst_amount=0;
		$total_gst=0;
		
		 
		
		
		if(isset($orderArray['cgst_amount']))
		{
			$cgst_amount=$orderArray['cgst_amount'];
		}	
		
		if(isset($orderArray['sgst_amount']))
		{
			$sgst_amount=$orderArray['sgst_amount'];
		}	
		
		if(isset($orderArray['igst_amount']))
		{
			$igst_amount=$orderArray['igst_amount'];
		}	
		
		
		
		if(isset($orderArray['utgst_amount']))
		{
			$utgst_amount=$orderArray['utgst_amount'];
		}	
		
		$num_igst_amount=(int)$igst_amount;
		$num_sgst_amount=(int)$sgst_amount;
		
		
		$shipping_igst=0;
		$shipping_cgst=0;
		$shipping_sgst=0;
		$shipping_utgst=0;
		
		
		
		
		if($num_igst_amount!=0)
		{
			$shipping_igst=$logistic_tax;
		}
		else
		{	
			$logistic_tax=$logistic_tax/2;
			$shipping_cgst=$logistic_tax;
			if($num_sgst_amount!=0)
			{
				$shipping_sgst=$logistic_tax;
			}
			else
			{
				$shipping_utgst=$logistic_tax;
			}		
			
		}		
		$total_gst=$cgst_amount+$sgst_amount+$igst_amount+$utgst_amount; 
		$total_shipping_gst=$shipping_igst+$shipping_cgst+$shipping_sgst+$shipping_utgst;
		$array=array(
			'igst'=>$igst_amount,
			'cgst'=>$cgst_amount,
			'sgst'=>$sgst_amount,
			'utgst'=>$utgst_amount,
			'total_gst'=>$total_gst,
			'shipping_igst'=>$shipping_igst,
			'shipping_cgst'=>$shipping_cgst,
			'shipping_sgst'=>$shipping_sgst,
			'shipping_utgst'=>$shipping_utgst,
			'total_shipping_gst'=>$total_shipping_gst,
		
		);
		return $array;
	}	

}
