<?php
declare(strict_types=1);

namespace OM\GenerateReport\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
class ProductWeight extends AbstractHelper
{
	public function __construct(
		\Magento\Framework\App\ResourceConnection $resource,
		\Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
		$this->_objectManager = $objectmanager;
		$this->connection = $resource->getConnection();
	}

	public function checkWeightNotFoundEmail()  
	{

  }

  public function productWeightEmail()  
	{
		return  explode(',',$this->scopeConfig->getValue('automated_report_sharing/productweight/email'));
	}

  public function getSellerEmail()  
	{
	}

  public function isSellerEnabled()  
	{

	}

}