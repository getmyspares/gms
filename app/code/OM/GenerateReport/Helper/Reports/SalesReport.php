<?php 
/**
 * Copyright Â©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OM\GenerateReport\Helper\Reports;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Customer\Api\CustomerRepositoryInterface;
use  OM\GenerateReport\Helper\EmailReport;

class SalesReport extends AbstractHelper
{

    public function __construct(
        ResourceConnection $resourceConnection,
        CustomerRepositoryInterface $customerRepository,
        EmailReport $emailReport,
        Context $context
    )
    {
        $this->connection = $resourceConnection->getConnection();
        $this->customerRepository = $customerRepository;
        $this->emailReport = $emailReport;
        parent::__construct($context);
    }

    public function getSalesReportSeller()
    {
        $seller_array=[];
        $query = "Select distinct seller_code from  om_sales_report";
        $seller_ids = $this->connection->fetchAll($query);
            
        foreach($seller_ids as $seller_data)
        {
            $seller_id = $seller_data['seller_code'];
            $seller_name = $this->getSellerNameById($seller_id);
            /* seller name will not be thjere if seller is deleted @ritesh5april2022 */
            if($seller_name)
            {
                $seller_array[]=array("seller_name"=>$seller_name,"seller_id"=>$seller_id);
            }
        }   
        return $seller_array;
    }

    public function getSellerNameById($id)
    {
        /* this should be in try catch as , 
            if the seller is already deleted , magento throw customer not found error  
            @ritesh5april2022
            */
        try{
            $customer = $this->customerRepository->getById($id);
            return $customer->getFirstname()." ".$customer->getLastname();
        } catch(\Exception $e)
        {

            /* return null if customer deleted instead of error */
            return null;
        }
    }

    public function getResultData($request,$lUserId){
        $connection = $this->connection;
        $query = $this->createQueryString($request,$lUserId);
       
        $perpage    = isset($request['perpage']) ? $request['perpage'] : 15;
        $page       = isset($request['page']) ? $request['page'] : 1;
        $countTotal = $connection->query($query)->rowCount();
        
        $offset     = ($page*$perpage)-$perpage;
        $total_page = ceil($countTotal/$perpage);
        $active="";
        $getU = $request;
        unset($getU['page']);
        
        $pagingData = '';

        if($total_page > 1){
            
            if($page>1){
                $getU['page'] = 1;
                $str = http_build_query($getU, '', '&');
                $pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sales.php?'.$str.'">First</a></li>';
                
                $getU['page'] = $page-1;
                $str = http_build_query($getU, '', '&');
                $pagingData.='<li class="paginate_button"><a href="/reports/om_sales.php?'.$str.'">Previous</a></li>';
            } 		
            
            if($total_page > 10){
                $end = ($page+10) <= $total_page ? ($page+10) : $total_page;
                $start =  $end - 10;
            }else{
                $end = $total_page;
                $start =  1;
            }
            
            for($i=$start;$i<=$end;$i++){ 
                $getU['page'] = $i;
                $str = http_build_query($getU, '', '&');
                $active = ($page==$i) ? ' active':'';
                $pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sales.php?'.$str.'">'.$i.'</a></li>';
            }
            
            if($page < $total_page){
                $getU['page'] = $page+1;
                $str = http_build_query($getU, '', '&');
                $pagingData.='<li class="paginate_button"><a href="/reports/om_sales.php?'.$str.'">Next</a></li>';
                
                $getU['page'] = $total_page;
                $str = http_build_query($getU, '', '&');
                $pagingData.='<li class="paginate_button '.$active.'"><a href="/reports/om_sales.php?'.$str.'">Last</a></li>';
            }
            
        }

        $query." ORDER BY order_increment_id DESC LIMIT $offset,$perpage";
        $datas = $connection->fetchAll($query." ORDER BY order_increment_id DESC LIMIT $offset,$perpage");
        $tdatas = $connection->fetchAll($this->createQueryStringTotal($request,$lUserId));

        $return = [];
        $return['tdatas']  = $tdatas;
        $return['datas']  = $datas;
        $return['paging'] = $pagingData;
        return $return;	
    }

    public function createQueryStringTotal($request,$lUserId)
    {
        $query = 'SELECT so.increment_id,count(so.increment_id) as total_orders, sum(so.total_invoiced) as total_invoiced , sum(so.total_refunded) as total_refunded FROM `sales_order` so ';
        $where = [];
        $whereStr = '';
        
        if($lUserId > 0 ){
            $query.=' JOIN marketplace_orders mo ON mo.order_id=so.entity_id ';
            $where[] = "mo.seller_id = $lUserId";
        }
        
        
        if( isset($request['search']) || isset($request['export_data']) ){
            
            if( isset($request['order_id']) && $request['order_id']!='' && isset($request['order_id_to']) && $request['order_id_to']!='' ){
                $where[] = "increment_id BETWEEN '".$request['order_id']."' AND '".$request['order_id_to']."'";
            }else{
                if( isset($request['order_id']) && $request['order_id']!='' ){
                    $where[] = "increment_id LIKE '%".$request['order_id']."%'";
                }
                
                if( isset($request['order_id_to']) && $request['order_id_to']!='' ){
                    $where[] = "increment_id LIKE '%".$request['order_id_to']."%'";
                }	
            }
            
            if( isset($request['status']) && $request['status']!='' ){
                if(in_array('refudnandrefunded',$request['status'])) {
                    $request['status'] = array_diff( $request['status'], ['refudnandrefunded'] );
                    $request['status'][] = 'Refund';
                    $request['status'][] = 'Refunded';
                }
                $where[] = "status IN  "."('" . implode ( "', '", $request['status'] ) . "')";
            }
            
            
            if( isset($request['from_date']) && $request['from_date']!='' ){
                $date = date('Y-m-d H:i:s',strtotime($_GET['from_date'])-19800);
                $where[] = "so.created_at > '$date'";
            }
            if( isset($request['to_date']) && $request['to_date']!='' ){
                $date = date('Y-m-d H:i:s',strtotime($_GET['to_date'])-19800+86399);
                $where[] = "so.created_at < '$date'";
            }
        }
        
        $where[] = "so.status NOT IN ('pending_payment','pending','canceled') ";
        if(!empty($where)){
            $whereStr = ' WHERE '.implode(' AND ', $where);
        }
        return $query.$whereStr;
    }  
    
    function createQueryString($request,$lUserId){
        
        $query = 'SELECT `sor`.`o_created_by`,osr.*, oeas.`order_status`, oeas.`pickup_date`, oeas.`awb_number`, oeas.`delivery_date`
        FROM `om_sales_report` osr 
        JOIN om_ecom_api_status oeas ON osr.order_increment_id=oeas.increment_id 
        JOIN sales_order sor ON sor.increment_id=osr.order_increment_id ';
        $where = [];
        $whereStr = '';
        
        if( isset($request['search']) || isset($request['export_data']) ){
            
            if( isset($request['order_id']) && $request['order_id']!='' && isset($request['order_id_to']) && $request['order_id_to']!='' ){
                $where[] = "order_increment_id BETWEEN '".$request['order_id']."' AND '".$request['order_id_to']."'";
            }else{
                if( isset($request['order_id']) && $request['order_id']!='' ){
                    $where[] = "order_increment_id LIKE '%".$request['order_id']."%'";
                }
                
                if( isset($request['order_id_to']) && $request['order_id_to']!='' ){
                    $where[] = "order_increment_id LIKE '%".$request['order_id_to']."%'";
                }	
            }
            
            if( isset($request['status']) && $request['status']!='' ){
                if(in_array('refudnandrefunded',$request['status'])) {
                    $request['status'] = array_diff( $request['status'], ['refudnandrefunded'] );
                    $request['status'][] = 'Refund';
                    $request['status'][] = 'Refunded';
                }
                $where[] = "oeas.order_status IN  "."('" . implode ( "', '", $request['status'] ) . "')";
            }
            if( isset($request['from_date']) && $request['from_date']!='' ){
                $date = date('Y-m-d H:i:s',strtotime($_GET['from_date'])-19800);
                $where[] = "order_date > '$date'";
            }
            if( isset($request['to_date']) && $request['to_date']!='' ){
                $date = date('Y-m-d H:i:s',strtotime($_GET['to_date'])-19800+86999);
                $where[] = "order_date < '$date'";
            }
            
        }
        /* seller functionality needed in backend too */
        if($lUserId > 0 ){
            $where[] = "osr.seller_code = $lUserId";
        }
        if(!empty($where)){
            $whereStr = ' WHERE '.implode(' AND ', $where) ." and osr.movement_type=oeas.movement_type";
        } else   {
            $whereStr = 'where osr.movement_type=oeas.movement_type';
        }
        return $query.$whereStr;
    }

    public function exportData($request,$lUserId){
        $connection=$this->connection;
        $query = $this->createQueryString($request,$lUserId);
        $datas = $connection->fetchAll($query.' ORDER BY order_increment_id DESC');
        $report_send_helper = $this->emailReport;
        $columns = $report_send_helper->getSalesColumns();
        $csvHeader = array_values($columns);
        $fp = fopen('php://memory', 'w');
        fputcsv( $fp, $csvHeader,",");
        foreach ($datas as $data) {
            $insertArr=$report_send_helper->generateSalesReportRow($data);
            fputcsv($fp, $insertArr, ",");
        }
        
        $filename = 'Sales Report-'.date('Y-m-d-H-i-s').'.csv';
        fseek($fp, 0);
        header('Content-Type: text/csv');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($fp);
        exit;    	
    }

}

?>