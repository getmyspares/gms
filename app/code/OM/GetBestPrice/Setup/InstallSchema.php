<?php

namespace OM\GetBestPrice\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('CREATE TABLE `om_get_best_price` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `free_text` text, 
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1');


		

		}

        $installer->endSetup();

    }
}
