<?php
namespace OM\GetBestPrice\Model;

class Getbestprice extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\GetBestPrice\Model\ResourceModel\Getbestprice');
    }
}
?>