<?php

namespace OM\GetBestPrice\Model\ResourceModel\Getbestprice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OM\GetBestPrice\Model\Getbestprice', 'OM\GetBestPrice\Model\ResourceModel\Getbestprice');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>