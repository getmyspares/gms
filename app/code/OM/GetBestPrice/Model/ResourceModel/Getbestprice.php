<?php
namespace OM\GetBestPrice\Model\ResourceModel;

class Getbestprice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('om_get_best_price', 'id');
    }
}
?>