<?php

namespace OM\GetBestPrice\Controller\Adminhtml\getbestprice;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPagee;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('OM_GetBestPrice::getbestprice');
        $resultPage->addBreadcrumb(__('OM'), __('OM'));
        $resultPage->addBreadcrumb(__('Manage item'), __('Manage Best Price Requests'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Best Price Requests'));

        return $resultPage;
    }
}
?>
