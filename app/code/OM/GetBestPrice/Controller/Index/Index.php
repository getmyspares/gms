<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OM\GetBestPrice\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{
   /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
 
    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;
 
 
    /**
     * View constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory, JsonFactory $resultJsonFactory)
    {
 
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
 
        parent::__construct($context);
    }

    public function execute()
    {
		$data = $this->getRequest()->getPostValue();
		$action = $this->getRequest()->getParam('action');
		if ($data && $action=='save') {
            $model = $this->_objectManager->create('OM\GetBestPrice\Model\Getbestprice');
			$model->setData($data);
			$model->save();
			echo "success"; exit;
        }
        $result = $this->_resultJsonFactory->create();
        $resultPage = $this->_resultPageFactory->create();
        $block = $resultPage->getLayout()
                ->createBlock('OM\GetBestPrice\Block\Index\Index')
                ->setTemplate('OM_GetBestPrice::getbestprice_index_index.phtml')
                ->toHtml();
 
		echo $block; exit;
        //$result->setData(['output' => $block]);
        //return $result;
    }
}
