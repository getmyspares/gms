<?php

namespace OM\GetBestPrice\Block\Index;


class Index extends \Magento\Framework\View\Element\Template {

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Customer\Model\Session $customer,
        array $data = []
    ) {
        $this->customer = $customer;
        parent::__construct($context, $data);

    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }


    public  function getName()
    {
        $value="";
        if($this->customer->isLoggedIn())
        {
            $value = $this->customer->getCustomer()->getData('firstname');
        }
        return $value;
    }

    public  function getEmail()
    {
        $value="";
        if($this->customer->isLoggedIn())
        {
            $value = $this->customer->getCustomer()->getData('email');
        }
        return $value;
    }

    public  function getMobile()
    {
        $value="";
        if($this->customer->isLoggedIn())
        {
            $value = $this->customer->getCustomer()->getData('mobile');
        }
        return $value;
    }

    public  function getPostcode()
    {
        $value="";
        if($this->customer->isLoggedIn())
        {

            $value = $this->customer->getCustomer()->getDefaultShippingAddress()->getData('postcode');
        }
        return $value;
    }

    public  function getCity()
    {
        $value="";
        if($this->customer->isLoggedIn())
        {
            $value = $this->customer->getCustomer()->getDefaultShippingAddress()->getData('city');
        }
        return $value;
    }
}