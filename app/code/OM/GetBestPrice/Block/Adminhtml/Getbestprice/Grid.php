<?php
namespace OM\GetBestPrice\Block\Adminhtml\Getbestprice;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \OM\GetBestPrice\Model\getbestpriceFactory
     */
    protected $_getbestpriceFactory;

    /**
     * @var \OM\GetBestPrice\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \OM\GetBestPrice\Model\getbestpriceFactory $getbestpriceFactory
     * @param \OM\GetBestPrice\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \OM\GetBestPrice\Model\GetbestpriceFactory $GetbestpriceFactory,
        \OM\GetBestPrice\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_getbestpriceFactory = $GetbestpriceFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_getbestpriceFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'product_id',
					[
						'header' => __('Product ID'),
						'index' => 'product_id',
					]
				);
				
				$this->addColumn(
					'product_name',
					[
						'header' => __('Product Name'),
						'index' => 'product_name',
					]
				);
				
				$this->addColumn(
					'qty',
					[
						'header' => __('Qty'),
						'index' => 'qty',
					]
				);
				
				$this->addColumn(
					'customer_email',
					[
						'header' => __('Customer Email'),
						'index' => 'customer_email',
					]
				);
				
				$this->addColumn(
					'customer_name',
					[
						'header' => __('Customer Name'),
						'index' => 'customer_name',
					]
                );
                
                $this->addColumn(
					'pincode',
					[
						'header' => __('Mobile'),
						'index' => 'pincode',
					]
				);
    
/** removing  1 column as  not needed  @ritesh7january2021*/                
/* 				
				
				$this->addColumn(
					'city',
					[
						'header' => __('City'),
						'index' => 'city',
					]
				); */
				
				$this->addColumn(
					'created_at',
					[
						'header' => __('Created At'),
						'index' => 'created_at',
						'type'      => 'datetime',
					]
				);

					


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('getbestprice/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('getbestprice/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('OM_GetBestPrice::getbestprice/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('getbestprice');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('getbestprice/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('getbestprice/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('getbestprice/*/index', ['_current' => true]);
    }

    /**
     * @param \OM\GetBestPrice\Model\getbestprice|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'getbestprice/*/edit',
            ['id' => $row->getId()]
        );
		
    }

	

}
