<?php
namespace OM\GetBestPrice\Block\Adminhtml\Getbestprice\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('getbestprice_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Get Best Price Information'));
    }
}
