<?php
namespace HK\Localwishlist\Controller\Index;

use Magento\Framework\App\Action;
use Magento\Catalog\Model\Product\Exception as ProductException;
use Magento\Framework\Controller\ResultFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Cart extends \Magento\Wishlist\Controller\AbstractIndex
{
    /**
     * @var \Magento\Wishlist\Controller\WishlistProviderInterface
     */
    protected $wishlistProvider;

    /**
     * @var \Magento\Wishlist\Model\LocaleQuantityProcessor
     */
    protected $quantityProcessor;

    /**
     * @var \Magento\Wishlist\Model\ItemFactory
     */
    protected $itemFactory;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $cartHelper;

    /**
     * @var \Magento\Wishlist\Model\Item\OptionFactory
     */
    private $optionFactory;

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    protected $productHelper;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    public function __construct(
        Action\Context $context,
        \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider,
        \Magento\Wishlist\Model\LocaleQuantityProcessor $quantityProcessor,
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Wishlist\Model\Item\OptionFactory $optionFactory,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Framework\Escaper $escaper,
        \Magento\Wishlist\Helper\Data $helper,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
    ) {
        $this->wishlistProvider = $wishlistProvider;
        $this->quantityProcessor = $quantityProcessor;
        $this->itemFactory = $itemFactory;
        $this->cart = $cart;
        $this->optionFactory = $optionFactory;
        $this->productHelper = $productHelper;
        $this->escaper = $escaper;
        $this->helper = $helper;
        $this->cartHelper = $cartHelper;
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

   
    public function execute()
    {	
			
		
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $resultRedirect->setPath('*/*/');
        }

        $itemId = (int)$this->getRequest()->getParam('item');
        /* @var $item \Magento\Wishlist\Model\Item */
        $item = $this->itemFactory->create()->load($itemId);
        if (!$item->getId()) {
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
        $wishlist = $this->wishlistProvider->getWishlist($item->getWishlistId());
        if (!$wishlist) {
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
		
		 
        // Set qty
        $qty = $this->getRequest()->getParam('qty');
		$postQty = $this->getRequest()->getPostValue('qty');
	
        if ($postQty !== null && $qty !== $postQty) {
            $qty = $postQty;
        }
        if (is_array($qty)) {
            if (isset($qty[$itemId])) {
                $qty = $qty[$itemId];
            } else {
                $qty = 1;
            }
        }
        $qty = $this->quantityProcessor->process($qty);
        if ($qty) {
            $item->setQty($qty);
        }

        $redirectUrl = $this->_url->getUrl('*/*');
        $configureUrl = $this->_url->getUrl(
            '*/*/configure/',
            [
                'id' => $item->getId(),
                'product_id' => $item->getProductId(),
            ]
        );
		
        try {
            /** @var \Magento\Wishlist\Model\ResourceModel\Item\Option\Collection $options */
            $options = $this->optionFactory->create()->getCollection()->addItemFilter([$itemId]);
            $item->setOptions($options->getOptionsByItem($itemId));

            $buyRequest = $this->productHelper->addParamsToBuyRequest(
                $this->getRequest()->getParams(),
                ['current_config' => $item->getBuyRequest()]
            );
		
		
			//-- custom code --------------------------------
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$qty_limit = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('example_section_1/general_1/qty_limit_num');

			$req_qty = $item->getQty();
			$cards_qty = 0;
			//------ getting the product quantity in card 
			$all_data = $this->cart->getQuote()->getAllItems();
			$productId = $item->getProductId();// -- current wishlist product id 
			//print_r($all_data);
			foreach ( $all_data as $newitem) {
				if ($newitem->getProductId() == $productId) {
					$cards_qty = $newitem->getQty();
				}
			}
			
			$req_qty += $cards_qty;
			
			$error_message = "";
			//---------------- Getting the customer group id -----------------------
			$customerSession = $objectManager->get('Magento\Customer\Model\Session');
			if($customerSession->isLoggedIn()) {
				$custom_data = $customerSession->getData();
				$custom_group_id = $custom_data['customer_group_id'];
			}else{
				$custom_group_id ="";
			}
			
			if($custom_group_id != 4){
				if($req_qty > $qty_limit){
					$item->mergeBuyRequest($buyRequest);
					//$item->addToCart($this->cart, true);
					//$this->cart->save()->getQuote()->collectTotals();
					$wishlist->save();
					$error_message = "The quantity limit for the product is ".$qty_limit.". Please try again after changing the quantity.";
					$this->messageManager->addError(__($error_message));
				}else{
					$item->mergeBuyRequest($buyRequest);
					$item->addToCart($this->cart, true);
					$this->cart->save()->getQuote()->collectTotals();
					$wishlist->save();
				}	
			}else{
				$item->mergeBuyRequest($buyRequest);
				$item->addToCart($this->cart, true);
				$this->cart->save()->getQuote()->collectTotals();
				$wishlist->save();
				
			}
		
		   
            // $item->mergeBuyRequest($buyRequest);
            //$item->addToCart($this->cart, true);
            //$this->cart->save()->getQuote()->collectTotals();
            // $wishlist->save();
			
			
            if (!$this->cart->getQuote()->getHasError()) {
				if(!$error_message){
					$message = __(
						'You added %1 to your shopping cart.',
						$this->escaper->escapeHtml($item->getProduct()->getName())
					);
					$this->messageManager->addSuccess($message);
				}
				
            }

            if ($this->cartHelper->getShouldRedirectToCart()) {
                $redirectUrl = $this->cartHelper->getCartUrl();
            } else {
                $refererUrl = $this->_redirect->getRefererUrl();
                if ($refererUrl && $refererUrl != $configureUrl) {
                    $redirectUrl = $refererUrl;
                }
            }
        } catch (ProductException $e) {
            $this->messageManager->addError(__('This product(s) is out of stock.'));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addNotice($e->getMessage());
            $redirectUrl = $configureUrl;
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add the item to the cart right now.'));
        }

        $this->helper->calculate();

        if ($this->getRequest()->isAjax()) {
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(['backUrl' => $redirectUrl]);
            return $resultJson;
        }
        
        $resultRedirect->setUrl($redirectUrl);
        return $resultRedirect;
        
    }
}
