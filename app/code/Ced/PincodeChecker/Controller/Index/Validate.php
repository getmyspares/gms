<?php 

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */  
 
namespace Ced\PincodeChecker\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Validate extends Action
{
    /**
     * Default vendor dashboard page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    protected $resultPageFactory;

    protected $_request;
    protected $_response;
    protected $_objectManager;
    protected $_coreRegistry = null;
    protected $_helper;
    
     public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Ced\PincodeChecker\Helper\Data $helper,
        ScopeConfigInterface $scopeConfig,
        \OM\Pincodes\Model\Pincodes $pincodes,
        \Magento\Framework\Controller\Result\JsonFactory $resultjson
    ) {
        $this->_coreRegistry = $registry;
        $this->_helper = $helper;
        $this->_pincodes = $pincodes;
        $this->_scopeConfig = $scopeConfig;
        $this->resultJsonFactory = $resultjson;
        parent::__construct($context);
    }

    public function execute()
    {
        /* removing this as we need pincoding checking functionality on product page irrespective of module is disabled from backend or not @ritesh30june2021 */
        // if(!$this->_helper->isPinCodeCheckerEnabled())
        //     return false;

        $post_data = $this->getRequest()->getPost();
		
        $zip = $post_data['zip'];
        $product_id = $post_data['product_id'];
        $qty = $post_data['qty'];
        $zip = $post_data['zip'];
        $can_cod_flag  = false;
        $can_ship_flag = false;
        $days_to_deliver = '-';
        $zip_not_found = true;
        
        /* bad practice , removing object manager and using class in constructor instead @ritesh  
            $pincode_model = $this->_objectManager->create('OM\Pincodes\Model\Pincodes')
                                                ->getCollection()
                                                ->addFieldToFilter('is_active','1')
                                                ->addFieldToFilter('pincode',array('eq'=>$zip))
                                                ->getData();
        */

        $pincode_model = $this->_pincodes->getCollection()
                        //->addFieldToFilter('is_active','1')
                        ->addFieldToFilter('pincode',array('eq'=>$zip))
                        ->getData();
		
		$cod_text='';
		$ship_text=''; 
		$custom_msg=''; 
	   if(count($pincode_model)){
            foreach($pincode_model as $model){
                $can_ship_flag = true;
                if($model['cod_available'] == '1'){
                    $can_cod_flag = true;
                }
            
                $days_to_deliver = $model['days_to_deliver'];
				$plusone = $days_to_deliver+1;
            }
        
			if($can_cod_flag==true) {	
				//$cod_text='<span>COD Available</span>';
				$cod_text=' ';
			}
			
			if($can_ship_flag==true) {	
			
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
				$shipping_rate=$helpers->ecom_price_by_product($zip,$product_id,$qty);


				if($shipping_rate=='0')
				{
                    $shipping_rate+=$shipping_rate;
					$shipping_rate='Free Shipping'; 
					$ship_text='<span> Shipping charges: Free </span>';
				} 
				else
				{		
				$ship_text='<span>Shipping: ₹ '.$shipping_rate.'</span><span>Delivered in next '.$days_to_deliver.'-'.$plusone.' '.'Business Days</span>';
				}
                $ship_text='<span>Delivered in next '.$days_to_deliver.'-'.$plusone.' '.'Business Days</span>';
			}
			 
			$custom_msg=$cod_text.$ship_text;
			
		 
			
		
		
		}
        $msg = $this->getDeliveryDaysMessage((int)$days_to_deliver, $zip);
        
		
		$can_cod_flag = true;
		
		$response = array('cod' => $can_cod_flag,
                        'ship' => $can_ship_flag,
                        'days' => $msg,
                        'zip_not_found' => $zip_not_found,
                        'custom_msg' => $custom_msg
                    );  
                        

        $this->getResponse()->setBody($response);
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
        
    }

    protected function getDeliveryDaysMessage($days, $pincode){
        $codes = array('{{from-days}}', '{{to-days}}', '{{pincode}}');
        $msg = $this->_scopeConfig->getValue('pincode/general/delivery_text', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $margin_days = (int) $this->_scopeConfig->getValue('pincode/general/delivery_days_margin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        $replace_string = array($days, $days+$margin_days, $pincode);
        return str_replace($codes,$replace_string,$msg);
    }
}
