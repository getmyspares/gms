<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */ 

  
namespace Ced\PincodeChecker\Controller\Adminhtml\Pincodechecker;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($data = $this->getRequest()->getPost())
        {
            $model = $this->_objectManager->create('Ced\PincodeChecker\Model\Pincode');
            $id = $this->getRequest()->getParam('id');
            $post_data=$this->getRequest()->getPostValue();
            $zipcode = $post_data['zipcode'];
            if ($id) {
                $model->load($id);
            }
            else{
                $existing_pincode = $model->getCollection()->addFieldToFilter('zipcode', $zipcode)->getData();
                if(!empty($existing_pincode)){
                    $this->messageManager->addError(__('Zipcode already exists.'));
                    $this->_redirect("*/*/");
                    return;
                }

            }
            $model->addData($post_data);
            $model->save();
            $this->messageManager->addSuccess(__('Zip Code Saved Successfully.')); 
        }
        else
          $this->messageManager->addError(__('No data found to save.'));
        $this->_redirect("*/*/");
    	
   }
}