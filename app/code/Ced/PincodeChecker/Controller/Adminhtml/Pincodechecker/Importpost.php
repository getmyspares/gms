<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */ 
  
namespace Ced\PincodeChecker\Controller\Adminhtml\Pincodechecker;

use Magento\Backend\App\Action;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Filesystem\DirectoryList;

class Importpost extends \Magento\Backend\App\Action
{
    protected $_pincodehelper;

    protected $messageManager;

    protected $_storeManger;

    protected $_connection;

    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     */
    public function __construct(
      Action\Context $context,
      \Ced\PincodeChecker\Helper\Data $_pincodehelper,
      StoreManagerInterface $storeManager,
      \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\ResourceConnection $resource,
        \Psr\Log\LoggerInterface $logger
    )
    {   
        $this->_pincodehelper = $_pincodehelper;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_resource = $resource;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {  
      if(!$this->_pincodehelper->isPinCodeCheckerEnabled())
        return $this->_redirect('*/*/');


      if(!empty($_FILES['import_csv']['name'])){
        if(isset($_FILES['import_csv']['name'])){;
          $this->_importCsv($_FILES['import_csv']);
        }
      }

    }

    protected function _importCsv($csvFile){

      try{
         $columns = array('website_id', 'vendor_id', 'zipcode','can_cod', 'can_ship', 'days_to_deliver');

         $import = $this->_objectManager->create('Ced\PincodeChecker\Model\ResourceModel\Pincode')->importCsv($csvFile, $columns);
     
        if($import->_importErrors){
          $error = __('File has not been imported. See the following list of errors: %s', implode(" \n", $import->_importErrors));
          $this->messageManager->addErrorMessage($error);
        }
        
        $this->messageManager->addSuccessMessage(__('Zipcode CSV Imported Successfully, and empty rows will be skipped.'));
      }
      catch(\Exception $e)
      { 
        $this->messageManager->addErrorMessage($e->getMessage());
      }
      $this->_redirect('*/*/index');
    }

}