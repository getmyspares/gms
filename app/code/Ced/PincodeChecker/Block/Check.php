<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */
 
namespace Ced\PincodeChecker\Block;

use Magento\Framework\UrlFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;

class Check extends \Magento\Framework\View\Element\Template
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
	
	/**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    public $_objectManager;
	
	protected $urlModel;

	protected $session;

    public $_helper;

    public $_options;

    public $allowed_guest;

    protected $scopeConfig;

    public $_coreRegistry = null;
    /**
     * @param Context $context
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
		Session $customerSession,
		\Magento\Framework\ObjectManagerInterface $objectManager,
        \Ced\PincodeChecker\Helper\Data $helper,
		UrlFactory $urlFactory,
        \Magento\Framework\Registry $registry
    ){
		$this->session = $customerSession;
		$this->urlModel = $urlFactory;
		$this->_objectManager = $objectManager;
        $this->_helper = $helper;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    public function getZipCodeLabel(){
        return $this->_scopeConfig->getValue('pincode/general/pincode_label', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) ? $this->_scopeConfig->getValue('pincode/general/pincode_label', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) : __('Pincode : ');
    }
	
}
