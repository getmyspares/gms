<?php 

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\PincodeChecker\Block\Adminhtml\Pincode\Grid\Renderer;
 
class Cancod extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {
 
	protected $_vproduct;
	public $_objectManager;
	
	public function __construct(
			\Magento\Backend\Block\Context $context,
			\Magento\Framework\ObjectManagerInterface $objectManager,
			array $data = []
	) {
		$this->_objectManager = $objectManager;
		parent::__construct($context, $data);
	}
	
	public function render(\Magento\Framework\DataObject $row) {
		if($row->getCanCod()){
			return 'Yes';
		}
		else{
			return 'No';
		}
		
	}
}