<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */ 

namespace Ced\PincodeChecker\Block\Adminhtml\Pincode;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

	
	protected $_websiteFactory;
    /**
     * @var \SR\Weblog\Model\Status
     */
    protected $_group;
	
	protected $_options;

    protected $_objectManager;
 
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
		\Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\View\Model\PageLayout\Config\BuilderInterface $pageLayoutBuilder,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ){
		$this->_websiteFactory = $websiteFactory;
        $this->pageLayoutBuilder = $pageLayoutBuilder;
        $this->moduleManager = $moduleManager;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $backendHelper, $data);
    }
 
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('pincodechecker');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return $this->getStore($storeId);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {   
        $store = $this->_getStore();
        $collection = $this->_objectManager->get('Ced\PincodeChecker\Model\Pincode')->getCollection();                                                    
                
        $this->setCollection($collection);      
        try{
            return parent::_prepareCollection();
        }
        catch(\Exception $e){    
            echo $e->getMessage();die;
        }
    }
 
    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
		$this->addColumn('id', [
				'header'    => __('ID'),
				'align'     =>'right',
				'index'     => 'id',
                'width'     => '80px',
				'type'	  => 'text',
                'is_system' => true
			]
		);
		
		$this->addColumn('zipcode', [
				'header'        => __('Zipcode'),
				'align'         => 'left',
				'type'          => 'text',
				'index'         => 'zipcode',
			]
		);
		
        $this->addColumn('can_ship', [
				'header'    => __('Shipment Available'),
				'align'     =>'left',
				'index'     => 'can_ship',
                'type'      => 'options',
                'renderer' => 'Ced\PincodeChecker\Block\Adminhtml\Pincode\Grid\Renderer\Canship',
                'options' => [0 => 'No', 1 => 'Yes']
			]
		);
		
       $this->addColumn('can_cod', [
				'header'        => __('COD Available'),
				'align'     	=> 'left',
				'index'         => 'can_cod',
				'type'          => 'options',
                'renderer' => 'Ced\PincodeChecker\Block\Adminhtml\Pincode\Grid\Renderer\Cancod',
                'options' => [0 => 'No', 1 => 'Yes']
			]
        ); 
			
		$this->addColumn('days_to_deliver', [
                'header'        => __('Days To Deliver'),
                'align'         => 'left',
                'index'         => 'days_to_deliver',
                'type'          => 'text',
            ]
        );

         $this->addColumn('action', [
                'header' => __('Action'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => ['base' => '*/*/edit'],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'action',
                'is_system' => true
            ]
        );
		
        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }
 
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setTemplate('Magento_Catalog::product/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('id');
 
        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete Item(s)'),
                'url' => $this->getUrl('*/*/deletezip'),
                'confirm' => __('Are you sure?')
            ]
        );
 
        $statuses = array('0' => 'No', '1' => 'Yes');
        
        $this->getMassactionBlock()->addItem('can_ship_status',
                [
                'label'=> __('Change Can Ship(s) Status'),
                'url'  => $this->getUrl('*/*/shipstatus/', ['_current'=>true]),
                'additional' => [
                        'visibility' => [
                                'name' => 'ship_status',
                                'type' => 'select',
                                'class' => 'required-entry',
                                'label' => ('Status'),
                                'default'=>'-1',
                                'values' =>$statuses,
                        ]
                ]
                ]
        );

        $this->getMassactionBlock()->addItem('can_cod_status',
                [
                'label'=> __('Change Can COD(s) Status'),
                'url'  => $this->getUrl('*/*/codstatus/', ['_current'=>true]),
                'additional' => [
                        'visibility' => [
                                'name' => 'cod_status',
                                'type' => 'select',
                                'class' => 'required-entry',
                                'label' => ('Status'),
                                'default'=>'-1',
                                'values' =>$statuses,
                        ]
                ]
                ]
        );
        
        
                
        return $this;
    }

     /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }
 
}