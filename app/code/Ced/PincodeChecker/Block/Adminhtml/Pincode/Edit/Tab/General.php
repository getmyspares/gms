<?php
 
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */ 

namespace Ced\PincodeChecker\Block\Adminhtml\Pincode\Edit\Tab;

/**
 * Cms page edit form main tab
 */
class General extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    public $_objectManager;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
    	
        $this->_objectManager = $objectManager;
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('pincode_data');
        if ($this->_isAllowedAction('Magento_Cms::save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Edit Item')]);


        $fieldset->addField(
            'zipcode',
            'text',
            [
                'name' => 'zipcode',
                'label' => __('Zipcode'),
                'title' => __('Zipcode'),
                'required' => true,
                'class' => 'validate-zip'
            ]
        );

        $fieldset->addField(
            'can_ship',
            'select',
            [
                'name' => 'can_ship',
                'label' => __('Shipment Available'),
                'title' => __('Shipment Available'),
                'required' => true,
                'onchange' => 'changecod(this)',
                'options' => ['1' => 'YES','0' => 'NO']
            ]
        );

        $fieldset->addField(
            'can_cod',
            'select',
            [
                'name' => 'can_cod',
                'label' => __('COD Available'),
                'title' => __('COD Available'),
                'required' => true,
                'options' => ['1' => 'YES','0' => 'NO']
            ]
        );

        $fieldset->addField(
            'days_to_deliver',
            'text',
            [
                'name' => 'days_to_deliver',
                'label' => __('Days To Deliver'),
                'title' => __('Days To Deliver'),
                'required' => true,
                'class' => 'validate-not-negative-number'
            ]
        );

        $script = $fieldset->addField(
            'vendor',
            'hidden',
            [
                'name' => 'vendor',
                'value' => 'admin'
            ]
        );

        $script->setAfterElementHtml("<script type=\"text/javascript\">
            function changecod(selectObject) {
                var option = selectObject.value;  
                var element = document.getElementById('page_can_cod');
                element.value = option;
            }require([
                'jquery',
                'jquery/ui', 
                'jquery/validate',
                'mage/translate'
                ], function($){
            jQuery.validator.addMethod(
                'validate-zip', function (value) { 
                    return value.match(/^[a-z0-9\-\s]+$/i);
                }, $.mage.__('Enter Valid Zipcode'));
            });
            </script>");

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('General');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
    public function getAvailableStatuses()
    {
    	return [1 => __('Enabled'), 2 => __('Disabled')];
    }
    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
