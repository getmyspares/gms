<?php 

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\PincodeChecker\Observer; 

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\Redirect as ResultRedirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Url\DecoderInterface;

Class Validate implements ObserverInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    protected $resultPageFactory;
    protected $_urlManager;
    protected $_helper;
    protected $_checkoutSession;
    public function __construct(
        RequestInterface $request,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $urlManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Ced\PincodeChecker\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $checkoutSession
    ) {
        $this->_objectManager = $objectManager;
        $this->request = $request;
        $this->session = $customerSession;
        $this->_helper = $helper;
        $this->_scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_urlManager = $urlManager;
        $this->_checkoutSession = $checkoutSession;
    }
    /**
     *redirect on advance order link
     *
     *@param $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {     

        if(!$this->_helper->isPinCodeCheckerEnabled())
          return false;
        $event = $observer->getEvent();
        $method = $event->getMethodInstance();
        $result = $event->getResult();
        //$checkout_session = $this->_objectManager->create('\Magento\Checkout\Model\Session');
        $quote = $this->_checkoutSession->getQuote();
        if ($quote && $result->getIsAvailable() && $quote->getShippingAddress()->getPostcode()) {
          $zipcode = $quote->getShippingAddress()->getPostcode();
          $pincode_model = $this->_objectManager->create('Ced\PincodeChecker\Model\Pincode')
                                                ->getCollection()
                                                ->addFieldToFilter('vendor_id', 'admin')
                                                ->addFieldToFilter('zipcode', array('eq' => $zipcode))
                                                ->getData();

          $methods_to_restrict = explode(",", $this->_scopeConfig->getValue('pincode_general_methods_to_restrict', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
          if(count($pincode_model)){
              foreach($pincode_model as $model){
                if($model['can_ship']){
                  if ($method->getCode () == 'cashondelivery' && $model ['can_cod'] == '1') {
                        $result->setData('is_available',true);
                        return;
                        break;
                  }
                  elseif($method->getCode () == 'cashondelivery' && $model ['can_cod'] == '0'){
                        $result->setData('is_available',false);
                        return;
                        break;
                  }
                  else{
                    $result->setData('is_available',true);
                    return;
                    break;
                  } 
                }
                else{
                  $result->setData('is_available',false);
                  return;
                  break;
                }
              }
          }
          else{
              $result->setData('is_available',false);
              return;
          }
        }  

    }      
}
