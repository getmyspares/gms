<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author 		CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */
namespace Ced\PincodeChecker\Model;
/*
 * used to save data in Quote To OrderItem table
 * */
class MethodListPlugin
{

    /**
     * @var \Magento\Payment\Helper\Data
     * @deprecated
     */
    protected $paymentHelper;

    /**
     * @var \Magento\Payment\Model\Checks\SpecificationFactory
     */
    protected $methodSpecificationFactory;

    /**
     * @var \Magento\Payment\Api\PaymentMethodListInterface
     */
    private $paymentMethodList;

    /**
     * @var \Magento\Payment\Model\Method\InstanceFactory
     */
    private $paymentMethodInstanceFactory;

    protected $_objectManager;
    protected $resultPageFactory;
    protected $_helper;
    protected $_checkoutSession;

    /**
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param Checks\SpecificationFactory $specificationFactory
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Payment\Model\Checks\SpecificationFactory $specificationFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Ced\PincodeChecker\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $checkoutSession
    ) {
        $this->paymentHelper = $paymentHelper;
        $this->methodSpecificationFactory = $specificationFactory;
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @return \Magento\Payment\Model\MethodInterface[]
     * @api
     */
    public function aftergetAvailableMethods($subject, $result)
    { 
        if(!$this->_helper->isPinCodeCheckerEnabled())
          return $result;

        $quote = $this->_checkoutSession->getQuote();
        if ($quote && $quote->getShippingAddress()->getPostcode()) {
            $pincode_model = $this->_objectManager->create('Ced\PincodeChecker\Model\Pincode');
            
            /*if no values exist in DB then return the  previous result*/  
            if (count($pincode_model->getCollection()->getData()) <= 0) {
                return $result;                
            }
            $zipcode = $quote->getShippingAddress()->getPostcode();
            $pincode_model = $pincode_model->load($zipcode, 'zipcode')->getData();

            if(!empty($pincode_model)){
                if($pincode_model['can_ship']){
                    foreach($result as $key => $payment){
                        if($payment->getCode() == 'cashondelivery' && $pincode_model['can_cod'] == '0'){
                            unset($result[$key]);
                       }
                    }
                    return $result;
                }
                else{
                    return array();
                }
            }
            else{
                return array();
            }
        }else{
            return $result;
        }
        /*foreach($result as $payment){
            echo $payment->getCode();die;
        }*/
    }
}