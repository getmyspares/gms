<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_PincodeChecker
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\PincodeChecker\Model;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Filesystem\DirectoryList;

class Import extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_context;
    protected $_storeManger;
    protected $_producttype;
    protected $_objectManager;
    protected $scopeConfig;
    protected $_filesystem;
    protected $_connection;
    protected $_resource;
    protected $_logger;
 
    /**
     * @param \Magento\Framework\App\Helper\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\Product\Type $producttype,
        StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectInterface,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_storeManager = $storeManager;
        $this->_context = $context;
        $this->_producttype = $producttype;
        $this->_objectManager = $objectInterface;
        $this->_filesystem = $filesystem;
        $this->_resource = $resource;
    }
    
    /**
     * Retrieve Option values array
     *
     * @param  boolean $defaultValues
     * @param  boolean $withEmpty
     * @return array
     */
    public function toOptionArray($defaultValues = false, $withEmpty = false,$storeId=null)
    {
        return $this->getActivPaymentMethods();
    }

    public function getActivPaymentMethods(){
        $payments = $this->_objectManager->create('Magento\Payment\Model\Config')->getActiveMethods();
        $methods = array();
    
        foreach ($payments as $paymentCode=>$paymentModel) {

            $paymentTitle = $this->_scopeConfig->getValue('payment/'.$paymentCode.'/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if($paymentCode == 'cashondelivery')
                continue;
            $methods[] = array(
                    'label'   => $paymentTitle,
                    'value' => $paymentCode,
            );
        }
        return $methods;
    }
}
