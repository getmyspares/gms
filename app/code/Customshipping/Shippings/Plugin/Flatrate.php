<?php
namespace Customshipping\Shippings\Plugin;    
 
class Flatrate{

protected $_checkoutSession;        

protected $_scopeConfig;

protected $_customerSession;

public function __construct(
    \Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Customer\Model\Session $customerSession
) {
    $this->_storeManager = $storeManager;
    $this->_checkoutSession = $checkoutSession;
    $this->_scopeConfig = $scopeConfig;
    $this->_customerSession = $customerSession;
}

public function afterCollectRates(\Magento\OfflineShipping\Model\Carrier\Flatrate $Flatrate, $result)
{   
 
    //Magento-2 Log Here 
    $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/magento2.log');
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);      
    $logger->info("Flatrate shipping has been calling");                
    //keep your condition if you want
 
    return false;       

    return $result;
}  

}