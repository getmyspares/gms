<?php

namespace Noroute\Custom\Controller;

use Magento\Framework\App\Action\Context as Context;
use Magento\Framework\View\Result\PageFactory as PageFactory;

class NoRoute extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    /**
     * NoRoute constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultLayout = $this->resultPageFactory->create();
		  $resultLayout->getConfig()->getTitle()->set(__("Page not found"));
      
    //   $pageMainTitle = $resultPage->getLayout()->getBlock('page.main.title');
       // if ($pageMainTitle && $pageMainTitle instanceof \Magento\Theme\Block\Html\Title) {
       //  $pageMainTitle->setPageTitle('Error 404'); 
        //  }

        return $resultLayout;
    }
}