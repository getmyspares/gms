<?php

namespace Noroute\Custom\Controller;

use Magento\Framework\App\RequestInterface as Request;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\RequestInterface; 
class NoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface
{ 
    /**
     * @param Request $request
     * @return bool
     */ 
	 protected $request;
	 protected $_request;
	 protected $resultRedirect;
     protected $resultFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
		 ResultFactory $resultFactory,
		 \Magento\Framework\Controller\ResultFactory $result,
        \Magento\Framework\App\Request\Http $request
    ){
       // parent::__construct($context);
        $this->request = $request;
		 $this->resultRedirect = $result;
		//$this->resultFactory = $resultFactory;
    }
public function _prepareLayout()  
{  

   $this->pageConfig->getTitle()->set(__('No Result Found'));  

   return parent::_prepareLayout();  
}        
    public function process(Request $request)
    {   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_urls=$storeManager->getStore()->getBaseUrl();
	 
  $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
 $request = $objectManager->get('Magento\Framework\App\Request\Http');
	$newOne =  $request->getServer('HTTP_REFERER');  
    
    
        $moduleName = isset($parts[0]) ? $parts[0] : '';
        $actionPath = isset($parts[1]) ? $parts[1] : '';
        $actionName = isset($parts[2]) ? $parts[2] : '';
      
        if($newOne == $site_urls) {
			
            $request->setModuleName('notfoundpages')
                    ->setControllerName('noroute')
                    ->setActionName('product');
        } else {
            $request->setModuleName('notfoundpages')
                ->setControllerName('noroute')
                ->setActionName('other');
        }
        return false;
    }
}