<?php
namespace Yireo\ExampleAddressFieldMobile\Block\Address\Edit\Field;

use Magento\Framework\View\Element\Template;

/**
 * Class Landmark
 *
 * @package Yireo\ExampleAddressFieldMobile\Block\Address\Edit\Field
 */
class Altmobile extends Template
{
    /**
     * @var string
     */
    protected $_template = 'address/edit/field/altmobile.phtml';

    /**
     * @var \Magento\Customer\Api\Data\AddressInterface
     */
    protected $_address;

    /**
     * @return string
     */
    public function getCommentValue() : string
    {
        /** @var \Magento\Customer\Model\Data\Address $address */
        $address = $this->getAddress();
        $commentValue = $address->getCustomAttribute('altmobile');

        if (!$commentValue instanceof \Magento\Framework\Api\AttributeInterface) {
            return '';
        }

        return $commentValue->getValue();
    }

    /**
     * Return the associated address.
     *
     * @return \Magento\Customer\Api\Data\AddressInterface
     */
    public function getAddress() : \Magento\Customer\Api\Data\AddressInterface
    {
        return $this->_address;
    }

    /**
     * Set the associated address.
     *
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     */
    public function setAddress(\Magento\Customer\Api\Data\AddressInterface $address)
    {
        $this->_address = $address;
    }
}