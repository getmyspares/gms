<?php
namespace Editpost\Custom\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;
use Magento\Newsletter\Model\SubscriberFactory; 
use Magento\Framework\App\RequestInterface; 
use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator;
class EditPost
{ 
    protected $resultFactory;
    protected $url;
    protected $_request;
    protected $_response; 
	 protected $uploaderFactory;
    protected $adapterFactory;
    protected $filesystem;	
	protected $customerFactory;
	protected $subscriberFactory;
	 protected $session;
	  protected $formKeyValidator; 
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        UrlInterface $url,
		 Session $customerSession,
        ResultFactory $resultFactory,
		 UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		StoreManagerInterface $storeManager,
		\Magento\Customer\Model\CustomerFactory $customerFactory,
		 SubscriberFactory $subscriberFactory,
		Validator $formKeyValidator, 
		CustomHelper $helper
		
    )
    {
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->url = $url;
		 $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->resultFactory = $resultFactory;
		$this->helper = $helper;    
		$this->messageManager = $messageManager;
		$this->storeManager = $storeManager;
		$this->customerFactory  = $customerFactory;
		 $this->subscriberFactory = $subscriberFactory;
		 $this->session = $customerSession; 
		$this->formKeyValidator = $formKeyValidator; 
    }

    public function aroundExecute(\Magento\Customer\Controller\Account\EditPost $subject, \Closure $proceed) 
	{ 
        
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		 
		
		$post = $this->_request->getPostValue();

		
		$compny_info=0; 
		if(isset($post['change_comp_info']))
		{
			if($post['change_comp_info']==1) {
				$compny_info=1;
			}   
		}
		 
		if(isset($post['cus_change_mobile']) && $post['cus_change_mobile']=="on")
		{
			$customerSession = $objectManager->get('Magento\Customer\Model\Session');
			$customer_date = $customerSession->getCustomerMobileChange();
			$customer_date_array=json_decode($customer_date,true);
			$isotpconfirmed = $customer_date_array['is_otp_confirmed'];
			if($isotpconfirmed!='yes')
			{
				$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
				$resultRedirect->setUrl($this->url->getUrl('customer/account/edit'));
				$this->messageManager->addError(__('Otp does not match'));  
				return $resultRedirect;     		
				die;
			}
		}
				
		$custId=$this->session->getCustomerId();
		
		$requestedData=json_encode($_POST);
		$today=date('Y-m-d H:i:s');	 
		$customers = $objectManager->create('\Magento\Customer\Model\Customer')->getCollection();
		$groupRepository  = $objectManager->create('\Magento\Customer\Api\GroupRepositoryInterface');
		
		foreach ($customers as $customer) 
		{
			$customerId = $customer->getEntityId(); 
			if($custId==$customerId)
			{	
				$customerData=$customer->getData();
				$groupId = $customer->getGroupId(); 
				
			}      
		}
		
		
		$firstname=$customerData['firstname'];
		$lastname=$customerData['lastname'];
		$emailss=$customerData['email'];
		
		 
		
		$select = $connection->select()
                  ->from('marketplace_userdata') 
                  ->where('seller_id = ?', $custId)
                  ->limit(0,1);
        $result = $connection->fetchAll($select);
		
		 
		
		$is_seller=0;
		
		if(!empty($result)) {
			$is_seller=$result[0]['is_seller'];
		}   
		 if(isset($_FILES['my_customer_image']['name']) && $_FILES['my_customer_image']['name'] != '') {
		
		 $newfiles = $_FILES['my_customer_image']['name'];
		 $uploader = $this->uploaderFactory->create(['fileId' => 'my_customer_image']);
		 
		$uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
		 
		$uploader->setAllowRenameFiles(false);
		 
		$uploader->setFilesDispersion(false);
  
		 $path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('signature/saller');
		 
		$uploader->save($path);
		 
				 
				$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			//	$check_mobile=$custom_helpers->check_customer_mobile($cin_number);
				
				
				
				
				$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('entity_id = ?', $custId)
                  ->where('attribute_id = ?', '265');
                   
		
				$result = $connection->fetchAll($select);
				
				
				
				if(!empty($result))
				{
					
						$sql = "update customer_entity_varchar set value='".$newfiles."' WHERE entity_id='".$custId."' and attribute_id='265' ";
						$connection->query($sql);	
				}
				
				else
				{
					$sql = "insert into  customer_entity_varchar (attribute_id,entity_id,value) values ('265','".$custId."','".$newfiles."')"; 
					$connection->query($sql);	
				}       
			   	 
				
					
			}
		
		
		$compny_info=0; 
		if(isset($post['change_comp_info']))
		{
			if($post['change_comp_info']==1) {
				$compny_info=1;
			}   
		}
			  

		if($compny_info==1) 
		{
			$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			if($groupId!=4) {
			
				$postcode_valid=$custom_helpers->get_postcode_valid($post['seller_zipcode']); 
				 
				/* if($postcode_valid==0)
				{
					$message='No pickup available at given zipcode';	
					$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
					$resultRedirect->setUrl($this->url->getUrl('customer/account/edit'));
				
					$this->messageManager->addError(__($message));
					return $resultRedirect;   
					die; 
				}	 */ 
			}
			if(!empty($post['cus_change_mobile']))  		
			{  
				$mobile=$post['mobile'];  
				$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
				$check_mobile=$custom_helpers->check_customer_mobile($mobile);
				
				
				
				
				$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('entity_id = ?', $custId)
                  ->where('attribute_id = ?', '178');
                   
		
				$result = $connection->fetchAll($select);
				
				
				
				if(!empty($result))
				{
					if($check_mobile > 0)
					{
						$this->messageManager->addError(
							__(
								'Enter mobile already exists' 
							)      
						); 
						return $resultRedirect->setPath('customer/account/edit'); 		
						die;
					}
					else
					{
						$sql = "update customer_entity_varchar set value='".$post['mobile']."' WHERE entity_id='".$custId."' and attribute_id='178' ";
						$connection->query($sql);	
					}
				}
				else
				{
					$sql = "insert into  customer_entity_varchar (attribute_id,entity_id,value) values ('178','".$custId."','".$post['mobile']."')";
					$connection->query($sql);	
				}       
				
				
				
					
			}
			
			
				if(!empty($post['cin_number']))  		
			{  
				$cin_number=$post['cin_number'];  
				$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
			//	$check_mobile=$custom_helpers->check_customer_mobile($cin_number);
				
				
				
				
				$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('entity_id = ?', $custId)
                  ->where('attribute_id = ?', '264');
                   
		
				$result = $connection->fetchAll($select);
				
				
				
				if(!empty($result))
				{
					
						$sql = "update customer_entity_varchar set value='".$post['cin_number']."' WHERE entity_id='".$custId."' and attribute_id='264' ";
						$connection->query($sql);	
				}
				
				else
				{
					$sql = "insert into  customer_entity_varchar (attribute_id,entity_id,value) values ('264','".$custId."','".$post['cin_number']."')";
					$connection->query($sql);	
				}       
				 
				
					
			}
			
			 
			 
			
			
			
			 
			
			
			$select = $connection->select()
                  ->from('custom_user_edit_account') 
                  ->where('user_id = ?', $custId);
                 
                  
		
			$result = $connection->fetchAll($select);
			
			
			$count=count($result);  
			if($count==0)
			{ 
				$sql = "Insert Into custom_user_edit_account (user_id,group_id,is_seller,edit_account,cr_date) values (
				'".$custId."','".$groupId."','".$is_seller."','".$requestedData."','".$today."')";
				$connection->query($sql);    
				$message='Company changes have been sent to Admin for approval';	
			}
			else      
			{   
				$sql = "Update custom_user_edit_account set edit_account='".$requestedData."',cr_date='".$today."' where user_id='".$custId."'";
				$connection->query($sql);
				$message='Updated company changes have been sent to Admin for approval';	 
			}     
			
			$this->helper->admin_change_company_request($firstname,$lastname,$emailss); 
			
			$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
			$resultRedirect->setUrl($this->url->getUrl('customer/account'));
			
			
			
			$this->messageManager->addSuccess(__($message));
			return $resultRedirect;      
		}  
		else
		{	
			$resultProceed = $proceed(); 
			return $resultProceed;
		}   
		
    }
}