<?php

namespace TheSGroup\ProductAlertGrid\Block\Adminhtml\Stock\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('thesgroup_productalertgrid_stock_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Stock'));
    }
}
