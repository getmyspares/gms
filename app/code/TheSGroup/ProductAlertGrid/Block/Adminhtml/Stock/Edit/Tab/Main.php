<?php

namespace TheSGroup\ProductAlertGrid\Block\Adminhtml\Stock\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface
{
    protected $_wysiwygConfig;

    protected $_scopeConfig;

    const XML_PATH_CUSTOM_STATUS = 'catalog/productalert/custom_status';
 
    public function __construct(
        \Magento\Backend\Block\Template\Context $context, 
        \Magento\Framework\Registry $registry, 
        \Magento\Framework\Data\FormFactory $formFactory,  
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, 
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) 
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Alert Information');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Alert Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_thesgroup_alertgrid_stock');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('item_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Stock Alert Information')]);
        if ($model->getId()) {
            $fieldset->addField('alert_stock_id', 'hidden', ['name' => 'alert_stock_id']);
        }
        
        $fieldset->addField(
            'custom_status',
            'select',
            ['name' => 'custom_status', 'label' => __('Status'), 'title' => __('Status'),  'options'   => $this->getStatus(), 'required' => true]
        );
        $fieldset->addField(
            'custom_note',
            'textarea',
            [
                'name' => 'custom_note',
                'label' => __('Note'),
                'title' => __('Note'),
                'style' => 'height:10em;'
            ]
        );
        
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getStatus()
    {
        $statuses = $this->_scopeConfig->getValue(self::XML_PATH_CUSTOM_STATUS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($statuses)
        {
            $_statuses = explode(',', str_replace(' ', '', strtolower($statuses)));
            $result = [];
            foreach ($_statuses as $value) {
                $result[$value] = ucfirst($value);
            }
            return $result;
        } else {
            return ['empty'];
        }
    }
    
}
