<?php
/**
 * @author     The S Group <support@sashas.org>
 * @copyright  2020  Sashas IT Support Inc. (https://www.sashas.org)
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)
 */
declare(strict_types=1);

namespace TheSGroup\ProductAlertGrid\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class AlertActions
 * Builds view customer and product links for each item.
 */
class AlertActions extends Column
{
    /**
     * Url path
     */
    const URL_PATH_CUSTOMER = 'customer/index/edit';
    const URL_PATH_PRODUCT = 'catalog/product/edit';
    const URL_PATH_EDIT = 'thesgroup_alertgrid/stock/edit';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * AlertActions constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['customer_id'])) {
                    $item[$this->getData('name')]['edit_customer'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::URL_PATH_CUSTOMER,
                            [
                                'id' => $item['customer_id'],
                            ]
                        ),
                        'label' => __('View Customer'),
                        '__disableTmpl' => true,
                        'target' => '_blank'
                    ];
                }
                if (isset($item['product_id'])) {
                    $item[$this->getData('name')]['edit_product'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::URL_PATH_PRODUCT,
                            [
                                'id' => $item['product_id'],
                            ]
                        ),
                        'label' => __('View Product'),
                        '__disableTmpl' => true,
                        'target' => '_blank'
                    ];

                    $item[$this->getData('name')]['edit'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::URL_PATH_EDIT,
                            [
                                'id' => $item['alert_stock_id'],
                            ]
                        ),
                        'label' => __('Edit Row')
                    ];
                }
            }
        }
        return $dataSource;
    }
}
