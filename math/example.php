

<?php

//Include phpequations class:
include_once("phpequations.class.php");

//Declare an object
$obj = new phpequations; 

//Call to solve equations
$solution = $obj->solve("
	x+5x=100
");

//Print results
print_r($solution);

?>