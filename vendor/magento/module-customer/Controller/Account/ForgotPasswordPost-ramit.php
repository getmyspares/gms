<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Customer\Controller\Account;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SecurityViolationException;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
/**
 * ForgotPasswordPost controller
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ForgotPasswordPost extends \Magento\Customer\Controller\AbstractAccount implements HttpPostActionInterface
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param Escaper $escaper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        Escaper $escaper,
		
		CustomHelper $helper
    ) {
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->escaper = $escaper;
		
		$this->helper = $helper;   
        parent::__construct($context);
    }

    /**
     * Forgot customer password action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $email = (string)$this->getRequest()->getPost('email');
        
				
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		
		$customers = $objectManager->create('\Magento\Customer\Model\Customer')->getCollection();
		$groupRepository  = $objectManager->create('\Magento\Customer\Api\GroupRepositoryInterface');
		
			
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1);
		$CustomerModel->loadByEmail($email);
		$userId = $CustomerModel->getId();
		if($userId=="")
		{
			$this->messageManager->addErrorMessage(__('This email is not registered. Please check email and try again. '));
			return $resultRedirect->setPath('*/*/forgotpassword');	
			die;
		}    
		
		$customerRepository = $objectManager
					   ->get('Magento\Customer\Api\CustomerRepositoryInterface');
		$customer = $customerRepository->getById($userId);
		$cattrValue = $customer->getCustomAttribute('mobile')->getValue();
		
		
		$phone=$cattrValue;  
		
		  
		
		  
		
		foreach ($customers as $customer) {
			$customerId = $customer->getEntityId(); 
			if($userId==$customerId)
			{	
				$groupId = $customer->getGroupId(); 
				
				$group = $groupRepository->getById($groupId); 
				$groupCode = $group->getCode();  
			}    
		}
		
		
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	
		 
		//Select Data from table
		$sql = "Select * FROM marketplace_userdata where seller_id='".$userId."' limit 0,1";
		$result = $connection->fetchAll($sql);
		
		
		$is_seller=0;
		/*
		echo "<pre>";
			print_r($result[0]);
		echo "</pre>";   
		*/
		if(!empty($result)) {
			$is_seller=$result[0]['is_seller'];
		}
		/* $count=count($result);  
		if($count > 0)  
		{
			$resultRedirect = $this->resultRedirectFactory->create();
			$resultRedirect->setPath('customer/account/create/');
			$this->messageManager->addError(
				__(
					'Entered mobile number is already in use'
				)  
			);    
			
			
			return $resultRedirect; 		
			die;
		}   */ 
		  
		 
		  
		$userId=0; 
		 
		 
		
		if ($email) 
		{
			if (!\Zend_Validate::is($email, \Magento\Framework\Validator\EmailAddress::class)) {
                $this->session->setForgottenEmail($email);
                $this->messageManager->addErrorMessage(
                    __('The email address is incorrect. Verify the email address and try again.')
                );
                return $resultRedirect->setPath('*/*/forgotpassword');
            }
			
			
			
			$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
			$CustomerModel->setWebsiteId(1);
			$CustomerModel->loadByEmail($email);
			$userId = $CustomerModel->getId();
			
			if($userId==0)   
			{
				$this->messageManager->addErrorMessage(__('This email is not registered. Please check email and try again. '));
				return $resultRedirect->setPath('*/*/forgotpassword');	
			}  
			else  
			{
				
				if($groupId=='1')
				{   
					if($is_seller==1)
					{
						try {
		
							$this->customerAccountManagement->initiatePasswordReset(
								$email,
								AccountManagement::EMAIL_RESET
							);
							
						} 
						catch (NoSuchEntityException $exception) 
						{
							// Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
							$this->messageManager->addErrorMessage(__('This email is not registered. Please check email and try again. '));
							return $resultRedirect->setPath('*/*/forgotpassword');
						} 
						catch (SecurityViolationException $exception) {
							$this->messageManager->addErrorMessage($exception->getMessage());
							return $resultRedirect->setPath('*/*/forgotpassword');
						} 
						catch (\Exception $exception) {
							$this->messageManager->addExceptionMessage(
								$exception,
								__('We\'re unable to send the password reset email.')
							);
							return $resultRedirect->setPath('*/*/forgotpassword');
						}
						$this->messageManager->addSuccessMessage($this->getSuccessMessage($email));
						return $resultRedirect->setPath('*/*/');
					}
					else
					{	
						$pin = mt_rand(1000, 9999);				  
						$today=date('Y-m-d H:i:s');
						$forgetotp=$customerSession->getForgetPinJson(); 
						if($forgetotp=='')
						{
							$forgetotp=0;	
						}	
						  
						if($forgetotp=='')
						{	
							$completeUser=array(
								'pin'=>$pin,	
								'phone'=>$phone,	
								'userId'=>$userId,	  
								'email'=>$email,	  
								'cr_time'=>$today	  
							);  

							$userForgetPinJson=json_encode($completeUser);
							$customerSession->setForgetPinJson($userForgetPinJson); //set value in customer session
							$customerSession->getForgetPinJson(); //Get value from customer session 
							
							$customerSession->setForgetcountdown(1); 		
							      
							
							$message="Please complete your Forget Password process by entering this OTP - ".$pin; 
							$this->helper->ForgetPasswordHelperOTP($pin,$phone,$message,$email);  
							return $resultRedirect->setPath('*/*/forgotpassword');	        
						}  
					}	
				} 
				else 
				{    
					try {
	
						$this->customerAccountManagement->initiatePasswordReset(
							$email,
							AccountManagement::EMAIL_RESET
						);
						
					} 
					catch (NoSuchEntityException $exception) 
					{
						// Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
						$this->messageManager->addErrorMessage(__('This email is not registered. Please check email and try again. '));
						return $resultRedirect->setPath('*/*/forgotpassword');
					} 
					catch (SecurityViolationException $exception) {
						$this->messageManager->addErrorMessage($exception->getMessage());
						return $resultRedirect->setPath('*/*/forgotpassword');
					} 
					catch (\Exception $exception) {
						$this->messageManager->addExceptionMessage(
							$exception,
							__('We\'re unable to send the password reset email.')
						);
						return $resultRedirect->setPath('*/*/forgotpassword');
					}
					$this->messageManager->addSuccessMessage($this->getSuccessMessage($email));
					return $resultRedirect->setPath('*/*/');
					  
					
				}   
			}    
			  
			
		} 
		else 
		{
            $this->messageManager->addErrorMessage(__('Please enter your email.'));
            return $resultRedirect->setPath('*/*/forgotpassword');
        }
    }

    /**
     * Retrieve success message
     *
     * @param string $email
     * @return \Magento\Framework\Phrase
     */
    protected function getSuccessMessage($email)
    {
        return __(
            'If there is an account associated with %1 you will receive an email with a link to reset your password.',
            $this->escaper->escapeHtml($email)
        );
    }
}
