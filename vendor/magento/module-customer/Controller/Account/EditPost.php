<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magento\Customer\Controller\Account; 

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\Phrase;
use Panasonic\CustomUser\Helper\Data as CustomHelper; 
/**
 * Class EditPost
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EditPost extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    /**
     * Form code for data extractor
     */
    const FORM_DATA_EXTRACTOR_CODE = 'customer_account_edit';

    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var CustomerExtractor
     */
    protected $customerExtractor;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \Magento\Customer\Model\EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var Mapper
     */
    private $customerMapper;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     * @param CustomerExtractor $customerExtractor
     * @param Escaper|null $escaper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        CustomerExtractor $customerExtractor,
		CustomHelper $helper,
        ?Escaper $escaper = null
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->customerExtractor = $customerExtractor;
		$this->helper = $helper;  
        $this->escaper = $escaper ?: ObjectManager::getInstance()->get(Escaper::class);
    }

    /**
     * Get authentication
     *
     * @return AuthenticationInterface
     */
	 
    private function getAuthentication()
    {

        if (!($this->authentication instanceof AuthenticationInterface)) {
            return ObjectManager::getInstance()->get(
                \Magento\Customer\Model\AuthenticationInterface::class
            );
        } else {
            return $this->authentication;
        } 
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated 100.1.0
     */
    private function getEmailNotification()
    {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return ObjectManager::getInstance()->get(
                EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        }
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/edit');

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Invalid Form Key. Please refresh the page.')]
        );
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }

    /**
     * Change customer email or password action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());
		
		
		if ($validFormKey && $this->getRequest()->isPost()) 
		{  
			$currentCustomerDataObject = $this->getCustomerDataObject($this->session->getCustomerId());
			$customerCandidateDataObject = $this->populateNewCustomerDataObject(
				$this->_request,
				$currentCustomerDataObject
			);
			  		
			
			try {   
				
				$this->changeCustomerPassword($currentCustomerDataObject->getEmail());
                
				$post = $this->getRequest()->getPostValue();
				
				/* echo "<pre>";
				print_r($post);
				echo "</pre>";  */
				 
				
				$email=$post['email'];
				$firstname=$post['firstname'];
				$lastname=$post['lastname'];
				$mobile=$post['mobile']; 
				$email = filter_var($email, FILTER_SANITIZE_STRING);
				$firstname = filter_var($firstname, FILTER_SANITIZE_STRING);
				$lastname = filter_var($lastname, FILTER_SANITIZE_STRING);
				$mobile = filter_var($mobile, FILTER_VALIDATE_INT);
	
				
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				
				$customerSession = $objectManager->get('Magento\Customer\Model\Session');
				$custID=$customerSession->getCustomer()->getId();
				
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();
					
					
				$sql = "update customer_entity set firstname='".$firstname."',lastname='".$lastname."' WHERE entity_id='".$custID."' ";
				$connection->query($sql);
				
				if(!empty($post['cus_change_email']))  		
				{  
					
					
					
					$select = $connection->select()
						->from('customer_entity') 
						->where('entity_id = ?', $custID)
						->where('email = ?', $email);
                 
					$result = $connection->fetchAll($select);
					
					if(!empty($result))
					{
						$this->messageManager->addError(
							__(
								'Enter Email already exists'
							)      
						); 
						return $resultRedirect->setPath('customer/account/edit'); 		
						die;
					}
					else
					{
						$sql = "update customer_entity set email='".$email."' WHERE entity_id='".$custID."' ";
						$connection->query($sql);	 
					} 	
				}
	          
					
				if(!empty($post['cus_change_mobile']))  		
				{  
			    
					
					
					$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
					$check_mobile=$custom_helpers->check_customer_mobile($mobile);
					
				
					
					$select = $connection->select()
						->from('customer_entity_varchar')   
						->where('entity_id = ?', $custID)
						->where('attribute_id = ?', '178');
                 
					$result = $connection->fetchAll($select);
					
					
					if(!empty($result))
					{
						 
						if($check_mobile > 0)
						{
							$this->messageManager->addError(
								__(
									'Enter mobile already exists' 
								)      
							); 
							return $resultRedirect->setPath('customer/account/edit'); 		
							die;
						}
						else
						{
						
							$sql = "update customer_entity_varchar set value='".$mobile."' WHERE entity_id='".$custID."' and attribute_id='178' ";
							$connection->query($sql);	
						}
					}
					else
					{
						 $sql = "insert into  customer_entity_varchar (attribute_id,entity_id,value) values ('178','".$custID."','".$mobile."')";
						$connection->query($sql);	
					}       
					
					  
					
					 	
				}
						
				$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
				$check_this_seller=$api_helpers->check_user_seller($custID);   
				
				if($check_this_seller==0)     
				{	
					$buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
					$buyer_helpers->create_buyer_report_updated($custID );       	   
				} 
				 
				
				$this->messageManager->addSuccess(__('Changes Saved'));  	
				$resultRedirect = $this->resultRedirectFactory->create();
				$resultRedirect->setPath('customer/account/edit');
 				return $resultRedirect;
				 
  
				
				
			} catch (InvalidEmailOrPasswordException $e) {
				$this->messageManager->addErrorMessage($this->escaper->escapeHtml($e->getMessage()));
			} catch (UserLockedException $e) {
				$message = __(
					'The account sign-in was incorrect or your account is disabled temporarily. '
					. 'Please wait and try again later.'
				);
				$this->session->logout();
				$this->session->start();
				$this->messageManager->addError($message);
				return $resultRedirect->setPath('customer/account/login');
			} catch (InputException $e) {
				$this->messageManager->addErrorMessage($this->escaper->escapeHtml($e->getMessage()));
				foreach ($e->getErrors() as $error) {
					$this->messageManager->addErrorMessage($this->escaper->escapeHtml($error->getMessage()));
				}
			} catch (\Magento\Framework\Exception\LocalizedException $e) {
				$this->messageManager->addError($e->getMessage());
			} catch (\Exception $e) {
				$this->messageManager->addException($e, __('We can\'t save the customer.'));
			}

			$this->session->setCustomerFormData($this->getRequest()->getPostValue()); 
		}  

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/edit');
        return $resultRedirect;
    }

    /**
     * Account editing action completed successfully event
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customerCandidateDataObject
     * @return void
     */
    private function dispatchSuccessEvent(\Magento\Customer\Api\Data\CustomerInterface $customerCandidateDataObject)
    {
        $this->_eventManager->dispatch(
            'customer_account_edited',
            ['email' => $customerCandidateDataObject->getEmail()]
        );
    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }

    /**
     * Create Data Transfer Object of customer candidate
     *
     * @param \Magento\Framework\App\RequestInterface $inputData
     * @param \Magento\Customer\Api\Data\CustomerInterface $currentCustomerData
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function populateNewCustomerDataObject(
        \Magento\Framework\App\RequestInterface $inputData,
        \Magento\Customer\Api\Data\CustomerInterface $currentCustomerData
    ) {
        $attributeValues = $this->getCustomerMapper()->toFlatArray($currentCustomerData);
        $customerDto = $this->customerExtractor->extract(
            self::FORM_DATA_EXTRACTOR_CODE,
            $inputData,
            $attributeValues
        );
        $customerDto->setId($currentCustomerData->getId());
        if (!$customerDto->getAddresses()) {
            $customerDto->setAddresses($currentCustomerData->getAddresses());
        }
        if (!$inputData->getParam('change_email')) {
            $customerDto->setEmail($currentCustomerData->getEmail());
        }

        return $customerDto;
    }

    /**
     * Change customer password
     *
     * @param string $email
     * @return boolean
     * @throws InvalidEmailOrPasswordException|InputException
     */
    protected function changeCustomerPassword($email)
    {
        $isPasswordChanged = false;
        if ($this->getRequest()->getParam('change_password')) {
            $currPass = $this->getRequest()->getPost('current_password');
            $newPass = $this->getRequest()->getPost('password');
            $confPass = $this->getRequest()->getPost('password_confirmation');
            if ($newPass != $confPass) {
                throw new InputException(__('Password confirmation doesn\'t match entered password.'));
            }

            $isPasswordChanged = $this->customerAccountManagement->changePassword($email, $currPass, $newPass);
        }

        return $isPasswordChanged;
    }

    /**
     * Process change email request
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $currentCustomerDataObject
     * @return void
     * @throws InvalidEmailOrPasswordException
     * @throws UserLockedException
     */
    private function processChangeEmailRequest(\Magento\Customer\Api\Data\CustomerInterface $currentCustomerDataObject)
    {
        if ($this->getRequest()->getParam('change_email')) {
            // authenticate user for changing email
            try {
                $this->getAuthentication()->authenticate(
                    $currentCustomerDataObject->getId(),
                    $this->getRequest()->getPost('current_password')
                );
            } catch (InvalidEmailOrPasswordException $e) {
                throw new InvalidEmailOrPasswordException(
                    __("The password doesn't match this account. Verify the password and try again.")
                );
            }
        }
    }
    
    /**
     * Get Customer Mapper instance
     *
     * @return Mapper
     *
     * @deprecated 100.1.3
     */
    private function getCustomerMapper()
    {
        if ($this->customerMapper === null) {
            $this->customerMapper = ObjectManager::getInstance()->get(\Magento\Customer\Model\Customer\Mapper::class);
        }
        return $this->customerMapper;
    }
}
   