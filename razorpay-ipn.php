<?php
include('app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
 

$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');

$array=json_decode(urldecode($json));

/*
echo "<pre>";
	print_r($array);
echo "</pre>";
*/ 

 
$payment_id=$array->payload->payment->entity->id;
$status=$array->payload->payment->entity->status;
$email=$array->payload->payment->entity->email;
$contact=$array->payload->payment->entity->contact;
$razorpay_order_id=$array->payload->payment->entity->order_id;
$quote_id=$array->payload->payment->entity->notes->merchant_order_id;
$created_at=$array->created_at;
$event=$array->event;
$account_id=$array->account_id;

// timestamp
$timestamp = $created_at;

// output
$created_at=date('Y-m-d H:i:s',$timestamp);

date_default_timezone_set('Asia/Kolkata'); 
$timezone_object = date_default_timezone_get(); 
$created_at=date('Y-m-d H:i:s');   


if($quote_id!='') 
{	
	$select = $connection->select()
		  ->from('razorpay_orders')
		  ->where('quote_id = ?', $quote_id)   
		  ->where('razorpay_order_id = ?', $razorpay_order_id)  
		  ->where('payment_id = ?', $payment_id)    
		  ->where('account_id = ?', $account_id);
	$result = $connection->fetchAll($select);    

	if(empty($result))
	{
		$sql="INSERT INTO `razorpay_orders`(`quote_id`, `account_id`, `razorpay_order_id`, `payment_id`, `email`, `status`, `cr_date`) VALUES ('".$quote_id."','".$account_id."','".$razorpay_order_id."','".$payment_id."','".$email."','".$status."','".$created_at."')";
		$connection->query($sql);   	
	}	
	else
	{
		$sql="update razorpay_orders set razorpay_order_id='".$razorpay_order_id."',payment_id='".$payment_id."',email='".$email."',status='".$status."',cr_date='".$created_at."' where quote_id='".$quote_id."' and account_id='".$account_id."'";
		$connection->query($sql);         
	}	
	  

	$subject='IPN - '.$quote_id;    
	$data_helpers->test_email($sql,'101',$subject); 
}    
/*
$arrays=array(
	'event'=>$array->event,	
	'account_id'=>$array->account_id,	
	'payment_id'=>$payment_id,	
	'status'=>$status,	
	'email'=>$email,	
	'quote_id'=>$quote_id,	
	'razorpay_order_id'=>$razorpay_order_id,	 
	'created_at'=>$created_at,	  
);
*/


?>


