<?php
	session_start();
	require "../security/sanitize.php";
	
	
	
	
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$result = sanitizedJsonPayload();
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	
	$response = null;	
	$user_array=null;
	
	

	if(empty($result))
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Payload  is empty'); 	
		echo json_encode($result_array);	
		die;
	}

	$argumentsCount=count($result);
	if ($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Payload  is wrong'); 	
		echo json_encode($result_array);	
		die;
	}
	
	if(empty($result['username']) )
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Payload  is wrong'); 	
		echo json_encode($result_array);	
		die;
	}

	if(empty($result['password'])  )
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Payload  is wrong'); 	
		echo json_encode($result_array);	
		die;
	}

	if(empty($result['customer']))
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Payload  is wrong'); 	
		echo json_encode($result_array);	
		die;
	}

	if(empty($result['deviceid']))
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Payload  is wrong'); 	
		echo json_encode($result_array);	
		die;
	}
	$device_id = $result['deviceid'];
	if($result['customer']==1)
	{
		$email = $result['username'];
		$password = $result['password'];

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
		{
			$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
			echo json_encode($result_array);	
			die;
		}

		$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
		$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
		$CustomerModel->setWebsiteId(1); 
		$CustomerModel->loadByEmail($email); 
		$userId = $CustomerModel->getId();
		if($userId=='')
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'cannot  log  in','customer'=>'2'); 	
			echo json_encode($result_array);	
			die;
		}
		else
		{
			$locked=$api_helpers->check_user_locked($userId);
			if($locked==0)
			{
				$result_array=array('success' => 0,'result' => $user_array, 'error' => 'cannot  log  in 2','customer'=>'2'); 	
				echo json_encode($result_array);	
				die;
			} 
		
		} 
		try
		{ 
			$customer = $objectManager->get('\Magento\Customer\Model\AccountManagement')->authenticate($email, $password);
			$custId=$customer->getId();
			$group_id=$customer->getGroupId();
			
			if($group_id==4) 
			{
				$confirmation=$helpers->checkUserEmailConfirmation($custId); 
				if($confirmation==0)
				{
					$result_array=array('success' => 0,'result' => $user_array, 'error' => 'cannot  log  in 3','customer'=>'2'); 	
					echo json_encode($result_array);	
					die;
				} 
				
				$approved=$helpers->companyUserApproved($custId);
				if($approved==0)
				{
					$result_array=array('success' => 0,'result' => $user_array, 'error' => 'cannot  log  in 4','customer'=>'2'); 	
					echo json_encode($result_array);	
					die;
				}  
				
			}
			
			/* log in customer in magento also  for  validation other information in rest api calls IDOR issue @ritesh2022020 */
			$customerSession = $objectManager->create('Magento\Customer\Model\Session');
			$customerSession->setCustomerAsLoggedIn($CustomerModel);
		
			$token = rand();
			if(!isset($_SESSION))
			{
				session_start();
			}
			$session_id = session_id(); 
			$_SESSION['app_accesstoken'] = $token;
			$status = $api_helpers->registerToken($custId,$device_id,$token);
			if($status)
			{
				$result_array=array('success' => 1, 'result' => "$token",'sid'=>"$session_id", 'error' => 'none','customer'=>'1'); 	
				echo json_encode($result_array);
			}
			die();
		}
		catch(Exception $e) 
		{
			$ms = $e->getMessage();
			$result_array=array('success' => 0,'result' => $user_array, 'error' => " cannot  log  in $ms ",'customer'=>'2'); 	
			echo json_encode($result_array);	
			die;
		}  
	}

	if($result['customer']==2)
	{	
		$username = "admin";
		$password = "admin@123";

		if($result['username']=="$username" && $result['password']=="$password")
		{
			$token = rand();
			if(!isset($_SESSION))
			{
				session_start();
			}
			$session_id = session_id();
			$_SESSION['app_accesstoken'] = $token ;
			$toekn =  $_SESSION['app_accesstoken'];
			$status = $api_helpers->registerToken(null,$device_id,$toekn);
			if($status)
			{
				$result_array=array('success' => 1, 'result' => "$toekn",'sid'=>"$session_id", 'error' => 'none','customer'=>'1'); 	
				echo json_encode($result_array);
			}

			die();
		}	
	}
	
	$result_array=array('success' => 0,'result' => $response , 'error' => 'Payload  is wrong','customer'=>'2');
	echo json_encode($result_array);	
	die;
	
?>