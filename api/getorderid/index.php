<?php
	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/removecart/?accesstoken=1234&payment_id=12345 
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
			
	//$result = sanitizedJsonPayload();
	// print_r($result);
	// die();
	
	$result=$_GET;
	
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
		
	$response = null;
	$argumentsCount=count($result);
	$user_array=null;
	/* if($argumentsCount < 3 || $argumentsCount > 3)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}    */
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty($result['payment_id'])){
		$payment_id = $result['payment_id'];
	}else{
		$payment_id = '';	
	}

		
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	
	
	if($payment_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Payment id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	

	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	 
			die;
		}
		
	}
	


	//$payment_id='pay_EF0QWKoAUjr4Ad';   
	$orderArray=array();	
	/* removing depenndency from custom mobile order table */
	// $sql="SELECT * FROM `sales_order_payment` where  last_trans_id='$payment_id'";  
	
	$sql="SELECT b.`increment_id` FROM `sales_order_payment`  as  a  left  join sales_order as  b on a.`parent_id`=b.`entity_id` where  last_trans_id='$payment_id'";  
	
	// $sql="select order_increment_id from mobile_sales_order where payment_details='".$payment_id."'";  
	$results = $connection->fetchAll($sql);		    
	$order_ids_array=array();
	if(!empty($results))
	{
		foreach($results as $order_ids)
		{
			$order_ids_array[] = $order_ids['increment_id'];
		} 
	}	
	$user_array=array('order_id'=>$order_ids_array);   
	$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Order Id'); 	
	echo json_encode($result_array);	
	die;	

	
	
?>