<?php
	// Catgeory's Rating filter as per stars. 
	//https://dev.idsil.com/design/api/category_product_rating_filter/?cat_id=66&accesstoken=1234&rating_filter=1,2,3&page_no=3
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	if($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	}

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}
	
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
	}else{
		$rating_filter = '';	
	}
	
	if($rating_filter ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'rating number is missing. Please try again.'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$rating_filter = $result['rating_filter'];
	}
	

	try
	{
		//$categoryId = $_GET['cat_id'];
	 
		 
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		 
		 // YOUR CATEGORY ID
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
									 ->addAttributeToSelect('*')->setCurPage($page_no) // page Number
									 ->addAttributeToFilter('status', 1)
									 ->addAttributeToSort('price', 'DESC')
									 ->addAttributeToFilter('visibility', 4);

									 
		$product_counts = $category->getProductCollection()->count();
									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();

		//------- For multiple rating
		$filter_values = $result['rating_filter'];
		$filter_values = (explode(",",$filter_values));
		$i = 0 ;
		foreach($filter_values as $stars){
		 // echo $stars;
		 
		 // Set Rating parameter as per the rating_filter number 
		if($stars==1){
			$rating_from = 0;
			$rating_to = 20;
			
		}else if($stars==2){
			$rating_from = 21;
			$rating_to = 40;
			
		}else if($stars==3){
			$rating_from = 41;
			$rating_to = 60;
		
		}else if($stars==4){
			$rating_from = 61;
			$rating_to = 80;
		
		}else if($stars==5){
			$rating_from = 81;
			$rating_to = 100;
			
		}
		// echo $rating_from;
		// echo '<br>';
		// echo $rating_to;
		
		
		// rating_count
		
		
			foreach ($categoryProducts as $product) {
				$id = $product->getId();
				$ratings = 0;
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				
								
				if (($ratings >= $rating_from) && ($ratings <= $rating_to)){

					//$products[$id]= $product->getData();
					$product_array[$i]['id']= $product->getId();
					$product_array[$i]['name']= "'".$product->getName()."'";
					$product_array[$i]['sku']= "'".$product->getSku()."'";
					$product_array[$i]['price']= "'".$product->getPrice()."'";
					$product_array[$i]['specialprice'] = $product->getSpecialPrice();
					$product_array[$i]['specialfromdate'] = $product->getSpecialFromDate();
					$product_array[$i]['specialtodate'] = $product->getSpecialToDate();
					$product_array[$i]['brands_name'] = $product->getAttributeText('brands');
					$product_array[$i]['brands_id'] = $product->getData('brands');
					$product_array[$i]['image'] = "'".$store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage()."'";
					
					if(!empty($ratings)){
					$product_array[$i]['rating_count'] = $ratings;
					}else{
						$product_array[$i]['rating_count'] = 0;
					}
					
					$i++;
				}			
			}
		 
		 
			
		}
		
		foreach($product_array as $value){
			$data[] = $value;
		}
			
		
		if( $page_no > (ceil($product_counts/10))  ){
			
			$result_array = array('success' => 0, 'result' => null, 'error' => "The page number exceded the max number."); 
			echo json_encode($result_array);
			die();
			
		}
		
		// print_r($product_array);
		// die();
		
		if(!empty($data)){
			$product_array['product_count'] = $i;
			$product_array['pages_count'] = ceil($i/10);
			$result_array = array('success' => 1, 'result' => $data,"product_count" =>$product_array['product_count'],"pages_count" =>$product_array['pages_count'], 'error' =>"Product lies under ".$result['rating_filter']." star rating."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $data, 'error' => "In this category there is not product under ".$result['rating_filter']." star rating."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>