<?php
	/*require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$result = $_GET;
	$product_array = null;

	$result = $objectManager->create('Mirasvit\SearchAutocomplete\Model\Result');
	$result->init();
	$product_array = $result->toArray();*/	
	
	
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();

	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');	
	$site_url=$storeManager->getStore()->getBaseUrl();
	$product_array="";
	$q = $_GET['q'];
	/* handling of  spaces in the request @ritesh13april2021*/
	$q = str_replace(" ","+",$q);
	
	if($q=='undefined')
	{
		$result_array = array('success' => 0, 'result' => $product_array, 'error' => "There is not product with this name. Please try some other keyword."); 
		echo json_encode($result_array);
		die();
	}
	$searchUrl = $site_url.'searchautocomplete/ajax/suggest/?q='.$q.'&store_id=1&cat=false&_='.time();
	$product_array = json_decode(file_get_contents($searchUrl));
	
	if(!empty($product_array)){
		$result_array = array('success' => 1, 'result' => $product_array, 'error' =>"Search data found"); 
		echo json_encode($result_array);
	}else{
		$result_array = array('success' => 0, 'result' => $product_array, 'error' => "There is not product with this name. Please try some other keyword."); 
		echo json_encode($result_array);
		die();
	}
?>