<?php
	
	// Check delivery_avalability
	//https://dev.idsil.com/design/api/faq_listing/?product_id=1399&accesstoken=1234
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	// print_r($result);
	
	
	$response = null;
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$product_id = @get_numeric($result['product_id']);
	}
	
	

	try
	{
		
		if(empty(@get_numeric($result['user_id'])))
		{
			$user_id=0;	
		}	
		else
		{
			$user_id=@get_numeric($result['user_id']); 
		}		
		$like_dislike=null;	  
		
		
		
		$like_count=0;
		$dislike_count=0;
		
		$prd_question = null;
		//$product = $block->getCurrentProduct();//get current product
		//$product_id=$block->getCurrentProduct()->getId();
		$product_id = @get_numeric($result['product_id']);
		
		//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		//$tableName = $resource->getTableName('wk_qaquestion');
		$sql = "SELECT * FROM wk_qaquestion WHERE product_id = '$product_id'";
		$result = $connection->fetchAll($sql); 
		// echo '<pre>'; print_r($result); echo '</pre>';
		$i=0;
		$total_answer_count = 0;
		foreach ($result as $question){
			$answer_array = null;
			$question_id = $question['question_id'];
			$prd_question[$i]['question_id'] = $question_id ;
			$prd_question[$i]['buyer_id'] = $question['buyer_id'] ;
			$prd_question[$i]['subject'] = $question['subject'] ;
			$prd_question[$i]['content'] = $question['content'] ;
			$prd_question[$i]['qa_nickname'] = $question['qa_nickname'] ;
			$prd_question[$i]['status'] = $question['status'] ;
			$prd_question[$i]['created_at'] = $question['created_at'] ;
			
			$sql2 = "SELECT * FROM wk_qaanswer WHERE question_id = '$question_id'";
			$result_answer = $connection->fetchAll($sql2);
			
			
			
			
			
			
			
			if(!empty($result_answer))
			{
				foreach($result_answer as $answer)
				{
					if($user_id!=0)
					{
						$user_id;
						$like_dislike=$api_helpers->check_current_user_like_answer($answer['answer_id'],$user_id);		
						
					}	 
						
					
					$like_count=$api_helpers->ans_like_dislike_count($answer['answer_id'],'1');
					$dislike_count=$api_helpers->ans_like_dislike_count($answer['answer_id'],'0');
												
					$answer_array[]=array(
						'answer_id'=>$answer['answer_id'],	
						'question_id'=>$answer['question_id'],	
						'respond_from'=>$answer['respond_from'],	
						'respond_nickname'=>$answer['respond_nickname'],	
						'respond_type'=>$answer['respond_type'],	
						'content'=>$answer['content'],	
						'status'=>$answer['status'],	
						'created_at'=>$answer['created_at'],	
						'like_dislike'=>$like_dislike,
						'like_count'=>$like_count,
						'dislike_count'=>$dislike_count
					);	
				}	
			}	
				
			
			/* foreach($result_answer as $answer){
				$answer_array[] = $answer;
			} */
			
			
			
			
			if(!empty($answer_array)){
				$total_answer_count += count($answer_array) ;
			}
			$prd_question[$i]['answers'] = $answer_array ;
			
			$i++;
			
		}
		if(!empty($prd_question)){
		$question_counts = count($prd_question);
		}else{
			$question_counts =null;
		}
		// echo '<pre>';
		// print_r($prd_question);
		// echo '</pre>';
		//die('faq listing');
		
		
		if(!empty($prd_question)){
			$result_array = array('success' => 1, 'result' => $prd_question,'question_counts'=>$question_counts,'answers_counts'=>$total_answer_count,'error' =>"The F&Q listing Data."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $prd_question, 'error' => "There is no question and answer for this product."); 
			echo json_encode($result_array);
			die();
		}

	}catch(Exception $e){
		

		$result_array = array('success' => 0,'result' => $prd_question, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
		
		
	}
?>