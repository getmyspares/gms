<?php
require "../security/sanitize.php";
	
	//https://dev.idsil.com/design/api/wishlistlisting/

	// $result = sanitizedJsonPayload();
	$result = $_GET;
		
	/*{"accesstoken": "1234","customer_id":"269"}*/

	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$argumentsCount=count($result);
	$data=null; 

	if($argumentsCount < 1 || $argumentsCount > 1) 
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}	


	if(!isset($result['accesstoken']))
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Access Token is missing'); 	
		echo json_encode($result_array);	
		die; 
	}


	$accesstoken=$result['accesstoken'];
	

	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 

	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
	
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0){
			$result_array=array('success' => 0, 'result' => $data, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	
   try{
		$countryCode = 'IN';
		//Reward helper
		$country = $objectManager->get('\Magento\Directory\Model\Country');
		
		//Store
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
		
		// Get Regions listing
		$regionCollection = $country->loadByCode($countryCode)->getRegions();
        $region_array = $regionCollection->loadData()->toOptionArray(false);
		$i = 0;
		foreach($region_array as $region){
			if($i>0){
				$region_arr['value']= $region['value'];
				$region_arr['title']= $region['title'];
				$region_arr['country_id']= $region['country_id'];
				$region_arr['label']= $region['label'];
				$data[] = $region_arr;
			}
			$i++;
		}
		// print_r($region_array);
		// die();
		// $data[] = $region_array;
		// echo '</pre>';
		// print_r($data);
		// echo '<pre>';
		// die();



		if(!empty($data)){
			$result_array=array('success' => 1, 'result' =>$data, 'error' => 'Region listing with id and name');
			echo json_encode($result_array);
		}else{
			$result_array=array('success' => 1, 'result' =>$data, 'error' => 'There is no region for this country id.');
			echo json_encode($result_array);
			die();
		}
			
		// print_r($data);
	
		
	}catch(Exception $e){
		$result_array=array('success' => 0, 'result' =>$data, 'error' => $e->getMessage());
		echo json_encode($result_array);		
	} 
	 
	
?>