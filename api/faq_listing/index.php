<?php
	
	// Check delivery_avalability
	//https://dev.idsil.com/design/api/faq_listing/?product_id=1399&accesstoken=1234

	require "../security/sanitize.php";
	
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$result = $_GET;
	// print_r($result);
	// die();
	
	$response = null;
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$product_id = @get_numeric($result['product_id']);
	}
	
	

	try
	{
		$prd_question = null;
		//$product = $block->getCurrentProduct();//get current product
		//$product_id=$block->getCurrentProduct()->getId();
		$product_id = @get_numeric($result['product_id']);
		
		//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		//$tableName = $resource->getTableName('wk_qaquestion');
		$sql = "SELECT * FROM wk_qaquestion WHERE product_id = '$product_id'";
		$result = $connection->fetchAll($sql); 
		// echo '<pre>'; print_r($result); echo '</pre>';
		$i=0;
		$total_answer_count = 0;
		$total_ques=0;
		$total_answer=0; 
		$questionArray=array();
		
		foreach ($result as $question){
			$anserArray=array();
			$answer_array = null;
			$question_id = $question['question_id'];
			$prd_question[$i]['question_id'] = $question_id ;
			$prd_question[$i]['buyer_id'] = $question['buyer_id'] ;
			$prd_question[$i]['subject'] = $question['subject'] ;
			$prd_question[$i]['content'] = $question['content'] ;
			$prd_question[$i]['qa_nickname'] = $question['qa_nickname'] ;
			$prd_question[$i]['status'] = $question['status'] ;
			//$prd_question[$i]['created_at'] = $question['created_at'] ;
			$prd_question[$i]['created_at'] = date("M d, Y", strtotime($question['created_at']));
			
			 $sql2 = "SELECT * FROM wk_qaanswer WHERE question_id = '$question_id'";
			$result_answer = $connection->fetchAll($sql2);
			foreach($result_answer as $answer){
				$answer_array[] = $answer;
			}
			if(!empty($answer_array)){
				
				$total_answer_count += count($answer_array) ;
				
				foreach($answer_array as $answers)
				{
					
					$total_likes=$api_helpers->count_answer_like_dislike($answers['answer_id'],1);
					$total_dislikes=$api_helpers->count_answer_like_dislike($answers['answer_id'],0);
					
					
					$content = str_replace(array("\n", "\r"), '', $answers['content']);
					
					$anserArray[]=array(
						'answer_id'=>$answers['answer_id'],	
						'question_id'=>$answers['question_id'],	
						'respond_from'=>$answers['respond_from'],	
						'respond_nickname'=>$answers['respond_nickname'],	
						'respond_type'=>$answers['respond_type'],	
						'content'=>trim($content), 	 
						'status'=>$answers['status'],	
						'like_count'=>$total_likes,	 
						'dislike_count'=>$total_dislikes,	
						'created_at'=>date("M d, Y", strtotime($answers['created_at'])),	
						
					);
					
					if($answers['status']==1)
					{
						$total_answer=$total_answer+1;
					} 		
				}	
				
				
				
			}
			
			
			//print_r($anserArray);
			//die;
			
			$prd_question[$i]['answers'] = $anserArray ; 
			 
			$i++;
			
			
			
			if($question['status']==1)
			{
				$total_ques=$total_ques+1;	
			}	
			
		}
		
		/* if(!empty($answer_array))
		{
			foreach($answer_array as $answers)
			{
				if($answers['status']==1)
				{
					$total_answer=$total_answer+1;
				}		
			}	 
		}	 */
		
		
		if(!empty($prd_question)){
		$question_counts = count($prd_question);
		}else{
			$question_counts =null;
		}
		// echo '<pre>';
		// print_r($prd_question);
		// echo '</pre>';
		//die('faq listing');
		
		if($total_ques==0)
		{
			$total_answer=0;	
			$total_answer_count=0;	
		}	
		
		if(!empty($prd_question)){ 
			//$result_array = array('success' => 1, 'result' => $prd_question,'question_counts'=>$total_ques,'answers_counts'=>$total_answer,'error' =>"The F&Q listing Data."); 
			$result_array = array('success' => 1, 'result' => $prd_question,'question_counts'=>$total_ques,'answers_counts'=>$total_answer_count,'error' =>"The F&Q listing Data.");  
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $prd_question,'question_counts'=>$total_ques,'answers_counts'=>$total_answer, 'error' => "There is no question and answer for this product."); 
			echo json_encode($result_array); 
			die();
		}

	}catch(Exception $e){
		

		$result_array = array('success' => 0,'result' => $prd_question, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
		
		
	}
?>