<?php
	// Catgeory's product api. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
	$result = $_GET;
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	$categoryId = 3;
	try 
	{
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		// YOUR CATEGORY ID
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
							->addAttributeToSelect('entity_id')
							->setPageSize(3)   
							->addAttributeToFilter('is_saleable', 1, 'left')
							->addAttributeToFilter('status',1);  
		$product_counts = $category->getProductCollection()->count();
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();			
		foreach ($categoryProducts as $product) {
			$finalArray[]=$product->getId();
		} 
		if(empty($finalArray))
		{
			$data=array();
			$result_array = array('success' => 0,'result' => $data,'block_title'=>'You May Like', 'error' => 'No product found');  
			echo json_encode($result_array);	
			die;  
		}	
		$product_arrayy=array_unique($finalArray, SORT_REGULAR);
		foreach($product_arrayy as $value){ 
			$data[] = $value;
		} 
		foreach($data as $row)
		{
			$product_id=$row;	 
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
			
			if(!empty($product->getImage())){
				$imageUrl = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('300','300')->getUrl();
			}else{ 
				$site_url = $storeManager->getStore()->getBaseUrl();
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
			}
			
			//$imageUrl=$api_helpers->get_product_thumb($product_id);
			$sale_percentage='';
			$price=$product->getPrice();
			$specialprice=number_format($product->getSpecialPrice(), 2, '.', '');
			$specialfromdate = $product->getSpecialFromDate(); 
			$specialtodate = $product->getSpecialToDate();
			$ratings ='';
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
			if(!empty($RatingOb->getCount())){
				$ratings = $RatingOb->getSum()/$RatingOb->getCount(); 
			}
			if(!empty($ratings)){
				$rating_count = number_format($ratings, 2, '.', ''); 
			}else{ 
				$rating_count = null;
			}
			$sale = round((($price-$specialprice)/$price)*100); 
			$sale_percentage = $sale.' %'.'Off';   
			
			if(!empty(@get_numeric($result['user_id']))) 
			{ 
				$user_id=@get_numeric($result['user_id']);
				$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product_id);  
			}
			else
			{
				$whishlist=null;
			}  
			$product_array[]=array(
				'id'=>$product->getId(),	
				'name'=>trim($product->getName()), 	
				'sku'=>trim($product->getSku()), 	
				'image'=>$imageUrl,
				'product_price'=>number_format($product->getPrice(), 2, '.', ''),
				'rating_count'=>$rating_count,
				'wishlist'=>$whishlist   
			
			);
		}	
		//print_r($product_array);
		$result_array = array('success' => 1, 'result' => $product_array,'category_id'=> $categoryId,'block_title'=>'You May Like', 'error' =>"Product Found"); 
		echo json_encode($result_array); 
	} catch(Exception $e){
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data,'block_title'=>'You May Like', 'error' => $e->getMessage()); 
		echo json_encode($result_array);
	}
?>