<?php

//$result = sanitizedJsonPayload();
//print_r($result);
require "../security/sanitize.php";

$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result);
$user_array=null; 

if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'user Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
 

  

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);




$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 


if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else 
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	 
		die;
	}
	
	
}

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$select = $connection->select()
		->from('mst_rewards_transaction') 
		->where('customer_id = ?', $user_id);
$result = $connection->fetchAll($select);


$total_reward=0;
if(!empty($result))
{
	foreach($result as $row)
	{
		$total_reward=$total_reward + $row['amount'];  
	}	 
}	

$user_array=array('total_points'=>$total_reward);
 
$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Reward points'); 	
echo json_encode($result_array);	
die;	




?>