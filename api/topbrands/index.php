<?php
	// Top brands api
	//https://dev.idsil.com/design/api/topbrands/?accesstoken=1234
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	
	try{
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');	
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	$resource = $objectManager->get('Mageplaza\Shopbybrand\Helper\Data');
	$brandFactory = $objectManager->get('Mageplaza\Shopbybrand\Model\BrandFactory');
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	
	$shopbybrand_block = $objectManager->create('\Mageplaza\Shopbybrand\Block\Brand');
		 $featureBrands = [];
        $collection = $brandFactory->create()->getBrandCollection($storeManager->getStore()->getId());
        foreach ($collection as $brand) {
			$newOne = $brand->getData();
			$newFuture = $newOne['is_featured'];
            if ($newFuture == 1) {
                $featureBrands[] = $newOne;
            } 
			
        }
		// echo "<pre>";
			//print_r($featureBrands);
			foreach($featureBrands as $value){
				//print_r($value);
				
				$data['option_id'] = $value['option_id'];
				$data['attribute_id'] = $value['attribute_id'];
				$data['sort_order'] = $value['sort_order'];
				$data['default_value'] = $value['default_value'];
				$data['value'] = $value['value'];
				$data['image'] =  $storeManager->getStore()->getBaseUrl().'pub/media/'.$value['image'];
				
				/* $url=trim($value);  
				$url_details = $custom_helpers->get_url_type($url);
				$url_details_array = json_decode($url_details,true);
				// print_r($url_details_array);
				if($url_details_array['success']==1){
					$banner_array [$i]['url_type']=  $url_details_array['entity_type'];
					$banner_array [$i]['url_id']= $url_details_array['entity_id']; 
				} */
				
				$data['url_type']= 'topbrands';
				$data['url_id']= $value['option_id']; 
				$top_brands[] = $data;
			}
			// print_r($top_brands);
			// die();
		if(!empty($top_brands)){	
			$result_array=array('success' => 1, 'result' =>$top_brands,'block_title'=>'Top Brands', 'error' => 'Top brand Details');
			echo json_encode($result_array);  
		}else{
			$result_array=array('success' => 0, 'result' =>$top_brands,'block_title'=>'Top Brands', 'error' => 'There no brands assigned.');
			echo json_encode($result_array);
		}
		
	}catch(Exception $e){
		$result_array=array('success' => 0, 'result' =>$top_brands, 'error' => $e->getMessage());
		echo json_encode($result_array);		
	} 
	 
	