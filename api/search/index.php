<?php
	// Top search product api. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	 // print_r($result);
	
	$product_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty($result['key'])){
		$key = $result['key'];
	}else{
		$key = '';	
	}

	if($key ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Search key is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$key = $result['key'];
	}
	
	
	

	try
	{
		$i = 0; 
		$productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();	
		$collection = $productCollectionFactory->create();
		$collection->addAttributeToSelect('*');
		$collection->addAttributeToFilter(
		  [
		   ['attribute' => 'name', 'like' => '%'.$key.'%']
		  ]);
		
		$collection->setpagesize(20);  
		foreach ($collection as $product) {
			if($product->getStatus()==1){
				// print_r($product->getData());
				$product_array[$i]['id']= $product->getId();
				$product_array[$i]['name']= $product->getName();
				$product_array[$i]['sku']= $product->getSku();
				$product_array[$i]['price']= number_format($product->getPrice(), 2, '.', '');
				$product_array[$i]['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
				$product_array[$i]['url'] = $store->getBaseUrl() .$product->getUrlKey().'.html';				
				// echo "<br>";
				$i++;
			}
		}
		 // print_r($product_array);
		// die();
		
		$str_length=strlen($key);
		$newproduct=array();
		if($str_length > 2)
		{	 
			if(empty($product_array))
			{
				
				$key=substr($key,0,-1);
				
				$i = 0; 
				$productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
				$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();	
				$collection = $productCollectionFactory->create();
				$collection->addAttributeToSelect('*');
				$collection->addAttributeToFilter(
				  [
				   ['attribute' => 'name', 'like' => '%'.$key.'%']
				  ]);
				
				 
				foreach ($collection as $product) {
					if($product->getStatus()==1){
						// print_r($product->getData());
						
						$newproduct[]=$product->getId();
						
						
						$product_array[$i]['id']= $product->getId();
						$product_array[$i]['name']= $product->getName();
						$product_array[$i]['sku']= $product->getSku();
						$product_array[$i]['price']= number_format($product->getPrice(), 2, '.', '');
						$product_array[$i]['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
						$product_array[$i]['url'] = $store->getBaseUrl() .$product->getUrlKey().'.html';				
						// echo "<br>";
						$i++;
					}
				}	
				
			}	
		} 
		
		
		if(empty($newproduct))
		{
			if($str_length > 3)
			{	
				if(empty($product_array))
				{
					
					$key=substr($key,0,-2); 
					
					$i = 0; 
					$productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
					$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();	
					$collection = $productCollectionFactory->create();
					$collection->addAttributeToSelect('*');
					$collection->addAttributeToFilter(
					  [
					   ['attribute' => 'name', 'like' => '%'.$key.'%']
					  ]);
					
					 
					foreach ($collection as $product) {
						if($product->getStatus()==1){
							// print_r($product->getData());
							
							$newproduct[]=$product->getId();
							
							
							$product_array[$i]['id']= $product->getId();
							$product_array[$i]['name']= $product->getName();
							$product_array[$i]['sku']= $product->getSku();
							$product_array[$i]['price']= number_format($product->getPrice(), 2, '.', '');
							$product_array[$i]['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
							$product_array[$i]['url'] = $store->getBaseUrl() .$product->getUrlKey().'.html';				
							// echo "<br>";
							$i++;
						}
					}	
					
				}	
			}
		}		
		
		
		
		if(!empty($product_array)){
			$result_array = array('success' => 1, 'result' => $product_array, 'error' =>"Product assigned to this category."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $product_array, 'error' => "There is not product with this name. Please try some other keyword."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $product_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>