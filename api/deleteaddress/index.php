<?php
	/*
	Delete Delivery Address for the customer
	
	{ 
		"accesstoken": "1234",
		"address_id": "89"
	}
	 
	
	*/
    // SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
		require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection(); 
 
$argumentsCount=count($result);

/* if($argumentsCount < 1 || $argumentsCount > 1)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
} */	


$accesstoken=$result['accesstoken'];
$address_id=@get_numeric($result['address_id']);
$user_id=@get_numeric($result['user_id']);





$user_array=null; 

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');  

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}		
		
	}


if($address_id=='') 
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Address Id is missing'); 	
	echo json_encode($result_array);	
	die;
} 
else
{
	$select = $connection->select()
		->from('customer_address_entity') 
		->where('entity_id = ?', $address_id);
		
	$results = $connection->fetchAll($select);	
	if(empty($results))
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Address Id is not exists'); 	
		echo json_encode($result_array);	
		die; 
	}  
}


	




   
  
try
{
	$address_id;
	
	$sql = "select * from customer_address_entity where entity_id='".$address_id."'";
	$result = $connection->fetchAll($sql); 
	$user_id=$result[0]['parent_id']; 
  
	$delete=$api_helpers->delete_address($address_id); 
	      
	if($delete=='1')
	{ 
		
		$sql = "select count(*) 'total_address' from customer_address_entity where parent_id='".$user_id."'";
		$result = $connection->fetchAll($sql); 
		if(!empty($result))
		{	
			$total_address=$result[0]['total_address']; 
			
			if($total_address==1)
			{
				
				$sql = "select * from customer_address_entity where parent_id='".$user_id."' order by entity_id desc limit 0,1";
				$results = $connection->fetchAll($sql); 
				$address_id=$results[0]['entity_id']; 
				
				
				$sql="update customer_entity set default_billing='".$address_id."',default_shipping='".$address_id."' where entity_id='".$user_id."'";
				$connection->query($sql);	   	 
			}	
		}
		
		
		
		
		
		$user_array=array('address_id'=>$address_id); 
		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Address has been deleted');   	
		echo json_encode($result_array);	
		die; 
	}   
	else
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Address not found');   	
		echo json_encode($result_array);	
		die; 
	}
	  
} 
catch(Exception $e) 
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Error in api'); 	
	echo json_encode($result_array);	
	die;
}   


?>

	