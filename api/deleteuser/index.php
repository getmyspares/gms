<?php

$result_array=array('success' => 0,'result' => $product_array , 'error' => 'depreciated api'); 	
echo json_encode($result_array);	
die;
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$objectManager->get('Magento\Framework\Registry')->register('isSecureArea', true);
	$result = $_GET;
	$product_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	$user_id = @get_numeric($result['user_id']); 
	// die();
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
	try{	
		
		$customerID = $user_id;
		//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$customer = $objectManager->create('Magento\Customer\Model\Customer')
		->load($user_id);
		$customer->delete();
		
		$result_array = array('success' => 1,'result' =>' Customer deleted.'); 

		echo json_encode($result_array);
	
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' =>$product_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>