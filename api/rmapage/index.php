<?php


/*
{ 
	"accesstoken": "1234",
	"item_id": "720",
	"product_id": "1258",
	"order_id": "578"
	"user_id": "578"
}
*/  
  
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);

//$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result); 
$user_array=null; 
 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
$store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getStore();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

if($argumentsCount < 4 || $argumentsCount > 4)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}  

 
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['item_id']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset(@get_numeric($result['product_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['order_id']) || empty(@get_numeric($result['order_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order Id is missing'); 	
	echo json_encode($result_array);	  
	die; 
}

if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
$authentication_response = $api_helpers->authenticateUser(@get_numeric($result['user_id']),$result['accesstoken']);
if($authentication_response['success']==0)
{
	echo json_encode($authentication_response);	
	die;
}
  
   

$accesstoken=$result['accesstoken'];
$item_id=$result['item_id'];
$product_id=@get_numeric($result['product_id']);
$order_id=$result['order_id']; 

   


$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
 

  
       

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

	 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');

$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);

if(empty($order->getAllItems()))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order item not  found'); 	
	echo json_encode($result_array);	  
	die; 
}

foreach ($order->getAllItems() as $item)
{
	$item_ids=$item->getId();
	if($item_ids==$item_id) 
	{	
		$item_ordered=$item->getQtyOrdered();
	} 
	
}
$item_ordered=(int)$item_ordered;

$image=''; 
if(!empty($product->getImage())) 
{ 
							
	$image = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
}
else{  
	$site_url = $storeManager->getStore()->getBaseUrl();
	$imageUrl = $site_url.'Customimages/product-thumb.jpg';
	$image = $imageUrl; 
} 
 

$productarray=array(
	'name'=>$product->getName(),
	'sku'=>$product->getsku(),
	'image'=>$image

); 

//print_r($productarray); 


$select = $connection->select()
		  ->from('wk_rma_reason')  
		  ->where('status = ?', '1');
$reasons = $connection->fetchAll($select);

/* echo "<pre>";
	print_r($reasons);
echo "</pre>"; */

foreach($reasons as $row)
{
	$reasonsarray[]=array(
		'id'=>$row['id'],
		'name'=>$row['reason'],
		'status'=>$row['status'],
	);	
}	


/* echo "<pre>";
	print_r($reasonsarray);
echo "</pre>";  */
 
 
$resolution=array(
	'0'=>array(
		'id'=>0,
		'name'=>'Refund'
	),
	'1'=>array(
		'id'=>1,
		'name'=>'Exchange'
	)
);

$package_condition=array(
	'0'=>array(
		'id'=>0,
		'name'=>'Open'
	),
	'1'=>array(
		'id'=>1,
		'name'=>'Packed' 
	)
);

$select = $connection->select() 
		  ->from('wk_rma_items') 
		  ->where('order_id = ?', $order_id)
		  ->where('item_id = ?', $item_id); 
$result = $connection->fetchAll($select);
$total_item_qty=0;

if(!empty($result))
{
	
	foreach($result as $row)
	{
		$item_qty=$row['qty'];	
		$total_item_qty=$total_item_qty+$item_qty;
	
	}		
}	


/* echo "<pre>";
	print_r($resolution);
echo "</pre>";  */




$user_array=array('product'=>$productarray,'reasons'=>$reasonsarray,'resolution'=>$resolution,'package_condition'=>$package_condition,'item_ordered'=>$item_ordered,'already_return_qty'=>$total_item_qty); 
$result_array=array('success' => 1, 'result' => $user_array, 'error' => 'Rma Page Found');	 
echo json_encode($result_array);	 
die;



?>



