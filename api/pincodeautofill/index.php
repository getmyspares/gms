<?php
require "../security/sanitize.php";
$result=$_GET;

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$argumentsCount=count($result); 
$user_array=null; 

if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['pincode']) ) 
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Pincode is missing'); 	
	echo json_encode($result_array);	  
	die; 
}
$accesstoken = $result['accesstoken'];
$pincode     = $result['pincode'];

$pincodeModel = $objectManager->create('OM\Pincodes\Model\Pincodes'); 

$arr = array(				
	'BH' => 'BR',
	'PY' => 'PD',
	'OR' => 'OD',
	'TS' => 'TG'
);

$pcollection = $pincodeModel->getCollection()
				->addFieldToFilter('pincode',array('eq'=>$pincode))
				->getData();
$datas = array();
foreach($pcollection as $pcodes){
	$datas['city_name'] = $pcodes['city_name'];
	$state = $pcodes['state'];
	$scode = array_key_exists($state,$arr) ? $arr[$state] : $state;				
	$st = $connection->fetchOne("SELECT region_id FROM `directory_country_region` WHERE country_id='IN' AND code='".$scode."'");				 
	$datas['state']     = $st;				
}	 

if(empty($datas)){
	$result_array = array('success' => 0,'result' => $datas,'error' =>"Pincode not found."); 
	echo json_encode($result_array);	
	die;
}else{
	$result_array = array('success' => 1,'result' => $datas,'error' =>""); 
	echo json_encode($result_array);	
	die;
}
?>
