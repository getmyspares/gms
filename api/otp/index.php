<?php
	// OTP check for individual customer.  
	/*
	
	Example of json format for the OTP match
	{
		"accesstoken": "3g55t6xksz7j1uxj4buu9h71vjprskai",
		"customer_id":"387",
		"systemotp":"1245",
		"customerotp":"1245"
	}
	
	*/
	require "../security/sanitize.php";
	$result = sanitizedJsonPayload(); 
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	if($result['systemotp']==$result['customerotp']){
		try
		{
			$url = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
			$state = $objectManager->get('\Magento\Framework\App\State'); 
			$state->setAreaCode('frontend');
			// Get Store ID
			$store = $storeManager->getStore();
			$storeId = $store->getStoreId();
			$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory'); 
			$customer=$customerFactory->create();
			$customer_id = 	@get_numeric($result['customer_id']);
			if(empty($customer_id))
			{
				$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User Id is missing'); 	
				echo json_encode($result_array);	
				die;
			}

			$customer->load($customer_id);
			$customer->setConfirmation(null);
			$customer->save();
			$array = array('success' => 1, 'error' => 'User is successfully update'); 
			echo json_encode($array);
		
		}catch(Exception $e){
			
			$array = array('success' => 0, 'error' => $e->getMessage()); 
			echo json_encode($array);
			
		}
		// echo "same otp";
	}else{
		// echo "error";
		$array = array('success' => 0, 'error' => 'OTP is mismatch.Please try again.'); 
		echo json_encode($array);
	}
	
?>