<?php

//$result = sanitizedJsonPayload();
//print_r($result);

require "../security/sanitize.php";
$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result);
$user_array=null; 

if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'user Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
  

  

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);




$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 


if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else 
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
	
}

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$total_spent_reward=0;
$select = $connection->select()
		->from('mst_rewards_transaction') 
		->where('customer_id = ?', $user_id)
		->where('amount < ?', '0');
$result = $connection->fetchAll($select);


if(!empty($result))
{
	foreach($result as $row)
	{
		$total_spent_reward=$total_spent_reward + $row['amount'];  
	}	 
}


$total_spent_reward=abs($total_spent_reward);


$select = $connection->select()
		->from('mst_rewards_transaction') 
		->where('customer_id = ?', $user_id);
$result = $connection->fetchAll($select);


$total_reward=0;

if(!empty($result))
{
	foreach($result as $row)
	{
		$total_reward=$total_reward + $row['amount'];  
	}	 
}	




$user_array=array('total_points'=>$total_reward);

/*
$select = $connection->select()
		->from('mst_rewards_transaction') 
		->where('customer_id = ?', $user_id);
*/		   
  		
$select="select * from mst_rewards_transaction where customer_id='".$user_id."' order by transaction_id desc";		
$result = $connection->fetchAll($select); 
					
if(!empty($result))   
{
	foreach($result as $row)
	{
		if($row['amount'] > 0)
		{
			$status="green";	
		}	
		else
		{
			$status="red";	
		} 
		$history[]=array(
			'id'=>$row['transaction_id'],
			'comment'=>$row['comment'],
			'points'=>$row['amount'],
			'color'=>$status,
			'created_at'=>date("d M,y", strtotime($row['created_at']))
		);	 
	}	

	$columns = array_column($history, 'id');
	array_multisort($columns, SORT_DESC, $history);	
	
	
	
}	
else
{
	$history=array(); 
}  

	
 
//print_r($history);  

$current_reward='';
$spent=$total_spent_reward;
$balance=$total_reward-$spent;

if($balance > 1000 && $balance < 2000)
{
	$current_reward="Bronze";	
}
else if($balance > 2000 && $balance < 5000)
{
	$current_reward="Silver";	
}
else if($balance > 5000)  
{
	$current_reward="Gold";	
}
else
{
	$current_reward="0";	 
}	

$rewardstatus=array(
	'current_reward'=>$current_reward,
	'earned'=>$total_reward,
	'balance'=>$balance,
	'spent'=>$spent
	
);
 
 
$user_array=array('total_points'=>$total_reward,'rewardstatus'=>$rewardstatus,'history'=>$history); 

 
$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Reward points'); 	 
echo json_encode($result_array);	
die;	

 


?>