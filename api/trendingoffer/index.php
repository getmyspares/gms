<?php
	// Catgeory's product api. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);
	
	$itemsPerPage=10;

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	
	if($page_no=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Page no is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	
	$categoryId = 4;
	//$categoryId = 35; 



	try 
	{
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		$collection = $productCollection
			->addMinimalPrice()
			->addFinalPrice()
			->addAttributeToSelect('part_type')
            ->addAttributeToFilter('part_type', '236')    
			->addAttributeToFilter('status',1)
			->addAttributeToSelect('special_to_date')
			->addAttributeToFilter('special_price', ['neq' => ''])
			->addAttributeToFilter('is_saleable', 1, 'left')
			->addAttributeToSort('created_at', 'DESC')
			->setPageSize(5)
			->setCurPage($page_no);	  
			
		$collection->getSelect()->where('price_index.final_price < price_index.price');
		$collection->load();
		
		$excitingDeals = $collection->getData();

		//$special_price=$api_helpers->check_product_special_price('18432');
		
		$today = time(); 
		$price_return=0; 	
		$finalArray=array();	
		foreach ($excitingDeals as $product) { 
			$product_id=$product['entity_id'];
			//$special_price=$api_helpers->check_product_special_price($product_id);
			//$product_special_price=$api_helpers->check_product_special_price($product_id);
			//if($product_special_price==1 || true)
			//{	
				$finalArray[]=$product_id;
			//}
		} 
		if(empty($finalArray))
		{
			$data=array();
			$result_array = array('success' => 1,'result' => $data, 'error' => 'No product found');  
			echo json_encode($result_array);	
			die;
		}	
		
		$product_arrayy=array_unique($finalArray, SORT_REGULAR);
		$product_count = count($product_arrayy); 
		$pages_count = ceil($product_count/$itemsPerPage); 
		if($page_no > $pages_count)  
		{
			$result_array = array('success' => 0,'result' => null, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array); 
			die();
		}
		$offset = abs($itemsPerPage * ($page_no - 1)); 
		$dataa= array_slice($product_arrayy,$offset,$itemsPerPage);
		foreach($dataa as $row)
		{
			$product_id=$row;	 
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
			$imageUrl=$api_helpers->get_product_thumb($product_id);
			$sale_percentage='';
			$price=$product->getPrice();
			$specialprice=number_format($product->getSpecialPrice(), 2, '.', '');
			$specialfromdate = $product->getSpecialFromDate(); 
			$specialtodate = $product->getSpecialToDate();
			$ratings ='';
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
			if(!empty($RatingOb->getCount())){
				$ratings = $RatingOb->getSum()/$RatingOb->getCount(); 
			}
			if(!empty($ratings)){
				$rating_count = $ratings;
			}else{
				$rating_count = null;
			}
			$sale=null;
			$sale_percentage=null;
			if($product->getSpecialPrice()!='')
			{	
				$sale = round((($price-$specialprice)/$price)*100); 
				$sale_percentage = $sale.' %'.'Off';   
			}
			$product_array[]=array(
				'id'=>$product->getId(),	
				'name'=>$product->getName(),	
				'sku'=>$product->getSku(),	
				'price'=>number_format($product->getPrice(), 2, '.', ''),
				'image'=>$imageUrl,
				'specialprice'=>$specialprice,
				'specialfromdate'=>$specialfromdate,
				'specialtodate'=>$specialtodate,
				'sale_percentage'=>$sale_percentage,
				'rating_count'=>$rating_count
			);
		}	
		//print_r($product_array);
		if(!empty($dataa)){
			$result_array = array('success' => 1, 'result' => $product_array,'category_id'=> $categoryId,'product_count'=>$product_count,'pages_count'=>$pages_count,'error' =>"Product details."); 
			echo json_encode($result_array);
		}else{ 
			$data=array();
			$result_array = array('success' => 0, 'result' => $data, 'error' => "Product not found");  
			echo json_encode($result_array);
			die();
		} 
	}catch(Exception $e){
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
	}
?>