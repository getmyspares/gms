<?php
/*
FOR Company Buyer json format

{
	"accesstoken": "1234",
	"email": "amit.sandal11@idsil.com",
	"password": "tetttet",
	"mobile": "1234567890",
	"groupid": "1",
	"company_name": "company name",
	"company_address": "company address",
	"gst_number": "gst number",
	"pan_number": "pan number"
}
*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.

require "../security/sanitize.php";
$result = sanitizedJsonPayload();
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface'); 
$state = $objectManager->get('\Magento\Framework\App\State');
$connection = $resource->getConnection();

$state->setAreaCode('frontend');
$group_array=array('1','4','5');  
$null = null;
$argumentsCount=count($result);
$user_array=null;
$missing="Required Parameters Are  Missing";
$alibi="Cannot Create Account With The Details Provided";

if($argumentsCount < 5 || $argumentsCount > 12)
{ 
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}        
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['password']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['mobile']) || empty(@get_numeric($result['mobile'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['groupid']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	
	die; 
}	 


$accesstoken=$result['accesstoken'];
$email=$result['email'];
$password=  base64_decode($result['password']);
$mobile=@get_numeric($result['mobile']); 
$groupid=$result['groupid'];  

if($groupid==4){
	$company_name=$result['company_name'];  
	$company_address=$result['company_address'];  
	$gst_number=$result['gst_number'];  
	$pan_number=$result['pan_number'];  
}
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => "$missing"); 	
	echo json_encode($result_array);	
	die;
}
else
{
	if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
		echo json_encode($result_array);	
		die;
	} 
} 
if($password=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	
	die;
}
 

if($mobile=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => $missing); 	
	echo json_encode($result_array);	
	die;
}
else  
{
	$check_mobile=$app_helpers->check_mobile_exists($mobile);
	if($check_mobile==1)
	{	
		$result_array=array('success' => 0, 'result' => $user_array,'error' => "Email/ Mobile Alread Exist.");  	
		echo json_encode($result_array);	 
		die;  
	} 
}

if($groupid=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => "$missing"); 	
	echo json_encode($result_array);	
	die;
}
else  
{
	if (!in_array($groupid, $group_array)) {
		$result_array=array('success' => 0, 'result' => $user_array,'error' => "$alibi"); 	
		echo json_encode($result_array);	 
		die;  
	}
}

if($groupid==4){
		
	if($company_name=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => "$missing"); 	
		echo json_encode($result_array);	
		die;
	}
	
	if($company_address=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => "$missing"); 	
		echo json_encode($result_array);	
		die;
	}
	if($gst_number=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => "$missing"); 	
		echo json_encode($result_array);	
		die;
	}
	if($pan_number=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => "$missing"); 	
		echo json_encode($result_array);	
		die;
	}
}
 
$check_email=$app_helpers->check_email_exists($email); 
if($check_email > 0)
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => "Email/ Mobile Alread Exist."); 	
	echo json_encode($result_array);	 
	die;
}
else
{
	$check_email = $app_helpers->check_email_temp_exists($email);
	if($check_email > 0 && false)
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'You cancel OTP process. Please try again after some times'); 	
		echo json_encode($result_array);	 
		die; 

	}  
	
	if(($groupid==1 || $groupid==5))
	{
		$user_array=array('email'=>$email,'mobile'=>$mobile);
		$app_helpers->create_otp($result);
		$error="OTP has been sent. Please check mobile and email";
		$result_array=array('success' =>1, 'result' => $user_array,'error' => $error); 	
		echo json_encode($result_array);	   
		die;
	}
	else
	{
		 
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');
		$websiteId = $storeManager->getWebsite()->getWebsiteId();
		$store = $storeManager->getStore(); 
		$storeId = $store->getStoreId();

		$customer = $customerFactory->create();
		$customer->setWebsiteId($websiteId);
		$customer->setEmail($email);
		$customer->setPassword($password);
		$customer->setMobile($mobile); 
		$customer->setFirstname('firstname');
		$customer->setLastname('lastname');
		$customer->setCompanyNam($company_name);
		$customer->setCompanyAdd($company_address);
		$customer->setGstNumber($gst_number);
		$customer->setPanNumber($pan_number);
		$customer->setAttributeApproved('1');  
		$customer->save();
		$custID=$customer->getId();  
 
		$group_id=$groupid; 
		$sql = "update customer_grid_flat set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
		$connection->query($sql);
		
		$sql = "update customer_entity set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
		$connection->query($sql);     
		
		$sql = "INSERT INTO `customer_entity_int`(`attribute_id`, `entity_id`, `value`) VALUES ('177','".$custID."','1')";
		$connection->query($sql);    
  
		$user_array=array('user_id'=>$custID,'email'=>base64_encode($email),'mobile'=>base64_encode($mobile));
		$result_array=array('success' =>1, 'result' => $user_array,'error' => 'Please  login to access  your  account');  	
		echo json_encode($result_array);	   
		die;	 
		 
	}
	
}	

