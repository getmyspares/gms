<?php
	// Already existing customer details update file.
	
	/*
	
	FOR Company Buyer json format
	
	{"accesstoken": "3g55t6xksz7j1uxj4buu9h71vjprskai","customerid":"388","firstname":"Amit","lastname":"reres", "password":"tetttet","postcode":"123456","mobile":"1234567890","groupid":"4","company_name":"iiiiddddssss","company_address":"address details","gst_number":"ASDS2371837AAAA","pan_number":"12313123" }
	
	For Invidual Buyer jsn format
	
	{"accesstoken": "3g55t6xksz7j1uxj4buu9h71vjprskai","customerid":"388","email":"amit.sandal11@idsil.com","firstname":"Amit","lastname":"last", "password":"tetttet","postcode":"123456","mobile":"1234567890","groupid":"1" }
	
	*/
	require "../security/sanitize.php";
	$result = sanitizedJsonPayload();
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
// die();	 
	 
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();

	
	
	$customer_array = null;
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $customer_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $customer_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	//$customer_id = $result['customerid'];
	
	if(!empty($result['customerid'])){
		$customer_id = $result['customerid'];
	}else{
		$customer_id = '';	
	}

	if($customer_id ==''){
		$result_array=array('success' => 0, 'result' => $customer_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}

	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');	
	$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}	
	
	
	// Fields details to be updated
	if(!empty($result['firstname'])){
		$firstname = $result['firstname'];
	}
	if(!empty($result['lastname'])){
		$lastname = $result['lastname'];
	}
	if(!empty(@get_numeric($result['mobile']))){
		$mobile = @get_numeric($result['mobile']);
	}
	if(!empty(@get_numeric($result['postcode']))){
		$postcode = @get_numeric($result['postcode']);
	}
	//Company Buyer details
	if(!empty($result['company_name'])){
		$company_name = $result['company_name'];
	}
	if(!empty($result['company_address'])){
		$company_address = $result['company_address'];
	}
	if(!empty($result['gst_number'])){
		$gst_number = $result['gst_number'];
	}
	if(!empty($result['pan_number'])){
		$pan_number = $result['pan_number'];
	}
	
	try
	{
		$url = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
		$state = $objectManager->get('\Magento\Framework\App\State'); 
		$state->setAreaCode('frontend');
		// Get Store ID
		$store = $storeManager->getStore();
		$storeId = $store->getStoreId();
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory'); 
		$customer=$customerFactory->create();
		
		$customerRepository = $objectManager->create('Magento\Customer\Api\CustomerRepositoryInterface');
 
		
		
		// making mobile data array 
		$mobiledata = array();
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->getCollection();
		foreach($customerObj as $customerObjdata )
		{
			// print_r($customerObjdata ->getData());
			$customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
			$customer2 = $customerRepository->getById($customerObjdata ->getId());
			$cattrValue = $customer2->getCustomAttribute('mobile');
			if($cattrValue){
				$mobileno = $cattrValue->getValue();
			}
			$mobiledata[] = $mobileno;
			
		}
		// print_r($mobiledata);
		
		$customer->load($customer_id); // add customer id 
		$customerData = $customer->getDataModel();
		
		$group_id = $customer->getGroupId();
		
		$argumentsCount=count($result);
		if($group_id==1){
			if($argumentsCount < 2 || $argumentsCount > 9)
			{
				$result_array=array('success' => 0,'result' => $customer_array , 'error' => 'Input count not matched'); 	
				echo json_encode($result_array);	
				die;
			}
			
		}
		if($group_id==4){
			if($argumentsCount < 2 || $argumentsCount > 11)
			{
				$result_array=array('success' => 0,'result' => $customer_array , 'error' => 'Input count not matched'); 	
				echo json_encode($result_array);	
				die;
			}
			
		}
		
		if(!empty($firstname)){
			$customer->setFirstname($firstname);
		}
		if(!empty($lastname)){
			$customer->setLastname($lastname);
			
		}
		
		if(!empty($mobile)){

			$currentmobile = $customer->getMobile();
			if($currentmobile != $mobile){
				if (in_array($mobile, $mobiledata)) {
					$array = array('success' => 0, 'error' => 'This mobile number already exist in database please use another mobile number.');
					echo json_encode($array);
					die();
				}else{
					$customerData->setCustomAttribute('mobile',$mobile);
				}
			}
			
		}
		
		
		// $customer->setGroupId($group_id);
		
		if($group_id==4){
			//echo 'i am in loop';
			if(!empty($company_name)){
				//company_nam
				$customerData->setCustomAttribute('company_nam',$company_name);
				
				
			}
			if(!empty($company_address)){
				//company_add
				$customerData->setCustomAttribute('company_add',$company_address);
				
			}
			if(!empty($gst_number)){
				//gst_number
				$customerData->setCustomAttribute('gst_number',$gst_number);
				
			}
			if(!empty($pan_number)){
				//pan_number
				$customerData->setCustomAttribute('pan_number',$pan_number);
				
			}
		}
		$customer->updateData($customerData);
		$customer->save();
		$array = array('success' => 1, 'message' => 'User is updated successfully'); 

		echo json_encode($array);
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$array = array('success' => 0, 'error' => $e->getMessage()); 

		echo json_encode($array);
		
		
	}