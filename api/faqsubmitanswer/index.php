<?php
	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/faqsubmitanswer/
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	// $result = $_GET;
	$result = sanitizedJsonPayload();
	/*{"accesstoken": "1234","question_id":"1399","customer_id":"269","content":"text of content"}*/

	// print_r($result);
	// die();
	
	
	$response = null;
	$argumentsCount=count($result);

	if($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	if(!empty($result['question_id'])){
		$question_id = $result['question_id'];
	}else{
		$question_id = '';	
	}
	
	if($question_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Question id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$question_id = $result['question_id'];
	}
	
	if(!empty(@get_numeric($result['customer_id']))){
		$customer_id = @get_numeric($result['customer_id']);
	}else{
		$customer_id = '';	
	}
	
	if($customer_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Customer id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$customer_id = @get_numeric($result['customer_id']);
	}
	
	/*
	if(!empty($result['nickname'])){
		$nickname = $result['nickname'];
	}else{
		$nickname = '';	
	}
	
	if($nickname ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'nickname is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$nickname = $result['nickname'];
	}*/
	
	
	if(!empty($result['content'])){
		$content = $result['content'];
	}else{
		$content = '';	
	}
	
	if($content ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'content data is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$content = $result['content'];
	}
	


	try
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('wk_qaanswer'); //gives table name with prefix
		
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customer_id);
		$nickname = $customerObj->getFirstName()." ".$customerObj->getLastName();
		
		//Insert Data into table
		$now = date('Y-m-d H:i:s');
		$sql = "Insert Into " . $tableName . " (question_id, respond_from, respond_nickname, respond_type, content, status, created_at) Values ('$question_id','$customer_id','$nickname','Customer','$content',0,'$now')";
		$connection->query($sql);		
		

		$result_array = array('success' => 1,'error' =>"Answer have been added to this question."); 
		echo json_encode($result_array);
		die();
		

	}catch(Exception $e){		

		$result_array = array('success' => 0,'result' => null, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
?>