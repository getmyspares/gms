<?php
	// Catgeory's product api. 
require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	$categoryId = 4;
	//$categoryId = 35;



	try 
	{
		//$categoryId = $_GET['cat_id'];
	 
		 
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		 
		 // YOUR CATEGORY ID
		/*
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
									->addAttributeToSelect('*')
									->addAttributeToFilter('status',1);  
									 
		$product_counts = $category->getProductCollection()->count();
		
		*/
		
		
		
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');

		$collection = $productCollection->addAttributeToSelect('*')
		->addAttributeToFilter('part_type', '234') 
		// ->addAttributeToFilter('special_price', ['neq' => '']) 
		->addAttributeToSort('created_at','DESC')
		->setPageSize(5)
		->load();
		
		
		
		
		
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();			
		
		
		foreach ($collection as $product) {
			
			$product_id=$product['entity_id'];
			$special_price=$api_helpers->check_product_special_price($product_id);
			
			$product_special_price=$api_helpers->check_product_special_price($product_id);
			if($product_special_price==1 || true)
			{	
				$finalArray[]=$product_id;
			}
			 
		}  
		
		//print_r($finalArray);
		
		if(empty($finalArray))
		{
			$data=array();
			$result_array = array('success' => 1,'result' => $data, 'error' => 'No product found');  
			echo json_encode($result_array);	
			die;
		}	
		
		$product_arrayy=array_unique($finalArray, SORT_REGULAR);
		
		foreach($product_arrayy as $value){ 
			$data[] = $value;
		} 
		
		foreach($data as $row)
		{
			$product_id=$row;	 
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
			
			$imageUrl=$api_helpers->get_product_thumb($product_id);
			
			
			$sale_percentage='';
			$price=$product->getPrice();
			$specialprice=number_format($product->getSpecialPrice(), 2, '.', '');
			
			
			
			$specialfromdate = $product->getSpecialFromDate(); 
			$specialtodate = $product->getSpecialToDate();
			
			 
		
			$ratings ='';
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
			if(!empty($RatingOb->getCount())){
				$ratings = $RatingOb->getSum()/$RatingOb->getCount(); 
			}
			if(!empty($ratings)){
				$rating_count = $ratings;
			}else{
				$rating_count = null;
			}
			
			 
			if(!empty((int)($specialprice)))
			{
				$sale = round((($price-$specialprice)/$price)*100); 
			} else 
			{
				$sale=0;
			}
			
			
			$sale_percentage = $sale.' %'.'Off';   
			$product_array[]=array(
				'id'=>$product->getId(),	
				'name'=>$product->getName(),	
				'sku'=>$product->getSku(),	
				'price'=>number_format($product->getPrice(), 2, '.', ''),
				'image'=>$imageUrl,
				'specialprice'=>$specialprice,
				'specialfromdate'=>$specialfromdate,
				'specialtodate'=>$specialtodate,
				'sale_percentage'=>$sale_percentage,
				'rating_count'=>$rating_count
			
			);
			 
			
		}	
		
		
		//print_r($product_array);
	
		$result_array = array('success' => 1, 'result' => $product_array,'category_id'=> $categoryId, 'error' =>"Trending product details."); 
		echo json_encode($result_array);
		
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
	}
?>