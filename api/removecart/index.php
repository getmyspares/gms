<?php
	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/removecart/?accesstoken=1234&product_id=1399&user_id=523 
	//https://www.dev.idsil.com/design/api/removecart/?accesstoken=1234&item_id=1941&user_id=523 
	//Input 
	///*{"accesstoken": "1234","product_id":"1399","user_id":"269","qty":"2"}*/
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
			
	//$result = sanitizedJsonPayload();
	// print_r($result);
	// die();
	
	$result=$_GET;
	
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
		
	$response = null;
	$argumentsCount=count($result);
	$user_array=null;
	if($argumentsCount < 3 || $argumentsCount > 3)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}   
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	
	
	if(!empty($result['item_id'])){
		$item_id = $result['item_id'];
	}else{
		$item_id = '';	
	}
	 
	  
	
	
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	 
			die;
		}	
		
		
	}
	
	
	
	//$token=$api_helpers->get_token();
	//$quote_id=$api_helpers->get_quote_id($token,$user_id);
	//$quote_id=json_decode($quote_id);
	
	//$arrayss=$api_helpers->get_user_token_details($user_id);
	//print_r($arrayss);
	//die; 
	//$token=$arrayss['token'];
	//echo $quote_id=$arrayss['quote_id'];   
	//die;  
	
	$sql="delete from quote_item where item_id='".$item_id."'"; 
	$connection->query($sql); 
	
	$response=array('user_id'=>$user_id);
	$result_array=array('success' => 1, 'result' => $response, 'error' => 'Product Removed'); 	
	echo json_encode($result_array);	  
	die;
	
	  
	/*
	$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
	$allItems=$quote->getAllVisibleItems();
	
	
	$itemIdarray=array();
	foreach ($allItems as $item) {
		
		$itemIdarray[] = $item->getItemId();
		
	}
	
	
	
	if (!in_array($item_id, $itemIdarray)) 
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Item Id not exists'); 	
		echo json_encode($result_array);	 
		die; 		
	}	
	else
	{
		foreach ($allItems as $item) {
		
			$itemId = $item->getItemId();
			if($itemId==$item_id)
			{	
				$quoteItem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
				$quoteItem->delete();
			}
		}	
	}
	
	$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
	$allItems=$quote->getAllVisibleItems();
	
	if(empty($allItems))
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Cart is empty now'); 	
		echo json_encode($result_array);	
		die; 	
	}	 
	else 
	{
		$response=array('user_id'=>$user_id);
		$result_array=array('success' => 1, 'result' => $response, 'error' => 'Product Removed'); 	
		echo json_encode($result_array);	 
		die;
	}
	*/
	
	
?>