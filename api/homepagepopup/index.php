<?php
	// Help and Support  api.
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	//print_r($result);
	
	
	$page_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $page_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $page_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $page_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$is_enable = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('homepagepopup/mobile/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$mobile_image = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('homepagepopup/mobile/image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

		if($is_enable){
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$base_url = $storeManager->getStore()->getBaseUrl();
			$image_url = $base_url."pub/media/homepopup/mobile/".$mobile_image;
			$result_array = array('success' => 1, 'result' => $image_url, 'error' => 'no error');
			echo json_encode($result_array);
			die();
		}else{
			$result_array =array('success' => 0, 'result' =>"Null",'error' => 'Popup Disabled');
			echo json_encode($result_array);
			die();
		}
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$page_array, 'error' => $e->getMessage());
		echo json_encode($result_array);
		die();		
	} 
?>