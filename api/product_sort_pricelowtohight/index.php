<?php

	// Catgeory's product api. 
	//https://dev.idsil.com/design/api/product_sort_pricelowtohigh/?cat_id=56&accesstoken=1234&page_no=2
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 3 || $argumentsCount > 5)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	 
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		  
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	}

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}

	try
	{
		//$categoryId = $_GET['cat_id'];
	 
		 
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		 
		 // YOUR CATEGORY ID
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
									 ->addAttributeToSelect('*')->setCurPage($page_no) // page Number
									 ->addAttributeToFilter('status', 1)
									 // ->addAttributeToSort('price', 'DESC')
									 ->addAttributeToSort('price', 'ASC')
									 ->addAttributeToFilter('visibility', 4)
									 ->setPageSize(10); // elements per pages
									 
		$product_counts = $category->getProductCollection()->count();
									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();			
		
		$i = 1 ;
		foreach ($categoryProducts as $product) {
			// if($product->getStatus()==1){
				$id = $product->getId();
				//$products[$id]= $product->getData();
				$product_array['id']= $product->getId();
				$product_array['name']= $product->getName();
				$product_array['sku']= $product->getSku();
				$product_array['price']= $product->getPrice();
				$product_array['specialprice'] = $product->getSpecialPrice();
				$product_array['specialfromdate'] = $product->getSpecialFromDate();
				$product_array['specialtodate'] = $product->getSpecialToDate();
				
				
				//$product_array['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
				if(!empty($product->getImage())){
							
					$product_array['image'] = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
				}else{
					$site_url = $storeManager->getStore()->getBaseUrl();
					$imageUrl = $site_url.'Customimages/product-thumb.jpg';
					$product_array['image'] = $imageUrl;
				}
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$product_array['rating_count'] = $ratings;
				}else{
					$product_array['rating_count'] = null;
				}
				
				if(isset(@get_numeric($result['user_id'])))
				{  
					$user_id=@get_numeric($result['user_id']);
					$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
					$product_array['wishlist']=$whishlist;
				} 
				else
				{
					$product_array['wishlist']=null;
				} 
				
				
				
				$data[] = $product_array;
				$i++;
			// }
		}
		$product_array['product_count'] = $product_counts;
		$product_array['pages_count'] = ceil($product_counts/10);
		// print_r($product_array);
		// die();
		
		if( ($page_no) >($product_array['pages_count']) ){
			$result_array = array('success' => 0,'result' => null, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array);
			die();

		}
		
		
		if(!empty($data)){
			$result_array = array('success' => 1, 'result' =>$data,'product_count'=>$product_array['product_count'],'pages_count'=>$product_array['pages_count'], 'error' =>"Product assigned to this category."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $data, 'error' => "The category id is not valid. Please check the category. Or there is not product assigned to this category."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $product_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>