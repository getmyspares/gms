<?php
 require "../security/sanitize.php";
 

/*
Cancel Registration Process

{
	"accesstoken": "1234",
	"email": "amit.sandal11@idsil.com",
	
}
  



*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.

$result = sanitizedJsonPayload();
if(!is_array($result) || (is_array($result) && empty($result))) {
	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
	die;
}
//die;
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data');

$url = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface'); 

$state = $objectManager->get('\Magento\Framework\App\State');
		$state->setAreaCode('frontend');

$group_array=array('1','4','5');  
$null = null;
 $argumentsCount=count($result);

$user_array=null;

if($argumentsCount < 2 || $argumentsCount > 2)
{ 
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}        
	
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Email is missing'); 	
	echo json_encode($result_array);	 
	die; 
}


$accesstoken=$result['accesstoken'];
$email=$result['email'];
 




  
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Email is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
		echo json_encode($result_array);	
		die;
	}
} 

$connection->delete('otp_temp_user', ['email = ?' => $email]);

$user_array=array('email'=>$email);
$result_array = array('success' =>1, 'result' => $user_array, 'error' => 'Email is valid');
echo json_encode($result_array);	 
die; 
  
