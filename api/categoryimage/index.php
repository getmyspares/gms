<?php
	// Get parent category details. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$parent_category_array = null;
	
	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	$site_url=$storeManager->getStore()->getBaseUrl();
	try
	{
		$parent_category_array = null;
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categories = $categoryHelper->getStoreCategories();
		
		$i =0 ;
		$_outputhelper = $objectManager->create('Magento\Catalog\Helper\Output');
		
		foreach ($categories as $category) { 
			 $category = $objectManager->create('Magento\Catalog\Model\Category')->load($category->getId());
			$_imgUrl = $category->getImageUrl();
			
			 if (!empty($_imgUrl)) {   
			
             $_imgHtml = $_outputhelper->categoryAttribute($category, $_imgUrl, 'image');
             $parent_category_array[$i]['image'] = $site_url.$_imgHtml;	 
            }
			else{
				$parent_category_array[$i]['image'] =$site_url.'Customimages/default_cat_img.png';
			}  
			
			$parent_category_array[$i]['id'] = $category->getId();
			$parent_category_array[$i]['name'] = $category->getName();
			
			
			
			//echo $category->getName() . '<br />';
			$i++; 
		}
		//print_r($parent_category_array);
		

		
		if(!empty($parent_category_array)){
			$result_array = array('success' => 1, 'result' => $parent_category_array, 'error' =>"Product assigned to this category."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $parent_category_array, 'error' => "There are no categrory assigned."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $parent_category_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>