<?php
	// Catgeory's product api. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');	
	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	$categoryId = 7;
	//$categoryId = 35;

	try 
	{	
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		
		$now = date('Y-m-d');
		
		$collection = $productCollection
		->addMinimalPrice()
		->addFinalPrice()
		->addTaxPercents()
		->addAttributeToSelect('special_from_date')
		->addAttributeToFilter('status',1)
		->addAttributeToFilter('part_type', '236')  
		->addAttributeToSelect('special_to_date')
		->addAttributeToFilter('special_price', ['neq' => ''])
		->addAttributeToFilter('is_saleable', 1, 'left')
		->addAttributeToSort('created_at', 'DESC')
		->setPageSize(3);   
		
		
		$collection->getSelect()->where('price_index.final_price < price_index.price');
		$collection->load();	
	
		$excitingDeals = $collection->getData();
		
		//$special_price=$api_helpers->check_product_special_price('18432');
		
		$today = time(); 
		$price_return=0; 	
		$finalArray=array();	
		foreach ($excitingDeals as $product) { 
			
			$product_id=$product['entity_id'];
			$finalArray[]=$product_id;
		 
		}  
		
		if(empty($finalArray))
		{
			$data=array();
			$result_array = array('success' => 1,'result' => $data,'block_title'=>'Accessories','error' => 'No product found');  
			echo json_encode($result_array);	 
			die; 
		}	 
		
		$product_arrayy=array_unique($finalArray, SORT_REGULAR);
		
		foreach($product_arrayy as $value){ 
			$data[] = $value;
		} 
		
		foreach($data as $row)
		{
			$product_id=$row;	 
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
		 
			if(!empty($product->getImage())){
				$imageUrl = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('300','300')->getUrl();
			}else{ 
				$site_url = $storeManager->getStore()->getBaseUrl();
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
			}  
		    
			//$imageUrl=$api_helpers->get_product_thumb($product_id);
			
			$sale_percentage='';
			$price=$product->getPrice();
			$specialprice=number_format($product->getSpecialPrice(), 2, '.', '');
			
			
			
			$specialfromdate = $product->getSpecialFromDate(); 
			$specialtodate = $product->getSpecialToDate();
			
			 
		
			$ratings ='';
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
			if(!empty($RatingOb->getCount())){
				$ratings = $RatingOb->getSum()/$RatingOb->getCount(); 
			}
			if(!empty($ratings)){
				$rating_count = $ratings;
			}else{
				$rating_count = null;
			}
			
			$sale=null;
			$sale_percentage=null;
			if($product->getSpecialPrice()!='')
			{	
				$sale = round((($price-$specialprice)/$price)*100); 
			} else 
			{
				$sale =0;
			}
			$sale_percentage = $sale.' %'.'Off';   

			$product_array[]=array(
				'id'=>$product->getId(),	
				'name'=>$product->getName(),	
				'sku'=>$product->getSku(),	
				'price'=>number_format($product->getPrice(), 2, '.', ''),
				'image'=>$imageUrl,
				'specialprice'=>$specialprice,
				'specialfromdate'=>$specialfromdate,
				'specialtodate'=>$specialtodate,
				'sale_percentage'=>$sale_percentage,
				'rating_count'=>$rating_count
			
			);
			 
			
		}	
		
		
		//print_r($product_array);
	
		$result_array = array('success' => 1, 'result' => $product_array,'block_title'=>'Accessories', 'error' =>"Accessories product details."); 
		echo json_encode($result_array);
		
	
	}catch(Exception $e){
		 
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data,'block_title'=>'Accessories', 'error' => $e->getMessage()); 

		echo json_encode($result_array);
	}
?>