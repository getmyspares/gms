<?php
/*
payload @ritesh
{ 
	"accesstoken": "1234",
	"rma_id": "243",
	"order_id": "5090", 
	"user_id":"12"
	"rma_type":"rma" /* rma-order */

require "../security/sanitize.php";
$result = sanitizedJsonPayload();
include('../../app/bootstrap.php');

use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');


if (!isset($result['accesstoken'])) {
	$result_array = array('success' => 0, 'error' => 'Access Token is missing');
	echo json_encode($result_array);
	die;
}

if (!isset($result['rma_type']) || empty($result['rma_type'])) {
	$result_array = array('success' => 0, 'error' => 'Invalid Payload');
	echo json_encode($result_array);
	die;
}

if ($result['rma_type'] == "rma") {
	$rma_type = "rma";
	if (!isset($result['rma_id']) && empty($result['rma_id'])) {
		$result_array = array('success' => 0, 'error' => 'Invalid Payload');
		echo json_encode($result_array);
		die;
	}
	if (!is_numeric(($result['rma_id']))) {
		$result_array = array('success' => 0, 'error' => 'Rma Id is invalid');
		echo json_encode($result_array);
		die;
	}
	$rma_id = $result['rma_id'];
}

if ($result['rma_type'] == "order") {
	$rma_type = "order";
	if (!isset($result['order_id']) && empty($result['order_id'])) {
		$result_array = array('success' => 0, 'error' => 'Invalid Payload');
		echo json_encode($result_array);
		die;
	}
	$order_id = $result['order_id'];
}

if (!isset($rma_type)) {
	$result_array = array('success' => 0, 'error' => 'Invalid Payload');
	echo json_encode($result_array);
	die;
}

if (!isset($result['user_id']) || empty(@get_numeric($result['user_id']))) {
	$result_array = array('success' => 0, 'error' => 'User Id is missing');
	echo json_encode($result_array);
	die;
}

$user_id = @get_numeric($result['user_id']);
if ($user_id == '') {
	$result_array = array('success' => 0, 'result' => '', 'error' => 'User Id is missing');
	echo json_encode($result_array);
	die;
} else {
	$check_user = $api_helpers->check_user_exists($user_id);
	if ($check_user == 0) {
		$result_array = array('success' => 0, 'result' => '', 'error' => 'User Id not exists');
		echo json_encode($result_array);
		die;
	}
	$authentication_response = $api_helpers->authenticateUser($user_id, $result['accesstoken']);
	if ($authentication_response['success'] == 0) {
		echo json_encode($authentication_response);
		die;
	}
}

$accesstoken = $result['accesstoken'];
$ResourceConnection = $objectManager->get('Magento\Framework\App\ResourceConnection');
$helper = $objectManager->create('Webkul\Rmasystem\Helper\Data');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
				$site_url = $storeManager->getStore()->getBaseUrl();
$connection = $ResourceConnection->getConnection();

if ($rma_type == "rma") {
	$query = "Select * from wk_rma where rma_id='$rma_id'";
}
if ($rma_type == "order") {
	$query = "Select * from wk_rma where order_id='$order_id'";
}
$wk_rma_collection = $connection->query($query);
$wk_rma = $wk_rma_collection->fetchAll();



if (!empty($wk_rma)) {
	$rma_array = array();
	foreach ($wk_rma as $value) {
		$increment_id = $value['increment_id'];
		$order_id = $value['order_id'];
		$rma_id = $value['rma_id'];
		$package_condition = $value['package_condition'] == 1 ? 'packed' : 'open';
		$created_at = $value['created_at'];
		$consignment_number = $value['customer_consignment_no'];
		$admin_consignment_no = $value['admin_consignment_no'];
		$additional_info = $value['additional_info'];
		$seller_remark = $value['seller_remark'];
		$status = $value['status'];
		$acknowledge_date = $value['acknowledge_date'];
		$decline_date = $value['declined_date'];
		$approved_date = $value['approved_date'];
		$additopnal_img_array=[];
		$image = $value['image'];
		
		$subDir = 'webkul/rmasystem/RMA';

		$fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
		
		$BaseDirRead = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath($subDir.'/');
		$folderName = $BaseDirRead.$rma_id.'/image/';
		$additopnal_ims_date = glob($folderName.'*.{jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG,bmp,BMP}', GLOB_BRACE);

		foreach ($additopnal_ims_date as $filename) {
			$getfilename = explode("/",$filename);	
			$justfname = $site_url."pub/media/webkul/rmasystem/RMA/$rma_id/image/".end($getfilename);
			$additopnal_img_array[]=$justfname;
		}	

		$return_status = $helper->getRmaStatusTitle($value['status'], $value['final_status']);
		$customer_delivery_status = $helper->getRmaOrderStatusTitle($value['customer_delivery_status']);
		if ($value['resolution_type'] == 0) {
			$resolution_type = "Refund";
		} elseif ($value['resolution_type'] == 1) {
			$resolution_type = "Exchange";
		} else {
			$resolution_type = "Cancel Item";
		}
		$query = "Select * from wk_rma_items where rma_id='$rma_id'";
		$wk_rma_items_collection = $connection->query($query);
		$wk_rma_items = $wk_rma_items_collection->fetchAll();
		foreach ($wk_rma_items as  $item) {
			$item_id = $item['item_id'];
			$query = "Select product_id from sales_order_item where item_id='$item_id'";
			$product_id = $connection->fetchOne($query);
			
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
			if(!empty($product->getImage()))
			{
				$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
				$p_image = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
			} 
			else
			{
				
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
				$p_image = $imageUrl;
			} 
			$query = "Select * from sales_order_item where item_id='$item_id'";
			$sales_order_item_collecrtion = $connection->query($query);
			$sales_order_item = $sales_order_item_collecrtion->fetchAll()[0];

			$items[] = array(
				'product_name' => $sales_order_item['name'],
				'sku' => $sales_order_item['sku'],
				'return_quantity' => $item['qty'],
				'reason' => $item['rma_reason'],
				'product_image' => $p_image,
				'product_price' => $sales_order_item['price']
			);
		}

		$track_detail=array();
		$query = "Select created_at from sales_order where entity_id='$order_id'";
		$order_created_date = $connection->fetchOne($query);
		
		$query = "SELECT delivery_date FROM `om_ecom_api_status` where order_id='$order_id'";
		$delivery_date = $connection->fetchOne($query);

		$query = "SELECT pickup_date FROM `ecomexpress_awb_reverse` where rma_id='$rma_id'";
		$reverse_pickup_date = $connection->fetchOne($query);


		$track_detail[] = array('id'=>'1','title'=>'Ordered','date'=>$order_created_date,'info'=>'Items Ordered');
		$current_status='2';
		$track_detail[] = array('id'=>'2','title'=>'Delivered','date'=>$delivery_date,'info'=>'Item Delivered');
		$current_status='3';
		$track_detail[] = array('id'=>'3','title'=>'Return','date'=>$created_at,'info'=>'Return Requested');
		
		if($status=='3')
		{/* rma declined */
			$track_detail[] = array('id'=>'7','title'=>'Declined','date'=>$decline_date,'info'=>'Return/Exchange Delined');
			$current_status='4';
		}	else 
		{/* rma is accepted */
			
			if(!empty($acknowledge_date))
			{
				$current_status='4';
			}
			if(!empty($reverse_pickup_date))
			{
				$current_status='5';
			}
			if(!empty($approved_date))
			{
				$current_status='6';
			}
			$track_detail[] = array('id'=>'4','title'=>'Return Accepted','date'=>$acknowledge_date,'info'=>'Return Acknowledge');
			$track_detail[] = array('id'=>'5','title'=>'Pickup','date'=>$reverse_pickup_date,'info'=>'Reverse Shipment Picked');
			$track_detail[] = array('id'=>'6','title'=>'Refund','date'=>$approved_date,'info'=>'Refund Generated');
		}

		$rma_array[] = ['rma_id' => $rma_id,'order_increment_id'=>$increment_id, 'package_condition' => $package_condition, 'created_at' => $created_at, 'consignment_number' => $consignment_number, 'additional_info' => $additional_info, 'seller_remark' => $seller_remark,'trackDetail'=>$track_detail, 'additional_images' => $additopnal_img_array, 'return_status' => $return_status,'current_status'=>$current_status,'product_delivery_status' => $customer_delivery_status, 'resolution_type' => $resolution_type, 'items' => $items];

	}
	$result_array = array('success' => 1, 'error' => '', 'result' => $rma_array);
	echo json_encode($result_array);
	die;
} else {
	$result_array = array('success' => 1, 'error' => 'Rma not found', 'result' => '');
	echo json_encode($result_array);
	die;
}
