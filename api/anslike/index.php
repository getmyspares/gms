<?php
require "../security/sanitize.php";

$result = sanitizedJsonPayload();
if(!is_array($result) || (is_array($result) && empty($result))) {
	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
	die;
}


//$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result);
$user_array=null; 

if($argumentsCount < 2 || $argumentsCount > 4) 
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'user Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['ans_id']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Answer Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['like_dislike']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Like/dislike is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
 

  

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);
$ans_id=$result['ans_id'];
$like_dislike=$result['like_dislike'];



$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
	
}
	


$check=$api_helpers->check_current_user_like_answer($ans_id,$user_id);	
if($check!=null)
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'You already attempt like/dislike on this answer'); 	
	echo json_encode($result_array);	
	die;	
}
else
{
	$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$data = array(
		"answer_id" => "$ans_id",
		"review_from" => "$user_id",
		"like_dislike" => "$like_dislike"
	);
	$connection->insert('wk_qarespondreview', $data);

	// $sql="INSERT INTO `wk_qarespondreview`(`answer_id`, `review_from`, `like_dislike`) VALUES ('".$ans_id."','".$user_id."','".$like_dislike."')";
	// $connection->query($sql);  
	
	
	$user_array=array('user_id'=>$user_id,'like_dislike'=>$like_dislike);
	
	$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Your attempt is added');  	
	echo json_encode($result_array);	
	die;			
	
	
}	
  


?>