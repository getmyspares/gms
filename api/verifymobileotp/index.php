<?php
/*
FOR Company Buyer json format
{
	"accesstoken": "1234",
	"mobile": "1234567890"
}
*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result); 
//die;
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
      
$user_array=array();	
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['customer_id']) || empty(@get_numeric($result['customer_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Email  Id cannot be blank'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['otp']) || empty(@get_numeric($result['otp'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Otp cannot be blank'); 	
	echo json_encode($result_array);	
	die; 
}

$accesstoken=$result['accesstoken'];
$customer_id=@get_numeric($result['customer_id']); 
$otp=@get_numeric($result['otp']); 
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

$today=date('Y-m-d');

$sql="select * from otp where `customer_id`='$customer_id' and is_used=0 and date(cr_date)='$today' and  pin_code='$otp'"; 
$all_date = $connection->fetchAll($sql);

if(!empty($all_date)) 
{
	$pn_saved = $all_date[0]['pin_code'];
	if($pn_saved==$otp)
	{
		$otp_id = $all_date[0]['id'];
		$sql="update `otp` set `is_used`='1' where id='$otp_id'";	
		$connection->query($sql);
		$result_array = array('success' => 1, 'result' =>'','error' => 'Otp Verified'); 	
		echo json_encode($result_array);	
		die;    
	} else  
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Your OTP mismatch/expired'); 	
		echo json_encode($result_array);	
		die;
	}
}
else
{  
	$attempt=0; 
	$user_array=array('attempt'=>$attempt);
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Your OTP mismatch/expired'); 	
	echo json_encode($result_array);	
	die;    
}	  