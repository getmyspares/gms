<?php
require "../security/sanitize.php";

//$result = sanitizedJsonPayload();
//print_r($result);

$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result); 
$user_array=null; 
 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
$store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getStore();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

if($argumentsCount < 3 || $argumentsCount > 3)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}  

 
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['order_id']) || empty(@get_numeric($result['order_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order Id is missing'); 	
	echo json_encode($result_array);	  
	die; 
} 

if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	  
	die;  
} 

$accesstoken=$result['accesstoken'];
$order_id=$result['order_id'];
$user_id=@get_numeric($result['user_id']);

$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
if($authentication_response['success']==0)
{
	echo json_encode($authentication_response);	
	die;
}      
 

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

 
//$api_helpers->get_mobile_order();       

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

	
if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check=$api_helpers->check_user_exists($user_id);
	if($check==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;
	}

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}


}	


$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');	
$site_url=$storeManager->getStore()->getBaseUrl();

$customer_array=$api_helpers->get_user_details($user_id);
//print_r($customer_array);

$firstname=$customer_array[0]['firstname'];
$lastname=$customer_array[0]['lastname'];
$email=$customer_array[0]['email'];


$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id); 
$order_inc_id=$order->getIncrementId();

$order_status=$api_helpers->api_get_order_tracking($order_inc_id);
$order_status=strtolower($order_status);

$ordder_db_status = $order->getState();

if($ordder_db_status!="complete")
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Your Order is not complete');  	
	echo json_encode($result_array);	 
	die;	
}	


$order_inc_ids=$order->getIncrementId();
$order_inc_id=base64_encode($order_inc_id);
$link='<a href="'.$site_url.'invoice.php?id='.$order_inc_id.'" target="_blank">Click Here</a>';
$name=$firstname.' '.$lastname;
$message='
<p>Hello '.$name.'</p>
<p>'.$link.' to download your Invoice for order # '.$order_inc_ids.'</p>
';
$subject="GetmySpares - Download Invoice"; 

/* mail functionality replcaed with dynamic sendgrid  mail  sending api @ritesh19032021*/
$to = $email;
// $to = 'malemeemail@gmail.com';
$from = 'support@getmyspares.com';
$msg = $message;
$subject=$subject;
$helpers->sendEmailThroughCurl($to,$from,$msg,$subject);

$user_array=array('user_id'=>$user_id);
$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Invoice sent to registered email');  	
echo json_encode($result_array);	
die; 
 

?>



