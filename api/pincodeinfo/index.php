<?php
	// Homepage banner api.
	
	//	Url Example :- https://www.store.idsil.com/api/pincodeinfo/?accesstoken=1234&pincode=143001
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	//print_r($result);
	
	
	$banner_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $banner_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){ 
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if(!empty($result['pincode'])){ 
		$pincode = $result['pincode'];
	}else{
		$pincode = '';	
	}
	
	if($pincode=='')
	{
		$result_array=array('success' => 0, 'result' => $banner_array, 'error' => 'Pincode is missing'); 	
		echo json_encode($result_array);	 
		die;
	}
	 
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $banner_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $banner_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		
		$select = $connection->select()
                  ->from('pincode_api') 
                  ->where('pincode = ?', $pincode);
                 
		$result = $connection->fetchAll($select);
		if(!empty($result))
		{
			$city=$result[0]['city'];	
			$state=$result[0]['state'];	
			if($state=="New Delhi")
			{
				$state="Delhi";
			}		 
			$banner_array=array('city'=>$city,'state'=>$state);
			$result_array = array('success' => 1, 'result' => $banner_array, 'error' => 'Pincode Info');
			echo json_encode($result_array);
			die;
		}
		else
		{
			$result_array=array('success' => 0, 'result' =>$banner_array, 'error' => 'No pincode found'); 	
			echo json_encode($result_array);
			die;
		}
		//print_r($result);
		
		/*
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
		
		$result_array=array('success' => 0, 'result' =>$banner_array, 'error' => $e->getMessage()); 	
		*/ 
        
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$banner_array, 'error' => $e->getMessage()); 	
	} 
	  
	echo json_encode($result_array);
?>