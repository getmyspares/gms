<?php
	require "../security/sanitize.php";
?>

<?php


	/*
	Add Delivery Address for the customer
	
	
	{
		"accesstoken": "1234",
		"user_id": "455",
		"city": "Chandigarh",
		"firstname": "Ramit",
		"lastname": "last",
		"postcode": "160023",
		"telephone": "7894563214",
		"region": "Chandigarh",
		"region_id": "538",
		"street_1": "2121",
		"street_2": "Sector 23",
		"landmark": "near temple",
		"set_deault_shipping_adddress": "0",
		"set_deault_billing_adddress": "1",
		"address_type": "1"	
	}
	 
	// 1 Home Address
	// 2 Office Address
	 
	
	*/
    // SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
	
$result = sanitizedJsonPayload();
// if(!is_array($result) || (is_array($result) && empty($result))) {
// 	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
// 	die;
// }

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$argumentsCount=count($result);

if($argumentsCount < 15 || $argumentsCount > 15) 
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);
$city=$result['city'];
$firstname=$result['firstname'];
$lastname=$result['lastname'];
$postcode=@get_numeric($result['postcode']);
$telephone=@get_numeric($result['telephone']);
$region=$result['region'];
$region_id=$result['region_id'];
$street_1=$result['street_1'];
$street_2=$result['street_2'];
$landmark=$result['landmark'];
$address_type=$result['address_type'];
$set_deault_shipping_adddress=$result['set_default_shipping_address'];
$set_deault_billing_adddress=$result['set_default_billing_address'];

$user_array=null; 

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
	$CustomerModel->setWebsiteId(1); 
	$CustomerModel->load($user_id); 
	$user_email = $CustomerModel->getEmail();
	if($user_email=='')
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'No User Found'); 	
		echo json_encode($result_array);	 
		die;
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
} 


if($city=='')
{
	$result_array=array('success' => 0, 'error' => 'City is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($firstname=='')
{
	$result_array=array('success' => 0, 'error' => 'Firstname is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($lastname=='')
{
	$result_array=array('success' => 0, 'error' => 'Lastname is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($postcode=='')
{
	$result_array=array('success' => 0, 'error' => 'Pincode is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($telephone=='')
{
	$result_array=array('success' => 0, 'error' => 'Telephone is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($region=='')
{
	$result_array=array('success' => 0, 'error' => 'Region is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($region_id=='')
{
	$result_array=array('success' => 0, 'error' => 'Region Id is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($street_1=='')
{
	$result_array=array('success' => 0, 'error' => 'House no. is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($street_2=='')
{
	$result_array=array('success' => 0, 'error' => 'Road name is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($address_type=='')
{
	$result_array=array('success' => 0, 'error' => 'Address Type is missing'); 	
	echo json_encode($result_array);	
	die;
}



try
{ 
	$today=date('Y-m-d h:i:s');  
	$user_id=@get_numeric($result['user_id']);
	$city=$result['city'];
	$firstname=$result['firstname'];
	$lastname=$result['lastname'];
	$postcode=@get_numeric($result['postcode']);
	$telephone=@get_numeric($result['telephone']);
	$region=$result['region'];
	$region_id=$result['region_id'];
	$street_1=$result['street_1'];
	$street_2=$result['street_2'];
	$landmark=$result['landmark'];
	$address_type=$result['address_type']; 
	
/* this is important , please do not format , this is done to insert linebreak in adresses in database @ritesh */
// $address=$street_1.'
// '.$street_2.'
// '.$landmark;

	$address=$street_1.' ,'.$street_2.' ,'.$landmark;

	/*$allowed=$api_helpers->shipping_allowed_on_pincode($postcode); 
	if($allowed==0) 
	{ 
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Shipping is not allowed on selected pincode'); 	
		echo json_encode($result_array);	 
		die; 	
	}*/ 

	$data = array(
				"parent_id"=>"$user_id",
				"created_at"=>"$today",
				"updated_at"=>"$today",
				"is_active"=>"1",
				"city"=>"$city",
				"country_id"=>"IN",
				"firstname"=>"$firstname",
				"lastname"=>"$lastname",
				"postcode"=>"$postcode",
				"region"=>"$region",
				"region_id"=>"$region_id",
				"telephone"=>"$telephone",
				"street"=>"$address",
			);

	$set_deault_shipping_adddress = $result['set_default_shipping_address'];
	$set_deault_billing_adddress = $result['set_default_shipping_address'];
	

	$connection->insert('customer_address_entity', $data);
	
	$query = $connection->select()
						->from(['cae' => 'customer_address_entity'])
						->where('cae.parent_id=?', $user_id)
						->order('cae.entity_id DESC')
						->limit(1);
	$result = $connection->fetchAll($query);
	$address_id=$result[0]['entity_id']; 

	if($set_deault_shipping_adddress==1)
	{
		$address = $objectManager->create('\Magento\Customer\Model\Address')->load($address_id);
		$address->setIsDefaultShipping('1');
		$address->save();
	}
	if($set_deault_billing_adddress==1)
	{
		$address = $objectManager->create('\Magento\Customer\Model\Address')->load($address_id);
		$address->setIsDefaultBilling('1');
		$address->save();
	}		
	
	
	
	$data = array(
				"address_id"=>"$address_id",
				"address_type"=>"$address_type"
			);
	$connection->insert('customer_address_type', $data);

	$query = $connection->select()
						->from(['cae' => 'customer_address_entity'])
						->where('cae.parent_id=?', $user_id);
	$result = $connection->fetchAll($query);

	$total_address=count($result); 
	
	if($total_address==1)
	{
		$data = [
					'default_billing' => $address_id,
					'default_shipping' => $address_id
				];
		$where = ['entity_id = ?' => $user_id];
		$connection->update('customer_entity', $data, $where);
	}	 
	
	
	$noDeliveryMsg = '';
	$deliverable   = true;
	$pincode = $connection->fetchOne("SELECT postcode FROM `customer_address_entity` WHERE `entity_id` = '$address_id'");
	if($pincode){
		$rdata = $connection->fetchRow("SELECT is_active,msg FROM `pincode_details` WHERE `pincode` = '$pincode'");
		if($rdata['is_active']==0){
			$deliverable   = false;
			$noDeliveryMsg = 'Due to covid restriction, we are not shipping to the location selected by you.Please chose a different location.';
			if($rdata['msg']!=null && trim($rdata['msg'])!=''){
				$noDeliveryMsg = $rdata['msg'];				
			}
		}
	}
	
	$user_array=array('address_id'=>$address_id,'total_address'=>$total_address,'user_id'=>$user_id,'deliverable'=>$deliverable,'message'=>$noDeliveryMsg);    
	 
	$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Address has been added successfully'); 	
	echo json_encode($result_array);	
	die;     
	
}
catch(Exception $e) 
{
	echo $e->getMessage();
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Account not confirmed or Password not matched'); 	
	echo json_encode($result_array);	
	die;
} 


  


?>

	
