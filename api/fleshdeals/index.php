<?php
	// Excitingdeals api.
	//	Url Example :- https://dev.idsil.com/design/api/fleshdeals/?accesstoken=1234
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	// print_r($result);
	
	$block_data_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $block_data_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $block_data_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $block_data_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		
		
		$blocks = $objectManager->get('Magento\Cms\Model\Block')->load(2576);
		$blockArray=$blocks->getData();
		$block_data=$blockArray['content'];		
		//$content=$blockArray['content'];		
		//print_r($blockArray);
		
		if($blockArray['is_active']==0)
		{
			$result_array=array('success' => 0, 'result' =>$block_data_array,'block_title'=>'Fresh Deals', 'error' => 'Block is not active');
			echo json_encode($result_array); 
			die;
		}	
		
		preg_match_all('~<img(.*?)src="([^"]+)"(.*?)>~', $block_data, $image);
		$img_name=$image[2];
		$img_name=str_replace('{{media url=&quot;','',$img_name);
		$img_name=str_replace('&quot;}}','',$img_name);
		$image_val = implode( ", ", $img_name );
		$img_array = explode(",",$image_val );
		
		
		preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $block_data, $matches);
		$href_val = implode( ", ", $matches[2] );
		$href_array = explode(",",$href_val);
		// print_r($href_array);
		
		
		
		preg_match_all('/<a .*?>(.*?)<\/a>/',$block_data,$content);
		//preg_match_all('~>\K[^<>]*(?=<)~', $block_data, $content);
		// print_r($content[1]);
		
		
		$path=$api_helpers->get_path();
		
		$path=$path.'/pub/media/';
		
		
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$site_url=$storeManager->getStore()->getBaseUrl();
		$site_urls=$storeManager->getStore()->getBaseUrl();
		$site_url=$site_url.'pub/media/';
		
		
		$i=0; 
		foreach( $href_array as $value){
			$text = preg_replace("/<img[^>]+\>/i", "", $content[1][$i]); 
			$key= strip_tags($text);
			$block_data_array[$key]['title']= trim($key);
			//$block_data_array[$key]['href']= trim($value);
			$block_data_array[$key]['href']= null;
			
			$image=trim($img_array[$i]);
			
			$ser_path=$path.$image;
			
			
			if(file_exists($ser_path))
			{
				$block_data_array[$key]['image']= $site_url.$image;
			}
			else
			{
				$block_data_array[$key]['image']=$site_urls.'Customimages/fresh-soon.png';
			}
			
			
			
			
			
			//$block_data_array[$key]['image']= trim($img_array[$i]);
			
			$url=trim($value);  
			$url_details = $custom_helpers->get_url_type($url);
			$url_details_array = json_decode($url_details,true);
			// print_r($url_details_array);
			if($url_details_array['success']==1){
				$block_data_array[$key]['url_type']=  $url_details_array['entity_type'];
				//$block_data_array[$key]['url_id']= $url_details_array['entity_id']; 
				$block_data_array[$key]['url_id']= 3; 
			}else{ 
				$block_data_array[$key]['url_type']= 'category';
				$block_data_array[$key]['url_id']= 3;  
			
			}
			
			// $block_data_array[$key]['url_type']= 'category';
			// $block_data_array[$key]['url_id']= 4; 
			$i++;			
		}
		// print_r($block_data_array);
		foreach($block_data_array as $value){
			$data[] = $value;
		}

		
		if(!empty($block_data_array)){
			$result_array=array('success' => 1, 'result' =>$data,'block_title'=>'Fresh Deals', 'error' => 'Fresh deal details.');
		}else{
			$result_array=array('success' => 0, 'result' =>$block_data_array,'block_title'=>'Fresh Deals', 'error' => 'There is no data right now.Please check after sometime.');
		}
		
	}
	catch(Exception $e)  
	{
		$result_array=array('success' => 0, 'result' =>$block_data_array,'block_title'=>'Fresh Deals', 'error' => $e->getMessage()); 	
	} 
	 
	echo json_encode($result_array);
?>