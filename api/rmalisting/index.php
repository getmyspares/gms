<?php
/*
payload @ritesh
{ 
	"accesstoken": "1234",
	"user_id": "479",
}

*/   
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');
 
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!is_numeric ((@get_numeric($result['user_id']))))
{
	$result_array=array('success' => 0, 'error' => 'User Id is invalid'); 	
	echo json_encode($result_array);	 
	die; 
}
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
$authentication_response = $api_helpers->authenticateUser(@get_numeric($result['user_id']),$result['accesstoken']);
if($authentication_response['success']==0)
{
	echo json_encode($authentication_response);	
	die;
}

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']); 






$ResourceConnection = $objectManager->get('Magento\Framework\App\ResourceConnection');
$helper = $objectManager->create('Webkul\Rmasystem\Helper\Data');
$connection = $ResourceConnection->getConnection();

$query="Select * from wk_rma where customer_id='$user_id' order by rma_id desc";
$wk_rma_collection = $connection->query($query);
$wk_rma = $wk_rma_collection->fetchAll();

if(!empty($wk_rma))
{
	foreach ($wk_rma as $value) {
		$rma_id = $value['rma_id'];
		$increment_id = $value['increment_id'];
		$return_status = $helper->getRmaStatusTitle($value['status'], $value['final_status']);		
		$created_at = $value['created_at'];
		$rma_array[] = ['rma_id'=>$rma_id,'increment_id'=>$increment_id,'return_status'=>$return_status,'created_at'=>$created_at];
	}
	$result_array=array('success' => 1,'error'=>"", 'result' =>$rma_array);
	echo json_encode($result_array);
	die; 
} else 
{
	$result_array=array('success' => 1,'error'=>"No Rma has been filed yet", 'result' => ''); 	
	echo json_encode($result_array);	 
	die; 
}




?>



