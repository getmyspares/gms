<?php


/*
{ 
	"accesstoken": "1234",
	"order_id": "578"
	"user_id": "578"
}
*/  
  
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);

//$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result); 
$user_array=null; 
 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
$store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getStore();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

if($argumentsCount != 3)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}  
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['order_id']) || empty(@get_numeric($result['order_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order Id is missing'); 	
	echo json_encode($result_array);	  
	die; 
}

if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$authentication_response = $api_helpers->authenticateUser(@get_numeric($result['user_id']),$result['accesstoken']);
if($authentication_response['success']==0)
{
	echo json_encode($authentication_response);	
	die;
}

$authentication_response = $api_helpers->authenticateOrder(@get_numeric($result['user_id']),$result['order_id']);
if($authentication_response['success']==0)
{
	echo json_encode($authentication_response);	
	die;
}
  
   
$accesstoken=$result['accesstoken'];
$order_id=$result['order_id']; 

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
       

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

	 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');

$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);

if(empty($order->getAllItems()))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order item not  found'); 	
	echo json_encode($result_array);	  
	die; 
}

$productarray=array();
foreach ($order->getAllItems() as $item)
{
	$item_id=$item->getId();
	$item_ordered_quantity=$item->getQtyOrdered();
	$total_remainig_quantity = $item_ordered_quantity;
	$product_id = $item->getProductId();
	$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
	$image=''; 
	if(!empty($product->getImage())) 
	{ 
		$image = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
	}
	else{  
		$site_url = $storeManager->getStore()->getBaseUrl();
		$imageUrl = $site_url.'Customimages/product-thumb.jpg';
		$image = $imageUrl; 
	} 

	$select = $connection->select() 
		  ->from('wk_rma_items') 
		  ->where('order_id = ?', $order_id)
		  ->where('item_id = ?', $item_id); 
	$result = $connection->fetchAll($select);
	if(!empty($result))
	{
		foreach($result as $row)
		{
			$item__already_qty=$row['qty'];	
			$total_remainig_quantity = $item_ordered_quantity-$item__already_qty;
			
		}		
	}	
	if($total_remainig_quantity>0)
	{
		$productarray[] = array(
			'item_id'=>$item_id,
			'name'=>$item->getName(),
			'sku'=>$item->getsku(),
			'image'=>$image,
			'ordered_quantiy'=>$item_ordered_quantity,
			'quantity_available_for_refunded'=> $total_remainig_quantity,   
		);
	}
}

$select = $connection->select()
		  ->from('wk_rma_reason')  
		  ->where('status = ?', '1');
$reasons = $connection->fetchAll($select);

foreach($reasons as $row)
{
	$reasonsarray[]=array(
		'id'=>$row['id'],
		'name'=>$row['reason'],
		'status'=>$row['status'],
	);	
}	

$resolution=array(
	'0'=>array(
		'id'=>0,
		'name'=>'Refund'
	),
	'1'=>array(
		'id'=>1,
		'name'=>'Exchange'
	)
);

$package_condition=array(
	'0'=>array(
		'id'=>0,
		'name'=>'Open'
	),
	'1'=>array(
		'id'=>1,
		'name'=>'Packed' 
	)
);

$user_array=array('product'=>$productarray,'reasons'=>$reasonsarray,'resolution'=>$resolution,'package_condition'=>$package_condition); 
$result_array=array('success' => 1, 'result' => $user_array, 'error' => 'Rma Page Found');	 
echo json_encode($result_array);	 
die;

?>



