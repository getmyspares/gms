<?php
 
 

/*
FOR Company Buyer json format

{
	"accesstoken": "1234",
	"mobile": "1234567890"
}
  


*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result); 
//die;
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
      
$user_array=array();
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['customer_id']) || empty(@get_numeric($result['customer_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Email  Id cannot be blank'); 	
	echo json_encode($result_array);	
	die; 
}


$accesstoken=$result['accesstoken'];
$customer_id=@get_numeric($result['customer_id']); 
   
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

$sql="select `value` from `customer_entity_varchar` where attribute_id='178' and entity_id='$customer_id' ";
$mobile_number = $connection->fetchOne($sql);

if(!empty($mobile_number)) 
{
	
	$sms_helpers = $objectManager->create('OM\MobileOtp\Helper\SmsHelper'); 
	$pin = mt_rand(1000, 9999);
	$sms_helpers->send_sms($mobile_number,$pin);
	$sql="insert into otp  set customer_id='$customer_id' ,mobile='$mobile_number', pin_code='$pin'"; 
	$connection->query($sql);	
	$result_array=array('success' => 1, 'result' =>'','error' => 'Your OTP send'); 	
	echo json_encode($result_array);	
	die;    
	
}
else
{  
	$attempt=0; 
	$user_array=array('attempt'=>$attempt);
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Your OTP attempt is reached to maximum'); 	
	echo json_encode($result_array);	
	die;    
}	  


	
