<?php
require "../security/sanitize.php";
/* 

{"accesstoken": "1234","mobile":"1234567890","otp_pin":"1111"} 

*/

$result = sanitizedJsonPayload();
if(!is_array($result) || (is_array($result) && empty($result))) {
	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
	die;
}

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$state = $objectManager->get('\Magento\Framework\App\State');
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

$connection = $resource->getConnection();

$argumentsCount=0;


$group_array=array('1','2','5');
$null = null;
if(!empty($result)){
	$argumentsCount=count($result);
}
$user_array=null;
	
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['mobile']) || empty(@get_numeric($result['mobile'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['otp_pin']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	 
	die;  
}
if(!isset($result['deviceid']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	 
	die;  
}
$device_id = $result['deviceid'];
$accesstoken=$result['accesstoken'];
$mobile=@get_numeric($result['mobile']);
$otp_pin=$result['otp_pin'];
$str_len=strlen($otp_pin);   
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Invalid Payload');	
		echo json_encode($result_array);	
		die;
	}
}

if($mobile=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	
	die;
}
else  
{
	$check_mobile=$app_helpers->check_mobile_exists($mobile);
	if($check_mobile==1)
	{	
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Invalid Payload'); 	
		echo json_encode($result_array);	
		die;  
	} 
}

if($str_len!=4)
{
	
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Invalid Payload'); 	
	echo json_encode($result_array);	
	die;	
}	

$select = $connection->select()
		  ->from('otp_temp_user') 
		  ->where('mobile = ?', $mobile)
		  ->where('pin_code = ?', $otp_pin);
$results = $connection->fetchAll($select);

if(!$results)
{

	$result_array=array('success' => 0, 'result' => $user_array,'error' => "OTP Pin not matched"); 	
	echo json_encode($result_array);	
	die;	
}	
else
{

	$state->setAreaCode('frontend');
	
	$email=$results[0]['email'];
	$password=base64_decode($results[0]['password']);
	$mobile=$results[0]['mobile'];
	$groupid=$results[0]['group_id'];   

	$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');
	$websiteId = $storeManager->getWebsite()->getWebsiteId();
	$store = $storeManager->getStore(); 
	$storeId = $store->getStoreId();
	$customer = $customerFactory->create(); 
	$customer->setWebsiteId($websiteId);
	$customer->setEmail($email);
	$customer->setMobile($mobile); 
	$customer->setPassword($password);
	$customer->setFirstname('firstname');
	$customer->setLastname('lastname');     
	$customer->save();  
	$custID=$customer->getId();  
	$api_helpers->store_user_token($custID);    
	$customer = $objectManager->get('Magento\Customer\Model\Customer')->load($custID);

	// $customerSession = $objectManager->create('Magento\Customer\Model\Session');
	// $customerSession->setCustomerAsLoggedIn($customer);	   
	
	$group_id=$groupid;
	
	if($group_id==4) 
	{
		$customer->sendNewAccountEmail('confirmation', '', $customer->getStoreId());
	}  
	
	$data = [
			'group_id' => $group_id
		];
	$where = ['entity_id = ?' => $custID];

	$connection->update('customer_entity', $data, $where);
	$connection->update('customer_grid_flat', $data, $where);
	$connection->update('customer_entity', ['confirmation' => NULL], $where);
	$connection->update('customer_grid_flat', ['confirmation' => NULL], $where);
	$connection->delete('otp_temp_user',['email = ?' => $email]);
	        	    
	$helpers->individual_welcome('Firstname','Lastname',$email) ;     
	
	/* buyer audit report not needed  at  this  point of  time */
	// $buyer_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\buyer');
	// $buyer_helpers->create_buyer_report_updated($custID); 
	
	$token = rand();
	$user_array=array('user_id'=>$custID,'email'=>$email,'accesstoken'=>$token);  
	$status = $api_helpers->registerToken($custID,$device_id,$token);
		
	$result_array=array('success' => 1, 'result' => $user_array,'error' => 'OTP matched Successfully'); 	
	echo json_encode($result_array);
	die();


}	
