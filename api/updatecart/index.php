<?php
	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/add_to_cart_db/
	//Input 
	///*{"accesstoken": "1234","product_id":"1399","user_id":"269","qty":"2"}*/
	require "../security/sanitize.php";
	include('../../app/bootstrap.php'); 
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	
	$result = sanitizedJsonPayload();
	// print_r($result);
	// die();
	 
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	
	$response = null; 
	$argumentsCount=count($result);
	$user_array=null;
	if($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	
	if(!empty(@get_numeric($result['qty']))){
		$qty = @get_numeric($result['qty']);
	}else{
		$qty = '';	
	}
	
	
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
		
		
	}
	
	
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$product_id = @get_numeric($result['product_id']);
		$check_product=$api_helpers->check_product_exist($product_id); 
		if($check_product==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
			echo json_encode($result_array);	
			die; 
		}
	}
	
	if($qty ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Quantity is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		if(!is_numeric($qty))
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Quantity must have numbers'); 	
			echo json_encode($result_array);	
			die;
		}
		else if($qty==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Quantity 0 not allowed'); 	
			echo json_encode($result_array);	
			die;
		}	 
	}
	
	try{
		
		$user_array=array('user_id'=>$user_id); 
		$check=$api_helpers->addtocart($user_id,$product_id,$qty);
		
		if($check=='out')
		{
			$result_array = array('success' => 0,'result' => $user_array,'error' =>"Product out of stock"); 
			echo json_encode($result_array);	
			die; 
		}
		else if($check=='qty')
		{
			$result_array = array('success' => 0,'result' => $user_array,'error' =>"Requested Qty is greater"); 
			echo json_encode($result_array);	
			die; 
		}	
		else
		{	
			$result_array = array('success' => 1,'result' => $user_array,'error' =>"Cart Updated"); 
			echo json_encode($result_array); 
		}
		  
		
		die();
		

	}catch(Exception $e){		

		$result_array = array('success' => 0,'result' => null, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
	
?>