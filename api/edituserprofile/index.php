<?php
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);

/*
{
	"accesstoken": "1234",
	"user_id":"317",
	"firstname":"company",
	"lastname":"company",
	"email":"companyuser@user.com",
	"mobile":"321654987656",
	"gst":"29ABCDE1234F2Z5",
	"company_name":"company name",
	"company_address":"address",
	"pan":"ABCDS1234Y",
	"change_mobile":"0",
	"change_email":"0", 
	"change_company":"0"
	
}

*/


//$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$argumentsCount=count($result);
$user_array=null; 

/* if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
} */	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'user Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['firstname']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Firstname is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['lastname']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Lastname is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Email is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['mobile']) || empty(@get_numeric($result['mobile'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Mobile is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
 

  

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);
$firstname=$result['firstname'];
$lastname=$result['lastname'];
$email=$result['email'];
$mobile=@get_numeric($result['mobile']);

$change_mobile=$result['change_mobile'];
$change_email=$result['change_email'];
$change_company=$result['change_company'];

 


$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}


if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Email is missing'); 	
	echo json_encode($result_array);	
	die;
} 
else
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
		echo json_encode($result_array);	
		die;
	}
} 
	


if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 
	else
	{   

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
		
		$array=$api_helpers->get_user_details($user_id);	
		$group_id=$array[0]['group_id'];
	}
	  
}

if($firstname=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'First name is missing'); 	
	echo json_encode($result_array);	 
	die;
}

if($lastname=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Last name is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if($mobile=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Mobile is missing'); 	
	echo json_encode($result_array);	 
	die;
}

if($change_mobile=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Change mobile  is missing'); 	
	echo json_encode($result_array);	 
	die;
}



 


if($group_id==4)
{
	$gst=$result['gst'];
	$company_name=$result['company_name'];
	$company_address=$result['company_address'];
	$pan=$result['pan'];
	
	
	if($gst=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'GST is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	if($company_name=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Company name is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	if($company_address=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Company address is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	if($pan=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'PAN is missing'); 	
		echo json_encode($result_array);	 
		die;
	}
		
	
}	

 

$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');

		
if($change_company==0) 
{
	
	if($change_mobile==1)
	{
		$check_mobile=$custom_helpers->check_customer_mobile($mobile); 
		if($check_mobile==1)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Mobile Number already registered'); 	
			echo json_encode($result_array);	 
			die; 
		}	
		else
		{
			$select = $connection->select()
                  ->from('customer_entity_varchar') 
                  ->where('attribute_id = ?', '178')
                  ->where('entity_id = ?', $user_id);
			$result = $connection->fetchAll($select);
			if(!empty($result))
			{
				$sql = "update customer_entity_varchar set value='".$mobile."' WHERE entity_id='".$user_id."' and attribute_id='178' ";
				$connection->query($sql);		
			}
			else
			{
				$sql="INSERT INTO customer_entity_varchar(attribute_id, entity_id, value) VALUES ('178','".$user_id."','".$mobile."')";
				$connection->query($sql);		
			}  
		} 
	}    

  
	if($change_email==1)
	{
		$check=$api_helpers->check_email_exists($email);
		if($check==1)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Email already registered'); 	 
			echo json_encode($result_array);	 
			die; 
		}	
		else
		{
			$sql = "update customer_entity set email='".$email."' WHERE entity_id='".$user_id."' ";
			$connection->query($sql);	
		}
	} 
	
	   

	$sql = "update customer_entity set firstname='".$firstname."',lastname='".$lastname."' WHERE entity_id='".$user_id."' ";
	$connection->query($sql);
	
	$user_name=$firstname.'\n'.$lastname;	
	
	$sql = "update customer_grid_flat set name='".$user_name."' WHERE entity_id='".$user_id."' ";
	$connection->query($sql);  
	
	
	$user_array=array('user_id'=>$user_id);
	$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Changes saved successfully'); 	
	echo json_encode($result_array);	   
	die;  
		
	
	
	
	
} 
else
{ 
	
	
	
	$userArray=array(
		'firstname'=>$firstname,
		'lastname'=>$lastname,
		'email'=>$email,
		'mobile'=>$mobile,
		'company_nam'=>$company_name,
		'company_add'=>$company_address,
		'gst_number'=>$gst,
		'pan_number'=>$pan,
		'seller_zipcode'=>'0',
	);
	
	  
	
	$requestedData=json_encode($userArray);
	
	
	$select = $connection->select()
		  ->from('custom_user_edit_account') 
		  ->where('user_id = ?', $user_id);
		 
		  

	$result = $connection->fetchAll($select);
	$is_seller=0;  
	$today=date('Y-m-d H:i:s');	 
	if(empty($result))
	{
		$sql = "Insert Into custom_user_edit_account (user_id,group_id,is_seller,edit_account,cr_date) values (
		'".$user_id."','".$group_id."','".$is_seller."','".$requestedData."','".$today."')";
		$connection->query($sql);    
		$message='Company changes have been sent to Admin for approval';	
	}
	else      
	{  
		$sql = "Update custom_user_edit_account set edit_account='".$requestedData."',cr_date='".$today."' where user_id='".$user_id."'";
		$connection->query($sql); 
		
	}     
	
	$helpers->admin_change_company_request($firstname,$lastname,$email); 
	
	$user_array=array('user_id'=>$user_id);
	$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Changes Sent for Admin Approval'); 	
	echo json_encode($result_array);	  
	die;  
	
}	
    


?>