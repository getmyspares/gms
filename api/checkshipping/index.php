<?php
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);
/*
{
	"accesstoken": "1234",
	"quote_id": "455",
	"user_id": "14"
}
*/


include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$exps_helper = $objectManager->create('OM\ExpressShipping\Helper\Data');

$argumentsCount=count($result);
$user_array=null; 

if($argumentsCount < 3 || $argumentsCount > 3)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User ID is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['quote_id']) || empty(@get_numeric($result['quote_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Quote Id parameter is missing'); 	
	echo json_encode($result_array);	
	die; 
}


$accesstoken=$result['accesstoken'];
$user_id  = @get_numeric($result['user_id']);
$quote_id = @get_numeric($result['quote_id']); 


if(!empty(@get_numeric($result['user_id']))){
	$user_id = @get_numeric($result['user_id']);
}else{
	$user_id = '';	
}


if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	 
		die;
	}	
}



$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

	
if($user_id=="")
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User ID is missing'); 	
	echo json_encode($result_array);	
	die;	
}
else
{	  	
	$check_user=$api_helpers->check_customer_exists($user_id);	 
	 if($check_user==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User not exists'); 	
		echo json_encode($result_array);	
		die;	
	}	
}	

$quote = $objectManager->create('Magento\Quote\Model\QuoteFactory')->create()->load($quote_id);

$shippingData = $exps_helper->getExpressShippingPrice($quote);

print_r( $shippingData );

?>



