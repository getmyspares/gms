<?php
	// Catgeory's product api. 
	//https://dev.idsil.com/design/api/product_sort_pricehightolow/?cat_id=56&accesstoken=1234&page_no=2
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	if($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	}

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}
	
	if(!!isset($result['brand_id']) || empty(@get_numeric($result['brand_id']))){
		$brand_id = $result['brand_id'];
	}else{
		$brand_id = '';	
	}
	
	if($brand_id ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'brand id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$brand_id = $result['brand_id'];
	}
	

	try
	{
		//$categoryId = $_GET['cat_id'];
	 
		 
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		 
		 // YOUR CATEGORY ID
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
									 ->addAttributeToSelect('*')->setCurPage($page_no) // page Number
									 ->addAttributeToFilter('status', 1)
									 ->addAttributeToSort('price', 'DESC')
									 ->addAttributeToFilter('brands', $brand_id)
									 // ->addAttributeToSort('price', 'ASC')
									 ->addAttributeToFilter('visibility', 4)
									 ->setPageSize(10); // elements per pages
									 
		$product_counts = $category->getProductCollection()->count();
									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();			
		
		$counts=count($categoryProducts);
		
		
		$i = 1 ;
		foreach ($categoryProducts as $product) {
			// if($product->getStatus()==1){
				$id = $product->getId();
				//$products[$id]= $product->getData();
				$product_array[$i]['id']= $product->getId();
				$product_array[$i]['name']= "'".$product->getName()."'";
				$product_array[$i]['sku']= "'".$product->getSku()."'";
				$product_array[$i]['price']= "'".$product->getPrice()."'";
				$product_array[$i]['specialprice'] = $product->getSpecialPrice();
				$product_array[$i]['specialfromdate'] = $product->getSpecialFromDate();
				$product_array[$i]['specialtodate'] = $product->getSpecialToDate();
				$product_array[$i]['brands_name'] = $product->getAttributeText('brands');
				$product_array[$i]['brands_id'] = $product->getData('brands');
				
				$product_array[$i]['image'] = "'".$store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage()."'";
				
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$product_array[$i]['rating_count'] = $ratings;
				}else{
					$product_array[$i]['rating_count'] = null;
				}
				
				$i++;
			// }
		}
		
		foreach($product_array as $value){
			$data[] = $value;
		}
		// print_r($product_array);
		// die();
		
		if( $page_no > (ceil($product_counts/10))  ){
			
			$result_array = array('success' => 0, 'result' => null, 'error' => "The page number exceded the max number."); 
			echo json_encode($result_array);
			die();
			
		}
		
		if(!empty($data)){
			$product_array['product_count'] = $product_counts;
			$product_array['pages_count'] = ceil($product_counts/10);
			$result_array = array('success' => 1, 'result' => $data,"product_count" =>$counts,"pages_count" =>$product_array['pages_count'], 'error' =>"Product lies under this brand assigned to this category.");  
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $data, 'error' => "In this category there is not product assign to this brand ."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>