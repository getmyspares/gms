<?php
require "../security/sanitize.php";
/*
	
Example of json format for the OTP match
{
	"accesstoken": "1234",
	"custID":"387",
	"oldPassword":"1245",
	"newPassword":"1245"
	"confirmPassword":"1245"
} 
 
*/


$result = sanitizedJsonPayload();
if(!is_array($result) || (is_array($result) && empty($result))) {
	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
	die;
}

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

$user_array=null; 

$argumentsCount=count($result);

if($argumentsCount < 5 || $argumentsCount > 5)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['custID']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Customer ID is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['oldPassword']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Old Password is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['newPassword']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'New Password is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['confirmPassword']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Confirm Password is missing'); 	
	echo json_encode($result_array);	
	die;  
}

 

$accesstoken=$result['accesstoken'];
$cust_id= @get_numeric($result['custID']);
$oldpassword=$result['oldPassword'];
$newpassword=$result['newPassword'];
$confirmpassword=$result['confirmPassword']; 
  

 

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 



	
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
} 
	
if($cust_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Customer Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
$authentication_response = $api_helpers->authenticateUser($cust_id,$accesstoken);
if($authentication_response['success']==0)
{
	echo json_encode($authentication_response);	
	die;
}

if($oldpassword=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Old Password is missing'); 	
	echo json_encode($result_array);	
	die;
}
if($newpassword=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'New Password is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if($confirmpassword=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Confirm Password is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if($confirmpassword!=$newpassword)
{ 
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Confirm Password is not matching with new password'); 	
	echo json_encode($result_array);	  
	die; 
}	 


$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
$CustomerModel->setWebsiteId(1); 
$CustomerModel->load($cust_id);  
$userId = $CustomerModel->getId();
$useremail = $CustomerModel->getEmail();

if($userId=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User not exist'); 	
	echo json_encode($result_array);	
	die; 
}  	
else
{
	
	try
	{

		$customers = $objectManager->get('\Magento\Customer\Model\AccountManagement')->authenticate($useremail, $oldpassword);
		$custId=$customers->getId(); 
		 
		$customerId = $custId; // here assign your customer id
		$password = $newpassword; // set your custom password 

		$customerRepositoryInterface = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
		$customerRegistry = $objectManager->get('\Magento\Customer\Model\CustomerRegistry');
		$encryptor = $objectManager->get('\Magento\Framework\Encryption\EncryptorInterface');

		 
		 
		$customer = $customerRepositoryInterface->getById($customerId); // \Magento\Customer\Api\CustomerRepositoryInterface
		$customerSecure = $customerRegistry->retrieveSecureData($customerId); // _customerRegistry is an instance of 
		$customerSecure->setRpToken(null);
		$customerSecure->setRpTokenCreatedAt(null);
		$customerSecure->setPasswordHash($encryptor->getHash($password, true)); // here _encryptor is an instance of \Magento\Framework\Encryption\EncryptorInterface
		$customerRepositoryInterface->save($customer);

		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Password has been changed'); 	
		echo json_encode($result_array);	 
		die;     
		  
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Old Password not matched'); 	
		echo json_encode($result_array);	
		die;
	}  
	
} 
  


?>



