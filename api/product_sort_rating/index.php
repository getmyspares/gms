<?php
	// Catgeory's product api. 
	//https://dev.idsil.com/design/api/product_sort_pricehightolow/?cat_id=56&accesstoken=1234&page_no=2
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 3 || $argumentsCount > 3)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	}

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}

	try
	{
		//$categoryId = $_GET['cat_id'];

		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		 
		 // YOUR CATEGORY ID
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
									// ->addAttributeToSelect('*')->setCurPage($page_no) // page Number
									 ->addAttributeToFilter('status', 1)
									 // ->addAttributeToSort('price', 'DESC')
									 ->addAttributeToSort('price', 'ASC')
									 ->addAttributeToFilter('visibility', 4);
									 //->setPageSize(10); // elements per pages
									 
		$product_counts = $category->getProductCollection()->count();
									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();			
		
		$i = 1 ;
		foreach ($categoryProducts as $product) {
			// if($product->getStatus()==1){
				
				$id = $product->getId();
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($id);
				//$products[$id]= $product->getData();
				$product_array[$i]['id']= "'".$product->getId()."'";
				$product_array[$i]['name']= "'".$product->getName()."'";
				$product_array[$i]['sku']= "'".$product->getSku()."'";
				$product_array[$i]['price']= "'".$product->getPrice()."'";
				$product_array[$i]['specialprice'] = $product->getSpecialPrice();
				$product_array[$i]['specialfromdate'] = $product->getSpecialFromDate();
				$product_array[$i]['specialtodate'] = $product->getSpecialToDate();
				
				//$product_array[$i]['image'] = "'".$store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage()."'";
				
				if(!empty($product->getImage())){
					 		
					$product_array[$i]['image'] = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
				}else{
					$site_url = $storeManager->getStore()->getBaseUrl();
					$imageUrl = $site_url.'Customimages/product-thumb.jpg';
					$product_array[$i]['image'] = $imageUrl;
				}
				
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$product_array[$i]['rating_count'] = $ratings;
				}else{
					$product_array[$i]['rating_count'] = null;
				}
				  
				if(isset(@get_numeric($result['user_id'])))
				{  
					$user_id=@get_numeric($result['user_id']);
					$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
					$product_array[$i]['wishlist']=$whishlist;
				} 
				else
				{
					$product_array[$i]['wishlist']=null;
				} 
				
				
				$i++;
			// }
		}
		// ---  Function Order by rate 
		function multi_sort($array, $akey){ 
		  function compare($a, $b){
			 global $key;
			 return strcmp($a[$key], $b[$key]);
		  }
		  usort($array, "compare");
		  return $array;
		}
		
		$product_array = multi_sort($product_array, $key = 'rating_count');
		
		
		$end = ((10)*$page_no)-1;
		if($end>10){
		 $start = (10*($page_no-1));
		}else{
			$start = 0;
		}
		// echo 'start'.$start;
		// echo 'end'.$end;
		
		$product_array= array_slice($product_array,$start,$end);
		
		foreach($product_array as $value){
			$data[] = $value;
		}
		
		
		$product_array['product_count'] = $product_counts;
		$product_array['pages_count'] = ceil($product_counts/10);

		
		
		if( $page_no > (ceil($product_counts/10))  ){
			
			$result_array = array('success' => 0, 'result' => null, 'error' => "The page number exceded the max number."); 
			echo json_encode($result_array);
			die();
			
		}
		
		
		if(!empty($data)){
			$result_array = array('success' => 1, 'result' => $data,"product_count" =>$product_array['product_count'],"pages_count" =>$product_array['pages_count'], 'error' =>"Product assigned to this category."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => null, 'error' => "The category id is not valid. Please check the category. Or there is not product assigned to this category."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>