<?php
/* 
accesstoken
id
*/
require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$customer_array = null;
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $customer_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $customer_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $customer_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	//$customer_id = $_GET['id'];
	if(!empty($result['id']) && !empty(@get_numeric($result['id']))){
		$customer_id = $result['id'];
	}else{
		$customer_id = '';	
	}



	if($customer_id ==''){
		$result_array=array('success' => 0, 'result' => $customer_array, 'error' => 'customer id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
	$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}		

	try
	{
		$url = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
		$state = $objectManager->get('\Magento\Framework\App\State'); 
		$state->setAreaCode('frontend');
		// Get Store ID
		$store = $storeManager->getStore();
		$storeId = $store->getStoreId();
		$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory'); 
		$customer=$customerFactory->create();
		//$customer->setWebsiteId($websiteId);
		//$customer->loadByEmail('example@gmail.com');// load customer by email address
		//echo $customer->getEntityId();
		$customer->load($customer_id);// load customer by using ID
		$customer_array= $customer->getData();
		//print_r($data);
		if(!empty($customer_array)){
			$array = array('success' => 1, 'result' => $customer_array,'error' => "Customer's details."); 
		}else{
			$array = array('success' => 0, 'result' => $customer_array, 'error' => "Please check the customer id again."); 
		}
		echo json_encode($array);
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$array = array('success' => 0, 'result' => $customer_array, 'error' => $e->getMessage()); 

		echo json_encode($array);
		
		
	}