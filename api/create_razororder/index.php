<?php

	require "../security/sanitize.php";
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/add_to_cart_db/
	//Input 
	///*{"accesstoken": "1234","user_id":"1399","amount":"269"}*/

	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = sanitizedJsonPayload();
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');
	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	
	$response = null;	
	$argumentsCount=count($result);
	$user_array=null;
	if($argumentsCount < 3 || $argumentsCount > 3)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	if(!empty($result['amount'])){
		$amount = $result['amount'];
	}else{
		$amount = '';	
	}
	
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}		
		
	}
	
	
	
	if($amount ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Amount is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	 
	
	
	
	$token=$api_helpers->get_token(); 
	$quote_id=$api_helpers->get_quote_id($token,$user_id);
	$quote_id=json_decode($quote_id);
	
	
		
	$site_url=$storeManager->getStore()->getBaseUrl();
	
	$url=$site_url.'qbonline/connection/razorpayorder/amount/'.$amount.'/quote_id/'.$quote_id; 
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	$response  = curl_exec($ch); 
	
	$result=$response; 
	
	
	$check=$api_helpers->check_api();  
	//$check=1;   
	
	if($result=='0')  
	{
		$result_array = array('success' => 0,'result' => $user_array,'error' =>"Order id not created"); 
		echo json_encode($result_array);	
		die;
	}
	else 
	{
		if($check==0)   
		{
			$user_array=array();   
			$result=array();   
			$result_array = array('success' => 1,'order_id'=>$result,'user_id'=>$user_id,'quote_id'=>$quote_id,'result' => $user_array,'error' =>"Razorpay Order id created"); 
			echo json_encode($result_array);
		}
		else 
		{	
			$user_array=array('order_id'=>$result,'user_id'=>$user_id,'quote_id'=>$quote_id);  
			$result_array = array('success' => 1,'order_id'=>$result,'user_id'=>$user_id,'quote_id'=>$quote_id,'result' => $user_array,'error' =>"Razorpay Order id created"); 
			//print_r($result_array);
			echo json_encode($result_array); 
		}
		
		
		
	}	 

	
	
	
?>
