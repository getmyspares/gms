<?php
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data');

$url = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface'); 

$state = $objectManager->get('\Magento\Framework\App\State');
$state->setAreaCode('frontend');

if(empty($result))
{
    $result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}
$argumentsCount=count($result);
$user_array=null;
$missing="Required Parameters Are  Missing";
$alibi="Cannot Create Account With The Details Provided";

if($argumentsCount < 3 || $argumentsCount > 3)
{ 
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}        
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['customer_id']) || empty(@get_numeric($result['customer_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['device_id']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	 
	die; 
}

$accesstoken=$result['accesstoken'];
$customer_id=@get_numeric($result['customer_id']);
$device_id=$result['device_id'];
  

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}


if($device_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => "$missing"); 	
	echo json_encode($result_array);	
	die;
}
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');

if($customer_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check_user=$api_helpers->check_user_exists($customer_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}		
	
}

$sql = "select id from `mobile_token_auth` where device_id='$device_id' and user_id='$customer_id' and token='$accesstoken'";
$status = $connection->fetchOne($sql);
if(empty($status))
{
    $result_array=array('success' =>1, 'result' => $user_array,'error' => 'Invalid Payload');  	
    echo json_encode($result_array);	   
    die;	 

}

$sql = "delete from `mobile_token_auth` where device_id='$device_id' and user_id='$customer_id' and token='$accesstoken'";
$status = $connection->query($sql);
$result_array=array('success' =>1, 'result' => $user_array,'error' => 'Customer Token Flushed');  	
echo json_encode($result_array);	   
die;	 

	

