<?php
/*
{
	"accesstoken": "1234",
	"order_id": "517",
	"user_id": "722",
	"item_details":[
		{
			"item_id":"619",
			"qty":"1",
			"reason_id":"1",	
			"reason_name":"The Great"
		},
		{
			"item_id":"620",
			"qty":"1",
			"reason_id":"1",	
			"reason_name":"Name The"
		}
	],
	"image":"",
	"order_delivered": "1",
	"consignment_no": "",
	"reason": "1",
	"comments": "1",
	"refund_mode": "1"
}
*/   
// require "../security/sanitize.php";
// $result = sanitizedJsonPayload();
$result = $_POST;
$images = $_FILES;

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
if(!empty($result))
{
	$argumentsCount=count($result); 
}
$user_array=null; 
$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
$store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getStore();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['item_details']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Item Details are incorrect'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['order_id']) || empty($result['order_id']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order Id is missing'); 	
	echo json_encode($result_array);	  
	die; 
}

if(!isset($result['order_delivered']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order delivered is missing'); 	
	echo json_encode($result_array);	 
	die; 
}


if(!isset($result['comments']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Comments is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['refund_mode']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Refund mode is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

$accesstoken=$result['accesstoken'];
$order_id=$result['order_id']; 
$user_id=$result['user_id']; 
$order_delivered=$result['order_delivered']; 
$comments=$result['comments']; 
$refund_mode=$result['refund_mode']; 

if($order_delivered==0) 
{
	// if(isset($result['consignment_no']))
	// {	
		// 	$consignment_no=$result['consignment_no']; 
		// 	if($consignment_no=='')  
		// 	{
			// 		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Consignment Number is missing'); 	
			// 		echo json_encode($result_array);	 
			// 		die; 
			// 	} 
			// } 
			
			
			if(isset($result['package_condition']))
			{
				$package_condition=$result['package_condition']; 
				if($package_condition=='')  
				{
					$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Package Condition is missing'); 	
					echo json_encode($result_array);	 
					die;  
				}
			}	
		}	
else
{
	$consignment_no='';
	$package_condition='';
}    

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check=$api_helpers->check_user_exists($user_id);
	if($check==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
}	

$accesstoken=$result['accesstoken'];
$order_id=$result['order_id']; 
$user_id=$result['user_id']; 
$order_delivered=$result['order_delivered']; 
$comments=$result['comments']; 
$refund_mode=$result['refund_mode']; 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);

$item_details = json_decode($result['item_details'],true); 
foreach ($item_details as  $value) {
	$item_id = $value['item_id'];
	$return_qty = $value['qty'];
	$item_ordered = $connection->fetchOne("select qty_ordered from `sales_order_item` where  item_id='$item_id'");
	if(empty($item_ordered))
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Requested Item not found in that order'); 	
		echo json_encode($result_array);	
		die;	
	}
	$select = $connection->select() 
	->from('wk_rma_items') 
	->where('order_id = ?', $order_id)
	->where('item_id = ?', $item_id); 
	$result = $connection->fetchAll($select);
	$total_item_qty=0;

	if(!empty($result))
	{
		
		foreach($result as $row)
		{
			$total_item_qty=$total_item_qty+$item_qty;
			$item_qty=$row['qty'];	
		
		}		
	}	


	if($total_item_qty!=0)
	{
		$remaining_qty=$item_ordered-$total_item_qty;
		if($remaining_qty!=0)
		{
			if($return_qty > $remaining_qty)
			{
				$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Return Qty is incorrect'); 	
				echo json_encode($result_array);	
				die;	
			} 
		} else 
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Item already processed for refund'); 	
			echo json_encode($result_array);	
			die;
		}

	}	 

	if($return_qty > $item_ordered)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Return Qty is incorrect'); 	
		echo json_encode($result_array);	
		die;	
	}  
}

$user_details=$api_helpers->get_user_details($user_id);
$name=$user_details[0]['firstname'].' '.$user_details[0]['lastname'];
$imageArray = [];

$order_inc_id=$order->getIncrementId();
$group='customer';
$customer_id=$user_id;
$package_condition=$package_condition; 
$resolution_type=$refund_mode;
$additional_info=$comments; 
$customer_delivery_status=$order_delivered;
$customer_consignment_no=$consignment_no;
$admin_delivery_status='0';
$admin_consignment_no='';
$image=serialize($imageArray); 
$shipping_label='0';
$guest_email='';
$status='1';
$created_at=date('Y-m-d H:i:s');
$name=$name;
$admin_status='1';
$final_status='0'; 





$sql="INSERT INTO wk_rma(order_id,`group`,`increment_id`, customer_id, package_condition, resolution_type, additional_info, customer_delivery_status, customer_consignment_no, admin_delivery_status, admin_consignment_no, image, shipping_label, guest_email, status, created_at, name, admin_status, final_status) VALUES ('".$order_id."','".$group."','".$order_inc_id."','".$customer_id."','".$package_condition."','".$resolution_type."','".$additional_info."','".$customer_delivery_status."','".$customer_consignment_no."','".$admin_delivery_status."','".$admin_consignment_no."','".$image."','".$shipping_label."','".$guest_email."','".$status."','".$created_at."','".$name."','".$admin_status."','".$final_status."')";      
$connection->query($sql);	

$sql="select * from wk_rma where order_id='".$order_id."' and customer_id='".$customer_id."' order by rma_id desc limit 0,1";
$result = $connection->fetchAll($sql); 
$rma_id=$result[0]['rma_id'];



if(!empty($images))
{
	$lastRmaId = $rma_id;
	$rmmhelper = $objectManager->get('Webkul\Rmasystem\Helper\Data');
	$path = $rmmhelper->getBaseDir($lastRmaId);
	
	if(!is_dir($path)) {
		mkdir($path, 0777,true);
	}
	$i=0;
	foreach ($_FILES["image"]["name"] as $value) {
		$target_file = $path . basename($_FILES["image"]["name"][$i]);
		move_uploaded_file($_FILES["image"]["tmp_name"][$i], $target_file);
		$i++;
	}
}



foreach ($item_details as  $value) {
	$rma_id=$rma_id;
	$item_id=$value['item_id'];
	$qty=$value['qty'];
	$reason_id=$value['reason_id'];
	$order_id= $order_id; 
	$rma_reason=$value['reason_name'];   
	
	$sql="INSERT INTO `wk_rma_items`(`rma_id`, `item_id`, `reason_id`, `qty`, `order_id`, `rma_reason`) VALUES ('".$rma_id."','".$item_id."','".$reason_id."','".$qty."','".$order_id."','".$rma_reason."')";
	$connection->query($sql);	
}

$user_array=array('order_id'=>$order_id,'item_id'=>$item_id);
$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Return Successfully Saved.'); 	 
echo json_encode($result_array); 	 
die; 

?>