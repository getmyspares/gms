<?php
	// Get parent category details. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	 // print_r($result);
	
	$parent_model_array = null;
	$final_array = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 2 || $argumentsCount > 6)
	{
		$result_array=array('success' => 0,'result' => $parent_model_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	if(!empty($result['category_id'])){
		$categoryId = $result['category_id'];
	}else{
		$categoryId = '';	
	}
	
	if($categoryId=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'category is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	if(!empty($result['brand_filter']))
	{
		$brand_filter = $result['brand_filter'];
	}
	else
	{
		$brand_filter = '';	 
	}
		
	if(!empty($result['model'])){
		$model_filter = $result['model'];
		
	}else{
		$model_filter = '';	
	}	
	
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
		
	}else{
		$rating_filter = '';	
	}

	
		
	if(!empty($brand_filter))
	{
		$filter_brands = explode(",",$brand_filter);
	}		
		
	
	if(!empty($model_filter))
	{
		$filter_models = explode(",",$model_filter);
	} 
	
	if(!empty($rating_filter))
	{
		$filter_ratings = (explode(",",$rating_filter));
	} 
	
	
	$star_1=$star_2=$star_3=$star_4=$star_5=0;
	
	try
	{
		
		
		$defaultArray=array();
		$subcategoryArray=array();
		
		$brand_prc_array=array();
		$brand_prd_array=array();
		$rating_prd_array = array(); 
		$rating_prc_array = array(); 
		
		$category_prc_array = array(); 
		$category_prd_array = array(); 
		
		$brand_result = array(); 
		$modelarray = array(); 
		$model_result = array(); 
		
		$brand_result_array=array();
		$model_result_array=array();
		
		$brand_result_arrayy=array();
		$model_result_arrayy=array();
		
		$brandArray=array();
		
		
		
		
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		
		$category = $categoryFactory->create()->load($categoryId);
		$category_name = trim($category->getName()); 
		
		
		$categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('entity_id')
			->addAttributeToSort('price', 'desc');
		$maxProductPrice = $categoryProducts->getFirstItem()->getPrice();
		
		 
		$categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('entity_id')
			->addAttributeToSort('price', 'asc');
		$minProductPrice = $categoryProducts->getFirstItem()->getPrice();
		
		
		
		$categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('entity_id')
			->load();   
			
			
		
			
			
		
		$product_count=$categoryProducts->count();
		
		if($product_count==0)
		{
			$user_array=array();
			$result_array=array('success' => 0,'result' => $user_array,'category_name'=>$category_name, 'error' => 'No Product Found'); 	
			echo json_encode($result_array);	
			die;		
		}	
		
		$registry = $objectManager->get('\Magento\Framework\Registry');
		$layerResolver = $objectManager->get('\Magento\Catalog\Model\Layer\Resolver');
		
		$layer = $layerResolver->get();
        $layer->setCurrentCategory($category->getId());
		
		
        $fill = $objectManager->create('Magento\Catalog\Model\Layer\Category\FilterableAttributeList');
        $filterList = new \Magento\Catalog\Model\Layer\FilterList($objectManager,$fill);
        $filterAttributes = $filterList->getFilters($layer);
		
		foreach($filterAttributes as $filter)
		{
			$filter->getName();
		}
		
		$j = 0;
		
		foreach($filterAttributes as $filter)
		{
			if($filter->getName() == 'Brand'){
                $items = $filter->getItems();
                foreach($items as $item)
                {
                    $filterValues[$j]['name'] = strip_tags($item->getLabel());
                    $filterValues[$j]['id'] = $item->getValue();
                    $filterValues[$j]['count'] = $item->getCount(); 
                    $j++; 
                }
            }
		}
		
		
		$brandArray=$filterValues;
		
		//print_r($brandArray);
		
		
		
		$count_1=0;
		$count_2=0;
		$count_3=0;
		$count_4=0;
		$count_5=0;
		$prod_str='';
		foreach($categoryProducts as $product)
		{
			//$defaultArray[]=$product->getId(); 
			
			$product_iddd=$product['entity_id'];  
			$prod_str .=$product['entity_id'].',';
			
		}	
		
		$product_list=substr($prod_str,0,-1);	
		
		$sql_group="SELECT rating_summary,count(*) as 'total_count' FROM `review_entity_summary` where store_id=1 and entity_pk_value in (".$product_list.") group by rating_summary";  
		$resultsss_group = $connection->fetchAll($sql_group);	
		
		//print_r($resultsss_group);
		
		if(!empty($resultsss_group))
		{
			foreach($resultsss_group as $rating_star)
			{
				
				if($rating_star['rating_summary'] >= 20 && $rating_star['rating_summary'] < 40)
				{
					$count_1=$rating_star['total_count'];
				}
				
				if($rating_star['rating_summary'] >= 40 && $rating_star['rating_summary'] < 60)
				{
					$count_2=$rating_star['total_count'];
				}
				if($rating_star['rating_summary'] >= 60 && $rating_star['rating_summary'] < 80)
				{
					$count_3=$rating_star['total_count'];
				}
				if($rating_star['rating_summary'] >= 80 && $rating_star['rating_summary'] < 100)
				{
					$count_4=$rating_star['total_count'];  
				}
				if($rating_star['rating_summary']==100)
				{
					$count_5=$rating_star['total_count'];
				}
				
			}	
		}	
		
		
		
		
		//print_r($category_prc_array);
		
		//die;
		$model_result_arrayy=array();
		
		if(!empty($modelArray))
		{
			$modelArray = array_map("unserialize", array_unique(array_map("serialize", $modelArray)));
			foreach($modelArray as $row)
			{
				$model_id=$row;		
				$model_name=$api_helpers->get_product_model_name_new($model_id);	
				$count=0;
				
				$category = $categoryFactory->create()->load($categoryId);
				$categoryProducts = $category->getProductCollection()
					->addAttributeToSelect('model')
					->addAttributeToFilter('model', ['eq' => $model_id])	 
					->load();   
				
				$count=$categoryProducts->count();
				
				$model_result_arrayy[]=array(
					'id'=>$model_id,   
					'name'=>$model_name, 
					'count'=>$count 
				);	
				
				
			}  
			
		}	
		
		
		//print_r($model_result_arrayy);
		
		
		
		
		
		
		
		$star_1=$count_1;
		$star_2=$count_2;
		$star_3=$count_3;
		$star_4=$count_4;
		$star_5=$count_5; 
		
		
		$max_price=$maxProductPrice;
		$min_price=$minProductPrice;
		
		$default_price_min=number_format($min_price, 2, '.', '');
		$default_price_max=number_format($max_price, 2, '.', '');
		
		
		$min_price=number_format($min_price, 2, '.', '');
		$max_price=number_format($max_price, 2, '.', ''); 
		
		
		/*
		$min_price=0;
		$max_price=0;
		

		
		
		
		
		
		if(!empty($category_prc_array)){
			$default_price_min = min($category_prc_array);
			$default_price_max = max($category_prc_array);
			
			$default_price_min=number_format($default_price_min, 2, '.', '');
			$default_price_max=number_format($default_price_max, 2, '.', '');
			
			
		}
		
		if(!empty($category_prc_array)){
			$min_price = min($category_prc_array);
			$max_price = max($category_prc_array);
		
			$min_price=number_format($min_price, 2, '.', '');
			$max_price=number_format($max_price, 2, '.', ''); 
		
		}
		*/
		$subcategoryArray=array();
		
		$parent_model_array['sub_cat'] = $subcategoryArray;
		$parent_model_array['brand'] = $brandArray;  
		$parent_model_array['model'] = $model_result_arrayy;  
		 
		$parent_model_array['stars'] = array("1 Star"=>$star_1,"2 Star"=>$star_2,"3 Star"=>$star_3,"4 Star"=>$star_4,"5 Star"=>$star_5); 
		$parent_model_array['price'] = array("min_price"=>$min_price,"max_price"=>$max_price);
		$parent_model_array['default_price'] = array("min_price"=>$default_price_min,"max_price"=>$default_price_max);
		$result_array = array('success' => 1, 'result' => $parent_model_array, 'error' =>"Category filters details."); 
		echo json_encode($result_array);
		
		die; 
		
		
		
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $parent_model_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>