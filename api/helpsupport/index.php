<?php
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	//print_r($result);
	
	
	$page_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $page_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $page_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $page_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$content = $objectManager->create('\Magento\Cms\Model\PageFactory')->create();
		$content->load(565); 
		$page_arrays = $content->getContent();
		$page_array = strip_tags($page_arrays,"<style>");

		$substring = substr($page_array,strpos($page_array,"<style"),strpos($page_array,"</style>")+2);

		$page_array = str_replace($substring,"",$page_array);
		$page_array = str_replace(array("\t","\r","\n"),"",$page_array);
		$page_array = trim($page_array);


		if(!empty($page_array)){
			$result_array = array('success' => 1, 'result' => $page_array, 'error' => 'Content displayed properily .');
			echo json_encode($result_array);
			die();
		}else{
			$result_array =array('success' => 0, 'result' =>$page_array,'error' => 'There is was some issue while data.');
			echo json_encode($result_array);
			die();
		}
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$page_array, 'error' => $e->getMessage());
		echo json_encode($result_array);
		die();		
	} 
?>