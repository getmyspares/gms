<?php

	// Excitingdeals api.
	//	Url Example :- https://dev.idsil.com/design/api/logo/?accesstoken=1234
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	//print_r($result);
	
	$deals_array = null;
	$argumentsCount=count($result);


	$user_array=null; 

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}	

	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $deals_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $deals_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	try
	{

		$appState = $objectManager->get('\Magento\Framework\App\State');

		 
		$logo = $objectManager->get('\Magento\Theme\Block\Html\Header\Logo');
		// echo $logo->getLogoSrc() . '<br />';
		// echo $logo->getLogoAlt() . '<br />';

			
		$user_array=array(  
			'logoSrc'=>$logo->getLogoSrc(),
			'logoAlt'=>$logo->getLogoAlt()
		);
		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Site logo and alternative name data.'); 	
		echo json_encode($result_array);	 
		die;     
		
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'There was something wrong. Please try again.'); 	
		echo json_encode($result_array);	
		die;
	}  
	
	
	
	

  


?>