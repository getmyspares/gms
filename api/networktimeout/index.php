<?php
require "../security/sanitize.php"; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');   

$scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
$timeout = $scopeConfig->getValue('mobilepopup/mobile_api/networktimeout', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 

$result_array=array('success' => 1, 'timeout' => $timeout); 	
echo json_encode($result_array);	
