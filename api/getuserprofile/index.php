<?php
require "../security/sanitize.php";
//$result = sanitizedJsonPayload();
//print_r($result);


$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');
	
$argumentsCount=count($result);
$user_array=null; 

if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'user Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
 

  

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);




$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	}

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
	
}
	


	

//$user_array=$api_helpers->get_user_details($user_id);

$user_details=$api_helpers->get_user_details($user_id);
		
//print_r($user_details);

$group_id=$user_details[0]['group_id'];


$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$select = $connection->select()
		->from('mst_rewards_transaction') 
		->where('customer_id = ?', $user_id);
$result = $connection->fetchAll($select);


	


$total_reward=0;
if(!empty($result))
{
	foreach($result as $row)
	{
		$total_reward=$total_reward + $row['amount'];  
	}	 
}
 


$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
$wishlistAdd = $wishList->create()->loadByCustomerId($user_id, true);

// Check if the wishlist have product or not.
$wishlist_count =  $wishlistAdd->getItemCollection()->count();
 

if($group_id==1 || $group_id==5)
{	
	$userArray=array(
			'user_id'=>$user_id,
			'firstname'=>$user_details[0]['firstname'],	
			'lastname'=>$user_details[0]['lastname'],	 
			'email'=>base64_encode($user_details[0]['email']),	
			'mobile'=>base64_encode($user_details[0]['mobile']),	
			'group_id'=>$user_details[0]['group_id'],	
			'total_reward'=>$total_reward,	
			'wishlist_count'=>$wishlist_count
	);	
}
else
{
	$userArray=array(
			'user_id'=>$user_id,
			'firstname'=>$user_details[0]['firstname'],	
			'lastname'=>$user_details[0]['lastname'],	 
			'email'=>base64_encode($user_details[0]['email']),	 
			'mobile'=>base64_encode($user_details[0]['mobile']),	
			'group_id'=>$user_details[0]['group_id'],	
			'gst'=>$user_details[1]['gst'],	
			'company_name'=>$user_details[1]['company_name'],	
			'company_address'=>base64_encode($user_details[1]['company_address']),	
			'pan'=>$user_details[1]['pan'],	
			'total_reward'=>$total_reward,	
			'wishlist_count'=>$wishlist_count
	);	
}


$result_array=array('success' => 1,'result' => $userArray, 'error' => 'Details found'); 	
echo json_encode($result_array);	
die; 
  


?>