<?php
	// Catgeory's product price range filter. 
	//https://dev.idsil.com/design/api/category_product_price_filter/?cat_id=66&accesstoken=1234&price_from=1000&price_to=2000&page_no=3
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	 
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);

	if($argumentsCount < 5 || $argumentsCount > 5)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	}

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}
	
	//---- Product Price range from variable
	
	if(!empty($result['price_from'])){
		$price_from = $result['price_from'];
	}else{
		$price_from = '';	
	}
	
	if($price_from ==''){
		$price_from=array('success' => 0, 'result' => $product_array, 'error' => 'Price range From key value is missing. Please try again.'); 	
		echo json_encode($price_from);	
		die;
	}else{
		$price_from = $result['price_from'];
	} 
	
	//---- Product Price range to variable
	
	if(!empty($result['price_to'])){
		$price_to = $result['price_to'];
	}else{
		$price_to = '';	
	}
	
	if($price_to ==''){
		$price_to=array('success' => 0, 'result' => $product_array, 'error' => 'Price range To key value is missing. Please try again.'); 	
		echo json_encode($price_to);	
		die;
	}else{
		$price_to = $result['price_to'];
	} 
	
	if($price_to < $price_from ){
		$price_to=array('success' => 0, 'result' => $product_array, 'error' => 'Price From should be smaller than price to.'); 	
		echo json_encode($price_to);	
		die;
		
		
	} 
	
	
	try{

		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		 
		 // YOUR CATEGORY ID
		$category = $categoryFactory->create()->load($categoryId);
		 
		$categoryProducts = $category->getProductCollection()
									 ->addAttributeToSelect('*')->setCurPage($page_no) // page Number
									 ->addAttributeToFilter('status', 1)
									 ->addAttributeToSort('price', 'DESC')
									 // ->addAttributeToFilter('brands', $brand_id)
									 // ->addAttributeToSort('price', 'ASC')
									 ->addAttributeToFilter('visibility', 4);
									// ->setPageSize(10); // elements per pages
									 
		$product_counts = $category->getProductCollection()->count();
									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();

		$i = 0 ;
		foreach ($categoryProducts as $product) {
			$id = $product->getId();
			$actual_price = $product->getPrice() ;
			

			$specialprice = $product->getSpecialPrice();
			$specialPriceFromDate = $product->getSpecialFromDate();
			$specialPriceToDate = $product->getSpecialToDate(); 
			$today = time();
			$actual_price = $product->getPrice();
			// echo '<br>';
				if ($specialprice) {
					if(!empty($specialPriceFromDate)){
						if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
							
							$actual_price = $specialprice;

						}
					}
				}
			
			
			
			
			
			
							
			if (($actual_price >= $price_from) && ($actual_price <= $price_to)){

				//$products[$id]= $product->getData();
				$product_array[$i]['id']= $product->getId();
				$product_array[$i]['name']= "'".$product->getName()."'";
				$product_array[$i]['sku']= "'".$product->getSku()."'";
				$product_array[$i]['price']= "'".$product->getPrice()."'";
				$product_array[$i]['specialprice'] = $specialprice;
				$product_array[$i]['specialfromdate'] = $specialPriceFromDate;
				$product_array[$i]['specialtodate'] = $specialPriceToDate;
				$product_array[$i]['brands_name'] = $product->getAttributeText('brands');
				$product_array[$i]['brands_id'] = $product->getData('brands');
				$product_array[$i]['image'] = "'".$store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage()."'";
				
				
				$ratings = 0;
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				
				if(!empty($ratings)){
				$product_array[$i]['rating_count'] = $ratings;
				}else{
					$product_array[$i]['rating_count'] = 0;
				}
				
				$i++;
			}			
		}
		$total_prd_count =count($product_array);
		// echo '<pre>';
		// print_r($product_array);
		// echo '</pre>';
		// die();
		$end = ((10)*$page_no)-1;
		if($end>10){
		 $start = (10*($page_no-1));
		}else{
			$start = 0;
		}
		// echo 'start'.$start;
		// echo 'end'.$end;
		
		$product_array= array_slice($product_array,$start,$end);
		
		
		
		foreach($product_array as $value){
			$data[] = $value;
		}
		// print_r($product_array);
		// die();
		
		if(!empty($data)){
			$product_array['product_count'] = $i;
			$product_array['pages_count'] = ceil($i/10);
			$result_array = array('success' => 1, 'result' => $data, "product_count" =>$total_prd_count,"pages_count" =>$product_array['pages_count'], 'error' =>"Product data."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $data, 'error' => "There is not product between this range. Please select some other price range."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $product_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>