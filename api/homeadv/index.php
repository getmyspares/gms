<?php
	// Homepage banner api.
	
	//	Url Example :- https://dev.idsil.com/design/api/mainbanner/?accesstoken=1234
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	
	
	$result = $_GET;
	//print_r($result);
	
	
	$banner_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $banner_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $banner_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $banner_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		
		

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		
		$site_url=$storeManager->getStore()->getBaseUrl();

		
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'mageplaza/bannerslider/banner/image/';
            
        //Select Data from table
        //$sql = "Select * FROM mageplaza_bannerslider_banner";
		$sql = "SELECT a.*,b.* FROM `mageplaza_bannerslider_banner_slider` as a inner join mageplaza_bannerslider_banner as b on a.banner_id=b.banner_id where a.slider_id=3 and b.status='1'";
        $result = $connection->fetchAll($sql);
        $i = 0;
        foreach($result as $row)
        {
			$banner_url=$storeManager->getStore()->getBaseUrl().'/pub/media/mageplaza/bannerslider/banner/image/'.$row['image'];
			$banner_url = str_replace('.jpg','_thumb.jpg',$banner_url);
			//$banner_url=$site_url.'image.php?image='.$banner_url;
			//$banner_array [$i]['image']= $storeManager->getStore()->getBaseUrl().'/pub/media/mageplaza/bannerslider/banner/image/'.$row['image'];
			$banner_array [$i]['image']= $banner_url;
			if($row['title']=='')
			{
				$banner_array [$i]['title']= 'Panasonic';
			}
			else
			{		
				$banner_array [$i]['title']= strip_tags($row['title']);
			}
			/* Url check code */ 
			
			// $banner_array [$i]['url_banner']= $row['url_banner'];
			$url=$row['url_banner'];  
			$url_details = $custom_helpers->get_url_type($url);
			$url_details_array = json_decode($url_details,true);
			// print_r($url_details_array);
			if($url_details_array['success']==1){
				$banner_array [$i]['url']=  $url;
				$banner_array [$i]['url_type']=  $url_details_array['entity_type'];
				$banner_array [$i]['url_banner']= $url_details_array['entity_id']; 
				$banner_array [$i]['url_id']= $url_details_array['url_rewrite_id']; 
			}
						
			$i++;
		}
		
		// print_r($banner_array);
		
		if(!empty($banner_array)){
			$result_array = array('success' => 1, 'result' => $banner_array, 'error' => 'Homepage adv Banner.');
		}else{
			$result_array =array('success' => 0, 'result' =>$banner_array,'error' => 'There is was some issue while uploading banners.');
		}
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$banner_array, 'error' => $e->getMessage()); 	
	} 
	  
	echo json_encode($result_array);
?>