<?php
	// Catgeory's product api. 
	//https://www.dev.idsil.com/design/api/categoryproduct/?cat_id=4&accesstoken=1234&page_no=1&brand_filter=47,48&rating_filter=1,2,3&price_from=1000&price_to=1100
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$product_array = null;
	$data = null;
	$argumentsCount=count($result);
	$filter=0;
	$user_array=null;
	/* if($argumentsCount < 3 || $argumentsCount > 9)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	}

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}
	
	
	if(!empty($result['key'])){
		$key = $result['key'];
	}else{
		$key = '';	
	}
	
	if(!empty($result['model'])){
		$model_filter = $result['model'];
		$filter+=1;
	}else{
		$model_filter = '';	
	}
	
	// Brand Filter
	if(!empty($result['brand_filter'])){
		$brand_filter = $result['brand_filter'];
		$filter+=1;
	}else{
		$brand_filter = '';	
	}
	
	// Rating Filter
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
		$filter+=1;
	}else{
		$rating_filter = '';	
	}
	
	
	// Price Filter
	if(!empty($result['price_from'])){
		$price_from = $result['price_from'];
		$filter+=1;
	}else{
		$price_from = '';	
	}
	
	if(!empty($result['price_to'])){
		$price_to = $result['price_to'];
		$filter+=1;
	}else{
		$price_to = '';	
	}
	
	
	if(@get_numeric($result['user_id'])!='')
	{
		$check_user=$api_helpers->check_user_exists(@get_numeric($result['user_id']));
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 	

		$authentication_response = $api_helpers->authenticateUser(@get_numeric($result['user_id']),$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	} 
	
	
	
	

	
	try{
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categoryRepository = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
		//Reward helper	
		$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 
		$category = $objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);
		
		if(!empty($category->getName())){
			$category_name = trim($category->getName());
		}else{
			$category_name =  null;
		}
		//load category by its id
		$category = $categoryFactory->create()->load($categoryId);
		
		// Collection of all product in category which status enable
		$categoryProducts = $category->getProductCollection()->addAttributeToSelect('*')->addAttributeToFilter('status', 1);
		
		$final_array = null;
		
		
		// get product as per key search from collection
		
		if(!empty($key)){
			$categorykeyProducts = $category->getProductCollection()
							->addAttributeToSelect('*')
							->addAttributeToFilter(  [  ['attribute' => 'name', 'like' => '%'.$key.'%']  ]);

					
			foreach ($categorykeyProducts as $product) {
					$key_prd_array[]= $product->getId();
			}
			
			// echo '<pre>';
			// print_r($key_prd_array);
			// echo '</pre>';
			if($final_array == null ){
				$final_array = $key_prd_array;
			}
		}
		
			
								
		// get product id which is linking with brands in array 						
		if(!empty($brand_filter)){
			
			$brand_prd_array = array();
			
			$filter_brands = explode(",",$brand_filter);
			// print_r($filter_brands);
			
			foreach ($categoryProducts as $product) {				
				$current_prd_brand = $product->getData('brands');
				if(in_array($current_prd_brand, $filter_brands)){
					$brand_prd_array[]= $product->getId();
				}
			}
			// print_r($brand_prd_array);
			
			if($final_array == null ){
				$final_array = $brand_prd_array;
			}else{
				$final_array = array_intersect($brand_prd_array,$final_array);			
			}
			
		}
		
		
		// get product id which is linking with model in array 						
		if(!empty($model_filter)){
			
			$model_prd_array = array();
			$filter_models = explode(",",$model_filter);
			
			foreach ($categoryProducts as $product) {				
				$current_prd_model = $product->getData('model');
				//if( $current_prd_model==$model_filter ){
				if(in_array($current_prd_model, $filter_models)){
					$model_prd_array[]= $product->getId();
				}
			}
			// print_r($model_prd_array);
			
			if($final_array == null ){
				$final_array = $model_prd_array;
			}else{
				$final_array = array_intersect($model_prd_array,$final_array);			
			}
			
		}
		
		
		
		// Get product ids as per product rating.
		 if(!empty($rating_filter)){
			$rating_prd_array = array(); 
			
			$filter_ratings = (explode(",",$rating_filter));
			// print_r($filter_ratings); 
			
			foreach($filter_ratings as $stars){
		 
				// Set Rating parameter as per the rating_filter number 
				if($stars==1){
					$rating_from = 0;
					$rating_to = 20;
					
				}else if($stars==2){
					$rating_from = 21;
					$rating_to = 40;
					
				}else if($stars==3){
					$rating_from = 41;
					$rating_to = 60;
				
				}else if($stars==4){
					$rating_from = 61;
					$rating_to = 80;
				
				}else if($stars==5){
					$rating_from = 81;
					$rating_to = 100;
					
				}

				foreach ($categoryProducts as $product) {
					$id = $product->getId();
					$ratings = 0;
					$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
					if(!empty($RatingOb->getCount())){
						$ratings = $RatingOb->getSum()/$RatingOb->getCount();
					}		
					if (($ratings >= $rating_from) && ($ratings <= $rating_to)){
						$rating_prd_array[]= $product->getId();

					}			
				}
			}
			// print_r($rating_prd_array);
			
			if($final_array == null ){
				$final_array = $rating_prd_array;
			}else{
				$final_array = array_intersect($rating_prd_array,$final_array);			
			}
		}
		
		// Get product ids as per product price range.
		 if(!empty($price_from)){
			 $price_prd_array = array();
			 
			if($price_to < $price_from ){
				$price_to=array('success' => 0, 'result' => null, 'error' => 'Price From should be smaller than price to.'); 	
				echo json_encode($price_to);	
				die;
			} 
			foreach ($categoryProducts as $product) {
				$id = $product->getId();
				
				
				// code to check special price is applied or not
				$specialprice = $product->getSpecialPrice();
				$specialPriceFromDate = $product->getSpecialFromDate();
				$specialPriceToDate = $product->getSpecialToDate(); 
				$today = time();
				$actual_price = number_format($product->getPrice(), 2, '.', '');
				// echo '<br>';
				if ($specialprice) {
					if(!empty($specialPriceFromDate)){
						if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
							
							$actual_price = number_format($specialprice, 2, '.', '');

						}
					}
				}

				if (($actual_price >= $price_from) && ($actual_price <= $price_to)){
					$price_prd_array[]= $product->getId();
				}			
			}
			 //print_r($price_prd_array);
			if($final_array == null ){
				$final_array = $price_prd_array;
			}else{
				$final_array = array_intersect($price_prd_array,$final_array);			
			}
			
			 
		 }
		
		
		// echo '<pre>';
		// print_r($final_array);
		// echo '</pre>';
		// die();

									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();

		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		
		
		$i = 1 ;
		foreach ($categoryProducts as $product) {
					
					$id = $product->getId();
					
					if($final_array != null){
						if (in_array($id, $final_array)){  
							//$products[$id]= $product->getData();
							$product_array['id']= $product->getId();
							$product_array['name']= $product->getName();
							$product_array['sku']= $product->getSku();
							$product_array['price']= number_format($product->getPrice(), 2, '.', '');
							if(@get_numeric($result['user_id'])!='')
							{ 
								$user_id=@get_numeric($result['user_id']);
								$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
								$product_array['wishlist']=$whishlist;
							}
							else
							{
								$product_array['wishlist']=null;
							}
							if($product->getAttributeText('brands')){
								$product_array['brand']= $product->getAttributeText('brands');
							}else{
								$product_array['brand']= null;	
							}
							
							if($product->getAttributeText('supported_models')){
								$product_array['supported_models']= $product->getAttributeText('supported_models');
							}else{
								$product_array['supported_models']= null;	
							}
							// From Rma extension
							$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
							if($product->getAttributeText('return_period')=="Flat return period"){
								$product_array['return_days']= $return_date;
							}
							
							$product_array['specialprice'] = $product->getSpecialPrice();
							$product_array['specialfromdate'] = $product->getSpecialFromDate();
							$product_array['specialtodate'] = $product->getSpecialToDate();
							
							$product_array['model'] = $product->getAttributeText('model');
							
							

							//echo $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('65','65')->getUrl();
							  
							if(!empty($product->getImage())){
							
								$product_array['image'] = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
							}else{
								$site_url = $storeManager->getStore()->getBaseUrl();
								$imageUrl = $site_url.'Customimages/product-thumb.jpg';
								$product_array['image'] = $imageUrl;
							}
							// $product_array['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
							
							$product_array['earn_reward'] = $earnOutput->getProductPoints($product);
							
							$ratings ='';
							$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
							if(!empty($RatingOb->getCount())){
								$ratings = $RatingOb->getSum()/$RatingOb->getCount();
							}
							if(!empty($ratings)){
								$product_array['rating_count'] = $ratings;
							}else{
								$product_array['rating_count'] = null;
							}
							
							$data[] = $product_array;
							
							$i++;
					}
				}else{
					if($filter>0){
						$data=array();
						$result_array = array('success' => 0, 'result' => $data, 'error' => "There is no product as per selected filter."); 
						echo json_encode($result_array);
						die();
						
					}else{
					//$products[$id]= $product->getData();
							$product_array['id']= $product->getId();
							$product_array['name']= $product->getName();
							$product_array['sku']= $product->getSku();
							$product_array['price']= number_format($product->getPrice(), 2, '.', '');
							if(@get_numeric($result['user_id'])!='')
							{
								$user_id=@get_numeric($result['user_id']);
								$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
								$product_array['wishlist']=$whishlist;
							}
							else
							{
								$product_array['wishlist']=null;
							}
							if($product->getAttributeText('brands')){
								$product_array['brand']= $product->getAttributeText('brands');
							}else{
								$product_array['brand']= null;	
							}
							
							if($product->getAttributeText('supported_models')){
								$product_array['supported_models']= $product->getAttributeText('supported_models');
							}else{
								$product_array['supported_models']= null;	
							}
							// From Rma extension
							$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
							if($product->getAttributeText('return_period')=="Flat return period"){
								$product_array['return_days']= $return_date;
							}
							
							$product_array['specialprice'] = $product->getSpecialPrice();
							$product_array['specialfromdate'] = $product->getSpecialFromDate();
							$product_array['specialtodate'] = $product->getSpecialToDate();
							
							//product image 
							if(!empty($product->getImage())){
								$product_array['image'] = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
							}else{
								$site_url = $storeManager->getStore()->getBaseUrl();
								$imageUrl = $site_url.'Customimages/product-thumb.jpg';
								$product_array['image'] = $imageUrl;
							}
							
							
							$product_array['earn_reward'] = $earnOutput->getProductPoints($product);
							
							$ratings ='';
							$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
							if(!empty($RatingOb->getCount())){
								$ratings = $RatingOb->getSum()/$RatingOb->getCount();
							}
							if(!empty($ratings)){
								$product_array['rating_count'] = $ratings;
							}else{
								$product_array['rating_count'] = null;
							}
							
							$data[] = $product_array;
							
							$i++;
					
				} 
			}
					
			
		}

		$product_array['product_count'] = $i;
		$product_array['pages_count'] = ceil($i/10);
		
		if($page_no >$product_array['pages_count']){
			$result_array = array('success' => 0,'result' => null, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array);
			die();
		}
		
		
		$end = ((10)*$page_no);
		if($end>10){
		 $start = (10*($page_no-1));
		}else{
			$start = 0;
		}
		 // echo 'start'.$start;
		 // echo 'end'.$end;
		$data_count= count($data);
		
		$dataa= array_slice($data,$start,10);
		
		$keys = array_column($dataa, 'price');
		array_multisort($keys, SORT_ASC, $dataa);	  	
		 
		if(!empty($dataa)){
			$result_array = array('success' => 1, 'result' =>$dataa, 'category_name'=>$category_name, 'product_count'=>$data_count, 'pages_count'=>$product_array['pages_count'], 'error' =>"Product assigned to this category."); 
			echo json_encode($result_array);
			die();
		}else{
			$data=array();
			//$result_array = array('success' => 0, 'result' => $data, 'error' => "The category id is not valid. Please check the category. Or there is not product assigned to this category."); 
			$result_array = array('success' => 0, 'result' => $data, 'error' => ""); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		 
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 
		echo json_encode($result_array);	
		
	}
?>