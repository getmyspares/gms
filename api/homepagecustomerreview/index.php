<?php
	// Help and Support  api.
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	//print_r($result);
	
	
	$page_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $page_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $page_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $page_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$reviewCollectionFactory = $objectManager->get('\Magento\Review\Model\ResourceModel\Review\CollectionFactory');
		$reviewsCollection = $reviewCollectionFactory->create()
				->addFieldToSelect('*')
				//->addStoreFilter($currentStoreId)
				->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
				->setDateOrder()
				->addRateVotes();
				$review_json = array();
				if ($reviewsCollection && count($reviewsCollection) > 0) {
					$configIds = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mobilepopup/homepagereview/review_ids');
					$configIds = explode(',', $configIds);
					foreach ($reviewsCollection as $review) {
							if(!empty($configIds) && !in_array($review->getId(), $configIds)) {
								continue;
						}
						$review_id = $review->getId();
						$name = $review->getNickname();
						$date = date('d/m/Y',strtotime($review->getCreatedAt()));
						$content = $review->getDetail();
						$allRatingsAvg=0;
						$countRatings = count($review->getRatingVotes());
						if ($countRatings > 0) {
							$allRatings = 0;
							foreach ($review->getRatingVotes() as $vote) {
								$allRatings = $allRatings + $vote->getPercent();
							}
							$allRatingsAvg = $allRatings / $countRatings;
							
						}
						$rating = $allRatingsAvg;
						$review_json[]=array("id"=>"$review_id","name"=>"$name","date"=>"$date","content"=>"$content","rating"=>"$rating");
					}
				
				
					$result_array = array('success' => 1, 'result' => $review_json, 'error' => 'no error');
					echo json_encode($result_array);
					die();
				} else 
				{
					
					$result_array =array('success' => 0, 'result' =>"Null",'error' => 'Popup Disabled');
					echo json_encode($result_array);
					die();
				}
		if($is_enable){
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$base_url = $storeManager->getStore()->getBaseUrl();
			$image_url = $base_url."pub/media/homepopup/mobile/".$mobile_image;
		}else{
		}
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$page_array, 'error' => $e->getMessage());
		echo json_encode($result_array);
		die();		
	} 
?>