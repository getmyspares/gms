<?php
// {"accesstoken":"xxxx","order_inc_id":"xxxx","type":"Forward"}
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	$result = $_GET;
	$ecom_response=array();
	$argumentsCount=count($result);

	if($argumentsCount < 3 || $argumentsCount > 3)
	{
		$result_array=array('success' => 0,'result' => $ecom_response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $ecom_response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $ecom_response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!isset($result['order_inc_id']) || empty($result['order_inc_id'])) {
		$result_array=array('success' => 0, 'result' => $ecom_response, 'error' => 'Invalid Order Increment Id');	
		echo json_encode($result_array);
		die;
	}

	if(!isset($result['type']) || empty($result['type'])) {
		$result_array=array('success' => 0, 'result' => $ecom_response, 'error' => 'Invalid Tracking Type');	
		echo json_encode($result_array);
		die;
	}
	
	$order_inc_id = $result['order_inc_id']; 
	$sql = "select entity_id from sales_order where increment_id='$order_inc_id'";
	$order_id = $connection->fetchOne($sql);

	if(empty($order_id))
	{
		$result_array=array('success' => 0, 'result' => $ecom_response, 'error' => 'Invalid Order Increment Id');	
		echo json_encode($result_array);	
		die;
	}

	$ordered_msg="Order has been accepted";
	$processing_msg="Seller has shipped your order";
	$shipped_msg="Your order has been picked up for delivery";
	$out_for_delivery_msg="Your order is out for delivery";
	$delivered_msg="Order has been delivered";
	
	$processing_status="0";
		$shipped_status="0";
		$out_for_delivery_status="0";
		$delivered__status="0";
		$processing_date="";
		$shipped_date="";
		$expected_date="";
		$out_for_delivery_msg_date="";
		$processing_date="";
		$delivered_date="";
	
	try{	
		/* dont know why the helper class name is small caps , but had to use it as it is as will throw error otherwise  @ritesh*/
    $shipment_helpers = $objectManager->create('OM\Rewrite\Helper\shipment');
		
		$sql = "select awb from ecomexpress_awb where orderid='$order_id'";
		$awb = $connection->fetchOne($sql);
		
		$awb_track_array=$shipment_helpers->awbTracking($awb);
		$ecom_data = $awb_track_array['data'];
		$sql = "select created_at from sales_order where entity_id='$order_id'";
		$order_date = $connection->fetchOne($sql);
		
		$currentstatus="0";
		
		
		$currentstatus="0";
		$ecom_response[] = array("id"=>"0","title"=>"Ordered","status"=>"1","info"=>$ordered_msg,"date"=>indian_date($order_date));
		$result_array = array('success' => 1,'result' =>$ecom_response,'currentstatus'=>$currentstatus);
		
		$sql = "SELECT created_at FROM `sales_shipment` where order_id=$order_id";
		$shipment_date = $connection->fetchOne($sql);
		
		
		
		if(!empty($shipment_date))
		{
			$processing_date=indian_date($shipment_date);
			$currentstatus="1";
			$processing_status=1;

		}

		$sql = "select status from sales_order where entity_id='$order_id'";
		$status_order = $connection->fetchOne($sql);
		
		if($status_order=='canceled')
		{
			$ecom_response[] =array("id"=>"5","title"=>"Cancelled","status"=>"5","info"=>'cancelled',"date"=>indian_date($order_date));
			$currentstatus='5';
		}

		if($status_order=='pending_payment')
		{
			$ecom_response[] =array("id"=>"6","title"=>"Pending Payment","status"=>"6","info"=>'Payment Pending',"date"=>indian_date($order_date));
			$currentstatus='6';
		}
		
		
		if(empty(!$ecom_data))
		{
			$expected_date = $awb_track_array['completion_status']['expected_date'];
			foreach ($ecom_data as $key => $ecom_array) {
				$status = strtolower($ecom_array['status']);
				if(strpos($status, 'shipment picked up') !== false)
				{
					$shipped_date = indian_date($ecom_array['date']);
					if($currentstatus<="1")
					{
						$currentstatus="2";
					}
		$shipped_status="1";

				}
				if(strpos($status, 'out for delivery') !== false)
				{
					$out_for_delivery_msg_date = indian_date($ecom_array['date']);
					if($currentstatus<="2")
					{
						$currentstatus="3";
					}
		$out_for_delivery_status="1";

				}
				if(strpos($status, 'shipment delivered') !== false)
				{
					$delivered_date = indian_date($ecom_array['date']);
					if($currentstatus<="3")
					{
						$currentstatus="4";
					}
					$delivered__status="1";
				}
			}	

			
		}
		if($status_order!='pending_payment' && $status_order!='canceled')
		{
			$ecom_response[] = array("id"=>"1","title"=>"Processing","status"=>$processing_status,"info"=>$processing_msg,"date"=>$processing_date);
			$ecom_response[] = array("id"=>"2","title"=>"Shipped","status"=>$shipped_status,"info"=>$shipped_msg,"date"=>$shipped_date,'expected_date'=>$expected_date);
			$ecom_response[] = array("id"=>"3","title"=>"Out For Delivery","status"=>$out_for_delivery_status,"info"=>$out_for_delivery_msg,"date"=>$out_for_delivery_msg_date,'expected_date'=>$expected_date);
			$ecom_response[] =array("id"=>"4","title"=>"Delivered","status"=>$delivered__status,"info"=>$delivered_msg,"date"=>$delivered_date);
		}
		$result_array = array('success' => 1,'result' =>$ecom_response,'currentstatus'=>$currentstatus); 
		echo json_encode($result_array);
		
	} catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' =>$ecom_response, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}

	function indian_date($datwe)
	{
		return date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime($datwe)));
		// return  date('Y-m-d H:i:s',strtotime($datwe)-19800);
	}
?>