<?php
 
 

/*
FOR Company Buyer json format

{
	"accesstoken": "1234",
	"mobile": "1234567890"
}
  


*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result); 
//die;
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$group_array=array('1','4','5');  
$null = null;
$argumentsCount=count($result);
$attempt=null; 
$user_array=null;
$attempt=null; 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
if($argumentsCount < 2 || $argumentsCount > 2) 
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}        
	
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['mobile']) || empty(@get_numeric($result['mobile'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Mobile parameter is missing'); 	
	echo json_encode($result_array);	
	die; 
}


$accesstoken=$result['accesstoken'];
$mobile=@get_numeric($result['mobile']); 
   
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}



if($mobile=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Mobile is missing'); 	
	echo json_encode($result_array);	
	die;
}
else  
{
	/* $check_mobile=$app_helpers->check_mobile_exists($mobile);
	if($check_mobile==1)
	{	
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Mobile already exists'); 	
		echo json_encode($result_array);	
		die;  
	}  */
	
	
	
}





$select = $connection->select()
		->from('otp_temp_user') 
		->where('mobile = ?', $mobile);
		 
$results = $connection->fetchAll($select);
if(!empty($results)) 
{
	
	$db_no_times=$results[0]['no_times'];
	
	if($db_no_times > 2)  
	{
		$attempt=0; 
		$user_array=array('attempt'=>$attempt); 
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Resend limit exceeds');  	
		echo json_encode($result_array);	
		
		$sql = "delete from otp_temp_user  WHERE mobile='".$mobile."'"; 
		$connection->query($sql);         	    
		
		die;  	
	}	
	
	
	$user_array=json_decode($app_helpers->update_otp($mobile));   
	$email=$user_array->email; 
	$mobile;    
	
	$user_array=array('email'=>$email,'mobile'=>$mobile,'attempt'=>$attempt); 
	$result_array=array('success' =>1, 'result' => $user_array,'error' => 'OTP has been sent. Please check mobile and email'); 	
	echo json_encode($result_array);	    
	
	$select = $connection->select()
		->from('otp_temp_user') 
		->where('mobile = ?', $mobile);
	$resultss = $connection->fetchAll($select);
	$no_times=$resultss[0]['no_times'];
	$no_times=$no_times+1; 
	
	$sql="update otp_temp_user set no_times='".$no_times."' where mobile='".$mobile."'"; 
	$connection->query($sql);	   
	
	
	die;    
	
}
else
{  
	$attempt=0; 
	$user_array=array('attempt'=>$attempt);
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Your OTP attempt is reached to maximum'); 	
	echo json_encode($result_array);	
	die;    
}	  


	
