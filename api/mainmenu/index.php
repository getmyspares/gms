<?php

	require "../security/sanitize.php";
	$header = apache_request_headers(); 
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$null = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $null , 'error' => "Input count not  matched"); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $null, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $null, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	//$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	//$custom_helpers->update_product(); 
	 
	try
	{
		$categoryCollection = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
		$categories = $categoryCollection->create();
		$categories->addAttributeToSelect('*');

		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categories = $categoryHelper->getStoreCategories();
	
		$menu_cat = array();	 
		$cat_subcat1 = array();	 
		$cat_subcat2 = array();	 
	 
		$i = 0;
		
		
		$cat_id='';
		foreach ($categories as $category) {  
			
			//--- Get level 1    
			$cat_id=$category->getId(); 
			$subcategory = $objectManager->create('Magento\Catalog\Model\Category')->load($cat_id);
			$subcats = $subcategory->getChildrenCategories();
			
			$subArray=array();
			if(!empty($subcats))
			{
				foreach ($subcats as $subcat) {
					//print_r($subcat->getData());
					$sub_cat=$subcat->getId();
					
					$sub_sub_category = $objectManager->create('Magento\Catalog\Model\Category')->load($sub_cat);
					$sub_sub_cats = $sub_sub_category->getChildrenCategories();
					$sub_sub_Array=array();
					
					if(!empty($sub_sub_cats))
					{
						foreach ($sub_sub_cats as $sub_sub_cat) {
							
							$sub_sub_cat_id=$sub_sub_cat->getId();
							
							$sub_sub_Array[]=array(
								'id'=>$sub_sub_cat_id,
								'name'=>$sub_sub_cat->getName(), 
								'icon'=>$api_helpers->get_category_icon($category->getId())
							); 
						}	 
					}
					
					
					
					$subArray[]=array(
						'id'=>$subcat->getId(),
						'name'=>$subcat->getName(),
						'icon'=>$api_helpers->get_category_icon($category->getId()),
						'sub_cat'=>$sub_sub_Array 
					);
				}	
			}
			
			
			$mainArray[]=array(
				'id'=>$cat_id,
				'name'=>$category->getName(),
				'icon'=>$api_helpers->get_category_icon($category->getId()),
				'sub_cat'=>$subArray 
			);
				
			$y = 0;
			$i++;
		}
		
		//$menu_cat_array = $menu_cat;
		$menu_cat_array = $mainArray;   
		//print_r($menu_cat);
		$check=$api_helpers->check_api();  
		$check=1; 
	
		if(!empty($menu_cat_array)){
			
			if($check==0)  
			{
				$menu_cat_array=array();
				$result_array = array('success' => 1,'result' => $menu_cat_array, 'error' => 'Main menu data.');
			}
			else
			{	
				$result_array = array('success' => 1,'result' => $menu_cat_array, 'error' => 'Main menu data.');
			}
		}else{ 
			$result_array =array('success' => 0, 'result' =>$null,'error' => 'There is not assigned to the main menu. Please check again after assigning the categories.'); 
		}
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$null, 'error' => $e->getMessage()); 	
	} 
	 
	echo json_encode($result_array);
?>