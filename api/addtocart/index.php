<?php
	
	
	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/add_to_cart_db/
	//Input 
	///*{"accesstoken": "1234","product_id":"1399","user_id":"269","qty":"2"}*/
	
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = sanitizedJsonPayload();
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	
	$response = null;	
	$argumentsCount=count($result);
	$user_array=null;
	if($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	
	if(!empty(@get_numeric($result['qty']))){
		$qty = @get_numeric($result['qty']);
	}else{
		$qty = '';	
	}
	
	
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$product_id = @get_numeric($result['product_id']);
		$check_product=$api_helpers->check_product_exist($product_id); 
		if($check_product==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
			echo json_encode($result_array);	
			die; 
		}
	}
	
	if($qty ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Quantity is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		if(!is_numeric($qty))
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Quantity must have numbers'); 	
			echo json_encode($result_array);	
			die;
		}
		else if($qty==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Quantity 0 not allowed'); 	
			echo json_encode($result_array);	
			die;
		}	 
	}
	
	
	
	//$token=$api_helpers->get_token(); 
	//$quote_id=$api_helpers->get_quote_id($token,$user_id);
	//$quote_id=json_decode($quote_id);
	     
	//$api_helpers->addtocart_test($user_id,$product_id,$qty,$quote_id);
	/*
	$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
	$allItems=$quote->getAllVisibleItems();
	
	
	$total_qty=0;
	if(!empty($allItems))
	{	
		foreach ($allItems as $item) {
			
			
			$qty=$item->getQty();
			
			$itemId = $item->getItemId();
			$ProductId = $item->getProductId();
			$total_qty=$total_qty+$qty;
			
		}	
	}   
	
	$total_qty;
	*/
	
	 
	
	try{
		
		$checkArray=$api_helpers->addtocart_test_old($user_id,$product_id,$qty);  
		//print_r($checkArray);
		$check=$checkArray['status'];
		$cart_array=$checkArray['cart_array'];
		
		$user_array[]=array('user_id'=>$user_id);   
		$user_array[]=$cart_array;
		
		//print_r($user_array);
		//die;
		
		 
		
		if($check=='minsalable') 
		{
			$error=$checkArray['error'];
			$result_array = array('success' => 0,'result' => $user_array,'error' =>"$error"); 
			echo json_encode($result_array);	
			die; 
		}
		
		if($check=='out') 
		{
			$result_array = array('success' => 0,'result' => $user_array,'error' =>"Product out of stock"); 
			echo json_encode($result_array);	
			die; 
		}

		else if($check=='qty')
		{
			$result_array = array('success' => 0,'result' => $user_array,'error' =>"Requested Qty is exceeds");  
			echo json_encode($result_array);	
			die; 
		}	 else  if ($check=='nonshippable')
		{
			$result_array = array('success' => 0,'result' => $user_array,'error' =>"Item unable to  be shipped");  
			echo json_encode($result_array);	
			die; 
		}
		else if($check=='insert')
		{	
			
			$result_array = array('success' => 1,'result' => $user_array,'error' =>"Product added to cart."); 
			echo json_encode($result_array);
		}
		else if($check=='update')  
		{	
			$result_array = array('success' => 1,'result' => $user_array,'error' =>"Product already in the cart");   
			echo json_encode($result_array);  
		}  
		
		if($check=="insert" || $check=="update")
		{
			$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
			$wishlistAdd = $wishList->create()->loadByCustomerId($user_id, true);
			

			$_in_wishlist = false;
			foreach ($wishlistAdd->getItemCollection() as $_wishlist_item){
				if($product_id == $_wishlist_item->getProduct()->getId()){
					$_in_wishlist = true;
				} 
			}
			
			if($_in_wishlist){
				
				$items = $wishlistAdd->getItemCollection();
				foreach ($items as $item) 
				{
					if ($item->getProductId() == $product_id) 
					{
						$item->delete();
						$wishlistAdd->save();   
					}
				}

			}	 
		}	
	
	
		
		
		die();
		

	} catch(Exception $e){		

		$result_array = array('success' => 0,'result' => null, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
	
?>