<?php

require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);
 
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;



$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$argumentsCount=count($result);

if($argumentsCount < 3 || $argumentsCount > 3) 
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	
$accesstoken=$result['accesstoken'];
$email=$result['email'];
$password = base64_decode($result['password']);
$user_array=null;    
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Email is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
		echo json_encode($result_array);	
		die;
	}
} 
	
if($password=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Password is missing'); 	
	echo json_encode($result_array);	
	die;
}	 

$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
$CustomerModel->setWebsiteId(1); 
$CustomerModel->loadByEmail($email); 
$userId = $CustomerModel->getId();
if($userId=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Email not exist'); 	
	echo json_encode($result_array);	
	die;
}	
else
{
	
	$state = $objectManager->get('\Magento\Framework\App\State');
	$state->setAreaCode('frontend');
	try 
	{
		$customerId = $userId; 
		$customerRepositoryInterface = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
		$customerRegistry = $objectManager->get('\Magento\Customer\Model\CustomerRegistry');
		$encryptor = $objectManager->get('\Magento\Framework\Encryption\EncryptorInterface');
		$customer = $customerRepositoryInterface->getById($customerId); 
		$customerSecure = $customerRegistry->retrieveSecureData($customerId); 
		$customerSecure->setRpToken(null);  
		$customerSecure->setRpTokenCreatedAt(null);
		$customerSecure->setPasswordHash($encryptor->getHash($password, true)); 
		$customerRepositoryInterface->save($customer);    
		$sql = "delete from otp_temp_user  WHERE email='".$email."' ";
		$connection->query($sql);            	    
		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Password has been changed'); 	
		echo json_encode($result_array);	  
		die;    
	} 
	catch(Exception $e) 
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Error in Reset Password'); 	
		echo json_encode($result_array);	 
		die; 
	}    
}

?>



