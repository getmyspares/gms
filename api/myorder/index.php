<?php

require "../security/sanitize.php";
$result=$_GET;  

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$argumentsCount=count($result); 
$user_array=null; 
 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
$store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getStore();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

if($argumentsCount < 3 || $argumentsCount > 3)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}  

if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
   

$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);

$page_no=1;

if(!empty(@get_numeric($result['page_no'])))
{
	$page_no=@get_numeric($result['page_no']);  
}	

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 


if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

	
if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check=$api_helpers->check_user_exists($user_id);
	if($check==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;
	}

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
}

 
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$orderCollection = $objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Collection'); 
$orderCollection->addAttributeToFilter('customer_id',$user_id)->load();
if(!empty($orderCollection->getData()))
{	
$orderArray=$orderCollection->getData();


	$keys = array_column($orderArray, 'entity_id');

	array_multisort($keys, SORT_DESC, $orderArray);

	$orderArray;
	
	
	$need_to_show=5;
	$product_count = count($orderArray); 
	$pages_count = ceil($product_count/$need_to_show); 
	
	if($page_no >$product_count){
		$result_array = array('success' => 0,'result' => null,'search'=>$search_section, 'error' => "Page number exceed its limit."); 
		echo json_encode($result_array);
		die();
	}
	
	$end = (($need_to_show)*$page_no);
	if($end>$need_to_show){
	 $start = ($need_to_show*($page_no-1));
	}else{
		$start = 0;
	}
	$data_count= count($orderArray);
	$orderArray= array_slice($orderArray,$start,$need_to_show);
	foreach($orderArray as $orders)
	{
		$order_id=$orders['entity_id'];
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  
		$rma_helper = $objectManager->create('OM\Rma\Helper\Data');  
		$created_at=$order->getCreatedAt();
		$order_inc_id=$order->getIncrementId();
		$productIdArray=array();
		$return_valid = $rma_helper->isReturnAvailable($order_inc_id)==true? "Yes":"No";
		$created_at=date("d M,Y", strtotime($created_at));   
		$status=  str_replace("_"," " ,ucwords($order->getStatus())) ; 

		foreach ($order->getAllItems() as $item)
		{
			$product_id=$item->getProductId();
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
			
			if(!empty($product->getImage()))
			{
				$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
				$image = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
			} 
			else
			{
				$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
				$site_url = $storeManager->getStore()->getBaseUrl();
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
				$image = $imageUrl;
			} 
			
			$productIdArray[]=array(
				'product_id'=>$item->getProductId(),
				'item_id'=>$item->getId(),
				'item_qty'=>(int)$item->getQtyOrdered(),
				'name'=>$product->getName(),	
				'sku'=>$product->getSku(),
				'image'=>$image,	
			);
		}   
		
		$mainArray[]=array(
			'order_id'=>$order_id,
			'order_inc_id'=>$order_inc_id,
			'created_at'=>$created_at,
			'status'=>$status, 
			'products'=>$productIdArray,
			'return_valid'=>$return_valid 
			// 'return_valid'=>"Yes" 
		);
	}
	
	$user_array=$mainArray;
	$result_array=array('success' => 1,'result' => $user_array,'order_count'=>$data_count, 'pages_count'=>$pages_count, 'error' => 'Order found'); 	
	echo json_encode($result_array);	 
	die; 	
}
else
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'No Order found'); 	
	echo json_encode($result_array);	
	die;	
}	

?>



