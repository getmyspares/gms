<?php
require "../security/sanitize.php";

	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null; 
	$data = null;
	$argumentsCount=count($result);
	
	$itemsPerPage=10;

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	
	
	
	
	$product_arrayy=array();
	
	
	



	try 
	{
		//$categoryId = $_GET['cat_id'];
	 
									 
		$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();			
		
		$collectionfirst =$objectManager->create('Magento\Catalog\Model\Product')->getCollection();            
		$collectionfirst
			->addMinimalPrice()
			->addFinalPrice()
			->addTaxPercents()
			->addAttributeToSelect('special_from_date')
			->addAttributeToFilter('status',1)
			->addAttributeToSelect('special_to_date')
			->addAttributeToFilter('special_price', ['neq' => ''])
			->addAttributeToFilter('is_saleable', 1, 'left')
			->setPageSize(5);
			
		$collectionfirst->getSelect()->where('price_index.final_price < price_index.price');
		$collectionfirst->load();	
		
		$excitingDeals = $collectionfirst->getData();
		
		
		if(!empty($excitingDeals))
		{
		
			foreach ($excitingDeals as $row) 
			{
				
				$product_id=$row['entity_id'];
				
				//$product_special_price=$api_helpers->check_product_special_price($product_id);
				//if($product_special_price==1)
				//{	
					$finalArray[]=$product_id;
				//} 
			} 
		} 
		
		
		
		
		
		if(empty($finalArray))
		{
			$data=array();
			$result_array = array('success' => 1,'result' => $data,'block_title'=>'Exciting Deals', 'error' => 'No product found');  
			echo json_encode($result_array);	
			die; 
		}	
		
		$product_arrayy=array_unique($finalArray, SORT_REGULAR);
		//print_r($product_arrayy); 
		/*
		$product_count = count($product_arrayy); 
		$pages_count = ceil($product_count/$itemsPerPage); 
		 
		
		
		 
		if($page_no > $pages_count) 
		{
			$result_array = array('success' => 0,'result' => null, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array); 
			die();
		}
		
		 
		
		
		$offset = abs($itemsPerPage * ($page_no - 1)); 
		
		$dataa= array_slice($product_arrayy,$offset,$itemsPerPage);
		
		*/
		
		$categoryId=61;
		
		foreach($product_arrayy as $row)
		{
			$product_id=$row;	 
			$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
			
			if(!empty($product->getImage())){
				$imageUrl = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('300','300')->getUrl();
			}else{ 
				$site_url = $storeManager->getStore()->getBaseUrl();
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
			} 
			
			//$imageUrl=$api_helpers->get_product_thumb($product_id);
			 
			/* if(!empty($product->getImage())){
				$imageUrl = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
			}else{
				$site_url = $storeManager->getStore()->getBaseUrl();
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
			}   */ 
			 
			$sale_percentage='';
			$price=$product->getPrice();
			$specialprice=number_format($product->getSpecialPrice(), 2, '.', '');
			
			
			
			$specialfromdate = $product->getSpecialFromDate(); 
			$specialtodate = $product->getSpecialToDate();
			
			 
		
			$ratings ='';
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
			if(!empty($RatingOb->getCount())){
				$ratings = $RatingOb->getSum()/$RatingOb->getCount(); 
			}
			if(!empty($ratings)){
				$rating_count = $ratings;
			}else{
				$rating_count = null;
			}
			
			 
			$sale = round((($price-$specialprice)/$price)*100); 
			$sale_percentage = $sale.' %'.'Off';   
			
			/* $product_array[]=array(
				'id'=>$product->getId(),	
				'name'=>$product->getName(),	
				'sku'=>$product->getSku(),	
				'price'=>number_format($product->getPrice(), 2, '.', ''),
				'image'=>$imageUrl,
				'specialprice'=>$specialprice,
				'specialfromdate'=>$specialfromdate,
				'specialtodate'=>$specialtodate,
				'sale_percentage'=>$sale_percentage,
				'rating_count'=>$rating_count
			 
			); */
			 
			
			$product_array[]=array(
				'url_type'=>'product',	
				'product_id'=>$product->getId(),	
				'product_url'=>null, 	
				'product_img'=>$imageUrl, 	
				'product_price'=>number_format($product->getPrice(), 2, '.', ''),
				'specialprice'=>$specialprice, 	
				'specialPriceFromDate'=>$specialfromdate, 	
				'specialPriceToDate'=>$specialtodate, 	
				'sale_percentage'=>$sale_percentage	
				
			
			);
			
			
			
			 
			
		}	
		
		
		//print_r($product_array);
		//die;
		 
		if(!empty($product_arrayy)){ 
			$result_array = array('success' => 1, 'result' => $product_array,'category_id'=> $categoryId,'block_title'=>'Exciting Deals','error' =>"Exciting deals"); 
			echo json_encode($result_array); 
		}else{ 
			$data=array();
			$result_array = array('success' => 1, 'result' => $data,'block_title'=>'Exciting Deals','error' => "Product not found");  
			echo json_encode($result_array);   
			die(); 
		} 
		  
		
		
		
		
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
	}
?>