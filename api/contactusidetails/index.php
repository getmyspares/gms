<?php
// {"accesstoken":"xxxx","order_inc_id":"xxxx","type":"Forward"}
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	$result = $_GET;
	$contact_info=array();
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $contact_info , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	if(!empty($result['accesstoken'])){
    $accesstoken = $result['accesstoken'];
    $helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
    $checkToken=$helpers->checkToken($accesstoken); 
    if($checkToken==0)
    {
      $result_array=array('success' => 0, 'result' => $contact_info, 'error' => 'Session expired');	
      echo json_encode($result_array);	
      die;
    }
    
    $contactUsHelper = $objectManager->create('OM\Rewrite\Helper\ContactUs');
    $contact_info = $contactUsHelper->getContactUsArray();

    $result_array = array('success' => 1,'result' =>$contact_info, 'error' => ""); 
    echo json_encode($result_array);

  }else{

    $result_array=array('success' => 0, 'result' => $contact_info, 'error' => 'Token is missing'); 	
    echo json_encode($result_array);	
    die;
  }

  
?>