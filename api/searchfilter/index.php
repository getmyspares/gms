<?php
	// Catgeory's product api. 
	//https://www.dev.idsil.com/design/api/brandfilter/?brand_id=4&accesstoken=1234&page_no=1&brand_filter=47,48&rating_filter=1,2,3&price_from=1000&price_to=1100 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	
	$result = $_GET;
	// print_r($result);
	
	$product_array = null; 
	$data = null;
	$argumentsCount=count($result);
	$filter=0;
	$user_array=null;
	/* if($argumentsCount < 3 || $argumentsCount > 9)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{ 
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	/* if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	} */
	
	
	if(!empty($result['name'])){
		$key = $result['name'];
	}else{
		$key = '';	
	}
	
	if(!empty($result['model'])){
		$model_filter = $result['model'];
		
	}else{
		$model_filter = '';	
	}
	
	// Brand Filter
	if(!empty($result['brand_filter'])){
		$brand_filter = $result['brand_filter'];
		
	}else{
		$brand_filter = '';	
	}
	
	// Rating Filter
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
		
	}else{
		$rating_filter = '';	
	}
	
	
	// Price Filter
	if(!empty($result['price_from'])){
		$price_from = $result['price_from'];
		
	}else{
		$price_from = '';	
	}
	
	if(!empty($result['price_to'])){
		$price_to = $result['price_to'];
		
	}else{
		$price_to = '';	
	}
	
	$sort=0;
	if(!empty($result['sort'])){
		$sort = $result['sort'];
	}
	
	$search_section=0;
	if(!empty($result['search'])){
		$search_section = $result['search'];
	}   
	
	
	if($key=='')
	{
		$result_array=array('success' => 0,'result' => $user_array,'search'=>$search_section, 'error' => 'Search key is blank'); 	
		echo json_encode($result_array);	
		die;	
	}	
	if(!empty(@get_numeric($result['user_id'])))  
	{ 
		$check_user=$api_helpers->check_user_exists(@get_numeric($result['user_id']));
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array,'search'=>$search_section, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	  
		} 	
	} 
	
	$user_id=0;
	if(!empty(@get_numeric($result['user_id'])))  
	{ 
		$user_id=@get_numeric($result['user_id']);
	}
	//$api_helpers->check_product_special_price('1433');
	
	try{
		
		
				
		$defaultArray=array();
		$mainCatArray=array();
		$productPriceArray=array();
		
		
		$reviewFactory = $objectManager->create('Magento\Review\Model\Review');
		$storeId = $storeManager->getStore()->getId();
		
		
		
		$now = date('Y-m-d H:i:s');
		
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
		
		
		
		
		$collectionfirst=$productCollection->create();        
		$collectionfirst->addAttributeToSelect('entity_id')
		->addFieldToFilter('status',1)
		->addAttributeToFilter(
			  [
			   ['attribute' => 'name', 'like' => '%'.$key.'%']
			  ])
		->load();	  
		
		$excitingDeals = $collectionfirst->getData();
		
		
		
		
		
		
		$count_product=count($excitingDeals);
		if($count_product==0)
		{
			$array=array();
			$result_array=array('success' => 0, 'result' => $array, 'error' => 'Product not found'); 	
			echo json_encode($result_array);	
			die; 
		}		
		
		
		  
		
		
		$prod_str='';
		
		$count_1=0;
		$count_2=0;
		$count_3=0;
		$count_4=0;
		$count_5=0; 

		
		
		
	
		
		$collection = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory')->create();//get current category
		$collection->addAttributeToFilter('level', array('eq' => 2));
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		
		
		
		$prod_str='';
		foreach ($excitingDeals as $product) 
		{ 
			
			
			$prod_str .=$product['entity_id'].',';
			
		}	
			
			
			
		
		$product_list=substr($prod_str,0,-1);	
		
		$sql_group="SELECT rating_summary,count(*) as 'total_count' FROM `review_entity_summary` where store_id=1 and entity_pk_value in (".$product_list.") group by rating_summary"; 
		$resultsss_group = $connection->fetchAll($sql_group);		
		
		if(!empty($resultsss_group)) 
		{
			foreach($resultsss_group as $rating_star)
			{ 
				
				if($rating_star['rating_summary'] >= 20 && $rating_star['rating_summary'] < 40)
				{
					$count_1=$rating_star['total_count'];
				}
				
				if($rating_star['rating_summary'] >= 40 && $rating_star['rating_summary'] < 60)
				{
					$count_2=$rating_star['total_count'];
				}
				if($rating_star['rating_summary'] >= 60 && $rating_star['rating_summary'] < 80)
				{
					$count_3=$rating_star['total_count'];
				}
				if($rating_star['rating_summary'] >= 80 && $rating_star['rating_summary'] < 100)
				{
					$count_4=$rating_star['total_count'];  
				}
				if($rating_star['rating_summary']==100)
				{
					$count_5=$rating_star['total_count'];
				}
				
			}	
		}	
		
		
		
		
		
		$star_1=$count_1;
		$star_2=$count_2;
		$star_3=$count_3;
		$star_4=$count_4;
		$star_5=$count_5;
		
		
		
		 
		
		
		
		
		


		/*
		
		$min_price=0; 
		$max_price=0;
		
		
		$default_price_min=array();  
		$default_price_max=array();  
		
		if(!empty($productPriceArray)){
			$default_price_min = min($productPriceArray);
			$default_price_max = max($productPriceArray);
			
			
			$default_price_min=number_format($default_price_min, 2, '.', '');
			$default_price_max=number_format($default_price_max, 2, '.', '');
			
			
			$min_price = min($productPriceArray);
			$max_price = max($productPriceArray);
		
			$min_price=number_format($min_price, 2, '.', '');
			$max_price=number_format($max_price, 2, '.', ''); 
		
		}
		*/
		 
		
		
		//$parent_model_array['category'] =$mainCatArray;
		//$parent_model_array['price'] = array("min_price"=>$min_price,"max_price"=>$max_price);
		//$parent_model_array['default_price'] = array("min_price"=>$default_price_min,"max_price"=>$default_price_max);
		$parent_model_array['stars'] = array("1 Star"=>$star_1,"2 Star"=>$star_2,"3 Star"=>$star_3,"4 Star"=>$star_4,"5 Star"=>$star_5); 
		
		
		$result_array=array('success' => 1, 'result' => $parent_model_array,'error' => 'Product found'); 	
		echo json_encode($result_array); 	  
		die;	 
		
		
		
	
	}catch(Exception $e){
		 
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 
		echo json_encode($result_array);	
		
	}
?>