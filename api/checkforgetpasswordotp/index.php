<?php
 require "../security/sanitize.php";

/*
FOR Company Buyer json format

{"accesstoken": "1234","email":"1234567890","otp_pin":"1111"} 
 


*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.

$result = sanitizedJsonPayload();
if(!is_array($result) || (is_array($result) && empty($result))) {
	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
	die;
}
//die;
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();




$group_array=array('1','2','5');
$null = null;
$argumentsCount=count($result);

$user_array=null;

if($argumentsCount < 3 || $argumentsCount > 3)
{ 
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}        
	
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['email']) || empty($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'email parameter is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['otp_pin']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Otp Pin parameter is missing'); 	
	echo json_encode($result_array);	
	die; 
}


$accesstoken=$result['accesstoken'];
$email=$result['email'];
$otp_pin=$result['otp_pin'];

$str_len=strlen($otp_pin);   
 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

 
if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Mobile is missing'); 	
	echo json_encode($result_array);	
	die;
}


if($str_len!=4)
{
	
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Otp Pin length is not correct'); 	
	echo json_encode($result_array);	
	die;	
}	



$select = $connection->select()
		  ->from('otp_temp_user') 
		  ->where('email = ?', $email)
		  ->where('pin_code = ?', $otp_pin);
		 
$results = $connection->fetchAll($select);
if(!$results)
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Otp Pin not matched'); 	
	echo json_encode($result_array);	
	die;	
}	
else
{
	
	$email=$results[0]['email']; 
	   
	//$sql = "delete from otp_temp_user  WHERE email='".$email."' ";
	//$connection->query($sql);         	    
	 
	   
	$user_array=array('email'=>$email);    
	$result_array=array('success' => 1, 'result' => $user_array,'error' => 'OTP matched Successfully'); 	
	echo json_encode($result_array);	 
	die;	 

}	
