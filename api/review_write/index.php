<?php
	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/review_write/
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	// $result = $_GET;
	$result = sanitizedJsonPayload();
	/*{"accesstoken": "1234","product_id":"1399","customer_id":"269","nickname":"Testing","title":"text of content","detail":"This is the detail."}
	
	
		$productId=5;
		$customerId=13; //for Guest user $customerId=Null;
		$customerNickName='STACK EXCHANGE';
		$reviewTitle='STACK EXCHANGE';
		$reviewDetail='STACK EXCHANGE';
		$StoreId=1;
		$title='STACK EXCHANGE';
	
	
	
	*/
	
	$response = null;
	$argumentsCount=count($result);

	if($argumentsCount < 6 || $argumentsCount > 7)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// check if product id is present or not
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$product_id = @get_numeric($result['product_id']);
	}
	
	// check customer id is present or not. Is null if not present for guest user.
	
	if(!empty(@get_numeric($result['customer_id']))){
		$customer_id = @get_numeric($result['customer_id']);
	}else{
		$customer_id = '';	
	}

	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
	if($customer_id ==''){
		$customer_id = null;
	}else{
		$customer_id = @get_numeric($result['customer_id']);
		$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	 
			die;
		}
	}
	
	
	// check nickname 
	if(!empty($result['nickname'])){
		$nickname = $result['nickname'];
	}else{
		$nickname = '';	
	}
	
	if($nickname ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Nickname is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$nickname = $result['nickname'];
	}
	
	// check title 
	if(!empty($result['title'])){
		$title = $result['title'];
	}else{
		$title = '';	
	}
	
	if($title ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Title is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$title = $result['title'];
	}
	
	// check detail 
	if(!empty($result['detail'])){
		$detail = $result['detail'];
	}else{
		$detail = '';	
	}
	
	if($detail ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Detail is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$detail = $result['detail'];
	}
	
	
	// check rating value 
	if(!empty($result['rating'])){
		$rating = $result['rating'];
	}else{
		$rating = '';	
	}
	
	if($rating ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Rating is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$rating = $result['rating'];
	}
	


	try
	{
		$storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$StoreId       = $storeManager->getStore()->getStoreId(); 
		$storeName     = $storeManager->getStore()->getName();
		
		$productId = $product_id;
		$customerId = $customer_id; //for Guest user $customerId=Null;
		$customerNickName = $nickname;
		$reviewTitle = $title;
		$reviewDetail = $detail;

		$_review = $objectManager->get("Magento\Review\Model\Review")
		->setEntityPkValue($productId)    //product Id
		->setStatusId(\Magento\Review\Model\Review::STATUS_PENDING)// pending/approved
		->setTitle($reviewTitle)
		->setDetail($reviewDetail)
		->setEntityId(1)
		->setStoreId($StoreId)
		->setStores(1)
		->setCustomerId($customerId)//get dynamically here 
		->setNickname($customerNickName)
		->save();

		/* 
		
		// echo "Review Has been saved ";
		// echo "/////FOR SAVING RATING //////////////////////////////////////";
		 $_ratingOptions = array(
			 1 => array(1 => 1,  2 => 2,  3 => 3,  4 => 4,  5 => 5),   //quality
			 2 => array(1 => 6,  2 => 7,  3 => 8,  4 => 9,  5 => 10),  //value
			 3 => array(1 => 11, 2 => 12, 3 => 13, 4 => 14, 5 => 15),  //price 
			 4 => array(1 => 16, 2 => 17, 3 => 18, 4 => 19, 5 => 20)   //rating
		);

		//Lets Assume User Chooses Rating based on Rating Attributes called(quality,value,price,rating)
 		$ratingOptions = array(
					'1' => '1',
					'2' => '7',
					'3' => '13',
					'4' => '19'
		);	

		*/
		
		if($rating==1){
			$rating_value = 16;
		}else if($rating==2){
			$rating_value = 17;
		}else if($rating==3){
			$rating_value = 18;
		}else if($rating==4){
			$rating_value = 19;
		}else if($rating==5){
			$rating_value = 20;
		}
		
		
		$ratingOptions = array(
					'4' => $rating_value
		);      

		foreach ($ratingOptions as $ratingId => $optionIds) 
		{     
			   $objectManager->get("Magento\Review\Model\Rating")
							 ->setRatingId($ratingId)
							 ->setReviewId($_review->getId())
							 ->addOptionVote($optionIds, $productId);

		}
		// echo  "Latest REVIEW ID ===".$_review->getId()."</br>";     
		$response = $_review->aggregate();
		// echo "Rating has been saved submitted  successfully";

		// print_r($result);
		// die();

		// logic ends here
		
		$result_array = array('success' => 1,'error' =>"Rating has been saved submitted  successfully"); 
		echo json_encode($result_array);
		die();
		

	}catch(Exception $e){		

		$result_array = array('success' => 0,'result' => $response, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
?>