<?php
	require "../security/sanitize.php";
	// Catgeory's product api. 
	//https://www.dev.idsil.com/design/api/brand/?brand_id=4&accesstoken=1234&page_no=1&brand_filter=47,48&rating_filter=1,2,3&price_from=1000&price_to=1100 

	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	// print_r($result);
	
	$product_array = null; 
	$data = null;
	$argumentsCount=count($result);
	$filter=0;
	$user_array=null;
	/* if($argumentsCount < 3 || $argumentsCount > 9)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{ 
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!!isset($result['brand_id']) || empty(@get_numeric($result['brand_id']))){
		$brandId = $result['brand_id'];
	}else{
		$brandId = '';	
	} 

	if($brandId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Brand id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$brandId = $result['brand_id'];
	} 
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	} 
	
	
	
	
	
	
	
	if(!empty($result['key'])){
		$key = $result['key'];
	}else{
		$key = '';	
	}
	
	if(!empty($result['model'])){
		$model_filter = $result['model'];
		
	}else{
		$model_filter = '';	
	}
	
	// Brand Filter
	if(!empty($result['brand_filter'])){
		$brand_filter = $result['brand_filter'];
		
	}else{
		$brand_filter = '';	
	}
	
	// Rating Filter
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
		
	}else{
		$rating_filter = '';	
	}
	
	
	// Price Filter
	if(!empty($result['price_from'])){
		$price_from = $result['price_from'];
		
	}else{
		$price_from = '';	
	}
	
	if(!empty($result['price_to'])){
		$price_to = $result['price_to'];
		
	}else{
		$price_to = '';	
	}
	
	$sort=0;
	if(!empty($result['sort'])){
		$sort = $result['sort'];
	}
	
	$search_section=0;
	if(!empty($result['search'])){
		$search_section = $result['search'];
	}   
	
	$category_filter = '';	 
	if(!empty($result['category_filter'])){
		$category_filter = $result['category_filter'];
	}
	
	
	if(!empty($category_filter))
	{
		$filter_category = explode(",",$category_filter);
	}
	
	
	if(!empty($rating_filter))
	{
		$filter_ratings = (explode(",",$rating_filter));
	} 
	
	
	if(!empty(@get_numeric($result['user_id'])))  
	{ 
		$check_user=$api_helpers->check_user_exists(@get_numeric($result['user_id']));
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array,'search'=>$search_section, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	  
		} 
		
		$authentication_response = $api_helpers->authenticateUser(@get_numeric($result['user_id']),$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	} 
	
	$user_id=0;
	if(!empty(@get_numeric($result['user_id'])))  
	{ 
		$user_id=@get_numeric($result['user_id']);
	}
	//$api_helpers->check_product_special_price('1433');
	
	try{
		
		
				
		$defaultArray=array();
		$mainCatArray=array();
		$categoryproductArray=array();
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		
		$now = date('Y-m-d H:i:s');
		$collectionfirst= $objectManager->create('Magento\Catalog\Model\Product')->getCollection();            
		$collectionfirst->addAttributeToSelect('brands')
		->addAttributeToSelect('entity_id')
		->addAttributeToFilter('brands', ['eq' => $brandId])
		->addFinalPrice(); 
		
		$brand_name=$api_helpers->get_brand_by_id($brandId); 
		
		if($category_filter!='') 
		{
			
			
			$catalog_ids[] = $category_filter;
			$subCatArray=array(); 
			
			$select = $connection->select()
				  ->from('catalog_category_entity') 
				  ->where('parent_id= ?',$category_filter); 
			$result = $connection->fetchAll($select);
			$cat_idss='';
			$cat_ids='';
			
			if(!empty($result))
			{
				foreach($result as $row)
				{
					$catalog_ids[] = $row['entity_id'];
					
					$select_sub = $connection->select()
						->from('catalog_category_entity') 
						->where('parent_id= ?',$row['entity_id']);
					$result_sub = $connection->fetchAll($select_sub);
					foreach($result_sub as $row_sub)
					{
						$catalog_ids[] = $row_sub['entity_id'];
					}
				}		
			
				
			} 
			
			//print_r($catalog_ids); 
			 
			 
			$collectionfirst->addCategoriesFilter(array('in' => $catalog_ids));
			
			
			
			
		}
		
		if(!empty($price_from))
		{	
			$collectionfirst->getSelect()->where("price_index.final_price >=".$price_from)
                  ->where("price_index.final_price <=".$price_to);   	
		}
		
		if(!empty($rating_filter))  
		{
			
			if($rating_filter==1)
			{
				$rating_1=20;	
				$rating_2=40;	
			}
			else if($rating_filter==2)
			{
				$rating_1=40;	
				$rating_2=60;	
			}	
			else if($rating_filter==3)
			{
				$rating_1=60;	
				$rating_2=80;	
			}	
			else if($rating_filter==4)
			{
				$rating_1=80;	
				$rating_2=100;	
			}	
			else if($rating_filter==5)
			{
				$rating=100;	
			}  	
			
			$collectionfirst->getSelect()->joinLeft(array('rova'=> 'review_entity_summary'),'e.entity_id =rova.entity_pk_value', array("rating_summary" => 'rating_summary'))->group('e.entity_id');
			 
			if($rating_filter!=5)
			{	
				$collectionfirst->getSelect()->where("rova.rating_summary >= ".$rating_1."")->where("rova.rating_summary < ".$rating_2."")->where("rova.store_id=1"); 	
			}
			else
			{
				$collectionfirst->getSelect()->where("rova.rating_summary =".$rating)->where("rova.store_id=1");	
			}	    
		}	
		
		
		$need_to_show=10;  
		$product_count=$collectionfirst->count();
		$pages_count = ceil($product_count/$need_to_show);  
		
		if($product_count==0)
		{
			$user_array=array();
			$result_array=array('success' => 0,'result' => $user_array,'brand_name'=>$brand_name, 'error' => 'No Product Found'); 	
			echo json_encode($result_array);	
			die;		
		}	
		
		
		// Get products
		
		
		if($page_no >$pages_count) 
		{
			$result_array = array('success' => 0,'result' => null,'search'=>$search_section, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array);
			die();
		}
		
		$collectionfirst= $objectManager->create('Magento\Catalog\Model\Product')->getCollection();            
		$collectionfirst->addAttributeToSelect('brands')
		->addAttributeToSelect('entity_id')
		->addAttributeToFilter('brands', ['eq' => $brandId])
		->addFinalPrice()
		->setPageSize($need_to_show)  
		->setCurPage($page_no);  
		
		 
		if($category_filter!='') 
		{
			
			
			$catalog_ids[] = $category_filter;
			$subCatArray=array(); 
			
			$select = $connection->select()
				  ->from('catalog_category_entity') 
				  ->where('parent_id= ?',$category_filter); 
			$result = $connection->fetchAll($select);
			$cat_idss='';
			$cat_ids='';
			
			if(!empty($result))
			{
				foreach($result as $row)
				{
					$catalog_ids[] = $row['entity_id'];
					
					$select_sub = $connection->select()
						->from('catalog_category_entity') 
						->where('parent_id= ?',$row['entity_id']);
					$result_sub = $connection->fetchAll($select_sub);
					foreach($result_sub as $row_sub)
					{
						$catalog_ids[] = $row_sub['entity_id'];
					}
				}		
			
				
			} 
			
			//print_r($catalog_ids); 
			 
			 
			$collectionfirst->addCategoriesFilter(array('in' => $catalog_ids));
			
			
			
			
		}
		
		if(!empty($price_from))
		{	
			$collectionfirst->getSelect()->where("price_index.final_price >=".$price_from)
                  ->where("price_index.final_price <=".$price_to);   	
		}
		
		if(!empty($rating_filter))  
		{
			
			if($rating_filter==1)
			{
				$rating_1=20;	
				$rating_2=40;	
			}
			else if($rating_filter==2)
			{
				$rating_1=40;	
				$rating_2=60;	
			}	
			else if($rating_filter==3)
			{
				$rating_1=60;	
				$rating_2=80;	
			}	
			else if($rating_filter==4)
			{
				$rating_1=80;	
				$rating_2=100;	
			}	
			else if($rating_filter==5)
			{
				$rating=100;	 
			}  	
			
			$collectionfirst->getSelect()->joinLeft(array('rova'=> 'review_entity_summary'),'e.entity_id =rova.entity_pk_value', array("rating_summary" => 'rating_summary'))->group('e.entity_id');
			 
			if($rating_filter!=5)
			{	
				$collectionfirst->getSelect()->where("rova.rating_summary >= ".$rating_1."")->where("rova.rating_summary < ".$rating_2."")->where("rova.store_id=1"); 	
			}
			else
			{
				$collectionfirst->getSelect()->where("rova.rating_summary =".$rating)->where("rova.store_id=1");	
			}	    
		}
		
		
		if($sort==1) 
		{
			$collectionfirst->addAttributeToSort('price', 'asc'); 
		}
		else if($sort==2)
		{
			$collectionfirst->addAttributeToSort('price', 'desc');  
		}	
		else if($sort==3)
		{
			$collectionfirst->getSelect()->joinLeft(array('rova'=> 'review_entity_summary'),'e.entity_id =rova.entity_pk_value', array("rating_summary" => 'rating_summary'))->group('e.entity_id');  
			$collectionfirst->getSelect()->order('rating_summary desc');    
		}
		else if($sort==4) 
		{
			$collectionfirst->getSelect()
                ->joinLeft(
                    array('order_items' => $collectionfirst->getResource()->getTable('sales_order_item')),
                    'order_items.product_id = e.entity_id',
                    array('qty_ordered' => 'SUM(order_items.qty_ordered)')
                )
                ->group('e.entity_id')
                ->order('qty_ordered DESC');
		}	
		else 
		{
			$collectionfirst->addAttributeToSort('price', 'asc'); 
		} 
		
		
		
		
		foreach($collectionfirst as $product)
		{
			$defaultArray[]=$product->getId(); 
			$is_special=0;
			$ratings=0;
			$product_iddd=$product->getId();
			$price=$product->getPrice();	
			
			
			$orgprice = $product->getPrice();
			
			
			$specialprice = $product->getSpecialPrice();
			
			$specialfromdate = $product->getSpecialFromDate();
			$specialtodate =  $product->getSpecialToDate();
			$today = time(); 
			
			if(!is_null($specialfromdate) && !is_null($specialtodate))
			{
				$specialfromdate=strtotime($specialfromdate);
				$specialtodate=strtotime($specialtodate);
				
				if($today >= $specialfromdate &&  $today <= $specialtodate)
				{
					$is_special=1;			
				}	
			}
			
			
			if($is_special==1)
			{
				$sort_price = number_format($specialprice, 2, '.', '');
			} 	
			else
			{
				$sort_price = number_format($price, 2, '.', ''); 
			}
 
			
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_iddd);
			if(!empty($RatingOb->getCount()))
			{ 
				$ratings = $RatingOb->getSum()/$RatingOb->getCount();
			}
			
			
			$sold_number= $api_helpers->get_num_product_sold($product_iddd);
			
			
			$first_array[]=array(
				'id'=>$product_iddd,
				'price'=>$price,
				'sort_price'=>$sort_price,
				'rating_count'=>$ratings, 
				'sold_number'=>$sold_number,  
				'is_special'=>$is_special,  
			); 
			
			  
			
		}
		
		//print_r($first_array);
		//die;	
		
		 
        $dataaa= $first_array;
		if(!empty($dataaa))
		{
			foreach($dataaa as $row)
			{
				$product_id=$row['id'];	
				$sort_price=$row['sort_price'];	
				$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
				$product_array['id']= $product->getId();
				$product_array['name']= $product->getName();
				$product_array['sku']= $product->getSku();   
				if($product->getSupportedModels()!='')
				{	
					$product_array['supported_models']= str_replace(',',', ',$product->getSupportedModels());
				}
				else 
				{
					$product_array['supported_models']= null;
				}		
				//$product_array['sort_price']=$sort_price;
				$product_array['price']= number_format($product->getPrice(), 2, '.', '');
				if($user_id!=0)  
				{ 
					
					$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
					$product_array['wishlist']=$whishlist;
				}
				else
				{
					$product_array['wishlist']=null;
				}
				if($product->getAttributeText('brands')){
					$product_array['brand']= $product->getAttributeText('brands');
				}else{
					$product_array['brand']= null;	 
				}
				
				
				
				// From Rma extension
				$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
				if($product->getAttributeText('return_period')=="Flat return period"){
					$product_array['return_days']= $return_date;
				}
				
				
				
				 
				$imageUrl=$api_helpers->get_product_thumb($product_id);
				
				$product_array['image']=$imageUrl;
				 
				
				
				
				 
				$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 
				$product_array['earn_reward'] = $earnOutput->getProductPoints($product);
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$product_array['rating_count'] = $ratings;
				}else{
					$product_array['rating_count'] = 0;
				}
				
				
				$product_array['sold_number']= $api_helpers->get_num_product_sold($product->getId());
				
				$product_iddd=$product->getId();
				$is_special=0; 
				$price=$product->getPrice();	
				$orgprice = $product->getPrice();
				$specialprice =$product->getSpecialPrice(); 
				$specialfromdate = $product->getSpecialFromDate(); 
				$specialtodate =  $product->getSpecialToDate(); 
				$today = time();  
				
				if (!$specialprice)
				{	
					$specialprice = $orgprice;
				}
				else
				{
					if(!is_null($specialfromdate) && !is_null($specialtodate))
					{
						$specialfromdate=strtotime($specialfromdate);
						$specialtodate=strtotime($specialtodate);
						
						if($today >= $specialfromdate &&  $today <= $specialtodate)
						{
							$is_special=1;			
						}	
					}
				}
				
				
				if($is_special==1)
				{
					$product_array['sort_price'] = number_format($product->getSpecialPrice(), 2, '.', '');
					$product_array['specialprice'] = number_format($product->getSpecialPrice(), 2, '.', '');	
					$product_array['specialfromdate'] = $product->getSpecialFromDate(); 
					$product_array['specialtodate'] = $product->getSpecialToDate();
				} 	
				else
				{
					$product_array['sort_price']= number_format($product->getPrice(), 2, '.', ''); 
					$product_array['specialprice'] = null;
					$product_array['specialfromdate'] = null;
					$product_array['specialtodate'] = null;
				}
				
				
				$data[] = $product_array;		
			}	
		}
		
		$result_array = array('success' => 1, 'result' =>$data, 'product_count'=>$product_count,'brand_name'=>$brand_name,'pages_count'=>$pages_count, 'error' =>"Product assigned to this category.");  
		echo json_encode($result_array);
		die();  
		
		
		
		
	
	}catch(Exception $e){
		 
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => $e->getMessage()); 
		echo json_encode($result_array);	
		
	}
?>