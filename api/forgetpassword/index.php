<?php
require "../security/sanitize.php";

/*
	
Example of json format for the OTP match
{
	"accesstoken": "1234",
	"email":"ramit.j@idsil.com"
	
} 
 
*/


$result = sanitizedJsonPayload();
//print_r($result);

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement; 

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
 
 
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$user_array=null; 

$argumentsCount=count($result);

if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Email is missing'); 	
	echo json_encode($result_array);	
	die; 
}




$accesstoken=$result['accesstoken'];
$email=$result['email'];

  
	
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
} 
	

	
if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Email is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
		echo json_encode($result_array);	
		die;
	}
} 	
	

	
	
	
$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
$CustomerModel->setWebsiteId(1); 
$CustomerModel->loadByEmail($email);   
$userId = $CustomerModel->getId();
$useremail = $CustomerModel->getEmail();
$mobile = $CustomerModel->getMobile();

if($userId=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User not exist'); 	
	echo json_encode($result_array);	
	die; 
}  	
else
{
	
	$confirmation=$helpers->checkUserEmailConfirmation($userId); 
	if($confirmation==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Email not confirmed'); 	
		echo json_encode($result_array);	
		die;
	}     
	
	
	
	$group_id = $CustomerModel->getGroupId();
	
	if($group_id==1 || $group_id==5)
	{
		
		$email=$useremail;
		$password=base64_encode('abc'); 
		$mobile=$mobile; 
		$groupid=$group_id; 
		
		$array=array(
			'email'=>$email,
			'password'=>$password,
			'mobile'=>$mobile,
			'groupid'=>$groupid
		);
		
		$app_helpers->create_otp($array);	 
		   
		$user_array=array('email'=>$email,'group_id'=>$group_id);
		
		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'OTP has been sent successfully'); 	
		echo json_encode($result_array);	
		die; 
		
	}
	else
	{
		
		$customerArray=$app_helpers->get_user_details($userId);
		$approve=$customerArray[1]['approve'];
		// if($approve==0)
		// {
		// 	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Your account is pending for approval'); 	
		// 	echo json_encode($result_array);	
		// 	die; 	 
		// }	
		
		$error=0;
		
		$_customers = $objectManager->create('Magento\Customer\Api\AccountManagementInterface');
		$customerCollection = $objectManager->create('Magento\Customer\Model\ResourceModel\Customer\Collection');
		$customerCollection->load();
		
		foreach ($customerCollection as $customers) { 

			$emails = $customers->getData('email');
			
			if($emails==$email) { 
				try 
				{
					$_customers->initiatePasswordReset($email, AccountManagement::EMAIL_RESET);
					
					$error=0;
				} 
				catch (\Exception $exception) {
					
					$error=1;	
					/* $result_array=array('success' => 0,'result' => $user_array, 'error' => 'Unable to send the password reset email');  	
					echo json_encode($result_array);	
					die; */
				} 
			}  
		}
		
		if($error==0)
		{	 
			$user_array=array('email'=>$email,'group_id'=>$group_id);
			$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Reset password email has been sent');  	
			echo json_encode($result_array);	
			die;		
		} 
		else
		{ 
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Unable to send the password reset email');  	
			echo json_encode($result_array);	
			die;
		} 
	
	}
	 

} 
  


?>



