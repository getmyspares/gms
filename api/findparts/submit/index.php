<?php
	//------- Submit Find my part data.
	/* variables accesstoken,category_id,brand,model  */
	//https://dev.idsil.com/design/api/findparts/submit/?category_id=4&brand=47&model=113&accesstoken=1234
	require "../security/sanitize.php";
	include('../../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$inventory_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\inventory'); 
	$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 
	$result = $_GET;
	 // print_r($result);
	
	$product_array = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 1 || $argumentsCount > 5){
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}  */ 
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	if(!empty($result['category_id'])){
		$category_id = $result['category_id'];
	}else{
		$category_id = '';	
	}
	
	if($category_id=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	if(!empty($result['brand'])){
		$brand = $result['brand'];
	}else{
		$brand = null;	
	}
	
	
	/* if(!empty($result['model'])){
		$model = $result['model'];
		if(empty($result['brand'])){
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'please select brand first'); 	
			echo json_encode($result_array);	
			die;
		}
	}else{
		$model = null;	
	} */ 
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
		
	}else{
		$page_no = 1;	
	} 
	
	$model = null;	
	if(!empty($result['model']))
	{	 
		$model = $result['model'];
	}
	
	$sort=1;
	if(!empty($result['sort']))
	{
		$sort=$result['sort'];	
	}	
	
	try
	{
		/* 	echo $category_id;
		 echo '<br>';
		 echo $brand;
		 echo '<br>';
		 echo $model;
		 echo '<br>'; */
		
		
		
		/* $productArray=$inventory_helpers->get_product_array('16395','0');	
		
		echo "<pre>";
		print_r($productArray);
		echo "</pre>";
		
		
		die; */
		
		
		
		
		if($brand!=null)
		{	
			$brand_name=$api_helpers->get_brand_by_id($brand);
			$brand_name=trim(strtolower($brand_name));
		}
		
		
		if($model!=null)
		{
			$model_name=$api_helpers->get_model_by_id($model);
			$model_name=trim(strtolower($model_name)); 
		}
		
		$select = $connection->select()
			  ->from('catalog_category_entity') 
			  ->where('parent_id= ?',$category_id);
		$result = $connection->fetchAll($select);
		$cat_idss='';
		$cat_ids='';
		
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$cat_idss .=$row['entity_id'].',';	
				
			}		
		
			$cat_ids=substr($cat_idss,0,-1);	
		
			$cat_cond=' where category_id in ('.$cat_ids.')';
		}
		else
		{
			$cat_cond=' where category_id in ('.$category_id.')';
		}		
		
		
		$cat_cond=' where category_id in ('.$category_id.')';
		
		//print_r($result);
		
		$main_query="select * from catalog_category_product ".$cat_cond. " limit 0,200"; 
		
		$select = $main_query;
		$result = $connection->fetchAll($select); 
		$brandProductArray=array();
		$modelProductArray=array();
		$catArray=array();
		
		
		if(!empty($result)) 
		{
			foreach($result as $value)
			{
				$product_id=$value['product_id'];	
				$catArray[]=$product_id;	
				
				if($brand!=null)
				{	
					$product_brands=$api_helpers->get_product_brands($product_id);
					$product_brands=trim(strtolower($product_brands));
					
					if($product_brands==$brand_name)
					{
						$brandProductArray[]=$product_id;	
					}	
				}
				
				if($model!=null)
				{
					$product_model=$api_helpers->get_product_model($product_id);
					$product_model=trim(strtolower($product_model));
					
					if($product_model==$model_name)
					{
						$modelProductArray[]=$product_id;	
					}	 
				}
				
			} 		
		}	
		
		
		//print_r($catArray);  
		//print_r($brandProductArray);  
		//print_r($modelProductArray);  
		/*
			if(!empty($catArray) && empty($brandProductArray) && empty($modelProductArray))
			{
				$fullarray[]=$catArray;  	
				//echo "1";
			}
			else if(!empty($catArray) && !empty($brandProductArray) && empty($modelProductArray))
			{
				$fullarray[]=$catArray;  
				$fullarray[]=$brandProductArray; 	
				//echo "2";
			}	
			else if(!empty($catArray) && empty($brandProductArray) && !empty($modelProductArray))
			{
				$fullarray[]=$catArray;  
				$fullarray[]=$modelProductArray; 	
				//echo "3";
			}	
			else if(!empty($catArray) && !empty($brandProductArray) && !empty($modelProductArray))
			{
				$fullarray[]=$catArray;  
				$fullarray[]=$brandProductArray; 
				$fullarray[]=$modelProductArray;
				//echo "4";  
			}	
		
		*/
		
		
		if(empty($catArray))
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'No product found'); 	
			echo json_encode($result_array);	 
			die;
		}	
		
		
		$fullarray[]=$catArray;  
		$fullarray[]=$brandProductArray; 
		$fullarray[]=$modelProductArray;
		
		
		
		
		$array = array_filter($fullarray); //filter empty array
		$res_arr = array_shift($array); // Shift an element off the beginning of array
		foreach($array as $filter){
			$res_arr = array_intersect($res_arr, $filter);
		} 
		
		
		
		//$res_arr = array_map("unserialize", array_unique(array_map("serialize", $fullarray)));
		
		//$res_arr=array_unique($fullarray, SORT_REGULAR);
		$res_arr = array_values($res_arr);
		
		//print_r($res_arr);
		//die;
		
		
		
		$i = 1 ;
		foreach($res_arr as $product_iddd)
		{
			
			$is_special=0;
			$ratings=0;
			
			//if($product_iddd==1257)
			//{
			$price=$api_helpers->get_product_prices($product_iddd);		
			
			
			$orgprice = $api_helpers->get_product_prices($product_iddd);
			
			
			$specialprice = $api_helpers->get_product_special_prices($product_iddd);
			
			$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
			$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
			$today = time(); 
			
			if (!$specialprice)
			{	
				$specialprice = $orgprice;
			}
			else
			{
				if(!is_null($specialfromdate) && !is_null($specialtodate))
				{
					$specialfromdate=strtotime($specialfromdate);
					$specialtodate=strtotime($specialtodate);
					
					if($today >= $specialfromdate &&  $today <= $specialtodate)
					{
						$is_special=1;			
					}	
				}
			}
			
			
			if($is_special==1)
			{
				$sort_price = number_format($specialprice, 2, '.', '');
				
			} 	
			else
			{
				$sort_price = number_format($price, 2, '.', ''); 
				
			}

			
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_iddd);
			if(!empty($RatingOb->getCount()))
			{ 
				$ratings = $RatingOb->getSum()/$RatingOb->getCount();
			}
			
			
			$sold_number= $api_helpers->get_num_product_sold($product_iddd);
			
			
			$first_array[]=array(
				'id'=>$product_iddd,
				'price'=>$price,
				'sort_price'=>$sort_price,
				'rating_count'=>$ratings, 
				'sold_number'=>$sold_number,  
			
			); 
			
			$i++;  
			//}
		}	 
		
		
		 
		
		
		if(isset($sort))
		{
			if($sort==1) 
			{
				$keys = array_column($first_array, 'sort_price');
				array_multisort($keys, SORT_ASC, $first_array);	
			}
			else if($sort==2) 
			{
				$keys = array_column($first_array, 'sort_price');
				array_multisort($keys, SORT_DESC, $first_array);
				
			}	
			else if($sort==3)
			{
				$keys = array_column($first_array, 'rating_count');
				array_multisort($keys, SORT_ASC, $first_array);
			}
			else if($sort==4)
			{
				$keys = array_column($first_array, 'sold_number');
				array_multisort($keys, SORT_ASC, $first_array);
			}	
				
		}  
		else
		{
			$keys = array_column($first_array, 'sort_price');
			array_multisort($keys, SORT_ASC, $first_array);	
		}
		
		
		$need_to_show=20;
		$product_count = count($first_array); 
		$pages_count = ceil($product_count/$need_to_show); 
		
		if($page_no >$product_count){
			$result_array = array('success' => 0,'result' => null, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array);
			die();
		}
		  
		
		
		
		$end = (($need_to_show)*$page_no);
		if($end>$need_to_show){
		 $start = ($need_to_show*($page_no-1));
		}else{
			$start = 0;
		}
		 // echo 'start'.$start;
		 // echo 'end'.$end;
		$data_count= count($first_array); 
		
		$dataaa= array_slice($first_array,$start,$need_to_show);
		 
		//print_r($dataaa);
		//die; 
		
		if(!empty($dataaa))
		{
			foreach($dataaa as $row)
			{
				$product_id=$row['id'];	
				$sort_price=$row['sort_price'];	
				$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
				$product_array['id']= $product->getId();
				$product_array['name']= $product->getName();
				$product_array['sku']= $product->getSku();   
				//$product_array['sort_price']=$sort_price;
				$product_array['price']= number_format($product->getPrice(), 2, '.', '');
				if(!empty(@get_numeric($result['user_id'])))  
				{ 
					$user_id=@get_numeric($result['user_id']);
					$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
					$product_array['wishlist']=$whishlist;
				}
				else
				{
					$product_array['wishlist']=null;
				}
				if($product->getAttributeText('brands')){
					$product_array['brand']= $product->getAttributeText('brands');
				}else{
					$product_array['brand']= null;	
				}
				
				if($product->getAttributeText('model')){
					$product_array['model']= $product->getAttributeText('model');
				}else{
					$product_array['model']= null;	
				}
				
				if($product->getAttributeText('supported_models')){
					$product_array['supported_models']= $product->getAttributeText('supported_models');
				}else{
					$product_array['supported_models']= null;	
				}
				// From Rma extension
				$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
				if($product->getAttributeText('return_period')=="Flat return period"){
					$product_array['return_days']= $return_date;
				}
				
				
				
				 
				$imageUrl=$api_helpers->get_product_thumb($product_id);
				
				$product_array['image']=$imageUrl;
				 
				
				
				
				 
				
				$product_array['earn_reward'] = $earnOutput->getProductPoints($product);
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$product_array['rating_count'] = $ratings;
				}else{
					$product_array['rating_count'] = 0;
				}
				
				
				$product_array['sold_number']= $api_helpers->get_num_product_sold($product->getId());
				
				$product_iddd=$product->getId();
				$is_special=0; 
				$price=$api_helpers->get_product_prices($product_iddd);		
				$orgprice = $api_helpers->get_product_prices($product_iddd);
				$specialprice = $api_helpers->get_product_special_prices($product_iddd);
				$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
				$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
				$today = time(); 
				
				if (!$specialprice)
				{	
					$specialprice = $orgprice;
				}
				else
				{
					if(!is_null($specialfromdate) && !is_null($specialtodate))
					{
						$specialfromdate=strtotime($specialfromdate);
						$specialtodate=strtotime($specialtodate);
						
						if($today >= $specialfromdate &&  $today <= $specialtodate)
						{
							$is_special=1;			
						}	
					}
				}
				
				
				if($is_special==1)
				{
					$product_array['sort_price'] = number_format($product->getSpecialPrice(), 2, '.', '');
					$product_array['specialprice'] = number_format($product->getSpecialPrice(), 2, '.', '');	
					$product_array['specialfromdate'] = $product->getSpecialFromDate(); 
					$product_array['specialtodate'] = $product->getSpecialToDate();
				} 	
				else
				{
					$product_array['sort_price']= number_format($product->getPrice(), 2, '.', ''); 
					$product_array['specialprice'] = null;
					$product_array['specialfromdate'] = null;
					$product_array['specialtodate'] = null;
				}
				
				
				$data[] = $product_array;		


				
			}	
		}
		
		
		$category = $objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
		
		if(!empty($category->getName())){
			$category_name = trim($category->getName());
		}else{
			$category_name =  null;
		}
		
		
		
		if(!empty($data)){
			$result_array = array('success' => 1, 'result' => $data,'category_id' =>$category_id, 'category_name'=>$category_name,'product_count'=>$data_count, 'pages_count'=>$pages_count, 'error' =>"Parent Details.");   
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $product_array, 'error' => "There are no product for this filter."); 
			echo json_encode($result_array);
			die();
		}
		
		
		
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $parent_category_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>