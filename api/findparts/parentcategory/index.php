<?php
	require "../../security/sanitize.php";
	include('../../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	//print_r($result);
	
	$parent_category_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $parent_category_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_category_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $parent_category_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}


	try
	{
		//$parent_category_array = null;
		$categoryHelper = $objectManager->get('\Magento\Catalog\Helper\Category');
		$categories = $categoryHelper->getStoreCategories();
		
		$i =0 ;
		foreach ($categories as $category) {
			$parent_category_array[$i]['id'] = $category->getId();
			$parent_category_array[$i]['name'] = $category->getName();
			$i++;
		}
		
		if(!empty($parent_category_array)){
			$result_array = array('success' => 1, 'result' => $parent_category_array, 'error' =>"Parent categories list."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $parent_category_array, 'error' => "There are no categrory assigned."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		$result_array = array('success' => 0,'result' => $parent_category_array, 'error' => $e->getMessage());
		echo json_encode($result_array);

	}
?>