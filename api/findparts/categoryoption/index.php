<?php
	// Get parent category details. 
	require "../security/sanitize.php";
	include('../../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$result = $_GET;
	 // print_r($result);
	
	$parent_model_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $parent_model_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	if(!empty($result['category_id'])){
		$categoryId = $result['category_id'];
	}else{
		$categoryId = '';	
	}
	
	if($categoryId=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	


	try
	{
		
		$subcatArray=array();
		$catArray=array();
		$filterValues=array();
		$modelArray=array();
		$parent_model_array=array();
		
		$categoryId;	 
		$subcategory = $objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);
		$subcats = $subcategory->getChildrenCategories();
		$catArray=$subcats->getData();
		if(!empty($catArray))
		{	
			foreach($catArray as $cat)
			{
				$cat_id=$cat['entity_id'];	
				$category = $objectManager->create('Magento\Catalog\Model\Category')->load($cat_id);
				$cat_name=$category->getName();
						
				$subcatArray[]=array(
					'id'=>$cat_id,
					'name'=>$cat_name
				);		
						
				
			}	
		}
		
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$category = $categoryFactory->create()->load($categoryId);
		
		
		
		
		$registry = $objectManager->get('\Magento\Framework\Registry');
		$layerResolver = $objectManager->get('\Magento\Catalog\Model\Layer\Resolver');
		
		$layer = $layerResolver->get();
        $layer->setCurrentCategory($category->getId());
		
		
        $fill = $objectManager->create('Magento\Catalog\Model\Layer\Category\FilterableAttributeList');
        $filterList = new \Magento\Catalog\Model\Layer\FilterList($objectManager,$fill);
        $filterAttributes = $filterList->getFilters($layer);
		
		foreach($filterAttributes as $filter)
		{
			$filter->getName();
		}
		
		$j = 0;
		
		
		foreach($filterAttributes as $filter)
		{
			if($filter->getName() == 'Brand'){
                $items = $filter->getItems();
                foreach($items as $item)
                {
                    $filterValues[$j]['name'] = strip_tags($item->getLabel());
                    $filterValues[$j]['id'] = $item->getValue();
                    //$filterValues[$j]['count'] = $item->getCount(); 
                    $j++; 
                }
            }
		}
		
		 
		$brandArray=$filterValues;
		$model_result_arrayy=array();
		
		$categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('model')
			->load();
		 
		$model_count=$categoryProducts->count();
		if($model_count!=0)
		{	
			foreach($categoryProducts as $product)
			{
				
				if($product->getModel()!='')
				{	
					$modelArray[]=$product->getModel(); 
				}
			}	
			
			//print_r($modelArray);
			//die; 
			
			
			if(!empty($modelArray))
			{
				$modelArray = array_unique($modelArray); 
				
				//$modelArray = array_map("unserialize", array_unique(array_map("serialize", $modelArray)));
				foreach($modelArray as $row)
				{
					$model_id=$row;		
					$model_name=$api_helpers->get_product_model_name_new($model_id);	
					$count=0;
					
					/*
					$categoryProducts = $category->getProductCollection()
						->addAttributeToSelect('model')
						->addAttributeToFilter('model', ['eq' => $model_id])	 
						->load();   
					
					$count=$categoryProducts->count();
					*/
					
					if($model_name!='na')
					{	
						$model_result_arrayy[]=array(
							'id'=>$model_id,   
							'name'=>$model_name
						);	
					}
					
				}  
				
			}	
		}
		
		//print_r($subcatArray); 
		//print_r($brandArray);
		//print_r($model_result_arrayy);
		//die;
		$parent_model_array['category_id'] = $categoryId;
		$parent_model_array['sub_cat'] = $subcatArray; 
		$parent_model_array['brand'] = $brandArray; 
		$parent_model_array['model'] = $model_result_arrayy;
		
		
		
		
		// print_r($parent_model_array);
		// die();

		
		if(!empty($parent_model_array)){
			$result_array = array('success' => 1, 'result' => $parent_model_array, 'error' =>"Category options details."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $parent_model_array, 'error' => "There are no data."); 
			echo json_encode($result_array);
			die();
		}
		
	
	}
	catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $parent_model_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>