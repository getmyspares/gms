<?php
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
  if(!isset($_GET['accesstoken']) || !isset($_GET['product_id']) || !isset($_GET['customer_id']))
	{
		$result_array=array('success' => 0,'result' => $null , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
  }
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
  
  $accesstoken=$_GET['accesstoken'];
  $product_id=$_GET['product_id'];
  $customer_id=$_GET['customer_id'];
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $null, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $null, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
  $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
  $productRepository = $objectManager->create('Magento\Catalog\Model\ProductRepository');
	$data="";
	
	try {
		/* @var $product \Magento\Catalog\Model\Product */
		$product = $productRepository->getById($product_id);
		$store = $storeManager->getStore();
		/** @var \Magento\ProductAlert\Model\Stock $model */
		$model = $objectManager->create('\Magento\ProductAlert\Model\Stock')
		->setCustomerId($customer_id)
		->setProductId($product->getId())
		->setWebsiteId($store->getWebsiteId())
		->setStoreId($store->getId());
		$model->save();
		
		$result_array=array('success' => 1, 'result' =>$data, 'error' => 'Alert subscription has been saved.');
		echo json_encode($result_array);  
		die();
		
	} catch (\Magento\Framework\Exception\NoSuchEntityException $noEntityException) {
		echo $noEntityException->getMessage();
		$result_array=array('success' => 0, 'result' =>$data, 'error' => 'There are not enough parameters.');
		echo json_encode($result_array);  
		die();
	} catch (\Exception $e) {
		$result_array=array('success' => 0, 'result' =>$data, 'error' => "The alert subscription couldn't update at this time. Please try again later.");
		echo json_encode($result_array);  
		die();
	}

?>


