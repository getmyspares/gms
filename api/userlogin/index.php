<?php
/* payload 
{"deviceid":"xxxxx","accesstoken":"734678534","email":"malemeemail@gmaill.com","password":"TmlybWFsQDEyMw=="}
{"deviceid":"xxxxx","accesstoken":"734678534","email":"ombugtest123456@gmail.com","password":"UmFqdUAxMjM0"}
*/
require "../security/sanitize.php";
$result = sanitizedJsonPayload();

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');

$argumentsCount=count($result); 
$user_array=null; 

if($argumentsCount < 4 || $argumentsCount > 4)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}  
if(!isset($result['deviceid']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Incorrect Payload'); 	
	echo json_encode($result_array);	
	die; 
} 
$device_id = $result['deviceid'];
$common_message="Invalid Credentials";
$need_approval="Login Failed . Please Contact Admin";

if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}

if(!isset($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$common_message"); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['password']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => "$common_message"); 	
	echo json_encode($result_array);	
	die; 
}


$accesstoken=$result['accesstoken'];
$email=$result['email'];
$password= base64_decode($result['password']);
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => "$common_message"); 	
	echo json_encode($result_array);	
	die;
}
else
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => "$common_message");
		echo json_encode($result_array);	
		die;
	}
} 
	
if($password=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => "$common_message"); 	
	echo json_encode($result_array);	
	die;
}	


$CustomerModel->setWebsiteId(1); 
$CustomerModel->loadByEmail($email); 
$userId = $CustomerModel->getId();


if($userId=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => "$common_message"); 	
	echo json_encode($result_array);	
	die;
}	
else
{
		$locked=$api_helpers->check_user_locked($userId);
		if($locked==0)
		{
			$result_array=array('success' => 0,'result' => $user_array,'redirect'=>0,'error' => "$need_approval");echo json_encode($result_array);	 
			die; 
		} 
	
	try
	{ 
		
		$custId=$userId;
		$group_id=$CustomerModel->getGroupId();
		
		if($group_id==4) 
		{
			$confirmation=$helpers->checkUserEmailConfirmation($custId); 
			if($confirmation==0)
			{
				$result_array=array('success' => 0,'result' => $user_array, 'error' => "$need_approval"); 	
				echo json_encode($result_array);	
				die;
			} 
		}
		
		$customer = $objectManager->get('\Magento\Customer\Model\AccountManagement')->authenticate($email, $password);
		
		$query = "select amount from `mst_rewards_transaction` where customer_id='$custId'";
		$result = $connection->fetchAll($query);
		$total_reward=0;
		if(!empty($result))
		{
			foreach($result as $row)
			{
				$total_reward=$total_reward + $row['amount'];  
			}	 
		}
		
		$cart_total=0;

		/* Not  loading customer quote here for  speed  optimisation @ritesh */
		/*
		 $quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($custId); 
		$items = $quote->getAllItems();
		if(!empty($items))
		{ 	
			foreach($items as $item)  
			{
				$qty=$item->getQty(); 
				$cart_total=$cart_total+$qty;
			}		
		}  */
		
		/* not calculating wishlist item here for speed  optimisation , have removed the  code*/
		$wishlist_count =  0;

		$firstname=$CustomerModel->getFirstname();
		$lastname=$CustomerModel->getLastname();

		$api_helpers->store_user_token($custId);
		
		$mobile=$CustomerModel->getMobile();
		
		$comp_name=null;	
		$comp_address=null;	
		$gst_num=null;	
		$pan_num=null;		

		
		if($firstname=='firstname')
		{
			$firstname='';	
		}	
		
		if($lastname=='lastname')
		{
			$lastname='';	 
		}	
		
		if($group_id==1) 
		{  
			$user_array=array(   
				'userID'=>$custId,
				'userFirstName'=>$firstname,	
				'userLastName'=>$lastname,	
				'userEmail'=> base64_encode($email),	
				'total_reward'=>$total_reward,	
				'wishlist_count'=>$wishlist_count,	
				'cart_total'=>$cart_total,	
				'gst'=> base64_encode($gst_num),	
				'pan'=>base64_encode($pan_num),	
				'company_name'=>$comp_name,	
				'company_address'=>base64_encode($comp_address),	
				'group_id'=>$group_id,	
				'mobile'=>base64_encode($mobile)	
			);  
		}
		else 
		{
			$comp_name=$CustomerModel->getCompanyNam();	
			$comp_address=$CustomerModel->getCompanyAdd();	
			$gst_num=$CustomerModel->getGstNumber();	
			$pan_num=$CustomerModel->getPanNumber();	
			
			
			$user_array=array(   
				'userID'=>$custId,
				'userFirstName'=>$firstname,	
				'userLastName'=>$lastname,	
				'userEmail'=>base64_encode($email),	
				'total_reward'=>$total_reward,	
				'wishlist_count'=>$wishlist_count,	
				'cart_total'=>$cart_total,	
				'gst'=>base64_encode($gst_num),	
				'pan'=>base64_encode($pan_num),	
				'company_name'=>$comp_name,	
				'company_address'=>base64_encode($comp_address),	
				'group_id'=>$group_id,	
				'mobile'=>base64_encode($mobile)	 
			);  
		}
		$token = rand();
		$user_array['accesstoken'] = $token;

		$status = $api_helpers->registerToken($custId,$device_id,$token);
		$group_id;
		if($group_id==4 || $group_id==5)
		{
			$comp_name=$CustomerModel->getCompanyNam();	
			$comp_address=$CustomerModel->getCompanyAdd();	
			$gst_num=$CustomerModel->getGstNumber();	
			$pan_num=$CustomerModel->getPanNumber();	
		
			if($firstname=='' || $lastname=='' || $comp_name=='' || $comp_address=='' || $gst_num=='' || $pan_num=='')
			{
				$result_array=array('success' => 1,'result' => $user_array,'redirect'=>0,'error' => 'Some Information missing'); 	
				echo json_encode($result_array);	 
				die;  
			}  
			else   
			{
				$result_array=array('success' => 1,'result' => $user_array,'redirect'=>1,'error' => 'Have all information'); 	
				echo json_encode($result_array);	 
				die;  
			}   
		
		}
		else
		{
			$firstname=$CustomerModel->getFirstname();	
			$lastname=$CustomerModel->getLastname();	
			$mobile=$CustomerModel->getMobile();	
			        
			if($firstname=='' || $lastname=='' || $mobile=='')
			{
				$result_array=array('success' => 1,'result' => $user_array,'redirect'=>0,'error' => 'Some information missing'); 	
				echo json_encode($result_array);	 
				die;    
			}
			else 
			{
				$result_array=array('success' => 1,'result' => $user_array,'redirect'=>1,'error' => 'Have all information');  	
				echo json_encode($result_array);	  
				die; 
			}   
		}
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => "$common_message"); 	
		echo json_encode($result_array);	
		die;
	}  
}
  


?>



