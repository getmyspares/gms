<?php
/*
{ 
	"accesstoken": "1234",
	"item_id": "720",
	"order_id": "578",
	"user_id": "479",
	"order_delivered": "1",
	"reason": "1",
	"comments": "1",
	"refund_mode": "1"
	
	
}
----------------

{
	"accesstoken": "1234",
	"item_id": "97",
	"order_id": "85",
	"user_id": "24",
	"order_delivered": "1",
	"return_qty": "1",
	"consignment_no": "",
	"reason": "1",
	"comments": "1",
	"refund_mode": "1"
}

*/   
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);

//$result=$_GET; 

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$argumentsCount=count($result); 
$user_array=null; 
 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
$store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getStore();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

/* if($argumentsCount < 8 || $argumentsCount > 8)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}  */ 

 
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['item_id']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['order_id']) || empty(@get_numeric($result['order_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order Id is missing'); 	
	echo json_encode($result_array);	  
	die; 
}
if(!isset($result['order_delivered']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order delivered is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['reason']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Reason is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['comments']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Comments is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['refund_mode']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Refund mode is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

if(!isset($result['return_qty']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Return Qty is missing'); 	
	echo json_encode($result_array);	 
	die; 
}

$accesstoken=$result['accesstoken'];
$item_id=$result['item_id'];
$order_id=$result['order_id']; 
$user_id=@get_numeric($result['user_id']); 
$order_delivered=$result['order_delivered']; 
$reason=$result['reason']; 
$comments=$result['comments']; 
$refund_mode=$result['refund_mode']; 
$return_qty=$result['return_qty']; 

if($order_delivered==0) 
{
	if(isset($result['consignment_no']))
	{	
		$consignment_no=$result['consignment_no']; 
		if($consignment_no=='')  
		{
			$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Consignment Number is missing'); 	
			echo json_encode($result_array);	 
			die; 
		} 
	} 
	
	
	if(isset($result['package_condition']))
	{
		$package_condition=$result['package_condition']; 
		if($package_condition=='')  
		{
			$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Package Condition is missing'); 	
			echo json_encode($result_array);	 
			die;  
		}
	}	
}	
else
{
	$consignment_no='';
	$package_condition='';
}    

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check=$api_helpers->check_user_exists($user_id);
	if($check==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;
	}

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}

}	

$accesstoken=$result['accesstoken'];
$item_id=$result['item_id'];
$order_id=$result['order_id']; 
$user_id=@get_numeric($result['user_id']); 
$order_delivered=$result['order_delivered']; 
$reason=$result['reason']; 
$comments=$result['comments']; 
$refund_mode=$result['refund_mode']; 

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');		
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');

$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);
foreach ($order->getAllItems() as $item)
{
	$item_ids=$item->getId();
	if($item_ids==$item_id) 
	{	
		$item_ordered=$item->getQtyOrdered();
	} 
	
}  

$select = $connection->select() 
		  ->from('wk_rma_items') 
		  ->where('order_id = ?', $order_id)
		  ->where('item_id = ?', $item_id); 
$result = $connection->fetchAll($select);
$total_item_qty=0;

if(!empty($result))
{
	
	foreach($result as $row)
	{
		$item_qty=$row['qty'];	
		$total_item_qty=$total_item_qty+$item_qty;
	
	}		
}	

if($total_item_qty!=0)
{
	$remaining_qty=$item_ordered-$total_item_qty;
	 
	if($remaining_qty!=0)
	{
		if($return_qty > $remaining_qty)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Return Qty is incorrect'); 	
			echo json_encode($result_array);	
			die;	
		} 
	}	
}	 

if($return_qty > $item_ordered)
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Return Qty is incorrect'); 	
	echo json_encode($result_array);	
	die;	
}  	 

$user_details=$api_helpers->get_user_details($user_id);
$name=$user_details[0]['firstname'].' '.$user_details[0]['lastname'];
$imageArray = [];

$order_inc_id=$order->getIncrementId();
$group='customer';
$customer_id=$user_id;
$package_condition=$package_condition; 
$resolution_type=$refund_mode;
$additional_info=$comments; 
$customer_delivery_status=$order_delivered;
$customer_consignment_no=$consignment_no;
$admin_delivery_status='0';
$admin_consignment_no='';
$image=serialize($imageArray); 
$shipping_label='0';
$guest_email='';
$status='1';
$created_at=date('Y-m-d H:i:s');
$name=$name;
$admin_status='1';
$final_status='0'; 
 

/* $sql="select * from wk_rma_items where order_id='".$order_id."' and item_id='".$item_id."'";
$result = $connection->fetchAll($sql); 
if(!empty($result))
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Products is already in returned Queue'); 	
	echo json_encode($result_array);	 
	die;
} */	  
 
  


$sql="INSERT INTO wk_rma(order_id,`group`,`increment_id`, customer_id, package_condition, resolution_type, additional_info, customer_delivery_status, customer_consignment_no, admin_delivery_status, admin_consignment_no, image, shipping_label, guest_email, status, created_at, name, admin_status, final_status) VALUES ('".$order_id."','".$group."','".$order_inc_id."','".$customer_id."','".$package_condition."','".$resolution_type."','".$additional_info."','".$customer_delivery_status."','".$customer_consignment_no."','".$admin_delivery_status."','".$admin_consignment_no."','".$image."','".$shipping_label."','".$guest_email."','".$status."','".$created_at."','".$name."','".$admin_status."','".$final_status."')";      
 
$connection->query($sql);	


$sql="select * from wk_rma where order_id='".$order_id."' and customer_id='".$customer_id."' order by rma_id desc limit 0,1";
$result = $connection->fetchAll($sql); 


$rma_id=$result[0]['rma_id'];

$select = $connection->select() 
		  ->from('wk_rma_reason') 
		  ->where('id = ?', $reason);
$result = $connection->fetchAll($select);

$reason_name=$result[0]['reason'];


$rma_id=$rma_id;
$item_id=$item_id;
$qty=$return_qty;
$reason_id=$reason; 
$order_id=$order_id; 
$rma_reason=$reason_name;   


$sql="INSERT INTO `wk_rma_items`(`rma_id`, `item_id`, `reason_id`, `qty`, `order_id`, `rma_reason`) VALUES ('".$rma_id."','".$item_id."','".$reason_id."','".$qty."','".$order_id."','".$rma_reason."')";
$connection->query($sql);	

$user_array=array('order_id'=>$order_id,'item_id'=>$item_id);
$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Return Successfully Saved.'); 	 
echo json_encode($result_array); 	 
die; 

?>