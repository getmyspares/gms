<?php
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);
/*
	{
		"accesstoken": "1234",
		"user_id": "455",
		"address_id": "14"
	}
*/

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

$argumentsCount=count($result);
$user_array=null; 

if($argumentsCount < 3 || $argumentsCount > 3)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	

if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User ID is missing'); 	
	echo json_encode($result_array);	 
	die; 
}
if(!isset($result['address_id']) || empty(@get_numeric($result['address_id'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Address ID parameter is missing'); 	
	echo json_encode($result_array);	
	die; 
}

$accesstoken=$result['accesstoken'];
$user_id  = @get_numeric($result['user_id']);
$address_id = @get_numeric($result['address_id']); 

if(!empty(@get_numeric($result['user_id']))){
	$user_id = @get_numeric($result['user_id']);
}else{
	$user_id = '';	
}

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	 
		die;
	}	
}

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
} else {
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}
	
/*if($user_id=="")
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User ID is missing'); 	
	echo json_encode($result_array);	
	die;	
}
else
{	  
	$check_user=$api_helpers->check_customer_exists($user_id);	 
	 if($check_user==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User not exists'); 	
		echo json_encode($result_array);	
		die;	
	}	
}*/	


$check_address=$api_helpers->check_customer_address_exists($address_id);

if($check_address==0) 
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Address Id not exists'); 	
	echo json_encode($result_array);	
	die;	
}
else 
{ 	
	$api_helpers->set_default_shipping_address($address_id,$user_id);
	$api_helpers->set_default_billing_address($address_id,$user_id);
	
	$noDeliveryMsg = '';
	$deliverable   = true;
	$pincode = $connection->fetchOne("SELECT postcode FROM `customer_address_entity` WHERE `entity_id` = '$address_id'");
	if($pincode){
		$rdata = $connection->fetchRow("SELECT is_active,msg FROM `pincode_details` WHERE `pincode` = '$pincode'");
		if($rdata['is_active']==0){
			$deliverable   = false;
			$noDeliveryMsg = 'Due to covid restriction, we are not shipping to the location selected by you.Please chose a different location.';
			if($rdata['msg']!=null && trim($rdata['msg'])!=''){
				$noDeliveryMsg = $rdata['msg'];				
			}
		}
	}
	
	$user_array=array('user_id'=>$user_id,'address_id'=>$address_id,'deliverable'=>$deliverable,'message'=>$noDeliveryMsg); 
	$result_array=array('success' => 1, 'result' => $user_array,'error' => 'Selected address is set as default'); 	
	echo json_encode($result_array);	 
	die;
} 	
?>
