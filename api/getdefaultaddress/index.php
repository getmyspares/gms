<?php
	/*
	List Delivery Address for the customer
	
	
	{ 
		"accesstoken": "1234",
		"user_id": "455"
	}
	
	
	*/
    // SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
	
		require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result);

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');

$argumentsCount=count($result);

if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	


$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);


$user_array=null; 

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');  

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}




if($user_id=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
	
	$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
	$CustomerModel->setWebsiteId(1); 
	$CustomerModel->load($user_id); 
	$user_email = $CustomerModel->getEmail();
	if($user_email=='')
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'No User Found'); 	
		echo json_encode($result_array);	 
		die;
	}
	else
	{
		$userArrays=$CustomerModel->load($user_id);
		
		$userArray=$userArrays->getData();
		
		//print_r($userArrays->getData());
		
		$mobile=0;
		
		if(isset($userArray['mobile']))
		{
			$mobile=$userArray['mobile'];		
		}	
		
		$user_array=array(
			'user_id'=>$userArray['entity_id'],
			'mobile'=>base64_encode($mobile),
			'email'=>base64_encode($userArray['email']),
			'firstname'=>base64_encode($userArray['firstname']),
			'lastname'=>base64_encode($userArray['lastname'])
		);
		
	}		
} 




try
{
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$select = $connection->select()
	->from('customer_address_entity') 
	->where('parent_id = ?', $user_id);   
	
	$results = $connection->fetchAll($select);
	
	
	//print_r($results);	
	
	$mainArray=array();
	foreach($results as $row1)
	{
		$address_id=$row1['entity_id'];
		$street=explode(PHP_EOL, $row1['street']);
		
		$house_no='';
		$road_name='';
		$landmark='';
		
		if(isset($street[0]))
		{
			$house_no=$street[0];	
		}	
		
		if(isset($street[1]))
		{
			$road_name=$street[1];	
		}	
		
		
		if(isset($street[2]))
		{
			$landmark=$street[2];	
		}	
		
		$query =  "Select `default_billing` from customer_entity where entity_id=$user_id";
		$default_billing_id = $connection->fetchOne($query);
		

		$query =  "Select `default_shipping` from customer_entity where entity_id=$user_id";
		$default_shipping_id = $connection->fetchOne($query);
		

		$default_billing_address=0;
		$default_shipping_address=0;
		$address_type=$api_helpers->address_type($row1['entity_id']);
		$default_address=$api_helpers->check_default_address($row1['entity_id'],$user_id);
		if($address_id==$default_billing_id)
		{
			$default_billing_address=1;
			$mainArray[]=array(
				'id'=>$row1['entity_id'], 
				'title'=>"Billing Address", 
				'firstname'=>base64_encode($row1['firstname']),
				'lastname'=>base64_encode($row1['lastname']),
				'postcode'=>base64_encode($row1['postcode']),
				'city'=>base64_encode($row1['city']),
				'region'=>base64_encode($row1['region']),
				'region_id'=>base64_encode($row1['region_id']),
				'mobile'=>base64_encode($row1['telephone']),  
				'street_1'=>base64_encode($house_no),
				'street_2'=>base64_encode($road_name), 
				'landmark'=>base64_encode($landmark),
				'address_type'=>base64_encode($address_type), 
				'default_billing_address'=>$default_billing_address,
				'default_shipping_address'=>$default_shipping_address
			);	
			$default_billing_address=0;
		}

		if($address_id==$default_shipping_id)
		{
			$default_shipping_address=1;
			$mainArray[]=array(
				'id'=>$row1['entity_id'], 
				'title'=>"Shipping Address", 
				'firstname'=>base64_encode($row1['firstname']),
				'lastname'=>base64_encode($row1['lastname']),
				'postcode'=>base64_encode($row1['postcode']),
				'city'=>base64_encode($row1['city']),
				'region'=>base64_encode($row1['region']),
				'region_id'=>base64_encode($row1['region_id']),
				'mobile'=>base64_encode($row1['telephone']),  
				'street_1'=>base64_encode($house_no),
				'street_2'=>base64_encode($road_name), 
				'landmark'=>base64_encode($landmark),
				'address_type'=>base64_encode($address_type), 
				'default_billing_address'=>$default_billing_address,
				'default_shipping_address'=>$default_shipping_address
			);	
			$default_shipping_address=0;
		}
	}	 
	//print_r($mainArray);
	$noDeliveryMsg = '';
	$deliverable   = true;
	$pincode = $connection->fetchOne("SELECT postcode FROM `customer_address_entity` WHERE `entity_id` = '$default_shipping_id'");
	if($pincode){
		$rdata = $connection->fetchRow("SELECT is_active,msg FROM `pincode_details` WHERE `pincode` = '$pincode'");
		if($rdata['is_active']==0){
			$deliverable   = false;
			$noDeliveryMsg = 'Due to covid restriction, we are not shipping to the location selected by you.Please chose a different location.';
			if($rdata['msg']!=null && trim($rdata['msg'])!=''){
				$noDeliveryMsg = $rdata['msg'];				
			}
		}
	}
	
	if(!empty($mainArray))
	{
		$user_array=array('address'=>$mainArray,'user_detail'=>$user_array,'deliverable'=>$deliverable,'message'=>$noDeliveryMsg);
		
		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'List Address Found');  	
		echo json_encode($result_array);	
		die; 
	}
	else
	{
		$user_array=array('address'=>$mainArray,'user_detail'=>$user_array);
		
		
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'No address found');  	
		echo json_encode($result_array);	
		die; 	
	}    
	
	
}
catch(Exception $e) 
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Account not confirmed or Password not matched'); 	
	echo json_encode($result_array);	
	die;
} 






?>

	
