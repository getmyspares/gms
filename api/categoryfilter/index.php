<?php
	// Catgeory's product api. 
	//https://www.dev.idsil.com/design/api/categoryproduct/?cat_id=4&accesstoken=1234&page_no=1&brand_filter=47,48&rating_filter=1,2,3&price_from=1000&price_to=1100
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	
	$result = $_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$product_array = null; 
	$data = null;
	$argumentsCount=count($result);
	$filter=0;
	$user_array=null;
	/* if($argumentsCount < 3 || $argumentsCount > 9)
	{
		$result_array=array('success' => 0,'result' => $product_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{ 
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if(!empty(@get_numeric($result['cat_id']))){
		$categoryId = @get_numeric($result['cat_id']);
	}else{
		$categoryId = '';	
	} 

	if($categoryId ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'category id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$categoryId = @get_numeric($result['cat_id']);
	}
	
	
	if(!empty(@get_numeric($result['page_no']))){
		$page_no = @get_numeric($result['page_no']);
	}else{
		$page_no = '';	
	}
	
	if($page_no ==''){
		$result_array=array('success' => 0, 'result' => $product_array, 'error' => 'page number is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$page_no = @get_numeric($result['page_no']);
	}
	
	
	if(!empty($result['key'])){
		$key = $result['key'];
	}else{
		$key = '';	
	}
	
	if(!empty($result['model'])){
		$model_filter = $result['model'];
		
	}else{
		$model_filter = '';	
	}
	
	// Brand Filter
	if(!empty($result['brand_filter'])){
		$brand_filter = $result['brand_filter'];
		
	}else{
		$brand_filter = '';	
	}
	
	// Rating Filter
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
		
	}else{
		$rating_filter = '';	
	}
	
	
	// Price Filter
	if(!empty($result['price_from'])){
		$price_from = $result['price_from'];
		
	}else{
		$price_from = '';	
	}
	
	if(!empty($result['price_to'])){
		$price_to = $result['price_to'];
		
	}else{
		$price_to = '';	
	}
	
	$sort=4; 
	if(!empty($result['sort'])){
		$sort = $result['sort'];
	}
	
	$search_section=0;  
	if(!empty($result['search'])){ 
		$search_section = $result['search'];
	}   
	
	
	
	
	if(!empty(@get_numeric($result['user_id'])))  
	{ 
		$check_user=$api_helpers->check_user_exists(@get_numeric($result['user_id']));
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array,'search'=>$search_section, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	  
		} 
		
		$authentication_response = $api_helpers->authenticateUser(@get_numeric($result['user_id']),$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	} 
	
	$user_id=0;
	if(!empty(@get_numeric($result['user_id'])))  
	{ 
		$user_id=@get_numeric($result['user_id']);
	}
	//$api_helpers->check_product_special_price('1433');
	
	try
	{
		$defaultArray=array();
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		$category = $categoryFactory->create()->load($categoryId);
		$category_name = trim($category->getName()); 
		$brandFilter=null;
		$modelFilter=null; 
		$priceFilter=array(
			'gt' => '0', 
		);
		
		if(!empty($brand_filter))
		{
			
			if(!empty($brand_filter))
			{
				$filter_brands = explode("-",$brand_filter);
			}
			
			
			foreach($filter_brands as $brand_id)
			{
				$brandFilter[]=array(
					'attribute' => 'brands',
					'eq' => $brand_id
				);		
			} 	
			
		}	
		
		if(!empty($model_filter))
		{
			
			$filter_models = explode("-",$model_filter);
			
			
			foreach($filter_models as $model_id)
			{
				$modelFilter[]=array(
					'attribute' => 'model', 
					'eq' => $model_id
				);		
			}	 
			 
		}	
		
		$categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('entity_id')
			->addAttributeToFilter($brandFilter)
			->addAttributeToFilter($modelFilter)
			->addFinalPrice();

		if(!empty($price_from))
		{	
			$categoryProducts->getSelect()->where("price_index.final_price >=".$price_from)
                  ->where("price_index.final_price <=".$price_to);   	
		}	
			
		$rating_1=0;
		$rating_2=0;
		 
		if(!empty($rating_filter)) 
		{
			
			if($rating_filter==1)
			{
				$rating_1=20;	
				$rating_2=40;	
			}
			else if($rating_filter==2)
			{
				$rating_1=40;	
				$rating_2=60;	
			}	
			else if($rating_filter==3)
			{
				$rating_1=60;	
				$rating_2=80;	
			}	
			else if($rating_filter==4)
			{
				$rating_1=80;	
				$rating_2=100;	
			}	
			else if($rating_filter==5)
			{
				$rating=100;	
			}  	
			
			/*
			$categoryProducts->getSelect()->joinLeft(array('rova'=> 'rating_option_vote_aggregated'),'e.entity_id =rova.entity_pk_value', array("percent" => 'percent'))->group('e.entity_id');
			$categoryProducts->getSelect()->where("rova.percent =".$rating);	
			*/
			
			$categoryProducts->getSelect()->joinLeft(array('rova'=> 'review_entity_summary'),'e.entity_id =rova.entity_pk_value', array("rating_summary" => 'rating_summary'))->group('e.entity_id');
			
			if($rating_filter!=5)
			{	
				$categoryProducts->getSelect()->where("rova.rating_summary >= ".$rating_1."")->where("rova.rating_summary < ".$rating_2."")->where("rova.store_id=1"); 	
			}
			else
			{
				$categoryProducts->getSelect()->where("rova.rating_summary =".$rating)->where("rova.store_id=1");	
			}		
			
		}		
		 
		$need_to_show=10; 
		$product_count = count($categoryProducts); 
		$pages_count = ceil($product_count/$need_to_show);  
	
		
		if($product_count==0)
		{
			$user_array=array();
			$result_array=array('success' => 0,'result' => $user_array,'category_name'=>$category_name, 'error' => 'No Product Found'); 	
			echo json_encode($result_array);	
			die;		
		}	
		if($page_no >$pages_count)
		{
			$result_array = array('success' => 0,'result' => null,'search'=>$search_section, 'error' => "Page number exceed its limit."); 
			echo json_encode($result_array);
			die();
		}
		
		$categoryProducts = $category->getProductCollection()
		->addAttributeToSelect('entity_id')
		->addAttributeToFilter($brandFilter)
		->addAttributeToFilter($modelFilter)
		->addFinalPrice()
		->setPageSize($need_to_show)  
		->setCurPage($page_no)  
		; 
		if($sort==4)
			{
				// $asdasdadasdd=$categoryProducts->getData();
				$assf = $categoryProducts->getData();
			}
		// echo "========================Product data 1============================";
		// var_dump($categoryProducts->getData());
		if(!empty($price_from))
		{	
			$categoryProducts->getSelect()->where("price_index.final_price >=".$price_from)
			->where("price_index.final_price <=".$price_to);   	
		}	
		
		$rating_1=0;
		$rating_2=0;
		
		if(!empty($rating_filter)) 
		{
			
			if($rating_filter==1)
			{
				$rating_1=20;	
				$rating_2=40;	
			}
			else if($rating_filter==2)
			{
				$rating_1=40;	
				$rating_2=60;	
			}	
			else if($rating_filter==3)
			{
				$rating_1=60;	
				$rating_2=80;	
			}	
			else if($rating_filter==4)
			{
				$rating_1=80;	
				$rating_2=100;	
			}	
			else if($rating_filter==5)
			{
				$rating=100;	
			} 	
			
			/*
			$categoryProducts->getSelect()->joinLeft(array('rova'=> 'rating_option_vote_aggregated'),'e.entity_id =rova.entity_pk_value', array("percent" => 'percent'))->group('e.entity_id');
			$categoryProducts->getSelect()->where("rova.percent =".$rating);	
			*/
			
			$categoryProducts->getSelect()->joinLeft(array('rova'=> 'review_entity_summary'),'e.entity_id =rova.entity_pk_value', array("rating_summary" => 'rating_summary'))->group('e.entity_id');
			
			if($rating_filter!=5)
			{	
				$categoryProducts->getSelect()->where("rova.rating_summary >= ".$rating_1."")->where("rova.rating_summary < ".$rating_2."")->where("rova.store_id=1"); 	
			}
			else
			{
				$categoryProducts->getSelect()->where("rova.rating_summary =".$rating)->where("rova.store_id=1");	
			}		
			
		}	
		
		if($sort==1) 
		{
			$categoryProducts->addAttributeToSort('price', 'asc'); 
		}
		else if($sort==2)
		{
			$categoryProducts->addAttributeToSort('price', 'desc');  
		}	
		else if($sort==3)
		{
			$categoryProducts->getSelect()->joinLeft(array('rova'=> 'review_entity_summary'),'e.entity_id =rova.entity_pk_value', array("rating_summary" => 'rating_summary'))->group('e.entity_id');  
			$categoryProducts->getSelect()->order('rating_summary desc');   
		}
		else if($sort==4) 
		{
			
			$categoryProducts->getSelect()
			->joinLeft(
				array('order_items' => $categoryProducts->getResource()->getTable('sales_order_item')),
				'order_items.product_id = e.entity_id',
				array('qty_ordered' => 'SUM(order_items.qty_ordered)')
				)
				->group('e.entity_id')
				->order('qty_ordered DESC');
			}
			
			
			else 
			{
				$categoryProducts->addAttributeToSort('price', 'asc'); 
			}	
			$first_array=array();
			// echo "========================Product data 2============================";
			// var_dump($categoryProducts->getData());
			if($sort==4)
			{
				$asdasdadasdd=$categoryProducts->getData();
			}
			
			foreach($categoryProducts->getData() as $product_foreach)
			{
				$defaultArray[]=$product_foreach['entity_id']; 
				$is_special=0;
				$ratings=0;
				$product_iddd=$product_foreach['entity_id'];
				$price=$api_helpers->get_product_prices($product_iddd);		
				
				
				$orgprice = $api_helpers->get_product_prices($product_iddd);
				
				
				$specialprice = $api_helpers->get_product_special_prices($product_iddd);
				
				$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
				$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
				$today = time(); 
				
				if (!$specialprice)
				{	
					$specialprice = $orgprice;
				} else
				{
					if(!is_null($specialfromdate) && !is_null($specialtodate))
					{
						if($today >= strtotime($specialfromdate) &&  $today <= strtotime($specialtodate))
						{
						$is_special=1;			
					}	else 
					{
						$is_special=0;
					}
				} else{
					$is_special=1;
				}
			}
			
			
			if($is_special==1)
			{
				$sort_price = number_format($specialprice, 2, '.', '');
				
			} 	
			else
			{
				$sort_price = number_format($price, 2, '.', ''); 
				
			}

			
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_iddd);
			if(!empty($RatingOb->getCount()))
			{ 
				$ratings = $RatingOb->getSum()/$RatingOb->getCount();
			}
			
			
			$sold_number= $api_helpers->get_num_product_sold($product_iddd);
			
			
			$first_array[]=array(
				'id'=>$product_iddd,
				'price'=>$price,
				'sort_price'=>$sort_price,
				'rating_count'=>$ratings, 
				'sold_number'=>$sold_number,  
			
			); 
			
			
			
		}
		
		// print_r($first_array);   
		 

		$dataaa= $first_array;
		if(!empty($dataaa))
		{
			foreach($dataaa as $row)
			{
				$product_id=$row['id'];	
				$sort_price=$row['sort_price'];	
				$product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
				$product_array['id']= $product->getId();
				$product_array['name']= $product->getName();
				$product_array['sku']= $product->getSku();   
				if($product->getSupportedModels()!='')
				{	
					$product_array['supported_models']= str_replace(',',', ',$product->getSupportedModels());
				}
				else 
				{
					$product_array['supported_models']= null;
				}		
				//$product_array['sort_price']=$sort_price;
				$product_array['price']= number_format($product->getPrice(), 2, '.', '');
				if($user_id!=0)  
				{ 
					
					$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
					$product_array['wishlist']=$whishlist;
				}
				else
				{
					$product_array['wishlist']=null;
				}
				if($product->getAttributeText('brands')){
					$product_array['brand']= $product->getAttributeText('brands');
				}else{
					$product_array['brand']= null;	 
				}
				
				
				
				// From Rma extension
				$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
				if($product->getAttributeText('return_period')=="Flat return period"){
					$product_array['return_days']= $return_date;
				}
				
				
				
				 
				$imageUrl=$api_helpers->get_product_thumb($product_id);
				
				$product_array['image']=$imageUrl;
				 
				
				
				
				 
				$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 
				$product_array['earn_reward'] = $earnOutput->getProductPoints($product);
				
				$ratings ='';
				$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product_id);
				if(!empty($RatingOb->getCount())){
					$ratings = $RatingOb->getSum()/$RatingOb->getCount();
				}
				if(!empty($ratings)){
					$product_array['rating_count'] = $ratings;
				}else{
					$product_array['rating_count'] = 0;
				}
				
				
				$product_array['sold_number']= $api_helpers->get_num_product_sold($product->getId());
				
				$product_iddd=$product->getId();
				$is_special=0; 
				$price=$product->getPrice();	
				$orgprice = $product->getPrice();
				$specialprice =$product->getSpecialPrice(); 
				$specialfromdate = $product->getSpecialFromDate(); 
				$specialtodate =  $product->getSpecialToDate(); 
				$today = time();  
				
				if (!$specialprice)
				{	
					$specialprice = $orgprice;
				}
				else
				{
					if(!is_null($specialfromdate) && !is_null($specialtodate))
					{
						$specialfromdate=strtotime($specialfromdate);
						$specialtodate=strtotime($specialtodate);
						
						if($today >= $specialfromdate &&  $today <= $specialtodate)
						{
							$is_special=1;			
						}	
					}
				}
				
				
				if($is_special==1)
				{
					$product_array['sort_price'] = number_format($product->getSpecialPrice(), 2, '.', '');
					$product_array['specialprice'] = number_format($product->getSpecialPrice(), 2, '.', '');	
					$product_array['specialfromdate'] = $product->getSpecialFromDate(); 
					$product_array['specialtodate'] = $product->getSpecialToDate();
				} 	
				else
				{
					$product_array['sort_price']= number_format($product->getPrice(), 2, '.', ''); 
					$product_array['specialprice'] = null;
					$product_array['specialfromdate'] = null;
					$product_array['specialtodate'] = null;
				}
				
				
				$data[] = $product_array;		
			}	
		}
		
		$result_array = array('success' => 1, 'result' =>$data, 'category_name'=>$category_name, 'product_count'=>$product_count, 'pages_count'=>$pages_count, 'error' =>"Product assigned to this category."); 
		echo json_encode($result_array);
		die();  
	
	
	}
	catch(Exception $e)
	{
		 
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $data, 'error' => 'No product found');  
		echo json_encode($result_array);	
		
	}
?>