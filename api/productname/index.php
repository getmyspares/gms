<?php
	// Catgeory listing on main menu api.
	//	Url Example :- https://dev.idsil.com/api/mainmenu/?accesstoken=1234
	
	
	require "../security/sanitize.php";

	
	$header = apache_request_headers(); 
	 
	//print_r($header);
	
	/*   
	foreach ($header as $headers => $value) { 
		echo "$headers: $value <br />\n"; 
	} 

	die;	 */  
	
	
	
	
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	//print_r($result);
	
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$null = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $null , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $null, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $null, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	 
	try
	{
		
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		
		$collection = $productCollection->addAttributeToSelect('entity_id')
				->addAttributeToSelect('name') 
				->addAttributeToFilter('status', '1')
				->load();

		foreach ($collection as $product){
			
			
			$productArray[]=array(
				
				'id'=>$product->getId(),
				'name'=>$product->getName()
			);
		}  
		
		
		echo json_encode($productArray);
		
		
		//print_r($productArray);
		die;
		
		
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$null, 'error' => $e->getMessage()); 	
	} 
	 
	echo json_encode($result_array);
?>