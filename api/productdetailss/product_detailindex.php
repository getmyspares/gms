<?php

require "../security/sanitize.php";
$result = sanitizedJsonPayload();
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

//https://www.dev.idsil.com/design/api/productdetails/?product_id=1399&accesstoken=1234
$result = $_GET;
$productdata = null;
$data = null;
$argumentsCount=count($result);
$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');	

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
if(!empty($result['accesstoken'])){
	$accesstoken = $result['accesstoken']; 
}else{
	$accesstoken = '';	
}

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'result' => $data, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}


if(!empty(@get_numeric($result['product_id']))){
	$product_id = @get_numeric($result['product_id']);
}else{
	$product_id = '';	
}

if($product_id ==''){
	$result_array=array('success' => 0, 'result' => $data, 'error' => 'Product id is missing'); 	
	echo json_encode($result_array);	
	die;
}else{
	$product_id = @get_numeric($result['product_id']);
}
if(!empty(@get_numeric($result['user_id']))){
	$user_id = @get_numeric($result['user_id']); 
}else{
	$user_id = '';	
}
if(!empty($user_id)) 
{
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$user_array=null;
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 
	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}

	$api_helpers->store_user_token($user_id); 
}  
try 
{ 

	/* clustereed */
	$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
	$product_id =  $product->getId();
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$currentStoreId = $storeManager->getStore()->getId();
	$rating = $objectManager->get("Magento\Review\Model\ResourceModel\Review\CollectionFactory");
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
	$webkul_helper = $objectManager->create('Webkul\Marketplace\Helper\Data');

	$collection = $rating->create()->addStoreFilter($currentStoreId)
		->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
		->addEntityFilter('product',$product->getId())
		->setDateOrder();
	
		$total_rating = 0 ;
		$star_5=0;$star_4=0;$star_3=0;$star_2=0;$star_1=0;
	if(!empty($collection->getData())){
			$d=0;
			$review_data = $collection->getData(); //Get all review data of product
		foreach($review_data as $data){
			$revieww_idd=0; 
			$revieww_idd=$data['review_id'];
			$reviewdata['review_id']= $revieww_idd;
			$reviewdata['created_at']= $data['created_at'];
			$reviewdata['entity_id']= $data['entity_id'];
			$reviewdata['entity_pk_value']= $data['entity_pk_value'];
			$reviewdata['status_id'] = $data['status_id'];
			$reviewdata['detail_id'] = $data['detail_id'];
			$reviewdata['title'] = $data['title'];
			$reviewdata['detail'] = trim($data['detail']);   
			$reviewdata['nickname'] = $data['nickname'];
			$reviewdata['entity_code'] = $data['entity_code'];
			$ratingCollection = $objectManager->get('Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection')
			->addFieldToFilter('review_id',$revieww_idd);  
			$rating_value = $ratingCollection->getData();
			$ratings_percentage = null;
			$ratings_percentage = $rating_value[0]['percent'];
			$total_rating +=$ratings_percentage;
			$rating_count = $ratings_percentage/20;
			
			$select = $connection->select()
			  ->from('rating_option_vote') 
			  ->where('review_id= ?',$revieww_idd);
			$result = $connection->fetchAll($select);
			
			$review_ratings=$result[0]['value'];
			
			if (($ratings_percentage >= 0) && ($ratings_percentage <= 20)){
				$star_1 += 1;
			}else if(($ratings_percentage >= 21) && ($ratings_percentage <= 40)){
				$star_2 += 1;
			}else if(($ratings_percentage >= 41) && ($ratings_percentage <= 60)){
				$star_3 += 1;
			}else if(($ratings_percentage >= 61) && ($ratings_percentage <= 80)){
				$star_4 += 1;
			}else if(($ratings_percentage >= 81) && ($ratings_percentage <= 100)){
				$star_5 += 1;
			} 
			$reviewdata['rating'] = $review_ratings;
			$reviewsdata[] = $reviewdata;
			$d++;
		}
	} else {
		$review_data = null;
		$reviewsdata = null;		 
	}
	if(empty($star_5)){
		$star_5=0;
	}
	if(empty($star_4)){
		$star_4=0;
	}
	if(empty($star_3)){
		$star_3=0;
	}
	if(empty($star_2)){
		$star_2=0;
	}
	if(empty($star_1)){
		$star_1=0;
	}
	$star_total =$star_5+$star_4+$star_3+$star_2+$star_1;
	if($star_total>0){
		$star_5 = ($star_5/$star_total)*100;
		$star_4 = ($star_4/$star_total)*100;
		$star_3 = ($star_3/$star_total)*100;
		$star_2 = ($star_2/$star_total)*100;
		$star_1 = ($star_1/$star_total)*100;
	}

	$productdata['allow_add_to_cart']="1";
	$productdata['id']= $product->getId();
	$productdata['name']= strip_tags($product->getName());
	$productdata['sku']= strip_tags($product->getSku());
	$productdata['hsn']= strip_tags($product->getHsn());
	$is_special=0; 
	
	$price=$product->getPrice();		
	// $orgprice = $api_helpers->get_product_prices($product_id);
	$orgprice = $price;
	
	$specialprice = $product->getSpecialPrice();
	$specialfromdate = $product->getSpecialFromDate();
	$specialtodate =  $product->getSpecialToDate();
	
	$today = time(); 
	if (!$specialprice)
	{	
		$specialprice = $orgprice;
	} else
	{
		if(!is_null($specialfromdate) && !is_null($specialtodate))
		{
			if($today >= strtotime($specialfromdate) &&  $today <= strtotime($specialtodate))
			{
				$is_special=1;			
			}	
		}
	}

	if($is_special==1)
	{
		$productdata['price']=number_format($price, 2, '.', '');
		$productdata['specialprice']=number_format($specialprice, 2, '.', '');
		$productdata['specialfromdate'] = $specialfromdate;
		$productdata['specialtodate'] = $specialtodate;
		$productdata['final_price'] = number_format($specialprice, 2, '.', ''); 
	}  	
	else
	{
		$productdata['price']= strip_tags($price);
		$productdata['specialprice'] = null;
		$productdata['specialfromdate'] = null;
		$productdata['specialtodate'] = null;
		$productdata['final_price'] = number_format($price, 2, '.', '');
	}
	$productdata['product_url'] =$product->getProductUrl();
	$price = $price;
	$productdata['price'] = number_format($price, 2);
	$specialPriceFromDate = $specialfromdate;
	$specialPriceToDate = $specialtodate; 
	$today = time();
		
	if($price){
		$sale = round((($price-$specialprice)/$price)*100);
	}
	if ($specialprice) {
		if(!empty($specialPriceFromDate)){
			if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
				$productdata['specialprice'] = number_format($specialprice, 2);
				$productdata['specialfromdate'] = $specialPriceFromDate;
				$productdata['specialtodate'] = $specialPriceToDate;
				$productdata['sale_percentage'] = $sale.'%'.'Off';
			}
		}
	}
	$images_array = null;
	$images = $product->getMediaGalleryImages();
	$key = 0;
	$base_64='';
	if(!empty($images)){
		foreach($images as $image){
			$images_array[$key] = $image->getData();
			$imagess["value_id"] = $image->getData('value_id');
			$imageUrl = $_imageHelper->init($product, 'product_page_image_large')->setImageFile($image->getFile())->resize(650,650)->getUrl();
			$imagess["url"] = $imageUrl;  
			// $imagess["file"] = $image->getFile();
			$url = $imageUrl;
			$images_array[$key] = $imagess;
			$key++;
		}
	}

	if(!empty($images_array)){
		$productdata['images']= $images_array;
		$product_thumnail = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('500','500')->getUrl();
	}else{
		$site_url = $storeManager->getStore()->getBaseUrl();
		$imageUrl = $site_url.'Customimages/product-thumb.jpg';
		$images_array[0] = array("value_id"=>1,"url"=>$imageUrl);
		$productdata['images']= $images_array;
		$product_thumnail = $imageUrl; 
	}
	
	// $product_thumnail=$api_helpers->get_product_thumb($product_id);

	/* alternate for  url encode  to resolve spaces in url @ritesh3nov2020 */
	// $imageUrl = escapefile_url($imageUrl);

	/* experimental */
	// $curl = curl_init();
	// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	// curl_setopt($curl, CURLOPT_URL, '$imageUrl');
	// $b64image = curl_exec($curl);
	// curl_close($curl);
	/* experimental */
	
	// $b64image = file_get_contents($imageUrl);



	// $base_64='data:image/jpg;base64,'.base64_encode($b64image);
	// $productdata["base64"] = $base_64;
	$productdata['product_thumb']=$product_thumnail;
	$productdata['short_description']= strip_tags($product->getShortDescription());
	$productdata['tax_class_id']= $product->getTaxClassId();
	$in_stock=1;
	$product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
	if($product_qty==0)
	{
		$in_stock=0; 
	}
	if($product_qty==1)
	{
		$in_stock=0; 
		$productdata['stock_Qty']= $product_qty;
	}	
	else
	{
		$productdata['stock_Qty']= $product_qty;
	}	
	if(!$product->isSalable())
	{
		$in_stock=0;
	}	
	
	/* make  product out of  stock if  salable  quantity is  less  than minimum salable quantity */
	$min_sale_quantity = (int)$product->getExtensionAttributes()->getStockItem()->getData('min_sale_qty');
	if($productdata['stock_Qty']<$min_sale_quantity)
	{
		$in_stock=0;	
	}
	$productdata['min_sale_quantity']= $min_sale_quantity; 
	
	
	
	$productdata['in_stock']= $in_stock; 
	$productdata['description']= strip_tags($product->getDescription());
	$productdata['manufacturer']= $product->getAttributeText('manufacturer');
	$productdata['manufacturing_brand']= $product->getAttributeText('brands');
	$productdata['support_brands']= $product->getSupportBrands(); 
	$productdata['genuinity']= $product->getGenuinity();//genuinity
	if($product->getUniversal()!='')
	{
		$productdata['universal']='Yes';
		if($product->getUniversal()==0)
		{
			$productdata['universal']='No';
		}		
	}	
	else
	{
		$productdata['universal']=$product->getUniversal(); 
	}		
		
	$productdata['part_name']= $product->getPartName();//universal
		
	$warranty_units=$product->getData('warranty_units');
	if($warranty_units!='')
	{	
		$newWarrenity = $product->getData('warranty'); 
		if(empty($newWarrenity))
		{
			$newWarrenity=0;
		}
		$productdata['warranty_details']= (int)$newWarrenity.' '.$product->getData('warranty_units');  
	}
	else
	{
		$productdata['warranty_details']= null;
	}		 
	$productdata['warranty_link']= $product->getWarrantyLink();
	$productdata['return_period']= $product->getAttributeText('return_period');
	$productdata['standard_return_days']= '';
	$productdata['return_policy']= $product->getReturnPolicy();
	$productdata['part_manual']= $product->getPartManual();
	$productdata['disclaimer']= $product->getDisclaimer();
	$productdata['product_storage']= $product->getAttributeText('product_storage');
	$productdata['part_number']= $product->getItemModelNumber();
	$productdata['model_number']= $product->getPiModel();
	$productdata['material']= $product->getPartBodyMaterial();	
	$productdata['supported_models']= $product->getSupportedModels();
		
		if(is_numeric($product->getProductLength()))
		{
			$productdata['length']= $product->getProductLength().' cm';	
		}
		else
		{
			$productdata['allow_add_to_cart']="0";
			// $result_array=array('success' => 0, 'result' =>$data, 'error'=>'Product Length is not valid.');
			// echo json_encode($result_array);
			// die(); 
		}		
		if(is_numeric($product->getProductBreadth()))
		{
			$productdata['breadth']= $product->getProductBreadth().' cm';
		}
		else
		{
			$productdata['allow_add_to_cart']="0";

			// $result_array=array('success' => 0, 'result' =>$data, 'error'=>'Product Breadth is not valid.');
			// echo json_encode($result_array);
			// die(); 
		}	
		if(is_numeric($product->getProductHeight()))
		{
			
			$productdata['height']= $product->getProductHeight().' cm';
		}
		else
		{
			$productdata['allow_add_to_cart']="0";

			// $result_array=array('success' => 0, 'result' =>$data, 'error'=>'Product Height is not valid.');
			// echo json_encode($result_array);
			// die();  
		}	
		if(is_numeric($product->getProductWeight()))
		{
			$productdata['weight']= number_format($product->getProductWeight(), 2, '.', '').' kgs'; //ecom_length	
		} 
		else
		{
			$productdata['allow_add_to_cart']="0";

			// $result_array=array('success' => 0, 'result' =>$data, 'error'=>'Product Weight is not valid.');
			// echo json_encode($result_array); 
			// die(); 
		}	 
		$warranty_units = $product->getData('warranty_units'); 
		$part_type=$product->getPartType();
		if($part_type!='')
		{
			
			$select = $connection->select()
				  ->from('eav_attribute_option_value')  
				  ->where('option_id = ?', $part_type);
				 
			$result = $connection->fetchAll($select);	
			$part_type='';
			if(!empty($result))
			{	
				$part_type=$result[0]['value'];
			}
		}
		else
		{
			$part_type='';
		}		
		$productdata['color']= $product->getAttributeText('color'); //color
		$productdata['part_category']= $product->getPartCategory(); //part_category
		
		$productdata['part_body_material']= $product->getAttributeText('part_body_material'); //part_body_material
		$productdata['part_type']= $part_type;
		$productdata['expertise_required']= $product->getExpertiseRequired(); //part_body_material
		$productdata['boxed']= $product->getAttributeText('boxed'); //boxed
		
		
		$star_count_1=$api_helpers->get_star_count($product_id,1);	
		$star_count_2=$api_helpers->get_star_count($product_id,2);	
		$star_count_3=$api_helpers->get_star_count($product_id,3);	
		$star_count_4=$api_helpers->get_star_count($product_id,4);	
		$star_count_5=$api_helpers->get_star_count($product_id,5);	 
		if(!empty($reviewsdata)){
		$productdata['review_data']= $reviewsdata; //review data
		}else{
			$productdata['review_data']=null;
		}
		$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product->getId());
		$ratings=null;
		if(!empty($RatingOb->getCount())){

			$ratings = $total_rating/$RatingOb->getCount();
		}
		$rating_counts=0;
		
		if(!empty($reviewsdata))
		{
			$product_idsssa=$product->getId();
			
			$select = $connection->select()
				  ->from('rating_option_vote_aggregated')  
				  ->where('entity_pk_value = ?', $part_type);
			
			$sql = "SELECT * FROM `rating_option_vote_aggregated` WHERE `entity_pk_value`='".$product_idsssa."' and store_id='1'";
			$result_ss = $connection->fetchAll($sql);
			if(!empty($result_ss))
			{
				$rating_counts=$result_ss[0]['vote_count'];
			}	 
			$rating_counts=count($reviewsdata);
		}
		$productdata['rating_counts'] = $rating_counts;
		if(!empty($productdata['rating_counts']))
		{	
			$rating_counts=$productdata['rating_counts'];
		}
		$star_per_1=0;
		$star_per_2=0;
		$star_per_3=0;
		$star_per_4=0;
		$star_per_5=0;
		if($rating_counts!=0) 
		{	
			$star_per_1=round(($star_count_1*100)/$rating_counts);
			$star_per_2=round(($star_count_2*100)/$rating_counts);
			$star_per_3=round(($star_count_3*100)/$rating_counts);
			$star_per_4=round(($star_count_4*100)/$rating_counts);
			$star_per_5=round(($star_count_5*100)/$rating_counts);   
			$star_per_11=($star_count_1*100)/$rating_counts;
			$star_per_22=($star_count_2*100)/$rating_counts;
			$star_per_33=($star_count_3*100)/$rating_counts;
			$star_per_44=($star_count_4*100)/$rating_counts;
			$star_per_55=($star_count_5*100)/$rating_counts;
			if($star_per_1>100)
			{
				$star_per_1=100;
			}		
			
			if($star_per_2>100)
			{
				$star_per_2=100;
			}		
			
			if($star_per_3>100)
			{
				$star_per_3=100;
			}		
			
			if($star_per_4>100)
			{
				$star_per_4=100;
			}		
			
			
			if($star_per_5>100)
			{
				$star_per_5=100; 
			}		
		
		
		} 
		$stars[] = array("star_5"=>$star_per_5, "star_4"=>$star_per_4, "star_3"=>$star_per_3, "star_2"=>$star_per_2, "star_1"=>$star_per_1);
		$productdata['review_total_data']= $stars;
		
		if(!empty($product->getRatingSummary()))
		{
			$ratings = $product->getRatingSummary()->getRatingSummary();
		}
		$count_total_vote=0;
		$sql="SELECT * FROM `rating_option_vote` where entity_pk_value='".$product->getId()."'";
		$result_count = $connection->fetchOne($sql);	
		if(!empty($result_count))
		{
			if(!is_array($result_count))
			{
				$count_total_vote=$result_count;
			} else  
			{
				foreach($result_count as $row)
				{
					$count_total_vote=$count_total_vote+$row['value'];
				}				
			}
	
		}	
		$complete_rating=0;
		if(!empty($ratings)){
			$complete_rating=$ratings/20;
			$productdata['rating'] = number_format($ratings, 2, '.', '');
			$productdata['complete']=$complete_rating;
		}else{	
			$productdata['rating'] = null;	
			$productdata['complete']=0;
		}
		
		$sellerId = null;
		/* removed  object dependency */
		$sql="SELECT `seller_id` FROM `marketplace_product` where mageproduct_id='$product_id'";
		$sellerId = $connection->fetchOne($sql);


		if(!empty($sellerId)){
			
			$captchenable = $webkul_helper->getCaptchaEnable();
			$sellerInfo = $webkul_helper->getSellerInfo($sellerId);
			$sellerData = $webkul_helper->getSellerDataBySellerId($sellerId);
			$shopTitle = $sellerInfo['shop_title'];
			$shopUrl = $sellerInfo['shop_url'];
			$logo = $sellerInfo['logo_pic'];
			$productCount = $sellerInfo['product_count'];
			$companyLocality = trim($sellerInfo['company_locality']);
			if ($shopTitle=='') 
			{
				$shopTitle = $shopUrl;
			}
			else
			{
				$shopTitle=$api_helpers->remove_special($shopTitle);	
			}		
			$seller_array = $sellerData->getData();
			$seller_description = $seller_array[0]['company_description'];
			$productdata['seller_Id'] = $sellerId;
			$orderHelper = $objectManager->create('Webkul\Marketplace\Helper\Orders');
			$order_counts = $orderHelper->getSellerOrders($sellerId);
			$productdata['seller_shopTitle'] = $shopTitle;
			$productdata['seller_shopUrl'] = $shopUrl;
			$productdata['seller_description'] = strip_tags($seller_description);
			if(!empty($logo)){
				$productdata['seller_logo']= $webkul_helper->getMediaUrl().'avatar/'.$logo;
			}else{
				$productdata['seller_logo']= $webkul_helper->getMediaUrl().'avatar/noimage.png';
			}
			$productdata['seller_collectionPageUrl'] = $webkul_helper->getRewriteUrl('marketplace/seller/collection/shop/'.$shopUrl);
			$productdata['seller_profilePageUrl'] = $webkul_helper->getRewriteUrl('marketplace/seller/profile/shop/'.$shopUrl);
			$productdata['seller_productCount'] = $productCount;
			$productdata['seller_companyLocality'] = $companyLocality;
			$productdata['seller_order_count'] = $order_counts;
			$productdata['seller_feeds'] = $webkul_helper->getFeedTotal($sellerId);
			$productdata['seller_rating'] = $webkul_helper->getSelleRating($sellerId);
		}
		
		if($user_id!='')    
		{   
			
			$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
			$productdata['wishlist']=$whishlist;
			 
		}
		else   
		{
			$productdata['wishlist']=null;
		}  
		
		$in_cart=0;
		if($user_id!='')    
		{   
			
			$product->getId();
			$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
			$quoteItems=$quote->getAllVisibleItems();
			$items = $quote->getAllItems();
			if(!empty($items))
			{	
				foreach($items as $item)   
				{
					$product_id=$item->getProductId();
					if($product_id==$product->getId())
					{
						$in_cart=1;	
						break;
					}		
				}		
			} 
		}
		$productdata['in_cart']=$in_cart;
		$productdata['sell_with_us']='https://getmyspares.com/customuser/index/sellerregister/';
		$data = $productdata;
		if(!empty($data)){
			$result_array=array('success' => 1, 'result' =>$data, 'error'=>'Product id is valid.');
			echo json_encode($result_array);
			die();
		}else{
			
			$result_array=array('success' => 0, 'result' =>$data, 'error'=>'These is no data.');
			echo json_encode($result_array);
			die();
		}

	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$data, 'error' => 'This product have no proper data');
		echo json_encode($result_array);  
		die();		
	} 
	
	 
	function escapefile_url($url){
		$parts = parse_url($url);
		$path_parts = array_map('rawurldecode', explode('/', $parts['path']));
	  
		return
		  $parts['scheme'] . '://' .
		  $parts['host'] .
		  implode('/', array_map('rawurlencode', $path_parts))
		;
	  }
	
?>
