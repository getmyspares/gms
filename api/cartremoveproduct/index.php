<?php
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/add_to_cart_db/
	//Input 
	///*{"accesstoken": "1234","product_id":"1375","user_id":"269"}*/
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$result = sanitizedJsonPayload();
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$response = null;
	$argumentsCount=count($result);
	$user_array=null;
	if($argumentsCount < 3 || $argumentsCount > 3) 
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	
	
	
	
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 

		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	}
	
	
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$product_id = @get_numeric($result['product_id']);
		$check_product=$api_helpers->check_product_exist($product_id); 
		if($check_product==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
			echo json_encode($result_array);	
			die; 
		}
	}
	
	 
	try{
		
		
		$token=$api_helpers->get_token();
		$quote_id=$api_helpers->get_quote_id($token,$user_id);
		$quote_id=json_decode($quote_id);
		
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$allItems=$quote->getAllVisibleItems();
	
	
		$itemproductArray=array();
		foreach ($allItems as $item) {
			
			$itemproductArray[] = $item->getProductId();
			
		}
		
		
		//print_r($itemproductArray);
		
		if (!in_array($product_id, $itemproductArray)) 
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product Id not exists'); 	
			echo json_encode($result_array);	 
			die; 		
		}	 
		else
		{
			foreach ($allItems as $item) {
			
				$itemId = $item->getItemId();
				$ProductId = $item->getProductId();
				if($ProductId==$product_id)
				{	
					$product_id;
					$quoteItem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
					$quoteItem->delete();
					$api_helpers->removecart($user_id,$product_id);
					$response=array('user_id'=>$user_id,'product_id'=>$product_id);
					$result_array=array('success' => 1, 'result' => $response, 'error' => 'Product removed from cart'); 	
					echo json_encode($result_array);	
					die;  
				}
			}	
		} 
		
		
	}catch(Exception $e){		

		$result_array = array('success' => 0,'result' => null, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
	
?>