<?php
	// Top search product api. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	 // print_r($result);
	
	$payment_method = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 1)
	{
		$result_array=array('success' => 0,'result' => $payment_method , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $payment_method, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $payment_method, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	
	

	try
	{
		
		//$payment_method = array('Credit Card','Debit Card','Net Banking','Wallets','COD');
		$payment_method = array('0' => 'Credit Card',
                        '1' => 'Debit Card',
                        '2' => 'Net Banking',
                        '3' => 'Wallets'
                        //'4' => 'COD'
                    );
		// $payment_method = array("0"=>"Credit Card", "1"=>"Debit Card", "2"=>"Net Banking", "3"=>"Wallets", "4"=>"COD");

		
		if(!empty($payment_method)){
			$result_array = array('success' => 1, 'result' => $payment_method, 'error' =>"Payment Methods Data."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $payment_method, 'error' => "There is not payment methods."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $product_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>