<?php
	// Check delivery_avalability
	//https://dev.idsil.com/design/api/deliveryavailability/?product_id=1399&zip=140604&qty=2&&accesstoken=1234

	require "../security/sanitize.php";
	
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	// print_r($result);
	// die();
	
	$response = null;
	$argumentsCount=count($result);

	if($argumentsCount < 4 || $argumentsCount > 4)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$product_id = @get_numeric($result['product_id']);
	}
	
	
	if(!empty($result['zip'])){
		$zip = $result['zip'];
	}else{
		$zip = '';	
	}
	
	if($zip ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'zipcode is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$zip = $result['zip'];
	}
	
	if(!empty(@get_numeric($result['qty']))){
		$qty = @get_numeric($result['qty']);
	}else{
		$qty = '';	
	}
	
	if($qty ==''){
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'product qty is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$qty = @get_numeric($result['qty']);
	}
	

	try
	{
        // $product_id = @get_numeric($result['product_id']);
        // $qty = @get_numeric($result['qty']);
        // $zip = $result['zip'];
        $can_cod_flag = false;
        $can_ship_flag = false;
        $days_to_deliver = '-';
        $zip_not_found = true;
		// get the collection and filter it zipcode
        //$pincode_model = $objectManager->create('Ced\PincodeChecker\Model\Pincode')->getCollection()->addFieldToFilter('vendor_id','admin')->addFieldToFilter('zipcode',array('eq'=>$zip))->getData();      
		$pincode_model = $objectManager->create('OM\Pincodes\Model\Pincodes')->getCollection()->addFieldToFilter('is_active','1')->addFieldToFilter('pincode',array('eq'=>$zip))->getData();
		
		$cod_text='';
		$ship_text=''; 
		$custom_msg=''; 
		
		
		if(count($pincode_model)){
            foreach($pincode_model as $model){
                if($model['cod_available'] == '1')
                    $can_cod_flag = true;
                if($model['is_active'] == '1')
                    $can_ship_flag = true;
            
                $days_to_deliver = $model['days_to_deliver'];
				$plusone = $days_to_deliver+1;
            }
            
            $can_cod_flag = true;
        
			if($can_cod_flag==true) {	
				//$cod_text='<span>COD Available</span>';
				$cod_text='';
			}
			
			if($can_ship_flag==true) {	
				$qty=1; 
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
				$shipping_rate=$helpers->ecom_price_by_product($zip,$product_id,$qty);
				if($shipping_rate=='0')
				{
					$shipping_rate="Free Shipping";
					$ship_text ='<span>Free Shipping: ₹ '.$shipping_rate.'</span><span>Delivered in next '.$days_to_deliver.'-'.$plusone.' '.'Business Days</span>';
					$delivery_text='<span>Free Shipping . Delivered in next '.$days_to_deliver.'-'.$plusone.' '.'Business Days</span>';
				} else 
				{
					
					$ship_text ='<span>Shipping: ₹ '.$shipping_rate.'</span><span>Delivered in next '.$days_to_deliver.'-'.$plusone.' '.'Business Days</span>';
				}

				$ship_amount=$shipping_rate;
				$delivery_text='<span>Delivered in next '.$days_to_deliver.'-'.$plusone.' '.'Business Days</span>';
			} 
			 
			$custom_msg=$cod_text.$ship_text;

		}
		
		 $codes = array('{{from-days}}', '{{to-days}}', '{{pincode}}');
        //$msg = $this->getDeliveryDaysMessage((int)$days_to_deliver, $zip);
        $msg = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('pincode/general/delivery_text', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
		$margin_days = (int) $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('pincode/general/delivery_days_margin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		//echo '<br>';
		$msg = null;
		
		if($days_to_deliver!= '-'){
			$replace_string = array($days_to_deliver, $days_to_deliver+$margin_days, $zip);
			$msg = str_replace($codes,$replace_string,$msg);
		
			$msg = strip_tags($msg);
			
		}

		$custom_msg = strip_tags($custom_msg);
		
		if(($can_cod_flag==true)||($can_ship_flag==true)){
		
			$response = array(
                        'ship' => $can_ship_flag,
                        'days' => $days_to_deliver,
                        'zip_not_found' => $zip_not_found,
                        'ship_amount' => strip_tags($ship_amount)
                        //'delivery_text' => strip_tags($delivery_text)
                    ); 
		}
		
		// print_r($response);
		// die('Here');

		if(!empty($response)){
			$result_array = array('success' => 1, 'result' => $response, 'error' =>"The zipcode delivery details."); 
			echo json_encode($result_array);
		}else{
			$result_array = array('success' => 0, 'result' => $response, 'error' => "There is no delivery details about this zipcode."); 
			echo json_encode($result_array);
			die();
		}
	
	}catch(Exception $e){
		

		$result_array = array('success' => 0,'result' => $response, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
		
		
	}
?>
