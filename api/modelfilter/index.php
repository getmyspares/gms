<?php
	// Get parent category details. 
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$result = $_GET;
	 // print_r($result);
	
	$parent_model_array = null;
	$final_array = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 2 || $argumentsCount > 6)
	{
		$result_array=array('success' => 0,'result' => $parent_model_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);
		die;
	} */
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	if(!empty($result['category_id'])){
		$categoryId = $result['category_id'];
	}else{
		$categoryId = '';	
	}
	
	if($categoryId=='')
	{
		$result_array=array('success' => 0, 'result' => $parent_model_array, 'error' => 'category is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	if(!empty($result['brand_filter']))
	{
		$brand_filter = $result['brand_filter'];
	}
	else
	{
		$brand_filter = '';	 
	}
		
	if(!empty($result['model'])){
		$model_filter = $result['model'];
		
	}else{
		$model_filter = '';	
	}	
	
	if(!empty($result['rating_filter'])){
		$rating_filter = $result['rating_filter'];
		
	}else{
		$rating_filter = '';	
	}

	
		
	if(!empty($brand_filter))
	{
		$filter_brands = explode(",",$brand_filter);
	}		
		
	
	if(!empty($model_filter))
	{
		$filter_models = explode(",",$model_filter);
	} 
	
	if(!empty($rating_filter))
	{
		$filter_ratings = (explode(",",$rating_filter));
	} 
	
	
	$star_1=$star_2=$star_3=$star_4=$star_5=0;
	
	try
	{
		
		
		$defaultArray=array();
		$subcategoryArray=array();
		
		$brand_prc_array=array();
		$brand_prd_array=array();
		$rating_prd_array = array(); 
		$rating_prc_array = array(); 
		
		$category_prc_array = array(); 
		$category_prd_array = array(); 
		
		$brand_result = array(); 
		$modelarray = array(); 
		$model_result = array(); 
		
		$brand_result_array=array();
		$model_result_array=array();
		
		$brand_result_arrayy=array();
		$model_result_arrayy=array();
		
		$brandArray=array();
		
		
		
		
		
		$categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
		
		$category = $categoryFactory->create()->load($categoryId);
		$category_name = trim($category->getName()); 
		
		
		
		/* $categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('entity_id')
			->load();    */
			
		$categoryProducts = $category->getProductCollection()
			->addAttributeToSelect('model')
			->load();
		
		$model_count=$categoryProducts->count();
		
		if($model_count!=0)
		{
			foreach($categoryProducts as $product)
			{
				
				$modelArray[]=$product->getModel(); 
			}		
		}	
		
		//print_r($modelArray);
		
		
		
		//print_r($category_prc_array);
		
		//die;
		
		if(!empty($modelArray))
		{
			//$modelArray = array_map("unserialize", array_unique(array_map("serialize", $modelArray)));
			$modelArray = array_unique($modelArray); 
			foreach($modelArray as $row)
			{
				$model_id=$row;		
				$model_name=$api_helpers->get_product_model_name_new($model_id);	
				if($model_name!='na' || $model_name=='')
				{	
					$count=0;
					 
					$category = $categoryFactory->create()->load($categoryId);
					$categoryProducts = $category->getProductCollection()
						->addAttributeToSelect('model')
						->addAttributeToFilter('model', ['eq' => $model_id])	 
						->load();   
					
					$count=$categoryProducts->count();
					
					$model_result_arrayy[]=array(
						'id'=>$model_id,   
						'name'=>$model_name, 
						'count'=>$count 
					);	
				}
				
			}  
			
		}	
		
		
		//print_r($model_result_arrayy);
		
		
		
		 
		
		
		
		
		
		$parent_model_array['model'] = $model_result_arrayy;  
		$result_array = array('success' => 1, 'result' => $parent_model_array, 'error' =>"Category filters details."); 
		echo json_encode($result_array);
		
		die; 
		
		
		
	
	}catch(Exception $e){
		
		// echo $msg = 'Error : '.$e->getMessage();
		$result_array = array('success' => 0,'result' => $parent_model_array, 'error' => $e->getMessage()); 

		echo json_encode($result_array);
		
		
	}
?>