<?php
	
	//https://dev.idsil.com/design/api/wishlist_product/?productId=1399&accesstoken=1234&customer_id=269
	


	require "../security/sanitize.php";
	$result = sanitizedJsonPayload();

	// $result = $_GET;
		
	/* {"accesstoken": "1234","productId":"1399","user_id":"269","status":"1"} */

	// print_r($result);
	// die();
     
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$argumentsCount=count($result);
	$wishlist_array=null; 

	if($argumentsCount < 4 || $argumentsCount > 4) 
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}	


	if(!isset($result['accesstoken']))
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Access Token is missing'); 	
		echo json_encode($result_array);	
		die; 
	}
	if(!isset($result['productId']))
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Product ID is missing'); 	
		echo json_encode($result_array);	 
		die; 
	}
	if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'User id is missing'); 	
		echo json_encode($result_array);	
		die; 
	}
	if(!isset($result['status'])) 
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Status is missing'); 	
		echo json_encode($result_array);	
		die; 
	}
  
  
	

  

	$accesstoken=$result['accesstoken'];
	$productId=$result['productId'];
	$customer_id=@get_numeric($result['user_id']);
	$status=$result['status'];



	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
	
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
		$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}		

	}

	if($productId=='')
	{
		$result_array=array('success' => 0,'result' => $wishlist_array, 'error' => 'Product ID is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$check=$api_helpers->check_product_exist($productId);
		if($check==0)
		{
			$result_array=array('success' => 0,'result' => $wishlist_array, 'error' => 'This product does not exists'); 	
			echo json_encode($result_array);	
			die; 
		} 
	}
	
	
	
	if($status=='')
	{
		$result_array=array('success' => 0,'result' => $wishlist_array, 'error' => 'Status is missing'); 	
		echo json_encode($result_array);	  
		die;
	}

	$user_array=null;
	if($customer_id=='')
	{
		$result_array=array('success' => 0,'result' => $wishlist_array, 'error' => 'Customer ID is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$check_user=$api_helpers->check_user_exists($customer_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	 
		} 		
	}
	
    try
    {
		
		$user_id=$customer_id;
		//$token=$api_helpers->get_token();
		//$quote_id=$api_helpers->get_quote_id($token,$user_id);
		//$quote_id=json_decode($quote_id);
		 
		$arrayss=$api_helpers->get_user_token_details($user_id);
		//print_r($arrayss);
		//die;  
		$token=$arrayss['token'];
		$quote_id=$arrayss['quote_id'];   
		
		
		$wishlist=$api_helpers->wishlist($customer_id,$productId,$status);
		
		$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
		
		
		
        if($status==1)
		{	
			$wishlistAdd = $wishList->create()->loadByCustomerId($customer_id, true);
		
				// Check if the wishlist have product or not.
			$wishlist_count =  $wishlistAdd->getItemCollection()->count();
			
			if($wishlist=="already")
			{
				$user_array=array('user_id'=>$customer_id,'wishlist_count'=>$wishlist_count);
				$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Product is already in wishlist'); 	
				echo json_encode($result_array);	
				die; 
			}
			else
			{
				 
				$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();	
				
				$arrayss=$api_helpers->get_user_token_details($user_id);
				//print_r($arrayss);
				//die; 
				$token=$arrayss['token'];
				$quote_id=$arrayss['quote_id'];  
				
				
				$select = $connection->select()
                  ->from('quote_item')
				  ->where('quote_id = ?', $quote_id)
				  ->where('product_id = ?', $productId);
				$result = $connection->fetchAll($select);
				if(!empty($result))
				{	
				$item_id=$result[0]['item_id'];
				
				$sql = "delete from quote_item where item_id='".$item_id."'"; 
				$connection->query($sql);  
				
				$sql = "delete from quote_item_option where item_id='".$item_id."'";  
				$connection->query($sql);  
				}
				/*
				$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
				$allItems=$quote->getAllVisibleItems();
				
				$product_id=$productId;
				
				foreach ($allItems as $item) {
					
					$itemId = $item->getItemId();
					$ProductId = $item->getProductId();
					
					if($ProductId==$product_id)
					{	
						$product_id;
						$quoteItem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
						$quoteItem->delete();
						
					} 
				}	
				*/
				
				
				$user_array=array('user_id'=>$customer_id,'wishlist_count'=>$wishlist_count);
				$result_array=array('success' =>1,'result' => $user_array, 'error' => 'Product added in wishlist'); 	
				echo json_encode($result_array);	  
				die;  
			}
		} 
		else
		{
			$wishlistAdd = $wishList->create()->loadByCustomerId($customer_id, true);
		
				// Check if the wishlist have product or not.
			$wishlist_count =  $wishlistAdd->getItemCollection()->count();
			
			if($wishlist=='alreadyempty')
			{
				$user_array=array('user_id'=>$customer_id,'wishlist_count'=>$wishlist_count);
				$result_array=array('success' =>0,'result' => $user_array, 'error' => 'Wishlist already empty'); 	
				echo json_encode($result_array);	  
				die;  
			}
			else if($wishlist=='notinlist')
			{
				$user_array=array('user_id'=>$customer_id,'wishlist_count'=>$wishlist_count);
				$result_array=array('success' =>0,'result' => $user_array, 'error' => 'Selected product not in wishlist'); 	
				echo json_encode($result_array);	  
				die;  
			}  
			else
			{
				$user_array=array('user_id'=>$customer_id,'wishlist_count'=>$wishlist_count);
				//$user_array=array('user_id'=>$customer_id);
				$result_array=array('success' =>1,'result' => $user_array, 'error' => 'product removed from wishlist'); 	
				echo json_encode($result_array);	   
				die;  
			}  
		}
		
		
	}
	catch(Exception $e){
		$result_array=array('success' => 0, 'result' =>$wishlist_array, 'error' => $e->getMessage()); 	
	} 
	 
	echo json_encode($result_array);
?>