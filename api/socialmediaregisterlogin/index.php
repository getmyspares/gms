<?php

/*
FOR social media Buyer json format

{
	"accesstoken": "1234",
	"email": "baljitgmail1@gmail.com",
	
	 
	
}

*/
// SignUp User as Individual and company buyer. On company buyer it send the confirmation email.
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
//print_r($result); 
//die;
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();


$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$app_helpers = $objectManager->create('Customm\Apii\Helper\Data');

$url = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface'); 

$state = $objectManager->get('\Magento\Framework\App\State');
		$state->setAreaCode('frontend');

$group_array=array('1','4','5');  
$null = null;
 $argumentsCount=count($result);

$user_array=null;

if($argumentsCount < 2 || $argumentsCount > 4)  
{ 
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}        
	
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['email']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Email is missing'); 	
	echo json_encode($result_array);	 
	die; 
}



  

$firstname='';
$lastname='';  

if(isset($result['firstname']))  
{
	$firstname=$result['firstname'];	
}


if(isset($result['lastname']))  
{
	$lastname=$result['lastname'];	
}
	
$accesstoken=$result['accesstoken'];
$email=$result['email'];

 

 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($email=='')
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Email is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
	{
		$result_array = array('success' => 0, 'result' => $user_array, 'error' => 'Email address is not valid');
		echo json_encode($result_array);	
		die;
	}
} 





 
 
$check_email=$app_helpers->check_email_exists($email); 

if($check_email > 0)
{
	
	
	$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
	$websiteId = $storeManager->getWebsite()->getWebsiteId();
	$store = $storeManager->getStore(); 
	$storeId = $store->getStoreId();
	$CustomerModel->setWebsiteId($websiteId);
	$CustomerModel->loadByEmail($email);
	$userId = $CustomerModel->getId(); 
	
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$api_helpers->store_user_token($userId);      	
	
	$firstname=$CustomerModel->getFirstname();	
	$lastname=$CustomerModel->getLastname();	
	$mobile=$CustomerModel->getMobile();	
	
	 
	$user_array=array(   
		'user_id'=>$userId,
		'email'=>$email	 
	);
	
	
	
	if($firstname=='firstname')
	{
		$firstname='';
	}
	if($lastname=='lastname')
	{
		$lastname='';
	}
	 
	   	
			
	if($firstname=='' || $lastname=='' || $mobile=='')
	{
		$result_array=array('success' => 1,'result' => $user_array,'redirect'=>0,'error' => 'Some information missing'); 	
		echo json_encode($result_array);	 
		die;    
	}
	else 
	{
		$result_array=array('success' => 1,'result' => $user_array,'redirect'=>1,'error' => 'Have all information');  	
		echo json_encode($result_array);	  
		die; 
	}   
	
	
}
else
{
		
	$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');
	$websiteId = $storeManager->getWebsite()->getWebsiteId();
	$store = $storeManager->getStore(); 
	$storeId = $store->getStoreId();

	$password='login@123';
	$customer = $customerFactory->create();

	$customer->setWebsiteId($websiteId);
	$customer->setEmail($email);
	$customer->setPassword($password);
	
	if($firstname=='')
	{
		$customer->setFirstname('firstname');
	}
	else
	{
		$customer->setFirstname($firstname);
	}
	
	
	if($lastname=='')
	{
		$customer->setLastname('lastname');
	} 
	else
	{
		$customer->setLastname($lastname); 
	}  
	$customer->save();
	$custID=$customer->getId();
	  
	
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$api_helpers->store_user_token($custID);      
	
	
	$group_id=1;  
	
	$sql = "update customer_grid_flat set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
	$connection->query($sql);
	
	$sql = "update customer_entity set group_id='".$group_id."' WHERE entity_id='".$custID."' ";
	$connection->query($sql);      
	
	  
	
	$sql = "update customer_entity set confirmation = NULL WHERE entity_id='".$custID."' ";
	$connection->query($sql);  
	
	
	$sql = "update customer_grid_flat set confirmation = NULL WHERE entity_id='".$custID."' ";
	$connection->query($sql);   
	   
	$sql = "delete from otp_temp_user  WHERE email='".$email."' ";
	$connection->query($sql);         	     
	
	$helpers->individual_welcome('Firstname','Lastname',$email) ;            
	
	$message_text='Staging App Social media Mobile - '.$email.'-'.$custID;   
	$helpers->test_email($message_text,'101','mobile user');          
	
	$CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
	$CustomerModel->setWebsiteId(1); 
	$CustomerModel->loadByEmail($email);
	$userId = $CustomerModel->getId();
	
	
	
	$firstname=$CustomerModel->getFirstname();	
	$lastname=$CustomerModel->getLastname();	
	$mobile=$CustomerModel->getMobile();	
	
	if($firstname=='firstname')
	{
		$firstname='';
	}
	if($lastname=='lastname')
	{
		$lastname='';
	}
	
	
	
	
			
	if($firstname=='' || $lastname=='' || $mobile=='')
	{
		$result_array=array('success' => 1,'result' => $user_array,'redirect'=>0,'error' => 'Some information missing'); 	
		echo json_encode($result_array);	 
		die;    
	}
	else 
	{
		$result_array=array('success' => 1,'result' => $user_array,'redirect'=>1,'error' => 'Have all information');  	
		echo json_encode($result_array);	  
		die; 
	}   
	
	
	
	/* $user_array=array('user_id'=>$custID,'email'=>$email);
	$result_array=array('success' =>1, 'result' => $user_array,'error' => 'User has been registered successfully');    	
	echo json_encode($result_array);	    
	die;	   */
		
	
	
}	

