<?php
	/*
	Edit Delivery Address for the customer
	{
		"accesstoken": "1234",
		"user_id": "12",
		"address_id": "69",
		"city": "Chandigarh",
		"firstname": "Ramit",
		"lastname": "last",
		"postcode": "160023",
		"telephone": "7894563214",
		"region": "Chandigarh",
		"region_id": "538",
		"street_1": "2121",
		"street_2": "Sector 23",
		"landmark": "near temple",
		"set_deault_shipping_adddress": "0",
		"set_deault_billing_adddress": "1",
		"address_type": "1"
	}
	*/
require "../security/sanitize.php";
$result = sanitizedJsonPayload();
include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$argumentsCount=count($result);

if($argumentsCount < 16 || $argumentsCount > 16) 
{
	$result_array=array('success' => 0, 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}	
$accesstoken=$result['accesstoken'];
$address_id=@get_numeric($result['address_id']);
$user_id=@get_numeric($result['user_id']);
$city=$result['city'];
$firstname=$result['firstname'];
$lastname=$result['lastname'];
$postcode=@get_numeric($result['postcode']);
$telephone=@get_numeric($result['telephone']);
$region=$result['region'];
$region_id=$result['region_id'];
$street_1=$result['street_1'];
$street_2=$result['street_2'];
$landmark=$result['landmark'];
$address_type=$result['address_type'];
$set_deault_shipping_adddress=$result['set_default_shipping_address'];
$set_deault_billing_adddress=$result['set_default_billing_address'];

$user_array=null; 
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
}

if($address_id=='') 
{
	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Address Id is missing'); 	
	echo json_encode($result_array);	
	die;
} 
else
{
	$select = $connection->select()
		->from('customer_address_entity') 
		->where('entity_id = ?', $address_id);
		
	$results = $connection->fetchAll($select);	
	if(empty($results))
	{
		$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Address Id is not exists'); 	
		echo json_encode($result_array);	
		die; 
	} 
}	
  


if($city=='')
{
	$result_array=array('success' => 0, 'error' => 'City is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($firstname=='')
{
	$result_array=array('success' => 0, 'error' => 'Firstname is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($lastname=='')
{
	$result_array=array('success' => 0, 'error' => 'Lastname is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($postcode=='')
{
	$result_array=array('success' => 0, 'error' => 'Pincode is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($telephone=='')
{
	$result_array=array('success' => 0, 'error' => 'Telephone is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($region=='')
{
	$result_array=array('success' => 0, 'error' => 'Region is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($region_id=='')
{
	$result_array=array('success' => 0, 'error' => 'Region Id is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($street_1=='')
{
	$result_array=array('success' => 0, 'error' => 'House no. is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($street_2=='')
{
	$result_array=array('success' => 0, 'error' => 'Road name is missing'); 	
	echo json_encode($result_array);	
	die;
}

if($address_type=='')
{
	$result_array=array('success' => 0, 'error' => 'Address type is missing'); 	
	echo json_encode($result_array);	
	die;  
}
	 

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}		
	
}

try
{
	
	
	
	$today=date('Y-m-d h:i:s');  
	$address_id=@get_numeric($result['address_id']);
	$city=$result['city'];
	$firstname=$result['firstname'];
	$lastname=$result['lastname'];
	$postcode=@get_numeric($result['postcode']);
	$telephone=@get_numeric($result['telephone']);
	$region=$result['region'];
	$region_id=$result['region_id'];
	$street_1=$result['street_1'];
	$street_2=$result['street_2'];
	$landmark=$result['landmark'];
	$address_type=$result['address_type']; 
	
	
	$address=$street_1.'\n'.$street_2.'\n'.$landmark;
	
	
	// $allowed=$api_helpers->shipping_allowed_on_pincode($postcode); 
	// if($allowed==0)
	// {
	// 	$result_array=array('success' => 0, 'result' => $user_array,'error' => 'Shipping is not allowed on selected pincode'); 	
	// 	echo json_encode($result_array);	 
	// 	die; 	
	// }	 
	    
	   
	   
	   
	$sql="update customer_address_entity set city='".$city."',firstname='".$firstname."',lastname='".$lastname."',region='".$region."',region_id='".$region_id."',street='".$address."',telephone='".$telephone."',postcode='".$postcode."' where entity_id='".$address_id."'";  
	$connection->query($sql);	       
	
	
	$sql="update customer_address_type set address_type='".$address_type."' where address_id='".$address_id."'";  
	$connection->query($sql);	 
	
	if($set_deault_shipping_adddress==1)
	{
		$address = $objectManager->create('\Magento\Customer\Model\Address')->load($address_id);
		$address->setIsDefaultShipping('1');
		$address->save();
	}
	if($set_deault_billing_adddress==1)
	{

		$address = $objectManager->create('\Magento\Customer\Model\Address')->load($address_id);
		$address->setIsDefaultBilling('1');
		$address->save();
	}		
	
	
	$noDeliveryMsg = '';
	$deliverable   = true;
	$pincode = $connection->fetchOne("SELECT postcode FROM `customer_address_entity` WHERE `entity_id` = '$address_id'");
	if($pincode){
		$rdata = $connection->fetchRow("SELECT is_active,msg FROM `pincode_details` WHERE `pincode` = '$pincode'");
		if($rdata['is_active']==0){
			$deliverable   = false;
			$noDeliveryMsg = 'Due to covid restriction, we are not shipping to the location selected by you.Please chose a different location.';
			if($rdata['msg']!=null && trim($rdata['msg'])!=''){
				$noDeliveryMsg = $rdata['msg'];				
			}
		}
	}
	
	$user_array=array('address_id'=>$address_id,'deliverable'=>$deliverable,'message'=>$noDeliveryMsg); 
	  
	$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Address has been save successfully'); 	
	echo json_encode($result_array);	 
	die;     
	
}
catch(Exception $e) 
{
	echo $e->getMessage();
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Account not confirmed or Password not matched'); 	
	echo json_encode($result_array);	
	die;
} 


  


?>

	
