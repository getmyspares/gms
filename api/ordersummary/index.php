<?php


/*

	{
		"accesstoken": "1234",
		"product": [1375,1271],
		"qty": [1,1],
		"user_id": "1234"
	}

*/

require "../security/sanitize.php";
$result = sanitizedJsonPayload();

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');
$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');

$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');


/*
$argumentsCount=count($result);

if($argumentsCount < 2 || $argumentsCount > 2)
{
	$result_array=array('success' => 0,'result' => $data , 'error' => 'Input count not matched');
	echo json_encode($result_array);
	die;
}

*/

$user_array=null;
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token parameter is missing');
	echo json_encode($result_array);
	die;
}
if(!isset($result['product']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product array parameter is missing');
	echo json_encode($result_array);
	die;
}
if(!isset(@get_numeric($result['qty'])))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Qty array parameter is missing');
	echo json_encode($result_array);
	die;
}


$accesstoken=$result['accesstoken'];
$productArray=$result['product'];
$qtyArray=@get_numeric($result['qty']);
$user_id=@get_numeric($result['user_id']);





if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing');
	echo json_encode($result_array);
	die;
}
else
{

	$checkToken=$helpers->checkToken($accesstoken);
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');
		echo json_encode($result_array);
		die;
	}
}

$count_product=count($productArray);
$count_qty=count($qtyArray);

if($count_product!=$count_qty)
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product and Qty array not matched');
	echo json_encode($result_array);
	die;
}



$discountAmount=0;
$total_gst=0;
$total_base_amount=0;
$total_price=0;

$i=0;





foreach($productArray as $product_id)
{

	$id = $product_id;

	$priduct_check=$api_helpers->check_product_exist($id);
	if($priduct_check=='')
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product of '.$id.' not found');
		echo json_encode($result_array);
		die;
	}

	$qty=$qtyArray[$i];
	$product = $productRepository->getById($id);

	$check_price=$api_helpers->get_product_price($id);

	if($check_price==0)
	{
		$price=$product->getPrice() * $qty;
	}
	else
	{
		$price=$check_price * $qty;
	}


	$shippingGstRate = $product->getGstRate();

	$gstPercent = 100 + $shippingGstRate;





	$rowTotal = $price;

	$productPrice = ($rowTotal - $discountAmount) / $gstPercent;

	$gstAmount = $productPrice * $shippingGstRate;
	$total_gst = $total_gst + $gstAmount;
	$total_price=$total_price + $rowTotal;


$i++;
}


$total_base_amount=$total_price - $total_gst;

$total_gst.' '.$total_price.' '.$total_base_amount;





$total_gst=number_format($total_gst, 2, '.', '');
$total_base_amount=number_format($total_base_amount, 2, '.', '');



$subtotal=$total_gst + $total_base_amount;

$shippingAmount=0;



if($user_id!='')
{
	$user_idd=$api_helpers->check_user_exists($user_id);

	if($user_idd!=0)
	{

		$pincode=$custom_helpers->get_customer_shipping_address($user_id,'postcode');
		if($pincode!=0)
		{
			$shippingAmount=$data_helpers->ecom_price($pincode);
		}

	}
	else
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'User Id not exists');
		echo json_encode($result_array);
		die;
	}

	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}

}


$shippingAmount=number_format($shippingAmount, 2, '.', '');
$order_total=$subtotal + $shippingAmount;

$total_gst.' '.$total_base_amount.' '.$subtotal.' '.$order_total;

$subtotal=number_format($subtotal, 2, '.', '');
$order_total=number_format($order_total, 2, '.', '');

$array=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$shippingAmount,'order_total'=>$order_total);

$user_array=$array;

$result_array=array('success' => 1, 'result' => $user_array, 'error' => 'Order Summary');
echo json_encode($result_array);
die;
