<?php
	/**
	 * http://demo16.mobdigi.com/api/clearcart/?user_id=734&accesstoken=1468307817
	 */

	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	$result=$_GET;
	if(!is_array($result) || (is_array($result) && empty($result))) {
		echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$response = null;
	$argumentsCount=count($result);
	$user_array=null;
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 
		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	}  
	if(!empty($result['buy_now']))
	{
		$buy_now=$result['buy_now'];
	}
	else 
	{
		$buy_now=0;
	}
	$token=$api_helpers->get_token(); 
	$quote_id=$api_helpers->get_quote_id($token,$user_id);
	$quote_id=json_decode($quote_id);
	$orderArray=array();
	$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
	$quoteItems=$quote->getAllVisibleItems();
	$items = $quote->getAllItems(); 
	if(!empty($items))
	{	 
		foreach($items as $item)  
		{
			
			$product_id=$item->getProductId();
			$sql="select order_increment_id from mobile_sales_order where cart_id='".$quote_id."' and product_id='".$product_id."'";  
			$results = $connection->fetchAll($sql);		   
			if(!empty($results))
			{
				$order_increment_id=$results[0]['order_increment_id'];
				$orderArray[]=$order_increment_id; 
			}	
			
		}		 
	}     
	$newProductArray=array();
	if($buy_now==1) 
	{	
		$sql="select * from mobile_user_cart where user_id='".$user_id."' and quote_id='".$quote_id."' and buy_now='1'";
		$resultss = $connection->fetchAll($sql);		
		if(!empty($resultss))
		{	
			$product_id=$resultss[0]['product_id'];
			foreach($items as $item)   
			{ 
			
				$itemId = $item->getItemId();
				$productIdd = $item->getProductId();
				if($productIdd!=$product_id)
				{	
					$newProductArray[] = array(
						'user_id'=>$user_id,
						'qty'=>$item->getQty(),
						'product_id'=>$item->getProductId()
					);
				} 
			}	

			$connection->delete('mobile_user_cart', ['user_id = ?' => $user_id]);
		}
		$check=$api_helpers->clear_cart($user_id); 	 
		if(!empty($newProductArray))
		{	
			foreach($newProductArray as $row)
			{
				
				$product_id=$row['product_id'];
				$qty=$row['qty'];
				$user_id=$row['user_id']; 
				$api_helpers->addtocart_test($user_id,$product_id,$qty,$quote_id);
			}
		}	
		if($check=='1') 
		{
			$user_array=array('user_id'=>$user_id,'order_id'=>$orderArray); 
			$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Cart cleared'); 	
			echo json_encode($result_array);	
			die;	
		}
	} 
	if($buy_now==0)
	{
		$check=$api_helpers->clear_cart($user_id); 	  
	}
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	$api_helpers->store_user_token($user_id);      
	if($check=='1')
	{
		$user_array=array('user_id'=>$user_id,'order_id'=>$orderArray); 
		$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Cart cleared'); 	
		echo json_encode($result_array);	
		die;	
	}	
	else
	{
		
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Cart empty'); 	
		echo json_encode($result_array);	
		die;			 
	}		
?>