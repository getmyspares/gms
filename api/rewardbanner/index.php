<?php
	// Excitingdeals api.
	//	Url Example :- https://dev.idsil.com/design/api/rewardbanner/?accesstoken=1234
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$result = $_GET;
	// print_r($result);
	
	$data_array = null;
	$argumentsCount=count($result);

	if($argumentsCount < 1 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $data_array , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $data_array, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $data_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	try
	{
		$data_array = array();
		$block = $objectManager->create('Magento\Cms\Model\Block');
		$layoutObj = $objectManager->get('Magento\Framework\View\Layout');
		//$block_data = $block->load('reward_banner','identifier');
		$block_data = $layoutObj->createBlock('Magento\Cms\Block\Block')
						->setBlockId('reward_banner')
						->toHtml();
						
		if($block_data){
			// get href from the data
			preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $block_data, $matches);
			$href_val = implode( ", ", $matches[2] );
			
			// get image src code
			preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $block_data, $image);
			$image_val = $image['src'];

			$data_array[0]['href'] = strtok($href_val, '?');
			$data_array[0]['image'] = $image_val;
			
			$url = strtok($href_val, '?');
			
			$url=strtok($href_val, '?');  
			$url_details = $custom_helpers->get_url_type($url);
			$url_details_array = json_decode($url_details,true);
			// print_r($url_details_array);
			if($url_details_array['success']==1){
				$data_array[0]['url_type']=  $url_details_array['entity_type'];
				$data_array[0]['url_id']= $url_details_array['entity_id']; 
			}else{
				$data_array[0]['url_type']= 'category';
				$data_array[0]['url_id']= 4; 
			
			}
		}
		
		
		// $data_array[0]['url_type']= 'category';
		// $data_array[0]['url_id']= 4; 
		//print_r($data_array);

		
		if(!empty($data_array)){
			$result_array=array('success' => 1, 'result' =>$data_array, 'error' => 'Reward Banner.');
		}else{
			$result_array=array('success' => 0, 'result' =>$data_array, 'error' => 'There is no data right now.Please check after sometime.');
		}
		
	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$data_array, 'error' => $e->getMessage()); 	
	} 
	 
	echo json_encode($result_array);
?>