<?php
 
/*TODO: api need refactoring  bad code written   @ritesh19march2021  */

//$result = sanitizedJsonPayload();
//print_r($result);
require "../security/sanitize.php";
$result=$_GET;

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$argumentsCount=count($result); 
$user_array=null; 

if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['order_id']) || empty(@get_numeric($result['order_id']))) 
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Order Id is missing'); 	
	echo json_encode($result_array);	  
	die; 
}
$accesstoken=$result['accesstoken'];
$user_id=@get_numeric($result['user_id']);
$order_id=$result['order_id'];

/* need  to  remove item wise order  detail @ritesh */
// $item_id=$result['item_id'];

$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$tax_helpers = $objectManager->create('Panasonic\CustomUser\Helper\taxationfunction');

if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token not matched');	
		echo json_encode($result_array);	
		die;
	}
}


if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	} 

	$authentication_response = $api_helpers->authenticateUser($user_id,$result['accesstoken']);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
}

	
if($order_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Order ID is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	$check=$api_helpers->check_order_exists($order_id);
	if($check==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Order Id not exists'); 	
		echo json_encode($result_array);	
		die;
	} 

	$authentication_response = $api_helpers->authenticateOrder($user_id,$order_id);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}

}	

$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($order_id);  
$orderArray=$order->getData();

$is_ecom = $helpers->isEcomOrder($order_id);


if(!empty($orderArray)) 
{
	$address_id=$order->getShippingAddressId();
	$billing_address_id=$order->getBillingAddressId();

	$address=$api_helpers->get_address_details($order_id);  
	$billing_address=$api_helpers->get_billing_address_details($order_id);  
	//$order_summary=$api_helpers->get_order_summary($order_id,$user_id);
	$created_at=$order->getCreatedAt();
	$order_inc_id=$order->getIncrementId();
	$delivery_date=$order->getData('delivery_date');

	$allow_return=0;	
	$return_initiate=0;
	$order_status=$api_helpers->api_get_order_tracking($order_inc_id);

	$isreturnavailable = $objectManager->create('OM\Rma\Helper\Data')->isReturnAvailable($order_inc_id);  
	$allow_return = ($isreturnavailable==true)?'1':'0';
	$delivered_at="";
	if(!empty($delivery_date))
	{
		$delivered_at=date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime($delivery_date)));
	}
	

	$created_at = date("d M, Y", strtotime($created_at)); 
	$payment_method=$order->getPayment()->getMethod();

	if($payment_method=="cashondelivery")
	{
		$payment_method="Cash On Delivery";
	}	
	else
	{
		$payment_method="Pay Online";
	}	
	foreach ($order->getAllItems() as $item)
	{
		$item_idd=$item->getId();
		$product_id=$item->getProductId();
		/* if($item_id==$item_idd)
		{
		} */
			$productIdArray[]=array(
				'item_id'=>$item->getId(),
				'product_id'=>$item->getProductId(),
				'product_price'=>$item->getPrice(),
				'product_qty'=>round($item->getQtyOrdered(),0)
			);
	}
	  
	$shippingAmount=0; 
	$total_shippingAmount=0;

	if(!empty($productIdArray))	
		{
			foreach($productIdArray as $row)
			{
				$item_id=$row['item_id'];
				$product_id=$row['product_id'];
				$product_qty=$row['product_qty'];
				$product_price=$row['product_price'];
				$product_price=number_format($product_price, 2, '.', '');
				
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);		
					$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
					
					if(!empty($product->getImage()))
					{
						$image = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl();
					} 
					else
					{
						$site_url = $storeManager->getStore()->getBaseUrl();
						$imageUrl = $site_url.'Customimages/product-thumb.jpg';
						$image = $imageUrl;
					} 
					
					$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product->getId());
					if(!empty($RatingOb->getCount())){
						$ratings = $RatingOb->getSum()/$RatingOb->getCount();
					}
					
					$productdata['rating_counts'] = $RatingOb->getCount();
					
					if(!empty($ratings)){
						$product_rating = $ratings;
					}else{
						$product_rating = null;
					}
					
					$return_initiate=0;
					$return_status=0; 
					
					
						$check_return='';

						if($isreturnavailable)
						{
							$check_return=$api_helpers->check_product_in_return($order_id,$item_id);	
							if($check_return==1)  
							{ 
								$allow_return=1; 	 
								$return_initiate=0;
								$return_status=$api_helpers->get_product_return_status($order_id,$item_id);
							}	  else 
							{
								$allow_return=0; 	 
								$return_initiate=1;
								$return_status=$api_helpers->get_product_return_status($order_id,$item_id);
							} 
						}
					
					 	
					$productArray[]=array(
						'order_id'=>$order_id, 	   
						'id'=>$product->getId(), 	 
						'item_id'=>$item_id, 	  
						'name'=>$product->getName(),	
						'sku'=>$product->getSku(),
						'image'=>$image,	
						'qty'=>$product_qty,
						'price'=>$product_price,
						'rating'=>$product_rating,
						'created_at'=>$created_at,
						'delivered_at'=>$delivered_at,
						'allow_return'=>$allow_return,
						'return_initiate'=>$return_initiate,
						'return_status'=>$return_status 
					);
			}	
			
			$status=strtolower($api_helpers->api_get_order_tracking($order_inc_id));  
			$order_status=1; 

			if($status=='new')
			{
				$order_status=1;
			}
			else if($status=='shipped')
			{
				$order_status=2;
			}
			else if($status=='delivered')
			{
				$order_status=3;  
			} 	
			
			
			$ordersummaryArray=$tax_helpers->order_item_details_product($order_inc_id,$product_id);
			// die;
			
			$total_base_amount=$ordersummaryArray[0]['base_amount'];
			$total_gst=$ordersummaryArray[0]['total_gst'];
			
			/* no need  to  dynamically calculate  fetching from order table @ritesh
				$subtotal=$ordersummaryArray[0]['row_total'];
				$total_shippingAmount=$ordersummaryArray[0]['product_shipping'];
				$order_total=$subtotal+$total_shippingAmount;
			*/
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$sql = "Select rma_id FROM wk_rma where order_id='".$order_id."'";
			$result_rma = $connection->fetchOne($sql);
			$return_initiated = $result_rma?true:false;
			$subtotal=$orderArray['base_subtotal'];
			$total_shippingAmount=$orderArray['base_shipping_amount'];
			$order_total=$orderArray['base_grand_total'];
			// $order_db_status=$orderArray['base_grand_total'];
			$order_db_status=  str_replace("_"," " ,ucwords($orderArray['status'])) ;

			$order_summary=array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total,'order_status_db'=>$order_db_status);
			
			$user_array=array('order_id'=>$order_id,"return_initiated"=>$return_initiated,'order_inc_id'=>$order_inc_id,'summary'=>$order_summary,'address'=>$address,'billingaddress'=>$billing_address,'productarray'=>$productArray,'order_status'=>$order_status,'created_at'=>$created_at,'payment_method'=>$payment_method,'ecom_order'=>$is_ecom); 
			$result_array=array('success' => 1,'result' => $user_array, 'error' => 'Order Id exists'); 	
			echo json_encode($result_array);	 
			die; 	 		
	}	
}
else
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'Order not found'); 	
	echo json_encode($result_array);	 
	die; 
}	

?>



