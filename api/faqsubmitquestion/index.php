<?php
	
	// Check delivery_avalability
	//https://dev.idsil.com/design/api/faq_listing/?product_id=1399&accesstoken=1234
	require "../security/sanitize.php";
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	// $result = $_GET;
	$result = sanitizedJsonPayload();
	/*{"accesstoken": "1234","buyer_id": "269","product_id":"1399","content":"text of content"}*/

	// print_r($result);
	// die();
	
	
	$response = null;
	$argumentsCount=count($result);

	if($argumentsCount < 5 || $argumentsCount > 5)
	{
		$result_array=array('success' => 0,'result' => null , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => null, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => null, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	
	if(!empty($result['buyer_id'])){
		$buyer_id = $result['buyer_id'];
	}else{
		$buyer_id = '';	
	}
	
	if($buyer_id ==''){
		$result_array=array('success' => 0, 'result' => null, 'error' => 'Buyer id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$buyer_id = $result['buyer_id'];
	}
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => null, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$product_id = @get_numeric($result['product_id']);
	}
	
	
	if(!isset($result['subject']) || empty($result['subject']) ){
		$result_array=array('success' => 0, 'result' => null, 'error' => 'Subject is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$subject = $result['subject'];
	}
	
	
	
	if(!empty($result['nickname'])){
		$nickname = $result['nickname'];
	}else{
		
		$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($buyer_id);
		$nickname = $customerObj->getFirstName()." ".$customerObj->getLastName();
	}
	
	/*if($nickname ==''){
		$result_array=array('success' => 0, 'result' => null, 'error' => 'Nickname is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$nickname = $result['nickname'];
	}*/
	
	
	if(!empty($result['content'])){
		$content = $result['content'];
	}else{
		$content = '';	
	}
	
	if($content ==''){
		$result_array=array('success' => 0, 'result' => null, 'error' => 'Content data is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$content = $result['content'];
	}
	


	try
	{
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('wk_qaquestion'); //gives table name with prefix

		//Insert Data into table

		$now = date('Y-m-d H:i:s');
		$sql = "Insert Into " . $tableName . " (buyer_id, product_id, subject, content, qa_nickname, status, created_at) Values ('$buyer_id','$product_id','$subject','$content','$nickname',0,'$now')";
		
		// die('here');
		$connection->query($sql);		
		

		$result_array = array('success' => 1,'error' =>"Question have been asked from admin."); 
		echo json_encode($result_array);
		die();
		

	}catch(Exception $e){		

		$result_array = array('success' => 0,'result' => null, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
?>