<?php
	
	//
	//
	//

	
	// Check delivery_avalability
	//https://www.dev.idsil.com/design/api/add_to_cart_db/
	//Input 
	///*{"accesstoken": "1234","user_id":"269"}*/
	require "../security/sanitize.php";
	include('../../app/bootstrap.php'); 
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	
	$result = sanitizedJsonPayload();
	// print_r($result);
	// die();  
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	
	$response = null;
	$argumentsCount=count($result);
	$user_array=null;
	if($argumentsCount < 2 || $argumentsCount > 2)
	{
		$result_array=array('success' => 0,'result' => $response , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}
	
	
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken'];
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $response, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $response, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	// Check the user_id 
	
	if(!empty(@get_numeric($result['user_id']))){
		$user_id = @get_numeric($result['user_id']);
	}else{
		$user_id = '';	
	}
	
	
	
	
	if($user_id=='')
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		
		$check_user=$api_helpers->check_user_exists($user_id);
		if($check_user==0)
		{
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		}
		
		$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}
	}
	
	try{
		
		$customer_id=$user_id; 
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();


		/* $select = $connection->select()
				->from('mst_rewards_transaction') 
				->where('customer_id = ?', $user_id);
		$result = $connection->fetchAll($select);
		*/
		
			
		
		
		$total_reward=0;
		/* if(!empty($result))
		{
			foreach($result as $row)
			{
				$total_reward=$total_reward + $row['amount'];  
			}	 
		}	 */ 
		
		$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
		$wishlistAdd = $wishList->create()->loadByCustomerId($customer_id, true);
		
		// Check if the wishlist have product or not.
		$wishlist_count =  $wishlistAdd->getItemCollection()->count();
		
		$cart_total=0;
		
		$quote= $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
		$quoteItems=$quote->getAllVisibleItems();
		$items = $quote->getAllItems();
		if(!empty($items))
		{	
			foreach($items as $item)  
			{
				$product_id=$item->getProductId();
				$qty=$item->getQty(); 
				$cart_total=$cart_total+$qty;
			}		
		}
		
		$user_details=$api_helpers->get_user_details($user_id);
		
		//print_r($user_details);
		
		$group_id=$user_details[0]['group_id'];
		
		if($group_id==1 || $group_id==5)
		{	
			$userArray=array(
					'user_id'=>$user_id,	
					'firstname'=>base64_encode($user_details[0]['firstname']),	
					'lastname'=>base64_encode($user_details[0]['lastname']),	 
					'email'=>base64_encode($user_details[0]['email']),	
					'mobile'=>base64_encode($user_details[0]['mobile']),	
					'group_id'=>$user_details[0]['group_id'],	
					'total_reward'=>$total_reward,	
					'wishlist_count'=>$wishlist_count,
					'cart_total'=>$cart_total
			);	
		}
		else
		{
			$userArray=array(
					'user_id'=>$user_id,
					'firstname'=>$user_details[0]['firstname'],	
					'lastname'=>$user_details[0]['lastname'],	 
					'email'=>base64_encode($user_details[0]['email']),	
					'mobile'=>base64_encode($user_details[0]['mobile']),	
					'group_id'=>$user_details[0]['group_id'],	
					'gst'=>$user_details[1]['gst'],	
					'company_name'=>$user_details[1]['company_name'],	
					'company_address'=>base64_encode($user_details[1]['company_address']),	
					'pan'=>$user_details[1]['pan'],	
					'total_reward'=>$total_reward,	
					'wishlist_count'=>$wishlist_count,
					'cart_total'=>$cart_total
			);	
		}
		
		
		
		/*
		
		foreach($user_details as $row)
		{
			if(isset($row['firstname']))
			{
				$firstname=$row['firstname'];	
			}	
			if(isset($row['lastname']))
			{
				$lastname=$row['lastname'];	 
			}	
			$userArray=array(
				'firstname'=>$firstname,	
				'lastname'=>$lastname,	 
				'email'=>$row['email'],	
				'mobile'=>$row['mobile'],	
				'group_id'=>$row['group_id'],	
				'total_reward'=>$total_reward,	
				'wishlist_count'=>$wishlist_count
			);	 
		}
		*/
		
		$user_array=array('userDetail'=>$userArray);    
		$result_array = array('success' => 1,'result' => $userArray, 'error' => 'Result Found'); 
		echo json_encode($result_array); 
		
	
	}catch(Exception $e){		

		$result_array = array('success' => 0,'result' => null, 'error' => $e->getMessage()); 
		echo json_encode($result_array);
			
	}
	
?>