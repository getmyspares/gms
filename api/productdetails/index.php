<?php
	
	$result_array=array('success' => 0, 'result' => $data, 'error' => 'depriciated api');	
	echo json_encode($result_array);	
	die;

	require "../security/sanitize.php";
	$result = sanitizedJsonPayload();

	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;

	//https://www.dev.idsil.com/design/api/productdetails/?product_id=1399&accesstoken=1234
	
	$result = $_GET;
	//print_r($result);
	
	$productdata = null;
	$data = null;
	$argumentsCount=count($result);

	/* if($argumentsCount < 2 || $argumentsCount > 3)
	{
		$result_array=array('success' => 0,'result' => $data , 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	} */
	
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');	
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data');
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	
	if(!empty($result['accesstoken'])){
		$accesstoken = $result['accesstoken']; 
	}else{
		$accesstoken = '';	
	}
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
		
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $data, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}
	
	
	if(!empty(@get_numeric($result['product_id']))){
		$product_id = @get_numeric($result['product_id']);
	}else{
		$product_id = '';	
	}
	
	if($product_id ==''){
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Product id is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
		$product_id = @get_numeric($result['product_id']);
	}
	
	
	if(!empty(@get_numeric($result['user_id']))) 
	{
		$check_user=$api_helpers->check_user_exists(@get_numeric($result['user_id']));
		if($check_user==0)
		{
			$user_array=null;
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	
		} 	
	}  
	
	 
	try 
	{
		$reviewFactory = $objectManager->create('Magento\Review\Model\Review');
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
		
		$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
		$currentStoreId = $storeManager->getStore()->getId();
		$rating = $objectManager->get("Magento\Review\Model\ResourceModel\Review\CollectionFactory");
		
		$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');

		$collection = $rating->create()->addStoreFilter(
					$currentStoreId
				)->addStatusFilter(
					\Magento\Review\Model\Review::STATUS_APPROVED
				)->addEntityFilter(
					'product',
					$product->getId()
				)->setDateOrder();
		if(!empty($collection->getData())){
		$review_data = $collection->getData(); //Get all review data of product
		
		$d=0;
		$star_5=0;$star_4=0;$star_3=0;$star_2=0;$star_1=0;
		$total_rating = 0 ;
		foreach($review_data as $data){
			
			$reviewdata['review_id']= $data['review_id'];
			$reviewdata['created_at']= $data['created_at'];
			$reviewdata['entity_id']= $data['entity_id'];
			$reviewdata['entity_pk_value']= $data['entity_pk_value'];
			$reviewdata['status_id'] = $data['status_id'];
			$reviewdata['detail_id'] = $data['detail_id'];
			$reviewdata['title'] = $data['title'];
			$reviewdata['detail'] = $data['detail'];
			$reviewdata['nickname'] = $data['nickname'];
			$reviewdata['entity_code'] = $data['entity_code'];
			
			$ratingCollection = $objectManager->get('Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection')
                       ->addFieldToFilter('review_id',$data['review_id']);  
			$rating_value = $ratingCollection->getData();
			//print_r($rating_value);
			$ratings_percentage = null;
			$ratings_percentage = $rating_value[0]['percent'];
			$total_rating +=$ratings_percentage;
			$rating_count = $ratings_percentage/20;
			
			
			if (($ratings_percentage >= 0) && ($ratings_percentage <= 20)){
					// echo 'star_1';
					$star_1 += 1;
				}else if(($ratings_percentage >= 21) && ($ratings_percentage <= 40)){
					// echo 'star_2';
					$star_2 += 1;
				}else if(($ratings_percentage >= 41) && ($ratings_percentage <= 60)){
					// echo 'star_3';
					$star_3 += 1;
				}else if(($ratings_percentage >= 61) && ($ratings_percentage <= 80)){
					// echo 'star_4';
					$star_4 += 1;
				}else if(($ratings_percentage >= 81) && ($ratings_percentage <= 100)){
					// echo 'star_5';
					$star_5 += 1;
				}
			
			 
			// if($rating_count==5){
				// $star_5 =  $star_5+1;
			// }
			
			$reviewdata['rating'] = $rating_count;
			  
			// "review_id": "86",
                    // "created_at": "2019-05-21 12:59:29",
                    // "entity_id": "1",
                    // "entity_pk_value": "1399",
                    // "status_id": "1",
                    // "detail_id": "86",
                    // "title": "tesyt",
                    // "detail": "good product",
                    // "nickname": "baljit",
                    // "customer_id": null,
                    // "entity_code": "product"
					
					$reviewsdata[] = $reviewdata;
					$d++;
		}
		}else{
			$review_data = null;
			$reviewsdata = null;		 
		}
		if(empty($star_5)){
			$star_5=0;
		}
		if(empty($star_4)){
			$star_4=0;
		}
		if(empty($star_3)){
			$star_3=0;
		}
		if(empty($star_2)){
			$star_2=0;
		}
		if(empty($star_1)){
			$star_1=0;
		}
		$star_total =$star_5+$star_4+$star_3+$star_2+$star_1;
		if($star_total>0){
			$star_5 = ($star_5/$star_total)*100;
			$star_4 = ($star_4/$star_total)*100;
			$star_3 = ($star_3/$star_total)*100;
			$star_2 = ($star_2/$star_total)*100;
			$star_1 = ($star_1/$star_total)*100;
		}

		

		$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
		$StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
		$product = $productRepository->getById($product_id); 
	
		$productdata['id']= $product->getId();
		$productdata['name']= strip_tags($product->getName());
		$productdata['sku']= strip_tags($product->getSku());
		
		
		
		$product_iddd=$product->getId();
		$is_special=0; 
		$price=$api_helpers->get_product_prices($product_iddd);		
		$orgprice = $api_helpers->get_product_prices($product_iddd);
		$specialprice = $api_helpers->get_product_special_prices($product_iddd);
		$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
		$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
		$today = time(); 
		
		if (!$specialprice)
		{	
			$specialprice = $orgprice;
		}
		else
		{
			if(!is_null($specialfromdate) && !is_null($specialtodate))
			{
				$specialfromdate=strtotime($specialfromdate);
				$specialtodate=strtotime($specialtodate);
				
				if($today >= $specialfromdate &&  $today <= $specialtodate)
				{
					$is_special=1;			
				}	
			}
		}
		
		
		if($is_special==1)
		{
			
			$productdata['price']=number_format($product->getPrice(), 2, '.', '');
			$productdata['specialprice']=number_format($product->getSpecialPrice(), 2, '.', '');
			
			$productdata['specialfromdate'] = $product->getSpecialFromDate();
			$productdata['specialtodate'] = $product->getSpecialToDate();
			$productdata['product_url'] = $product->getProductUrl();
		} 	
		else
		{
			$productdata['price']= strip_tags($product->getPrice());
			$productdata['specialprice'] = null;
			$productdata['specialfromdate'] = null;
			$productdata['specialtodate'] = null;
			$productdata['product_url'] = null;
		}

		
		
		
		$price = $product->getPrice();
		$productdata['price'] = number_format($price, 2);
		$specialprice = $product->getSpecialPrice();
		$specialPriceFromDate = $product->getSpecialFromDate();
		$specialPriceToDate = $product->getSpecialToDate(); 
		$today = time();
		
		if($price){
			$sale = round((($price-$specialprice)/$price)*100);
		}
		if ($specialprice) {
			if(!empty($specialPriceFromDate)){
				if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
					
					
					$productdata['specialprice'] = number_format($specialprice, 2);
					$productdata['specialfromdate'] = $specialPriceFromDate;
					$productdata['specialtodate'] = $specialPriceToDate;
					$productdata['sale_percentage'] = $sale.'%'.'Off';
					
					
				}
			}
		}
		
		/*

		// Get product images
		$images_array = null;
		$images = $product->getMediaGalleryImages();
		$key = 0;
		foreach ($images as $image) {
		   $images_array[$key] = $image->getData();
		   $key++;
		}
		// print_r($images_array);
		// die();
		$productdata['images']= $images_array;
		
		*/
		
		$images_array = null;
		$images = $product->getMediaGalleryImages();
		$key = 0;
		$base_64='';
		if(!empty($images)){
			foreach($images as $image){
			   
				$images_array[$key] = $image->getData();
				$imagess["value_id"] = $image->getData('value_id');
				$imageUrl = $_imageHelper->init($product, 'product_page_image_large')->setImageFile($image->getFile())->resize(350,350)->getUrl();

				$imagess["url"] = $imageUrl;  
				$imagess["file"] = $image->getFile();
				
				$url = $imageUrl;
				if (file_exists($imageUrl)) {
					$image = file_get_contents($url);
					if ($image !== false){
					   $base_64='data:image/jpg;base64,'.base64_encode($image);
					}
				}
				$imagess["base64"] = $base_64;
				 
				
				
				$images_array[$key] = $imagess;
				$key++;
			}
		}
		if(!empty($images_array)){
			$productdata['images']= $images_array;
		}else{
			$site_url = $storeManager->getStore()->getBaseUrl();
			$imageUrl = $site_url.'Customimages/product-thumb.jpg';
			$images_array[0] = array("value_id"=>1,"url"=>$imageUrl);
			$productdata['images']= $images_array;
		}
		 
		// print_r($productdata['images']);
		// die();
		
		
		//$productdata['short_description']= strip_tags($product->getShortDescription());
		$productdata['short_description']= strip_tags($product->getShortDescription());
		$productdata['tax_class_id']= $product->getTaxClassId();
		$productdata['stock_Qty']= $StockState->getStockQty($product_id);
		

		
		
		//$productdata['description']= strip_tags($product->getDescription());
		$productdata['description']= strip_tags($product->getDescription());
		$productdata['manufacturer']= $product->getAttributeText('manufacturer');
		$productdata['manufacturing_brand']= $product->getAttributeText('brands');
		$productdata['genuinity']= $product->getGenuinity();//genuinity
		$productdata['universal']= $product->getUniversal();//universal
		$productdata['warranty_details']= $product->getWarrantyDetails();//warranty_details
		$productdata['warranty_link']= $product->getWarrantyLink();//warranty_link
		$productdata['return_period']= $product->getAttributeText('return_period');//return_period
		$productdata['standard_return_days']= '';//return_period		
		$productdata['return_policy']= $product->getReturnPolicy(); //return_policy
		$productdata['part_manual']= $product->getPartManual(); //part_manual
		$productdata['disclaimer']= $product->getDisclaimer(); //disclaimer
		$productdata['product_storage']= $product->getAttributeText('product_storage'); //product_storage
		
		$productdata['part_number']= $product->getItemModelNumber(); //item_model_number
		$productdata['model_number']= $product->getPiModel(); //pi_model
		
		/*
		$productdata['length']= $product->getEcomLength(); //ecom_length
		$productdata['breadth']= $product->getEcomBreadth(); //ecom_breadth
		$productdata['height']= $product->getEcomHeight(); //ecom_height
		$productdata['weight']= $product->getWeight(); //weight
		*/
		
		$productdata['length']= number_format($product->getEcomLength(), 2, '.', ''); //ecom_length
		$productdata['breadth']= number_format($product->getEcomBreadth(), 2, '.', ''); //ecom_breadth
		$productdata['height']= number_format($product->getEcomHeight(), 2, '.', ''); //ecom_height
		$productdata['weight']= number_format($product->getWeight(), 2, '.', ''); //weight 
		
		
		
		$productdata['color']= $product->getAttributeText('color'); //color
		$productdata['part_category']= $product->getPartCategory(); //part_category
		
		$productdata['part_body_material']= $product->getAttributeText('part_body_material'); //part_body_material
		$productdata['part_type']= $product->getPartType(); //part_type
		$productdata['expertise_required']= $product->getExpertiseRequired(); //part_body_material
		$productdata['boxed']= $product->getAttributeText('boxed'); //boxed
		
		//$stars[] = array("star_5"=>"35", "star_4"=>"37", "star_3"=>"43", "star_2"=>"43", "star_1"=>"43");
		$stars[] = array("star_5"=>$star_5, "star_4"=>$star_4, "star_3"=>$star_3, "star_2"=>$star_2, "star_1"=>$star_1);
		$productdata['review_total_data']= $stars; //review data
		// $productdata['review_data']= $review_data; //review data
		if(!empty($reviewsdata)){
		$productdata['review_data']= $reviewsdata; //review data
		}else{
			$productdata['review_data']=null;
		}
		
		
		
		$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($product->getId());
		// echo $RatingOb->getSum();
		// echo '<br>';
		// echo $total_rating;
		// die();
		if(!empty($RatingOb->getCount())){
			$ratings = $total_rating/$RatingOb->getCount();
		}
		
		$productdata['rating_counts'] = $RatingOb->getCount();
		
		if(!empty($ratings)){
			$productdata['rating'] = number_format($ratings, 2, '.', '');
		}else{	
			$productdata['rating'] = null;	
		}
		
		//Seller details of the product
		$webkul_helper = $objectManager->create('Webkul\Marketplace\Helper\Data');
		$sellerId = null;
		$marketplaceProduct = $webkul_helper->getSellerProductDataByProductId($product->getId());
		foreach ($marketplaceProduct as $value) {
			 $sellerId = $value['seller_id'];
		}
		if(!empty($sellerId)){
			
			$captchenable = $webkul_helper->getCaptchaEnable();
			$sellerInfo = $webkul_helper->getSellerInfo($sellerId);
			$sellerData = $webkul_helper->getSellerDataBySellerId($sellerId);
			
			$shopTitle = $sellerInfo['shop_title'];
			$shopUrl = $sellerInfo['shop_url'];
			$logo = $sellerInfo['logo_pic'];
			$productCount = $sellerInfo['product_count'];
			$companyLocality = trim($sellerInfo['company_locality']);
			//$companyLocality = $block->escapeHtml($companyLocality);
			if (!$shopTitle) {
				$shopTitle = $shopUrl;
			}
			// print_r($sellerInfo);
			//print_r($sellerData->getData());
			$seller_array = $sellerData->getData();
			$seller_description = $seller_array[0]['company_description'];
			
			
			$productdata['seller_Id'] = $sellerId;
			$orderHelper = $objectManager->create('Webkul\Marketplace\Helper\Orders');
			$order_counts = $orderHelper->getSellerOrders($sellerId);
			$productdata['seller_shopTitle'] = $shopTitle;
			$productdata['seller_shopUrl'] = $shopUrl;
			$productdata['seller_description'] = strip_tags($seller_description);
			if(!empty($logo)){
				$productdata['seller_logo']= $webkul_helper->getMediaUrl().'avatar/'.$logo;
			}else{
				$productdata['seller_logo']= $webkul_helper->getMediaUrl().'avatar/noimage.png';
			}
			$productdata['seller_collectionPageUrl'] = $webkul_helper->getRewriteUrl('marketplace/seller/collection/shop/'.$shopUrl);
			$productdata['seller_profilePageUrl'] = $webkul_helper->getRewriteUrl('marketplace/seller/profile/shop/'.$shopUrl);
			$productdata['seller_productCount'] = $productCount;
			$productdata['seller_companyLocality'] = $companyLocality;
			$productdata['seller_order_count'] = $order_counts;
			
			
			
			$productdata['seller_feeds'] = $webkul_helper->getFeedTotal($sellerId);
			$productdata['seller_rating'] = $webkul_helper->getSelleRating($sellerId);

		}
		
		
		if(!empty(@get_numeric($result['user_id'])))  
		{   
			$user_id=@get_numeric($result['user_id']);
			$whishlist=$api_helpers->check_product_in_whishlist($user_id,$product->getId());  
			$productdata['wishlist']=$whishlist;
		}
		else 
		{
			$productdata['wishlist']=null;
		}  
		
		
		$productdata['sell_with_us']='https://getmyspares.com/customuser/index/sellerregister/';
		
		$data = $productdata;
		// print_r($productdata);
		// die();
		if(!empty($data)){
			$result_array=array('success' => 1, 'result' =>$data, 'error'=>'Product id is valid.');
			echo json_encode($result_array);
			die();
		}else{
			
			$result_array=array('success' => 0, 'result' =>$data, 'error'=>'These is no data.');
			echo json_encode($result_array);
			die();
		}

	}
	catch(Exception $e) 
	{
		$result_array=array('success' => 0, 'result' =>$data, 'error' => $e->getMessage());
echo json_encode($result_array);
		die();		
	} 
	 
	
?>
