<?php
require "../security/sanitize.php";
 
/*
{"accesstoken": "1375616867","user_id": "6840","address_id":"663"}

*/  

$result = sanitizedJsonPayload();
if(!is_array($result) || (is_array($result) && empty($result))) {
	echo json_encode(array('success' => 0, 'error' => 'Invalid request data.'));	
	die;
}

include('../../app/bootstrap.php');
use Magento\Framework\App\Bootstrap;





$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');   
$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 

$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
$data_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	 
$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn'); 


$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$argumentsCount=count($result);

/*if($argumentsCount < 3 || $argumentsCount > 3)
{
	$result_array=array('success' => 0,'result' => $data , 'error' => 'Input count not matched'); 	
	echo json_encode($result_array);	
	die;
}*/

$user_array=null; 
if(!isset($result['accesstoken']))
{
	$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Access Token parameter is missing'); 	
	echo json_encode($result_array);	
	die; 
}
if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
{
	$result_array=array('success' => 1, 'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	 
	die; 
}


$accesstoken     =$result['accesstoken'];
$user_id         = @get_numeric($result['user_id']);
$addr_id         = @get_numeric($result['address_id']);
$billing_address = @get_numeric($result['billing_address']);

if(!empty($result['buy_now']))
{
	$buy_now=$result['buy_now'];
}
else 
{
	$buy_now=0;
}



if($accesstoken=='')
{
	$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$checkToken=$helpers->checkToken($accesstoken); 
	if($checkToken==0)
	{
		$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Session expired');	
		echo json_encode($result_array);	
		die;
	}
} 

if($user_id=='')
{
	$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id is missing'); 	
	echo json_encode($result_array);	
	die;
}
else
{
	
	$check_user=$api_helpers->check_user_exists($user_id);
	if($check_user==0)
	{
		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
		echo json_encode($result_array);	
		die;	
	}
	
	$authentication_response = $api_helpers->authenticateUser($user_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
	
} 


	
  
$buy_now;




//$cartArray=$api_helpers->get_user_cart_test($user_id); 



//$api_helpers->get_user_cart_test($user_id); 



$address=$api_helpers->check_pincode_address_exists($user_id);

$cart_total=0;
		

 
/* 
if($address==0) 
{
	$summary_array=array();
	$result_array=array('success' => 1, 'result' => $user_array,'summary'=> $summary_array,'cart_total'=>$cart_total,'error' => 'No Address Selected'); 	
	echo json_encode($result_array); 	
	die; 	  
}
*/ 
	
//$token=$api_helpers->get_token();
//$quote_id=json_decode($quote_id);

//$arrayss=$api_helpers->get_user_token_details($user_id);
//print_r($arrayss);
//die; 
//$token=$arrayss['token'];
//$quote_id=$api_helpers->get_quote_id_Om($token,$user_id);


// $quote_id=$arrayss['quote_id'];   
 

$quote_om  = $objectManager->create('Magento\Quote\Model\Quote')->loadByCustomer($user_id); 
$pincode_correct=true;
if($addr_id){
	

	/* setting address  id send by mahendra as customer default shipping and billing address  */
	$api_helpers->set_default_shipping_address($addr_id,$user_id);
	
	$shippingddress   = $objectManager->create('Magento\Customer\Model\AddressFactory')->create()->load($addr_id);
	$ship_address   = $shippingddress->getData();
	
	$quote_om->getShippingAddress()->setFirstname($ship_address['firstname']);
	$quote_om->getShippingAddress()->setLastname($ship_address['lastname']);
	$quote_om->getShippingAddress()->setStreet($ship_address['street']);
	$quote_om->getShippingAddress()->setCity($ship_address['city']);
	$quote_om->getShippingAddress()->setTelephone($ship_address['telephone']);
	$quote_om->getShippingAddress()->setPostcode($ship_address['postcode']);
	$quote_om->getShippingAddress()->setCountryId($ship_address['country_id']);
	
	
	$bill_add_id = ($billing_address > 0 ) ? $billing_address : $addr_id;
	$api_helpers->set_default_billing_address($bill_add_id,$user_id);
	
	$billingAddress   = $objectManager->create('Magento\Customer\Model\AddressFactory')->create()->load($bill_add_id);
	$billaddress   = $billingAddress->getData();

	$quote_om->getBillingAddress()->setFirstname($billaddress['firstname']);
	$quote_om->getBillingAddress()->setLastname($billaddress['lastname']);
	$quote_om->getBillingAddress()->setStreet($billaddress['street']);
	$quote_om->getBillingAddress()->setCity($billaddress['city']);
	$quote_om->getBillingAddress()->setTelephone($billaddress['telephone']);
	$quote_om->getBillingAddress()->setPostcode($billaddress['postcode']);
	$quote_om->getBillingAddress()->setCountryId($billaddress['country_id']);


	$quote_om->collectTotals()->save();

	/* TODO : Remove this  temporary fix  and manage it inside EcomExpressShipping  module  @ritesh*/
	if(isset($address['postcode']))
	{
		$postcode = $address['postcode'];
		$allowed=$api_helpers->shipping_allowed_on_pincode($postcode); 
		if($allowed==0) 
		{ 
			$pincode_correct = false;	
		} 
	}
}



$quote_id = $quote_om->getId();
//$allItems=$quote->getAllVisibleItems();

$mainArrayy=array();
$buy_product=0;

/* commmented to  synch mobile and web order  @ritesh*/
// if($buy_now==1)
// {
// 	$select = $connection->select()
// 			  ->from('mobile_user_cart') 
// 			  ->where('buy_now = ?', '1')
// 			  ->where('quote_id = ?', $quote_id);
	
// 	// $select = $connection->select()
// 	// ->from('mobile_user_buy_now') 
// 	// ->where('user_id = ?', $user_id);		  
			
// 	$result_cart = $connection->fetchAll($select);
	
// 	if(!empty($result_cart))
// 	{
// 		$buy_product=$result_cart[0]['product_id'];	
// 	}	
// 	else
// 	{
// 		$result_array=array('success' => 0,'result' => $user_array, 'error' => 'There is no buy now product in cart');  	
// 		echo json_encode($result_array);	
// 		die;	
// 	}		
// }	


//echo "aaaa";
//die;

$select = $connection->select()
	  ->from('quote_item') 
	  ->where('quote_id = ?', $quote_id);
$result_curr = $connection->fetchAll($select);
$cart_count=count($result_curr);

$cart_total=0;
foreach ($result_curr as $item) {
	//item id of particular item
	$itemId = $item['item_id'];
	$productId = $item['product_id'];
	$qty=round($item['qty']); 
	
	
	$cart_total=$cart_total+$qty;
	
	$mainArrayy[]=array(
		'item_id'=>$itemId,
		'product_id'=>$productId,
		'qty'=>$qty
	);
}

//print_r($mainArrayy);
//die;
  

 
$shippingAmount=0; 
$total_shippingAmount=0;
$ship_allowed = false;
$special_price=0;
$sale=0;
$discountAmount=0;
$total_gst=0;
$total_price=0;
if(!empty($mainArrayy))
{
	foreach($mainArrayy as $row) 
	{
		$id = $row['product_id'];
		$qty= $row['qty'];
		$item_id= $row['item_id']; 
		
		$priduct_check=$api_helpers->check_product_exist($id);
		if($priduct_check=='')
		{
			$result_array=array('success' => 0, 'result' => $user_array, 'error' => 'Product of '.$id.' not found','quote_id'=>$quote_id);	
			echo json_encode($result_array);	  
			die;  
		}	 
		
		$product = $productRepository->getById($id); 
		//$check_price=$api_helpers->get_product_price($id);
		$orignal_price=$product->getPrice();	 
		$special_price=$product->getSpecialPrice();	 
		
		$product_iddd=$product->getId();
		$is_special=0; 
		$price=$api_helpers->get_product_prices($product_iddd);		
		$orgprice = $api_helpers->get_product_prices($product_iddd);
		$specialprice = $api_helpers->get_product_special_prices($product_iddd);
		$specialfromdate = $api_helpers->get_product_special_from_date($product_iddd);
		$specialtodate =  $api_helpers->get_product_special_to_date($product_iddd);
		$today = time(); 
		
		if (!$specialprice)
		{	
			$specialprice = $orgprice;
		} else
		{
			if(!is_null($specialfromdate) && !is_null($specialtodate))
			{

				if($today >= strtotime($specialfromdate) &&  $today <= strtotime($specialtodate))
				{
					$is_special=1;			
				}	else 
				{
					$is_special=0;
				}
			} else{
				$is_special=1;
			}
		}
		
		$sale=0;
		
		if($is_special==1)
		{
			$orignal_price=number_format($product->getPrice(), 2, '.', '');
			$special_price=number_format($product->getSpecialPrice(), 2, '.', '');
			$sale = round((($orignal_price - $special_price)/$orignal_price) * 100);
			$final_price=$special_price;
		}   	
		else
		{
			$special_price=0;
			$orignal_price= number_format($product->getPrice(), 2, '.', '');
			$final_price=0;
		}	

		$mainArray[]=array(
			'id'=>$id,		
			'qty'=>$qty,		 
			'item_id'=>$item_id,		 
			'product_name'=>$product->getName(),		
			'product_image'=>$_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl(),	
			'orignal_price'=>$orignal_price,	
			'special_price'=>$special_price,	
			'final_price'=>$final_price,	
			'sale'=>$sale,	
			'reward_point'=>0
		);   
		//Order Summary
		$check_price=$api_helpers->get_product_price($id);
		
		if($check_price==0)
		{
			$price=$product->getPrice() * $qty;	
		}
		else
		{
			$price=$check_price * $qty;			
		}	 	
		if($is_special==1)
		{
			$price=$special_price * $qty;
		}

		$shippingGstRate = $product->getGstRate();
		$gstPercent = 100 + $shippingGstRate;
		$rowTotal = $price;	 
		$productPrice = ($rowTotal - $discountAmount) / $gstPercent;
		$gstAmount = $productPrice * $shippingGstRate;
		$total_gst = $total_gst + $gstAmount;
		$total_price=$total_price + $rowTotal;
		$pincode=$custom_helpers->get_customer_shipping_address($user_id,'postcode');
	}

  $helper_ecom_expreess = $objectManager->create('OM\ExpressShipping\Helper\Data');
	$shippingAmount_response=$helper_ecom_expreess->getExpressShippingPrice($quote_om); 
	if(is_array($shippingAmount_response) && !empty($shippingAmount_response))
	{
		$total_shippingAmount = $shippingAmount_response['price']; 	
		$ship_allowed = $shippingAmount_response['allowed'];
		if($pincode_correct==false)
		{
			$ship_allowed=false;
		}
	}
	
	$user_array=$mainArray; 
	$total_base_amount=$total_price - $total_gst;
	$total_gst=number_format($total_gst, 2, '.', '');
	$total_base_amount=number_format($total_base_amount, 2, '.', '');
	
	/* removing  dynamic calculation of  amount , amount should be same as  what present in quote table @ritesh*/
	// $subtotal=$total_gst + $total_base_amount;
	// $query = "select subtotal from quote where entity_id='$quote_id'";
	// $subtotal = $connection->fetchOne($query);
	$subtotal=$quote_om->getData('subtotal');




$total_shippingAmount = number_format($total_shippingAmount, 2, '.', '');

$order_total = $subtotal + $total_shippingAmount;
/* removing  dynamic calculation of  amount , amount should be same as  what present in quote table @ritesh*/
// $order_total=$quote_om->getData('grand_total');

$subtotal= number_format($subtotal, 2, '.', '');
$order_total= number_format($order_total, 2, '.', '');

$summary_array= array('base_price'=>$total_base_amount,'total_gst'=>$total_gst,'subtotal'=>$subtotal,'shipping_handling'=>$total_shippingAmount,'order_total'=>$order_total);

$user_address=0;
if($address!=0) 
{
	$user_address=1;
}	

$noDeliveryMsg="";
$pincode = $connection->fetchOne("SELECT postcode FROM `customer_address_entity` WHERE `entity_id` = '$addr_id'");
if($pincode){
	$rdata = $connection->fetchRow("SELECT is_active,msg FROM `pincode_details` WHERE `pincode` = '$pincode'");
	if($rdata['is_active']==0){
		$noDeliveryMsg = 'Due to covid restriction, we are not shipping to the location selected by you.Please chose a different location.';
		if($rdata['msg']!=null && trim($rdata['msg'])!=''){
			$noDeliveryMsg = $rdata['msg'];				
		}
	}
}

$result_array=array('success' => 1, 'result' => $user_array,'deliverable' => $ship_allowed,"delivery_message"=>"$noDeliveryMsg","delivery_title"=>"Covid Restriction", 'summary'=> $summary_array,'cart_total'=>$cart_total,'quote_id'=>$quote_id,'address_exist'=>$user_address,'error' => 'Cart Page Found'); 	
echo json_encode($result_array); 	  
die;	 

}
else
{
	 
	$result_array=array('success' => 1, 'result' => $user_array,'quote_id'=>$quote_id,'error' => 'Cart is empty'); 	
	echo json_encode($result_array);	
	die; 
}	
