<?php
	
	//https://dev.idsil.com/design/api/wishlist_product/?productId=1399&accesstoken=1234&customer_id=269
	require "../security/sanitize.php";
	$result = sanitizedJsonPayload();

	// $result = $_GET;
		
	/*{"accesstoken": "1234","productId":"1399","customer_id":"269"}*/

	// print_r($result);
	// die();
     
	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager();
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');

	$argumentsCount=count($result);
	$wishlist_array=null; 

	if($argumentsCount < 3 || $argumentsCount > 3) 
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}	


	if(!isset($result['accesstoken']))
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Access Token is missing'); 	
		echo json_encode($result_array);	
		die; 
	}
	if(!isset($result['productId']))
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Product ID is missing'); 	
		echo json_encode($result_array);	 
		die; 
	}
	if(!isset($result['customer_id']) || empty(@get_numeric($result['customer_id'])))
	{
		$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Customer id is missing'); 	
		echo json_encode($result_array);	
		die; 
	}


  

	$accesstoken=$result['accesstoken'];
	$productId=$result['productId'];
	$customer_id=@get_numeric($result['customer_id']);



	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 

	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else
	{
	
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0)
		{
			$result_array=array('success' => 0, 'result' => $wishlist_array, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
			
	}

	if($productId=='')
	{
		$result_array=array('success' => 0,'result' => $wishlist_array, 'error' => 'Product ID is missing'); 	
		echo json_encode($result_array);	
		die;
	}


	if($customer_id=='')
	{
		$result_array=array('success' => 0,'result' => $wishlist_array, 'error' => 'Customer ID is missing'); 	
		echo json_encode($result_array);	
		die;
	}

	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data');
	$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
	if($authentication_response['success']==0)
	{
		echo json_encode($authentication_response);	
		die;
	}
	
   try{
		// $customer_id = 2;

				$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
				$wishlistAdd = $wishList->create()->loadByCustomerId($customer_id, true);
				$message ='';
			


				$_in_wishlist = false;
				foreach ($wishlistAdd->getItemCollection() as $_wishlist_item){
					if($productId == $_wishlist_item->getProduct()->getId()){
					$_in_wishlist = true;
					}
				}
				
				if($_in_wishlist){
					//echo 'Removing product';

					 //$wish = $this->wishlist->loadByCustomerId($customerId);
					$items = $wishlistAdd->getItemCollection();
					foreach ($items as $item) 
					{
						if ($item->getProductId() == $productId) 
						{
							$item->delete();
							$wishlistAdd->save();   
							$message ="This product has been removed from Wishlist";								
						}
					}

				}else{
					//echo 'Adding product';

					// Add if product is not in wishlist
					$product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);

					$wishlistAdd->addNewItem($product);
					$wishlistAdd->save();
					
					$message = $product->getName()." is added to Wishlist";


                }
		
		$result_array=array('success' => 1, 'result' =>$wishlist_array, 'error' => ''.$message.'.');
		
	}catch(Exception $e){
		$result_array=array('success' => 0, 'result' =>$wishlist_array, 'error' => $e->getMessage()); 	
	} 
	 
	echo json_encode($result_array);
?>