<?php 
error_reporting(0);
$current_file_name =  basename(__FILE__, '.php');
sanitizeAutomatic();

/** automatic sanitize get and post request*/
function sanitizeAutomatic()
{
  sanitizeAllGet();
  sanitizeAllPost();
}

/**
 * [strip everything accept  alphabet A-Z , a-z , number 0-9 and character (space _ @ .) are  allowed]
 *
 * @return  [post/get]  [return NULL]
 */
function sanitizeAllGet()
{
  if(!empty($_GET))
  {
    foreach ($_GET as $key => $value) {
      $value = preg_replace('/[^A-Za-z0-9 @_.-]/', '', $value);
      $_GET[$key] = $value;
    }
  }
}

/**
 * [strip everything accept  alphabet A-Z , a-z , number 0-9 and character (space _ @ .) are  allowed]
 *
 * @return  [$POST]  [return NULL]
 */
function sanitizeAllPost()
{
  if(!empty($_POST))
  {
    foreach ($_POST as $key => $value) {
      $value = preg_replace('/[^A-Za-z0-9 @_.-]/', '', $value);
      $_POST[$key] = $value;
    }
  }  
}


/**
 * [strip everything accept  alphabet A-Z , a-z , number 0-9 and character (space _ @ .) are  allowed]
 *
 * @return  [json payload]  [return Array]
 */
function sanitizedJsonPayload()
{
	$result = json_decode(file_get_contents('php://input'), true);
  if(!empty($result))
  {
    foreach ($result as $key => $value) {
      if(!$key=="password")
      {
        $value = preg_replace('/[^A-Za-z0-9 @_.]/', '', $value);
        $result[$key] = $value;
      }
    }
  }  
  return (array)$result;
}

/* return only  number */
function get_numeric($val) {
  if (is_numeric($val)) {
    return $val + 0;
  }
  return 0;
}



?>