<?php
	require "../security/sanitize.php";
	//https://www.dev.idsil.com/design/api/wishlistlisting/?accesstoken=1234&user_id=269

	//$result = sanitizedJsonPayload();
	$result = $_GET;
		   
	

	include('../../app/bootstrap.php');
	use Magento\Framework\App\Bootstrap;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
	$objectManager = $bootstrap->getObjectManager(); 
	
	
	$appState = $objectManager->get('\Magento\Framework\App\State');
	$appState->setAreaCode('frontend');
	
	$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
	$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
	$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');	
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	
	$argumentsCount=count($result);
	$data=null; 
	
	$helpers = $objectManager->create('Panasonic\CustomUser\Helper\Data'); 
	$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
	//Reward helper
	$earnOutput = $objectManager->get('\Mirasvit\Rewards\Helper\Output\Earn');
	
	//Store
	$store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
	
	// Wishlist
	$wishList = $objectManager->get('\Magento\Wishlist\Model\WishlistFactory');
	$StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
	$stockRegistry = $objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface'); 
	if($argumentsCount < 2 || $argumentsCount > 2) 
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Input count not matched'); 	
		echo json_encode($result_array);	
		die;
	}	


	if(!isset($result['accesstoken']))
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Access Token is missing'); 	
		echo json_encode($result_array);	
		die; 
	}

	if(!isset($result['user_id']) || empty(@get_numeric($result['user_id'])))
	{
		$result_array=array('success' => 0, 'result' => $data, 'error' => 'Customer id is missing'); 	
		echo json_encode($result_array);	
		die; 
	}


	$accesstoken=$result['accesstoken'];
	$customer_id=@get_numeric($result['user_id']);

	
	
	
	if($accesstoken=='')
	{
		$result_array=array('success' => 0, 'error' => 'Token is missing'); 	
		echo json_encode($result_array);	
		die;
	}else{
	
		$checkToken=$helpers->checkToken($accesstoken); 
		if($checkToken==0){
			$result_array=array('success' => 0, 'result' => $data, 'error' => 'Session expired');	
			echo json_encode($result_array);	
			die;
		}
	}

	if($customer_id==''){
		$result_array=array('success' => 0,'result' => $data, 'error' => 'Customer ID is missing'); 	
		echo json_encode($result_array);	
		die;
	}
	else 
	{
		$check_user=$api_helpers->check_user_exists($customer_id);
		if($check_user==0)
		{ 
			$result_array=array('success' => 0,'result' => $user_array, 'error' => 'User Id not exists'); 	
			echo json_encode($result_array);	
			die;	 
		}

		$authentication_response = $api_helpers->authenticateUser($customer_id,$accesstoken);
		if($authentication_response['success']==0)
		{
			echo json_encode($authentication_response);	
			die;
		}


	}
	
	
	
	
   try{
		
		
		$wishlistAdd = $wishList->create()->loadByCustomerId($customer_id, true);
	
		// Check if the wishlist have product or not.
		$wishlist_count =  $wishlistAdd->getItemCollection()->count();
		if($wishlist_count < 1){
			$result_array=array('success' => 0,'result' => $data, 'error' => 'There is no product in wishlist.'); 	
			echo json_encode($result_array);	 
			die();
		}
		
		// Check the 	 			
		foreach ($wishlistAdd->getItemCollection() as $_wishlist_item){
			//print_r($_wishlist_item->getProduct()->getData());
			
			
			$id = $_wishlist_item->getProduct()->getId();
			
			$product = $productRepository->getById($id);
			$productdata['id']= $product->getId();
			$product_id=$product->getId(); 
			$productdata['name']= strip_tags($product->getName());
			$productdata['sku']= strip_tags($product->getSku());
			$productdata['price']= strip_tags($product->getPrice());
			$productdata['specialprice'] = $product->getSpecialPrice();
			$productdata['specialfromdate'] = $product->getSpecialFromDate();
			$productdata['specialtodate'] = $product->getSpecialToDate();
			
			$price = $product->getPrice();
			$productdata['price'] = number_format($price, 2);
			$specialprice = $product->getSpecialPrice();
			$specialPriceFromDate = $product->getSpecialFromDate();
			$specialPriceToDate = $product->getSpecialToDate(); 
			$today = time();
			
			if($price){
				$sale = round((($price-$specialprice)/$price)*100);
			}
			if ($specialprice) {
				if(!empty($specialPriceFromDate)){
					if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
						
						
						$productdata['specialprice'] = number_format($specialprice, 2);
						$productdata['specialfromdate'] = $specialPriceFromDate;
						$productdata['specialtodate'] = $specialPriceToDate;
						$productdata['sale_percentage'] = $sale.'%'.' Off';
						
						
					}
				}
			}
			
			$productdata['brand']= null;
			if($product->getBrands()!='')
			{
				
				$brand_id=$product->getBrands();
				// $sql_brand="select value from eav_attribute_option_value where option_id='".$brand_id."'"; 

				$sql_brand = $connection->select() 
				->from(['cae' => 'eav_attribute_option_value'])
				->where('cae.option_id=?', $brand_id);

				$result_brand = $connection->fetchAll($sql_brand);	
				$brand_name=$result_brand[0]['value']; 	 	
				
				$productdata['brand']=$brand_name;	
			}	
			
			
			
			/* if($product->getAttributeText('supported_models')){
				$productdata['supported_models']= $product->getAttributeText('supported_models');
			}else{
				$productdata['supported_models']= null;	
			} */
			 
			// From Rma extension
			$return_date = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('rmasystem/parameter/days');
			if($product->getAttributeText('return_period')=="Flat return period"){
				$productdata['return_days']= $return_date.' days';
			}else{
				$productdata['return_days']= "No Return";
			}
			
			//product image resize and setting the default image.
			if( !empty($product->getImage()) ){
				$productdata['image'] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product'.$product->getImage();
				$productdata['image'] = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize('120','120')->getUrl(); 
			}else{ 
				$site_url = $storeManager->getStore()->getBaseUrl();
				$imageUrl = $site_url.'Customimages/product-thumb.jpg';
				$productdata['image'] = $imageUrl;
			}
			
			//$productdata['earn_reward'] = $earnOutput->getProductPoints($product);
			
			$productdata['stock_Qty']= $StockState->getStockQty($product_id);
		
		
			$productStock = $stockRegistry->getStockItem($product_id);
			$productQty = $productStock->getQty(); 
			
			$in_stock=1;
			if($StockState->getStockQty($product_id)==0)
			{
				$in_stock=0; 
			}
			
			$product_qty=$product->getExtensionAttributes()->getStockItem()->getQty();
			if($product_qty==1)
			{
				$in_stock=0; 
			}		
			$productdata['in_stock']= $in_stock; 
			
			
			
			$ratings ='';
			$RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($id);
			if(!empty($RatingOb->getCount())){
				$ratings = $RatingOb->getSum()/$RatingOb->getCount();
			}
			if(!empty($ratings)){
				$productdata['rating_count'] = $ratings;
			}else{
				$productdata['rating_count'] = 0;
			} 
			
			$data[] = $productdata;
			// print_r($data);
			// die();
			
	
		}
	
		// print_r($data);
		// die();
	
		$wishlist_count=count($data);
		 
		
		if(!empty($data)){ 
			$result_array=array('success' => 1, 'result' =>$data,'wishlist_count'=>$wishlist_count, 'error' => 'Wishlist product details');
			echo json_encode($result_array);
		}else{
			//$result_array=array('success' => 0, 'result' =>$data, 'error' => 'There is no product in wishlist.');
			$wishlist_count=0;
			$result_array=array('success' => 0, 'result' =>$data,'wishlist_count'=>$wishlist_count, 'error' => '');
			echo json_encode($result_array);
			die();
		} 
			
		
	
		
	}catch(Exception $e){
		
		$result_array=array('success' => 0, 'result' =>$data,'wishlist_count'=>$wishlist_count,'error' => $e->getMessage());
		echo json_encode($result_array);		
	} 
	 
	
?>