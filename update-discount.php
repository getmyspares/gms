<?php 
include('app/bootstrap.php');
use  \Magento\Framework\App\Bootstrap;
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$app_state = $objectManager->get('\Magento\Framework\App\State');
$app_state->setAreaCode('frontend');

$objectManager   = \Magento\Framework\App\ObjectManager::getInstance();
$productCollections = $objectManager->create('\Magento\Catalog\Model\Product')->getCollection();
$productCollections->addFieldToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
        ->load();
    if ($productCollections->getSize() > 0) {
        foreach ($productCollections as $productCollection) {
         $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productCollection->getEntityId());
            $oldPrice = $product->getPrice();
            $finalPrice = $product->getFinalPrice();
            $priceDiff = $oldPrice - $finalPrice;
            $specialprice = $product->getSpecialPrice();
            $specialfromdate = $product->getSpecialFromDate();
            $specialtodate = $product->getSpecialToDate();
            $today = time();
            if (!$specialprice)
              $specialprice = $oldPrice;
            if ($specialprice< $oldPrice) {
                if ((is_null($specialfromdate) &&is_null($specialtodate)) || ($today >= strtotime($specialfromdate) &&is_null($specialtodate)) || ($today <= strtotime($specialtodate) &&is_null($specialfromdate)) || ($today >= strtotime($specialfromdate) && $today <= strtotime($specialtodate))) {

                      $sp =  $finalPrice/$oldPrice*100; 
                      if($sp > 1 && $sp < 11){
                        $product->setData('discount', 128 );
                        $product->save();
                      }
                      elseif($sp >= 11 && $sp < 21){
                        $product->setData('discount', 129 );
                        $product->save();
                      }
                      elseif($sp >= 21 && $sp < 31){
                        $product->setData('discount', 130 );
                        $product->save();
                      }
                      elseif($sp >= 31 && $sp < 41){
                        $product->setData('discount', 131 );
                        $product->save();
                      }
                      elseif($sp >= 41 && $sp < 51){
                        $product->setData('discount', 132 );
                        $product->save();
                      }
                      elseif($sp >= 51 && $sp < 61){
                        $product->setData('discount', 133 );
                        $product->save();
                      }
                      elseif($sp >= 61 && $sp < 71){
                        $product->setData('discount', 134 );
                        $product->save();
                      }
                      elseif($sp >= 71 && $sp < 81){
                        $product->setData('discount', 135 );
                        $product->save();
                      }
                      elseif($sp >= 81 && $sp < 91){
                        $product->setData('discount', 136 );
                        $product->save();
                      }
                      elseif($sp >= 91 && $sp < 101){
                        $product->setData('discount', 138 );
                        $product->save();
                      } 
                   
                }
                else{
                     $product->setData('discount', '' );
                     $product->save();
                }
            }
          
        }
    }
?>