<?php 

//error_reporting(E_ALL);
ini_set('display_errors',1);

 $file = fopen('price.csv', 'r', '"');  
 
if ($file !== false) {
	
    require __DIR__ . '/app/bootstrap.php';
    $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);

    $objectManager = $bootstrap->getObjectManager();

    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('adminhtml');

    // used for updating product info 
    $productRepository = $objectManager->get('Magento\Catalog\Model\ProductRepository');

    // used for updating product stock
    $stockRegistry = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');

    // add logging capability
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/import-update.log');
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);

    // enter the number of data fields you require the product row inside the CSV file to contain
    $required_data_fields = 2;

    $header = fgetcsv($file); // get data headers and skip 1st row

    while ( $row = fgetcsv($file, 3000, ",") ) {

        $data_count = count($row);
        if ($data_count < 1) {
            continue;
        }

        $data = array();
        $data = array_combine($header, $row);

        $sku = $data['sku'];
        if ($data_count < $required_data_fields) {
            $logger->info("Skipping product sku " . $sku . ". Not enough data to import.");
            continue;
        }

      
        $price = trim($data['price']);

        echo 'Updating product SKU: '.$sku.' and Price:'.$price.'<br />';

        try {
            $product = $productRepository->get($sku);
        } 
        catch (\Exception $e) {
            $logger->info("Invalid product SKU: ".$sku);
            continue;
        }

        // You can set other product data with $product->setAttributeName() if you want to update more data
        if ($product->getPrice() != $price) {
            $product->setPrice($price) 
                    ->setStoreId(0) // this is needed because if you have multiple store views, each individual store view will get "Use default value" unchecked for multiple attributes - which causes issues.
                    ->save();
        }

        try {
            $stockItem = $stockRegistry->getStockItemBySku($sku);
        } 
        catch (\Exception $e) {
            $logger->info("Invalid stock for product SKU: ".$sku);
            continue;
        }

      
    }
    fclose($file);
}
?>