<?php 

if(isset($_FILES['fileToUpload']['tmp_name'])){
  $csv=$_FILES['fileToUpload']['tmp_name'];
  $row = 1;
  if (($handle = fopen($csv, "r")) !== FALSE) { ?>
  <style>
  table {
  border-collapse: collapse;
  table-layout: auto;
  width: 100%;
}
td {
  overflow: scroll;
  white-space: nowrap;
  text-overflow: ellipsis;
}
  </style>
  <?php 
    echo "<table border='1'>";
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
      $num = count($data);
      echo "<tr>";
      $row++;
      for ($c=0; $c < $num; $c++) {
        if($row==2)
        {
          echo "<th>".$data[$c] . "</th>";
        } else 
        {
          echo "<td>" .$data[$c] . "</td>";
        }
      }
      echo "<tr>";
    }
    fclose($handle);
    echo "</table>";
  }
} else { ?>

<!DOCTYPE html>
<html>
<body>

<form action="<?=$_SERVER['REQUEST_URI']?>" method="post" enctype="multipart/form-data">
  Select file to upload:
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" value="Check Csv" name="submit">
</form>

</body>
</html>



<?php } ?>