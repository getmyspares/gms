<?php
// The file
	$filename = $_GET['image'];
	//$filename = 'https://store.idsil.com//pub/media/mageplaza/bannerslider/banner/image/g/e/get_my_spares_hero_no_text.jpg';
	$percent = 0.1; // percentage of resize 

	// Content type
	header('Content-type: image/jpeg');

	// Get new dimensions
	list($width, $height) = getimagesize($filename);
	$new_width = $width * $percent;
	$new_height = $height * $percent;

	// Resample
	$image_p = imagecreatetruecolor($new_width, $new_height);
	$image = imagecreatefromjpeg($filename);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

	// Output
	imagejpeg($image_p, null, 100);
?> 