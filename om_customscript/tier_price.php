<?php
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
include __DIR__.'/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();

$updateTierprice = $objectManager->create('OM\Rewrite\Helper\Updatetierprice');
$updateTierprice->updateTierPrice();
