<?php
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
// include '/data/html/app/bootstrap.php';
include '/var/www/html/demo16.mobdigi.com/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$_inlineTranslation = $objectManager->get('\Magento\Framework\Translate\Inline\StateInterface');
$_transportBuilder = $objectManager->get('\Magento\Framework\Mail\Template\TransportBuilder');

shootMail($connection, $_inlineTranslation, $_transportBuilder);

function  shootMail($connection, $_inlineTranslation, $_transportBuilder){
//echo sendEmail('bikashkaushik10@gmail.com', $_inlineTranslation, $_transportBuilder);exit;

    $customer_emails = getCustomerList($connection);
    foreach  ($customer_emails as $email)
    {
        $customer_email = $email['email']; 
        $entity_id = $email['entity_id']; 

        $notblocklisted = checkifblocklisted($connection,$customer_email);
        if($notblocklisted==false)
        {
            $response  = sendEmail($customer_email, $_inlineTranslation, $_transportBuilder);
            $sql  = "update `customer_data_live` set `mail_sent` ='$response' where entity_id ='$entity_id'";
            $connection->query($sql);
        } else 
        {
            $sql  = "update `customer_data_live` set `mail_sent` ='blocklisted_email' where entity_id ='$entity_id'";
            $connection->query($sql);
        }
    }

}

function sendEmail($email, $_inlineTranslation, $_transportBuilder)
{
    $receiverInfo = [
		            'name' => '',
		            'email' => $email
		        ];

    $senderInfo = [
		            'name' => 'Getmyspares',
		            'email' => 'info@getmyspares.com',
		        ];

    $_inlineTranslation->suspend();    
    generateStoreTemplate([], $senderInfo, $receiverInfo, 1,$_transportBuilder);    
    $transport = $_transportBuilder->getTransport();
    $transport->sendMessage();        
    $_inlineTranslation->resume();

    return "success";
}

function generateStoreTemplate($emailTemplateVariables,$senderInfo,$receiverInfo,$storeid,$_transportBuilder)
{
    $template =  $_transportBuilder->setTemplateIdentifier(19)
			            ->setTemplateOptions(
			                [
			                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND, 
			                    'store' => $storeid,
			                ]
			            )
			            ->setTemplateVars($emailTemplateVariables)
			            ->setFrom($senderInfo)
			            ->addTo($receiverInfo['email'],$receiverInfo['name']);

}

function getCustomerList($connection){
    $sql =  "SELECT `email`,`entity_id` FROM `customer_data_live` where  mail_sent is  null limit 40";
   return $connection->fetchAll($sql);
}

function  checkifblocklisted($connection,$email){
   $sql  = "select email from `block_email` where email ='$email'";
    $email =  $connection->fetchOne($sql);
    if(!empty($email))
    {
        return true;
    }
    return false;
}

