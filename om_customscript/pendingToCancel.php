<?php
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
include __DIR__.'/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
$resource    = $objectManager->get('\Magento\Framework\App\ResourceConnection');
$orderManagement = $objectManager->get('\Magento\Sales\Api\OrderManagementInterface');

$lifetime = $scopeConfig->getValue('mobilepopup/payment_link/lifetime', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
$connection = $resource->getConnection();

$before = date('Y-m-d H:i:s', time()-($lifetime*60));
$query = "SELECT entity_id,grand_total,razorpay_order_id,razorpay_total FROM `sales_order_grid` WHERE status LIKE 'pending' AND created_at < '$before' ";

$pendingOrders = $connection->fetchAll($query) ;

foreach($pendingOrders as $pendingOrder){
	$orderId = $pendingOrder['entity_id'];
	$order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
	$order->cancel();
	$order->save();
	
	/*$orderManagement->cancel($orderId);
	if ($order->canCancel()) {
		echo $order->getEntityId();
		$order->cancel();
		try {
			$order->cancel();
			$order->save();
		} catch (Exception $e) {
			Mage::logException($e);
		}
	}
	$orderManagement->cancel($orderId);*/
}

