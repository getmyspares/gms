<?php
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;

$root = dirname(dirname(dirname(__FILE__))); 
include $root."/app/bootstrap.php";

require_once('PHPMailer/src/PHPMailer.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(BP."/pdf/mpdf.php");

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$from = date('d-m-Y', strtotime('-7 days')).' 00:00:00';
$to = date('d-m-Y', strtotime('-1 days')).' 23:59:59';

$invoiceHelper = $objectManager->create(
			                        'Webkul\Marketplace\Helper\Invoice'
			                    );
$collection = $objectManager->create(
							'Webkul\Marketplace\Model\ResourceModel\Seller\Collection'
						);
$collection->addFieldToFilter('is_seller', 1);
$directory = $objectManager->get(
                                '\Magento\Framework\Filesystem\DirectoryList'
                            );
$invoiceDir  =  $directory->getRoot() . '/om_customscript/invoice_email/invoices';

$resource = $objectManager->create('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
		
foreach ($collection as $seller) {
	$sellerId = $seller->getSellerId();
	$collection = $objectManager->create(
                                    'Webkul\Marketplace\Model\Saleslist'
                                )
                                ->getCollection()
                                ->addFieldToFilter(
                                    'seller_id',
                                    $sellerId
                                )
                                ->addFieldToFilter(
                                    'created_at',
                                    ['datetime' => true, 'from' => $from, 'to' => $to]
                                )
                                ->addFieldToSelect('order_id')
                                ->addFieldToSelect('magerealorder_id')
                                ->distinct(true);

    foreach ($collection as $coll) {
		
		$oSt = $connection->fetchOne("SELECT status FROM `sales_order_grid` where increment_id ='". $coll->getMagerealorderId()."'");
		if(!in_array($oSt,array('pending_payment','pending','canceled'))){
			$incrementId = $coll->getMagerealorderId();
			$sellerDir = $invoiceDir.'/'.$seller->getShopUrl();
			if (!file_exists($sellerDir)) {
				mkdir($sellerDir, 0777, true);
			}
			$fileNew = $sellerDir . '/' . 'invoice_'.$incrementId.'.pdf';
			$html = $invoiceHelper->getHtmlContent($incrementId);
			$mpdf = new \mPDF('c','A4-P','','',32,25,27,25,16,13);  
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0;
			$mpdf->WriteHTML($html);
			$mpdf->Output($fileNew,'F');
		}
    }
}

$_from = date('d-m-Y', strtotime('-7 days'));
$_to = date('d-m-Y', strtotime('-1 days'));
$zipFileName = 'Invoices_'.$_from.'_'.$_to.'.zip';
$zipFileLocation = $invoiceDir.'/'.$zipFileName;

$zip = new \ZipArchive();
$zip->open($zipFileLocation, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

$files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($invoiceDir),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

foreach ($files as $name => $file) {
    $fileName = $file->getFileName();
    if (!$file->isDir() && ($fileName != $zipFileName)) {
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($invoiceDir));
        $zip->addFile($filePath, $relativePath);
    }
}
$zip->close();

/* send email to admin users */
$scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
$receivers = $scopeConfig->getValue('mobilepopup/bulkinvoices/email');
$receivers = explode(',', $receivers);

if(count($receivers)) {
    $email = new PHPMailer();
    $email->SetFrom('no-reply@getmyspares.com', 'GetMySpares');
    $email->Subject   = "GMS Weekly Invoices";

    $mail->Host = "smtp.sendgrid.net";
    $mail->Username = "apikey";
    $mail->Password = "AUfrWDBqps6FD.3kRn7w3-gsTcgqAJZit4w4f2LaDDXbeeXF4K4OKeOv";
    $mail->SMTPSecure = "tls";
    $mail->Port = 587;

    $msg = "Hello User,\n\n";
    $msg .= "Please find attached invoices generated between ".$_from." to ".$_to."\n\n";
    $email->Body = $msg;

    foreach ($receivers as $receiver) {
        $email->AddAddress(trim($receiver));
    }

    //$email->AddBCC('kaushik.bikash@orangemantra.in');
    // $email->AddBCC('malemeemail@gmail.com');
    $email->AddAttachment($zipFileLocation,$zipFileName );
    // $email->Send();
    if($email->Send())
    {
        echo "true";
    }else{
        echo "false";
    }
}

/* remove temp directory and files */
removeDirectory($invoiceDir);
function removeDirectory($path) {
	$files = glob($path . '/*');
	foreach ($files as $file) {
		is_dir($file) ? removeDirectory($file) : unlink($file);
	}
	rmdir($path);
	return;
}
