<?php

ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
include __DIR__.'/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$api_helpers = $objectManager->create('Customm\Apii\Helper\Data'); 
$custom_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Customfunction');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

$configvalue=$objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
$username = $configvalue->getValue('carriers/ecomexpress/username');
$password = $configvalue->getValue('carriers/ecomexpress/password');
if($configvalue->getValue('carriers/ecomexpress/sanbox')==1){
  $url = 'https://clbeta.ecomexpress.in/apiv2/cancel_awb/';
}
else {
  $url = 'https://api.ecomexpress.in/apiv2/cancel_awb/';
}

if(isset($_GET['awb']))
{
  $awb = $_GET['awb'];
} else 
{
  echo "awb not passed";
  die();
}

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => array('username' => $username,'password' => $password,'awbs' => $awb),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;