<?php
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
include __DIR__.'/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

class SalesReportGeneration  {


    public $connection;
    public $objectManager;
    public  $common_helpers;

    public function __construct(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
        $this->common_helpers = $objectManager->create('Panasonic\CustomUser\Helper\Reports\common');
        $this->objectManager = $objectManager; 
    }

    public function createSalesreport(){
        $today=date('Y-m-d');
		$yesterday=date('Y-m-d',strtotime("-1 days"));
        $query="select distinct `order_id`,product_gst_rate from sales_order_item where (DATE(created_at) in ('".$today."','".$yesterday."') OR DATE(updated_at) in ('".$today."','".$yesterday."')) AND product_gst_rate is null";
        
        $all_order = $this->connection->fetchAll($query);
        if(!empty($all_order))
        {
            foreach($all_order as $order)
            {
                $order_id = $order['order_id'];
                $product_gst_rate = $order['product_gst_rate'];
                if(empty($product_gst_rate) && !empty($order_id))
                {
                    $this->salesDataInsertInSalesOrderItem($order_id);
                }
            }
        }
    }

    public function salesDataInsertInSalesOrderItem($order_id_parameter){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id_parameter);
        $order_inc_id = $order->getIncrementId();
        $order_id = $order->getId();
        foreach ($order->getAllVisibleItems() as $_orderItem) {
            $product_id = $_orderItem->getProductId();
            $product_qty = $_orderItem->getQtyOrdered();
            $item_id = $_orderItem->getItemId();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);	
            $product_hsn = $product->getHsn();
            $product_gst_rate = $product->getGstRate();


            $row_total_without_tax = $_orderItem->getRowTotal() - ($_orderItem->getData('cgst_amount')+$_orderItem->getData('sgst_amount')+$_orderItem->getData('igst_amount')+$_orderItem->getData('cgst_amount')+$_orderItem->getData('discount_amount')); 
            

            $categorydate = $this->getParentandBPCategory($product->getCategoryIds());
            $bp_category=$categorydate['bp_category'];
            $product_category=$categorydate['product_category'];

            $product_comission_category = $this->getComissionCategory($product_id);
            $product_comission_percentage = $this->common_helpers->get_single_product_logic($product_id);
            $shippingDetails = $this->shippingDetails($order_id,$order_inc_id,$product_id,$product_qty,$row_total_without_tax,$product_comission_percentage);
            
            $shipping_gst_rate = $shippingDetails['shipping_gst_rate'];
            $commision = $shippingDetails['commision'];
            $shipping_invoice_total = $shippingDetails['shipping_invoice_total'];
            $shipping_gst = $shippingDetails['shipping_gst'];
            $shipping_base = $shippingDetails['shipping_base'];
            
            
            $query = "update sales_order_item set 
            bp_category='$bp_category',
            product_category = '$product_category',
            hsn_code = '$product_hsn',
            product_gst_rate = '$product_gst_rate',
            product_comission_category = '$product_comission_category',
            product_comission_percentage = '$product_comission_percentage',
            shipping_gst_rate = '$shipping_gst_rate',
            commision = '$commision',
            shipping_invoice_total =  '$shipping_invoice_total',
            shipping_gst = '$shipping_gst',
            shipping = '$shipping_base'
            where `item_id`=$item_id;
            ";
            $this->connection->query($query);
        }
    
    }

    public  function shippingDetails($order_id,$order_inc_id,$product_id,$product_qty,$row_total,$product_comission_percentage){
        $helpers = $this->objectManager->create('Panasonic\CustomUser\Helper\Data');
        $query ="SELECT * FROM `order_tax_list` where order_id='$order_inc_id'";
        $result = $this->connection->fetchAll($query);
        $tax_rates_array=json_decode($result[0]['tax_list']);
        $shipping_gst_rate = $tax_rates_array->shipping_gst;
        $admin_comm_rate = $tax_rates_array->admin_comm;
        $roundoffrowtotal = round($row_total,2,PHP_ROUND_HALF_DOWN);
        $commision=(($roundoffrowtotal*$product_comission_percentage)/100);
        $sql = "Select postcode FROM sales_order_address where parent_id='$order_id' and address_type='shipping'";
        $pincode = $this->connection->fetchOne($sql);

        /* need to  remove it and  fetch from sales_invoice table  
        cannot  do it as time constrain
        @ritesh
        */
        $shipping_invoice_total=$helpers->ecom_price_by_product($pincode,$product_id,$product_qty);
 
        $order_shipping_amount =  ($shipping_invoice_total*100)/(100+$shipping_gst_rate);
        $logistic_tax=($order_shipping_amount*$shipping_gst_rate)/100;
        $shipping_gst= number_format($logistic_tax, 2, '.', '');
        $shipping_base=$shipping_invoice_total-$shipping_gst;
        
        return  [
            "shipping_gst_rate"=>"$shipping_gst_rate",
            'commision'=>"$commision",
            'shipping_invoice_total'=>"$shipping_invoice_total",
            'shipping_gst'=>"$shipping_gst",
            'shipping_base'=>"$shipping_base",
        ];
    }


    public function getComissionCategory($product_id){
        $product_comission_category="";
        $option_id_query = "Select `value` FROM catalog_product_entity_int where entity_id='".$product_id."' and attribute_id='261'";
        $option_id = $this->connection->fetchOne($option_id_query);  
        if(!empty($option_id))
        {	
            $part_value_query = "SELECT `value` FROM `eav_attribute_option_value` where option_id=$option_id";
            $product_comission_category = $this->connection->fetchOne($part_value_query);
        }
        return $product_comission_category;
    }

    public function getParentandBPCategory($categories){
        $selected_cat_name="";
        $parent_cat_name="";
        if(!empty($categories))
        {	
            foreach($categories as $category_id)
            {
                $cat_name=$this->get_category_name($category_id);
                $selected_cat_name .=$cat_name.',';
                $cat = $this->objectManager->create('Magento\Catalog\Model\Category')->load($category_id);
                $parentCategories = $cat->getParentCategories();
                if(!empty($parentCategories))
                {
                    foreach($parentCategories as $parent)
                    {
                        $parentArray=$parent->getData();
                        if($parentArray['is_active']==1)
                        {	
                            if($parentArray['entity_id']!=$category_id) {		
                                $parent_cat_name .=	$parentArray['name'].',';		
                            } 
                        }	
                    }	 
                }									
            }
            $selected_cat_name=substr($selected_cat_name,0,-1);
            $parent_cat_name=substr($parent_cat_name,0,-1);
            $parent_cat_name=str_replace("Default Category,","",$parent_cat_name);
        }
        return array("product_category"=>$selected_cat_name,"bp_category"=>$parent_cat_name);
    }

    public function get_category_name($cat_id)
    {   
        $select = $this->connection->select()
                    ->from('catalog_category_entity_varchar') 
                    ->where('entity_id = ?', $cat_id)
                    ->where('attribute_id = ?', '45'); 
            
        $result = $this->connection->fetchAll($select);
        if(!empty($result))
        {
            return $result[0]['value'];	
        }
    }
}

$obj = new SalesReportGeneration();
$obj->createSalesreport();
$objectManager->get('OM\GenerateReport\Helper\SalesHelperModified')->generateSaleReport();

