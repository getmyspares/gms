<?php
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 18000);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
include '/data/html/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/generate_invoices.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);

$invoiceHelper = $objectManager->create('Webkul\Marketplace\Helper\Invoice');

$_resource   =  $objectManager->create('\Magento\Framework\App\ResourceConnection');
$connection = $_resource->getConnection();

$dateTime = date('Y-m-d H:i:s',strtotime('-1 days'));

$q = "SELECT * FROM `sales_shipment` WHERE `created_at` >= '$dateTime'";
$results = $connection->fetchAll($q);

foreach ($results as $result) {
	$oId = $result['order_id'];

	$incrementId = $connection->fetchOne("SELECT increment_id FROM `sales_order` WHERE entity_id = $oId");
	$sellerId = $connection->fetchOne("SELECT seller_id FROM `marketplace_orders` WHERE order_id = $oId");
	if($incrementId && $sellerId) {
		$invoiceHelper->generatePdf($sellerId, $incrementId);
		$logger->info('order: '.$incrementId.' seller: '.$sellerId);
	}
}
