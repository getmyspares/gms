<?php

use Magento\Framework\App\Bootstrap;
include __DIR__.'/../app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$url=$storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
$min_qty_filter = getMinQuantityToInclude();

$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
$collection = $productCollection->addFieldToSelect('entity_id')
	->setFlag('has_stock_status_filter', false)
	->addAttributeToSort('updated_at', 'desc')
	->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
	->joinField('stock_item', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id', 'is_in_stock=1')
	->joinField('qty', 'cataloginventory_stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')
	->addAttributeToFilter('qty',['gteq'=>$min_qty_filter])
	->setPageSize(10)
	->setCurPage(1);


$normal_product_collection = $collection->getAllIds();
/* commenting  popular  products as conditions not implemented to  check out of  stock products @ritesh13dec2021*/
// $popular_product_id_array = getPopularProducts();
$custom_include_product = getCustomAddedProducts();
$product_to_exclude = getCustomDeselectedProducts();

$file_path='google_feed/getmyspares_product_feed.csv';
$complete_path =__DIR__."/".$file_path; 
chmod($complete_path, 0777);
$file = fopen($complete_path, 'w');
fputcsv($file, array('id','title','custom_label_0','description','link','condition','price','sale_price','availability','image_link','mpn','brand','shipping_weight','custom_label_1'));
// fputcsv($file, array('id','title','custom_label_0','description','link','condition','price','qty','sale_price','availability','image_link','mpn','brand','shipping_weight'));


/* merge all collection */

/* $feeed_product_ids = array_merge($normal_product_collection,$popular_product_id_array);
$feeed_product_ids = array_merge($feeed_product_ids,$custom_include_product);
 */

$feeed_product_ids = array_merge($normal_product_collection,$custom_include_product);
$feeed_product_ids = array_diff($feeed_product_ids,$product_to_exclude);

foreach ($feeed_product_ids as $product_id) {
	$special_price="";
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
	// $product = $objectManager->create('Magento\Catalog\Model\Product')->load(15427);
	$row = array();
	$row['id'] = $product->getData('sku');
	$sku = $product->getData('sku');
	if(strlen($row['id'])>=50)
	{
		continue;
	}
	$price = $product->getData('price');
	$price = floatval($price);
	$row['title'] = $product->getData('name');
	$row['custom_label_0'] = $product->getData('part_category');
	$row['description'] = $product->getData('description');
	$row['link'] = $url.$product->getData('url_key');
	$row['condition'] = "New";
	$row['price'] = "INR ".$price;
	$final_price = $product->getPriceInfo()->getPrice('final_price')->getValue();
	$final_price = floatval($final_price);
	// $row['qty'] = $product->getData('quantity_and_stock_status')['qty'];
	
	if((int)$final_price<(int)$price)
	{
		$special_price = "INR ".$final_price;
	}
	$row['special_price'] = $special_price;
	$row['availability'] = "In Stock";
	if(empty($product->getImage()))
	{
		continue;
	}
	$row['image_link']  = $url. 'pub/media/catalog/product' . $product->getImage();
	$row['mpn'] =  $product->getData('sku');
	$productCategoryIds = $product->getCategoryIds();
	$universal_category_id=7;
	if(in_array($universal_category_id,$productCategoryIds))
	{
		$row['brand'] =  "Trion Global";
	} else 
	{
		$row['brand'] =  "Panasonic";
	}
	$parentCategories_arrya=array();
	foreach ($productCategoryIds as $categoryId){
		$categoryFactory = $objectManager->create('Magento\Catalog\Model\Category');
		$category = $categoryFactory->load($categoryId);   // Load Category
    $parentCategories = $category->getparent_id();   // Get Parent Category Id
    $midddlwecat = $category->getName();   // Get Parent Category Id
		if(!empty($parentCategories))
		{
			if($parentCategories==2)
			{
				$parentCategories_arrya[] = $midddlwecat; 	
			} else  
			{
				$categoryFactor = $objectManager->create('Magento\Catalog\Model\Category');
				$ParcategoryObj = $categoryFactor->load($parentCategories);   // Load Category
				$parentCategories_arrya[]=$ParcategoryObj->getName();
			}
		}
	}
	$row['shipping_weight'] =  $product->getData('weight');
	if(!empty($parentCategories_arrya))
	{
		$row['custom_label_0']=$parentCategories_arrya[0];
	} else 
	{
		$row['custom_label_0']="";
	}
	$product_array=['DRE0024','DRE0025','DRE0028','DRE0029','DRE0030','DRE0020','DRE0019','DRE0018','DRE0015','DRE0034','DRE0035','DRE0036','DRE0001','DRE0002','DRE0003','DRE0040','DRE0041','DRE0042','DRE0043','DRE0044','DRE0045','DRE0046','DRE0047','DRE0048','DRE0049','DRE0050','DRE0051','DRE0052','DRE0004'];
	if((in_array($sku,$product_array)))
	{
		$row['custom_label_1']="TSP";
	} else 
	{
		$row['custom_label_1'] = getSellerCode($product_id);
	}
	fputcsv($file, $row);
}
// Close the file
fclose($file);

function getPopularProducts()
{
	$product_array=array();
	$date =   date("Y-n-j", strtotime("first day of previous month"));
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
	$query = "select `product_id` from `sales_bestsellers_aggregated_monthly` where date(period) > '$date' limit  50";
	$results = $connection->fetchAll($query);
	if(!empty($results))
	{
		foreach ($results as $value) {
			$product_array[] = $value['product_id'];
		}
	}
	return $product_array;
}

function getCustomAddedProducts()
{
	$product_array=array();
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
	$includeproductstring = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('googlefeedsetting/feedsetting/include_product');
	$includeproductstring=str_replace('"', '', $includeproductstring);
	$includeproductstring=str_replace("'", '', $includeproductstring);
	$sku_array = explode(",",$includeproductstring);
	$sku_array = explode(",",$includeproductstring);
	$sku_string='"'.implode('", "', $sku_array).'"';
	$query = "select `entity_id` from `catalog_product_entity` where sku in ($sku_string)";
	$results = $connection->fetchAll($query);
	if(!empty($results))
	{
		foreach ($results as $value) {
			$product_array[] = $value['entity_id'];
		}
	}
	return $product_array;
}

function getMinQuantityToInclude()
{
	$product_array=array();
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
	$minquanfilter = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('googlefeedsetting/feedsetting/minquanfilter');
	$minquanfilter= (int)$minquanfilter;
	if(empty($minquanfilter) || $minquanfilter==0 || $minquanfilter==null)
	{
		return 10;
	}
	return $minquanfilter;
}

function getCustomDeselectedProducts()
{
	$product_array=array();
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
	$excludeproductstring = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('googlefeedsetting/feedsetting/exclude_product');
	$excludeproductstring=str_replace('"', '', $excludeproductstring);
	$excludeproductstring=str_replace("'", '', $excludeproductstring);
	$sku_array = explode(",",$excludeproductstring);
	$sku_array = explode(",",$excludeproductstring);
	$sku_string='"'.implode('", "', $sku_array).'"';
	$query = "select `entity_id` from `catalog_product_entity` where sku in ($sku_string)";
	$results = $connection->fetchAll($query);
	if(!empty($results))
	{
		foreach ($results as $value) {
			$product_array[] = $value['entity_id'];
		}
	}
	return $product_array;
}

function getSellerCode($product_id)
{
	$invoice_prefix="";
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$connection = $objectManager->create('\Magento\Framework\App\ResourceConnection')->getConnection();
	$query = "SELECT seller_id FROM `marketplace_product` where mageproduct_id='$product_id'";
	$seller_id = $connection->fetchOne($query);
	if($seller_id)
	{
		$query = "SELECT invoice_prefix FROM `marketplace_userdata` where seller_id='$seller_id'";
		$invoice_prefix = $connection->fetchOne($query);
	}
	return $invoice_prefix;
}